<?php


// next line is Yii2 default
//$params = require __DIR__ . '/params.php';

if ( file_exists(__DIR__ . '/params-local.php') !== false ){
    // params-local.php will array merge itself with params.php
    $params = require __DIR__ . '/params-local.php';
} else {
    $params = require __DIR__ . '/params.php';
}



// Next line is Yii2 v2.0.15.1 default code
// $db = require __DIR__ . '/db.php';

// This block added by dbyrd
if ( file_exists(__DIR__ . '/db-local.php') !== false ){
    // db-local.php will array merge itself with db.php
    $db = require __DIR__ . '/db-local.php';
} else {
    $db = require __DIR__ . '/db.php';
}

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    //'bootstrap' => ['log'],
    'bootstrap' => [
        'log',
        'gsm',
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '1GTkxI8wpiU-kddhSl4DSt0c_svYCDsM',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        /* default Yii User Class is a Component. The dektrium User Class is a Module
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        */
        'errorHandler' => [
            // next line is yii2 default
            // 'errorAction' => 'site/error',
            'class' => '\bedezign\yii2\audit\components\web\ErrorHandler',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'bfile' => [

           'class' => 'app\components\bfile',

           ],        
        'barray' => [

           'class' => 'app\components\barray',

           ],   
        'db' => $db,
        
        'urlManager' => [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                
                '<controller:[\w\-]+>/<id:\d+>'=>'<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>'=>'<controller>/<action>',                
                //Rules with Server Names
                //'http://127.0.0.1:8211/index.php/' => '/',
                //'http://admin.example.com:8211/' => '/admin',
            ],
        ],
        
        'authManager' => [
            //'class' => 'yii\rbac\DbManager',
            'class' => 'dektrium\rbac\components\DbManager'
        ],
        
//        'view' => [
//             'theme' => [
//                 'pathMap' => [
//                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
//                 ],
//             ],
//        ],

        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-black',
                    
//            "skin-blue",
//            "skin-black",
//            "skin-red",
//            "skin-yellow",
//            "skin-purple",
//            "skin-green",
//            "skin-blue-light",
//            "skin-black-light",
//            "skin-red-light",
//            "skin-yellow-light",
//            "skin-purple-light",
//            "skin-green-light"                    
                ],
            ],
        ],
    ],
    'params' => $params,
    'modules' => [
        /* dektrium */
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin','dana-byrd']
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
        // audit generates a lot of data
        // 'audit' => 'bedezign\yii2\audit\Audit',
        'gridview'    => ['class' => 'kartik\grid\Module'],
        'datecontrol' => ['class' => 'kartik\datecontrol\Module'],
        'markdown' => [
              'class' => 'kartik\markdown\Module',
        ],
//        'gsm'    => [
//            'basePath' => '@app/modules/gsm',
//            'class' => 'app\modules\gsm\Module'
//            
//        ],
    ],    
];

if (array_key_exists('application-name', $config['params']) !== false){
    Yii::$app->name = $config['params']['application-name'];
//    echo '<pre><code>' . yii\helpers\VarDumper::dumpAsString($config['params']['application-name']) . '</pre></code>';
//    echo '<pre><code>' . Yii::$app->name . '</pre></code>';
}



if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
    
    $config['bootstrap'][] = 'gsm';
    $config['modules']['gsm'] = [
        'class' => 'app\modules\gsm\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];    
}



// Next line is Yii2 v2.0.15.1 default code
// return $config;

// This block added by dbyrd
if ( file_exists(__DIR__ . '/web-local.php') !== false ){
    // web-local.php is merged with the contents of web.php 
    $config_local = require __DIR__ . '/web-local.php';
    $config_master= yii\helpers\ArrayHelper::merge($config, $config_master);
} else {
    return $config;
}
