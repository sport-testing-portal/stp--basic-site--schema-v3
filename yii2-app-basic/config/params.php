<?php

return [
    'adminEmail' => 'admin@example.com',
    'application-name' => 'My Application',
    'company-name'     => 'My Company',
    'version'          => '0.6.0',
    'copyright'        => '<strong>Copyright &copy; 2017-2018 <a href="http://example.com">Example.com</a></strong>. All rights reserved.',
    
    // See also: Semantic Versioning Specification (SemVer)
    //   http://semver.org/
    'version_info'=>[
            'major'=>'0',
            'minor'=>'7',
            'patch'=>'0',
            'suffix_alpha'=>'a',
            'suffix_beta'=>'b',
            'suffix_release_candidate'=>'RC',
            'release_candidate_iteration'=>'0',
            // alpha, beta, RC, production
            'user_testing_status'=>'alpha',
            // construction, testing, staging, production-stabilizing, production, depreciated
            'app_life_cycle_status'=>'construction',
            //'app_life_cycle_status'=>'testing',
            //'app_life_cycle_status'=>'staging',
            //'app_life_cycle_status'=>'production-stabilizing',
            //'app_life_cycle_status'=>'production',
            //'app_life_cycle_status'=>'depreciated',
    ],    
];
