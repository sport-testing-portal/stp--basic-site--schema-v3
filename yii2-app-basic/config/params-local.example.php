<?php

return yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/params.php',
    [
        'application-name' => 'Something Catchy',
        'company-name'=>'mycompany.com',
        'adminEmail' => 'admin@mycompany.com',  
        'version' => '0.6.0',
        // set vars for the PHP environment? 
        'app_title'=>'',                   // unused deadwood
        'app_version'=>'',                 // unused deadwood
        // Use a short name for the menu header
        'app_name' => 'Something Catchy',
        'copyright'   => '<strong>Copyright &copy; 2017-2018 <a href="http://mycompany.com">mycompany.com</a></strong>. All rights reserved.',
    ]
);

