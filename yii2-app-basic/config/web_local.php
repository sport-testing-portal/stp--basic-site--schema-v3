<?php


$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    //'bootstrap' => ['log'],
    'bootstrap' => [
        'log',
        'gsm',
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'bfile' => [

           'class' => 'app\components\bfile',

           ],        
        'barray' => [

           'class' => 'app\components\barray',

           ],   
        'db' => $db,
        
        'urlManager' => [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                
                '<controller:[\w\-]+>/<id:\d+>'=>'<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>'=>'<controller>/<action>',                
                //Rules with Server Names
                //'http://127.0.0.1:8211/index.php/' => '/',
                //'http://admin.example.com:8211/' => '/admin',
            ],
        ],
        
        'authManager' => [
            //'class' => 'yii\rbac\DbManager',
            'class' => 'dektrium\rbac\components\DbManager'
        ],
        
//        'view' => [
//             'theme' => [
//                 'pathMap' => [
//                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
//                 ],
//             ],
//        ],

        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-black',
                    
//            "skin-blue",
//            "skin-black",
//            "skin-red",
//            "skin-yellow",
//            "skin-purple",
//            "skin-green",
//            "skin-blue-light",
//            "skin-black-light",
//            "skin-red-light",
//            "skin-yellow-light",
//            "skin-purple-light",
//            "skin-green-light"                    
                ],
            ],
        ],
    ],
    'modules' => [
        'gsm'    => [
            'basePath' => '@app/modules/gsm',
            'class' => 'app\modules\gsm\Module'
            
        ],
    ],    
];

if (array_key_exists('application-name', $config['params']) !== false){
    Yii::$app->name = $config['params']['application-name'];
//    echo '<pre><code>' . yii\helpers\VarDumper::dumpAsString($config['params']['application-name']) . '</pre></code>';
//    echo '<pre><code>' . Yii::$app->name . '</pre></code>';
}



if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
    
    $config['bootstrap'][] = 'gsm';
    $config['modules']['gsm'] = [
        'class' => 'app\modules\gsm\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];    
}

return $config;
