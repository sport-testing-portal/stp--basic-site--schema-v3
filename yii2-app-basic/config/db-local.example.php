<?php
return yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/db.php',
    [
        'class' => 'yii\db\Connection',
        //'dsn' => 'mysql:host=localhost;dbname=yii2advanced_test',
        //'dsn' => 'mysql:host=localhost;dbname=yii2basic_utf8',
        'dsn' => 'mysql:host=localhost;dbname=sport__dev_design_003',
        'username' => 'mysqlroot',
        'password' => 'mysqlpassword',
        'charset' => 'utf8',                
    ]
);

