<?php

return yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/params.php',
    [
        'application-name' => 'STP Basic Site',
        'company-name'=>'hypothetical-company.com',
        'adminEmail' => 'admin@example.com',  
        'version' => '0.6.0',
        // set vars for the PHP environment? 
        //'app_title'=>'Address Test p8211',
        //'app_version'=>'',
        // Use a short name for the menu header
        'app_name' => 'STP Basic Site',
        'copyright'=> '<strong>Copyright &copy; 2017-2018 <a href="http://byrdbrain.webredirect.org">STP Demos</a></strong>. All rights reserved.',
        'copyleft' => '<strong>Copyleft <span class="copy-left">&copy; 2017-2018 <a href="http://byrdbrain.webredirect.org">STP Demos</a></strong>. All rights reserved.',
        //<span class="copy-left">&copy;</span><<year>> <<ORGANIZATION-NAME>>
    
        // List paths relative to the epf_inbox folder see: alias inbound_datafile_path
        // @since 0.8.0
        'data_input__by_file__path'=>[
            'real_path_base_folder' =>realpath(__DIR__ . '/../../epf_inbox'), // one folder above protected
            'base_folder'=>'/',
            'base_folder2'=>'/epf_inbox',
            'to_process' =>'/data_sources__to_process/',
            'archive'    =>'/data_sources__archive/',
            'purged'     =>'/data_sources__to_purged/',
            // example base
        ],        
        
        
        // Code Development Status Settings
        // See also: Semantic Versioning Specification (SemVer)
        //   http://semver.org/
        // @since 0.8.0
        // local development status-version metadata accessed in debug mode 
        // consumed by module dbyrd/refactor-codebase
        //    features code-generation, bulk code-maintenence, 
        //    scenario and mvc class aware multi-file edit-replace with highlighted preview
        'version_info'=>[
                'major'=>'0',
                'minor'=>'6',
                'patch'=>'0',
                'release_candidate_iteration'=>'0',
                
                // version-constants listed below
                'suffix_alpha'=>'a',
                'suffix_beta'=>'b',
                'suffix_release_candidate'=>'RC',
                
                'user_testing_status'=>'alpha',
                'user_testing_status_options'=>['alpha', 'beta', 'RC', 'production'],
                
                'app_life_cycle_status'=>'construction',            
                'app_life_cycle_status_options'=>[
                    'construction',
                    'testing',
                    'staging',
                    'production-stabilizing', 
                    'production',
                    'depreciated',
                    'sunset',
                ],
                
                'dev_cycle_status'=>'highly unstable',
                'dev_cycle_status_desc'=>'destructive construction underway',
                'dev_cycle_status_comments'=>'architectural changes are frequent',
                'local-dev'=>[
                    'dev_cycle_status'=>'micro level code construction and code testing',
                    'dev_cycle_scope'=>'a single function on a single class',
                    'dev_cycle_scope_affect'=>'global - affects the core code of all stp projects',
                    'dev_cycle_status_options'=>[
                        'code construction',
                        'destructive testing',
                        'non-destructive testing',    
                        'user-testing',
                        'code refactoring',
                        'staging',
                        'production-stabilizing',
                        'production-stable',
                        'depreciated',
                        'sunset',
                    ]
                    
                //'app_life_cycle_status'=>'staging',
                //'app_life_cycle_status'=>'production-stabilizing',
                //'app_life_cycle_status'=>'production',
                //'app_life_cycle_status'=>'depreciated',
                ],
        ],
        // Bulk Refactoring Module - Folders
        // @since v0.8.0
        'refactor-codebase'=>[
            'folders'=>[
                '_scaffolds','_tests','_ports','_refactored',
            ],
            'files'=>['ported-files-info'],
              
            'scaffolds'=>[
                'path'=>'_scaffolds',
                'desc'=>'generated code cherry picked for parameters and functions',
                'purpose'=>'allows the use of multiple code generators with harming existing code files',
                'path-models'      =>'app/models/_scaffolds',
                'path-controllers' =>'app/controllers/_scaffolds',
                'path-views'       =>'app/views/_scaffolds',
            ],
            'tests'=>[
                'path'=>'_tests',
                'desc'=>'test data for mvc testing and test result log files',
                'purpose'=>'allows developers and software quality assurance staff to easily test and xray code issues',
                'path-models'      =>'app/models/_tests',
                'path-controllers' =>'app/controllers/_tests',
                'path-views'       =>'app/views/_tests',
            ],
            'ports'=>[
                'path'=>'_ports',
                'desc'=>'non-running read-only template program files from other projects',                
                'purpose'=>'allows obsolete code sources to be rewritten while using 3rd party files as supplimental ideas and concepts',
                'path-models'      =>'app/models/_ports',
                'path-controllers' =>'app/controllers/_ports',
                'path-views'       =>'app/views/_ports',
            ],
            'refactored'=>[
                'path'=>'_refactored',
                'desc'=>'a ported file archive eg files that have been fully ported and contain no addition value',                
                'purpose'=>'allows project burndown tools to scrape the project folders to obtain a port completion status of a file',
                'path-models'      =>'app/models/_refactored',
                'path-controllers' =>'app/controllers/_refactored',
                'path-views'       =>'app/views/_refactored',
            ]  
                
       ],
            
    ]
);

