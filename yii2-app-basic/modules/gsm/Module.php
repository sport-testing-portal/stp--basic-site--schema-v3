<?php

namespace app\modules\gsm;

class Module extends \yii\base\Module 
{ 
    public $controllerNamespace = 'app\modules\gsm\controllers'; 
    public $myModuleName = '';
        
    public function init() 
    { 
        parent::init(); 
        
        // initialize the module with the configuration loaded from config.php
        //\Yii::configure($this, require __DIR__ . '/config.php');        
        \Yii::configure($this, require __DIR__ . '/config.php');        
        \Yii::trace($message='runing gsm init', $category='app modules');
        \Yii::$app->getUrlManager()->addRules([
            [
                'class' => 'yii\web\UrlRule',
                'route' => $this->id,
                'pattern' => $this->id,
            ],
            [
                'class' => 'yii\web\UrlRule',
                'route' => $this->id . '/<controller>/<action>',
                'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>',
            ]
        ], false);        
        
        $this->setAliases([ 
            '@gsm-assets' => __DIR__ . '/assets' 
        ]); 
    } 
} 

