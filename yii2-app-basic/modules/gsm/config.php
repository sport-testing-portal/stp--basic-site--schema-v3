<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'myModuleName'=>'GSM Frontend Module',
    'components' => [
        // list of component configurations
        'urlManager' => [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                
                '<controller:[\w\-]+>/<id:\d+>'=>'<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>'=>'<controller>/<action>',                
                //Rules with Server Names
                //'http://127.0.0.1:8211/index.php/' => '/',
                //'http://admin.example.com:8211/' => '/admin',
            ],
        ],        
    ],
    'params' => [
        // list of parameters
    ],
];
