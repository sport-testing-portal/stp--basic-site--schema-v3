<?php

namespace app\modules\gsm\assets; 
use yii\web\AssetBundle; 

class GsmAsset extends AssetBundle 
{ 
    // the alias to your assets folder in your file system 
    public $sourcePath = '@gsm-assets'; 
    
    //public $sourcePath = '@app/modules/gsm/assets'; 
    public $publishOptions = [];
    // finally your files..  
    public $css = [ 
      'css/first-css-file.css', 
      //'css/second-css-file.css', 
    ]; 
    public $js = [ 
      //'js/first-js-file.js', 
      //'js/second-js-file.js', 
    ]; 
//    public $img = [ 
//      'img/forest_image.jpg', 
//      'img/sport_testing__header_logo.png', 
//    ];    
    // that are the dependecies, for makeing your Asset bundle work with Yii2 framework 
    public $depends = [ 
        'yii\web\YiiAsset', 
        'yii\bootstrap\BootstrapAsset', 
    ]; 
} 
