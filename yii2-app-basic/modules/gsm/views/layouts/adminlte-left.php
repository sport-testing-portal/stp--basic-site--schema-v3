<?php 
use yii\helpers\Html; 

$userId = (Yii::$app->getUser() ? Yii::$app->getUser()->getId() : null);
if (is_null($userId) ){
    // Initialize any user attribute variables using generic slug values
    $altTitle = 'User Image';
}

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php
                if (! is_null($userId)){
                    $avatarUrl = $profile->getAvatarUrl(230);
                    $altTitle = empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name); 
                    if (!empty($avatarUrl)){
                        echo Html::img($avatarUrl, [
                            'class' => 'img-rounded img-responsive',
                            'alt' => $altTitle,
                            ]); 
                    }
                } else {
                    echo '<img src="<?= $directoryAsset ?>/img/user7-128x128.jpg" class="img-circle" alt="User Image"/>';
                }
                ?>
            </div>
            <div class="pull-left info">
                <p><?=$altTitle?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php 
        $identity = Yii::$app->user->identity;
        if (!is_null($identity)){
            $isAdmin = $identity->getIsAdmin();
        } else {
            $isAdmin = false;
        }        
        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Home', 'url' => ['/']],
                    ['label' => 'Orgs', 'url' => ['/org']],
                    ['label' => '', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Users', 'url' => ['/user/admin/index'], 'visible' => $isAdmin],
                    ['label' => 'Audit', 'url' => ['/audit'], 'visible' => $isAdmin],
                    ['label' => 'Wiki', 'url' => ['/wiki'], 'visible' => true],                    
                    ['label' => 'Login', 'url' => ['user/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
