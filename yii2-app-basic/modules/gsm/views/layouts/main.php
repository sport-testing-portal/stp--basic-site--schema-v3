<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        //'brandLabel' => Yii::$app->name,
        'brandLabel' => Yii::$app->params['application-name'],
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    // Next block enables a "User Admin" menu for RBAC
    $identity = Yii::$app->user->identity;
    if (!is_null($identity)){
        $isAdmin = $identity->getIsAdmin();
    } else {
        $isAdmin = false;
    }
    
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            ['label' => 'Sign Up', 'url' => ['/user/registration/register'], 'visible' => Yii::$app->user->isGuest],
            ['label' => 'Users', 'url' => ['/user/admin/index'], 'visible' => $isAdmin],
            ['label' => 'Audit', 'url' => ['/audit'], 'visible' => $isAdmin],
            ['label' => 'Code Gen', 'url' => ['/gii'], 'visible' => $isAdmin],
            ['label' => 'CEC', 'url' => ['/metadata-codebase-cec'], 'visible' => $isAdmin],
            
            [
                'label' => 'GSM',
                'items' => [
                     '<li class="divider"></li>',
                     '<li class="dropdown-header">Individuals</li>',  
                    ['label' => 'Privacy Policy',  'url' => Url::toRoute('gsm/gsm/privacy')],
                    ['label' => 'Refund Policy',   'url' => Url::toRoute('gsm/gsm/refund')],
                    ['label' => 'Terms of Use',    'url' => Url::toRoute('gsm/gsm/terms')],                    
                    ]
            ],
            
            [
                'label' => 'Entities',
                'items' => [
                     '<li class="divider"></li>',
                     '<li class="dropdown-header">Individuals</li>',                    
                    ['label' => 'People/Persons', 'url' => Url::toRoute('person/index')],
                    ['label' => 'Players/Athletes', 'url' => Url::toRoute('player/index')],
                    ['label' => 'Coaches', 'url' => Url::toRoute('coach/index')],
                    
                     '<li class="divider"></li>',
                     '<li class="dropdown-header">Institutions</li>',
                    ['label' => 'Orgs', 'url' => Url::toRoute('org/index')],
                    ['label' => 'Schools', 'url' => Url::toRoute('school/index')],                    
                    ['label' => 'Teams', 'url' => Url::toRoute('team/index')],
                    ['label' => 'Camps', 'url' => Url::toRoute('camp/index')],
                    
                     '<li class="divider"></li>',
                     '<li class="dropdown-header">App Metadata</li>',
                    ['label' => 'Codebase CEC',         'url' => Url::toRoute('metadata-codebase-cec/index')],
                    ['label' => 'Codebase General',     'url' => Url::toRoute('metadata-codebase/index')],                    
                    ['label' => 'Codebase mvc Models',      'url' => Url::toRoute('metadata-codebase-mvc-model/index')],
                    ['label' => 'Codebase mvc Controllers', 'url' => Url::toRoute('metadata-codebase-mvc-control/index')],
                    ['label' => 'Codebase mvc Views',       'url' => Url::toRoute('metadata-codebase-mvc-view/index')],
                    ['label' => 'Codebase Functions',     'url' => Url::toRoute('metadata-codebase-function/index')],
                    ['label' => 'Codebase Function Items','url' => Url::toRoute('metadata-codebase-function-items/index')],
                    ['label' => 'Metadata Database',    'url' => Url::toRoute('metadata-database/index')],
                    ['label' => 'Database CEC',    'url' => Url::toRoute('metadata-database-cec/index')],
                    ['label' => 'Database CEC Status',    'url' => Url::toRoute('metadata-database-cec-status/index')],
                    ['label' => 'Database Dev',    'url' => Url::toRoute('metadata-database-dev/index')],
                    ['label' => 'Database Dev Status',    'url' => Url::toRoute('metadata-database-dev-status/index')],
                ],
            ],            
            
            
            Yii::$app->user->isGuest ? (
                //['label' => 'Login', 'url' => ['/site/login']] the default site web form won't work with Yii2-user
                ['label' => 'Sign In', 'url' => ['/user/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::$app->params['company-name'] ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
