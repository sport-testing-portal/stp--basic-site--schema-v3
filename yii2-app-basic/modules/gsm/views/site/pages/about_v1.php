<?php
/* @var $this SiteController */
/* @var $this GsmController */

//$this->pageTitle=Yii::app()->name . ' - About';
if (isset(\Yii::$app->params['app_name'])){
    //$this->pageTitle= \Yii::$app->params['app_name'] . ' - About';
    //\Yii::$app->view->title = \Yii::$app->params['app_name'] . ' - About';
    $this->title = \Yii::$app->params['app_name'] . ' - About';
    
} else {
    // use settings from app-root/config/web.php 
    // $webConfig['name'=>'My Application'];
    //$this->pageTitle=Yii::app()->name . ' - About';
    //\Yii::$app->view->title = Yii::app()->name . ' - About';
    $this->title = Yii::app()->name . ' - About';
}

// v1
//$this->breadcrumbs=array(
//	'About',
//);

$this->params['breadcrumbs'][] = ['label' => 'GSM', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

//\yii\helpers\BaseVarDumper::dump($this);


?>
<h1>About</h1>

<p>This is a "static" page. You may change the content of this page
by updating the file <code><?php echo __FILE__; ?></code>.</p>
