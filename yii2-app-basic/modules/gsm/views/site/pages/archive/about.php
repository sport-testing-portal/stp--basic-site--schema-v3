<?php
/* @var $this SiteController v1*/
/* @var $this GsmController v2*/

/* @var $this yii\web\View */

// that class is usually used, if you work with html in your view. 
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\gsm\assets\GsmAsset; 

// now Yii puts your css and javascript files into your view's html. 
$assets = GsmAsset::register($this); 
//var_dump($assets);

$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<h1>About</h1>

<p>This is a "static" page. You may change the content of this page
by updating the file <code><?php echo __FILE__; ?></code>.</p>
