<?php
use yii\helpers\Html; 
use app\modules\gsm\assets\GsmAsset; 

// now Yii puts your css and javascript files into your view's html. 
$assets = \app\modules\gsm\assets\GsmAsset::register($this); 

$this->title = \Yii::$app->name . ' - Terms of Use';
$this->params['breadcrumbs'][] = 'Terms of Use';




//var_dump($assets);

?>
<!-- Page content -->
<div id="page-content" class="animation-pullDown" style="max-width: 1200px;">
    <!-- Article Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media" style="height: 150px;">
        <div class="header-section">
            <a href="/" class="pull-right">
                <img height="100" width="100" 
					 src="<?php echo $assets->baseUrl; ?>/img/GSMlogoFINALv2.png" 
					 alt="logo" class="img-circle">
            </a>
            <h1>Terms of Use</h1>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img style="width:100%; height: 160px; top:0px; left:0px; 
			 right: -55px; margin-left: 0px; margin-right: 0px; 
			 bottom: -15px; margin-bottom: 15px; overflow: hidden;" 
			 height="150" src="<?php echo $assets->baseUrl; ?>/img/soccerfield_2560x248.png" 
			 alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Article Header -->

    <!-- Article Content -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <!-- Article Block -->
            <div class="block block-alt-noborder">
                <!-- Article Content -->
                <article>
                    <h3 class="sub-header text-center"> 
						<a name="terms_of_use"><strong>Terms of Use</strong></a>
						<br><strong>January 20, 2014</strong>
					</h3>
                    <p>
						Welcome to Soccer Department’s website (the "website"). These Terms of Use constitute 
						a legally binding agreement (the “Agreement”) that governs your use of the Soccer 
						Department website or your use of any Soccer Department soccer performance assessment 
						programs, which are provided both online and offline. For a full understanding of our 
						policies, please also review Soccer Department's 
						<a href="page?view=privacy_policy#standard_privacy_policy">Standard Website Privacy Policy</a>, 
						<a href="page?view=privacy_policy#children__child_privacy_policy">Children's Privacy Policy</a>  
						and <a href="page?view=refund_policy">Refund Policy</a>.
					</p>
					<p>
						In this Agreement, a "user" is anyone registered with a Soccer Department website 
						account, or any persons authorized by you to use the account. A "parent" refers to 
						both parents and legal guardians. "Soccer Department," "we" or "us" means Soccer 
						Department LLC.
					</p>
					
					<div style="margin-left: 100px; margin-right: 100px; font-weight: 700;" 
						 class="alert alert-warning alert-dismissable">
						<p>
							BY ACCESSING OR USING THE WEBSITE, YOU AGREE TO BE BOUND BY THE TERMS OF THIS 
							AGREEMENT, WHETHER YOU ARE A CASUAL VISITOR TO THE WEBSITE OR A REGISTERED USER. 
							YOU ALSO AGREE TO BE BOUND BY OUR <a href="page?view=privacy_policy">PRIVACY POLICIES</a>.
						</p>
					</div>
					
					
					<h3 class="sub-header text-center">
						<a name="terms_applicable_to_unregistered_and_registered_visitors">
							<strong>Terms Applicable To Unregistered And Registered Visitors To The Soccer Department Website</strong>
						</a>
					</h3>
					<div style="margin-left: 100px; margin-right: 100px;">
						<ol>
							<li style="font-size: larger">
								Availability. Soccer Department uses reasonable efforts to ensure that the website 
								is available 24 hours a day, 7 days a week. However, the website may become unavailable 
								due to planned or unplanned maintenance or upgrades, or for failures beyond the control 
								of Soccer Department. You understand and accept that Soccer Department is providing the 
								website on an 'As Is' basis without any warranty, express or implied, either to 
								merchantability or fitness for any particular purpose. The website may also be unavailable 
								due to computer viruses or attacks, strikes, shortages, riots, insurrection, fires, 
								flood, storm, explosions, acts of God, war, government action, labor conditions, 
								earthquakes, or any other cause which is beyond Soccer Department’s reasonable control. 
								Soccer Department shall not be liable to you for any interruption in the availability of 
								its website or any loss of data due to these or similar causes.
							</li>
							<li style="font-size: larger">
								Electronic Communications. When you visit the website or send e-mails to us, you are 
								communicating with us electronically. You consent to receive communications from us 
								electronically. We will communicate with you by e-mail or by posting notices on the 
								website. You agree that all agreements, notices, disclosures and other communications 
								that we provide to you electronically satisfy any legal requirement that such 
								communications be in writing.
							</li>
							<li style="font-size: larger">
								Third Party Fees. You are solely responsible for any third party fees incurred for 
								accessing the website as well as for the equipment you use to access the website.
							</li>
							<li style="font-size: larger">
								Trademarks and Copyrights. Soccer Department, its brand, marks and product names 
								are the property of Soccer Department. Third parties’ brands, names and marks are the 
								property of their respective owners. No license or right to these properties is granted 
								by Soccer Department to any person accessing the website or participating in Soccer 
								Department performance assessments or events. Soccer Department is the owner of all 
								copyright and database rights related to both the website and its performance assessments 
								and events, unless otherwise stated.
							</li>
							<li style="font-size: larger">
								External Links. Third party links are provided for convenience only and you use 
								them at your own risk. Soccer Department does not endorse, evaluate, or assume 
								responsibility for the content, usability or availability of any other site. Soccer 
								Department assumes no liability for any loss or damage caused by other sites, 
								promotional emails or linked services, or any contaminating or destructive worms, 
								viruses or Trojan horses that you might be exposed to by using these links.
							</li>
							<li style="font-size: larger">
								Revision of Terms. The terms of this Agreement may be changed at any time without 
								prior notice. Your continued use of the website constitutes acceptance of this 
								Agreement and any revisions of it.
							</li>
							<li style="font-size: larger">
								Contact. Any questions, comments or suggestions, including any report of violation 
								of this Agreement should be provided to the Administrator by sending an 
								email to: info@globalsoccermetrix.com
							</li>
						</ol>
					</div>
					<p>
						If you use the website, you are responsible for maintaining the confidentiality of 
						your account and password and for restricting access to your computer, and you agree 
						to accept responsibility for all activities that occur under your account or password. 
						If you are under 18, you may use the website only with the approval and involvement of 
						a parent or guardian. Soccer Department reserves the right to refuse service, terminate 
						accounts, remove or edit content, or cancel orders in its sole discretion.
					</p>						
					
					
					<h3 class="sub-header text-center">
						<a name="terms_applicable_to_registered_users_of_the_website">
							<strong>Terms Applicable To Registered Users Of The Website</strong>
						</a>
					</h3>
					<div style="margin-left: 100px; margin-right: 100px;">
						<ol>
							<li style="font-size: larger">
								1. <span class="text-primary">Registration</span>. In consideration of your use of the 
								website and/or performance assessment programs, you represent and certify that you are of legal 
								age to enter into a binding contract and not prevented or barred from doing so by 
								regulation or law. You agree that the information you provide is true and accurate 
								and you also agree to update it regularly. You understand and accept that your 
								failure to maintain true and accurate information with us is grounds for suspension 
								or termination of your access to the website and programs. Any malicious or intentionally 
								misleading or false information or performance results entered into the website, or 
								provided during one of its programs, are also grounds for suspension or termination of 
								your account, the loss of access to any and all information provided by and to you, and 
								may lead to additional penalties.
							</li>
							<li style="font-size: larger">
								2. <span class="text-primary">Required Age</span>: To register as a user of Soccer 
								Department's website you must be at least 18 years of age. Minor children under the 
								age of 18 who wish to access the Soccer Department website and programs must obtain 
								permission from their parents and their parents must agree to this Agreement. Children 
								younger than 13 years of age may not set up an account directly and must access the 
								Soccer Department system and their profile via their parent(s). Parents are responsible 
								for determining if the Soccer Department website, content and performance assessment 
								programs are appropriate for their children.
							</li>
							<li style="font-size: larger">
								4. <span class="text-primary">Revision of Website or Programs</span>. Soccer Department 
								retains the right to amend the website and program offerings, services, prices and fees 
								at any time, for any reason, without notice, unless a separate written agreement is fully 
								executed between Soccer Department and certain individuals or organizations, at Soccer 
								Department's sole discretion. If a material change is made, Soccer Department will use 
								its reasonable efforts to notify you of these changes by sending an email based on the 
								email address provided and maintained in your Soccer Department account. If you continue 
								to use the website and programs after receiving notification, it will be assumed you have 
								accepted these changes.
							</li>
							<li style="font-size: larger">
								5. <span class="text-primary">Site License</span>. Soccer Department grants you a personal, 
								limited, revocable, non-exclusive and non-transferable license to use the website and to 
								participate in Soccer Department's performance assessment programs. This license cannot be 
								transferred. Soccer Department retains its rights, title and interest in any and all aspects 
								of its software, performance assessment programs, images, brands, marks copyrights, 
								trade secrets, know-how and other property.
							</li>
							<li style="font-size: larger">
								6. <span class="text-primary">Restrictions on Use</span>. You may not modify the website, 
								copy or reverse engineer, assemble or compile, or take any other action that would reveal 
								any source code, trade secrets, know-how or other proprietary information of Soccer Department. 
								This license remains in effect only for the period you continue to use the website.
							</li>
							<li style="font-size: larger">
								7. <span class="text-primary">Account and Password Security</span>. When you register 
								on the Soccer Department website, you will create an account using a user i.d. and a 
								password. You are responsible for your account and password and any and all activities 
								that occur using your account and password. If you become aware of a breach of security 
								involving your account and password or of any other use of the website or programs, please 
								contact Soccer Department immediately at info@globalsoccermetrix.com. While Soccer 
								Department will take reasonable actions to investigate and resolve the breach of security, 
								Soccer Department is not liable for any loss or damage from your failure to protect the 
								security of your account and password.
							</li>
							<li style="font-size: larger">
								8. <span class="text-primary">Your Content</span>. You grant Soccer Department a 
								non-exclusive, perpetual and irrevocable license to retain, duplicate, publish, 
								display and transmit the information you provide to us. You remain the owner of the 
								information provided. You are responsible for obtaining any permission required to 
								use the information provided to Soccer Department. You also agree to indemnify and 
								hold Soccer Department harmless for its use of your information.
							</li>
							<li style="font-size: larger">
								9. <span class="text-primary">Rules of Conduct</span>. You are required to read, 
								understand and comply with Soccer Department's Rules of Conduct ("Rules") for use 
								of our website. The Rules require you to not obtain, record, publish, disclose or 
								otherwise disseminate information or take or cause others to take any action using 
								the website, devices, applications, or other recording equipment, that is:
							</li>
							<ol>
								<li style="font-size: larger">
									1. obscene, vulgar, pornographic, sexually explicit, or profane;
								<li style="font-size: larger">
									2. racially or otherwise offensive;
								<li style="font-size: larger">
									3. anonymous or uses a false identity or address;
								</li>
								<li style="font-size: larger">
									4. abusive, harassing, defamatory, or threatening;
								</li>
								<li style="font-size: larger">
									5. manipulative, misleading, intentionally inaccurate or fraudulent;
								</li>
								<li style="font-size: larger">
									6. suggestive or encourages illegal act or behavior such as theft, stalking, 
									sexual assault, trafficking in drugs, obscene or stolen material;
								</li>
								<li style="font-size: larger">
									7. an infringement of copyright or trademark law;
								</li>
								<li style="font-size: larger">
									8. a violation of the right to privacy of an individual;
								</li>
								<li style="font-size: larger">
									9. a commercial or business-related advertisement or offer to sell products or service, 
									or any other business venture, without Soccer Department's express, written permission;
								</li>
								<li style="font-size: larger">
									10. destructive in any manner; for example: email, html and text delivered viruses,
									worms, Trojan horses, denial of service attacks, spam, unsolicited offers, or any other 
									code or instruction that contaminates, disrupts or destroys access, data or communication;
								</li>
								<li style="font-size: larger">
									11. in violation any applicable federal, state, local or international law or regulation;
								</li>
								<li style="font-size: larger">
									12. financial information about yourself or any other individual or group not required 
									for making a purchase or paying fees;
								</li>
								<li style="font-size: larger">
									13. used to exploit children under 18 years of age; invades the privacy of any person, 
									including but not limited to submitting personal identifying or otherwise private 
									information about a person without their consent (or their parent's consent in the 
									case of a child under 13 years of age);
								</li>
								<li style="font-size: larger">
									14. requesting personal information from a child under 13 years of age;
								</li>
								<li style="font-size: larger">
									15. antisocial, disruptive or destructive including "flaming", "spoofing", "spamming", 
									"flooding", "trolling", "griefing" or other similar activities; and/or,
								</li>
								<li style="font-size: larger">
									16. in violation of confidentiality obligations.
								</li>
							</ol>


							<li style="font-size: larger">
								10. <span class="text-primary">Your Account</span>. You expressly agree that you 
								are solely responsible for any and all acts and omissions that occur under your account 
								or password, and you agree not to engage in any unacceptable uses of the website, which 
								include, without limitation, use of the website to:
							</li>
								<ol>
									<li style="font-size: larger">
										1. register for the website if you have not acknowledged reading, and agreed 
										to abide by, the Terms of Use, Privacy Policy, Children Privacy Policy and Refund Policy;
									</li>
									<li style="font-size: larger">
										2. post your personal information (other than profile or performance results) such 
										as instant messaging addresses, personal URLs, physical addresses and phone numbers 
										in any publicly viewable areas of the website;
									</li>
									<li style="font-size: larger">							
										3. use any manual or automated means to access other user accounts, user i.d., or passwords;
									</li>
									<li style="font-size: larger">
										4. create user accounts or payments by automated means or under fraudulent or false pretenses;
									</li>
									<li style="font-size: larger">
										5. create or transmit unsolicited electronic communications such as spam to users;
									</li>
									<li style="font-size: larger">
										6. engage in competitions or enter content that can cause physical or 
										mental harm to the participants or visitors to either Soccer Department's 
										website or Soccer Department's performance assessment programs;
									</li>
									<li style="font-size: larger">
										7. collect and publish any information about any of our Users;
									</li>
									<li style="font-size: larger">
										8. use any spider, robot, retrieval application, or any other device to retrieve 
										any portion of the website;
									</li>
									<li style="font-size: larger">
										9. reformat any of the pages that are part of the website; and/or
									</li>
									<li style="font-size: larger">
										10. engage in any other activity deemed by Soccer Department Soccer to be in 
										conflict with the spirit of these Terms and Policies.
									</li>
								</ol>
							<li style="font-size: larger">
								11. <span class="text-primary">Remove, Edit or Delete Information</span>. Soccer Department 
								reserves the right to remove, delete or edit any information that, in its sole discretion, 
								is unsafe or inappropriate in one or more areas as described in the Rules above. Soccer 
								Department also reserves the right to suspend or terminate any account from which 
								inappropriate information is entered, stored, originated or distributed, without any 
								requirement on Soccer Department's part to determine the actual individual or individuals 
								responsible. You agree to indemnify and hold Soccer Department harmless for any damage 
								of any type caused by Soccer Department enforcing its Rules.
							</li>
							<li style="font-size: larger">
								12. <span class="text-primary">Prices, Fees and Payment Disclosures</span>. The prices, fees 
								and payment schedule for Soccer Department's performance assessment programs, analytical 
								and reporting services, and other products may be provided on the website for convenience. 
								These may change at any time.
							</li>
							<li style="font-size: larger">
								13. <span class="text-primary">Making Payments</span>. Soccer Department accepts credit 
								card payments for testing events, subscriptions, reports and other products and services. 
								Soccer Department is not responsible for any denial of credit or related costs, and you 
								remain solely responsible for payments for Soccer Department’s products and services.
							</li>
							<li style="font-size: larger">
								14. <span class="text-primary">Disclosure of Your Information</span>. You understand that 
								Soccer Department may be required to access, retain and disclose your account information 
								if, in Soccer Department's good faith opinion, it is required to do so by law or in defense 
								of a potential or actual civil or criminal action.
							</li>
							<li style="font-size: larger">
								15. <span class="text-primary">Suspension and Termination of Account</span>. Soccer 
								Department reserves the right to suspend or terminate your account and block your use of 
								the website, at any time, without notice, for any reason including, but not limited to, the following:
							</li>
							<ol>
								<li style="font-size: larger">
									1. non-payment of agreed amounts under any agreements between you, your club, team or 
									professional training organization, and Soccer Department;
								</li>
								<li style="font-size: larger">
									2. breach of the terms of this agreement or the policies or guidelines established by 
									Soccer Department which are published on this website
								</li>
								<li style="font-size: larger">
									3. your actions or statements that Soccer Department believes, it its sole discretion, 
									are harmful to other users or to our company or third party partners.
								</li>
							</ol>
							<li style="font-size: larger">
								16. <span class="text-primary">You agree to indemnify</span> and hold Soccer Department harmless to you or any third party due 
								to a termination of your account for any actions defined immediately above. 
							</li>
							<li style="font-size: larger">
								17. <span class="text-primary">Privacy</span>. Please refer to Soccer Department's Privacy Policy 
								and Children's Privacy Policy for important terms and conditions regarding our efforts to respect 
								the privacy of individuals using our website.
							</li>
							<li style="font-size: larger">
								18. <span class="text-primary">No Resale of Services</span>. You agree not to reproduce, duplicate, 
								copy, sell, trade, resell or exploit for any commercial purposes, any portion of the service, use 
								of the service, or access to the service.
							</li>
							<li style="font-size: larger">
								19. <span class="text-primary">Compliance by Others</span>. We cannot and do not assure that other users 
								are or will be complying with the foregoing Rules or any other provisions of the Terms, and, as between 
								you and us, you hereby assume all risk of harm or injury resulting from any such lack of compliance. 
								We reserve the right, but have no obligation, to facilitate and resolve disputes between users.
							</li>
						</ol>					
					</div>
						
					<h3 class="sub-header text-center">
						<a name="disclaimer_of_warranty">
							<strong>DISCLAIMER OF WARRANTY</strong>
						</a>
					</h3>
					<div style="margin-left: 100px; margin-right: 100px; font-weight: 700;" 
						 class="alert alert-warning alert-dismissable">
						<p>
							a. YOUR USE OF THE WEBSITE, ITS CONTENT IS AT YOUR OWN RISK. WE DO NOT WARRANT THAT THE WEBSITE OR OUR SERVICE WILL MEET YOUR EXPECTATIONS OR REQUIREMENTS. WE HEREBY DISCLAIM ALL WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT AND FITNESS FOR PARTICULAR PURPOSE.
						</p>
					</div>
					<div style="margin-left: 100px; margin-right: 100px; font-weight: 700;" 
						 class="alert alert-warning alert-dismissable">
						<p>
							b. WE WILL NOT BE LIABLE FOR ANY LOSS OR DAMAGE CAUSED BY A DISTRIBUTED DENIAL-OF-SERVICE ATTACK, VIRUSES OR OTHER TECHNOLOGICALLY HARMFUL MATERIAL THAT MAY INFECT YOUR COMPUTER EQUIPMENT, COMPUTER PROGRAMS, DATA OR OTHER PROPRIETARY MATERIAL DUE TO YOUR USE OF THE SERVICE OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE SERVICE OR TO YOUR DOWNLOADING OF ANY MATERIAL POSTED ON IT, OR ON ANY SERVICE LINKED TO IT.
						</p>
					</div>
					<h3 class="sub-header text-center">
						<a name="">
							<strong>LIMITATION OF LIABILITY</strong>
						</a>
					</h3>
					<div style="margin-left: 100px; margin-right: 100px; font-weight: 700;" 
						 class="alert alert-warning alert-dismissable">
						<p>
							IN NO EVENT WILL WE, OUR EMPLOYEES, AGENTS, OFFICERS OR DIRECTORS BE LIABLE FOR DAMAGES OF ANY KIND, UNDER ANY LEGAL THEORY, ARISING OUT OF OR IN CONNECTION WITH YOUR USE, OR INABILITY TO USE, THE SERVICE, ANY SITES LINKED TO IT, ANY CONTENT, INCLUDING ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING BUT NOT LIMITED TO, PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL DISTRESS, LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA, AND WHETHER CAUSED BY TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN IF FORESEEABLE. NO CLAIM, SUIT OR ACTION MAY BE BROUGHT AGAINST US AFTER SIX MONTHS FROM THE UNDERLYING CAUSE OF ACTION.
						</p>
					</div>
					<h3 class="sub-header text-center">
						<a name="general_terms">
							<strong>General Terms</strong>
						</a>
					</h3>
					<div style="margin-left: 100px; margin-right: 100px;">
						<ol>
							 <li style="font-size: larger">
								Independent Contractors. No joint venture, partnership, employment, or agency relationship 
								exists between you and Soccer Department as a result of this Agreement or use of the website 
								or programs.
							</li>
							 <li style="font-size: larger">
								Enforcement. If any legal action is brought to enforce this Agreement, the prevailing party 
								will be entitled to reimbursement of its attorneys' fees, court costs, and other collection 
								expenses, in addition to any other relief it may receive from the other party.
							</li>
							 <li style="font-size: larger">
								Waiver. The failure of Soccer Department to enforce any right or provision in this 
								Agreement will not constitute a waiver of such right or provision unless acknowledged 
								and agreed to by Soccer Department in writing.
							</li>
							 <li style="font-size: larger">
								Construction. The headings of sections of the Agreement are for convenience and are 
								not to be used in interpretation.
							</li>
							 <li style="font-size: larger">
								Entire Agreement. This Agreement, along with all referenced documents, constitutes 
								the entire agreement between you and Soccer Department and governs your use of the website, 
								superseding any prior agreements between you and Soccer Department. If any provision of this 
								Agreement is found by a court of competent jurisdiction to be invalid, the parties 
								nevertheless agree that the court should endeavor to give effect to the parties' intentions 
								as reflected in the provision, and the other provisions of the Agreement remain in full 
								force and effect.
							</li>
							 <li style="font-size: larger">
								Applicable Law. By visiting the website or using Soccer Department's performance 
								assessment programs, you agree that the laws of the state of California, without 
								regard to principles of conflict of laws, will govern these Terms of Use and any 
								dispute of any sort that might arise between you and Soccer Department.
							</li>
							 <li style="font-size: larger">
								Disputes. Any dispute relating in any way to your visit to the website or to Soccer 
								Department's performance assessment programs, products or services sold or distributed 
								by Soccer Department or through the website in which the aggregate total claim for 
								relief sought on behalf of one or more parties exceeds an amount defined as a small 
								claim shall be adjudicated in any state or federal court in [insert the city that is 
								most convenient for you here], California, and you consent to exclusive jurisdiction 
								and venue in such courts.
							</li>
							 <li style="font-size: larger">
								Site Policies, Modification and Severability. Please review our other policies, such 
								as Soccer Department's Privacy Policy, Children's Privacy Policy and Refund Policy, 
								posted on this site. These policies also govern your visit to the website. We reserve 
								the right to make changes to our site, policies, and these Terms of Use at any time. 
							</li>
						</ol>
					</div>
						
						
					
					
					
                </article>
                <!-- END Article Content -->
            </div>
            <!-- END Article Block -->

            <!-- Author and More Row -->
            <div class="row">
                <div class="col-md-6">
                    <!-- More Block -->
                    <div class="block block-alt-noborder full">
                        <!-- More Content -->
                        <h3 class="sub-header">Read More</h3>
                        <ul class="fa-ul list-li-push">
                            <li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
									href="page?view=privacy_policy#standard_privacy_policy">Standard Website Privacy Policy</a></li>
                            <li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
									href="page?view=privacy_policy#children__child_privacy_policy">Child Privacy Policy</a></li>
                            <li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
									href="page?view=refund_policy">Refund Policy</a></li>
                        </ul>
                        <!-- END More Content -->
                    </div>
                    <!-- END More Block -->
                </div>
            </div>
            <!-- END Author and More Row -->
        </div>
    </div>
    <!-- END Article Content -->
</div>
<!-- END Page Content -->
