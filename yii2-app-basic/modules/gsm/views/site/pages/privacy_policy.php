<?php
$this->title= \Yii::$app->name . ' - Privacy Policy';
//$this->title= 'Privacy Policy';

$this->params['breadcrumbs'][] = $this->title;
//$this->params['breadcrumbs'][] = 'Privacy Policy';

// that class is usually used, if you work with html in your view. 
use yii\helpers\Html; 
use app\modules\gsm\assets\GsmAsset; 
// now Yii puts your css and javascript files into your view's html. 
$assets = \app\modules\gsm\assets\GsmAsset::register($this); 
?> 



<?php 


?>
<div>
<?php 
/*
if(file_exists($assets->basePath . '/img/forest_image.jpg') ){
    //echo "forrest exists! <br>";
    echo "Assets basePath " . $assets->basePath . "<br>";
} else {
    echo "Assets basePath " . $assets->basePath . "<br>";
    echo 'forrest not found! ' . $assets->basePath . '/img/forest_image.jpg' ." <br>";
}
echo Html::img($assets->baseUrl . '/img/forest_image.jpg', 
    ['class'=>'bghalf', 'width'=>'100%', 'height'=>'50%'], ['alt'=>'alt image']); 
 * 
 */


?>
    <p class="image-text-centered">
</div>

<?php 
// The following menu has no effect
//$this->menu=array(
//    array(
//            'label'=>'<span class="title"><strong>Project Actions</strong></span>',
//            'url'=> '#'),
//    array('label'=>'<i class="icon-plus"></i> Create A New Project', 'url'=>array('site/index')),
//    array('label'=>'<i class="icon-archive"></i> View Archived Projects', 'url'=>'#'),
//);
?>



<!-- Page content -->
<div id="page-content" class="animation-pullDown" style="max-width: 1200px;">
    <!-- Article Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media" style="height: 150px;">
        <div class="header-section">
            <a href="/" class="pull-right">
                <img height="100" width="100" 
					 src="<?php echo $assets->baseUrl; ?>/img/GSMlogoFINALv2.png" 
					 alt="logo" class="img-circle">
            </a>
            <h1>Privacy Policy</h1>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img style="width:100%; height: 160px; top:0px; left:0px; 
			 right: -55px; margin-left: 0px; margin-right: 0px; 
			 bottom: -15px; margin-bottom: 15px; overflow: hidden;" 
			 height="150" src="<?php echo $assets->baseUrl; ?>/img/soccerfield_2560x248.png" 
			 alt="header image" class="animation-pulseSlow">
	</div>
    <!-- END Article Header -->

    <!-- Article Content -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <!-- Article Block -->
            <div class="block block-alt-noborder">
                <!-- Article Content -->
                <article>
                    <h2 class="sub-header text-center">
						<a name="children__child_privacy_policy"><strong>Child Privacy Policy</strong></a>
						<br><strong>January 20, 2014</strong>
					</h2>
                    <p>
						Soccer Department, LLC ("Soccer Department") is focused on protecting the privacy of the 
						personal information we obtain from you. This Children's Privacy Policy (the “CPP”) 
						is designed specifically to address the policies and procedures Soccer Department follows 
						in respect to children who use the Soccer Department website and programs and who are less 
						than 13 years of age. 
					</p>
					<p>
						The Children's Privacy Policy is in addition to our 
						<a href="#standard_privacy_policy">standard Website Privacy Policy</a>
						and <a href="page?view=terms_of_use">Terms of Use</a>, which we also ask that you review carefully.
					</p>

					<h3 class="sub-header text-center">
						<a name="children__how_do_we_collect_information"><strong>How Do We Collect Information?</strong>
						</a>
					</h3>
                    <p>
						Soccer Department collects, uses and discloses personal information about participants on our website, to 
						enable them to record, monitor progress, calculate, report and compare their individual 
						performance results with other participants. For children under the age of 13, we want parents and legal 
						guardians to be fully aware of how children are able to use our website, what information they provide to us, 
						and how they participate on Soccer Department's website and in Soccer Department's assessment programs. 
						The term "parents" in this policy refers to both parents and legal guardians. 
						Children can access Soccer Department's website and view publicly available information without registering 
						or providing any personal information. If a child wishes to register to participate in Soccer Department's 
						programs, we ask their parents to complete an online profile. The information gathered for this profile 
						includes the child's full name, parent's full name and e-mail address, 
						child's birth date, user name, city, state, team or club(s), and password. We ask for the child's birth date 
						to be able to validate the child's participation and, along with their city, state, team or club, to be able 
						to provide comparisons of their performance with other participants of similar age groups and geographic areas.
					</p>
					
					<h3 class="sub-header text-center"> 
						<a name="children__what_information_do_we_publish_about_a_registered_child">
							<strong>What Information Do We Publish About A Registered Child?</strong>
						</a>
					</h3>
                    <p>
						When a parent registers an athlete on the Soccer Department system they provide the child's email address, 
						first name, last name, date of birth, city and/or state on our website. An organization, club, team or 
						assessment specialist organization may invite a parent and child to be affiliated with their particular 
						organization, club, team or assessment specialist organization or other similar organization. However, 
						the parent must complete the player registration including any personal identifying information. A 
						child under the age of 13 may not provide an email address on or through Soccer Department. Soccer 
						Department may share information about a child's performance with their club, team or other 
						affiliations provided upon registration. This enables these individuals and organizations to use 
						the Soccer Department performance data for developing programs and encouraging the child's improvement 
						in both individual and group activities. Soccer Department may publish performance data for research, 
						promotion, education and other purposes. No personally identifiable information will be shared in this way.
						Soccer Department does not allow children under the age of 13 to participate in any open forums, 
						message boards or chat rooms on the Soccer Department website. Soccer Department asks that parents 
						monitor their children's use of other open forums like Facebook, Twitter, MySpace and similar services, 
						and specifically any discussions about their involvement in Soccer Department activities, as Soccer 
						Department is unable to do so in any way.
					</p>
					
					<h3 class="sub-header text-center">
						<a name="children__how_do_we_keep_parents_and_legal_guardians_informed">
							<strong>How Do We Keep Parents And Legal Guardians Informed?</strong>
						</a>
					</h3>
                    <p>
						When an athlete registers onto the Soccer Department system, the athlete's date of birth is a required field. 
						If the date of birth indicates that the child is less than 13 years of age, the registration is halted 
						and the child's registration page is prompted with a message to have their parent register them onto 
						the Soccer Department system. If an athlete is between 13 and 17 years of age, the child must enter the 
						parent(s) email address, which is a required field to complete the registration process. Upon provision 
						of the parent email, Soccer Department sends the parent(s) an e-mail with instructions on how to complete 
						the child's registration and profile with Soccer Department. Upon receiving the email, the parent may:
					</p>
					<div style="max-width: 800px;">
						<ol>
							<li style="font-size: larger">
								Accept the child's participation on Soccer Department's website. No response to the email is required.
							</li>
							<li style="font-size: larger">
								Reject the child's participation on Soccer Department's website. A parent can do this by signing on 
								to Soccer Department and deleting the child's profile and records.
							</li>
							<li style="font-size: larger">
								Modify the child's profile. The parent is provided with access to the Soccer Department system 
								and complete access to the child’s information including: name, date of birth and email and may 
								modify these fields.
							</li>
							<li style="font-size: larger">
								The parent may upload their child's unofficial test scores onto the Soccer Department system.
							</li>
							<li style="font-size: larger">
								The parent may view and analyze and compare their child's test scores on the Soccer Department system
							</li>
						</ol>
					</div>
					
					
					<h3 class="sub-header text-center">
						<a name="children__what_is_a_child_under_13_not_allowed_to_do_on_our_website">
							<strong>What is a Child Under 13 <span style="font-weight: 700">Not</span> Allowed To Do On Our Website?</strong>
						</a>
					</h3>
                    <p>
						A child under the age of 13 is not permitted to register or create a profile on our website, 
						participate in any open forums, message boards or chat rooms on the Soccer Department website.
					</p>					

					
					<h3 class="sub-header text-center">
						<a name="children__what_can_a_parent_do_to_monitor_activity">
							<strong>What Can a Parent Do to Monitor Activity?</strong>
						</a>
					</h3>
                    <p>
						We encourage parents to monitor a child's activity on the Soccer Department website. 
						Using the parent's or child's user i.d. and password, a parent can review any 
						information provided by the child or generated by Soccer Department activities 
						at any time.
					</p>

					
					<h3 class="sub-header text-center">
						<a name="children__who_collects_and_maintains_childs_personal_information">
							<strong>Who Collects and Maintains Child's Personal Information?</strong>
						</a>
					</h3>
					<ul class="fa-ul">
						<li style="font-size: larger"><i class="fa fa-pencil fa-li text-primary"></i>Jami Dansingburg</li>
						<li style="font-size: larger"><i class="fa fa-pencil fa-li text-primary"></i>Lizzie Eves</li>
						<li style="font-size: larger"><i class="fa fa-pencil fa-li text-primary"></i>Scott Bell</li>
						<li style="font-size: larger"><i class="fa fa-pencil fa-li text-primary"></i>Dana Byrd</li>
						<li style="font-size: larger"><i class="fa fa-pencil fa-li text-primary"></i>Simon Clark</li>
						<li style="font-size: larger"><i class="fa fa-pencil fa-li text-primary"></i>Hugh Scott</li>
						<li style="font-size: larger"><i class="fa fa-pencil fa-li text-primary"></i>David Hodge</li>
					</ul>

                    <p>
						If you have any questions regarding how we maintain your child's personal information, 
						please contact Lizzie Eves at Soccer Department via email: info@globalsoccermetrix.com
						<!--a href="mailto:info@globalsoccermetrix.com?subject=Lizzie%20Eves%20Regarding%20Global%20Soccer%20Metrix%20Privacy%20Policy" 
						   title="Send email to Lizzie Eves via info@globalsoccermetrix.com">
							email Lizzie
						</a-->						
						
						<a	href="mailto:info@globalsoccermetrix.com?subject=Lizzie%20Eves%20Regarding%20Global%20Soccer%20Metrix%20Privacy%20Policy" 
							data-toggle="tooltip" data-placement="bottom" title=""
							data-original-title="Send email to Lizzie Eves via info@globalsoccermetrix.com">
							email Lizzie
						</a>						
					</p>					
					
<!-- a href="javascript:void(0)" class="btn btn-sm btn-default" 
   data-toggle="tooltip" data-placement="right" title="" data-original-title="Tooltip on right!"><i class="fa fa-chevron-right fa-fw"></i></a-->					
					
					
					<h3 class="sub-header text-center">
						<a name="children__how_can_a_parent_review_or_have_deleted_the_childs_personal_information">
							<strong>How Can a Parent Review or Have Deleted the Child's Personal Information?</strong>
						</a>
					</h3>
                    <p>
						You can review, request to have deleted, or refuse to permit any further collection 
						or use of the child’s personal information by contacting Lizzie Eves at Soccer 
						Department via email: info@globalsoccermetrix.com
						
						<a	href="mailto:info@globalsoccermetrix.com?subject=Lizzie%20Eves%20Regarding%20Global%20Soccer%20Metrix%20Privacy%20Policy" 
							data-toggle="tooltip" data-placement="bottom" title=""
							data-original-title="Send email to Lizzie Eves via info@globalsoccermetrix.com">
							email Lizzie
						</a>						
					</p>
					<p>
						Our website policies, including this Children's Privacy Policy, are subject to 
						change at any time. Please review these policies periodically. If there are 
						any material changes to Soccer Department's website policies, we will notify 
						participants through online alerts and/or email notifications. Non-material 
						changes will not be communicated. In order for Soccer Department to communicate 
						with parents we ask that you keep your personal contact information up to date. 
						Changes to our website policies will also occur to comply with changes to laws 
						and regulations applicable to Soccer Department's activities.
					</p>
					

					
					<h3 class="sub-header text-center">
						<a name="children__questions_and_comments">
							<strong>Questions and Comments</strong>
						</a>
					</h3>
                    <p>
						If you have any questions or comments about this privacy policy, please 
						share them with us by email at: Soccer Department info@globalsoccermetrix.com
						
						<a	href="mailto:info@globalsoccermetrix.com?subject=Lizzie%20Eves%20Regarding%20Global%20Soccer%20Metrix%20Privacy%20Policy" 
							data-toggle="tooltip" data-placement="bottom" title=""
							data-original-title="Send email to Lizzie Eves via info@globalsoccermetrix.com">
							email Lizzie
						</a>						
					</p>
					<p>
						Our website policies, including this Children's Privacy Policy, are subject to 
						change at any time. Please review these policies periodically. If there are 
						any material changes to Soccer Department's website policies, we will notify 
						participants through online alerts and/or email notifications. Non-material 
						changes will not be communicated. In order for Soccer Department to communicate 
						with parents we ask that you keep your personal contact information up to date. 
						Changes to our website policies will also occur to comply with changes to laws 
						and regulations applicable to Soccer Department's activities.
					</p>
					
					
					
                </article>
                <!-- END Article Content -->
            </div>
            <!-- END Article Block -->
		</div>
		<!-- END Column -->
	</div>
	<!-- END Row -->
	
    <!-- Article Content -->
    <div class="row" style="margin-top: 75px;">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <!-- Article Block -->
            <div class="block block-alt-noborder">
                <!-- Article Content -->
                <article>
                    <h2 class="sub-header text-center">
						<a name="standard_privacy_policy">
							<strong>Standard Privacy Policy</strong>
						</a>
						<br><strong>January 20, 2014</strong>
					</h2>
                    <p>
						Soccer Department, LLC ("Soccer Department") provides you with an online system 
						that enables individuals to create a personal profile, track ongoing performance, 
						and obtain score comparisons. Soccer Department is committed to protecting the privacy 
						and security of your personal information. This Privacy Policy describes how we collect 
						and use your information. By using this website, you are agreeing to the terms of the 
						Soccer Department Website Privacy Policy ("Privacy Policy"). Soccer Department's Website 
						Terms of Use ("Terms of Use") provides additional information concerning the use of this 
						website. If you are a minor or an adult responsible for a minor, please also read carefully 
						the Soccer Department Website Children Privacy Policy ("Children Privacy Policy") for special 
						information regarding Soccer Department's guidelines for users under the age of 13.
					</p>

					<h3 class="sub-header text-center">
						<a name="information_we_collect">
							<strong>Information We Collect</strong></a>
					</h3>
					<p>
						Soccer Department collects information about you and your use of the website. Our intent 
						is to get to improve your experience. Once you enter information into the system, we will 
						retain this information. We ask for information when you register, request information, 
						participate in an assessment session, record official and unofficial results, or make 
						purchases or pay fees through our website or affiliates. The information may include, 
						but may not be limited to, your full name, address, telephone number, 
						child/children’s name, email address, age, gender, team, club, or other affiliations 
						related to your sport activities. On occasion we may also conduct surveys to assist us 
						in enhancing or developing our products or to improve your experience.
					</p>
					<p>
						Your Soccer Department information and experiences may include:
					</p>
					<div style="max-width: 800px;">
					<ol style="">
						<li class="" style="font-size: larger">
							Use of Soccer Department's website
						</li>
						<li style="font-size: larger">
							Participating in official and unofficial performance assessments
						</li>
						<li style="font-size: larger">
							Involvement in Soccer Department sponsored events
						</li>						
						<li style="font-size: larger">
							Responses to direct mail and internet surveys
						</li>
						<li style="font-size: larger">
							Information provided by and to other third parties affiliated with the website or 
							Soccer Department or with Soccer Department affiliated events including, but not 
							limited to, coaches, clubs, trainers and administrators regarding your team or 
							club as well as your performance individually
						</li>
					</ol>
					</div>
					<p>
						We will also collect information from reputable sources and partners to enhance the 
						Soccer Department website, our performance assessment programs, and how we market and 
						provide services to you. Any personal information collected as part of these 
						experiences will be protected according to this Privacy Policy.
					</p>
					
					
					<h3 class="sub-header text-center">
						<a name="information_we_share">
							<strong>Information We Share</strong></a>
					</h3>
					<p>
						To deliver our products and services and to provide this website, we provide information 
						to and work closely with partners in areas including, but not limited to:
					</p>
					<ol>
						<li style="font-size: larger">
							Website design and development
						</li>
						<li style="font-size: larger">
							Market research and promotions
						</li>
						<li style="font-size: larger">
							Credit card processing
						</li>
						<li style="font-size: larger">
							Email distribution and shipping
						</li>
					</ol>
					<p>
						Based on the affiliations of the participant, Soccer Department will provide information 
						to your team administrators and coaches, and your club/organization/event administrators, 
						directors of coaching and officials, for the purpose of reviewing and analyzing the 
						athlete's test scores and ongoing athletic development. While Soccer Department will 
						use its best efforts to protect personal information, we may be required to divulge 
						information by order of a government agency or court, or by law or regulation. We 
						may also be required to take actions to protect against fraud and to protect the 
						property, safety and rights of ourselves and others who are part of the Soccer 
						Department experience.
					</p>
					
					
					<h3 class="sub-header text-center">
						<a name="how_we_protect_your_information">
							<strong>How We Protect Your Information</strong>
						</a>
					</h3>
                    <p>
						We are committed to protecting your personal information online by keeping it secure and 
						confidential. We take precautions to ensure that your account and personal information at 
						Soccer Department are accessible only by employees and affiliates who are authorized by 
						Soccer Department. We will only grant access to your account if the proper email address 
						and password are entered when you successfully log in to our website. Your password is 
						initially sent to the email you supplied during registration. The password may be changed 
						in the "edit my profile" section. Always keep your password confidential. Soccer 
						Department's website and database are backed up periodically using industry standard 
						backup procedures. To prevent your account from remaining open when not in use, your 
						online session with Soccer Department's website will be terminated after being inactive 
						for a reasonable period of time. If this occurs, you will need to sign on to your account 
						again with your user i.d. and password. Any data you may have entered and not saved will 
						not be retained when you sign back into your account. Your access to Soccer Department's 
						website is dependent upon the level of access you requested or were assigned by Soccer 
						Department. This may restrict your access to certain features or sections of the website.
						Regardless of our efforts to protect information, no data transmission over the Internet 
						or storage process can be guaranteed to be 100% secure.
					</p>					

					<h3 class="sub-header text-center">
						<a name="how_we_process_payments">
							<strong>How We Process Payments</strong>
						</a>
					</h3>
                    <p>
						Soccer Department accepts credit card payments for testing events, subscriptions, 
						reports and other products and services. To protect the security of your personal 
						financial information and to ensure that your choices for its intended use are honored. 
						Soccer Department carefully protects your data from loss, misuse, unauthorized access or 
						disclosure, alteration or destruction. In this regard, Soccer Department does not capture 
						or retain your personal credit card information on Soccer Department’s company servers.
					</p>
					

					<h3 class="sub-header text-center">
						<a name="how_we_use_cookies_and_others_identify_tags">
							<strong>How We Use Cookies and Others Identify Tags</strong>
						</a>
					</h3>
                    <p>
						A cookie is a small removable data file that is stored by your web browser on your 
						computer. We may use cookies to improve and personalize your online Soccer Department 
						experience. We also collect standard information that your internet browser provides 
						when you visit, including your IP address, type of browser, time of day, and 
						navigation information. You can change the settings on your browser to prevent 
						the use of cookies and other forms of identification and behavior recording on 
						Soccer Department's website. Although some websites required that you enable cookies, 
						for example, you are not required to do so in order to use Soccer Department's website.
					</p>
					

					<h3 class="sub-header text-center">
						<a name="">
							<strong>How To Unsubscribe Or Restrict Communications From Soccer Department</strong>
						</a>
					</h3>
                    <p>
						If you prefer not to receive information from Soccer Department, please let us know. 
						You can do this by unsubscribing from future communications by following the 
						instructions at the bottom of emails from Soccer Department or contacting us 
						at info@globalsoccermetrix.com
						
						
						<a	href="mailto:info@globalsoccermetrix.com?subject=Unsubscribing%20from%20Future%20Communications" 
							data-toggle="tooltip" data-placement="bottom" title=""
							data-original-title="Send email to Unsubscribe from Future Communications via info@globalsoccermetrix.com">
							unsubscribe
						</a>						
					</p>
					
					<h3 class="sub-header text-center">
						<a name="">
							<strong>Changes To Our Privacy Policy</strong>
						</a>
					</h3>
                    <p>
						We reserve the right to revise our Privacy Policy over time. Please revisit this 
						policy periodically to keep informed of our current approach to protecting your 
						information.
					</p>
					
					
                </article>
                <!-- END Article Content -->

            </div>
            <!-- END Article Block -->			
		</div>
		<!-- END Column -->
	</div>			
	<!-- END Row -->
	
	<!-- Author and More Row -->
	<div class="row" style="margin-top: 75px;">
		<div class="col-md-6">
			<!-- More Block -->
			<div class="block block-alt-noborder full">
				<!-- More Content -->
				<h3 class="sub-header">Read More</h3>
				<ul class="fa-ul list-li-push">
					<li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
							href="page?view=terms_of_use">Terms of Use</a></li>
					<li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
							href="page?view=refund_policy">Refund Policy</a></li>
					<li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
							href="#children__child_privacy_policy">Child Privacy Policy (on this page)</a></li>
					<li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
							href="#standard_privacy_policy">Standard Website Privacy Policy (on this page)</a></li>
					
				</ul>
				<!-- END More Content -->
			</div>
			<!-- END More Block -->
		</div>
	</div>
	<!-- END Author and More Row -->
 </div>
 <!-- END Page Content -->
