<?php
/**
 * @var $this
 * @var $model
 * @var $form
 *
 * @version 1.0
 * @author Dana Byrd <danabyrd@byrdbrain.com>
 * @internal Development Status = Golden!
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$isGsmStaff = true;

$createdDate = $model->attributes['created_dt'];
$createdBy   = $model->attributes['created_by'];
if (is_null($createdBy)){
	$createdByPersonName = 'Unknown';
} else {
	$createdByPersonName = Person::model()->with('user')->findByAttributes(['user_id'=>$createdBy]);
}
$createdText = $createdByPersonName . ' on ' . $createdDate;

$updatedDate = $model->attributes['updated_dt'];
$updatedBy   = $model->attributes['updated_by'];
if (is_null($updatedBy)){
	$updatedByPersonName = 'Unknown';
} else {
	$updatedByPersonName = Person::model()->with('user')->findByAttributes(['user_id'=>$updatedBy]);
}
$updatedText = $updatedByPersonName . ' on ' . $updatedDate;

if (! isset($_SESSION['is_gsm_staff'])){
	$auth    = Yii::app()->authManager;
	$user_id = Yii::app()->user->id;
	if ($auth->isAssigned('GSM Staff',$user_id)){
		$isGsmStaff = true;
	} else {
		$isGsmStaff = false;
	}
	$session = Yii::app()->session;
	$session['is_gsm_staff'] = $isGsmStaff;
} else {
	$isGsmStaff = $_SESSION['is_gsm_staff'];
}

//		echo $form->textFieldRow($model, 'created_dt', array('class' => 'span5','disabled'=>true,));
//		echo $form->textFieldRow($model, 'updated_dt', array('class' => 'span5','disabled'=>true,));
//		echo $form->textFieldRow($model, 'created_by', array('class' => 'span5','disabled'=>true,));
//		echo $form->textFieldRow($model, 'updated_by', array('class' => 'span5','disabled'=>true,));

// base text
//$text = "<b>Created by: $createdByPersonName </b>" . " on " . $createdDate
//		. "</br>"
//		."<b>Updated by: $updatedByPersonName </b>" . " on " . $updatedDate;

if ($model->isNewRecord){
	// User is creating a new row, so there are no values to display
	$text = "<b>Created by: </b><span style='color:darkgray;'><b>Empty</b></span>"
			. "</br>"
			."<b>Updated by: </b><span style='color:darkgray;'><b>Empty</b></span>";

} elseif ($isGsmStaff){
	// User is a GSM staff member so include the user's row id that updated/created the row
	$text = "<b>Created by: </b><span style='color:darkgray;'><b>$createdByPersonName </b>(u#$createdBy)" . " on " . $createdDate . "</span>"
			. "</br>"
			."<b>Updated by: </b><span style='color:darkgray;'><b>$updatedByPersonName </b>(u#$updatedBy)" . " on " . $updatedDate . "</span>";

} else {
	// User is a standard 'normal' user so DONT include the user's row id that performed the update/create
	$text = "<b>Created by: </b><span style='color:darkgray;'><b>$createdByPersonName </b>" . " on " . $createdDate . "</span>"
			. "</br>"
			."<b>Updated by: </b><span style='color:darkgray;'><b>$updatedByPersonName </b>" . " on " . $updatedDate . "</span>";
}

?>
	<?php
	$htmlOptions = ['style'=>'margin-bottom:0px;'];//array('class'=>'span5');
	//$htmlOptions = array('class'=>'span5');

	$htmlHeaderOptions = array('style'=>'margin-bottom:0px;color:green;');

	$htmlContentOptions = array('style'=>'margin-bottom:7px;');

		//$this->beginWidget('bootstrap.widgets.TbBox', array(
		$this->beginWidget('bootstrap.widgets.TbBoxGsmUpdatedDate', array(

			'title' => 'Data Modification History',
			//'content' => $form->textFieldRow($model, 'created_dt', array('class' => 'span5','disabled'=>true,)),
			'content'			=> $text,
			'htmlOptions'		=> $htmlOptions,
			'htmlHeaderOptions' => $htmlHeaderOptions,
			'htmlContentOptions' =>$htmlContentOptions,
		));
		$this->endWidget();
        ?>

