<?php
/* @var $this SiteController */
/* @var $this GsmController */

//$this->pageTitle=Yii::app()->name . ' - About';
if (isset(\Yii::$app->params['app_name'])){
    $this->pageTitle= \Yii::$app->params['app_name'] . ' - About';
} else {
    // use settings from app-root/config/web.php 
    // $webConfig['name'=>'My Application'];
    $this->pageTitle=Yii::app()->name . ' - About';
}
$this->breadcrumbs=array(
	'About',
);
?>
<h1>About</h1>

<p>This is a "static" page. You may change the content of this page
by updating the file <code><?php echo __FILE__; ?></code>.</p>
