<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\gsm\controllers;

use Yii;
use app\models\MetadataDatabaseCecStatus;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class GsmController extends Controller
{
    public $layout = '@app/modules/gsm/views/layouts/column1';
    
    public function actionView()
    {
//        $model = \app\models\MetaDataDatabase::findOne($id);
//        if ($model === null) {
//            throw new NotFoundHttpException;
//        }
        $this->layout = 'column1';
        //$this->layout = 'column2';
        return $this->render('/site/pages/about', [
            //'model' => $model,
        ]);
//        return $this->render('/about', [
//            //'model' => $model,
//        ]);
    }

    public function actionTerms()
    {
//        $model = \app\models\MetaDataDatabase::findOne($id);
//        if ($model === null) {
//            throw new NotFoundHttpException;
//        }
        //$this->layout = 'column1';
        $this->layout = 'column2';
        return $this->render('/site/pages/terms_of_use', [
            //'model' => $model,
        ]);

    }

    public function actionRefund()
    {
//        $model = \app\models\MetaDataDatabase::findOne($id);
//        if ($model === null) {
//            throw new NotFoundHttpException;
//        }
        //$this->layout = 'column1';
        $this->layout = 'column2';
        return $this->render('/site/pages/refund_policy', [
            //'model' => $model,
        ]);
    }

    public function actionPrivacy()
    {
//        $model = \app\models\MetaDataDatabase::findOne($id);
//        if ($model === null) {
//            throw new NotFoundHttpException;
//        }
        //$this->layout = 'column1';
        $this->layout = 'column2';
        return $this->render('/site/pages/privacy_policy', [
            //'model' => $model,
        ]);
//        return $this->render('/about', [
//            //'model' => $model,
//        ]);
    }
    
    public function actionCreate()
    {
        $model = new MetadataDatabaseCecStatus;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
}