    <!-- Begin Package y2v-0001-package-v0.0.0.1 -->        
    <!-- CEC: y2v-0001 Add verbose and test output divs in all _form files -->
    
    <!-- debug displays variable values and decision points made in the code -->
    <div id="debug"       class="note"> 
    <!--xray is typically used to run read-only events to display what 'would have happened-->    
    <div id="xray"        class="note">
    <!-- verbose displays choice points made in the code -->
    <div id="verbose"     class="note">
    <!-- test is typically used for new code and testing new features     -->
    <div id="test"        class="note">
    <!-- runtime options are only presented to staff and admins - but generally used only by developers-->
    <div id="runtime_options" class="note">
    <!-- End of package: y2v-0001-package-v0.0.0.1 --> 
