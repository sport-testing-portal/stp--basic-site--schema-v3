<?php

/* Returns a settings array for installing a cec package of code
 * 
 * 
 * @example $installArray = cecPackageInstall::package();
 * @version 0.0.0.1
 * @todo consider copying the class name to cecDefineRule because some packages
 * may later have many rules
 */

class cecPackageInstall {
    
    public function __construct() {
    
    }
    public static function __init() {
    
    }
    
    /**
     * @return array $package
     * @version 0.0.0.1
     */
    public static function package() {
        $package = self::definePackage();
        return $package;
    }
    
    /**
     * @todo add folded comments over the heredoc variables
     * @version 0.0.0.1
     */
    protected static function definePackage() {
        
        
$find = <<< FIND1
    <p class="note">
        <?php echo Yii::t('AweCrud.app', 'Fields with') ?> <span class="required">*</span>
        <?php echo Yii::t('AweCrud.app', 'are required') ?>.    </p>
FIND1;


$replace = <<< REPLACE1
    <p class="note">
        <?php echo Yii::t('AweCrud.app', 'Fields with') ?> <span class="required">*</span>
        <?php echo Yii::t('AweCrud.app', 'are required') ?>.    </p>

    <!-- Begin Package: y1v-0001-package-v0.0.0.1 -->        
    <!-- CEC: y1v-0001 Add verbose and test output divs in all _form files -->
    <div id="debug"   class="note"> 
    <div id="xray"    class="note">         
    <div id="verbose" class="note"> 
    <div id="test"    class="note">         
    <!-- End of package: y1v-0001-package-v0.0.0.1 -->        
    
REPLACE1;

            // @todo consider adding a dbCriteria array as 'selection criteria'
            // select * from metadata__codebase where file_name = '_form.php';
            return $install=[
                'codebase_cec_id'=>6, // used for status log entries
                'scope'=>[
                    'query'=>[
                        'sql'=>"select * from metadata__codebase "
                             . "where file_name = '_form.php';"]],
                'find'=>[
                    'string'    => $find,
                    'regex'     => '/<p class="note">.+?<\/p>/',
                    'regexmods' => 's',
                    'use regex' => true,
                ],

                'replace'=>[
                    'string' => $replace,
                    'regex' => $replace,
                    
                ]
            ];    
    } // end of define
    
}

