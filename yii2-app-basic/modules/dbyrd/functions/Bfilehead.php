<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: bfilehead::scrubAndRenameCsv();
 *   Original source code from: /protected/extensions/bc/tools/file.header.validate.php
 */
// class file_hdr_xlation{
class bfilehead extends CApplicationComponent
{

    public static $hdr_row_cnt_to_check = 3; // was 10
    public static $readonly_yn = "N";
    public static $verbose_yn  = "N";
    public static $debug_yn    = "N";

    // List objects
    public static $dhxp  = null;   // bc models
    public static $dhxd  = null;   // bc model
    protected static $db = null;
	public static $maps  = null;

    // List return values
    public static $line_meta_data = array();
    public static $source_hdr_mismatch_items = array();
    public static $header_conversions = array();

    public static $target_file_name = "";
    public static $target_file_uri  = "";
    public static $target_path      = "epf_inbox/data_sources__to_process/";
    public static $target_hdr_line  = "";

    public static $source__folder_uri          = "epf_inbox/data_sources__to_process/";
    public static $source__archive_target_path = "epf_inbox/data_sources__archive/";

	// Models
	// Yii models
	protected static $dataHdrXlation      = null;
	protected static $dataHdrXlationItem  = null;
	// BC models
	// protected $dhxp =  null;
	// protected $dhxd =  null;


	public function init() {
	}



	/**
	 * Write a file out
	 * @param string $data_file_uri
	 * @return string
	 */
	public static function scrubAndRenameCsv($data_file_uri=null) {
		// @todo: insert code from bc/tools/import.csv.file.php
		// Add new methods to the models
		//require_once("data.hdr.xlation__mc.php");
		//require_once("data.hdr.xlation.item__mc.php");

		// next call will rewrite a scrubbed csv file
		//$data_file_uri = "epf_inbox/data_sources__to_process/b262852a-2799-43a0-b32b-6224410c461f.csv";
		//$data_file_uri = "epf_inbox/data_sources__to_process/f9a5fe7e-ded8-438c-ae17-0491dee4078d_nces_school_sat.csv";
		$target_file_uri = self::hdr_xlation($data_file_uri);
		//otherwise
		return $target_file_uri;
	}



// <editor-fold defaultstate="collapsed" desc="clean csv files">

	/**
	 * write out new column headers based on column mapping in data_hdr_xlation_item
	 * @param string $data_file_uri
	 * @return int
	 */
    public static function hdr_xlation($data_file_uri){
        $begin_mtime = Bdate::mTime();
//        $xlation_table_list = array(
//             "parent"=>"data.hdr.xlation"
//            ,"detail"=>"data.hdr.xlation.item"
//            );

        //self::$dhxp = new sqlTableClass__data_hdr_xlation();
        //self::$dhxd = new sqlTableClass__data_hdr_xlation_item();

        self::$dataHdrXlation       = DataHdrXlation::model();
        self::$dataHdrXlationItem   = DataHdrXlationItem::model();

        // low level file open as an array of rows
        if (file_exists($data_file_uri) === false){
            //$msg = "file doesnt exist.";
            return -1;
        }
        $file_lines = file($data_file_uri);
        $items_to_process_cnt__original = sizeof($file_lines);
        $dhxp_match_found_yn = "N";
        foreach ($file_lines as $idx=>$line){
            $line_length = strlen($line);
            if ($dhxp_match_found_yn == "N" || $dhxp_match_found_yn == "Y"){

                $dhxp_match_found_yn = self::dhxp_match_found(self::$dhxp, trim($line), $idx + 1);
            }
            if($dhxp_match_found_yn == "N" && $line_length > 100) {
                // Definitely data there
            }

            if ($dhxp_match_found_yn == "YY") {
                // The column headers have been remapped. Set status for
                //   non-header data-only row processing, ie data rows only
                $dhxp_match_found_yn = "YYY";
                continue;
            }
            if ($dhxp_match_found_yn == "YYY"){
                // Write the scrubbed lines to a csv file
                $line_end = substr(trim($line), -1);
                if ($line_end == ","){
                    $line = substr(trim($line), 0, -1);
                }
                file::write_to_file(self::$target_file_uri, $line . "\n", "append");
                //break;
            }
            if ($idx >= self::$hdr_row_cnt_to_check - 1 && $dhxp_match_found_yn !== "YYY"){
                break;
            }
        }

        if (self::$verbose_yn == "Y"){
            echo "<br><br><span style='font-family:ubuntu;color:blue;font-weight:700;'>Header Conversions</span><br>";
            $output_table = "<table border='0' cellpadding='3' cellspacing='1'>"
                    ."<tr style='font-family:ubuntu;font-size:14px;font-weight:700;'>"
                    ."<td>Ordinal #</td><td>Source Column Name</td><td>Target Column Name</td></tr>";
            $rownum = 0;
            foreach (self::$header_conversions as $hdr_idx => $hdr_vals) {
                $rownum++;
                if (fmod($rownum,2)>0) {$rowcolor = "bgcolor='#CCCC99'";} else{$rowcolor = "";}

                $output_table .= "<tr $rowcolor><td align='center'>$hdr_idx</td>"
                        ."<td>".$hdr_vals["source"] ."</td>"
                        ."<td>".$hdr_vals["target"] . "</td></tr>";
            }
            $output_table .= "</table>";
            echo $output_table;

            echo "<br><br>";
            echo "<span style='font-family:ubuntu;'>Source File:</span> " . basename($data_file_uri) . "<br>";
            echo "<span style='font-family:ubuntu;'>Target File:</span> " . self::$target_file_name . "<br>";
            $target_file_uri = self::$target_file_uri;
            $end_mtime = bdate::mTime();
            echo "<br><b>$items_to_process_cnt__original</b> lines scrubbed. "
                    ."<b>" . bnum::nice_number(strlen(file_get_contents($target_file_uri)))
                    . "</b> bytes written. <b>" . bdate::mtime_elapsed($begin_mtime, $end_mtime)
                    . "</b> seconds elapsed<br>";
            //echo "<br><b>$items_to_process_cnt__original</b> lines scrubbed. <br>";
        }

        // Archive the source file
        if ($dhxp_match_found_yn == "YYY"){
            // The source file was processed successfully
            self::archive_source_file(basename($data_file_uri));
        }
		return $target_file_uri;
    }

	/**
	 * get the hdr_xlation id for a CSV file
	 * @param string $data_file_uri
	 * @return int
	 */
    public static function determine_hdr_xlation_id($data_file_uri){
        //xlation_table_list
        //     "parent"=>"data.hdr.xlation"
        //    ,"detail"=>"data.hdr.xlation.item"

        // low level file open as an array of rows
        if (file_exists($data_file_uri) === false){
            //$msg = "file doesnt exist.";
            return -1;
        }
        $file_lines = file($data_file_uri);
        $dhxp_match_found_yn = "N";
		$hdr_xlation_id = 0;
        foreach ($file_lines as $idx=>$line){
            $line_length = strlen($line);
            //if ($dhxp_match_found_yn == "N" || $dhxp_match_found_yn == "Y"){

			$hdr_xlation_id = self::dhxp_get_hdr_xlation_id(self::$dhxp, trim($line), $idx + 1);
			if((int)$hdr_xlation_id > 0){
				$dhxp_match_found_yn = "Y";
				return (int)$hdr_xlation_id;
			} else {
				$dhxp_match_found_yn = "N";
			}

            if($dhxp_match_found_yn == "N" && $line_length > 100) {
                // Definitely data there
            }

            if ($idx >= self::$hdr_row_cnt_to_check - 1 && $dhxp_match_found_yn !== "Y"){
                break;
            }
        }  // End of loop through file lines
        return 0;
    }

	/**
	 * use yii data models to find header matches
	 * @param $dhxp model, $line array, $line_num int
	 * @return string 'Y','N'
	 */
    public static function dhxp_match_found($dhxp, $line, $line_num){
        $verbose_yn = self::$verbose_yn;
        $debug_yn   = self::$debug_yn;

        $found_yn = "N";
        // output the actual line value text and hash
        $line_hash = sha1($line);
        if ($verbose_yn == "Y"){
            echo "<br>Actual line value from incoming data file (line #$line_num): <b>$line </b><br>";
            if ($debug_yn == "Y"){
                echo "line hash: $line_hash <br>";
				$msg = "line hash: $line_hash";
				YII_DEBUG && YII_TRACE_LEVEL >=3 && Yii::trace($msg);
            }
        }

        $i = 0;
		$dhxpy = DataHdrXlation::model()->findAll();
		// test looping through a yii result set
		foreach ($dhxpy as $rowkey => $row){
			$row_id = $row["data_hdr_xlation_id"];
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($row, 10, true);
		}

        $dhxp_row_ids = $dhxp->get__row_ids();
        $dhxp_match_found_yn = "N";
        // Loop through the header xlation rows
        foreach ($dhxp_row_ids as $row_idx => $row) {
            $dhxp_match_found_yn = "N";
            $dhxp_row_id = (int)$row["data_hdr_xlation_id"];
            $dhxp->data_hdr_xlation_id = $dhxp_row_id;
            $dhxp->get_sql_data();

            $dhxp_rgx      = $dhxp->data_hdr_regex;
            $dhxp_hash     = $dhxp->hdr_sha1_hash;
            $dhxp_trash_yn = $dhxp->data_hdr_is_trash_yn;

            // Look for a match by regex
            if (strlen($dhxp_rgx) > 0 && preg_match($dhxp_rgx, $line)){
                $dhxp_match_found_yn = "Y";
                echo "<span style='font-family:ubuntu;color:green;'>";
                echo "regex match was <b>found</b> <br>";
                echo "</span>";
            } else {
                if ($debug_yn == "Y"){
                    echo "regex match NOT FOUND <br>";
                }
            }

            // Look for a match by sha1 hash
            if (strlen($dhxp_hash) > 0 && $line_hash === $dhxp_hash){
                $dhxp_match_found_yn = "Y";
                echo "<span style='font-family:ubuntu;color:green;'>";
                echo "hash match was <b>found</b> <br>";
                echo "</span>";
            } else {
                if ($debug_yn == "Y"){
                    echo "hash match NOT FOUND <br>";
                }
            }

            if ($verbose_yn == "Y" && $dhxp_match_found_yn == "Y"){
                echo "<span style='font-family:mono;'>";
                echo "hdr xlation regex.............: $dhxp_rgx <br>";
                echo "hdr xlation hash..............: $dhxp_hash <br>";
                echo "hdr line should be removed Y/N:<b> $dhxp_trash_yn </b><br>";
                echo "</span>";
            }

            // Determine the correct header
            if ($dhxp_match_found_yn == "Y" && $dhxp_trash_yn == "N"){
                // Capture the target file name
                // Get the target column names
                // Write the target data file
                // Archive the source data file
                $target_hdr_line = self::find__hdr_target_col_names($dhxp_row_id, $line);
                if (strlen($target_hdr_line) == 0){
                    return "N";
                }
                return "YY";
            }

        }
        return $dhxp_match_found_yn;
    }

	/**
	 * use yii data models to find header matches
	 * @param $dhxp model, $line array, $line_num int
	 * @return string 'Y','N'
	 */
    public static function dhxp_get_hdr_xlation_id($dhxp, $line, $line_num){
        $verbose_yn = self::$verbose_yn;
        $debug_yn   = self::$debug_yn;

		$dhxp_row_id = 0; // initialize the return variable

        $found_yn = "N";
        // output the actual line value text and hash
        $line_hash = sha1($line);
        if ($verbose_yn == "Y"){
            echo "<br>Actual line value from incoming data file (line #$line_num): <b>$line </b><br>";
            if ($debug_yn == "Y"){
                echo "line hash: $line_hash <br>";
				$msg = "line hash: $line_hash";
				YII_DEBUG && YII_TRACE_LEVEL >=3 && Yii::trace($msg);
            }
        }

        $i = 0;
		$dhxpy = DataHdrXlation::model()->findAll();
		// test looping through a yii result set
		/*foreach ($dhxpy as $rowkey => $row){
			$row_id = $row["data_hdr_xlation_id"];
			echo "<br>Dumping row data in dhxp_get_hdr_xlation_id()<br>";
			echo "\$rowkey = $rowkey<br>";
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($row, 10, true);
		}*/

		$dhxp_match_found_yn  = "N";
		$regex_match_found_yn = "N";
		$hash_match_found_yn  = "N";
        // Loop through the header xlation rows
        foreach ($dhxpy as $row) {
            $dhxp_match_found_yn = "N";
            $dhxp_row_id = (int)$row->attributes["data_hdr_xlation_id"];

            $dhxp_rgx      = $row->attributes['data_hdr_regex'];
            $dhxp_hash     = $row->attributes['hdr_sha1_hash'];
            $dhxp_trash_yn = $row->attributes['data_hdr_is_trash_yn'];

            // Look for a match by regex
            if (strlen($dhxp_rgx) > 0 && preg_match($dhxp_rgx, $line)){
                $dhxp_match_found_yn = "Y";
				$regex_match_found_yn = "Y";
				if ($debug_yn == "Y"){
					echo "<span style='font-family:ubuntu;color:green;'>";
					echo "regex match was <b>found</b> <br>";
					echo "</span>";
				}
            } else {
                if ($debug_yn == "Y"){
                    echo "regex match NOT FOUND <br>";
                }
            }

            // Look for a match by sha1 hash
            if (strlen($dhxp_hash) > 0 && $line_hash === $dhxp_hash){
                $dhxp_match_found_yn = "Y";
				$hash_match_found_yn = "Y";
				if ($debug_yn == "Y"){
					echo "<br><span style='font-family:ubuntu;color:green;'>";
					echo "hash match was <b>found</b> <br>";
					echo "</span>";
				}
            } else {
                if ($debug_yn == "Y"){
                    echo "hash match NOT FOUND <br>";
                }
            }

            if ($verbose_yn == "Y" && $dhxp_match_found_yn == "Y"){
                echo "<span style='font-family:mono;'>";
                echo "hdr xlation regex.............: $dhxp_rgx <br>";
                echo "hdr xlation hash..............: $dhxp_hash <br>";
                echo "hdr line should be removed Y/N:<b> $dhxp_trash_yn </b><br>";
                echo "</span><br>";
            }

            // Determine the correct header
            if ($dhxp_match_found_yn == "Y" && $dhxp_trash_yn == "N"){
                // Capture the target file name
                // Get the target column names
                // Write the target data file
                // Archive the source data file
				if ($dhxp_row_id > 0){
					$target_hdr_line = self::find__hdr_target_col_names($dhxp_row_id, $line);
				} else {
					$target_hdr_line = "";
				}
                if (strlen($target_hdr_line) == 0){
                    return 0;
                }
                return (int)$dhxp_row_id;
            }

        }
        return 0;
    }

    public static function dhxp_match_found_v0_bcode($dhxp, $line, $line_num){
        $verbose_yn = self::$verbose_yn;
        $debug_yn   = self::$debug_yn;

        $found_yn = "N";
        // output the actual line value text and hash
        $line_hash = sha1($line);
        if ($verbose_yn == "Y"){
            echo "<br>Actual line value from incoming data file (line #$line_num): <b>$line </b><br>";
            if ($debug_yn == "Y"){
                echo "line hash: $line_hash <br>";
				$msg = "line hash: $line_hash";
				YII_DEBUG && YII_TRACE_LEVEL >=3 && Yii::trace($msg);
            }
        }

        $i = 0;
        $dhxp_row_ids = $dhxp->get__row_ids();
        $dhxp_match_found_yn = "N";
        // Loop through the header xlation rows
        foreach ($dhxp_row_ids as $row_idx => $row) {
            $dhxp_match_found_yn = "N";
            $dhxp_row_id = (int)$row["data_hdr_xlation_id"];
            $dhxp->data_hdr_xlation_id = $dhxp_row_id;
            $dhxp->get_sql_data();

            $dhxp_rgx      = $dhxp->data_hdr_regex;
            $dhxp_hash     = $dhxp->hdr_sha1_hash;
            $dhxp_trash_yn = $dhxp->data_hdr_is_trash_yn;

            // Look for a match by regex
            if (strlen($dhxp_rgx) > 0 && preg_match($dhxp_rgx, $line)){
                $dhxp_match_found_yn = "Y";
                echo "<span style='font-family:ubuntu;color:green;'>";
                echo "regex match was <b>found</b> <br>";
                echo "</span>";
            } else {
                if ($debug_yn == "Y"){
                    echo "regex match NOT FOUND <br>";
                }
            }

            // Look for a match by sha1 hash
            if (strlen($dhxp_hash) > 0 && $line_hash === $dhxp_hash){
                $dhxp_match_found_yn = "Y";
                echo "<span style='font-family:ubuntu;color:green;'>";
                echo "hash match was <b>found</b> <br>";
                echo "</span>";
            } else {
                if ($debug_yn == "Y"){
                    echo "hash match NOT FOUND <br>";
                }
            }

            if ($verbose_yn == "Y" && $dhxp_match_found_yn == "Y"){
                echo "<span style='font-family:mono;'>";
                echo "hdr xlation regex.............: $dhxp_rgx <br>";
                echo "hdr xlation hash..............: $dhxp_hash <br>";
                echo "hdr line should be removed Y/N:<b> $dhxp_trash_yn </b><br>";
                echo "</span>";
            }

            // Determine the correct header
            if ($dhxp_match_found_yn == "Y" && $dhxp_trash_yn == "N"){
                // Capture the target file name
                // Get the target column names
                // Write the target data file
                // Archive the source data file
                $target_hdr_line = self::find__hdr_target_col_names($dhxp_row_id, $line);
                if (strlen($target_hdr_line) == 0){
                    return "N";
                }
                return "YY";
            }

        }
        return $dhxp_match_found_yn;
    }

	/**
	 *
	 * @param type $dhxp_row_id
	 * @param type $line
	 * @return string
	 */
    public static function find__hdr_target_col_names($dhxp_row_id, $line){
		if (empty($dhxp_row_id)){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dhxp_row_id, 10, true);
			return;
		}
        // Get the detail rows
        /*$query = "select * from data_hdr_xlation_item \n"
                ." where data_hdr_xlation_id = $dhxp_row_id \n"
                ." order by ordinal_position_num ; \n";
        self::$db = new sdb();
        $rsa = self::$db->exa($query);
        */
		if (is_null(self::$maps) || isset(self::$maps[$dhxp_row_id]) === false ){
			$criteria = new CDbCriteria();
			$criteria->condition = 'data_hdr_xlation_id=:data_hdr_xlation_id';
			$criteria->order     = 'ordinal_position_num';
			$criteria->params    = array(':data_hdr_xlation_id'=>$dhxp_row_id);
			$rsa = DataHdrXlationItem::model()->findAll($criteria);
			self::$maps[$dhxp_row_id] = $rsa;
		} else {
			$rsa = self::$maps[$dhxp_row_id];
		}

		//echo"<br>Dumping \$rsa in bfilehead::find__hdr_target_col_names()";
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rsa, 10, true);

        // If the line contains a trailing comma, then remove the trailing comma
        //$line_len = strlen($line);
        $line_end = substr(trim($line), -1);
        if ($line_end == ","){
            $line = substr(trim($line), 0, -1);
        }

        // Trap column header exceptions for error reporting

        $source_column_header_exceptions = array();
        $header_conversions = array();
        // validate the header items
        $line_items = explode(",", $line);
        $source_line_vals_match_expected_norms_yn = "Y";
        $target_hdr_line = "";
        foreach ($line_items as $idx => $source_column_hdr) {
            if (strlen($source_column_hdr) == 0){
                // don't process and empty header item like a trailing comma that creates an empty hdr item
                continue;
            }

            //$data_hdr_xlation_item_source_value = $rsa[$idx]["data_hdr_xlation_item_source_value"];
			$data_hdr_xlation_item_source_value = $rsa[$idx]->attributes["data_hdr_xlation_item_source_value"];
            if ($source_column_hdr == $data_hdr_xlation_item_source_value){
                // The line matches the expect values
                //$target_hdr_line .= $rsa[$idx]["data_hdr_xlation_item_target_value"];
				$target_hdr_line .= $rsa[$idx]->attributes["data_hdr_xlation_item_target_value"];
                $target_hdr_line .= ",";
                self::$header_conversions[] = array("source"=>$source_column_hdr, "target"=>$rsa[$idx]["data_hdr_xlation_item_target_value"]);

            } else {
                // The line DOES NOT matche the expected values
                $source_line_vals_match_expected_norms_yn = "N";
                $source_column_header_exceptions[$dhxp_row_id] = $source_column_hdr;
            }

			// Create an exception for the STI Coach Report since the format of it is so whacked out
			if ($dhxp_row_id == 8 && $idx == 2){
				return $target_hdr_line;
			}
			// Create an exception for the STI Coach Report Mod since the format of it is so whacked out
			if ($dhxp_row_id == 9 && $idx == 2){
				return $target_hdr_line;
			}
			// Create an exception for the STI Coach Report since the format of it is so whacked out
			if ($dhxp_row_id == 10 && $idx == 2){
				return $target_hdr_line;
			}
        }

        if ($source_line_vals_match_expected_norms_yn == "Y"){
            // perform file xlation
            // Remove the last comma from the target header line
            $target_hdr_line = substr($target_hdr_line, 0, -1);
            //self::$target_file_name = self::$dhxp->xxlrater_target_table_name . ".csv";
			self::$target_file_name = DataHdrXlation::model()->findByPk($dhxp_row_id)->attributes['gsm_target_table_name'] . ".csv";
			if (self::$debug_yn == 'Y'){
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump(self::$target_file_name, 10, true);
			}
            //self::$target_file_uri  = self::$target_path . self::$target_file_name;
			self::$target_file_uri  = str_ireplace('/protected', '', Yii::app()->basePath) .'/'.self::$target_path . self::$target_file_name;
            self::$target_hdr_line  = $target_hdr_line;
//			if (stripos(self::$target_file_uri, 'multiple tables.csv') !== false){
//				$msg = "skipping the writing of 'multiple tables.csv'";
//				YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
//			} else {
//				// file OK to write
//				Bfile::write_to_file(self::$target_file_uri, $target_hdr_line . "\n", "append");
//
//			}
			Bfile::write_to_file(self::$target_file_uri, $target_hdr_line . "\n", "append");
        } else {
            $target_hdr_line = "";
            if (sizeof($source_column_header_exceptions) > 0){
                echo "<span style='font-family:ubuntu;color:blue;'><b>Source Column Header Exceptions</b></span><br>";
                echo_array($source_column_header_exceptions);
            }
        }
        return $target_hdr_line;
    }

    public static function insert__new_hdr_definition($file_lines) {
        // insert row in data_hdr_xlation
        //   create regex?
        //   create hash
        // insert row in data_hdr_xlation_item
        //   pull headers from file?
        //   insert column names into item table
    }

    public static function archive_source_file($source_file_name){
        // Development Status = Ready for testing - doesn't see the source file :(
        // Purpose: Move a source file to an archive folder
        $readonly_yn = self::$readonly_yn;
        if ($readonly_yn == "Y"){
            return 0;
        }
        $source_path = self::$source__folder_uri;
        $target_path = self::$source__archive_target_path;

        $source_uri = add_trailing_slash(DOCROOT)
                . add_trailing_slash($source_path)
                . $source_file_name;
        $target_uri = add_trailing_slash(DOCROOT)
                . add_trailing_slash($target_path)
                . $source_file_name;

        $source_uri = $source_path . $source_file_name;
        $target_uri = $target_path . $source_file_name;

        $deleted = -1;
        if(file_exists($source_uri)){
            file::copy_file_or_folder($source_uri, $target_uri);
            if(file_exists($target_uri)){
                // file copied successfully
                $deleted = file::file_delete($source_uri, "Y");
            }
        }
        if ($deleted == 1){
            // The file was deleted sucessfully
        } else {
            // The file deletion failed
        }

    }



}
// </editor-fold>





