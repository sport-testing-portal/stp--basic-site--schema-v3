<?php

/*
 * Byrdcode Data Source File
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *  Original bc call path
 *    file.csv.to.json.class.php
 *      $dsf = new data_source_file();
 *        // dsf contains methods: if_data_source_files_exist_yn(), archive_source_file()
 *      // the csv_to_table_array class is also defined in this file. (new name bcsv2arr)
 *
 *
 *   Sample calls:
 *     bdsf::importCsv($csv_file_name)
 *     bdsf::archive_source_file($file_name);
 *     bdsf::archive_source_file($file_uri);
 */

// Original source bc/file.csv.to.json.class.php which contained a class called data_source__file()
class bdsf extends CApplicationComponent
{

    // Scenarios:
    //   if a csv file is found convert it to a json file and exit?
    //   if a csv file is found convert it to a json, and
    //     * call the file importer with the use local file setting?
	// @todo: fetch file path from application parms array
    public static $readonly_yn	= "Y";
	public static $verbose_yn	= "N";
	public static $debug_yn		= "N";

    public static $data_source_path  = "epf_inbox/data_sources__to_process";
    public static $data_archive_path = "epf_inbox/data_sources__archive";

    public static $data_source_path__absolute  = "";
    public static $data_archive_path__absolute = "";

    public static $source_uri_jede = "";
    public static $source_uri_json = "";
    public static $source_uri_csv  = "";

    //public $target_table

	public function init() {
	}


	/**
	 * import a single csv file?
	 * @param string $csv_file_name
	 * @return string
	 */
	public static function importCsv($csv_file_name=NULL) {
		// @todo: insert code from bc/tools/import.csv.file.php
		$import_success = -1;
		$import_successful_yn = 'N';

		$data_files_exist_yn = self::data_source_files_exist_yn();

		if (self::data_source_files_exist_yn() == 'Y'){
			// Verfiy! Are we using the correct function call here? Yes!!
			$data_array = bcsv2arr::convert_to_array($csv_file_name);

			$data       = $bcsv2arr::convert_data_array_to_jpa($data_array); // returns a json process array

			$csv_file_basename = bfile::base_name($bdsf::source_uri_csv);

		}
		if($import_successful_yn == 'Y') {
			self::archive_source_file($csv_file_basename);
		}
		return $import_successful_yn;
	}



// <editor-fold defaultstate="collapsed" desc="configuration class">

// <editor-fold defaultstate="collapsed" desc="data source - file class">
//class data_source__file{


    function __construct($source_path=NULL, $archive_path=NULL, $target_table=NULL) {
        $doc_root = $_SERVER['DOCUMENT_ROOT'];
        if (!empty($source_path)){
            $this->data_source_path__absolute = $source_path;
        } else {
            $this->data_source_path__absolute  = file::add_trailing_slash($doc_root)
                    . file::add_trailing_slash($this->data_source_path);
        }

        if (!empty($archive_path)){
            $this->data_archive_path__absolute = $archive_path;
        } else {
            $this->data_archive_path__absolute = file::add_trailing_slash($doc_root)
                . file::add_trailing_slash($this->data_archive_path);
        }
        if ($this->data_source_files_exist_yn() == "Y"){
            return true;
        } else {
            return false;
        }

    }

	/**
	 * determines if files are awaiting import into the database
	 * @param null
	 * @return string
	 */
    public static function data_source_files_exist_yn(){
        // glob the data sources path
        $source_path = $this->data_source_path__absolute;

        $file_list = file::glob_files($source_path, "csv");
        $file_cnt  = 0;
        foreach($file_list as $file_idx=>$file){
            $file_cnt++;
            $source_uri_csv      = $source_path . trim($file["basename"]) .".csv";
            $source_uri_json     = $source_path . trim($file["basename"]) .".json";
            $source_uri_jede     = $source_path . trim($file["basename"]) .".jede";
            // $csv_file_exists_yn  = "Y"; // this is a given but listed here to be explict.
            $csv_file_exists_yn  = ( (file_exists($source_uri_csv) ?"Y":"N" ));
            $json_file_exists_yn = ( (file_exists($source_uri_json)?"Y":"N" ));
            $jede_file_exists_yn = ( (file_exists($source_uri_jede)?"Y":"N" ));

            // @todo: if a json or jede file exists then archive the csv file immediatey
            if ($jede_file_exists_yn == "Y" && $json_file_exists_yn == "Y" && $csv_file_exists_yn == "Y"){
                $this->source_uri_jede = $source_uri_jede;
                $this->archive_source_file($source_uri_json);  // archive the json file
                $this->archive_source_file($source_uri_csv);  // archive the csv file

            }elseif ($jede_file_exists_yn == "N" && $json_file_exists_yn == "Y" && $csv_file_exists_yn == "Y"){
                $this->source_uri_json = $source_uri_json;
                $this->archive_source_file($source_uri_csv); // archive the csv file

            }elseif ($jede_file_exists_yn == "N" && $json_file_exists_yn == "N" && $csv_file_exists_yn == "Y"){
                $this->source_uri_csv = $source_uri_csv;
                //$this->archive_source_file($source_uri_csv); // archive the csv file, keep the json file
                return "Y";
            }elseif ($jede_file_exists_yn == "N" && $json_file_exists_yn == "N" && $csv_file_exists_yn == "N"){
                return "N";
            }

        }
    }

    public static function archive_source_file($source_file_name){
        // Development Status = Ready for testing
        // Purpose: Move a source file to an archive folder
		// Notes: see also the CFile extension
        $readonly_yn = $this->readonly_yn;
        if ($readonly_yn == "Y"){
            return 0;
        }
        $source_path = $this->thumbnail_source__folder_uri;
        $target_path = $this->thumbnail_source__archive_target_path;

        $source_uri = bfile::add_trailing_slash(DOCROOT)
                . bfile::add_trailing_slash($source_path)
                . $source_file_name;
        $target_uri = bfile::add_trailing_slash(DOCROOT)
                . bfile::add_trailing_slash($target_path)
                . $source_file_name;

        if(!file_exists($source_uri)){
            bfile::copy_file_or_folder($source_uri, $target_uri);
            if(file_exists($target_uri)){
                // file copied successfully
                $deleted = bfile::file_delete($source_uri, "Y");
            }
        }
    }

	/**
	 * @param int $data_hdr_xlation_id
	 * @return array[CActiveRecord]
	 */
	public static function fetch__xlationItemsAR($data_hdr_xlation_id){

		if ( $data_hdr_xlation_id > 0 && $data_hdr_xlation_id < 10000){
//			$ruleBase=['id'=>'','type'=>'params','sub-type'=>'range','scope'=>0,'asserts'=>[]];
//			$rules = [
//				'params'=>[
//					'range'=>[
//						'parameter inside an allowable range of integer 1-10000' =>[
//							'asserts'=>[
//								'$data_hdr_xlation_id'=>$data_hdr_xlation_id,
//							]
//						]
//					]
//				]
//			];
			$okToContinue=true;

		} else {
			$msg = __METHOD__ . ' parameters outside of an allowable range of integer 1-10000';
			throw new CException($msg);
		}
		$dhx = DataHdrXlation::model()->findByPk($data_hdr_xlation_id);
		if (is_null($dhx)){
			return null;
		} else {
			if (self::$debug_yn == 'Y'){
				$msg = "\$dhx is not null";
				YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dhx, 10, true);
			}
		}
		$dhxp = $dhx->attributes;

		//echo "<br>Dumping \$dhxp in ". __METHOD__ ."<br>";
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dhxp, 10, true);

		//$dhxd = DataHdrXlation::model()->with('dataHdrXlationItems')->findAllByPk((int)$data_hdr_xlation_id);
		$criteria = new CDbCriteria();
		$criteria->condition = 't.data_hdr_xlation_id=:data_hdr_xlation_id';
		$criteria->order     = 'ordinal_position_num';
		$criteria->params    = array(':data_hdr_xlation_id'=>(int)$data_hdr_xlation_id);
		//$rsa = DataHdrXlationItem::model()->findAll($criteria);

		$dhxd = DataHdrXlation::model()->with('dataHdrXlationItems')->findAll($criteria);

		// YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dhxd, 10, true); // display the entire AR object
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dhxd[0]->attributes, 10, true); // Just show the attributes
		//echo "<br>Dump of \$dhxd is complete in " . __METHOD__ . "<br>";

		if (!is_null($dhxd)){

		}
		return $dhxd;
	}

	/**
	 * Get a list of target tables for the xlation by flattening the relational target item data
	 * @param int $data_hdr_xlation_id
	 * @return array
	 * @internal calls: fetch__xlationItemsAR();
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function fetch__xlatTargetTables($data_hdr_xlation_id){

		// get the outgoing file name
		// Test accessing all array values
		$dhxd = self::fetch__xlationItemsAR($data_hdr_xlation_id);

		$target_tables = array();
		foreach($dhxd[0]->getRelated('dataHdrXlationItems') as $row){
			//echo "<br>Dump of a \$dhxd child object \$row->attributes in " . __METHOD__ ."<br>";
			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($row->attributes, 10, true);

			$targetTableNamePotentiate	= trim($row->attributes['data_hdr_xlation_item_target_table']);
			$potentiateAlreadyInList	= array_search($targetTableNamePotentiate, $target_tables, true);

			if (array_search(trim($row->attributes['data_hdr_xlation_item_target_table']), $target_tables, true) === false
					&& ! empty($row->attributes['data_hdr_xlation_item_target_table'])){
				//$target_tables[] = trim($row->attributes['data_hdr_xlation_item_target_table']);
				$targetTableName = trim($row->attributes['data_hdr_xlation_item_target_table']);
				$target_tables[] = $targetTableName;
			}
		}
		//unset($targetTableName);
		//echo "<br><br>Dump of \$target_tables from DHXI actionImportSpecialReport<br>";
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($target_tables, 10, true);

		return $target_tables;
	}

}
// </editor-fold>


