<?php


/** Notes...:
 * @example bfile::csv_to_assoc_arr('filename');
 * @example bfile::get__import_path()
 * @example bfile::archive_import_file($file_name);
 */

//class file{
class bfile extends CApplicationComponent
{

	// <editor-fold defaultstate="collapsed" desc="file class">
	public function init() {
	}


	/**
	 * Convert a csv file to an associative array
	 * @param string $csv_file_uri
	 * @return string
	 */
    public static function csv_to_assoc_arr($csv_file_uri){
        // Base code from: http://steindom.com/articles/shortest-php-code-convert-csv-associative-array
        // Dbyrd: remember to test for trimming since file() includes the line return within value array
        $rows = array_map('str_getcsv', file($csv_file_uri));
        $header = array_shift($rows);
        $csv = array();
        foreach ($rows as $row) {
          $csv[] = array_combine($header, $row);
        }
		return $csv;
    }

	/**
	 * get name of the file currenting being executed
	 * @param string $exp
	 * @return string
	 */
    public static function file_name() {
        $bt   = debug_backtrace();
        $file_name = basename($bt[0]['file']);
        return $file_name;
    }

	/**
	 * returns file name and extension from a URI
	 * @param string $file_uri
	 * @return string
	 */
    public static function base_name($file_uri){
        // return myfile.php from /somefolder/myfile.php
        return pathinfo($file_uri, PATHINFO_BASENAME);
    }

	/**
	 * Get an actionable real file path to data file resources on the server
	 *
	 * @param string $path_type 'base_folder','to_process','archive','purged'
	 * @return string real file path to a data file folder
	 * @internal original source is golden and rock solid. This copy needs testing as a component
	 * @internal original source: protected/controllers/DataController.php
	 * @internal This function has been copied to the to the Bfile component: protected/components/functions/Bfile.php
	 */
	protected static function get__import_path($path_type=NULL)
	{
		if (empty($path_type)){
			$path_type = 'base_folder';
		}

		$base_path = Yii::app()->params['data_input__by_file__path']['real_path_base_folder'];
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($base_path, 10, true);
		// replace relative paths with real paths
		$paths = Yii::app()->params['data_input__by_file__path'];
		foreach ($paths as $idx=>$path){
			//$paths[$idx] = $path_from_alias . $path;
			if ($idx == 'real_path_base_folder'){
				continue;
			}
			$paths[$idx] = $base_path . $path;
		}

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($paths, 10, true);
		return $paths[$path_type];
	}

	/**
	 * Move a file from one folder to another.
	 * @param string $file_name
	 * @return boolean Returns true if archived successfully, otherwise false
	 * @internal original source is golden and rock solid. This copy needs testing as a component
	 * @todo add this function to an application component so it can be accessed by any controller or model
	 * @internal original source: /var/www/sites/gsm/dev/gsm-public-site/protected/controllers/DataController.php
	 * @internal This function has been copied to the to the Bfile component: protected/components/functions/Bfile.php
	 */
	protected static function archive_import_file($file_name)
	{
		//@todo: acquire from config
		$add_datetime_suffix_during_file_archival_yn = "Y";
		//$add_datetime_suffix_during_file_archival_yn = "N";

		$date_suffix = "";
		if ($add_datetime_suffix_during_file_archival_yn === 'Y'){
			$date_suffix = Bfile::date_to_filename();
			$file_name_original = $file_name;
			$target_file_name = pathinfo($file_name, PATHINFO_FILENAME)
					. '__' . $date_suffix
					. '.'  . pathinfo($file_name, PATHINFO_EXTENSION);
		} else {
			$target_file_name = $file_name;
		}

		// If URI passed in then use it
		$path_passed_in = pathinfo($file_name, PATHINFO_DIRNAME);
		if (strlen($path_passed_in) >0){
			$source_path = Bfile::add_trailing_slash($path_passed_in);
			$file_name_original = $file_name;
			$file_name = pathinfo($file_name, PATHINFO_BASENAME);
			$msg = "File URI passed to ($file_name) " . __METHOD__;
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
		} else {
			$source_path  = self::get__import_path('to_process');
		}

		$archive_path = self::get__import_path('archive');
		YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace("source path: $source_path");
		YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace("archive path: $archive_path");
		$cfile = Yii::app()->file->set($source_path . $file_name, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cfile, 10, true);
		// @todo: document why the file is moved twice
		$moved = $cfile->move($archive_path . $file_name);
		$moved = $cfile->move($archive_path . $target_file_name);
		//CVarDumper::dump($cfile->move($source_path . $archive_path);
		if ($moved){
			$msg = "import file: $file_name archived in $archive_path";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			return true;
		} else {
			$msg = "import file: $file_name was NOT archived in $archive_path";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			return false;
		}
	}




    /**
    * Remove the directory and its content (all files and subdirectories).
	*   ReMove Recusive Force
    * @param string $dir the directory name
    */
    public static function rmrf($dir) {
        foreach (glob($dir) as $file) {
            if (is_dir($file)) {
                rmrf("$file/*");
                rmdir($file);
            } else {
                unlink($file);
            }
        }
    }

	/**
	 * List the contents of a directory
	 * @param string $start
	 * @return string
	 */
    public static function listdirs_safe($start){
        $dir  = $start;
        $dirs = array();
        $next = 0;

        while (true)
        {
            $_dirs = glob($dir.'/*', GLOB_ONLYDIR|GLOB_NOSORT);

            if (count($_dirs) > 0)
            {
                foreach ($_dirs as $key => $_dir)
                    $dirs[] = $_dir;
            }
            else
                break;

            $dir = $dirs[$next++];
        }

        return $dirs;
    }

	/**
	 * Builds a list of files using PCRE Regex pattern matching
	 * @param string $folder
	 * @param string $pattern '/^.+\.php$/i'
	 * @return array
	 */
    public static function glob_recursive($folder, $pattern) {
        // @todo: return a file list, or a file property list?
        // a pattern to collect all php files in a folder tree: '/^.+\.php$/i
        try{
            $dir = new RecursiveDirectoryIterator($folder);
            $ite = new RecursiveIteratorIterator($dir);
            $files = new RegexIterator($ite, $pattern, RegexIterator::GET_MATCH);
            $fileList = array();
            foreach($files as $idx => $file) {
                // Is $files a fileInfo object?
                //$str_fragment = substr($file[0], -1);
                if (substr($file[0],-1) == '.'){
                    // remove folders . and ..
                    continue;
                }
                $fileList = array_merge($fileList, $file);
            }

            return $fileList;
        } catch(Exception $exc){
           echo $exc->getMessage();
        }
    }

	/**
	 * formats a date into an acceptable file name
	 * @param date $date
	 * @return string
	 */
    public static function date_to_filename($date=NULL) {
        // Returns the current datetime as a valid file name
        $dateFormat = 'Y-m-d_His';
        if ( empty($date)){
            $timeStamp = time();
        } else {
            $timeStamp = strtotime($date);
        }
        $dateTime= date($dateFormat,$timeStamp);
        return $dateTime;
    }

	/**
	 * Alias for add_trailing_slash()
	 * @param string $str
	 * @return string
	 * @uses add_trailing_slash()
	 */
    public static function addSlash($str){
        return self::add_trailing_slash($str);
    }

	/**
	 * Add a trailing slash to a path if a slash isn't already present
	 * @param string $str
	 * @return string
	 */
    public static function add_trailing_slash($str){
        $str .= (substr($str, -1) == '/' ? '' : '/');
        return $str;
    }

	/**
	 * remove a trailing backslash from a file path
	 * @param string $str
	 * @return string
	 */
    public static function remove_trailing_slash($str){
        if (substr($str, -1) == '/'){
			$str = substr($str, 0, -1);
		}
        return $str;
    }

	/**
	 * Delete a file.
	 * @param string $exp
	 * @return string
	 * @see CFile extension for yii object oriented file handling.
	 */
    public static function file_delete($file_uri, $return_int_yn="N"){

        $parm1_tag   = "(*$file_uri*)";
        $parm2_tag   = "(*$return_int_yn*)";
        $failure_tag = "File deletion skipped.";

        $scenario    = array();  // Scenarios and return values matrix: scenario[index,array(integer, string)].
        $scenario[1] = array(-1, "file_uri is a required parameter. The parameter value was empty $parm1_tag! $failure_tag");  // blank parameter passed
        $scenario[2] = array(-1, "The value passed for return_int_yn is invalid $parm2_tag. $failure_tag");                    // bogus parameter passed
        $scenario[3] = array( 0, "The file was not found $parm1_tag. $failure_tag");                                           // bogus parameter passed
        $scenario[4] = array( 0, "The file deletion failed $parm1_tag. $failure_tag");                                         // intended operation failed
        $scenario[5] = array( 1, "File deleted successully $parm1_tag.");                                                      // intended operation succeed

        // Assert the parameters passed in.
        if ( empty($file_uri) ){
            error_Handler(__METHOD__, __LINE__,              $scenario[1][1]);
            return ($return_int_yn=="Y") ? $scenario[1][0] : $scenario[1][1];   // bad parm passed
        }
        if ( empty($return_int_yn) ){
            $return_int_yn = "N";                                               // set default value

        } elseif (strtoupper($return_int_yn) != "Y" && strtoupper($return_int_yn) != "N"){
            error_Handler(__METHOD__, __LINE__,              $scenario[2][1]);
            return ($return_int_yn=="Y") ? $scenario[2][0] : $scenario[2][1];   // bad parm passed
        }

        // Assert that the file exists
        if ( file_exists($file_uri) ) {
            unlink($file_uri);                                                  // delete file
        } else {
            return ($return_int_yn=="Y") ? $scenario[3][0] : $scenario[3][1];   // file not found
        }

        // Assert that the file was deleted.
        if ( file_exists($file_uri) ) {
            return ($return_int_yn=="Y") ? $scenario[4][0] : $scenario[4][1];   // delete failed
        } else {
            return ($return_int_yn=="Y") ? $scenario[5][0] : $scenario[5][1];   // delete succeeded!
        }

    }

	/**
	 * Create a folder on the server
	 * @param  string $uri The path of the folder to create
	 * @return string $createfolder_result
	 */
    public static function create_folder($uri, $verbose_yn=null){
        // development status="Golden"
        if (empty($verbose_yn)){$verbose_yn = "N";}
        if (strlen($uri) == 0) {
            return "Error: CreateFolder() was passed an empty URI ($uri).";
        }
        if (! is_dir($uri)) {
            $rs = mkdir($uri, 0777, true);
            if (! $rs) {
                // print error information
                echo "An error was occurred. Attempting create folder ($uri). <br />";
                //echo '<br>dirPath: ' . $uri;
                echo 'php_errormsg: ' . $php_errormsg."<br />";
            }
            else {
                //chown($uri, "dbyrd");
                //chgrp($uri, "www-data");
                // recurse_chown_chgrp($uri, "dbyrd", "www-data");
                $createfolder_result = "CreateFolder() created: <b>$uri</b>";
                if ($verbose_yn == "Y") {
                    echo "\$createfolder_result = $createfolder_result <br />";
                }
            }

        }
        else {
            $createfolder_result = "Folder already exists ($uri).";
            if ($verbose_yn == "Y") {
                echo "\$createfolder_result = $createfolder_result <br />";
            }
        }
        return $createfolder_result;
    }

	/**
	 * A simple array of file names in a folder
	 * @param string $uri
	 * @return string
	 */
    public static function listFilesInFolder($uri) {
        // Development status: needs testing *
        try {
            $dir_handle = opendir($uri);
            if($uri) {
                while ($file_name = readdir($dir_handle)) {
                    if ($file_name!='.' && $file_name!='..' && $file_name!='.htaccess') {
                        //chmod($temp,777);
                        echo "<a href='$uri.$file_name'>$file_name</a><br/>";
                        echo "\$file_name = $file_name <br />";
                    }
                }
            }
            closedir($dir_handle);

        } catch (Exception $exc) {
            echo $exc->getTraceAsString(). "<br />";
        }
    }

	/**
	 * A simple array of file names in a folder
	 * @param string $uri
	 * @return string
	 */
    public static function list_files_in_folder($uri) {
        // Development status: needs testing *
        try {
            $dir_handle = opendir($uri);
            if($uri) {
                while ($file_name = readdir($dir_handle)) {
                    if ($file_name!='.' && $file_name!='..' && $file_name!='.htaccess') {
                        //chmod($temp,777);
                        echo "<a href='$uri.$file_name'>$file_name</a><br/>";
                        echo "\$file_name = $file_name <br />";
                    }
                }
            }
            closedir($dir_handle);

        } catch (Exception $exc) {
            echo $exc->getTraceAsString(). "<br />";
        }
    }

	/**
	 * Generate a unique and unused file name on the server
	 * @param string $parentFolder The folder that will contain the new folder
	 * @param string $useShortNameYN
	 * @return string
	 */
    public static function getFolderName__UniqueUnUsed($parentFolder, $useShortNameYN = "Y"){
        $attempt_cnt = 0;
        while ($attempt_cnt <= 100) {
            $attempt_cnt++;
            $unique_string = sha1(date("Y-m-d H:i:s.u",time()));
            if ($useShortNameYN  == "Y"){
                $qrtr_length = strlen($unique_string) / 4;
                $folder_name_proposed = substr($unique_string, 1, $qrtr_length);
            }
            $folder_uri_proposed = $parentFolder . $folder_name_proposed;
            if (is_dir($folder_uri_proposed) !== true){
                return $folder_name_proposed;
            }
        }
        // Increase the unique string length to half the original length
        if ($attempt_cnt == 101){
            $attempt_cnt++;
            $unique_string = sha1(date("Y-m-d H:i:s.u",time()));
            if ($useShortNameYN  == "Y"){
                $half_length = strlen($unique_string) / 2;
                $folder_name_proposed = substr($unique_string, 1, $half_length);
            }
            $folder_uri_proposed = $parentFolder . $folder_name_proposed;
            if (is_dir($folder_uri_proposed) !== true){
                return $folder_name_proposed;
            }
        }
    }

	/**
	 * Get an array of files that includes meta-data about the files including the sha256 for deduping.
	 * @param string $sourceFolder     The location of your files
	 * @param string $ext              File extension you want to limit to (i.e.: *.txt)
	 * @param int    $epocSecs         If you only want files that are at least so old.
	 * @param int    $maxFileCnt       Number of files you want to return
	 * @param bool   $ignoreSubFolders Ignore sub folders
	 * @return array[]
	 * @internal return structure ['basename','ext','filename','size','updated_dt','created_dt','accessed_dt','sha256','file_uri']
	 * @author   alan@synergymx.com, debugged and rewritten by Dana Byrd <danabyrd@byrdbrain.com>
	 * @internal source = http://php.net/manual/en/control-structures.break.php
	 * @internal Development Status = Golden!
	 */
    public static function globFiles($sourceFolder, $ext=null, $epocSecs=null, $maxFileCnt=null, $ignoreSubFolders=false){
        // dana: php glob() appears to prefer, ie only works with a physical file path on some websites.
        //   so I've added a reference to SERVER doc root and also added the automatic
        //   appendage of a trailing slash where needed as glob also seems to require the trailing
        //   slash, but this is unconfirmed as yet.
        // Sample output
        //  [path] => c:\temp\my_videos\
        //  [name] => fluffy_bunnies.flv
        //  [size] => 21160480
        //  [date] => 2007-10-30 16:48:05

        if( !is_dir( $sourceFolder ) ) {
            $msg =  "Invalid directory! (*$sourceFolder*)\n\n";
            return $msg;
        }
        // add a trailing slash
		if (! stripos($sourceFolder, '*') !== false){
			$globPath    = self::add_trailing_slash($sourceFolder)."*"; // append '/*' to the path
		} else {
			// a wild card file skeleton is already included in the path. Use the path as is.
			$globPath    = $sourceFolder;
		}

        $filesList    = glob($globPath, GLOB_MARK); // glob_mark adds trailing slash to the folders returned
        $set_limit    = 0;
		$out		  = [];

        $hash_enabled_yn = 'Y';
        $targetFileUri = "";
        foreach($filesList as $key => $fileUri) {

            if( !is_null($maxFileCnt) && $set_limit == $maxFileCnt ){
				// maximum file count has been reached
				break;
			}
            if( $ignoreSubFolders && substr($fileUri, -1) == '/'){
				// a trailing slash indicates a sub folder
				continue;
			}

            if(is_null($epocSecs) || filemtime( $fileUri ) > $epocSecs ){
                $targetFileUri = $fileUri;
                if ($hash_enabled_yn == "Y"){
                    $hash256 = hash_file('sha256', $targetFileUri);
                }
                $path_parts = pathinfo( $fileUri );
                if ( !empty($ext) && $path_parts['extension'] !== $ext){
                    continue;
                }

                $out[$key]['path']          = $path_parts['dirname'];
                $out[$key]['basename']      = $path_parts['basename'];  // includes extension
                if (isset($path_parts['extension'])){
                    $out[$key]['ext']           = $path_parts['extension'];
                }
                $out[$key]['filename']      = $path_parts['filename'];  // sans extension
                $out[$key]['size']          = filesize( $fileUri );
                $out[$key]['updated_dt']    = date('Y-m-d G:i:s', filemtime( $fileUri ) );
                $out[$key]['created_dt']    = date('Y-m-d G:i:s', filectime( $fileUri ) );
                $out[$key]['accessed_dt']   = date('Y-m-d G:i:s', fileatime( $fileUri ) );
                $out[$key]['sha256']        = $hash256;
                $out[$key]['file_uri']      = $fileUri;

                $set_limit++;
            }
        }
        if(!empty($out)){
            return $out;
        } else {
            // die( "No files found!\n\n" );
            $msg =  "No files found! (*$sourceFolder*)\n\n";
        }
    }

	/**
	 * Get a list of files and their properties from a folder
	 * @param string $source_folder
	 * @param string $ext The file extensions to include
	 * @param int $sec Unix epoc seconds
	 * @param int $limit The max number of files to return
	 * @return string
	 */
    public static function glob_files_v0($source_folder, $ext, $sec, $limit){
        // $source_folder // the location of your files
        // $ext           // file extension you want to limit to (i.e.: *.txt)
        // $sec           // if you only want files that are at least so old.
        // $limit         // number of files you want to return
        // author = alan at synergymx dot com
        // source = http://php.net/manual/en/control-structures.break.php

        // Sample output
        //  [path] => c:\temp\my_videos\
        //  [name] => fluffy_bunnies.flv
        //  [size] => 21160480
        //  [date] => 2007-10-30 16:48:05

        if( !is_dir( $source_folder ) ) {
            //die ( "Invalid directory.\n\n" );
            $msg =  "Invalid directory! (*$source_folder*)\n\n";
        }

        $glob_path  = $source_folder."\*.".$ext;
        $glob_path2 = $source_folder."*.".$ext;
        $glob_path3 = $source_folder."*";
        //$FILES = glob($source_folder."\*.".$ext);
        $FILES  = glob($glob_path);
        $FILES2 = glob($glob_path2);
        $FILES3 = glob($glob_path3);
        $set_limit    = 0;

        foreach($FILES as $key => $file) {

            if( $set_limit == $limit ){
				break;
			}

            if( filemtime( $file ) > $sec ){
                $FILE_LIST[$key]['path']    = substr( $file, 0, ( strrpos( $file, "\\" ) +1 ) );
                $FILE_LIST[$key]['name']    = substr( $file, ( strrpos( $file, "\\" ) +1 ) );
                $FILE_LIST[$key]['size']    = filesize( $file );
                $FILE_LIST[$key]['date']    = date('Y-m-d G:i:s', filemtime( $file ) );
                $set_limit++;
            }
        }
        if(!empty($FILE_LIST)){
            return $FILE_LIST;
        } else {
            // die( "No files found!\n\n" );
            $msg =  "No files found! (*$source_folder*)\n\n";
        }
    }

	/**
	 * Copy a file or folder on the server
	 * @param string $source  The source folder or file
	 * @param string $dest    The target folder or file
	 * @param array  $options Folder and file permissions using Unix like ordinal permissions
	 * @return string
	 */
    public static function copy_file_or_folder($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0755)) {

        // originally named smartCopy() by Sina Salek (author),
        // renamed by Dana Byrd for ease of use with intellisense.
        // From: http://www.php.net/manual/en/function.copy.php
        // Author: Sina Salek http://sina.salek.ws/en/contact
        //
        // Copy file or folder from source to destination, it can do
        // recursive copy as well and is very smart
        // It recursively creates the dest file or directory path if there weren't exists
        // Situtaions :
        // - Src:/home/test/file.txt ,Dst:/home/test/b ,Result:/home/test/b -> If source was file copy file.txt name with b as name to destination
        // - Src:/home/test/file.txt ,Dst:/home/test/b/ ,Result:/home/test/b/file.txt -> If source was file Creates b directory if does not exsits and copy file.txt into it
        // - Src:/home/test ,Dst:/home/ ,Result:/home/test/** -> If source was directory copy test directory and all of its content into dest
        // - Src:/home/test/ ,Dst:/home/ ,Result:/home/**-> if source was direcotry copy its content to dest
        // - Src:/home/test ,Dst:/home/test2 ,Result:/home/test2/** -> if source was directoy copy it and its content to dest with test2 as name
        // - Src:/home/test/ ,Dst:/home/test2 ,Result:->/home/test2/** if source was directoy copy it and its content to dest with test2 as name
        // @todo
        //     - Should have rollback technique so it can undo the copy when it wasn't successful
        // - Auto destination technique should be possible to turn off
        //  - Supporting callback function
        //  - May prevent some issues on shared enviroments : http://us3.php.net/umask
        // @param $source //file or folder
        // @param $dest ///file or folder
        // @param $options //folderPermission,filePermission
        // @return boolean
        //
        $result=false;

        if (is_file($source)) {
            if ($dest[strlen($dest)-1]=='/') {
                if (!file_exists($dest)) {
                    cmfcDirectory::makeAll($dest,$options['folderPermission'],true);
                }
                $__dest=$dest."/".basename($source);
            } else {
                $__dest=$dest;
            }
            $result=copy($source, $__dest);
            chmod($__dest,$options['filePermission']);

        } elseif(is_dir($source)) {
            if ($dest[strlen($dest)-1]=='/') {
                if ($source[strlen($source)-1]=='/') {
                    //Copy only contents
                } else {
                    //Change parent itself and its contents
                    $dest=$dest.basename($source);
                    @mkdir($dest);
                    chmod($dest,$options['filePermission']);
                }
            } else {
                if ($source[strlen($source)-1]=='/') {
                    //Copy parent directory with new name and all its content
                    @mkdir($dest,$options['folderPermission']);
                    chmod($dest,$options['filePermission']);
                } else {
                    //Copy parent directory with new name and all its content
                    @mkdir($dest,$options['folderPermission']);
                    chmod($dest,$options['filePermission']);
                }
            }

            $dirHandle=opendir($source);
            while($file=readdir($dirHandle))
            {
                if($file!="." && $file!="..")
                {
                    if(!is_dir($source."/".$file)) {
                        $__dest=$dest."/".$file;
                    } else {
                        $__dest=$dest."/".$file;
                    }
                    //echo "$source/$file ||| $__dest<br />";
                    $result=smartCopy($source."/".$file, $__dest, $options);
                }
            }
            closedir($dirHandle);

        } else {
            $result=false;
        }
        return $result;
    }

	/**
	 * A wrapper for PHP fwrite
	 * @param string $exp
	 * @return bool|int
	 * @version 0.1.0.3
     * @internal Note "w+" truncates the file to zero, that is, deletes everything in
	 * the file before writing. If the file doesn't exists, it create a new file.
	 */
    public static function write_to_file( $file_name, $data, $write_mode=NULL ) {

        if(empty($write_mode)){
            $write_mode_arg = "w";
            // Alternate modes a+, w+
        } elseif($write_mode == "clear-then-write"){
            $write_mode_arg = "w+";
        } elseif($write_mode == "append-top" || $write_mode == "append"){
            $write_mode_arg = "a+";
        } else {
            return -1;
        }
        if(empty($file_name)) {
            return false;
        }
        // Verify that the folder exists
        $path    = pathinfo($file_name, PATHINFO_DIRNAME);
        if(! file_exists($path)) {
            //CreateFolder($path);
        }
        $fh = fopen($file_name, $write_mode_arg);
        if (!$fh) {
            echo __METHOD__ ."()<br />";
            //echo "<span style='color:red;font-weight:700;'>Error on file open </span>(<pre>$file_name</pre>)";
            echo "<br><br>$data<br><br>)";
            return -1;
        }
        $len_data = strlen($data);
        if($write_mode == "append-top"){
            rewind($fh);
		}
        fwrite($fh, $data, $len_data);
        fclose($fh);
        return true;
    }

}
// </editor-fold>
