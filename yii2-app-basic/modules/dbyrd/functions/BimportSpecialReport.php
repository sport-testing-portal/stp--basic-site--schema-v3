<?php

/*
 * @todo return JEDE
 * @todo return JEDE JSON object and end yii app (classic ajax behavior)
 * @todo return JSON object and end yii app (classic ajax behavior)
 *
 *
 */

/**
 * @author Dana Byrd <dana@globalsoccermetrix.com>,dana@byrdbrain.com
 */
class bimportSpecialReport extends CApplicationComponent
{
	// <editor-fold defaultstate="collapsed" desc="class properties">
	public static $items_processed_cnt = 0;
	public static $add_datetime_suffix_during_file_archival_yn = "Y";
	public static $keys = null;
	public static $keyBlank = [
		'person_id'=>0,
		'player_id'=>0,
		'test_eval_summary_log_id'=>0,
		'test_eval_detail_test_desc_id'=>0,
		'test_eval_detail_import_file_id'=>0,
		//''=>0,
	];
	/**
	 * Counters to track inserted items when readonly mode is enabled and DB inserts are prevented
	 * @var type
	 */
	protected static $psuedoKeys = [
		'person_id'=>0,
		'player_id'=>0,
		'test_eval_summary_log_id'=>0,
		'test_eval_detail_test_desc_id'=>0,
		'test_eval_detail_import_file_id'=>0,
		//''=>0,
	];
	protected static $dhxRows	= null; // header rows
	protected static $dhxdRows	= null; // detail rows (field mappings)

	protected static $mapRows	= null; // vwDataHdrXlation rows (field mappings)
	protected static $data		= null; // mtfva rows (pre table write) ;rawIn,rawOut,xlat
	protected static $mapsByTargetTable = null; // table name indexed assoc array of friendly dhxi rows

	protected static $test_descriptions			= [];
	protected static $imported_data_summary		= [];
	protected static $imported_files_summary	= [];
	protected static $full_name_tracker			= '';
	protected static $is_new_person_yn			= '';
	protected static $use_case_scenario			= '';
	protected static $use_case_scenario_options = ['development-testing','production-test','production-import'];

	//private $_debug_local = false;
	private static $_debug_local	= false;
	private static $_debug_yn		= 'Y';
	private static $_readonly_yn	= 'N';
	private static $_file_stats		= [];
	private static $_verbose_yn		= 'Y';

	private static $_htmlOut			= '';
	private static $_htmlOutputBuffer	= [];
	private static $_isAjaxRequest		= false;
	private static $_data_xlation_id	= 0; // xlat_baseid for the data source conversion map

	private static $_mapsByTargetTable = null;

	private static $default_file_path_stub = "/epf_inbox/data_sources__to_process/";
	private static $default_file_header_template_path = "/epf_inbox/data_sources__header_templates/";

	private static $_special_report__data_xlation_id = 0; // local alias for xlat_baseid ($_data_xlation_id)
	public static $status_states = ['conversion preparation','conversion testing','conversion','conversion reporting'];
	// </editor-fold>

	public function init() {
	}

	/**
	 * import pending data files by file type
	 * @param
	 * @return string
	 * @example http://gsm-public:8097/index.php/data/importSpecialReport?XDEBUG_SESSION_START=netbeans-xdebug
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function import($calledBy=null){
		//
		$debug_local = false;
		$dhx_rows = self::fetch_dhxSpecialReport(); // sets self::$_data_xlation_id

		//$archive_import_file_yn = 'Y'; // @todo: archive file method is being called?
// @todo: prevent duplicate file processing
		$files_to_process	= self::filterDataFilesByXlatTypeID(self::$_data_xlation_id); // id=7		// calls fetchCsvFiles()
		$target_tables		= Bdsf::fetch__xlatTargetTables(self::$_data_xlation_id);
		//$dhxd				= Bdsf::fetch__xlationItemsAR(self::$_data_xlation_id); // cec 0.1.0 codebase
		$maps				= self::getMap();
		$tmaps				= self::get_mapsByTargetTable();
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($tmaps, 10, true);

		$fileProcessedCnt = 0;
		self::$imported_files_summary=[];
		$fieldCnt = 0;
		//return;
		foreach ($files_to_process as $file_to_import){
			if ($debug_local){
				echo "<br><h2>Dumping \$file_to_import in forloop in " . __METHOD__ . "</h2><br>";
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($file_to_import, 10, true);
			}

			$csvFileFva		        = Bcsv2arr::convert_to_array($file_to_import['file_uri']);
			$import_file_id			= self::log_data_source_file($file_to_import['file_uri']);
			$keys=[];
			$keys['test_eval_detail_import_file_id'] = $import_file_id;

			$colCnt = count($csvFileFva, COUNT_RECURSIVE);
			$rowCnt = count($csvFileFva);
			if($debug_local){
				echo "<br>Dumping CSV counts in outer loop in " . __METHOD__ . "<br>";
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($colCnt, 10, true); // cnt = 1764
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rowCnt, 10, true); // cnt = 126
			}

			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($data_array, 10, true);
			if (self::$_verbose_yn == 'Y'){
				echo "<br><br><span style='color:green;font-size:18px;font-family:sans;'>Beginning loop through CSV values</span><br>";
			}
			$rowsProcessedCnt=0;
			$sourceDataRowNum=0;

			foreach($csvFileFva as $csvRowFva){
				$sourceDataRowNum++;
				$keys['test_eval_detail_import_file_line_num'] = $sourceDataRowNum;
				if ($debug_local && self::$_verbose_yn == 'Y'){
					echo '<div style="height:5px;border-bottom;border-width:1px;border-color:black;width:100%;"></div>';
					echo "<br><br><span style='color:orange;font-size:18px;font-family:sans;font-weight:700;'>Dump of a \$csv array in CSV data loop </span>in ".__METHOD__ ." ln:" . __LINE__ . "<br>";
				}
				/*
				if( fmod($rowsProcessedCnt,2) === 0 ){
					YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($csvRowFva, 10, true);
				} else {
					//Barray::echo_array($csvRowFva);
					Bcommon::array_to_html_table([$csvRowFva],1450, 100, 1);
					//Bcommon::array_to_html_table([$csvRowFva],1450);
				}*/
				if($debug_local){
					YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($csvRowFva, 10, true);
				}
				$keys = self::massAssign($csvRowFva,$maps,$keys,$tmaps);

				$rowsProcessedCnt++;
			}

			$fileProcessedCnt++;
			//self::archive_import_file(basename($file_to_import['file_uri']));
			self::archive_import_file($file_to_import['file_uri']);
		}

		if ( isset($colCnt) && isset($rowCnt) ){
			$info[] = ['fileName'=>basename($file_to_import['file_uri']),'fieldCnt'=>$colCnt,'rowCnt'=>$rowCnt];
			$msg = "<b>$rowCnt</b> rows and <b>$colCnt</b> fields of data processed from <b>"
					. basename($file_to_import['file_uri'])."</b><br>";
			self::ah($msg,'import_status');
		}
		if (self::$_debug_local && self::$_verbose_yn == 'Y'){
			echo "<br><br><h2><span style='color:green;'>End of loop through CSV values in ".__METHOD__."</span></h2><br>";
		}
		self::removeImportProcessSupportFiles(); // delete 'multiple tables.csv'


		if ($calledBy=='ImportFileAjaxEndpoint'){
			// higher level routines may need to 'append' output data. So bubble up the msgs
			// rather than using msgToCaller() which will 'end' the session.
			return self::htmlOut();
		} elseif (isset($info)){
			self::msgToCaller($info);
		} else {
			self::msgToCaller();
		}
	}

	/**
	 * Get an actionable real file path to data file resources on the server
	 *
	 * @param string $path_type 'base_folder','to_process','archive','purged'
	 * @return string real file path to a data file folder
	 * @internal source = DataController.php get__import_path()
	 */
	protected static function get__import_path($path_type=null)
	{
		if (empty($path_type)){
			$path_type = 'base_folder';
		}

		$base_path = Yii::app()->params['data_input__by_file__path']['real_path_base_folder'];
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($base_path, 10, true);
		// replace relative paths with real paths
		$paths = Yii::app()->params['data_input__by_file__path'];
		foreach ($paths as $idx=>$path){
			//$paths[$idx] = $path_from_alias . $path;
			if ($idx == 'real_path_base_folder'){
				continue;
			}
			$paths[$idx] = $base_path . $path;
		}

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($paths, 10, true);
		return $paths[$path_type];
	}

	/**
	 * Return mass assignable array of import file metadata (name, uri, size, date, sha, etc)
	 * @category data import generic base
	 * @param string $file_name
	 * @return array $stats An import file metadata array enables import-file inserts
	 * @version 0.3.0
	 * @see v0.2 Base source code from DataController.php=>source_file_metadata()
	 * @see v0.1 Base source code from ImportFileController.php=>ImportFileStats()
	 */
	protected static function source_file_metadata($file_name) {
		// if no path use default
		//$path = $this->default_file_path;
		//$file_uri = $path . "/" . $file_name;
		$path  = self::get__import_path('to_process');
		// sha256
		$file_uri = $path . $file_name;
		if (! file_exists($file_uri)){
			// file doesnt exist
			throw new CHttpException(400, 'Invalid request. The requested file does not exist.');
		}
		$sha256    = hash_file("sha256", $file_uri);
		$file_dt   = filemtime($file_uri);
		$file_date = Bdate::sql_dt($file_dt);
		$file_size = filesize($file_uri);
		//? Generate a mass assignable array?
		$stats = array(
			'import_file_source_file_name'=>$file_name
			,'import_file_source_file_sha256'=>$sha256
			,'import_file_source_file_date'=>$file_date
			,'import_file_source_file_size'=>$file_size
			,'import_file_source_file_uri'=>$file_uri
		);
		self::$_file_stats = $stats;
		return $stats;
	}


	/**
	 * Imports a data source file's meta properties into the import_file table.
	 * @param string $file_name_or_file_uri
	 * @return int import_file_id The pkey
	 * @see /controllers/DataController>log_data_source_file
	 * @see /controllers/ImportFile=>actionImport()
	 * @internal Development Status = ready for testing (rft)
	 * @category writes-sql-data
	 * @version 0.1.0 pulled direct from data controller
	 */
	public static function log_data_source_file($file_name_or_file_uri){
		// @todo: get path
		$file_name = basename($file_name_or_file_uri);
		$file_path = pathinfo($file_name_or_file_uri, PATHINFO_DIRNAME) . '/';
		if (strlen($file_path == 1)){
			$file_path  = self::get__import_path('to_process');
		}
		$file_uri = $file_path . $file_name;

		// see if the file is writable
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($file, 10, true);
		//$file_name = $file['import_file_source_file_name'];
		//$file_name = $file['file_name'];
		$cFile = Yii::app()->file->set($file_uri, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cFile, 10, true);
		$is_writable = $cFile->getWriteable();
		$ok_to_import_yn = "N";
		if ($is_writable == false){
			//skip this file
			$toast_msg = "The file you are attempting to import <br>"
			."<b>$file_name </b>in not writeable!<br>"
			."This means that this file can not archived by the system as is: "
			. "<br><strong>Therefore this file can NOT be imported as is</strong><br>"
			. "Please talk to your administrator via our Contact page.<br>"
			. "We will respond ASAP, and resolve any issue(s) as quickly as we can.<br>";

			Yii::app()->user->setFlash('error', $toast_msg);
			return 0;
		} else {
			$ok_to_import_yn = "Y";
		}

		if ($ok_to_import_yn == "Y"){
			//$file_name = $file['file_name'];
			// insert base import file
			$row__insert_file = self::source_file_metadata($file_name); // returns file meta data
			$model = new ImportFile;
			$model->attributes = $row__insert_file; // mass assign the attribute values
			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model, 10, true);
			if($model->save()) {
				return (int)$model->attributes[$model->primaryKey()];
			} else {
				// row not saved
				return 0;
			}

			//$this->archive_import_file($file_name);
		}
	}



	/**
	 * This is a Crude prototype. See parsePerson() in this file.
	 * @param type $csvRow
	 * @deprecated since version 0
	 */
	protected function handlePersonTable($csvRow) {
		foreach($dhxd[0]->getRelated('dataHdrXlationItems') as $xlation_item_row){
//					echo "<br>Dump of a \$dhxd child object \$row->attributes in forloop in actionImportSpecialReport<br>";
//					YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($row->attributes, 10, true);
			if ($xlation_item_row->attributes['data_hdr_xlation_item_target_table'] == 'person'){
				// process as a person row
				$full_name = $csv_row[$xlation_item_row->attributes['data_hdr_xlation_item_source_value']];
				if ($full_name_tracker !== $full_name){
					$full_name_tracker = $full_name;
					$is_new_person_yn  = "Y";
				} else {
					$is_new_person_yn  = "N";
				}
//						$person_data[$xlation_item_row->attributes['data_hdr_xlation_item_target_value']]
//								= $csv_row[$xlation_item_row->attributes['data_hdr_xlation_item_source_value']];
				$name_parts = Bcommon::splitNameIntoParts($full_name);
				//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($name_parts, 10, true);
				$person_data['person_name_first'] = $name_parts['first_name'];
				$person_data['person_name_last']  = $name_parts['last_name'];
				$person_data['person_name_full']  = $name_parts['full_name'];
				$person_data['person_type_id']    = 1; // player
				$person_id = self::insert_person_stub($person_data);

				//$summary_data = array('person_id'=>$person_id,'test_eval_type_id'=>1);
				// @todo: Try adding the source_event_id as well as the person_id
				//$summary_data = array('person_id'=>$person_id);
				if ( isset($csv_row['Event Id']) ){
					$source_event_id = $csv_row['Event Id'];
				} else {
					$source_event_id = '';
				}

				$summary_data = array('person_id'=>$person_id, 'source_event_id'=>$source_event_id);
				if ($is_new_person_yn == "Y"){
					$summary_id   = self::insert_test_eval_summary_log($summary_data);
				}
				$player_data  = array('person_id'=>$person_id
						,'player_sport_preference'=>'Soccer');
				$player_id    = self::insert_player_stub($player_data);

			} else {
				continue;
			}
		}

	}

	/**
	 * simplify Parse Logic
	 * Intent: get rid of long field names muddying the code's intent
	 * @param type $param
	 */
	protected static function baseMap() {
		// original field names are on the left
		return [
			'data_hdr_xlation_id'						=>'dhx_id'
			,'data_hdr_xlation_item_id'					=>'dhxi_id'
			,'data_hdr_entity_name'						=>'source_entity'
			,'data_hdr_xlation_item_source_value'		=>'source_value'
			,'data_hdr_xlation_item_target_value'		=>'target_value'
			,'ordinal_position_num'						=>'col_ord'
			,'data_hdr_xlation_item_target_table'		=>'target_table'
			,'data_hdr_source_value_type_name_source'	=>'type_name_source'
			,'data_hdr_source_value_is_type_name'		=>'type_name'
		];

	}

	/**
	 * Provides simplified map field names eg source_field, target_field, target_table, etc
	 * @return array
	 * @version 0.4.0
	 * @category CEC 0.2.0 Simplicity is good, use it when possible.
	 * @internal Development Status = ready for testing (rft)
	 * @todo get rid of magick #
	 */
	protected static function getMap() {
		if (is_null(self::$mapRows)){
			$query = 'select * from vwDataHdrXlation where dhx_id = :dhx_id';
			$params = [':dhx_id' => 7]; // 7 = Special Report
			$cmd = Yii::app()->db->createCommand($query);
			self::$mapRows = $cmd->queryAll($fetchAssociative=true, $params);
		}
		return self::$mapRows;
	}

	/**
	 * Primary Row Parsing Handler - An Orchestration method for parsing a incoming data row
	 * @param array[] $csvRow FVA from the CSV source file. eg. ['fld1'=>val,'f2'=>val]
	 * @param array[] $maps   DHXI rows in fva format using simplified DHXI field names @see self::getMap()
	 * @param array[] $keys   Relevent pkey and fkey values being passed down the stack
	 * @category orchestration method
	 */
	protected static function massAssign($csvRow, $maps, $keys,$tmaps) {

		$person_data		= self::parsePerson($csvRow, $tmaps, $keys);
		$player_data		= self::parsePlayer(self::parseKeys($keys, $person_data));
		$summary_data		= self::parseSummary($csvRow,$maps, self::parseKeys($keys, $person_data));
		$detail_data		= self::parseDetail($csvRow, $maps, self::parseKeys($keys, $summary_data));
		if(self::$is_new_person_yn == 'Y'){
			// update keys
			$keys = self::parseKeys($keys, $summary_data);
		}

		$insert_data = array(
			'person_data' => $person_data,
			'player_data' => $player_data,
			'summary_log' => $summary_data,
			'detail_log'  => $detail_data,
		);

		self::$imported_data_summary[] = $insert_data;
		return $keys;
	}

	/**
	 *
	 * @param type $keys
	 * @param type $data
	 * @return type
	 * @version CEC 0.2.0 automate insert chain handling
	 */
	protected static function parseKeys($keys,$data) {
		$keyList = self::keyNames();
		$out=[];
		foreach($keyList as $key){
			if (isset($keys[$key])){
				$out[$key] = $keys[$key];
			}
			// layer the newest data on top
			if (isset($data[$key])){
				$out[$key] = $data[$key];
			}
		}
		self::$keys = $out;
		return $out;
	}

	protected static function keyNames() {
		return [
			'person_id','player_id',
			'test_eval_summary_log_id',
			'test_eval_detail_log_id',
			'test_eval_detail_import_file_id',
			'test_eval_detail_import_file_line_num',
			'event_id',
		];
	}

	/**
	 *
	 * @param type $csvRow
	 * @param array $maps tmap CEC 0.2.0 table-indexed map array.$a['table1'=>[[],[],]]
	 * @param array[] $keys Pkeys and fkeys.
	 * @return type
	 * @todo pass in only person map items
	 */
	protected static function parsePerson($csvRow,$maps, $keys) {

		//echo "<br>Parsing person data \$csv_row array in CSV data loop in ".__METHOD__."<br>";
		$person_data = [];
		// foreach($maps as $map){ v0.1.0 code loops through entire map array
		foreach($maps['person'] as $map){ // Assumes that a tmap CEC 0.2.0 table-indexed map array
//					echo "<br>Dump of a \$dhxd child object \$map in forloop in <br>";
//					YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($map, 10, true);
			if ($map['target_table'] == 'person'){
				// @todo: convert to person only map

				// process as a person row
				if ($map['target_value'] == 'parsePersonNameToParts()'){
					$full_name = $csvRow[$map['source_value']];
					if (self::$full_name_tracker !== $full_name){
						self::$full_name_tracker = $full_name;
						self::$is_new_person_yn  = 'Y';
					} else {
						self::$is_new_person_yn  = 'N';
						// Only parse person when its anew person
						return $keys;
					}

					$name_parts = Bcommon::splitNameIntoParts($full_name);
					//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($name_parts, 10, true);
					$person_data['person_name_first'] = $name_parts['first_name'];
					$person_data['person_name_last']  = $name_parts['last_name'];
					$person_data['person_name_full']  = $name_parts['full_name'];
					$person_data['person_type_id']    = $person_type_id=1; // player
				}
				if ($map['target_value'] == 'person_email_personal'){

					$email = self::emailISanityCheck($csvRow[$map['source_value']]);
					$person_data[$map['target_value']] = $email;
				}

//				if (self::$_readonly_yn == 'N'){
//					$person_id = self::insert_person_stub($person_data);
//				} else {
//					$person_id = 0;
//				}
//				$person_data['person_id'] = $person_id;

			} else {
				continue;
			}
		}

		if(self::$_readonly_yn == 'N'){
			$person_id = self::insert_person_stub($person_data, $person_type_id);
		} else {
			$person_id = self::getPsuedoKey('person');
		}
		$person_data['person_id'] = $person_id;

		echo "<br><span style='color:green;font-family:sans;'>Dump of \$person_data post parsing</span> in ".__METHOD__."<br>";
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($person_data, 10, true);

		return $person_data;
	}

	/**
	 * Filter out known bogus email addresses
	 * @param string $email
	 * @return string $email
	 */
	protected static function emailISanityCheck($email) {
		if ($email == 'athlete@globalsoccermetrix.com'){
			return '';
		} elseif (stripos($email,'@globalsoccermetrix.com')!== false){
			return '';
		} else {
			return $email;
		}
	}

	/**
	 *
	 * @param array[] $keys
	 * @return array[]
	 */
	protected static function parsePlayer($keys) {
		// Only parse player when its anew person
		$is_new_person_yn = self::$is_new_person_yn;
		if ($is_new_person_yn === 'N'){
			return $keys;
		}

		if (isset($keys['person_id'])){
			$person_id = $keys['person_id'];
		} else {
			$person_id = self::getPsuedoKey('person');
		}

		$player_data  = [
			'person_id'					=>$person_id,
			'player_sport_preference'	=>'Soccer'
		];
		if(self::$_readonly_yn == 'N'){
			$player_id    = self::insert_player_stub($player_data);
		} else {
			$player_id = self::getPsuedoKey('player');
		}
		$player_data['player_id'] = $player_id;
		echo "<br><span style='color:green;font-family:sans;'>Dump of \$player_data post parsing</span> in ".__METHOD__."<br>";
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($player_data, 10, true);
		return $player_data;
	}

	/**
	 *
	 * @param type $csvRow
	 * @param type $keys
	 * @return type
	 * @todo remove hard-coded values
	 * @todo add row insert logic?
	 *
	 */
	protected static function parseSummary($csvRow, $maps, $keys) {
		//$summary_data = array('person_id'=>$person_id,'test_eval_type_id'=>1);
		// @todo: Try adding the source_event_id as well as the person_id
		// Only parse summary when its a new person
		$is_new_person_yn = self::$is_new_person_yn;
		if ($is_new_person_yn === 'N'){
			return $keys;
		}

		if (isset($keys['person_id'])){
			$person_id = $keys['person_id'];
		} else {
			$person_id = self::getPsuedoKey('person');
		}
		// 'Event Id' is already mapped to TEDL
		if ( isset($csvRow['Event Id']) ){
			$source_event_id = $csvRow['Event Id'];
		} else {
			$source_event_id = '';
		}

		$summary_data = array('person_id'=>$person_id, 'source_event_id'=>$source_event_id);
		if ($is_new_person_yn == "Y"){
			if(self::$_readonly_yn == 'N'){
				$summary_id		= self::insert_test_eval_summary_log($summary_data);
			} else {
				$summary_id		= self::getPsuedoKey('summary');
			}
		}
		$summary_data['test_eval_summary_log_id'] = $summary_id;
		echo "<br><span style='color:green;font-family:sans;'>Dump of \$summary_data post parsing</span> in ".__METHOD__."<br>";
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($summary_data, 10, true);
		return $summary_data;
	}

	/**
	 * convert a test desciption type name value into a type row id
	 * notes: this should be a universal component
	 * @param string $type_name_value
	 * @return int type_id
	 * @version 0.2.0 Use a static desc array for cache storage
	 * @internal A copy of this function has been added to the DataController.
	 * @internal A copy of this function has been added to the TestEvalDetailLog model.
	 */
	protected static function get_test_description_type_id_from_type_name($type_name_value){

		if (isset(self::$test_descriptions[$type_name_value])){
			return self::$test_descriptions[$type_name_value];
		}

		$type_name_field_name = 'test_eval_detail_test_desc';
		$row = TestEvalDetailTestDesc::model()->findByAttributes(
				array($type_name_field_name => $type_name_value)
				);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($row, 10, true);
		if (empty($row) === false){
			$type_id = (int)$row->attributes['test_eval_detail_test_desc_id'];
		} else {
			$type_id = 0;
		}

		self::$test_descriptions[$type_name_value] = $type_id;
		return $type_id;
	}

	protected static function parseDetail($csvRow,$maps,$keys) {
		if (!is_null($keys) && isset($keys['test_eval_summary_log_id'])){
			$summary_id				= $keys['test_eval_summary_log_id'];
			$import_file_id			= $keys['test_eval_detail_import_file_id'];
			$import_file_line_num	= $keys['test_eval_detail_import_file_line_num']; //= $row_key + 1;
		} else {
			throw new CException("proper keys were not passed");
		}
		$debug_local = self::$_debug_local;
		// loop through
		foreach( $maps as $map){ // map = dhxi collection
			if ($debug_local == true){
				echo "<br>Parsing test_eval_detail_log data \$csv_row array in CSV data loop <br>";
			}
			if ($map['target_table'] == 'test_eval_detail_log'){
				// process as a 'test_eval_detail_log' row
				$target_field_name = $map['target_value'];

				/* Must handle empty source_record_id values
				 * if (
						$target_field_name == 'test_eval_detail_source_record_id'
						||
						$target_field_name == 'test_eval_detail_source_event_id'
					){
					$detail_log_data[$xlation_item_row->attributes['data_hdr_xlation_item_target_value']]
							= (int)$csv_row[$xlation_item_row->attributes['data_hdr_xlation_item_source_value']];
				 *
				 */
				if ($target_field_name == 'test_eval_detail_source_event_id'){
					$detail_log_data[$map['target_value']]
							= (int)$csvRow[$map['source_value']];
				}elseif ($target_field_name == 'test_eval_detail_source_record_id'){
					// source record id is sometimes empty
					$source_val = $csvRow[$map['source_value']];
					if ((int)$source_val > 0){
						$detail_log_data[$map['target_value']]
							= (int)$csvRow[$map['source_value']];
					} else {
						$detail_log_data[$map['target_value']] = '';
					}
//						}elseif ($target_field_name == 'test_eval_detail_source_record_id'){
//							$detail_log_data[$xlation_item_row->attributes['data_hdr_xlation_item_target_value']]
//									= (int)$csv_row[$xlation_item_row->attributes['data_hdr_xlation_item_source_value']];
				} elseif($target_field_name == 'created_dt'){
					// '6/7/2014 3:07'
					if (empty($csvRow[$map['source_value']]) == false){
						$dt = DateTime::createFromFormat('m/d/Y H:i'
								, $csvRow[$map['source_value']]);

						if ($dt instanceof DateTime){
							$time = $dt->format('U');
							$detail_log_data[$map['target_value']]
								= date('Y-m-d H:i:s', $time);
						} else {
							echo "<br>Data conversion failed: source date = " . $csvRow[$map['source_value']] . "<br>";
							$time = strtotime($csvRow[$map['source_value']]);
							YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dt, 10, true);
							$detail_log_data[$map['target_value']]
								= date('Y-m-d H:i:s', $time);
						}
//								$detail_log_data[$xlation_item_row->attributes['data_hdr_xlation_item_target_value']]
//										= date('Y-m-d H:i:s', $dt2);
					}
				} elseif($target_field_name == 'test_eval_detail_test_desc_id'){
					// eg 'GSM-Vertical Jump-Step In'
					$type_id = self::get_test_description_type_id_from_type_name(
							$csvRow[$map['source_value']]);
					if ((int)$type_id == 0){
						// Call for an type description insert
						$test_desc_values = array(
							'test_eval_detail_test_desc'=>trim($csvRow[$map['source_value']])
							,'units'=>$csvRow['Drill Unit']
							,'test_eval_detail_test_desc_display_order'=>'99'
						);
						self::insert_test_eval_detail_test_desc($test_desc_values);
						$type_id = self::get_test_description_type_id_from_type_name(
							$csvRow[$map['source_value']]
						);
					}
					if (empty($type_id)){
						if (YII_DEBUG && YII_TRACE_LEVEL >=3){
							echo "<br>Bogus type name value encountered (*"
								. $csvRow[$map['source_value']]
									."*) CSV array row_id = " . $csvRow['array_row_id'] ."<br>";
						}
					}
					$detail_log_data[$map['target_value']]
							= $type_id;
				} elseif($target_field_name == 'test_eval_detail_score_num'){
					// next line depreciated because it allows zero's rather null
					//$detail_log_data[$xlation_item_row->attributes['data_hdr_xlation_item_target_value']]
					//		= (float)$csv_row[$xlation_item_row->attributes['data_hdr_xlation_item_source_value']];

					$test_eval_detail_score_num= (float)$csvRow[$map['source_value']];
					if ( empty($test_eval_detail_score_num)){
						$detail_log_data[$map['target_value']] = null;
					} else {
						$detail_log_data[$map['target_value']] = $test_eval_detail_score_num;
					}
				} else {
					$detail_log_data[$map['target_value']]
							= $csvRow[$map['source_value']];
				}

			} else {
				continue;
			}
		}
		$detail_log_data['test_eval_summary_log_id']              = $summary_id;
		$detail_log_data['test_eval_detail_import_file_id']       = $import_file_id;
		$detail_log_data['test_eval_detail_import_file_line_num'] = $import_file_line_num;

		//$detail_log_id  = self::insert_test_eval_detail_log($detail_log_data);
		if (self::$_readonly_yn == 'N'){
			//$detail_log_id  = TestEvalDetailLog::Store($detail_log_data); // has dupe checking included
			$detail_log_id  = TestEvalDetailLog::importInsert($detail_log_data);
		} else {
			$detail_log_id = self::getPsuedoKey('detail');
		}

		$detail_log_data['test_eval_detail_log_id']   = $detail_log_id;
		$debug_local = true;
		if ($debug_local){
			echo "<br><span style='color:green;font-family:sans;'>Dump of \$detail_data post parsing</span> in ".__METHOD__."<br>";
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($detail_log_data, 10, true);
		}

//		$insert_data = array(
//			'person_data' => $person_data,
//			'summary_log' => $summary_data,
//			'detail_log'  => $detail_log_data,
//			'player_data' => $player_data
//		);
//		self::$imported_data_summary[] = $insert_data;
		return $detail_log_data;
	}

	/**
	 * Store a pre separated and cached table map by target table name
	 * Essentially retrieves self::$mapsByTargetTable and populates as needed
	 * @param bool $refresh true|false True clears the local cache eg self::$mapsByTargetTable=null
	 * @return array
	 * @internal Development Status = code construction
	 * @internal output consumed by row parser methods
	 */
	protected static function get_mapsByTargetTable($refresh=false) {
		if ($refresh == true){
			// overwrite module level maps? eg self::$mapRows?
			self::$mapsByTargetTable = null;
		}

		if (is_null(self::$mapsByTargetTable) ){
			$maps = self::getMap();
		} else {
			return self::$mapsByTargetTable;
		}
		$tables=[]; // capture table name
		foreach ($maps as $map) {
			if (isset($map['target_table']) && !empty($map['target_table']) ){
				if( array_search($map['target_table'], $tables) === false){
					$tables[]=$map['target_table'];
				}
			}
		}
		// parse maps by target table names
		$out=[];
		foreach ($maps as $map) {
			foreach($tables as $table){
				if ($table == $map['target_table']){
					$out[$table][] = $map;
				}
			}
		}
		self::$mapsByTargetTable = $out;
		return $out;
	}

	/**
	 * Store a pre separated and cached table map by target table name
	 * Essentially retrieves self::$mapsByTargetTable and populates as needed
	 * @param bool $refresh true|false True clears the local cache eg self::$mapsByTargetTable=null
	 * @return array
	 * @internal Development Status = code construction
	 * @internal output consumed by row parser methods
	 */
	protected static function parseDefaultMaps($refresh=false) {
		if ($refresh == true){
			// overwrite module level maps? eg self::$mapRows?
			self::$mapsByTargetTable = null;
		}

		if (is_null(self::$mapsByTargetTable) ){
			$maps = self::getMap();
		} else {
			return self::$mapsByTargetTable;
		}
		$tables=[]; // capture table name
		foreach ($maps as $map) {
			if (isset($map['target_table_name']) && !empty($map['target_table_name']) ){
				if( array_search($map['target_table_name'], $tables) === false){
					$tables[]=$map['target_table_name'];
				}
			}
		}
		// parse maps by target table names
		$out=[];
		foreach ($maps as $map) {
			foreach($tables as $table){
				if ($table == $map['target_table_name']){
					$out[$table][] = $map;
				}
			}
		}
		self::$mapsByTargetTable = $out;
		return $out;
	}

	/**
	 *
	 * @param type $dhxd
	 * @param type $csvRow
	 * @param array $tableNames
	 * @example path description
	 * @internal Development Status = code construction
	 */
	protected static function parseAttributes($dhxd, $csvRow, $tableNames=null) {
		if (is_null($tableNames)){
			// build the table list
			foreach ($dhxd as $data_hdr_xlation_item_id => $dhxi_row) {
				$tableNames[$dhxi_row['data_hdr_xlation_item_target_table']]=[];
			}
		}
		$mtfva = []; // multi-table field-value array
		$mtfvaBase = []; // multi-table field-value array

		foreach($tableNames as $tableName){
			$targetTableRow=[];
			foreach ($csvRow as $colName=>$colVal){
				$targetTableRow[''];
			}

		}

	}
	//[]

	/**
	 *
	 * @param type $dhxd
	 * @param type $csvRow
	 * @param array $tableNames
	 * @internal Development Status = code construction
	 */
	protected static function parseBulkAttributesViaStripes($dhxd, $csvRow, $tableNames=null) {
		if (is_null($tableNames)){
			// build the table list
//			foreach ($dhxd as $data_hdr_xlation_item_id => $dhxi_row) {
//				$tableNames[$dhxi_row['data_hdr_xlation_item_target_table']]=[];
//			}
		}
		$mtfva = [
			'person'=>[

			],
			'test_eval_detail_log'=>[

			]
		]; // multi-table field-value array

//		$mtfvaBase = []; // multi-table field-value array
//
//		$persons[];
//		$summary[];
//		$tedl[];

		foreach($tableNames as $tableName){
			$targetTableRow=[];
			foreach ($csvRow as $colName=>$colVal){
				foreach ($dhxd as $key => $row) {

				}
				$targetTableRow[''];
			}

		}

	}

	/**
	 *
	 * @param type $dhxd
	 * @param type $csvRows
	 * @param array $tableNames
	 * @internal Development Status = code construction
	 */
	protected static function parseBulkAttributes($dhxd,$csvRows, $tableNames=null) {
		if (is_null($tableNames)){
			// build the table list
			foreach ($dhxd as $data_hdr_xlation_item_id => $dhxi_row) {
				$tableNames[$dhxi_row['data_hdr_xlation_item_target_table']]=[];
			}
		}
		$mtfva = []; // multi-table field-value array
		$mtfvaBase = []; // multi-table field-value array

		//$tables
	}

	/**
	 *
	 * @param type $tableName
	 * @return array
	 * @internal Development Status = code construction
	 */
	protected static function defaultInsertValues($tableName=null) {
		$defaults = [
			'test_eval_summary_log'=>[

			],
			'person'=>[
				'person_type_id'=>1,
			],
			'event'=>[
				'event_type_id'=>1,
			],

		];
		if (! is_null($tableName) && isset($defaults[$tableName])){
			return $defaults[$tableName];
		} else {
			return $defaults;
		}
	}

	/**
	 * Lookup Person and Update OR Insert a basic stub row for person in the dataabase
	 * @param array[] $person_data
	 * @return int $person_id
	 * @internal source code from: DataController
	 */
	protected static function insert_person_stub($person_data){
		// default to player person_type_id
		/*
		if (empty($person_type_id)){
			$person_type_id = 1;
		}

		$name_parts = bcommon::splitNameIntoParts($full_name);
		*/
		if (array_key_exists('person_name_first', $person_data) == false
		  || array_key_exists('person_name_last', $person_data) == false)
		{
			throw new Exception("Person's first name and last name are required!");
		} elseif (array_key_exists('person_type_id', $person_data) == false) {
			// set the default person type to insert
			$person_data['person_type_id'] = 1; // 1=player, 2=coach
		}
		// see if the person exists already
		$criteria = new CDbCriteria();
		$criteria->select    = array('person_id', 'person_name_first', 'person_name_last');
		$criteria->condition = 'person_name_first=:person_name_first';
		$criteria->addCondition('person_name_last=:person_name_last', 'AND');
		$criteria->order     = 'person_id';
		$criteria->params    = array(
			':person_name_first'=>$person_data['person_name_first']
			,':person_name_last'=>$person_data['person_name_last']
		);
		$local_debug = true;
		if ($local_debug == true){

			//$cmd = Yii::app()->db->createCommand($criteria);
			$PersonSearchTest = Person::model()->find($criteria);
			//CVarDumper::dump($PersonSearchTest->attributes,15,true);
			if(is_array($PersonSearchTest) && isset($PersonSearchTest[0]->attributes['person_id'])){
				$existing_person_id = (int)$PersonSearchTest[0]->attributes['person_id'];
			}
			if(is_object($PersonSearchTest) && isset($PersonSearchTest->attributes['person_id'])){
				$existing_person_id = (int)$PersonSearchTest->attributes['person_id'];
			}
//			$obj = $cmd->query($cmd);
//			CVarDumper::dump($obj,10,true);
		}

		$Persons = Person::model()->findAll($criteria);
		if (count($Persons, COUNT_RECURSIVE) == 0){
			// insert?
			$model = new Person();
			//$this->performAjaxValidation($model, 'import-file-form');
			$model->attributes     = $person_data;
			//$model->person_type_id = $person_type_id;
		} elseif (count($Persons, COUNT_RECURSIVE) > 0){
			// update
			// if a person is changing their name then the lookup would not have found them with the search.
			// In short, no update is needed in this method
			$person_id = $Persons[0]->attributes['person_id'];
			return (int)$person_id;
			//$model     = Person::model()->findByPk($person_id);
			// the next method isn't needed because model::beforeSave() will call model::validate()
			// $this->performAjaxValidation($model, 'person-form');
			/*
			if(isset($_POST['Person']))
			{
					if (isset($_POST['Person']['appUser'])) $model->appUser = $_POST['Person']['appUser'];
					$model->appUser = array();
					if (isset($_POST['Person']['gender'])) $model->gender = $_POST['Person']['gender'];
					$model->gender = array();
					if (isset($_POST['Person']['org'])) $model->org = $_POST['Person']['org'];
					$model->org = array();
					if (isset($_POST['Person']['personType'])) $model->personType = $_POST['Person']['personType'];
					$model->personType = array();
					if (isset($_POST['Person']['user'])) $model->user = $_POST['Person']['user'];
					$model->user = array();
				$model->attributes = $_POST['Person'];
				if($model->save()) {
					$this->redirect(array('view','id' => $model->person_id));
				}
			}*/
		}
		//echo "<br>Dumping found in insert_person_stub() <br>";
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($found, 10, true);


		if($model->save()) {
			$id = $model->person_id;
		} else {
			//echo "<br>Person::Save() failed <br>";
			return -1;
		}
		return $id;
	}

	/**
	 * parse a full name and create a person row in the database
	 * @param string $full_name
	 * @return int person_id
	 * @internal source code from: DataController
	 */
	protected static function insert_person_stub__test_harness(){
		// Test cases:
		//   A person known to exist with a type of player
		//   A person known NOT to exist with a type of player
		//   A person known to exist with a type of player parent
	}



	/**
	 * Assign a data_hdr_xlation type to each file
	 * @param array $file, int $data_hdr_xlation_id
	 * @return array(0=>array("file_name"=>"", "file_type"=>))
	 * @author Dana Byrd <dana@globalsoccermetrix.com>,dana@byrdbrain.com
	 */
	protected static function add_file_hdr_type_to_csv_file_list($files=NULL, $data_hdr_xlation_id=NULL) {
		// @todo assign a file type
		$out = array();

		foreach ($files as $file){
			if (stripos($file,'multiple tables') !== false){
				continue;
			}
			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($file, 10, true);
			$data_hdr_xlation_id = Bfilehead::determine_hdr_xlation_id($file);
			//Bfilehead::scrubAndRenameCsv($data_file_uri=NULL);
			$file_type = pathinfo($file, PATHINFO_EXTENSION);
			$out[] = array(
				"file_uri"				=>$file ,
				"file_type"				=>$file_type,
				"data_hdr_xlation_id"	=>$data_hdr_xlation_id,
			);
		}
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($out, 10, true);
		return $out;
	}

	/**
	 * Generate url for a list item link of waiting files
	 * @param the path to capture files from
	 * @return array(); array("file_uri","file_uri2")
	 * @author Dana Byrd
	 * @internal pulled source from: DataController::get_data_sources_to_process_csv($path=NULL)
	 * @internal a module level function copied here for convenience
	 */
	protected static function get_data_sources_to_process_csv($path=null) {
		// @todo: get path from config
		if (!is_null($path)){
			$cf = CFile::getInstance($path);
			if ($cf->getExists($path)){
				echo "path passed by caller exists ln:".__LINE__."<br>";
			}
		}
		//$path = Yii::getPathOfAlias('application'). "/epf_inbox/data_sources__to_process/";
		//$path = Yii::getPathOfAlias('application') . $path_stub;  // epf under protected
		$path_stub = self::$default_file_path_stub;
		$path_local = str_ireplace('/protected', '', Yii::getPathOfAlias('application')) . $path_stub;    // above protected
		// YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($path, 10, true); // golden
		$pathSansTrailingSlash = Bfile::remove_trailing_slash($path_local);
		$ext_to_process = ['csv'];
		$find_options = [
			'fileTypes'	=>$ext_to_process
			,'level'	=>0];
		$files = CFileHelper::findFiles($pathSansTrailingSlash, $find_options);
		// YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($files, 10, true); // golden

		if (count($files, COUNT_RECURSIVE) == 0){
			$files = array();
		}
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($files, 10, true);
		// format retuned is zero indexed fully qualified file uri
		// array(0=>'/var/www/.../file1',1='/var/www/.../file2')
		return $files;
	}

	/**
	 * insert a stub row into test_eval_summary_log and return a row id
	 * @param string $person_id
	 * @return int person_id
	 * @version 0.1
	 */
	protected static function insert_test_eval_summary_log__v01($data_values){
		// @todo: duplicate rows are being inserted.
		$model = new TestEvalSummaryLog();
		if(array_key_exists('person_id', $data_values) === false){
			$message  = "person_id is a required parameter";
			$code     = "insert_test_eval_summary_log";
			$previous = '';
			throw new Exception($message, $code, $previous);
		}
		$model->unsetAttributes(); // clear any default values
		$model->attributes = $data_values;
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model, 10, true);
		if($model->save()) {
			$id = $model->test_eval_summary_log_id;
		}
		return $id;
	}

	/**
	 * Insert a player stub in SQL
	 * @param array[] $data_values An FVA array eg ['fld1'=>'val','fld2'=>'val']
	 * @return int player_id
	 */
	protected static function insert_player_stub($data_values){
		// @todo: Are we assuming the person already exists? If so what is the person_id
		// insure the person_type_id exists
		if (array_key_exists('person_id', $data_values) === false){
			$person_id = self::insert_person_stub($data_values);
			if ((int)$person_id > 0){
				$data_values['person_id'] = (int)$person_id;
			}
		} else {
			$person_id = (int)$data_values['person_id'];
		}

		$model     = new Player();
		$player_id = $model->Store($data_values);
		return (int) $player_id;
	}

	/**
	 * Insert or update a coach data row
	 * @param array $data_values
	 * @return int $coach_id
	 */
	protected static function insert_coach_stub($data_values){
		// @todo: Are we assuming the person already exists? If so what is the person_id
		// insure the person_type_id exists
		if (array_key_exists('person_id', $data_values) === false){
			$person_id = self::insert_person_stub($data_values);
			if ((int)$person_id > 0){
				$data_values['person_id'] = (int)$person_id;
			}
		} else {
			$person_id = (int)$data_values['person_id'];
		}

		$model     = new Coach();
		$id        = $model->Store($data_values);
		return (int) $id;
	}

	/**
	 *
	 * @return array[CActiveRecord]
	 * @category low-level sql access
	 * @internal calls self::fetch_dhx
	 * @internal Development Status = ready for testing (rft)
	 */
	protected static function fetch_dhxSpecialReport() {
		$data_hdr_entity_name = 'STI';
		$data_hdr_xlation_name = 'STI Special Report';

		$dhx_rows = self::fetch_dhx($data_hdr_entity_name, $data_hdr_xlation_name);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dhx_rows, 10, true);
		$data_xlation_id = (int) $dhx_rows[0]->attributes["data_hdr_xlation_id"];
		self::$_data_xlation_id = $data_xlation_id;
		self::$_special_report__data_xlation_id = $data_xlation_id;

		if ( self::$_debug_yn =='Y'){
			$msg = "\$special_report__data_xlation_id = (*$data_xlation_id*)";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
		}
		return $dhx_rows;
	}

	/**
	 * Sets the data header xlation ID
	 *
	 */
	protected static function fetch_dhx($data_hdr_entity_name, $data_hdr_xlation_name) {
		$dhx_rows = DataHdrXlation::model()->findAllByAttributes(
			array('data_hdr_entity_name'=>$data_hdr_entity_name,
				'data_hdr_xlation_name'=>$data_hdr_xlation_name)
				, 'data_hdr_xlation_name=:data_hdr_xlation_name'
				, [':data_hdr_xlation_name'=>$data_hdr_xlation_name]
			);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dhx_rows, 10, true);
		//$dx_row = $dhx_rows[0]->attributes;

		self::$_data_xlation_id = (int) $dhx_rows[0]->attributes["data_hdr_xlation_id"];
		return $dhx_rows;
	}

	/**
	 * insert a test_eval_detail_log row in the database and return an id
	 * @param array $data_values
	 * @return int test_eval_detail_log_id
	 * @internal Known issues; sometimes a source_record_id is legitimately empty
	 */
	protected static function insert_test_eval_detail_log($data_values){
		if (array_key_exists('test_eval_detail_test_desc_id',$data_values) == false
			|| empty($data_values['test_eval_detail_test_desc_id'])	){
			//echo "<br><br><span style='color:red;'>Critical value is missing:insert_test_eval_detail_log()</span><br>";
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($data_values, 10, true);
		}
		// Add a row update (happens if a report is processed twice or the import is partially processed
		if (
				(
					array_key_exists('test_eval_detail_source_event_id', $data_values) === true
					&&
					array_key_exists('test_eval_detail_source_record_id', $data_values) === true
				)
				&&
				(
					(int)$data_values['test_eval_detail_source_event_id'] > 0
					&&
					(int)$data_values['test_eval_detail_source_record_id'] > 0
				)
			)
		{
			// Build database query criteria
			$criteria = new CDbCriteria();
			$criteria->condition = 'test_eval_detail_source_event_id=:test_eval_detail_source_event_id';
			$criteria->addCondition('test_eval_detail_source_record_id=:test_eval_detail_source_record_id', 'AND');
			$criteria->params    = array(
				':test_eval_detail_source_event_id'  =>(int)$data_values['test_eval_detail_source_event_id']
				,':test_eval_detail_source_record_id'=>(int)$data_values['test_eval_detail_source_record_id']);
			$record_found  = TestEvalDetailLog::model()->find($criteria);
			if ($record_found instanceof CActiveRecord){
				$existing_row_id = $record_found->attributes['test_eval_detail_log_id'];

				if ((int)$existing_row_id > 0){
					// Perform an update
					$record_found->attributes = $data_values; // perform a mass assignment
					if($record_found->save()) {
						return $existing_row_id;
					} else {
						//echo "<br>TestEvalDetailLog::Save() existing row update failed <br>";
						YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($data_values, 10, true);
						YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($record_found->attributes, 10, true);
					}
					return $existing_row_id;
				}
			}
		}

		$model = new TestEvalDetailLog();
		$model->unsetAttributes();
		$model->attributes = $data_values;
		//echo "<br>Dumping model in insert_test_eval_detail_log()<br>";
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model, 10, true);
		if($model->save()) {
			$id = $model->test_eval_detail_log_id;
		} else {
			//echo "<br>TestEvalDetailLog::Save() failed <br>";
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($data_values, 10, true);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model->attributes, 10, true);
		}
		return $id;
	}

	/**
	 * insert a test_eval_detail_log row in the database and return an id
	 * @param array $data_values
	 * @return int test_eval_detail_test_desc_id
	 */
	protected static function insert_test_eval_detail_test_desc($data_values){
		if (array_key_exists('test_eval_detail_test_desc_id',$data_values) == false
			|| empty($data_values['test_eval_detail_test_desc_id'])	){
			//echo "<br><br><span style='color:red;'>Critical value is missing:insert_test_eval_test_desc()</span><br>";
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($data_values, 10, true);
		}
		$model = new TestEvalDetailTestDesc();
		$model->unsetAttributes();
		$model->attributes = $data_values;
		//echo "<br>Dumping model in insert_test_eval_detail_test_desc()<br>";
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model, 10, true);
		if($model->save()) {
			$id = $model->test_eval_detail_test_desc_id;
		} else {
			//echo "<br>TestEvalDetailTestDesc::Save() failed <br>";
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($data_values, 10, true);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model->attributes, 10, true);
		}
		return $id;
	}

	/**
	 * insert a stub row into test_eval_summary_log and return a row id
	 * @param string $person_id
	 * @return int person_id
	 * @version 0.2
	 */
	protected static function insert_test_eval_summary_log($data_values){
		// @todo: duplicate rows are being inserted.
		$model = new TestEvalSummaryLog();
		if(array_key_exists('person_id', $data_values) === false){
			$message  = "person_id is a required parameter";
			$code     = "insert_test_eval_summary_log";
			$previous = '';
			throw new Exception($message, $code, $previous);
		}
		$id = $model->Store($data_values);
		return $id;
	}

	/**
	 * Fetch all CSV files
	 * @return array['file1uri','file2uri']
	 * @internal Development Status = Golden!
	 */
	protected static function fetch_csv_files() {
		$csv_files = self::get_data_sources_to_process_csv(); // lists all csv files regardless of hdr type
		//echo "<br>Dumping files to process in ". __METHOD__ ."<br>";
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($files_to_process, 10, true);
		return $csv_files;
	}

	/**
	 * Remove 'muliple tables.csv' from the to_process folder
	 * @return array['file1uri','file2uri']
	 * @internal Development Status = Golden!
	 */
	protected static function removeImportProcessSupportFiles() {
		$csv_files = self::get_data_sources_to_process_csv(); // lists all csv files regardless of hdr type
		foreach($csv_files as $file_uri){
			if (stripos($file_uri,'multiple tables.csv') !== false){
				// delete the file
				$cfile = Yii::app()->file->set($file_uri, true);
				$cfile->delete($file_uri);
			}
		}
		//echo "<br>Dumping files to process in ". __METHOD__ ."<br>";
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($files_to_process, 10, true);
		return $csv_files;
	}


	protected static function getFileHashesFromDB() {
		// -- get tedl cnts per import file
		$query = "select distinct
			import_file_source_file_name     as file_name
			,import_file_source_file_sha256  as sha256
			,import_file_id 				 as id
			, count(*) 						 as TEDL_cnt
			from person per
				inner join test_eval_summary_log tesl
					on per.person_id = tesl.person_id
				inner join test_eval_detail_log tedl
					on tesl.test_eval_summary_log_id = tedl.test_eval_summary_log_id
				inner join import_file impf
					on tedl.test_eval_detail_import_file_id = impf.import_file_id
			group by import_file_id";
		$cmd = Yii::app()->db->createCommand($query);
		$rsa = $cmd->queryAll($fetchAssociative=true);
		return $rsa;
	}


	/**
	 * Returns a list of unprocessed CSV special report files
	 * @param int $data_hdr_xlation_id
	 * @return array
	 * @example $data_hdr_xlation_id=7 Special Report
	 * @internal Development Status = Golden!
	 * @internal called by: self::import()
	 * @version 0.1.0
	 * @todo must prevent duplicate file processing
	 *  at what level?
	 */
	protected static function filterDataFilesByXlatTypeID($data_hdr_xlation_id) {

		$csv_files			= self::fetch_csv_files();

		// remove files already processed?
		$prev_files = self::getFileHashesFromDB();
		$TEDLcnts=[]; // hold cnts from muliple matches
		foreach ($csv_files as $csv_idx => $file_uri){
			$sha256    = hash_file("sha256", $file_uri);
			$found     = array_search($sha256, $prev_files );

			$TEDLcnt	= 0;
			$TEDLcntSum = 0;
			foreach ($prev_files as $file) {
				if ($file['sha256'] == $sha256) {
					//$TEDLcnt 		= (int)$file['TEDL_cnt'] ;
					$TEDLcntRaw		= (int)$file['TEDL_cnt'] ;
					$TEDLcntSum		+= (int)$file['TEDL_cnt'] ;
					$TEDLcnts[]		= $TEDLcntRaw;
					//$pf_matches[]	= $file;
				}
			}
			//if ($found !== false ){
			if ($TEDLcntSum > 0 ){

				// verify the TEDL cnt, remove files that have been processed before
				$TEDLcnt = $prev_files[$found]['TEDL_cnt'];
				if ($TEDLcntSum > 0){
					// the file has been processed before so skip it
					$msg = '<div class="well"><div class="panel-warning"><p style="font-family:sans;font-size:14px;">The GSM import skipped processing of: <b>'.basename($file_uri)
							. '</b> because it has <b>'.$TEDLcntSum . '</b> rows of data in the database already.'
							.' <span style="color:black;">Please delete this file from the import queue.</span></p>'
							.'<p class="hint" style="color:grey;">hint: Click the red "Delete All" button above to remove this file from the import queue.</p>'
							. ' <p class="hint" style="color:grey;"><b>Note</b>: <span>This file will not be imported by the system unless the prior import of this file is deleted.</span></p>'
							.'</div><!-- end of warning --></div><!-- end of well -->';
					self::ah($msg,'import status');
					unset($csv_files[$csv_idx]);
					continue;
				}
			}
		}

		$files_with_hdr_id	= self::add_file_hdr_type_to_csv_file_list($csv_files);
		if (self::$_debug_local == true){
			echo "<br>Dumping file header types in " .__METHOD__. "<br>";
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($files_with_hdr_id, 10, true);
		}

		$fileProcessedCnt = 0;
		$spec_report_files=[];
		foreach ($files_with_hdr_id as $file_with_hdr_id){
			if (self::$_debug_local == true){
				echo "<br>Dumping \$file_with_hdr_id in forloop in ". __METHOD__ ." ln:".__LINE__."<br>";
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($file_with_hdr_id, 10, true);
			}
			if ((int)$file_with_hdr_id['data_hdr_xlation_id'] != $data_hdr_xlation_id){ // 7=special report
				if (self::$_debug_local == true){
					echo "<br><b><span style=''>Skipping file</span></b> because it's not a special report file<br>";
					YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($file_with_hdr_id, 10, true);
				}
				continue; // only process special report files
			}
			$spec_report_files[] = $file_with_hdr_id;
			$fileProcessedCnt++;
		}

		return $spec_report_files;

	}

	/**
	 * Generate a psuedo key for readonly testing
	 * @param string $tableName?
	 * @param string $table_key?
	 * @version 0.1.0
	 * @internal Development Status = ready for testing (rft)
	 */
	protected static function getPsuedoKey($table_name) {
		//$debug_local = false;
		if(isset(self::$psuedoKeys[$table_name])){

			self::$psuedoKeys[$table_name]++;

			return self::$psuedoKeys[$table_name];
		} else {
			self::$psuedoKeys[$table_name] = 1;
			return self::$psuedoKeys[$table_name];
		}
	}

	/**
	 * Adds to the static htmlOutput buffer
	 * @param type $topic
	 * @return string $html
	 * @version CEC 0.2.0
	 */
	protected static function ah($html,$topic=null) {
		if (!is_null ($topic ) ){
			self::$_htmlOutputBuffer[$topic] = $html;
		} else {
			self::$_htmlOutputBuffer[] = $html;
		}
	}

	/**
	 * Output use cases?
	 *
	 * @param type $topic
	 * @return string $html
	 * @version CEC 0.2.0 - CASL Applicabable
	 * @internal use cases: 'console window testing'
	 * @internal 'console window testing' v1 show array dumps
	 * @internal 'console window testing' v2 show grid dumps
	 * @todo note link values in blue and purple eg pkey=purple,fkey=blue,data=black
	 */
	protected static function htmlOut($topic=null) {
		if (!is_null ($topic ) && isset(self::$_htmlOutputBuffer[$topic]) ){
			return implode('',self::$_htmlOutputBuffer[$topic]);
		} else {
			return implode('',self::$_htmlOutputBuffer);
		}
	}

	/**
	 * Output use cases?
	 *
	 * @param type $topic
	 * @return string $html
	 * @version CEC 0.2.0 - CASL Applicabable
	 * @internal use cases: 'console window testing'
	 * @internal 'console window testing' v1 show array dumps
	 * @internal 'console window testing' v2 show grid dumps
	 * @todo note link values in blue and purple eg pkey=purple,fkey=blue,data=black
	 */
	protected static function htmlTestHarnessTemplate($topic=null) {
		$styles=[
			'table_name_in_header_h1'=>['style'=>'color:green;',],
			'table_name_in_header_h2'=>['style'=>'color:green;',],
			'field_name_in_body_p1'=>['style'=>'color:black;',],
			'field_value_in_body_p1'=>['style'=>'color:black;',],
			'field_name_in_body_p2'=>['style'=>'color:black;',],
			'field_value_in_body_p2'=>['style'=>'color:black;',],
			'field_name_in_body_pkey'=>['style'=>'color:purple;',],
			'field_value_in_body_pkey'=>['style'=>'color:purple;',],
			'field_name_in_body_fkey'=>['style'=>'color:blue;',],
			'field_value_in_body_fkey'=>['style'=>'color:blue;',],
		];
		if (!is_null ($topic ) && isset(self::$_htmlOutputBuffer[$topic]) ){
			return implode('',self::$_htmlOutputBuffer[$topic]);
		} else {
			return implode('',self::$_htmlOutputBuffer);
		}
	}

	/**
	 * Returns operations status to the caller
	 * @param array $info
	 * files processed?
	 * @internal Development Status = ready for testing (rft)
	 */
	protected static function msgToCaller($info=null) {

//		$csv_file_basename = Bfile::base_name($file["file_uri"]);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($imported_data_summary, 10, true);
//		$fileProcessedCnt++;
//		$imported_files_summary[$csv_file_basename] = $imported_data_summary;


//		$htmlOut = $imported_data_summary;
		$htmlOut = self::htmlOut();
		if (isset($info) ){

			$returnData = [
				'data'=>[
					'success'	=>true,
					'message'	=>count($info) .' files processed',
					'html'		=>['special_report_html'=>$htmlOut],
				],
			];
		} else {
			//$htmlOut = self::htmlOut();
			$returnData = [
				'data'=>[
					'success'	=>true,
					'message'	=>'no files were processed',
					'html'		=>( (isset($htmlOut)) ? ['special_report_html'=>$htmlOut] :'No files were processed.'),
				],
			];
		}
		$requestIsAjax = Yii::app()->request->isAjaxRequest;

		$rules = ['developer_level_verbosity'=>true];
		$local_debug = false;
		if( $requestIsAjax == true ) {
			// @todo: return import status
			if($local_debug == true){
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($returnData, 10, true);
			}
			Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($returnData);
		}
		else {
			// not an Ajax request, but debug is turned on
			if( isset($rules['developer_level_verbosity']) ) {
				$developer_level_verbosity = $rules['developer_level_verbosity'];
			}
			if($developer_level_verbosity == true){
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($returnData, 10, true);
			}
		}

	}


	/**
	 * Move a file from one folder to another.
	 * @param string $file_name
	 * @return boolean Returns true if archived successfully, otherwise false
	 * @todo add this function to an application component so it can be accessed by any controller or model
	 * @internal This function has been copied to the to the Bfile component: protected/components/functions/Bfile.php
	 */
	protected static function archive_import_file($file_name)
	{
		//@todo: acquire from config
		$add_datetime_suffix_during_file_archival_yn = "Y";
		//$add_datetime_suffix_during_file_archival_yn = "N";

		$date_suffix = "";
		if ($add_datetime_suffix_during_file_archival_yn === 'Y'){
			$date_suffix = Bfile::date_to_filename();
			$file_name_original = $file_name;
			$target_file_name = pathinfo($file_name, PATHINFO_FILENAME)
					. '__' . $date_suffix
					. '.'  . pathinfo($file_name, PATHINFO_EXTENSION);
		} else {
			$target_file_name = $file_name;
		}

		// If URI passed in then use it
		$path_passed_in = pathinfo($file_name, PATHINFO_DIRNAME);
		if (strlen($path_passed_in) >0){
			$source_path = Bfile::add_trailing_slash($path_passed_in);
			$file_name_original = $file_name;
			$file_name = pathinfo($file_name, PATHINFO_BASENAME);
			$msg = "File URI passed to ($file_name) " . __METHOD__;
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
		} else {
			$source_path  = self::get__import_path('to_process');
		}

		$archive_path = self::get__import_path('archive');
		YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace("source path: $source_path");
		YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace("archive path: $archive_path");
		$cfile = Yii::app()->file->set($source_path . $file_name, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cfile, 10, true);
		// @todo: document why the file is moved twice
		$movedFile = $cfile->move($archive_path . $file_name);
		$movedTarget = $cfile->move($archive_path . $target_file_name);
		//CVarDumper::dump($cfile->move($source_path . $archive_path);
		if ($movedFile){
			$msg = "import file: $file_name archived in $archive_path";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			return true;
		} else {
			$msg = "import file: $file_name was NOT archived in $archive_path";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			return false;
		}
	}



} // end of BImportSpecialReport component


