<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: Barray::array_shift_examples();
 */
// <editor-fold defaultstate="collapsed" desc="array class">
// class arr{
namespace yii\components\barray;
//use yii\base\Component;
use yii\base\BaseObject;

class barray extends BaseObject
{
    public $prop1;
    public $prop2;
    
    
    public function __construct($param1, $param2, $config = [])
    {
        // ... initialization before configuration is applied

        parent::__construct($config);
    }    
    
    public function init()
    {
        parent::init();

        // ... initialization after configuration is applied
    }

        
	/**
	 *
	 */
	public static function array_shift_examples(){

		// Here's how to get the first key, the last key, the first value or
		// the last value of a (hash) array without explicitly copying or
		// altering the original array:

		$array = array('first'=>'111', 'second'=>'222', 'third'=>'333');

		// get the first key: returns 'first'
		print array_shift(array_keys($array));

		// get the last key: returns 'third'
		print array_pop(array_keys($array));

		// get the first value: returns '111'
		print array_shift(array_values($array));

		// get the last value: returns '333'
		print array_pop(array_values($array));
	}

	/**
	 * Build an option group array for a Select or Select2 webform control
	 * @param array $array An associative array returned from a CDbCommand->queryAll()
	 * @param string $valueField An array key
	 * @param string $textField An array key
	 * @param string $groupField An array key
	 * @return array[] An array that can be bound to a control
	 * @example $list=flatArrayToSelectOptgroupList(
	 * @example			Yii::app()->db->createCommand($sql)
	 * @example				->queryAll($associative=true), $valueField, $textField, $groupField);
	 * @example Barray::flatArrayToSelectOptgroupList()
	 */
	public static function flatArrayToSelectOptgroupList(array $array, $valueField, $textField, $groupField) {
		$debug = false;
		//$debug = true;
		$listData	= [];
		// get option groups
		$groups		= [];
		foreach($array as $row){
			$groups[$row[$groupField]] = 0; // get group names
		}
		$groupList = array_keys($groups);

		// create group stub template
		$groupStubTemplate = [
			'id'=> '',
			'text'=> '{{group_name}}',
			'children'=> [0=>0]
		];
		$groupStubTemplateJSON = CJSON::encode($groupStubTemplate);

		// capture details and build output array
		foreach ($groupList as $group_name) {
			$children = [];
			foreach ($array as $row) {
				if($row[$groupField] == $group_name){
					$children[] = ['id'=>$row[$valueField], 'text'=>$row[$textField]];
				}
			}
			$stub = json_decode(
						str_ireplace('{{group_name}}', $group_name, $groupStubTemplateJSON),
						$assoc=true
					);
			$stub['children'] = $children;
			$listData[] = $stub;
		}

		if ($debug){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($groupList, 10, true);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($listData, 10, true);
		}

		return $listData;
	}

	/**
	 *
	 */
	public static function flatArrayToSelectOptgroupList__test_harnes() {

		$array=[
			['org_id'=>1,'org_name'=>'someorg11','org_type_name'=>'Club'],
			['org_id'=>2,'org_name'=>'someorg21','org_type_name'=>'Club'],
			['org_id'=>3,'org_name'=>'someorg31','org_type_name'=>'Club'],
			['org_id'=>4,'org_name'=>'someorg41','org_type_name'=>'Association'],
			['org_id'=>5,'org_name'=>'someorg51','org_type_name'=>'Association'],
			['org_id'=>6,'org_name'=>'someorg61','org_type_name'=>'Association'],
			['org_id'=>7,'org_name'=>'someorg71','org_type_name'=>'School'],
			['org_id'=>8,'org_name'=>'someorg81','org_type_name'=>'School'],
			['org_id'=>9,'org_name'=>'someorg91','org_type_name'=>'School'],
		];

		$rtn = $this->arrayFlatToSelectOptgroupList(
				$array,
				$valueField='org_id',
				$textField='org_name',
				$groupField='org_type_name'
		);

		unset($textField, $groupField);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rtn, 10, true);
	}

	/**
	 *
	 */
	public static function arrayMergeRecursive_NoInsertsOnLeft_testHarness(){

		$defaults = [
			'id'            => 123456,
			'client_id'     => null,
			'client_secret' => null,
			'options'       => [
				'trusted' => false,
				'active'  => false
			]
		];

		$options = [
			'client_id'       => 789,
			'client_secret'   => '5ebe2294ecd0e0f08eab7690d2a6ee69',
			'client_password' => '5f4dcc3b5aa765d61d8327deb882cf99', // ignored
			'client_name'     => 'IGNORED',                          // ignored
			'options'         => [
				'active' => true
			]
		];

		//$out = self::arrayMergeByKeys_recursive_NoKeyInsertsOnLeft($defaults, $options);

		//$out = self::array_merge_default($defaults, $options);

		//$out = self::array_merge_recursive_distinct($defaults, $options);
		//$out = self::array_merge_recursive_distinct($options, $defaults);

		$out = self::array_merge_recursive_leftsource($options, $defaults);
		$out = self::array_merge_recursive_leftsource($defaults, $options);

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($out, 10, true);
	}

	/**
	 * Merge a base array (master) with values from a second array ONLY WHEN the keys
	 * in the left side base array are matched in the second array.
	 * Works somewhat like a left outer join where values on the right-side
	 * will update a matching key on the left. Each matching key on the left will
	 * recieve values from the right (second array).
	 *
	 * If a key does not match then no key insert is allowed into the left
	 * side array from the right side
	 *
	 * Another name for this would be an array_join_by_key_no_insert()
	 * @param array $defaults The 'left-side' array. Has fixed immutable keys
	 * @param array $options The 'right-side' array. Only values with a
	 * matching key on the left-side are set on the left side. No keys are inserted
	 * into the left side array.
	 * @internal Development Status = code construction (testing failed on the current designs)
	 */
	public static function arrayMergeByKeys_recursive_NoKeyInsertsOnLeft(array $defaults, array $options){
		// Here is a better way to merge settings using some defaults as a whitelist.

		//	$defaults = [
		//		'id'            => 123456,
		//		'client_id'     => null,
		//		'client_secret' => null,
		//		'options'       => [
		//			'trusted' => false,
		//			'active'  => false
		//		]
		//	];
		//
		//	$options = [
		//		'client_id'       => 789,
		//		'client_secret'   => '5ebe2294ecd0e0f08eab7690d2a6ee69',
		//		'client_password' => '5f4dcc3b5aa765d61d8327deb882cf99', // ignored
		//		'client_name'     => 'IGNORED',                          // ignored
		//		'options'         => [
		//			'active' => true
		//		]
		//	];

		//	var_dump(
		//		array_merge_recursive($defaults,
		//			array_intersect_key(
		//				$options, $defaults
		//			)
		//		)
		//	);

		//	Output:
		//
		//	array (size=4)
		//		'id'            => int 123456
		//		'client_id'     => int 789
		//		'client_secret' => string '5ebe2294ecd0e0f08eab7690d2a6ee69' (length=32)
		//		'options'       =>
		//			array (size=2)
		//				'trusted' => boolean false
		//				'active'  => boolean true

//		$intersectedKeys1 = array_intersect_key($options, $defaults);
//		$intersectedKeys = array_intersect_key($defaults, $options);
//
//		$out1 = array_merge_recursive($defaults, $intersectedKeys);
//
//		$out = array_intersect_key($options, $defaults) + $defaults;

		return array_merge_recursive($defaults,
					array_intersect_key(
						$options, $defaults
					)
				);


//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump(array_merge_recursive($defaults,
//					array_intersect_key(
//						$options, $defaults
//					)
//				), 10, true);

		return $out;

	}

	/**
	 *
	 * @param type $default
	 * @param type $data
	 * @return array
	 * @internal Development Status = ready for testing (rft)	
	 */
	public static function array_merge_default($default, $data) {
        $intersect = array_intersect_key($data, $default); //Get data for which a default exists
        $diff = array_diff_key($default, $data); //Get defaults which are not present in data
        return $diff + $intersect; //Arrays have different keys, return the union of the two
	}

	/**
	 * @internal Development Status = ready for testing (rft)
	 * @return array
	 */
	public static function &array_merge_recursive_distinct(){
		$aArrays = func_get_args();
		$aMerged = $aArrays[0];

		for($i = 1; $i < count($aArrays); $i++)
		{
			if (is_array($aArrays[$i]))
			{
				foreach ($aArrays[$i] as $key => $val)
				{
					if (is_array($aArrays[$i][$key]))
					{
						$aMerged[$key] = is_array($aMerged[$key]) ? self::array_merge_recursive_distinct($aMerged[$key], $aArrays[$i][$key]) : $aArrays[$i][$key];
					}
					else
					{
						$aMerged[$key] = $val;
					}
				}
			}
		}

		return $aMerged;
	}


	/**
	 * Merges two arrays, and returns the resulting array. The base array is
	 * the left one ($a1), and if a key is set in both arrays, the right value
	 * has precedence. If a value in the left one is an array and also
	 * an array in the right one, the function calls itself (recursion).
	 *
	 * If the left one is an array and the right one exists but is not an
	 * array, then the right non-array-value will be used.
	 *
	 * *Any key only appearing in the right one ($a2) will be ignored*
	 *
	 * @param array $a1
	 * @param array $a2
	 * @return array $newArray
	 *
	 * @version 0.1.0
	 * @package Athlete Resumes
	 *
	 * @internal Development Status = Golden!
	 *
	 */
	public static function array_merge_recursive_leftsource(&$a1, &$a2) {
		$newArray = array();
		foreach ($a1 as $key => $v) {
			if (!isset($a2[$key])) {
				$newArray[$key] = $v;
				continue;
			}

			if (is_array($v)) {
				if (!is_array($a2[$key])) {
					$newArray[$key] = $a2[$key];
					continue;
				}
				$newArray[$key] = self::array_merge_recursive_leftsource($a1[$key], $a2[$key]);
				continue;
			}

			$newArray[$key] = $a2[$key];
		}
		return $newArray;
	}


	/**
	 *  Generate a formatted list with all globals
	 */
    public static function listGlobalArrayContent(){
        // Generate a formatted list with all globals
        //----------------------------------------------------
        // Custom global variable $_CUSTOM
        $_CUSTOM = array('USERNAME' => 'john', 'USERID' => '18068416846');

        // List here whichever globals you want to print
        // This could be your own custom globals
        $globals = array(
            '$_SERVER' => $_SERVER,   '$_ENV' => $_ENV,
            '$_REQUEST' => $_REQUEST, '$_GET' => $_GET,
            '$_POST' => $_POST,       '$_COOKIE' => $_COOKIE,
            '$_FILES' => $_FILES,     '$_CUSTOM' => $_CUSTOM
        );
        $html = "
        <html>
            <style>
                <!-- // Adjust CSS formatting for your output  -->
                .left {
                    font-weight: 700;
                }
                .right {
                    font-weight: 700;
                    color: #009;
                }
                .arrayname {
                    font-weight: 700;
                    font-family:sans;
                    font-size:18px;
                }
                .key {
                    color: #d00;
                    font-style: italic;
                }
            </style>
            <body>
                <h1>Super Globals</h1>";

                foreach ($globals as $globalkey => $global) {
                    $html.= "<span class='arrayname'>$globalkey</span></br>";
                    foreach ($global as $key => $value) {
                        $html .= "<span class='left'>" . $globalkey
                                . "[<span class='key'>'"
                                . $key ."'</span>]</span> = <span class='right'>"
                                . $value . "</span><br />";
                    }
                }
                $html.= "</body></html>";

        return $html;
    }


	/**
	 * Reads a data file and converts it to a CSV file
	 * @param string $data A URI for a data file
	 * @param string $delimiter The character(s) that separate field values
	 * @param string $enclosure The character that wraps string values
	 * @return string CSV formatted data
	 */
    public static function toCSV($data, $delimiter = ',', $enclosure = '"') {
        //generateCsv($data, $delimiter = ',', $enclosure = '"')
       $contents = "";
       $handle = fopen('php://temp', 'r+');
       foreach ($data as $line) {
               fputcsv($handle, $line, $delimiter, $enclosure);
       }
       rewind($handle);
       while (!feof($handle)) {
               $contents .= fread($handle, 8192);
       }
       fclose($handle);
       return $contents;
    }


	/**
	 * If there is at least one string key, $array will be regarded as associative array.
	 * it will treat "1" as an associative key, not a numeric key.
	 * @param array $array
	 * @return bool $is_assoc
	 * @see http://stackoverflow.com/questions/173400/how-to-check-if-php-array-is-associative-or-sequential
	 * @internal orginal base code from Captain kurO via StackOverflow
	 */
    public static function is_assoc_v0(array $array) {
		if (! is_array($array)){
			return false;
		}
        // Captain kurO via StackOverflow
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }


	/**
	 *
	 * @param array $array
	 * @return type
	 */
	public static function isAssoc_v1(array $array)
	{
		$arrayKeys  = array_keys($array);
		$compare = ( $arrayKeys !== array_keys($arrayKeys) );
		return $compare;
	}


	/**
	 *
	 * @param array $array
	 * @return boolean
	 * @example Barray::is_assoc($array);
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function is_assoc(array $array)
	{
		if (! is_array($array)){
			return false;
		}

	   $arrayKeys            = array_keys($array);
	   $arrayKeysOfArrayKeys = array_keys($arrayKeys);

	   $keyDiff = array_diff_key($arrayKeys, $arrayKeysOfArrayKeys);
	   if (count($keyDiff == 0) && count($arrayKeys) > 0){
		   $isAssoc = false;
	   }
	   elseif (count($keyDiff) == 0 && count($arrayKeys) == 0){
		   $isAssoc = false;
	   } else {
		   $isAssoc = true;
	   }

	   if ($array != array_keys($arrayKeys)){
		   $isAssocOld = true;
	   } else {
		   $isAssocOld = false;
	   }
	   return $isAssoc;
	   //return ($array != array_keys($arrayKeys));
	}


	/**
	 * An an extremely thorough check (foreach typecast)
	 * Avoid using this on large datasets
	 * @param array $array
	 * @return boolean
	 * @example Barray::is_assoc($array);
	 */
	public static function is_assoc_exhaustiveCheck(array $array){

		if (! is_array($array)){
			return false;
		}

		foreach($array as $key => $value) {
			if ($key !== (int) $key) {
				return true;
			}

		}
		unset($value);
		return false;
    }

	/**
	 * Determines if an array is associative (quick)
	 * @param array $array
	 * @return boolean
	 */
	function isAssocStr(array $array) {
		for (reset($array); is_int(key($array)); next($array)) {
			if ( is_null(key($array)) ){
				return false;
			} return true;
		}

	}

	/**
	 * Prepends $string to each element of $array
	 * @param array $array
	 * @param string $string
	 * @param bool $deep
	 * @return string
	 */
    public static function array_prepend($array, $string, $deep=false) {
       /*
        * Prepends $string to each element of $array
        * If $deep is true, will indeed also apply to sub-arrays
        * @author BigueNique AT yahoo DOT ca
        * @since 080324
        */
        if(empty($array)||empty($string)){
			return $array;
		}
        foreach($array as $key => $element){
            if(is_array($element)){
                if($deep){
                    $array[$key] = array_prepend($element,$string,$deep);
				} else {
                    trigger_error('array_prepend: array element',E_USER_WARNING);
				}
			} else {
                $array[$key] = $string.$element;
			}
		}
        return $array;
    }

	/**
	 * Returns an aligned heiarchical list as a string.
	 * @param array $arr An indexed or associative array
	 * @param int $level A depth of sub-arrays to process
	 * @return string
	 */
    public static function dumpAsString(array $arr, $level=null) {
        // Development Status = Golden with single depth arrays
        // Returns an aligned heiarchical list as a string.
        // 2013-09-06 0.0.0.1 ported this from javascrpt and modified it heavily.
        $dumped_text = "";
        if (empty($level)){
            $level = 0;
        }
        if(! $level){
			$level = 0;
		}

        //The padding given at the beginning of the line.
        //$level_padding = "";
        //$level_padding_len = strlen($level_padding);
        $delim_align_str = "";
        $key_len_max     = 0;
        foreach($arr as $key=>$val){
            $key_len = strlen($key);
            if ($key_len > $key_len_max){
                $key_len_max = $key_len;
            }
        }
        $level_padding = str_repeat("    ", $level + 1);
        //for($j=0;$j<$level+1;$j++) $level_padding .= "    ";
        $i = 0;
        foreach($arr as $key => $val){
            $i++;
            $key_len         = strlen($key);
            $delim_align_str = str_repeat(" ", ($key_len_max - $key_len));
            if (is_array(($val))){
                $dumped_text .= "$level_padding $key \n";

                //$dumped_text .= "\n";
                $dumped_text .= Barray::dumpAsString($val,$level+1);
                $dumped_text .= "\n";
            } else {
                $dumped_text .= $level_padding . "'" . $key . "'$delim_align_str => \"" . $val . "\"\n";
            }
        }
    //	if(typeof($arr) == 'object') { //Array/Hashes/Objects
    //		foreach($item as $arr) {
    //			$value = $arr[$item];
    //
    //			if(typeof($value) == 'object') { //If it is an array,
    //				$dumped_text += $level_padding + "'" + $item + "' ...\n";
    //				$dumped_text += $dump($value,$level+1);
    //			} else {
    //				$dumped_text += $level_padding + "'" + $item + "' => \"" + $value + "\"\n";
    //			}
    //		}
    //	} else { //Stings/Chars/Numbers etc.
    //		$dumped_text = "===>"+$arr+"<===("+typeof($arr)+")";
    //	}
            return $dumped_text;
    }

//    public static function array_to_csv($array){
//        // dbyrd - this function uses hard coded single quotes as delimiters
//        // get keys
//        $head_row = $array[0];
//        $csv_head = "";
//        $i=0;
//        foreach($head_row as $key=>$val){
//            $csv_head .= "'" .$key. "', ";
//        }
//        $csv_head = substr($csv_head, 0 ,strlen($csv_head) -2) . "\n";
//
//        $csv_body = "";
//
//        foreach($array as $row){
//            $csv_row  = "";
//            foreach($row as $field_name=>$field_value){
//                $csv_row .= "'" .mysql_real_escape_string($field_value). "', ";
//            }
//            $csv_row   = substr($csv_row, 0 ,strlen($csv_row) -2) . "\n";
//            $csv_body .= $csv_row;
//        }
//        return $csv_head . $csv_body;
//    }

    public static function assoc_to_csv($array){
        // dbyrd - this function uses hard coded dbouble quotes as delimiters
        // get keys
        $head_row = $array[0];
        $csv_head = "";
        $i=0;
        foreach($head_row as $key=>$val){
            $csv_head .= '"' .$key. '", ';
        }
		unset ($key, $val);
        $csv_head = substr($csv_head, 0 ,strlen($csv_head) -2) . "\n";

        $csv_body = "";

        foreach($array as $row){
            $csv_row  = "";
            foreach($row as $field_name=>$field_value){
                $csv_row .= '"' .mysql_real_escape_string($field_value). '", ';
            }
			unset($field_name, $field_value);
            $csv_row   = substr($csv_row, 0 ,strlen($csv_row) -2) . "\n";
            $csv_body .= $csv_row;
        }
		unset ($row);
        return $csv_head . $csv_body;
    }

	/**
	 * Formats a line (passed as a fields  array) as CSV and returns the CSV as a string.
	 * @internal Adapted from http://us3.php.net/manual/en/function.fputcsv.php#87120
	 */
    public static function arrayFieldValuesLineToCsv( array &$fields, $delimiter = ';', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false ) {
        /**
          * Formats a line (passed as a fields  array) as CSV and returns the CSV as a string.
          * Adapted from http://us3.php.net/manual/en/function.fputcsv.php#87120
          */
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        foreach ( $fields as $field ) {
            if ($field === null && $nullToMysqlNull) {
                $output[] = 'NULL';
                continue;
            }

            // Enclose fields containing $delimiter, $enclosure or whitespace
            if ( $encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
                $output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
            }
            else {
                $output[] = $field;
            }
        }
        unset($field);
        return implode( $delimiter, $output );
    }


	/**
	 * Used to download database data to a csv file to import into a spreadsheet
	 * @param array  $data           Could be session or any array
	 * @param string $arrayKey       The key used to extract data from $arrayWithData
	 * @param string $csv_name       The output file name uri
	 * @param bool   $enableDownload true=download the csv file after done
	 */
	function createCSV(array $data, $arrayKey, $csv_name, $enableDownload=true) {

        $i = 1;
        $csv = "";

        // erase the old file, if it exists
		// @todo replace this with CFile reference
        @unlink("../../csv/" . $csv_name . ".csv");

        // array is in a session variable
        // this may be useful to avoid many db queries if it is the case /
        //$tmpArray = $_SESSION['csvOut'];
		$tmpArray = $data[$arrayKey];

        // how many fields has the given array
        $fields = count(array_keys($tmpArray[0]));

        // extracting the titles from the array
        foreach(array_keys($tmpArray[0]) as $title)
        {
            // array_keys percurs the title of each vector
            $csv .= $title;

            // while it is not the last field put a semi-colon ;
            if($i < $fields){
                $csv .= ";";
			}
            $i++;
        }

        // insert an empty line to better visualize the csv
        //$csv .= chr(10).chr(13);
        //$csv .= chr(10).chr(13);

        // get the values from the extracted keys
        foreach (array_keys($tmpArray) as $tipo)
        {
            $i = 1;

            foreach(array_keys($tmpArray[$tipo]) as $sub)
            {

                $csv .= $tmpArray[$tipo][$sub];

                if ($i < $fields){
                    $csv .= ";";
				}
                $i++;
            }

            //$csv .= chr(10).chr(13);
			$csv .= "/n";

        }

        // export the csv
		$csvFileUri = "../../csv/". $csv_name .".csv";
		Bfile::write_to_file($csvFileUri, $csv, $writeMode='w+');
		unset($writeMode);

        // download the csv //
        if ($enableDownload === true){
            header('Location:' . $csvFileUri);
		}
        //exit();
    }


	/**
	 * Used to search a multi-dimensional array for a value.
	 * Works with object properties as well.
	 * @param type $elem
	 * @param array|object $array
	 * @return boolean
	 * @author cousinka at gmail dot com
	 * @internal source code: ource Code: http://php.net/manual/en/function.in-array.php
	 */
    public static function in_multiarray($elem, $array){
        //
        // S
        //
        // if the $array is an array or is an object
         if( is_array( $array ) || is_object( $array ) )
         {
             // if $elem is in $array object
             if( is_object( $array ) )
             {
                 $temp_array = get_object_vars( $array );
                 if( in_array( $elem, $temp_array ) ){
                     return TRUE;
				 }
             }

             // if $elem is in $array return true
             if( is_array( $array ) && in_array( $elem, $array ) ){
                 return TRUE;
			 }

             // if $elem isn't in $array, then check foreach element
             foreach( $array as $array_element )
             {
                 // if $array_element is an array or is an object call the in_multiarray function to this element
                 // if in_multiarray returns TRUE, than return is in array, else check next element
                 if( ( is_array( $array_element ) || is_object( $array_element ) ) && in_multiarray( $elem, $array_element ) )
                 {
                     return TRUE;
                     //exit;
                 }
             }
         }

         // if isn't in array return FALSE
         return FALSE;
    }

	/**
	 * This is a vestigal routine from bcommon.php that is being sunset
	 * @param type $arr
	 * @param type $width
	 * @param int $height
	 * @param type $row_limit
	 * @deprecated since version 0.54.3
	 */
    public static function array_to_html_table($arr,$width,$height=null,$row_limit=null){
        //echo_array($arr);
        $count = count($arr);

        if($count > 0){
           reset($arr);
           if (empty($height)){
               $height = 350;
           }
           if (empty($row_limit)){
               $row_limit = sizeof($arr);
           }
           $num = count(current($arr));
		   // <editor-fold defaultstate="collapsed" desc="html style">
		   echo "<style>\n"
                ."table.green_hover tr:hover{
                    background: rgb(180,227,145);
                    background: -moz-linear-gradient(top,  rgba(180,227,145,1) 0%, rgba(97,196,25,1) 50%, rgba(180,227,145,1) 100%);
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(180,227,145,1)), color-stop(50%,rgba(97,196,25,1)), color-stop(100%,rgba(180,227,145,1)));
                    background: -webkit-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: -o-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: -ms-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4e391', endColorstr='#b4e391',GradientType=0 );
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }\n
                .green_bg{
                    background: rgb(180,227,145);
                    background: -moz-linear-gradient(top,  rgba(180,227,145,1) 0%, rgba(97,196,25,1) 50%, rgba(180,227,145,1) 100%);
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(180,227,145,1)), color-stop(50%,rgba(97,196,25,1)), color-stop(100%,rgba(180,227,145,1)));
                    background: -webkit-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: -o-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: -ms-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4e391', endColorstr='#b4e391',GradientType=0 );
                }\n
                .shadow_deep{
                    -moz-box-shadow: 7px 7px 15px #ccc;
                    -webkit-box-shadow: 7px 7px 15px #ccc;
                    box-shadow: 7px 7px 15px #ccc;
                }\n
                .shadow{
                    -moz-box-shadow: 4px 4px 6px #ccc;
                    -webkit-box-shadow: 4px 4px 6px #ccc;
                    box-shadow: 4px 4px 6px #ccc;
                }\n
                div.border_green{
                    border-color: rgba(180,227,145,1);
                    border-style: solid;
                    border-width: 1px;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }\n
            .round_corners{
            border-color: rgba(180,227,145,1);
            border-style: solid;
            border-width: 1px;
            height: 22px;
            color: whitesmoke;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;

            padding-top: 2px;
            padding-right: 3px;
            padding-bottom: 0px;
            padding-left: 3px;

            padding-right: 6px;
            padding-left: 6px;

            font-family:sans;
            background: rgb(180,221,180);
            background: -moz-linear-gradient(top, rgba(180,221,180,1) 0%, rgba(131,199,131,1) 17%, rgba(82,177,82,1) 33%, rgba(0,138,0,1) 67%, rgba(0,87,0,1) 83%, rgba(0,36,0,1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(180,221,180,1)), color-stop(17%,rgba(131,199,131,1)), color-stop(33%,rgba(82,177,82,1)), color-stop(67%,rgba(0,138,0,1)), color-stop(83%,rgba(0,87,0,1)), color-stop(100%,rgba(0,36,0,1)));
            background: -webkit-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
            background: -o-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
            background: -ms-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4ddb4', endColorstr='#002400',GradientType=0 );
            background: linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
            }\n

 *          .gradient_black{
            color: darkgrey;
            background: rgb(125,126,125);
            background: -moz-linear-gradient(top, rgba(125,126,125,1) 0%, rgba(14,14,14,1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(125,126,125,1)), color-stop(100%,rgba(14,14,14,1)));
            background: -webkit-linear-gradient(top, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%);
            background: -o-linear-gradient(top, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%);
            background: -ms-linear-gradient(top, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%);
            background: linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#7d7e7d', endColorstr='#0e0e0e',GradientType=0 );
            text-shadow: 2px 2px 3px rgba(255,255,255,0.1);
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;  \n

            .grid_container{


                border-color: rgba(180,227,145,1);
                border-style: solid;
                border-width: 1px;
                color: whitesmoke;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;

                padding-top: 0px;
                padding-right: 3px;
                padding-bottom: 0px;
                padding-left: 3px;

                font-family:sans;
                background: rgb(180,221,180);
                background: -moz-linear-gradient(top, rgba(180,221,180,1) 0%, rgba(131,199,131,1) 17%, rgba(82,177,82,1) 33%, rgba(0,138,0,1) 67%, rgba(0,87,0,1) 83%, rgba(0,36,0,1) 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(180,221,180,1)), color-stop(17%,rgba(131,199,131,1)), color-stop(33%,rgba(82,177,82,1)), color-stop(67%,rgba(0,138,0,1)), color-stop(83%,rgba(0,87,0,1)), color-stop(100%,rgba(0,36,0,1)));
                background: -webkit-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
                background: -o-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
                background: -ms-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4ddb4', endColorstr='#002400',GradientType=0 );
                background: linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);

                background: #ffffff;
                background: -moz-linear-gradient(top,  #ffffff 0%, #e5e5e5 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5));
                background: -webkit-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
                background: -o-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
                background: -ms-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
                background: linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 );

            }\n
            .dark_bluegrey_gradient{
            color: rgb(206,220,231);
            color: -moz-linear-gradient(top,  rgba(206,220,231,1) 0%, rgba(89,106,114,1) 100%);
            color: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(206,220,231,1)), color-stop(100%,rgba(89,106,114,1)));
            color: -webkit-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%);
            color: -o-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%);
            color: -ms-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%);
            color: linear-gradient(to bottom,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cedce7', endColorstr='#596a72',GradientType=0 );

            }\n
            " ."</style>\n
                <div class=\"shadow border_green grid_container\" style=\"width:"
                   . $width. "px; overflow:auto; height:"
                   . ((int)$height + 35) ."px; left:35px; padding:10px; margin:25px; border-width:1px\">\n";

           echo "<p class=\"\" style=\"text-align:center; vertical-align:middle;
               color:grey;font-size:24px;font-family:sans;font-weight:700;
               padding:5px;margin:5px; text-align:center;text-shadow: 2px 2px 3px rgba(255,255,255,0.1);\">Fields to Skip During Bulk Updates</p>";

           echo "<div><table class=\"green_hover round_corners dark_bluegrey_gradient\" style=\"font-family:sans;height:93%;width:100%;\"
               align=\"left\" cellpadding=\"4\" cellspacing=\"0\" >\n";

			// </editor-fold>
			echo "<tr class=\"round_corners\">\n";
			// Loop through the header row by looping the the current row
			foreach(current($arr) as $key => $value){
				if ($key == "is_checked" || $value == "is_checked"){
					//echo "skipping is_checked header. key: $key, value: $value " . "<br>";
					continue;
				}
				echo "<th class=\"green_bg \" style=\"color:darkgreen;\">";
				echo $key."&nbsp;";
				echo "</th>\n";
			}

			echo "</tr>\n";
			$i=0;
			while ($curr_row = current($arr)) {
				echo "<tr>\n";
				$col = 1;
				$i++;
				while ($curr_field = current($curr_row)) {
					if (trim($curr_field) == "is_checked"){
						$num--;
						next($curr_row);
						$col++;
						continue;
					// } elseif($i >= $row_limit){   logic skips over the last row to process
					} elseif($i > $row_limit){
                       $num--;
                       next($curr_row);
                       $col++;
                       continue;
                   }
                   if ($col == 1){
						// look for a check box
						if (array_key_exists('is_checked',$curr_row) ){
							if($curr_row["is_checked"] == "is_checked"){
								echo "<td><input name=\"test\" type=\"checkbox\" value=\"$curr_field&nbsp;\" checked=\"true\"
									style=\"text-align:center; vertical-align:middle; \"/>\n</td>";
							} else {
								echo "<td><input name=\"test\" type=\"checkbox\" value=\"$curr_field&nbsp;\"
									style=\"text-align:center; vertical-align:middle; \"/>\n</td>";
							}
//					    } else {
//								echo "<td><input name=\"test\" type=\"text\" value=\"$curr_field&nbsp;\"
//									style=\"text-align:center; vertical-align:middle; \"/>\n";
					    }
                   } else {
                       //echo "<td>";
                   }
				   echo "<td>";
                   echo $curr_field."&nbsp;";
                   //echo "</td>\n";
                   if ($col == 1){
                       echo "</td>\n";
                   } else {
                       echo "</td>\n";
                   }
                   next($curr_row);
                   $col++;
               }
               while($col <= $num){
                   echo "<td>&nbsp;</td>\n";
                   $col++;
               }
               echo "</tr>\n";
               next($arr);
           }
           echo "</table></div>\n</div>\n";
        }
    }

	/**
	 * This is a vestigal routine from bcommon.php that is being sunset
	 * @param type $arr
	 * @param type $width
	 * @param int $height
	 * @param type $row_limit
	 * @param type $title
	 * @param array $cols_to_exclude
	 * @deprecated since version 0.54.3
	 */
    public static function array_to_html_table2($arr,$width=null,$height=null,$row_limit=null, $title=null,$cols_to_exclude=null){
        //echo_array($arr);
        $count = count($arr);

        if($count > 0){
           reset($arr);
           if (empty($height)){
               $height = 350;
           }
           if (empty($row_limit)){
               $row_limit = sizeof($arr);
           }
           $num = count(current($arr));
           // <editor-fold defaultstate="collapsed" desc="html style">
           echo "<style>\n"
                ."table.green_hover tr:hover{
                    background: rgb(180,227,145);
                    background: -moz-linear-gradient(top,  rgba(180,227,145,1) 0%, rgba(97,196,25,1) 50%, rgba(180,227,145,1) 100%);
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(180,227,145,1)), color-stop(50%,rgba(97,196,25,1)), color-stop(100%,rgba(180,227,145,1)));
                    background: -webkit-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: -o-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: -ms-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4e391', endColorstr='#b4e391',GradientType=0 );
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }\n

                .green_bg{

                    background: rgb(180,227,145);
                    background: -moz-linear-gradient(top,  rgba(180,227,145,1) 0%, rgba(97,196,25,1) 50%, rgba(180,227,145,1) 100%);
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(180,227,145,1)), color-stop(50%,rgba(97,196,25,1)), color-stop(100%,rgba(180,227,145,1)));
                    background: -webkit-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: -o-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: -ms-linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    background: linear-gradient(top,  rgba(180,227,145,1) 0%,rgba(97,196,25,1) 50%,rgba(180,227,145,1) 100%);
                    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4e391', endColorstr='#b4e391',GradientType=0 );
                }\n
                .shadow_deep{
                    -moz-box-shadow: 7px 7px 15px #ccc;
                    -webkit-box-shadow: 7px 7px 15px #ccc;
                    box-shadow: 7px 7px 15px #ccc;
                }\n
                .shadow{
                    -moz-box-shadow: 4px 4px 6px #ccc;
                    -webkit-box-shadow: 4px 4px 6px #ccc;
                    box-shadow: 4px 4px 6px #ccc;
                }\n
                div.border_green{
                    border-color: rgba(180,227,145,1);
                    border-style: solid;
                    border-width: 1px;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    border-radius: 5px;
                }\n
            .round_corners{
            border-color: rgba(180,227,145,1);
            border-style: solid;
            border-width: 1px;
            height: 22px;
            color: whitesmoke;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;

            padding-top: 2px;
            padding-right: 3px;
            padding-bottom: 0px;
            padding-left: 3px;

            padding-right: 6px;
            padding-left: 6px;

            font-family:sans;
            background: rgb(180,221,180);
            background: -moz-linear-gradient(top, rgba(180,221,180,1) 0%, rgba(131,199,131,1) 17%, rgba(82,177,82,1) 33%, rgba(0,138,0,1) 67%, rgba(0,87,0,1) 83%, rgba(0,36,0,1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(180,221,180,1)), color-stop(17%,rgba(131,199,131,1)), color-stop(33%,rgba(82,177,82,1)), color-stop(67%,rgba(0,138,0,1)), color-stop(83%,rgba(0,87,0,1)), color-stop(100%,rgba(0,36,0,1)));
            background: -webkit-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
            background: -o-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
            background: -ms-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4ddb4', endColorstr='#002400',GradientType=0 );
            background: linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
            }\n

            .gradient_black{
            color: darkgrey;
            background: rgb(125,126,125);
            background: -moz-linear-gradient(top, rgba(125,126,125,1) 0%, rgba(14,14,14,1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(125,126,125,1)), color-stop(100%,rgba(14,14,14,1)));
            background: -webkit-linear-gradient(top, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%);
            background: -o-linear-gradient(top, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%);
            background: -ms-linear-gradient(top, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%);
            background: linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#7d7e7d', endColorstr='#0e0e0e',GradientType=0 );

            text-shadow: 2px 2px 3px rgba(255,255,255,0.1);
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;  \n

            .grid_container{

                border-color: rgba(180,227,145,1);
                border-style: solid;
                border-width: 1px;
                color: whitesmoke;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;

                padding-top: 0px;
                padding-right: 3px;
                padding-bottom: 0px;
                padding-left: 3px;

                font-family:sans;
                background: rgb(180,221,180);
                background: -moz-linear-gradient(top, rgba(180,221,180,1) 0%, rgba(131,199,131,1) 17%, rgba(82,177,82,1) 33%, rgba(0,138,0,1) 67%, rgba(0,87,0,1) 83%, rgba(0,36,0,1) 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(180,221,180,1)), color-stop(17%,rgba(131,199,131,1)), color-stop(33%,rgba(82,177,82,1)), color-stop(67%,rgba(0,138,0,1)), color-stop(83%,rgba(0,87,0,1)), color-stop(100%,rgba(0,36,0,1)));
                background: -webkit-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
                background: -o-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
                background: -ms-linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4ddb4', endColorstr='#002400',GradientType=0 );
                background: linear-gradient(top, rgba(180,221,180,1) 0%,rgba(131,199,131,1) 17%,rgba(82,177,82,1) 33%,rgba(0,138,0,1) 67%,rgba(0,87,0,1) 83%,rgba(0,36,0,1) 100%);

                background: #ffffff;
                background: -moz-linear-gradient(top,  #ffffff 0%, #e5e5e5 100%);
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5));
                background: -webkit-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
                background: -o-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
                background: -ms-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
                background: linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 );

            }\n
            .dark_bluegrey_gradient{
    color: rgb(206,220,231);
    color: -moz-linear-gradient(top,  rgba(206,220,231,1) 0%, rgba(89,106,114,1) 100%);
    color: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(206,220,231,1)), color-stop(100%,rgba(89,106,114,1)));
    color: -webkit-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%);
    color: -o-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%);
    color: -ms-linear-gradient(top,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%);
    color: linear-gradient(to bottom,  rgba(206,220,231,1) 0%,rgba(89,106,114,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cedce7', endColorstr='#596a72',GradientType=0 );

            }\n
            " ."</style>\n
                <div class=\"shadow border_green grid_container\" style=\"width:"
                   . $width. "px; overflow:auto; height:"
                   . ((int)$height + 35) ."px; left:35px; padding:10px; margin:25px; border-width:1px\">\n";

           echo "<p class=\"\" style=\"text-align:center; vertical-align:middle;
               color:grey;font-size:24px;font-family:sans;font-weight:700;
               padding:5px;margin:5px; text-align:center;text-shadow: 2px 2px 3px rgba(255,255,255,0.1);\">$title</p>";

           echo "<div><table class=\"green_hover round_corners dark_bluegrey_gradient\" style=\"font-family:sans;height:93%;width:100%;\"
               align=\"left\" cellpadding=\"4\" cellspacing=\"0\" >\n";
           echo "<tr class=\"round_corners\">\n";
            // </editor-fold>
			//
		   //$cols_to_exclude = array();
		   if (is_null($cols_to_exclude)){
			   $cols_to_exclude = [];
		   }

           foreach(current($arr) as $key => $value){
               //@todo: filter out headers to execlude
               if (in_array($key, $cols_to_exclude)){
                   //echo "skipping header. key: $key, value: $value " . "<br>";
                   continue;
               }
               echo "<th class=\"green_bg \" style=\"color:darkgreen;\">";
               echo $key."&nbsp;";
               echo "</th>\n";
           }
           echo "</tr>\n";
           $i=0;
           while ($curr_row = current($arr)) {
               echo "<tr>\n";
               $col = 1;
               $i++;
               while ($curr_field = current($curr_row)) {
                   if (trim($curr_field) == "is_checked"){
                       $num--;
                       next($curr_row);
                       $col++;
                       continue;
                   } elseif($i >= $row_limit){
                       $num--;
                       next($curr_row);
                       $col++;
                       continue;
                   }
                   if ($col == 1){
//                       if($curr_row["is_checked"] == "is_checked"){
//                           echo "<td><input name=\"test\" type=\"checkbox\" value=\"$curr_field&nbsp;\" checked=\"true\"
//                               style=\"text-align:center; vertical-align:middle; \"/>\n";
//                       } else {
                           echo "<td><input name=\"test\" type=\"checkbox\" value=\"$curr_field&nbsp;\"
                               style=\"text-align:center; vertical-align:middle; \"/>\n";
//                       }
                   } else {
                       echo "<td>";
                   }
                   echo $curr_field."&nbsp;";
                   //echo "</td>\n";
                   if ($col == 1){
                       echo "</td>\n";
                   } else {
                       echo "</td>\n";
                   }
                   next($curr_row);
                   $col++;
               }
               while($col <= $num){
                   echo "<td>&nbsp;</td>\n";
                   $col++;
               }
               echo "</tr>\n";
               next($arr);
           }
           echo "</table></div>\n</div>\n";
        }
    }


	/**
	 * Used to test self::array_keys_recursive()
	 */
	public static function array_keys_recursive_testHarness() {
		$data=array(
			'Player' => array(
				'id' => '4',
				'state' => 'active',
			),
			'LevelSimulation' => array(
				'id' => '1',
				'simulation_id' => '1',
				'level_id' => '1',
				'Level' => array(
					'id' => '1',
					'city_id' => '8',
					'City' => array(
						'id' => '8',
						'class' => 'home',
					)
				)
			),
			'User' => array(
				'id' => '48',
				'gender' => 'M',
				'group' => 'user',
				'username' => 'Hello'
			)
		);

		//self::array_keys_recursive($data, $maxDepth, $depth, $arrayKeys);
		$out = self::array_keys_recursive($data);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($out, 10, true);

		// Data output is golden


		self::echo_array($out);
		$html = self::dumpAsString($out);
		echo $html . "<br />";
		//self::array_to_html_table($out, $width=800);
		self::array_to_html_table2($out, 800,600,25,'Array Keys Recursive');
	}

	/**
	 *
	 * @param array  $data
	 * @param int    $maxDepth
	 * @param int    $depth
	 * @param array  $arrayKeys
	 * @return array $arrayKeys
	 * @example
		input:
			array(
				'Player' => array(
					'id' => '4',
					'state' => 'active',
				),
				'LevelSimulation' => array(
					'id' => '1',
					'simulation_id' => '1',
					'level_id' => '1',
					'Level' => array(
						'id' => '1',
						'city_id' => '8',
						'City' => array(
							'id' => '8',
							'class' => 'home',
						)
					)
				),
				'User' => array(
					'id' => '48',
					'gender' => 'M',
					'group' => 'user',
					'username' => 'Hello'
				)
			)

		output:
			array(
				'Player' => array(),
				'LevelSimulation' => array(
					'Level' => array(
						'City' => array()
					)
				),
				'User' => array()
			)
	 *
	 */
	public static function array_keys_recursive(array $data, $maxDepth=99, $depth=0, $arrayKeys=[]){
        if($depth < $maxDepth){
            $depth++;
            $keys = array_keys($data);
            foreach($keys as $key){
                if(is_array($data[$key])){
                    $arrayKeys[$key] = self::array_keys_recursive($data[$key], $maxDepth, $depth);
                }
            }
        }

        return $arrayKeys;
    }


    /**
     *
     * @param array $array
     * @param string $encode_html_entities_yn
     * @see $this->dumpAsString()
     * @internal Development Status = Golden!
     */
    public static function echo_array(array $array, $encode_html_entities_yn=NULL){
        // Display easy-to-read valid PHP array code
        if ( empty($encode_html_entities_yn) ){
            $encode_html_entities_yn = "N";
        } else {
            // The field has a value so validate the parameter value passed in.
            if ( $encode_html_entities_yn !== "Y" && $encode_html_entities_yn !== "N"){
                $msg = "The correct values for \$encode_html_entities_yn is Y or N. Please try again.";
                error_Handler(__METHOD__, __LINE__, "<b>$msg</b><br />");
            }
        }

        if ( $encode_html_entities_yn == "Y") {
            echo '<pre>' . htmlentities(var_export( $array, true )) . '</pre>';
        } else {
            echo '<pre>' . var_export( $array, true ) . '</pre>';
        }
    }

    
    /**
     *
     * @param array $array
     * @param string $encode_html_entities_yn
     * @see $this->dumpAsString()
     * @internal Development Status = Golden!
     */
    public function echo_array2(array $array, $encode_html_entities_yn=NULL){
        // Display easy-to-read valid PHP array code
        if ( empty($encode_html_entities_yn) ){
            $encode_html_entities_yn = "N";
        } else {
            // The field has a value so validate the parameter value passed in.
            if ( $encode_html_entities_yn !== "Y" && $encode_html_entities_yn !== "N"){
                $msg = "The correct values for \$encode_html_entities_yn is Y or N. Please try again.";
                error_Handler(__METHOD__, __LINE__, "<b>$msg</b><br />");
            }
        }

        if ( $encode_html_entities_yn == "Y") {
            echo '<pre>' . htmlentities(var_export( $array, true )) . '</pre>';
        } else {
            echo '<pre>' . var_export( $array, true ) . '</pre>';
        }
    }    
        
        
	/**
	 * Sort a multi-dimensional array by associative key then by value
	 * @param array $array A multi-dimensional array to be sorted by associative key then by value
	 * @param bool $valrev Value sorted ASC by default eg false=ASC, true=DESC sort
	 * @param bool $keyrev Key sorted ASC by default eg false=ASC, true=DESC sort
	 * @see http://php.net/manual/en/function.asort.php
	 * @author Nick <nick@nickyost.com>
	 * @internal v0.1 name = aksort(&$array,$valrev=false,$keyrev=false) renamed on 2015-10-16
	 */
	public static function valAndKeySort(&$array,$valrev=false,$keyrev=false) {
		if ($valrev) { arsort($array); } else { asort($array); }
		$vals = array_count_values($array);
		$i = 0;
		foreach ($vals AS $val=>$num) {
			$first = array_splice($array,0,$i);
			$tmp = array_splice($array,0,$num);
			if ($keyrev) { krsort($tmp); } else { ksort($tmp); }
			$array = array_merge($first,$tmp,$array);
			unset($tmp);
			$i = $num;
		}
	}
}
// </editor-fold>




