<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: fn::CDbExpression('NOW()');
 */

class bfn extends CApplicationComponent
{
	public function init() {
	}
	
	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
	public static function CDbExpression($exp, $db='db') {
		if($exp == 'NOW()') {
			if (substr(Yii::app()->{$db}->connectionString, 0, 7) === 'sqlsrv:') {
				return new CDbExpression('GETDATE()');
			}
		}

		//otherwise
		return new CDbExpression($exp);
	}
	
}