<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: fn::CDbExpression('NOW()');
 */

class bcsv extends CApplicationComponent
{
	public function init() {
	}
	
	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
	public static function CDbExpression($exp, $db='db') {
		if($exp == 'NOW()') {
			if (substr(Yii::app()->{$db}->connectionString, 0, 7) === 'sqlsrv:') {
				return new CDbExpression('GETDATE()');
			}
		}

		//otherwise
		return new CDbExpression($exp);
	}
	
	
	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
	public static function importCsv($exp, $db='db') {
		// @todo: insert code from bc/tools/import.csv.file.php
		
		
		if($exp == 'NOW()') {
			if (substr(Yii::app()->{$db}->connectionString, 0, 7) === 'sqlsrv:') {
				return new CDbExpression('GETDATE()');
			}
		}

		//otherwise
		return new CDbExpression($exp);
	}	
	
}