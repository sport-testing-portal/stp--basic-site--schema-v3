<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: barray::CDbExpression('NOW()');
 */

class bnum extends CApplicationComponent
{
	public function init() {
	}

//	/**
//	 * handle NOW() function for ms-sql server
//	 * @param string $exp
//	 * @return string
//	 */
//	public static function CDbExpression($exp, $db='db') {
//		if($exp == 'NOW()') {
//			if (substr(Yii::app()->{$db}->connectionString, 0, 7) === 'sqlsrv:') {
//				return new CDbExpression('GETDATE()');
//			}
//		}
//
//		//otherwise
//		return new CDbExpression($exp);
//	}



	// <editor-fold defaultstate="collapsed" desc="numbers class">
	//class num{
	/**
	 * output easy to read numbers
	 * @param numeric $n
	 * @return string
	 */
    public static function nice_number($n) {
        //#    Output easy-to-read numbers
        //#    by james at bandit.co.nz

        // first strip any formatting;
        $n = (0+str_replace(",","",$n));

        // is this a number?
        if(!is_numeric($n)) return false;

        // now filter it;
        if($n>1000000000000) return round(($n/1000000000000),1).' trillion';
        else if($n>1000000000) return round(($n/1000000000),1).' billion';
        else if($n>1000000) return round(($n/1000000),1).' million';
        else if($n>1000) return round(($n/1000),1).' thousand';

        return number_format($n);
    }

    public static function comp($Num1,$Num2,$Scale=null) {
        // Base code: http://www.php.net/manual/en/function.bccomp.php#92357
        // Base code written by Nitrogen
        // I made this to compare an unlimited size of numbers..
        // This could be useful for those without the BCMath extension.
        // It allows decimals, and option $Scale parameter.  If $Scale isn't
        // specified, then it'll automatically adjust to the correct number
        // of decimals to compare.
        //
        // Purpose: Compare extremely large scale numbers using regular expressions

        // check if they're valid positive numbers, extract the whole numbers and decimals
        if(!preg_match("/^\+?(\d+)(\.\d+)?$/",$Num1,$Tmp1)||
           !preg_match("/^\+?(\d+)(\.\d+)?$/",$Num2,$Tmp2)) return('0');

        // remove leading zeroes from whole numbers
        $Num1=ltrim($Tmp1[1],'0');
        $Num2=ltrim($Tmp2[1],'0');

        // first, we can just check the lengths of the numbers, this can help save processing time
        // if $Num1 is longer than $Num2, return 1.. vice versa with the next step.
        if(strlen($Num1)>strlen($Num2)) return(1);
        else {
            if(strlen($Num1)<strlen($Num2)) return(-1);

          // if the two numbers are of equal length, we check digit-by-digit
          else {

                // remove ending zeroes from decimals and remove point
                $Dec1=isset($Tmp1[2])?rtrim(substr($Tmp1[2],1),'0'):'';
                $Dec2=isset($Tmp2[2])?rtrim(substr($Tmp2[2],1),'0'):'';

                // if the user defined $Scale, then make sure we use that only
                if($Scale!=null) {
                  $Dec1=substr($Dec1,0,$Scale);
                  $Dec2=substr($Dec2,0,$Scale);
                }

                // calculate the longest length of decimals
                $DLen=max(strlen($Dec1),strlen($Dec2));

                // append the padded decimals onto the end of the whole numbers
                $Num1.=str_pad($Dec1,$DLen,'0');
                $Num2.=str_pad($Dec2,$DLen,'0');

                // check digit-by-digit, if they have a difference, return 1 or -1 (greater/lower than)
                for($i=0;$i<strlen($Num1);$i++) {
                  if((int)$Num1{$i}>(int)$Num2{$i}) return(1);
                  else
                    if((int)$Num1{$i}<(int)$Num2{$i}) return(-1);
                }

                // if the two numbers have no difference (they're the same).. return 0
                return(0);
          }
        }
      }
}
// </editor-fold>



