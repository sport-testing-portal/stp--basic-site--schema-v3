<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: bdate::CDbExpression('NOW()');
 */

class bdate extends CApplicationComponent
{
	public function init() {
	}



// <editor-fold defaultstate="collapsed" desc="date class">
//class dt{

	/**
	 *
	 * @param type $start_dt
	 * @param type $end_dt
	 * @return type
	 */
    public static function age_in_days($start_dt, $end_dt=NOW){
        // returns int
        $sdate = strtotime($start_dt);
        $edate = strtotime($end_dt);
        $time_elapsed_in_seconds = $edate - $sdate;
        $day_cnt = ($edate - $sdate) / 86400;
        return (int)$day_cnt;
    }

	/**
	 *
	 * @param type $start_dt
	 * @param type $end_dt
	 * @return type
	 */
    public static function age_in_years($start_dt, $end_dt=NOW){
        // returns int
        $sdate = strtotime($start_dt);
        $edate = strtotime($end_dt);
        $yr_cnt = (int)(($edate - $sdate) / (86400 * 365));

        $age = date('Y', strtotime($end_dt)) - date('Y', strtotime($start_dt));
        if (date('md', strtotime($end_dt)) < date('md', strtotime($start_dt))) {
            $age--;
        }
        return (int)$age;
    }

	/**
	 *
	 * @param type $date
	 * @return type
	 */
    public static function age_in_years_old($date){
        // returns int
        $age = date('Y') - date('Y', strtotime($date));
        if (date('md') < date('md', strtotime($date))) {
            $age--;
        }
        return $age;
    }

	/**
	 *
	 * @param type $year
	 * @return type
	 */
    public static function isLeapYear($year) {
        return ((($year%4==0) && ($year%100)) || $year%400==0) ? (true):(false);
    }


	/**
	 * A microseconds date function
	 * @example echo udate('Y-m-d H:i:s.u T');
	 * @example // Will output something like: 2014-01-01 12:20:24.42342 CET
	 * @see http://php.net/manual/en/datetime.format.php
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function udate($format = 'u', $utimestamp = null) {
		// Seems like datetime::format does not really support microseconds as the documentation under date suggest it will.
		// Here is some code to generate a datetime with microseconds and timezone:
        if (is_null($utimestamp))
            $utimestamp = microtime(true);

        $timestamp = floor($utimestamp);
        $milliseconds = round(($utimestamp - $timestamp) * 1000000);

        return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }


	/**
	 * A milliseconds date function
	 * @return type
	 * @internal Development Status = Golden!
	 */
    public static function mTime(){
        //admin@travian-utils.com
        // http://php.net/manual/en/mysqli.next-result.php
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec+(float)$sec);
    }

	/**
	 * Returns a high precision time stamp as a string with all trailing zeros intact
	 * @param string|float $mtime
	 * @return string $dataReturn Example: 2011-08-09 12:05:03.7543
	 * @internal Development Status = Golden!
	 * @link http://php.net/manual/en/function.microtime.php
	 */
	public static function mtime_datetime_string($mtime=null){
        // Development Status = Golden!
        // returns a string 2011-08-09 12:05:03.7543
        if (empty($mtime)){
            //$mtime = mTime();
			$mtime = microtime(true);
        }
        $dateFormat = 'Y-m-d H:i:s';
        $date_str = date($dateFormat, $mtime);
        # if debugging include microseconds
        //$microseconds = $mtime - floor($mtime);

        $marray = explode(".", $mtime);
        //$date_array = getdate($mtime); // shows the date parts when debugging
        $decimal_points = 4;
        if (strlen($marray[1]) < $decimal_points) {
            // trailing zeros get dropped. Put them back!
            $resultBfrPad = (string)$marray[1];
            //echo $marray[1] . "<br />";
			$pad = '0'; // zero
			$length = $decimal_points;
            $m_result = Bcommon::str_rpad($resultBfrPad, $length,$pad);
            //echo "it's a short one!$marray[1] converted to $m_result<br />";
        } else {
            $m_result = $marray[1];
        }
        //$date_return = $date_str . ".$marray[1]";
        $date_return = $date_str . ".$m_result";
        //echo strlen($marray[1]) . "<br />";
        return $date_return;
    }

    public static function mtime_table_name($mtime=null){
        // Development Status = Golden!
        // returns a string 20130904_120503_7543
        if (empty($mtime)){
            $mtime = self::mTime();
        }
        $dateFormat = 'Y-m-d H:i:s';
        $date_str = date($dateFormat, $mtime);
        # if debugging include microseconds
        //$microseconds = $mtime - floor($mtime);

        $marray = explode(".", $mtime);
        //$date_array = getdate($mtime); // shows the date parts when debugging
        $decimal_points = 4;
        if (strlen($marray[1]) < $decimal_points) {
            // trailing zeros get dropped. Put them back!
            $m_result = (string)$marray[1];
            //echo $marray[1] . "<br />";
            $m_result = str_rpad($m_result, $l=$decimal_points,$pad="0");
            //echo "it's a short one!$marray[1] converted to $m_result<br />";
        } else {
            $m_result = $marray[1];
        }
        //$date_return = $date_str . ".$marray[1]";
        $date_return = $date_str . ".$m_result";
        $str_01      = str_ireplace("-",  "", $date_return);
        $str_02      = str_ireplace(":",  "", $str_01);
        $str_03      = str_ireplace(" ", "_", $str_02);
        $str_04      = str_ireplace(".", "_", $str_03);
        //echo strlen($marray[1]) . "<br />";
        return $str_04;
    }

    public static function timestamp_format(){
        return "Y-m-d H:i:s";
    }

	/**
	 * Converts a Unix date a mysql timestamp
	 * @param numeric Unix time
	 * @return string Current datetime in a mysql format 2014-09-01 00:00:00
	 * @category sql data integration
	 * @version 1.1
	 */
    public static function sql_dt($timeStamp=NULL) {
        // Returns the current datetime in a mysql format
        $dateFormat = 'Y-m-d H:i:s';
		if (empty($timeStamp)){
			$timeStamp = time();
		}
        $dateTime= date($dateFormat,$timeStamp);
        return $dateTime;
    }

	/**
	 * Creates a mysql timestamp using the current time
	 * @return string Current datetime in a mysql format 2014-09-01 00:00:00
	 * @category sql data integration
	 * @version 1.0
	 */
    public static function sql_dt__v1() {
        // Returns the current datetime in a mysql format
        $dateFormat = 'Y-m-d H:i:s';
        $timeStamp = time();
        $dateTime= date($dateFormat,$timeStamp);
        return $dateTime;
    }

    public static function sql_date_only() {
        $dateFormat = 'Y-m-d';
        $timeStamp = time();
        $dateTime = date($dateFormat,$timeStamp);
        return $dateTime;
    }

    public static function math($SourceDate,$doMath,$ReturnFmt=NULL){
        //
        // ~~ jm AT trinitywebdev DOT com ~~
        // *
        // * doMath Format[+/-][Qty][Unit]
        // *
        // * Example +6m   = Add 6 Months
        // * Example -180d = Subtract 180 Days
        // * Example +1Y   = Add 1 Year
        // * Example +2H   = Add 2 hours
        // * Example +57i  = Add 57 minutes
        // * Example +725s = Add 725 seconds
        // *
        // * Unit follows date() formats
        // * @return newday
        // *
        // Can only return a date string value. If hours, minutes, or seconds
        //  are specified via the ReturnFmt they will returned as zeros.
        // Notes: Dbyrd this function to to work with minutes, hours, and seconds.
        // History:
        // 2010-10-08 0.1.4.4 dbyrd Enable math interval for "H, i, s" date format codes.
        // 2010-09-05 0.1.3.3 dbyrd Allow date-time parameter without crashing.
        // 2010-08-30 0.1.2.2 dbyrd Convert third parameter to an optional parameter
        // 2010-08-30 0.1.1.1 dbyrd Rename from jmDateMath to dateMath

        if(is_string($SourceDate)){
            $date_time_strings = explode(" ", $SourceDate);  // separate date and time into two values
            $date_only_string = $date_time_strings[0];
            $source_date = strtotime($SourceDate);
        }
        if(!$source_date){
            return(0);
        }
        if(empty($ReturnFmt)){
            $ReturnFmt = 'Y-m-d';
        }

        //$date_time_strings_size = sizeof($date_time_strings);


//        if($ReturnFmt == ''){
//            $ReturnFmt = 'Y-m-d';
//        }

        $m = date('m',$source_date);
        $d = date('d',$source_date);
        $Y = date('Y',$source_date);
        if(sizeof($date_time_strings) > 1 && strlen($date_time_strings[1]) > 1){
            $hours   = date('H', $source_date);
            $minutes = date('i', $source_date);
            $seconds = date('s', $source_date);
            $time_of_day_as_seconds = ($hours * 3600) + ($minutes * 60) + $seconds;
        } else {
            // Use default values
            $hours   = 0;
            $minutes = 0;
            $seconds = 0;
            $time_of_day_as_seconds = 0;
        }

        $MathFunc = substr($doMath,0,1);
        $Unit = substr($doMath,-1,1);
        $Qty = substr($doMath, 1,strlen($doMath)-2);
        if($MathFunc == '-'){
            Switch($Unit){
                Case "m":
                    $newday = mktime($hours,$minutes,$seconds,($m-$Qty),$d,$Y);
                break;
                Case "d":
                    $newday = mktime($hours,$minutes,$seconds,$m,($d-$Qty),$Y);
                break;
                Case "Y":
                    $newday = mktime($hours,$minutes,$seconds,$m,$d,($Y-$Qty));
                break;
                case "H":
                    $newday = mktime(($hours-$Qty),$minutes,$seconds,$m,$d,$Y);
                    break;
                case "i":
                    $newday = mktime($hours,($minutes-$Qty),$seconds,$m,$d,$Y);
                    break;
                case "s":
                    $newday = mktime($hours,$minutes,($seconds-$Qty),$m,$d,$Y);
                    break;
            }

        }elseif($MathFunc == '+'){

            Switch($Unit){
                Case "m":
                    $newday = mktime($hours,$minutes,$seconds,($m+$Qty),$d,$Y);
                break;
                Case "d":
                    $newday = mktime($hours,$minutes,$seconds,$m,($d+$Qty),$Y);
                break;
                Case "Y":
                    $newday = mktime($hours,$minutes,$seconds,$m,$d,($Y+$Qty));
                break;
                case "H":
                    $newday = mktime(($hours+$Qty),$minutes,$seconds,$m,$d,$Y);
                    break;
                case "i":
                    $newday = mktime($hours,($minutes+$Qty),$seconds,$m,$d,$Y);
                    break;
                case "s":
                    $newday = mktime($hours,$minutes,($seconds+$Qty),$m,$d,$Y);
                    break;
            }
        }elseif($doMath ==''){
            $newday = mktime($hours,$minutes,$seconds,$m,$d,$Y);
        }

        if(array_count_values($date_time_strings) > 1) {
            //$newday = date($ReturnFmt,$newday) . " " . $date_time_strings[1];
            $newday = date($ReturnFmt,$newday);
        } else {
            $newday = date($ReturnFmt,$newday);
        }
        return($newday);
    }

    public static function mtime_elapsed($begin_mtime, $end_mtime=null){
        if ( empty($end_mtime)){
            $end_mtime_is_empty_yn = "Y";
        } else {
            $end_mtime_is_empty_yn = "N";
        }
//        if ( is_null($end_mtime) ){
//            echo "\$end_mtime is null <br />";
//            $end_mtime = mTime();
//        } else {
//            // validate mtime passed in?
//            // $search_end_mtime      = mTime();
//        }

        $duration_sec   = round(($end_mtime - $begin_mtime), 4); // golden!
        // The duration will often be off by one millisecond short. Test the return value.
        $time_check_1        = $begin_mtime + $duration_sec;
        $time_check_2        = $begin_mtime + $duration_sec + 0.0001;
        // a compare function is needed for huge floats
        $time_check_1_comp   = num::comp($time_check_1, $end_mtime, 4);
        $time_check_2_comp   = num::comp($time_check_2, $end_mtime, 4);

        if (      $time_check_1_comp === 0 ){
            return $duration_sec;

        } elseif( $time_check_2_comp === 0 ){
            return $duration_sec + 0.0001;

        } else {
            return $duration_sec;
        }
//        //echo "$begin_mtime => $end_mtime <br />";
//        $begin_dtm      = mTime_datetime_string($begin_mtime);
//        $end_dtm        = mTime_datetime_string($end_mtime);
//        $result_list = [
//               "duration_sec"=>$duration_sec
//             , "begin_mtime" =>$begin_dtm
//             , "end_mtime"   =>$end_dtm];
//        return $result_list;
    }

    public static function mtime_elapsed__prototype($begin_mtime, $end_mtime=null){
        if (empty($end_mtime)){
            $end_mtime = mTime();
        } else {
            // validate mtime passed in?
            // $search_end_mtime      = mTime();
        }

        $duration_sec   = timeDiff($begin_mtime, $end_mtime);
        $duration_sec   = $end_mtime - $begin_mtime ;
        $duration_sec   = round(($end_mtime - $begin_mtime), 4); // golden!
        $begin_dtm      = mTime_datetime_string($begin_mtime);
        $end_dtm        = mTime_datetime_string($end_mtime);
        $result_list = array(
               "duration_sec"=>$duration_sec
             , "begin_mtime" =>$begin_dtm
             , "end_mtime"   =>$end_dtm);
        return $result_list;
    }

    public static function time_diff($start_dt, $end_dt, $return_array_yn=NULL, $use_tight_format_yn=NULL, $drop_detail_yn=NULL){
        // Development Status = Golden!
        $start_time = strtotime($start_dt);
        $end_time   = strtotime($end_dt);
        $delta = $end_time - $start_time;
        $delta_str = sec2time($delta, $return_array_yn, $use_tight_format_yn, $drop_detail_yn);
        return $delta_str;
    }

    public static function sec2time($time, $return_array_yn=NULL, $use_tight_format_yn=NULL, $drop_detail_yn=NULL){
        // Development Status = Unknown
        // Next three lines added by dbyrd to avoid php warning msg re: an undefined variable.
        $yr  = 0;
        $day = 0;
        $hr  = 0;
        $min = 0;
        if (empty($return_array_yn)){
            $return_array_yn = "N";
        }
        if (empty($use_tight_format_yn)) {
            $use_tight_format_yn = "Y";
        }
        if (empty($drop_detail_yn)) {
            $drop_detail_yn = "Y";
        }
        if(is_numeric($time)){
            $value = array(
              "years" => 0, "days" => 0, "hours" => 0,
              "minutes" => 0, "seconds" => 0,
        );
        if($time >= 31556926){
            $value["years"] = floor($time/31556926);
            $yr   = floor($time/31556926);
            $time = ($time%31556926);
        }
        if($time >= 86400){
            $value["days"] = floor($time/86400);
            $day = floor($time/86400);
            $time = ($time%86400);
        }
        if($time >= 3600){
            $value["hours"] = floor($time/3600);
            $hr = floor($time/3600);
            $time = ($time%3600);
        }
        if($time >= 60){
            $value["minutes"] = floor($time/60);
            $min = floor($time/60);
            $time = ($time%60);
        }
        $value["seconds"] = floor($time);
        $sec = floor($time);

        if ($use_tight_format_yn == "Y"){
            $str =  (($yr  > 0) ? "<b>$yr</b>y " : "") ;
            $str .= (($day > 0) ? "<b>$day</b>d " : "") ;
            $str .= (($hr  > 0) ? "<b>$hr</b>h " : "") ;
            if ($drop_detail_yn == "N") {
                $str .= (($min > 0) ? "<b>$min</b>m " : "") ;
                $str .= (($sec > 0) ? "<b>$sec</b>s" : "") ;
            }
        } else {
            $str =  (($yr  > 0) ? pluralize($yr, "yr")." " : "") ;
            $str .= (($day > 0) ? pluralize($day,"day")." " : "") ;
            $str .= (($hr  > 0) ? pluralize($hr,"hr")." " : "") ;
            if ($drop_detail_yn == "N") {
                $str .= (($min > 0) ? pluralize($min,"min")." " : "") ;
                $str .= (($sec > 0) ? pluralize($sec,"sec") : "") ;
            }
        }

        if ($return_array_yn == "Y") {
            return (array) $value;
        } else {
            return $str;
        }
      }else{
        return (bool) FALSE;
      }
    }

    public static function date_to_year($date=NULL) {
        // Returns the datetime as an integer class
        $dateFormat = 'Y';
        if (is_string($date)) {
            $timestamp = strtotime($date);
        } elseif(is_numeric($date)) {
            $timestamp = $date;
        } else {
            $timeStamp = time();
        }
        $date_class = date($dateFormat,$timeStamp);
        return (int)$date_class;
    }

    public static function dateTimeToDateOnly($dateTime) {
        // Returns the datetime as an integer class
        $dateFormat = "Y-m-d";
        if (is_string($dateTime)) {
            $timeStamp = strtotime($dateTime);
        } elseif(is_numeric($dateTime)) {
            $timeStamp = $dateTime;
        } else {
            throw new CException('invalid parameter value or type', 707);
        }

        $dateStringOut = date($dateFormat,$timeStamp);
        return $dateStringOut;
    }

    public static function date_generate_random_dt($min_dt, $max_dt, $return_array_yn=NULL, $array_size=NULL){
        // Development Status = Golden!
        // pick a random number between date A and date B, then convert it to a date.

        if ( empty($return_array_yn)) {
            $return_array_yn = "Y";
        }
        if ( empty($array_size)) {
            $array_size = 10;
        }

        $random_dt_list = array();

        $min = strtotime($min_dt);
        $max = strtotime($max_dt);

        if ($return_array_yn == "Y"){
            $rand_dt_cnt = 1;
            while ($rand_dt_cnt <=$array_size){
                $random_time = rand($min, $max);
                $date_random = date("Y-m-d H:i:s", $random_time);
                $random_dt_list[] = $date_random;
                //echo $date_random ."<br />";
                $rand_dt_cnt++;
            }
            return $random_dt_list;
        } elseif ($return_array_yn == "N") {
            $random_time = rand($min, $max);
            $date_random = date("Y-m-d H:i:s", $random_time);
            echo $date_random ."<br />";
        }
        //$date_to_send = dateMath($date_random,"+".$days_to_add."d", "Y-m-d H:i:s");
        return $date_random;
    }

    public static function time_elapsed($start, $end="NOW"){
        // 2011-04-05 This function has been superceded by timeDiff which also calls pluralize.
    // original function name = date_diff($start, $end="NOW")

    //    Contributed by: eugene@ultimatecms.co.za
    //        at: 17-Mar-2010 07:58
    //        via: http://php.net/manual/en/function.date-diff.php

    // Example:
    // $start_date = 2010-03-15 13:00:00
    // $end_date = 2010-03-17 09:36:15
    // echo date_diff($start_date, $end_date);
    // Returns: 1days 20hours 36min 15sec

        $sdate = strtotime($start);
        $edate = strtotime($end);

        $time = $edate - $sdate;
        if($time>=0 && $time<=59) {
            // Seconds
            $timeshift = $time.' seconds ';

        } elseif($time>=60 && $time<=3599) {
            // Minutes + Seconds
            $pmin = ($edate - $sdate) / 60;
            $premin = explode('.', $pmin);

            $presec = $pmin-$premin[0];
            $sec = $presec*60;

            $timeshift = $premin[0].' min '.round($sec,0).' sec ';

        } elseif($time>=3600 && $time<=86399) {
            // Hours + Minutes
            $phour = ($edate - $sdate) / 3600;
            $prehour = explode('.',$phour);

            $premin = $phour-$prehour[0];
            $min = explode('.',$premin*60);

            //$presec = '0.'.$min[1];
            $presec = '0.'. ( (sizeof($min)>1) ? $min[1]: "0");
            $sec = $presec*60;

            $timeshift = $prehour[0].' hrs '.$min[0].' min '.round($sec,0).' sec ';

        } elseif($time>=86400) {
            // Days + Hours + Minutes
            $pday = ($edate - $sdate) / 86400;
            $preday = explode('.',$pday);

            $phour = $pday-$preday[0];
            $prehour = explode('.',$phour*24);

            $premin = ($phour*24)-$prehour[0];
            $min = explode('.',$premin*60);

            //$presec = '0.'.$min[1];
            $presec = '0.'. ( (sizeof($min)>1) ? $min[1]: "0");
            $sec = $presec*60;

            $timeshift = $preday[0].' days '.$prehour[0].' hrs '.$min[0].' min '.round($sec,0).' sec ';

        }
        return $timeshift;
    }

    public static function date_to_filename($date=NULL) {
        // Returns the current datetime as a valid file name
        $dateFormat = 'Y-m-d_His';
        if ( empty($date)){
            $timeStamp = time();
        } else {
            $timeStamp = strtotime($date);
        }
        $dateTime= date($dateFormat,$timeStamp);
        return $dateTime;
    }

}
// </editor-fold>



