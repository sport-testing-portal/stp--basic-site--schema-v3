<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Dana Byrd <dana@globalsoccermetrix.com>, <danabyrd@byrdbrain.com>
 *  Notes...:
 *   Sample call: barray::CDbExpression('NOW()');
 */
// <editor-fold defaultstate="collapsed" desc="Version History">
/* Version History
 * v0.5.0 sessionStore($field_value_array)				Write values to session for access by all form controls
 * v0.5.0 sqlTrim($sql)									Removes double spaces and tabs from a sql string to allow a clean var_dump of the sql text
 * v0.4.0 convertPostalCodeToLatLong($postalCode)		Converts any US postal code into a lat-long associative array
 * v0.3.0 fetchModelKeyFieldNames($model_name)
 * v0.2.0 fetchModelFkeyFieldNames($model_name)
 * v0.1.0 listData()
 *
 */
// </editor-fold>
class BaseModel extends CApplicationComponent{
	protected $dataParamOriginal  = [];
	protected $dataParamCopy      = [];
	protected $dataParamRelations = []; // keyed by relationName value = 'modelName'
	protected $dataParamDepth     = []; // storesRelations that have data values

	public function init() {
	}

	/**
	 *
	 * @param string $module The name of the module
	 * @return array List of yii models
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function listYiiModels($storeList=true, $force=false){

		$listStoreArray = Yii::app()->params;
		$listStoreKey   = 'model_names';
		if ( (isset(Yii::app()->params['model_names']) && count(Yii::app()->params['model_names']) > 0 )){
			$exists = true;
		} else {
			$exists = false;
		}
		if ( (isset($listStoreArray[$listStoreKey]) && count($listStoreArray[$listStoreKey]) > 0 )){
			$modelListExists = true;
		} else {
			$modelListExists = false;
		}

		if ($modelListExists && $force == false){
			//return Yii::app()->params['model_names'];
			return $listStoreArray[$listStoreKey];
		}

		$models = [];
		$sourcefolderUri = '../protected/models/*.php';

		foreach(glob($sourcefolderUri) as $fileURI){
			$models[] = pathinfo($fileURI, PATHINFO_FILENAME); // strip path and file extenstion
		}
		// Remove exclusions eg models that extend CFormModel and therefore
		// are not typically used to persist data on the backend, nor do they
		// have relations
		$modelsToExclude=['ContactForm','ContactFormGsm','FileUpload','LoginForm'];
		foreach($modelsToExclude as $modelToExclude){
			if (array_search($modelToExclude, $models) !== false){
				$excludeKey = array_search($modelToExclude, $models);
				unset($models[$excludeKey]);
			}
		}

		if ($modelListExists == false || $force){
			if ($storeList && count($models) > 0){
				$listStoreArray[$listStoreKey] = $models;
			}
		}

		return $models;
	}

	/**
	 *
	 * @param string $module The name of the module
	 * @return array List of yii models
	 * @internal Development Status = code construction
	 */
	public static function listModelsInModule($module, $force=false){

		$listStoreArray = Yii::app()->params;
		$listStoreKey   = 'model_names';
		if ( (isset(Yii::app()->params['model_names']) && count(Yii::app()->params['model_names']) > 0 )){
			//$exists = true;
		}
		if ( (isset($listStoreArray[$listStoreKey]) && count($listStoreArray[$listStoreKey]) > 0 )){
			$modelListExists = true;
		} else {
			$modelListExists = false;
		}

		if ($modelListExists && $force == false){
			//return Yii::app()->params['model_names'];
			return $listStoreArray[$listStoreKey];
		}


		$models = [];
		if (!empty($module)){
			$sourcefolderUri = "../protected/modules/$module/models/*.php";
		}
		foreach(glob($sourcefolderUri) as $fileURI){
			$models[] = pathinfo($fileURI, PATHINFO_FILENAME); // strip path and file extenstion
		}

		if ($modelListExists == false || $force){
			$listStoreArray[$listStoreKey] = $models;
		}

		return $models;
	}

	public static function stubFunctionForControllers(){
		if (!array_key_exists(Yii::app()->params['model_names'])) {
			Yii::app()->params['model_names'] = array();

		}
		if (in_array('<?php echo "$this->modelClass" ?>', Yii::app()->params['model_names'])) {
			Yii::app()->params['model_names'][] = "'$this->modelClass'";

		}
	}

	/**
	 * Converts an array of CActiveRecord models and related models into a simple assoc array
	 * @param type $models
	 * @param array $filterAttributes
	 * @return array $mtfva (a simple attributeName=>value array) indexed by model name
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function convertModelToArray($models, array $filterAttributes = null) {
        if (is_array($models)){
            $arrayMode = TRUE;
		}else {
            $models = array($models);
            $arrayMode = FALSE;
        }

        $result = array();
        foreach ($models as $model) {
            $attributes = $model->getAttributes();

            if (isset($filterAttributes) && is_array($filterAttributes)) {
                foreach ($filterAttributes as $key => $value) {

                    if (strtolower($key) == strtolower($model->tableName()) && strpos($value, '*') === FALSE) {
                        $value = str_replace(' ', '', $value);
                        $arrColumn = explode(",", $value);

                        foreach ($attributes as $key => $value){
                            if (!in_array($key, $arrColumn)){
                                unset($attributes[$key]);
							}
						}
                    }
                }
            }

            $relations = array();
            foreach ($model->relations() as $relationName => $relatedAttributes) {
                if ($model->hasRelated( $relationName )) {
                    $relations[$relationName] = self::convertModelToArray($model->$relationName, $filterAttributes);
                }
            }
			unset($relatedAttributes);
            $all = array_merge($attributes, $relations);

            if ($arrayMode){
                array_push($result, $all);
			}else{
                $result = $all;
			}
        }
		unset($model);
		$mtfva = $result;
        return $mtfva;
    }


	/**
	 * Converts an array of CActiveRecord objects into an HTML bindable list
	 * @param CActiveRecord $models
	 * @param string $valueField Used as the associative index: array[$valueField]
	 * @param string $textField Used as the associative array value: array[$valueField]=$textField
	 * @param string $groupField Allows grouping of the values into embedded associative arrays
	 * @param boolean $encode Use html encoding for the values
	 * @return array $listData {val=>text},{val=>text},...
	 * @internal This function can handle encoding of the values without an anonymous callback function as required by CHtml::listData()
	 * @see CHtml::listData()
	 */
	public static function listData($models,$valueField,$textField,$groupField='', $encode=false) {
		$listData=array();
		if($groupField==='')
		{
			if ($encode==true){
				foreach($models as $model)
				{
					$value=$model[$valueField];
					$text=$model[$textField];
					$listData[$value] = CHtml::encode($text);
				}
			} else { // no html encoding required
				foreach($models as $model){
					$value=$model[$valueField];
					$text=$model[$textField];
					$listData[$value]=$text;
				}
			}
		}
		else
		{ // a group field has been supplied
			if ($encode==true){
				foreach($models as $model)
				{
					$group=$model[$groupField];
					$value=$model[$valueField];
					$text=$model[$textField];
					if($group===null){
						$listData[$value] = CHtml::encode($text);
					} else {
						$listData[$group][$value] = CHtml::encode($text);
					}
				}
			} else { // No encoding requried
				foreach($models as $model)
				{
					$group=$model[$groupField];
					$value=$model[$valueField];
					$text=$model[$textField];
					if($group===null){
						$listData[$value] = $text;
					} else {
						$listData[$group][$value] = $text;
					}
				}
			}
		}
		return $listData;
	}

	/**
	 *
	 * @param type $data_envelope_array
	 * @internal data_envelope_array('table1'=array('fld1'=>'val', 'f2'=>'val'), 't2'=>array('fld1'=>'val'))
	 * @todo: parse a jede?
	 * @internal dev sts = UC
	 * @version 0.0.1
	 * @internal Development Status = ready for testing (rft)
	 */
	protected function StoreDataEnvelope($data_envelope_array) {
		// @todo: loop through tables
		// @todo:   -- loop through fields
		foreach ($data_envelope_array as $table_name => $field_value_array) {
			// @todo: if table name exists then open model
			$model_name = Bcommon::generateYiiModelClassName($table_name);
			$pk_field_name  = $model_name::getPk();
			if (array_key_exists($pk_field_name, $field_value_array)){
				$pk_field_value = $field_value_array[$pk_field_name];
			} else {
				$pk_field_value = 0;
			}

			if ((int)$pk_field_value > 0){
				$pk_cnt = $model_name::exists(array($pk_field_name=>$pk_field_value));
			} else {
				$pk_cnt = 0;
			}

			if ( empty($pk_cnt)){
				$scenario = 'insert';
			} else {
				$scenario = 'update';
			}
			// @todo: get fkey vals then fkey counts per parent table based on addition parameters passed for the table

			if ($scenario == 'insert'){
				$model = new $model();
			}elseif ($scenario == 'update') {
				$model = $model_name::model()->findByPk($pk_field_value);
			}

			$model->setAttributes($field_value_array);

			$model->save();
		}
	}

	/**
	 *
	 * @param array $data_envenlope_array
	 * @return string returns an encrypted string
	 */
	protected function arrayToJEDE($data_envenlope_array) {
		$jede = Bjede::convert_array_to_jede_str($data_envenlope_array);
		return $jede;
	}

	/**
	 * Converts any US postal code into a lat-long associative array
	 * @param string $postalCode
	 * @return array[] $rtn
	 */
	public static function convertPostalCodeToLatLong($postalCode){
		$ar = ZipCode::model()->findByAttributes(
			array('postal_code'=>':postal_code'),
			array(':postal_code'=>$postalCode)
		);
		$rtn = array(
			'latitude'	=>$ar->attributes['latitude']
			,'longitude'=>$ar->attributes['longitude']
		);
		return $rtn;
	}

	/**
	 * Implement a store-this-model method that links to a parent table.
	 * @param array $fva Description
	 * @see http://www.yiiframework.com/doc/guide/1.1/en/database.arr
	 * @internal Development Status = code construction
	 * @deprecated since version 0.0.0
	 */
//	protected function Store($fva) {
//		// @todo: if no model row exists and a parent PK is not supplied via params throw exception
//		// @todo:
//		$teamID = $fva['team_id'];
//		$attributes = array('team_id'=>':team_id');
//		$params     = array(':team_id'=>$teamID);
//		$rows = TeamPlayer::model()->findAllByAttributes($attributes,$params);
//		foreach ($rows as $model){
//
//		}
//		unset($model);
//	}


	/**
	 * AWE Recordset update
	 * Handles BELONGS_TO relations. This is a template for all belongs_to relations
	 * @param string $modelName
	 * @data array $data
	 */

	public static function store($modelName, $data){

		if(! isset($mtfva['Org'])){

			return;
		}

		if(array_key_exists('Org', $mtfva)){
			$orgValues = $mtfva['Org'];
			if(isset($orgValues['org_id']) && (int)$orgValues['org_id']>0){
				$scenario = 'update';
				$pkVal = (int)$orgValues['org_id'];
				$model = Org::model()->findByPk($pkVal);

			} else {
				$scenario = 'insert';
				$model    = new Org;

				// $this->performAjaxValidation($model, 'org-form');
				if(isset($_POST['ajax']) && $_POST['ajax'] === 'org-form')
				{
					echo CActiveForm::validate($model);
					Yii::app()->end();
				}
			}

            if (isset($mtfva['Org']['orgLevel'])) {
				// by relationName
				$model->orgLevel = $mtfva['Org']['orgLevel'];

			} elseif (isset($mtfva['OrgLevel'])) {
				// byModelName
				$model->orgLevel = $mtfva['OrgLevel'];

			} else {

				if ($scenario === 'update'){
					$model->orgLevel = array();
				}
			}


            if (isset($mtfva['Org']['orgType'])) {
				// by relationName
				$model->orgType = $mtfva['Org']['orgType'];

			} elseif (isset($mtfva['OrgType']) ) {
				// byModelName
				$model->orgLevel = $mtfva['OrgType'];

			} else {
				$model->orgLevel = array();
			}

			$model->attributes = $mtfva['Org'];
			if($model->save()) {
				$this->redirect(array('view','id' => $model->org_id));
            }
		}

	}

	/**
	 * Use the Haversine Formula to display the 100 closest matches to $origLat, $origLon
	 * Only search the MySQL table $tableName for matches within a 10 mile ($dist) radius.
	 * @param string $origLat original latitude the orig lat and lon are used to put an 'X' on a map
	 * @param string $origLon original longitude
	 * @param numeric $maxDistance This is the maximum distance (in miles) away
	 * from $origLat, $origLon in which to search
	 */
	protected function getRowsByDistance($origLat, $origLon, $maxDistance, $rowLimit=100) {
		//include("./assets/db/db.php"); // Include database connection function
		//$db = new database(); // Initiate a new MySQL connection

		$tableName = "zip_code";

//		$origLat = 42.1365;
//		$origLon = -71.7559;
//		$rowLimit = 100;
//		$dist = 10; // This is the maximum distance (in miles) away from $origLat, $origLon in which to search

//		$query = "SELECT name, latitude, longitude, 3956 * 2 *
//				  ASIN(SQRT( POWER(SIN(($origLat - abs(latitude))*pi()/180/2),2)
//				  +COS($origLat*pi()/180 )*COS(abs(latitude)*pi()/180)
//				  *POWER(SIN(($origLon-longitude)*pi()/180/2),2)))
//				  as distance FROM $tableName WHERE
//				  longitude between ($origLon-$dist/abs(cos(radians($origLat))*69.1))
//				  and ($origLon+$maxDistance/abs(cos(radians($origLat))*69.1))
//				  and latitude between ($origLat-($maxDistance/69.1))
//				  and ($origLat+($maxDistance/69.1))
//				  having distance < $maxDistance ORDER BY distance limit $rowLimit;";

		$query = "SELECT postal_code, latitude, longitude, 3956 * 2 *
				  ASIN(SQRT( POWER(SIN(($origLat - abs(latitude))*pi()/180/2),2)
				  +COS($origLat*pi()/180 )*COS(abs(latitude)*pi()/180)
				  *POWER(SIN(($origLon-longitude)*pi()/180/2),2)))
				  as distance FROM $tableName WHERE
				  longitude between ($origLon-$maxDistance/abs(cos(radians($origLat))*69.1))
				  and ($origLon+$maxDistance/abs(cos(radians($origLat))*69.1))
				  and latitude between ($origLat-($maxDistance/69.1))
				  and ($origLat+($maxDistance/69.1))
				  having distance < $maxDistance ORDER BY distance limit $rowLimit;";


		$cmd  = Yii::app()->db->createCommand($query);
		$data = $cmd->queryAll($fetchAssociative=true);

		// if $data == false then no data
		//if ($data == false){

		return $data;

	}


	/**
	 * @internal dev sts = UC
	 * @internal pulled from models/Player.php
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 * @deprecated since version 0.54.3
     */
    public static function StoreWithParentThatHasAHavingOneRelationWithTheChildTable($field_value_array)
    {
		$primary_key_field_name = $this->primaryKey();
		$foreign_key_field_name = 'person_id';
		$class = __CLASS__;
		// run a search by primary key
		$row_found_yn = "N";
		if (array_key_exists($primary_key_field_name, $field_value_array)) {
			$pkey_val = $field_value_array[$primary_key_field_name];
			//$row = Player::model()->findByPk($pkey_val); // returns object
			$row = $class::model()->findByPk($pkey_val); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}

		}
		if ($row_found_yn == "N" && array_key_exists($foreign_key_field_name, $field_value_array)) {
			// run a search by foreign key
			$fkey_val = (int)$field_value_array[$foreign_key_field_name];
			//$row = Player::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
			$row = $class::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}

		if ($row_found_yn == "Y"){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $row->attributes;
			$row->attributes = $field_value_array;
			$diff = array_diff($bfr_upd,$row->attributes);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$primary_key_field_name];
			}
			// The values must be different. Save the model.
			if ($row->save()){
				return (int)$row->attributes[$primary_key_field_name];
			} else {
				return 0;
			}

		} else {
			// Insert
			//$model = new Player();
			$model = new $class();
			$model->unsetAttributes(); // clears default values
			$model->attributes = $field_value_array;
			if ($model->save()){
				$id = (int)$model->attributes[$primary_key_field_name];
			} else {
				$id = 0;
			}

			return (int)$id;
		}

		return false;
    }

    /**
	 *
	 * @param string $modelName
	 * @param string|int $personID
     * @return int
	 * Added locally as by dbyrd as a test for protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public static function RowCountByPersonId($modelName, $personID){

		$attributes = ['person_id'=>$personID];
		Person::model()->countByAttributes($attributes);
		$count = $modelName::model()->countByAttributes($attributes);

		$localDebug = false;
		if ($localDebug === true){
			$models = $modelName::model()->findAllByAttributes($attributes);
			if (is_array($models) && count($models) > 0){
				$cnt = count($models);
			} else {
				$cnt = 0;
			}
			return $cnt;
		}
		return (int)$count;
    }


	/**
	 * Fetch foreign keys from a model
	 * @param string $modelName
	 * @return array $fkeys
	 * @version 1.0.0
	 */
	public static function fetchModelFkeyFieldNames($modelName) {
		$fkeys		= array();
		$model		= $modelName::model();
		//$pkey		= $model->primaryKey();
		$relations	= $model->relations();
		foreach($relations as $relation){
			$fkey = $relation[2]; // fkey field name
			if (! array_key_exists($fkey, $fkeys)){
				$fkeys[] = $fkey;
			}
		}
		return $fkeys;
	}


	/**
	 * Fetch attributes from a model while excluding relation name attributes
	 * key[0] will always be the primary key
	 * @param string $modelName
	 * @return array ['keys','non-keys', 'all-fields']
	 * @version 1.0.0
	 * @internal Development Status = Golden!
	 */
	public static function fetchModelKeyFieldNames($modelName) {
		$keys		= array();
		$model		= $modelName::model();
		$pkey		= $model->primaryKey();
		$keys[]		= $pkey;
		$relations	= $model->relations();
		foreach($relations as $relationName=>$relationInfo){
			$fkey = $relationInfo[2]; // fkey field name
			if (array_search($fkey, $keys) === false){
				$keys[] = $fkey;
			}
		}
		unset($relationName, $relationInfo);
		// key[0] will always be the primary key
		return $keys;
	}


	/**
	 * Fetch an assoc array of model attributes (keys,non-keys,all) from multiple models
	 * @param string|array $modelsList
	 * @return array $attributesMaster ['modelName'=>['keys','non-keys','all']]
	 * @throws CException
	 * @version 1.0.0
	 * @uses BaseModel::fetchModelKeyFieldNames()
	 * @internal Called by: AthleteTeams::generateYiiNormativeModelValuesArray();
	 * @internal Development Status = Golden!
	 */
	public static function fetchModelFieldNamesMuliple($modelsList) {

		if (is_string($modelsList) && stripos($modelsList, ',') > 0){
			$modelNames = explode(',',$modelsList);
		} elseif (is_array($modelsList)){
			$modelNames = $modelsList;
		} else {
			throw new CException('modelsList param needs be a comma delimited string or an indexed array of model names', 777);
		}


		$attributesMaster = [];
		foreach ($modelNames as $modelName){

			$attributes = self::fetchModelFieldNames(trim($modelName));
			if (count($attributes) > 0){
				$attributesMaster[trim($modelName)] = $attributes;
			}
		}
		return $attributesMaster;
	}


	/**
	 * Fetch attributes from a model while excluding relation name attributes
	 * key[0] will always be the primary key
	 * @param string $modelName
	 * @return array ['keys','non-keys', 'all-fields']
	 * @version 1.0.0
	 * @internal Development Status = Golden!
	 */
	public static function fetchModelFieldNames($modelName) {
		$keys		= array();
		$model		= $modelName::model();
		$pkey		= $model->primaryKey();
		$keys[]		= $pkey;
		$relations	= $model->relations();
		$attributes = $model->attributes;
		$nonKeyAttributes = $attributes;
		unset($nonKeyAttributes[$pkey]);
		foreach($relations as $relationName=>$relationInfo){
			$fkey = $relationInfo[2]; // fkey field name
			if (array_search($fkey, $keys) === false){
				$keys[] = $fkey;
				unset($nonKeyAttributes[$fkey]);
			}
			unset($attributes[$relationName]); // leave only field names
		}
		// key[0] will always be the primary key
		return ['keys'=>$keys, 'non-keys'=>$nonKeyAttributes, 'all-fields'=>$attributes];
	}


	/**
	 * Fetch an assoc relations array from a model by model name
	 * Yii original
	 * @param string $modelName
	 * @param bool $eSaveOnly true Excludes any relation that is not eSaveRelatedBehavior compatible
	 * @param bool $v1_Output true limits output to byRelationName
	 * @param bool $useModelNameAsKey true Replaces relationName default key with modelName as the key
	 * @return array $relationsOut[byRelationName, byModelName, byRelationType]
	 * @version 2.0.0
	 * @internal Called by: self::fetchModelRelationsMultiple()
	 * @internal Called by: ESaveRelatedBehavior->saveWithRelated()
	 * @internal Called by: ESaveRelatedBehavior->saveRelated()
	 * @internal Called by: AweActiveRecord->relationsAssoc() eg every generated
	 * Yii model inherits this method "relationsAssoc()"
	 * @internal Development Status = Golden!
	 * @internal v2 returns an containing three associative arrays of relation info
	 *   byRelationName, byModelName, byRelationType as reference data.
	 * @internal v1 returned a single associative array of relation info
	 * @internal v
	 */
	public static function fetchModelRelations($modelName, $eSaveOnly=false,
			$v1_Output=false, $useModelNameAsKey=true)
	{
		$model		= $modelName::model();
		$relations	= $model->relations(); // index array
		$relationsOut = [];                // assoc array
		$relationsOut_v1 = [];              // assoc array
		$byModelName    = [];
		$byRelationName = [];
		$byRelationType = [];
		// stores relations
		// 'byModelName'   =>$byModelName,
		// 'byRelationType'=>$byRelationType,
		// 'byRelationName'=>$byRelationName,


		foreach ($relations as $relationName=>$relationInfo){

			if ($relationInfo[0] === 'CManyManyRelation' || $relationInfo[0] === 'CHasManyRelation' || $relationInfo[0] === 'CHasOneRelation'){
				// CBelongsToRelation type is automatically added to all AWE generated models
				$eSaveCompatible = true;
			} else {
				// must be CBelongsToRelation
				$eSaveCompatible = false;
			}

			if ($eSaveOnly == true && $eSaveCompatible == false){
				// skip this relation
				continue;
			}

			$relationAssoc = [
				'relationType'   =>$relationInfo[0],
				'relatedModel'   =>$relationInfo[1],
				'fkey'           =>$relationInfo[2],
				'eSaveCompatible'=>$eSaveCompatible,
				'relationName'   =>$relationName,
			];
			ksort($relationAssoc, SORT_NATURAL);


			$relatedModel=$relationAssoc['relatedModel'];
			$relationType=$relationAssoc['relationType'];

			if ($v1_Output===true){
				$relationsOut_v1[$relationName]   = $relationAssoc;
			}
			elseif ($v1_Output===false)
			{

				// @todo depreciate the v1 code block  after all dependencies are cleaned up
//				$relationsOut[$relationName]   = $relationAssoc;
//				$relationsOut[$relatedModel]   = $relationAssoc;
//				// There can one or more relations per relation type
//				// so an indexed sub-array is used for relation types
//				$relationsOut[$relationType][] = $relationAssoc;

				if ($useModelNameAsKey){
					$relationTopLevelKey = $relatedModel;
				} else {
					$relationTopLevelKey = $relationName;
				}

				$byModelName    [$relatedModel]				    = $relationAssoc;
				$byRelationName [$relationName]					= $relationAssoc;
				$byRelationType [$relationType][$relatedModel]  = $relationAssoc;

				// v1.1 results layout
//				$relationsOut[$relationTopLevelKey] = [
//					'byModelName'   =>$byModelName,
//					'byRelationType'=>$byRelationType,
//					'byRelationName'=>$byRelationName,
//				];
			}
		}

		if ($v1_Output===true ){
			if (count($relationsOut_v1 > 0)){
				ksort($relationsOut_v1, SORT_NATURAL);
				return $relationsOut_v1;
			}else{
				return [];
			}
		}

		if (count($byModelName) > 0){
			ksort($byModelName, SORT_NATURAL);
		}
		if (count($byRelationName)>0){
			ksort($byRelationName, SORT_NATURAL);
		}
		if (count($byRelationType)>0){
			ksort($byRelationType, SORT_NATURAL);
		}


		if (count($relationsOut) > 0){
			ksort($relationsOut, SORT_NATURAL);
		}


		if (count($byModelName) > 0) {
			$relationsOut = [
				'byModelName'   =>$byModelName,
				'byRelationName'=>$byRelationName,
				'byRelationType'=>$byRelationType,
			];
			return $relationsOut;
		} else {
			return [];
		}
	}


	/**
	 * Fetch an associative relations array from a model by model name while filtering
	 * out any model relations that are not in the list of allowed models in the output.
	 * The sparse list typically called upon by composite models that read-write to many other models.
	 * Yii original method returns an indexex array by relationName
	 * @param string $modelName The model to get relations from
	 * @param array $modelsAllowed The only models that will be included in relations output.
	 * @param bool $eSaveOnly true Excludes any relation that is not eSaveRelatedBehavior compatible
	 * @return array $relationsOut[byRelationName, byModelName, byRelationType]
	 * @version 2.0.0
	 * @internal Called by: self::fetchModelRelationsMultiple()
	 * @internal Called by: ESaveRelatedBehavior->saveWithRelated()
	 * @internal Called by: ESaveRelatedBehavior->saveRelated()
	 * @internal Development Status = Golden!
	 * @internal v2 returns an containing three associative arrays of relation info
	 *   byRelationName, byModelName, byRelationType as reference data.
	 * @internal v1 returned a single associative array of relation info
	 * @internal v
	 */

	public static function fetchModelRelationsAsSparseList($modelName, $modelsAllowed=[],$eSaveOnly=false)
	{
		$model		= $modelName::model();
		$relations	= $model->relations(); // index array
		$relationsOut = [];                // assoc array
		$byModelName    = [];
		$byRelationName = [];
		$byRelationType = [];
		// stores relations
		// 'byModelName'   =>$byModelName,
		// 'byRelationType'=>$byRelationType,
		// 'byRelationName'=>$byRelationName,


		foreach ($relations as $relationName=>$relationInfo){

			if ($relationInfo[0] === 'CManyManyRelation' || $relationInfo[0] === 'CHasManyRelation' || $relationInfo[0] === 'CHasOneRelation'){
				$eSaveCompatible = true;
			} else {
				$eSaveCompatible = false;
			}

			if ($eSaveOnly == true && $eSaveCompatible == false){
				// skip this relation
				continue;
			}

			$relationAssoc = [
				'relationType'   =>$relationInfo[0],
				'relatedModel'   =>$relationInfo[1],
				'fkey'           =>$relationInfo[2],
				'eSaveCompatible'=>$eSaveCompatible,
				'relationName'   =>$relationName,
			];
			ksort($relationAssoc, SORT_NATURAL);


			$relatedModel=$relationAssoc['relatedModel'];
			$relationType=$relationAssoc['relationType'];

			if (!in_array($relatedModel, $modelsAllowed)){
				continue;
			}

			$byModelName    [$relatedModel]				    = $relationAssoc;
			$byRelationName [$relationName]					= $relationAssoc;
			$byRelationType [$relationType][$relatedModel]  = $relationAssoc;

		}


		if (count($byModelName) > 0){
			ksort($byModelName, SORT_NATURAL);
		}
		if (count($byRelationName)>0){
			ksort($byRelationName, SORT_NATURAL);
		}
		if (count($byRelationType)>0){
			ksort($byRelationType, SORT_NATURAL);
		}

		if (count($relationsOut) > 0){
			ksort($relationsOut, SORT_NATURAL);
		}


		if (count($byModelName) > 0) {
			$relationsOut = [
				'byModelName'   =>$byModelName,
				'byRelationName'=>$byRelationName,
				'byRelationType'=>$byRelationType,
			];
			return $relationsOut;
		} else {
			return [];
		}
	}


	/**
	 * Fetch an assoc relations array from a model by model name
	 * @param string $modelName
	 * @param bool $eSaveOnly true Excludes any relation that is not eSaveRelatedBehavior compatible
	 * @return array $relationsOut
	 * @version 1.1.0
	 * @internal Called by: self::fetchModelRelationsMultiple()
	 * @internal Called by: ESaveRelatedBehavior->saveWithRelated()
	 * @internal Called by: ESaveRelatedBehavior->saveRelated()
	 * @internal Development Status = Golden!
	 * @deprecated since version 0.54.3
	 */
	public static function fetchModelRelations_v1($modelName, $eSaveOnly=false) {
		$model		= $modelName::model();
		$relations	= $model->relations(); // index array
		$relationsOut = [];                // assoc array
		foreach ($relations as $relationName=>$relationInfo){
			if ($relationInfo[0] === 'CManyManyRelation' || $relationInfo[0] === 'CHasManyRelation' || $relationInfo[0] === 'CHasOneRelation'){
				$eSaveCompatible = true;
			} else {
				$eSaveCompatible = false;
			}

			if ($eSaveOnly == true && $eSaveCompatible == false){
				// skip this relation
				continue;
			}

			$relationAssoc = [
				'relationType'   =>$relationInfo[0],
				'relatedModel'   =>$relationInfo[1],
				'fkey'           =>$relationInfo[2],
				'eSaveCompatible'=>$eSaveCompatible,
				'relationName'   =>$relationName,
			];
			ksort($relationAssoc, SORT_NATURAL);

			$relationsOut[$relationName] = $relationAssoc;

		}

		if (count($relationsOut) > 0){
			ksort($relationsOut, SORT_NATURAL);
			return $relationsOut;
		} else {
			return [];
		}
	}


	/**
	 * Fetch an assoc relations array from a delimited string or an indexed array of model names
	 * @param string|array $modelsList
	 * @param bool $eSaveOnly =true Excludes any relation that is not eSaveRelatedBehavior compatible
	 * @return array $relationsOut
	 * @throws CException
	 * @version 1.0.0
	 * @uses BaseModel::fetchModelRelations()
	 * @internal Called by: AthleteTeams::getRelationsAsSparseList();
	 * @internal Development Status = Golden!
	 */
	public static function fetchModelRelationsMultiple($modelsList, $eSaveOnly=false) {
		if (is_string($modelsList) && stripos($modelsList, ',') > 0){
			$modelNames = explode(',',$modelsList);
		} elseif (is_array($modelsList)){
			$modelNames = $modelsList;
		} else {
			throw new CException('modelsList param needs be a comma delimited string or an indexed array of model names', 777);
		}
		$relationsMaster = [];
		foreach ($modelNames as $modelName){
			//$relationsMaster[trim($modelName)] = self::fetchModelRelations(trim($modelName),$eSaveOnly);
			$relations = self::fetchModelRelations(trim($modelName),$eSaveOnly);
			if (count($relations) > 0){
				$relationsMaster[trim($modelName)] = $relations;
			}
		}
		return $relationsMaster;
	}


	/**
	 * Fetch an assoc relations array from a delimited string or an indexed array of model names
	 * and only include relations to certain models
	 * @param string|array $modelsList
	 * @param bool $eSaveOnly =true Excludes any relation that is not eSaveRelatedBehavior compatible
	 * @return array $relationsOut
	 * @throws CException
	 * @version 1.0.0
	 * @uses BaseModel::fetchModelRelations()
	 * @internal Called by: AthleteTeams::getRelationsAsSparseList();
	 * @internal Development Status = Golden!
	 * @internal Called by: AthleteTeams::generateYiiNormativeModelValuesArray()
	 * @example BaseModel::fetchModelRelationsMultipleAsSparseList(['Person','Team'], ['Player','Coach','TeamPlayer'], $eSave=true);
	 */
	public static function fetchModelRelationsMultipleAsSparseList($modelsList, $modelsAllowed=[], $eSaveOnly=false) {
		if (is_string($modelsList) && stripos($modelsList, ',') > 0){
			$modelNames = explode(',',$modelsList);
		} elseif (is_array($modelsList)){
			$modelNames = $modelsList;
		} else {
			throw new CException('modelsList param needs be a comma delimited string or an indexed array of model names', 777);
		}
		$relationsMaster = [];
		foreach ($modelNames as $modelName){
			//$relationsMaster[trim($modelName)] = self::fetchModelRelations(trim($modelName),$eSaveOnly);
			$relations = self::fetchModelRelationsAsSparseList(trim($modelName), $modelsAllowed, $eSaveOnly);
			if (count($relations) > 0){
				$relationsMaster[trim($modelName)] = $relations;
			}
		}
		return $relationsMaster;
	}


	public static function relationsWithinModelTreee($modelsList, $eSaveOnly=false){

	}

	/**
	 * Create a list of relations for every model in the AthleteTeams sql view
	 * @param array $modelsList
	 * @param bool $eSaveOnly =true Excludes any relation that is not eSaveRelatedBehavior compatible
	 * @internal Development Status = Golden!
	 * @example AthleteTeams::getRelationsAsSparseList();
	 * @uses self::getComponentModels() The source list of models to fetch relations info from
	 * @internal The base code comes from the AthleteTeams model
	 * @deprecated since version 0.54.3
	 */
	public static function fetchModelRelationsMultipleAsSparseList_Prototype($modelsList, $eSaveOnly=false){
		$componentModelsList = AthleteTeams::getComponentModels();

		foreach ($modelsList as $modelName){
			if (in_array($modelName, $componentModelsList)){
				unset($componentModelsList[$modelName]);
			}
		}

		// Assert the assumption that the models are related
		$mRelationsOfModelsToProcess = BaseModel::fetchModelRelationsMultiple($componentModelsList,$eSaveOnly);
		$mRelationsByModelNames      = BaseModel::relationsArrayToModelsArray($mRelationsOfModelsToProcess);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($mRelationsByModelNames, 10, true);
		//return;
		$mRelationsByModelNamesOut=$mRelationsByModelNames;
		$mRelationsByModelNamesOut2=[]; // build up the relations
		$relationsRemoved=[];
		$relationsRemovedDeep=[];

		$localDebug = false;
		$breakCount = 0;
		//$breakType=[];
		foreach($mRelationsByModelNames as $modelName=>$mRelationInfo){
			$relationsByModelName    = $mRelationInfo['byModelName'];
			//$relationsByRelationName = $mRelationInfo['byRelationName'];
			//$relationsByRelationType = $mRelationInfo['byRelationType'];

			if ($modelName == 'TeamOrg'){
				$x = "break here";
			}
			foreach($relationsByModelName as $relatedModelName=>$relationInfo){
				if(! in_array($relatedModelName,$componentModelsList)){
					$relationName = $relationInfo['relationName'];
					$relationType = $relationInfo['relationType'];

					if (isset($mRelationsByModelNamesOut[$modelName]['byModelName'][$relatedModelName])){
						unset($mRelationsByModelNamesOut[$modelName]['byModelName'][$relatedModelName]);
						$relationsRemovedDeep[$modelName]['byModelName'][]=$relatedModelName;
					} else {
						$x = "break here";
						$breakCount++;
					}
					if (isset($mRelationsByModelNamesOut[$modelName]['byRelationName'][$relationName])){
						unset($mRelationsByModelNamesOut[$modelName]['byRelationName'][$relationName]);
						$relationsRemovedDeep[$modelName]['byRelationName'][]=$relationName;
					} else {
						$x = "break here";
						$breakCount++;
					}
					if (isset($mRelationsByModelNamesOut[$modelName]['byRelationType'][$relationType][$relatedModelName])){
						unset($mRelationsByModelNamesOut[$modelName]['byRelationType'][$relationType][$relatedModelName]);
						$relationsRemovedDeep[$modelName]['byRelationType'][$relationType][]=$relatedModelName;
					} else {
						$x = "break here";
						$breakCount++;
					}
					$relationsRemoved[$modelName][]=$relatedModelName;
				} else {
					$mRelationsByModelNamesOut2[$modelName]['byModelName'][$relatedModelName]=$relationInfo;
				}
			}

		}
		$includeRemoved = false;
		// if errors were found include removals
		if ($breakCount > 0){
			$includeRemovedDeep = true;
		} else {
			$includeRemovedDeep = false;
		}
		if ($localDebug){
			$msg = "Component model relations";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($mRelationsByModelNamesOut, 10, true);
		}
		if ($includeRemoved){
			$msg2 = "Relations removed";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg2);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($relationsRemoved, 10, true);
		}
		if ($includeRemovedDeep){
			$msg3 = "Deep Relations removed";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg3);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($relationsRemovedDeep, 10, true);
		}

		return $mRelationsByModelNamesOut;
	}


	/**
	 * Fetch base table name, model_name, $model_field_name
	 * @param string $viewModelName
	 * @param string $viewFieldName
	 * @param string $baseTableName
	 * @return array $attr
	 * @version 1.0.0
	 * @internal Made to be called by a SQL view model setViewAttributes($fva)
	 * @internal Development Status = code construction
	 */
	public static function fetchBaseModelFieldName($viewModelName, $viewFieldName, $baseTableName) {
		$keys		= array();
		$model		= $viewFieldName::model();
		$pkey		= $model->primaryKey();
		$keys[]		= $pkey;

		$mRelations  = self::fetchModelRelations($baseTableName);
		$baseAttributes = self::fetchModelFieldNames($baseTableName);
		$viewAttributes = self::fetchModelFieldNames($viewModelName);

		$matchingAttributes = [];
		foreach($viewAttributes['All'] as $viewAttribute){

			if (in_array($viewAttribute, $baseAttributes)){
				$matchingAttributes[] = $viewAttribute;
			}
		}
		unset($mRelations);
		return $matchingAttributes;
	}

	/**
	 * Removes double spaces and tabs from a sql string to allow a clean var_dump of the sql text
	 * @param string $sql
	 * @return string
	 * @version 1.0.0
	 * @internal Development Status = Golden!
	 */
	public static function sqlTrim($sql){
		return str_ireplace(['  ',"\t"], ' ', $sql);
	}

	/**
	 * Write values to session for access by all form controls
	 * @param array[] $fva ['field_name1'=>'field_value1, 'field_name2'=>'field_value2']
	 * @param string|int $coach_team_coach_id
	 * @version 2.0.0
	 * @internal RFT
	 * @internal typically called by:
	 */
	public static function sessionStore($field_value_array) {
		$session = Yii::app()->session;

		foreach ($field_value_array as $fieldName => $fieldValue) {
			if (!isset($_SESSION[$fieldName])){
				$session[$fieldName] = $fieldValue;
			} elseif (isset($_SESSION[$fieldName]) && $_SESSION[$fieldName] !== $fieldValue){
				// Record previous value and set the new value
				$session[$fieldName . '__previous']		= $_SESSION[$fieldName];
				$session[$fieldName]					= $fieldValue;
			}
		}
	}

	/**
	 * Used to determine if the logged in user is a GSM staff member
	 * @return bool
	 *
	 * @internal Pulled base code from /views/site/_createdby.php
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function isGsmStaff() {

		if (! isset($_SESSION['is_gsm_staff'])){
			$auth    = Yii::app()->authManager;
			$user_id = Yii::app()->user->id;

			if ($auth->isAssigned('GSM Staff',$user_id)){
				$isGsmStaff = true;
			} else {
				$isGsmStaff = false;
			}
			$session = Yii::app()->session;
			$session['is_gsm_staff'] = $isGsmStaff;
		} else {
			$isGsmStaff = $_SESSION['is_gsm_staff'];
		}
		return $isGsmStaff;
	}

	/**
	 * Returns true if the user_id passed in is a GSM staff member
	 * @param int|string $user_id
	 * @return bool
	 *
	 * @internal Pulled base code from /views/site/_createdby.php
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function isUserIdGsmStaff($user_id) {

		$auth    = Yii::app()->authManager;
		//$user_id = Yii::app()->user->id;

		if ($auth->isAssigned('GSM Staff',$user_id)){
			$isUserIdGsmStaff = true;
		} else {
			$isUserIdGsmStaff = false;
		}

		return $isUserIdGsmStaff;
	}

	/**
	 * Used to determine if the logged in user is a GSM developer
	 * @return bool
	 *
	 * @internal Pulled base code self::isGsmStaff()
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function isGsmDeveloper() {

		if (! isset($_SESSION['is_gsm_developer'])){
			$auth    = Yii::app()->authManager;
			$user_id = Yii::app()->user->id;

			if ($auth->isAssigned('Developer Level Verbosity',$user_id)){
				$isGsmDeveloper = true;
			} else {
				$isGsmDeveloper = false;
			}
			$session = Yii::app()->session;
			$session['is_gsm_developer'] = $isGsmDeveloper;
		} else {
			$isGsmDeveloper = $_SESSION['is_gsm_developer'];
		}
		return $isGsmDeveloper;
	}


	/**
	 * Returns an array for pkeys, keys, success, errors, modelsTouched
	 * @return array
	 * @internal called by the various store methods in the app to get a common
	 *   array structure for logging-reporting db-transaction meta-data
	 */
	public static function getStoreChainMetaData_ArrayTemplate() {
		$pkeys			= []; // written to as various models are processed
		$keys			= []; // create a holder for all keys (pkey, and fkeys)
		$success		= []; // written to on model save
		$warnings		= []; // written to on model save
		$errors			= []; // written to on model save
		$scenario		= []; // insert or update
		$modelsTouched  = []; // stores all saved model=>attributes

		$storeChainMetaData = [
			'pkeys'      =>$pkeys,    // lists the primary keys (pkeys) of models written to (unique row ids)
			'keys'       =>$keys,     // lists pkeys, and fkeys of the models written to
			'success'    =>$success,  // contains names of models with zero issues
			'warnings'	 =>$warnings, // anomalies encountered that are non-fatal, eg items to be aware of
			'errors'     =>$errors,   // critical anomalies that cause an operational error
			'scenario'	 =>$scenario, // typically contains 'insert' or 'update'
			'modelsTouched'=>$modelsTouched // typically lists each value written to a model
		];
		return $storeChainMetaData;
	}

	/**
	 * Stores data from editable controls
	 * @param string | int $id
	 * @param array[] $jede
	 * @version 4.1.0
	 * @see Ajax Multi Model Inserts-Updates (freeplane).mm
	 * @internal base source code from: ajaxStoreDropdownSubmitTeam($id,$jede)
	 * @internal Development Status = Golden!
	 * @internal usecases verified:
	 *   org ins|upd + person upd, org_type ro + org upd,
	 *   team_age_group upd, primary_position upd
	 */
	public static function storeChain($id,$jede){
		$requestIsAjax = Yii::app()->request->isAjaxRequest;
		if( $requestIsAjax == false){											// assert this is an ajax request
			return;
		}

		if (isset($jede['field_values_array']) && is_array($jede['field_values_array'])){
		} else {
			return;
		}

		if (! isset($jede['field_values_array']['models'])){					// Assert required array
			return;
		}
		if (! isset($jede['list_source'])){									// Assert required array
			return;
		}

		$pkeys			= []; // written to as various models are processed
		$success		= []; // written to on model save
		$errors			= []; // written to on model save
		$keys			= []; // create a holder for all keys (pkey, and fkeys)
		$models_touched = []; // stores all saved model=>attributes

		$session		= Yii::app()->session;

		if(isset($jede['submit_widget_usecase'])){
			$widget_usecase = $jede['submit_widget_usecase'];
		} else {
			$widget_usecase = '';
		}

		foreach($jede['field_values_array']['models'] as $model_name=>$model_array){
			$model_name = self::parseModelNameFromChainRule($model_name);
			$keys[$model_name] = BaseModel::fetchModelKeyFieldNames($model_name);
		}

		// Read values from the post array
		//$post_pk	= $_POST['pk'];
		$post_value	= $_POST['value'];
		//$post_name	= $_POST['name'];

		// determine scenario
		if (stripos($post_value,'item-text-to-insert___') !== false){
			$scenario		= 'insert';
			$value			= str_ireplace('item-text-to-insert___','',$post_value);
		} else {
			$scenario		= 'update';
			$value			= $post_value;
		}

		$model_processed_cnt = 0;
  		foreach($jede['field_values_array']['models'] as $model_name=>$model_array){
			// each model array will contain fields to process
			$err_count = count($errors);
			if($err_count > 0) {
				return;
			}

			// Allow conditional processing of each model (*ins, *upd)
			$model_name_parts = explode('*', $model_name);
			if (count($model_name_parts)>1){
				$model_name		= $model_name_parts[0];
				$model_condition= $model_name_parts[1]; // ins | upd | niu (No Insert or Update)
			} else {
				$model_condition=  substr($scenario, $start=0, 3);
			}

			// should the model be processed in the current scenario
			if ($model_condition !== 'niu'){
				if ($model_condition !== substr($scenario, $start=0, 3)){
					continue;
				}
			}

			$pk_name = $model_name::model()->primaryKey();

			// prototype new universal field value loop
			foreach($model_array as $field_name => $field_value){
				// Handle the array translation early in the loop
				if (is_array($field_value) && array_key_exists('{{session_value}}', $field_value)){
					$field_value_fetch			= $field_value['{{session_value}}'];
					if (isset($session[$field_value_fetch['session_variable_name']])){
						$model_array[$field_name]	= $session[$field_value_fetch['session_variable_name']];
					}
					unset($field_value_fetch); // prevent a recursive fetch
					continue;
				}

				// A shared attribute is model->attribute updated within this __METHOD__
				// handle a shared attribute having the exact same name as $field_name
				if (is_string($field_value) == true){
					if(stripos($field_value,'{{shared_attribute}}') !== false){
						foreach ($models_touched as $mt_model_values) {
							foreach($mt_model_values as $mt_field_name => $mt_field_value){
								if($mt_field_name == $field_name){
									$field_value = $mt_field_value;
									$model_array[$field_name]	= $field_value;
									continue 3;
								}
							}
						}
					}
				}
				// handle a shared attribute defined in an array thus handling
				// a shared attribute that doesn't have the exact same name as $field_name
				if (is_array($field_value) && array_key_exists('{{shared_attribute}}', $field_value)){
					$field_value_fetch			= $field_value['{{shared_attribute}}'];
					$model_array[$field_name]	= $field_value;
					foreach ($field_value_fetch as $fvf_key => $fvf_value) {
						if ($fvf_key == 'model_name'){
							$shared_attr_model = $fvf_value;
						} elseif ($fvf_key == 'model_attribute'){
							$shared_attr_attr  = $fvf_value;
						}
					}
					$field_value = $models_touched[$shared_attr_model][$shared_attr_attr];
					$model_array[$field_name]	= $field_value;
					unset($field_value_fetch); // prevent a recursive fetch
					continue;
				}

				// Handle fields with the {{value}} place holder
				if ($field_value==='{{value}}'){	// ie target fkey eg org_id
					$model_array[$field_name] = $value;
				}

				// parse fetch values
				if (is_array($field_value) && array_key_exists('{{fetch_value}}', $field_value)){
					$field_value_fetch			= $field_value['{{fetch_value}}'];
					$field_value				= '{{fetch_value}}';
					$model_array[$field_name]	= $field_value;
					// Handle {{shared_attribute}} place holder within $field_value_fetch
					foreach ($field_value_fetch as $fvf_key => $fvf_value) {
						if (is_string($fvf_value) == true){
							if(stripos($fvf_value,'{{shared_attribute}}') !== false){
								$fvf_val_parts = explode('=',$fvf_value);
								// update the right hand side of the operator using inserted keys
								if (array_key_exists(trim($fvf_val_parts[0]), $pkeys)) {
									$fvf_val_parts[1] = $pkeys[trim($fvf_val_parts[0])];
								}
								// write the updated string back to $field_value_fetch
								$field_value_fetch[$fvf_key] = implode(' = ',$fvf_val_parts);
							}
						} elseif (is_array($fvf_value) && $fvf_key == "where"){
							foreach ($fvf_value as $fvf_key1 => $fvf_value1) {
								// $fvf_key1 will always be numeric in a where clause
								if(is_string($fvf_value1)){
									if(stripos($fvf_value1,'{{shared_attribute}}') !== false){
										$fvf_val1_parts = explode('=',$fvf_value1);
										// update the right hand side of the operator using inserted keys
										// ?? read from the keys[] array instead? Nope, it won't have values only field names
										if (array_key_exists(trim($fvf_val1_parts[0]), $pkeys)) {
											$fvf_val1_parts[1] = $pkeys[trim($fvf_val1_parts[0])];
										}
										// write the updated string back to the inner array
										$fvf_value[$fvf_key1] =  implode(' = ',$fvf_val1_parts);
									}
								}
							}
							// write the updated string back to $field_value_fetch
							$field_value_fetch[$fvf_key] = $fvf_value;
						}
					}
					//@todo parse values with a single method call
					//$shared_attr_val = $this->parseAttribute($attribute_name, $attribute_array);

					// Handle field(s) with the {{fetch_value}} place holder
					if ($field_value == '{{fetch_value}}'){
						$cmd  = Yii::app()->db->createCommand($field_value_fetch);
						$data = $cmd->queryRow($fetchAssociative=true);
					}

					// if the array is empty then we must insert this model rather than update it
					if ($data == false){
						$model_array[$field_name] = ''; // must insert this model
					} else {
						$model_array[$field_name] = $data[$field_name]; // can update
					}
				} // end of parse fetch values

				// handle pkey field
				if ($field_name === $pk_name && $model_name==$jede['list_source']['model_name']){
					if ($scenario === 'insert'){
						$model_array[$field_name]	= '';					// set pkey to null for inserts
					} elseif ($scenario === 'update'){
						$field_value = $value;
						$pkeys[$pk_name] = $field_value;				// grab pkey val for related table updates
						$model_array[$field_name] = $field_value;
					}
				}

				// Handle field with the {{insert_text}} place holder
				if ($scenario==='insert' && $field_value==='{{insert_text}}'){
					$model_array[$field_name] = $value;
					$text = $value;
				} elseif ($scenario==='update' && $field_value==='{{insert_text}}'){
					// populate the return variable $insert_text with a value
					$text = $model_name::model()->findByPk($value)->$field_name;
					// remove the field and value from the array so the field wont be overwritten with string insert_text
					unset($model_array[$field_name]);
				} elseif ($scenario==='update' && $jede['submit_widget_type'] == 'text'){
					$text = $value;
				}

			} // end of universal field value loop


			if ($model_condition !== 'niu'){
				// NIU = no insert and no update -- so all model writes are skipped
				if ((int)$model_array[$pk_name] == 0){
					$m = new $model_name();										// scenario = insert so instantiate an empty model
				} else {
					$m = $model_name::model()->findByPk($model_array[$pk_name]);										// scenario = update so locate the correct model
				}
				$m->attributes = $model_array;								// set all values via bulk assignment
				if ($m->save()){
					$success[] = $model_name;
					$models_touched[$model_name]=$m->attributes;
					$pkeys[$pk_name] = (int)$m->attributes[$pk_name];
				}else{
					$errors[$model_name] = $m->errors;
				}
			}


			if ($model_name == $jede['list_source']['model_name']){
				if($widget_usecase == 'numeric_key'){
					$out = ['success'=>((count($errors)==0) ? true : false),
						'more'=>false,
						'results'=>[
							'id'=>(int)$m->attributes[$pk_name],
							'text'=>$text,
						]];
					$value = (int)$m->attributes[$pk_name];
				} elseif($widget_usecase == 'text_value'){
					// can be used for a drop down list that writes the text to another table
					$out = ['success'=>((count($errors)==0) ? true : false),
						'more'=>false,
						'results'=>[
							'id'=>$text,
							'text'=>$text,
						]];
					$value = $text;
				}
			}

			$model_processed_cnt++;
		} // end of model processing loop

		if ($scenario == 'insert'){
			return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out); // returns a json object
		} elseif ($scenario == 'update'){
			return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out); // returns a json object
		}
	}


	/**
	 *
	 * @param string $modelName
	 * @return strin
	 */
	public static function parseModelNameFromChainRule($modelName) {
		// Allow conditional processing of each model (*ins, *upd)
		$model_name_parts = explode('*', $modelName);
		if (count($model_name_parts)>1){
			$modelName		= $model_name_parts[0];
			//$model_condition= $model_name_parts[1]; // ins | upd
		}
		return $modelName;
	}

	/**
	 * Returns content for an intelligent drop-down that only contains items already found within a table - used for search UIs
	 * @param string $tableWithFkey
	 * @param string $fkeyFieldName
	 * @param string $tableWithPkey
	 * @param string $pkeyFieldName
	 * @param string $pkeyRepresentedTextField
	 * @param string $tableWithFkey_Condition Uses the alias 't' to refer to the fkeytable and t2 for the pkeytable and the where is already included eg "t.created_by > '2001-01-04'"
	 * @internal do not include a 'where ' in the condition parameter
	 * @example path description
	 */
	public static function getDropdownValsWithCount($tableWithFkey, $fkeyFieldName,
			$tableWithPkey, $pkeyFieldName, $pkeyRepresentedTextField,
			$tableWithFkey_Condition=null, $returnFlatArray=false) {

		/* Example that looks at the relationship between the Person and User tables
		 * $tableWithFkey				= 'person'
		 * $fkeyFieldName				= 'user_id' eg person.user_id
		 * $tableWithPkey				= 'user'
		 * $pkeyFieldName				= 'id'      eg user.id
		 * $pkeyRepresentedTextField	= 'username eg user.username
		 * $tableWithFkey_Condition		= primary
		 */
		//		<editor-fold defaultstate="collapsed" desc="original default sql">
//		$sql_template =
//				"select
//				case when b.id is not null then b.`id` 	 else a.`id`   end as `id` ,
//				case when b.id is not null then b.`text` else a.`text` end as `text` ,
//				case when b.id is not null then b.`desc` else a.`desc` end as `desc`
//				from
//					(
//						select 1 as `link`, 0 as `id`, 'no results found' as `text`, '' as `desc`
//					) a
//					left outer join (
//						select
//							1 as 'link'
//							t2.id as id,
//							t2.id as user_id,
//							t2.username as `desc`,
//							count(*) as cnt,
//							concat(t2.username, ' (',count(*), ')') as `text`
//							from player_results_search_log t
//								join user t2 on t.user_id = t2.id
//							group by t2.username
//					) b on a.id = b.id" ;
//		</editor-fold>
		if (is_null($tableWithFkey_Condition)){
			$sql =
				"select
					case when b.id is not null then b.`id` 	 else a.`id`   end as `id` ,
					case when b.id is not null then b.`text` else a.`text` end as `text` ,
					case when b.id is not null then b.`desc` else a.`desc` end as `desc`
					from
						(
							select 1 as `link`, 0 as `id`, 'no results found' as `text`, '' as `desc`
						) a
						left outer join (
							select
								1 as `link`,
								t2.`$pkeyFieldName` as id,
								t2.`$pkeyRepresentedTextField` as `desc`,
								count(*) as cnt,
								concat(t2.`$pkeyRepresentedTextField`, ' (',count(*), ')') as `text`
								from `$tableWithFkey` t
									join `$tableWithPkey` t2 on t.`$fkeyFieldName` = t2.`$pkeyFieldName`
								group by t2.`$pkeyRepresentedTextField`
								order by t2.`$pkeyRepresentedTextField`
						) b on a.link = b.link" ;
/*
			$sql =
				"select
					case when b.id is not null then b.`id` 	 else a.`id`   end as `id` ,
					case when b.id is not null then b.`text` else a.`text` end as `text`
					from
						(
							select 1 as `link`, 0 as `id`, 'no results found' as `text`, '' as `desc`
						) a
						left outer join (
							select
								1 as `link`,
								t2.`$pkeyFieldName` as id,
								t2.`$pkeyRepresentedTextField` as `desc`,
								count(*) as cnt,
								concat(t2.`$pkeyRepresentedTextField`, ' (',count(*), ')') as `text`
								from `$tableWithFkey` t
									join `$tableWithPkey` t2 on t.`$fkeyFieldName` = t2.`$pkeyFieldName`
								group by t2.`$pkeyRepresentedTextField`
								order by t2.`$pkeyRepresentedTextField`
						) b on a.link = b.link" ;
 *
 */
		} else {
			$sql =
				"select
					case when b.id is not null then b.`id` 	 else a.`id`   end as `id` ,
					case when b.id is not null then b.`text` else a.`text` end as `text` ,
					case when b.id is not null then b.`desc` else a.`desc` end as `desc`
					from
						(
							select 1 as `link`, 0 as `id`, 'no results found' as `text`, '' as `desc`
						) a
						left outer join (
							select
								1 as `link`,
								t2.`$pkeyFieldName` as id,
								t2.`$pkeyRepresentedTextField` as `desc`,
								count(*) as cnt,
								concat(t2.`$pkeyRepresentedTextField`, ' (',count(*), ')') as `text`
								from `$tableWithFkey` t
									join `$tableWithPkey` t2 on t.`$fkeyFieldName` = t2.`$pkeyFieldName`
								where $tableWithFkey_Condition
								group by t2.`$pkeyRepresentedTextField`
								order by t2.`$pkeyRepresentedTextField`
						) b on a.link = b.link" ;
		}
		//echo "<html><pre><code>$sql</code></pre></html>";

		$cmd = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$localDebug = false;
		if ($localDebug == true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($data, 10, true);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cmd->text, 10, true);
		}
		if (!is_null($data)){
			if ($returnFlatArray){
				$out = self::flattenAssocArray($data, $keys=[0=>'id',1=>'text']);
				return $out;
			} else {
				return $data;
			}
		}
	}

	/**
	 * Convert an Associative array to a flat array suitable for a drop down list creation
	 * @param array[] $data The result set from a cmd->queryAll(fetchAssociative=true)
	 * @param array $keys eg [0=>'id',1=>'text']
	 * @internal flattens [0=>['id'=>1,'text'=>'apple'],1=['id'=>2,'text'=>'orange']
	 *	to [1='apple',2='orange']
	 * @internal this function works like CHtml::listData() but doesn't have a group option
	 * @see also self::listData() for grouping
	 */
	public static function flattenAssocArray($data, $keys=[0=>'id',1=>'text']) {
		$out=[];
		$keyCnt = count($keys);
		if($keyCnt !== 2){
			throw new CException("'invalid key count $keyCnt keys were passed");
		}
		foreach ($data as $row) {
			$out[$row[$keys[0]]] = $row[$keys[1]];
		}
		return $out;
	}

	/**
	 * Convert an Associative array to a zero indexed tag-array for a Select2 NON-dropdown list
	 * @param array[] $data The result set from a cmd->queryAll(fetchAssociative=true)
	 * @param array $keys eg [0=>'id',1=>'text']
	 * @internal flattens [0=>['id'=>1,'text'=>'apple'],1=['id'=>2,'text'=>'orange']
	 *	to [0='apple',1='orange']
	 * @internal this function works like CHtml::listData() but doesn't have a group option
	 * @see also self::listData() for grouping
	 */
	public static function flattenAssocArrayToTags($data, $keys=[0=>'id',1=>'text']) {
		$out=[];
		$keyCnt = count($keys);
		if($keyCnt !== 2){
			throw new CException("'invalid key count $keyCnt keys were passed");
		}
		foreach ($data as $row) {
			$out[] = $row[$keys[1]];
		}
		return $out;
	}

	/**
	 * Parse tag text from a string or array that contains an item count eg. "tagText (35)"
	 * @param string | array $tags
	 * @return array $out
	 * @example converts source string "apple (45), orange (98)" to ['apple','orange']
	 * @example converts source array ['apple (45)','orange (98)'] to ['apple','orange']
	 * @example converts source array values into storable and queriable data values
	 *  converts:
	 *  ['data'=>[
	 *		['field1'=>'apple (37), orange (98)'],
	 *		['field2'=>'tables (10), chairs (15)'],
	 *  ]
	 *  to this:
	 *  ['data'=>[
	 *		['field1'=>'apple, orange'],
	 *		['field2'=>'tables, chairs'],
	 *  ]
	 *
	 * @version 1.1 enabled processing of values without a tag count to passthrough unchanged
	 * @internal Development Status = Golden!
	 */
	public static function tagTextParser($tags, $delimeter=',') {
		$regex	='/(.+?)\s\(\d+\)/iU';
		$out	=[];
		$matches=[];

		if (is_string($tags)){
			$tag_items = explode($delimeter, $tags);
		}
		if (is_array($tags)){
			$tag_items = $tags;
			$out = $tags; // insure the return array has all the same keys
		}

		foreach ($tag_items as $key=>$tag_raw) {
			if (stripos($tag_raw, $delimeter) !== false){
				$items = explode($delimeter, $tag_raw);
				$itemsProcessed=0;
				$preOut = [];
				foreach ($items as $itemText) {
					$itemsProcessed++;
					preg_match_all($regex, $itemText, $matches);
					if (isset($matches[1][0])){
						$preOut[] = trim($matches[1][0]); // handle ', ' delimiter via left trim
					} else {
						// if no match was found then a tag count didn't exist
						$preOut[] = $itemText; // the item is fine as is
					}
				}
				$out[$key] = implode($delimeter,$preOut);
			} else { // there is only one value to process ie no delimiter
				preg_match_all($regex, $tag_raw, $matches);
				if (isset($matches[1][0])){
					$out[$key] = trim($matches[1][0]); // handle ', ' delimiter via left trim
				} else {
					// if no match was found then a tag count didn't exist
					$out[$key] = $tag_raw; // the item is fine as is
				}
			}
		}
		return $out;
	}

	/**
	 * @return array[] ['table_name'=>'alias']
	 */
	public static function tableAliases() {
		return [
			'test_eval_detail_log'	=>'test results',
			'test_eval_summary_log'	=>'test results summary',
			'team_player'			=>'team player'
		];
	}

    /**
     * converts an array of AR objects or primary keys to only primary keys
     *
     * @throws CDbException
     * @param CActiveRecord[] $records
     * @return array
     */
    public static function objectsToPrimaryKeys($records)
    {
        $pks=array();
        foreach($records as $record) {
            if (is_object($record) && $record->isNewRecord){
                throw new CDbException('You can not save a record that has new related records!');
			}
            $pks[]=is_object($record) ? $record->getPrimaryKey() : $record;
        }
        return $pks;
    }

	/**
	 * Add the following methods to any model where you want to toggle the db connection
	 * @return boolean
	 * @see protected/extensions/AweCrud/components/AweActiveRecord.php
	 */
	public function switchToLocalDb()
	{
		self::$db=Yii::app()->db;
		return true;
	}
	public function switchToStagingDb()
	{
		self::$db=Yii::app()->dbStaging;
		return true;
	}

	/**
	 * Get a list of site user names in an array indexed by user table's ID
	 * @param type $tableName
	 * @param type $pk
	 * @return array[]
	 * @internal Development Status = code construction
	 */
	public static function siteUsers($tableName=null,$pk=null){
		if (!empty($tableName) && !empty($pk)){
			// caller is looking for a specific record on a specific table
			$modelName = bcommon::generateYiiModelClassName($tableName);
			$pkName = $modelName::model()->primaryKey();
			$sql = self::sqlTrim("
				select updated_by as userid from $tableName where updated_by where $pkName = :pk
				union
				select created_by as userid from $tableName where created_by where $pkName = :pk
			");
			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':pk'=>$pk];
			$data  = $cmd->queryAll($fetchAssociative = true, $params);

		} elseif (!empty($tableName)){
			// caller is looking for all rows in a specific table
			$sql = self::sqlTrim("
				select updated_by as id from $tableName where updated_by is not null group by updated_by
				union
				select created_by as id from $tableName where created_by is not null group by created_by
			");
			$cmd   = Yii::app()->db->createCommand($sql);
			$data  = $cmd->queryAll($fetchAssociative = true); // A unique list of user's that have edited this table
		} else {
			// caller is looking for all website users
			$sql   = "select id, trim(concat(ifnull(firstname,''),' ', ifnull(lastname,''))) as user from user";
			$cmd   = Yii::app()->db->createCommand($sql);
			$data  = $cmd->queryAll($fetchAssociative = true);
		}

		if ($data !== false){
			$out = [];
			foreach ($data as $row){
				// create an array with a key that is user->id
				$out[$row['id']] = $row['user'];
			}
			return $out;
		} else {
			return [];
		}
	}

//	public static function fetchRelationTree_TestHarness_v0() {
//
//		$topTier = self::fetchRelationTree();
////		$msg = "Top Tier Models Follow (save these first)";
////		YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
////		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($topTier, 10, true);
////		return;
//
//		$topTierWithChildren = $topTier;
//
//
//		$modelsYii = BaseModel::listYiiModels(); // get a list of all models
//		// Remove sql views
//		foreach ($modelsYii as $modelKey=>$modelName) {
//			$viewPrefix  = 'Vw';
//			$modelPrefix = substr($modelName, 0, 2);
//			if ($modelPrefix === $viewPrefix){
//				unset($modelsYii[$modelKey]);
//			}
//		}
//		// Returns an array with modelName a the level one key and level two keys as relationNames
//		$mRelations = BaseModel::fetchModelRelationsMultiple($modelsYii);
//
//		// next method returns relations array with	the keys of
//		// [modelName=>['byModelName','byRelationType','byRelationName']
//		//
//		$mRelationsByModelNames = self::relationsArrayToModelsArray($mRelations);
//
//		return;
//		$belongsToRelations = $mRelations;
//		$belongsToCounts    = [];
//		foreach ($mRelations as $modelName=>$relations){
//			$belongsToCount = 0;
//			foreach($relations as $relatedModel=>$relationInfo){
//				if($relationInfo['relationType'] !== 'CBelongsToRelation'){
//					unset($belongsToRelations[$modelName][$relatedModel]);
//				} else {
//					$belongsToCount++;
//				}
//			}
//			$belongsToCounts[$modelName] = $belongsToCount;
//		}
//		asort($belongsToCounts, SORT_NATURAL);
//		$out = self::belongsToSort($belongsToCounts);
//
//		self::aksort($belongsToCounts);
//		$topLevelModels = $out;
//		$valCnt = array_count_values($out);
//		array_splice($topLevelModels, $valCnt[0]);
//
//		foreach ($topTierWithChildren as $topTierModel => $topTierInt) {
////			if (array_search($topTierModel, $mRelations) !== false){
////				$mRelationIdx = array_search($topTierModel, $mRelations);
////				$topTierWithChildren[$topTierModel] = $mRelations[$mRelationIdx];
////			}
//			if (array_key_exists($topTierModel, $mRelations) !== false){
//				$topTierWithChildren[$topTierModel] = $mRelations[$topTierModel];
//				foreach($mRelations[$topTierModel] as $relationName=>$relationInfo){
//					$relatedModelName = $relationInfo['relatedModel'];
//					echo 'related model ' . $relatedModelName .'<br>';
//					$modelsByRelationType=[];
//					foreach($mRelations[$relatedModelName] as $relatedSubModelRelationName=>$relatedSubModelInfo){
//						$relatedSubModelModelName = $relatedSubModelInfo['relatedModel'];
//						$relationType = $relatedSubModelInfo['relationType'];
//						$modelsByRelationType[$relationType]=$relatedSubModelInfo;
//						echo 'related sub model ' . $relatedSubModelModelName .'<br>';
//						YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($relatedSubModelInfo, 10, true);
//					}
//				}
//				self::aksort($modelsByRelationType);
//			}
//		}
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($topTierWithChildren, 10, true);
//		// Loop through the 2nd tier tables to find out if they are owned by other relations
//
//
//
//
//		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($mRelations, 10, true);
////		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($belongsToCounts, 10, true);
////		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($belongsToRelations, 10, true);
//
//	}
//	public static function fetchRelationTree_TestHarness() {
//
//		$topTier = self::fetchRelationTree();
//		$topTierWithChildren = $topTier;
//
//		$modelsYii = BaseModel::listYiiModels(); // get a list of all models
//		// Remove sql views because they do not have any relations
//		foreach ($modelsYii as $modelKey=>$modelName) {
//			$viewPrefix  = 'Vw';
//			$modelPrefix = substr($modelName, 0, 2);
//			if ($modelPrefix === $viewPrefix){
//				unset($modelsYii[$modelKey]);
//			}
//		}
//		// Returns an array with modelName as the level one key and level two keys as relationNames
//		$mRelations = BaseModel::fetchModelRelationsMultiple($modelsYii);
//
//		// next method returns relations array with	the keys of
//		// [modelName=>['byModelName'=>[],'byRelationType'=>[],'byRelationName'=>[]]
//		$mRelationsByModelNames = self::relationsArrayToModelsArray($mRelations);
//
//		$belongsToRelations = $mRelations;
//		$belongsToCounts    = [];
//		foreach ($mRelations as $modelName=>$relations){
//			$belongsToCount = 0;
//			foreach($relations as $relatedModel=>$relationInfo){
//				if($relationInfo['relationType'] !== 'CBelongsToRelation'){
//					unset($belongsToRelations[$modelName][$relatedModel]);
//				} else {
//					$belongsToCount++;
//				}
//			}
//			$belongsToCounts[$modelName] = $belongsToCount;
//		}
//		asort($belongsToCounts, SORT_NATURAL);
//		$out = self::belongsToSort($belongsToCounts);
//
//		self::aksort($belongsToCounts);
//		$topLevelModels = $out;
//		$valCnt = array_count_values($out);
//		array_splice($topLevelModels, $valCnt[0]);
//
//		foreach ($topTierWithChildren as $topTierModel => $topTierInt) {
//			if (array_key_exists($topTierModel, $mRelations) !== false){
//				// determine if second tier models have other owners besides the top tier models
//				$topTierWithChildren[$topTierModel] = $mRelationsByModelNames[$topTierModel];
//			}
//		}
//
//		$prequisites = self::fetchRelationTree_LowerTiers($topTierWithChildren, $mRelationsByModelNames);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($prequisites, 10, true);
//		return;
//
//	}


	/**
	 * Convert a series of relations arrays into usable reference arrays. The array
	 *   returned is keyed by modelName rather than relationName and has the relations
	 *   for the model in sorted categories.
	 * @param array $mRelations [[modelName][['relationType','relatedModel','fkey','eSaveCompatible']]]
	 * @return array $out  [modelName=>['byModelName'=>[],'byRelationType'=>[],'byRelationName'=>[]]
	 * @internal Development Status = Golden!
	 * @version 1.0
	 * @uses self::relationsByTypeArrayToModelsArray()
	 * @see BaseModel::fetchModelRelations($modelName, $eSaveOnly=false);
	 * @deprecated since version 0.54.3
	 */
	public static function relationsArrayToModelsArray($mRelations){
		//$relationsInputTemplate = [['relationName'][['relationType','relatedModel','fkey','eSaveCompatible']]];
		$out=[];
		$byRelationTypeLog=[];
		$localDebug = false;
		//$modelNamesToRelationNames=[];
		//$relationNamesToModelNamesTo=[];
		foreach ($mRelations as $modelName=>$relations){
			$byModelName=[];    // key='byModelName'
			$byRelationName=[]; // key='byRelationName
			$byRelationType=[]; // key='byRelationType'
			// The next block used to create a breakpoint in x-debug that won't break the
			//   the netbeans memory heap after several uses. To use just change the model name
			//   to whatever needs deep inspection
//			if ($modelName == 'SubscriptionStatusType'){
//				echo '<br>';
//			}
			foreach($relations as $relationName=>$relationInfo){
				ksort($relationInfo);
				$relatedModelName = $relationInfo['relatedModel'];
				$relationType     = $relationInfo['relationType'];
				// fixed the need for the next line in
				//$relationInfo['relationName']    = $relationName;

				$byModelName[$relatedModelName]  = $relationInfo;
				// There is an inherent one-to-many on relation types
				$byRelationType[$relationType][] = $relationInfo;
				$byRelationName[$relationName]   = $relationInfo;
			}
			// can the sub arrays be sorted? Yes, if empty arrays are skipped.
			//self::aksort($byModelName); // this will fail type conversion

			// Handle the numeric-index relationType array by substituting numeric indexes with related model names
			$byRelationType_numericIndexValues = $byRelationType;
			unset($byRelationType);
			$byRelationType  = [];
			// enable sorting by related model
			//$relatedModelsDeep=[];
			foreach($byRelationType as $relationTypeKey=>$relationInfoIdxArray){
				//$byRelationTypeNew[$relationTypeKey][]=$relationInfoIdxArray;
				foreach($relationInfoIdxArray as $relationInfoArray){
					ksort($relationInfoArray);
					$relatedModelNameDeep = $relationInfoArray['relatedModel'];
					//$relatedModelsDeep[$relationTypeKey]=[];
					$byRelationType[$relationTypeKey][$relatedModelNameDeep]=$relationInfoArray;
				}
			}
			if (count($byRelationType) > 0){
				//$byRelationTypeNewBfrSort = $byRelationType;
				ksort($byRelationType, SORT_NATURAL);
			}

			if (count($byModelName) > 0){
				ksort($byModelName, SORT_NATURAL);
			}
			if (count($byRelationName) > 0){
				ksort($byRelationName, SORT_NATURAL);
			}

			if (count($byRelationType) > 0){

				ksort($byRelationType, SORT_NATURAL);
				$byRelationTypeLog[$modelName][]=$byRelationType;
				// The next method keys the array under the relation type by modelName instead
				//   of a numeric index.
				//$byRelationTypeWithModelKey = self::relationsByTypeArrayToModelsArray($byRelationType);
			} else {
				// since this var is not reset at the top of the loop it is reset here.
				//$byRelationTypeWithModelKey=[];
			}

			$out[$modelName] = [
				'byModelName'   =>$byModelName,
				//'byRelationType'=>$byRelationType,
				// next line was used for debugging byRelationType
				//'byRelationTypeIndexed'=>$byRelationType,
				// The function that was called for the next line has a bug that remains unfixed
				//'byRelationType'=>$byRelationTypeWithModelKey,
				'byRelationType'=>$byRelationType,
				'byRelationName'=>$byRelationName,
			];
		}
		if ($localDebug){
			$out['debug'] = $byRelationTypeLog;
		}
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($out, 10, true);
		return $out;
	}


	/**
	 * Convert an indexed array to assoc array with the model name as the key
	 * @param type $relationsByType
	 * @internal called by BaseModel::relationsArrayToModelsArray()
	 * @internal Development Status = Golden!
	 *
	 */
	protected static function relationsByTypeArrayToModelsArray($relationsByType) {
		$out=[];

		foreach ($relationsByType as $relationType=>$indexedRelationsArray){
			$byModel=[];
			$modelName = $indexedRelationsArray['relatedModel'];
			$byModel[$modelName] = $indexedRelationsArray;
//			foreach($indexedRelationsArray as $relationPropertyName=>$relationPropertyValue){
//				//$modelName = $relationDetail['relatedModel'];
//				//$byModel[$modelName][$relationName]=$relationDetail;
//			}
			if (count($byModel, COUNT_RECURSIVE) > 1){
				ksort($byModel,SORT_NATURAL);
			}
			$out[$relationType]=$byModel;
		}
		return $out;
	}

	/**
	 *
	 * @param type $array
	 * @param type $valrev
	 * @param type $keyrev
	 * @return type
	 */
	protected static function belongsToSort($array,$valrev=false,$keyrev=false) {

		if ($valrev) { arsort($array); } else { asort($array); }
		$vals = array_count_values($array);
		$i = 0;
		foreach ($vals AS $num) {
			$first = array_splice($array,0,$i);
			$tmp = array_splice($array,0,$num);
			if ($keyrev) { krsort($tmp); } else { ksort($tmp); }
			$array = array_merge($first,$tmp,$array);
			unset($tmp);
			$i = $num;
		}
		return $array;
	}

	/**
	 * Sort a multi-dimensional array by associative key then by value
	 * @param array $array A multi-dimensional array to be sorted by associative key then by value
	 * @param bool $valrev Value sorted ASC by default eg false=ASC, true=DESC sort
	 * @param bool $keyrev Key sorted ASC by default eg false=ASC, true=DESC sort
	 * @see http://php.net/manual/en/function.asort.php
	 * @author Nick <nick@nickyost.com>
	 * @internal This method is also in the Barray class. This copy is located in this
	 *   class because it is called frequently by methods in this class and I wanted
	 *   to reduce the overhead of calling another static class loading
	 */
	protected static function aksort(&$array,$valrev=false,$keyrev=false) {
		if ($valrev) { arsort($array); } else { asort($array); }
		$vals = array_count_values($array);
		$i = 0;
		foreach ($vals AS $num) {
			$first = array_splice($array,0,$i);
			$tmp = array_splice($array,0,$num);
			if ($keyrev) { krsort($tmp); } else { ksort($tmp); }
			$array = array_merge($first,$tmp,$array);
			unset($tmp);
			$i = $num;
		}
	}

	/**
	 *
	 * @param type $returnTieredList
	 * @return type
	 */
	public static function fetchRelationTree($returnTieredList=false) {
		// Get models to persist first
		$modelTree = self::fetchRelationTree_AllTiers($returnTieredList);

		return $modelTree;
	}

	/**
	 * Get a list of models which are not owned by any other model. These
	 *   are the models that must be saved first in a generic store chain.
	 */
	public static function fetchRelationTree_TopTier() {
		$modelsYii = BaseModel::listYiiModels(); // get a list of all models
		// Remove sql views
		foreach ($modelsYii as $modelKey=>$modelName) {
			$viewPrefix  = 'Vw';
			$modelPrefix = substr($modelName, 0, 2);
			if ($modelPrefix === $viewPrefix){
				unset($modelsYii[$modelKey]);
			}
		}
		$mRelations = BaseModel::fetchModelRelationsMultiple($modelsYii);
		$belongsToRelations = $mRelations;
		$belongsToCounts    = [];
		foreach ($mRelations as $modelName=>$relations){
			$belongsToCount = 0;
			foreach($relations as $relatedModel=>$relationInfo){
				if($relationInfo['relationType'] !== 'CBelongsToRelation'){
					unset($belongsToRelations[$modelName][$relatedModel]);
				} else {
					$belongsToCount++;
				}
			}
			$belongsToCounts[$modelName] = $belongsToCount;
		}
		asort($belongsToCounts, SORT_NATURAL);
		$out = self::belongsToSort($belongsToCounts);

		//self::aksort($belongsToCounts);
		$topLevelModels = $out;
		$valCnt = array_count_values($out);
		array_splice($topLevelModels, $valCnt[0]);

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($mRelations, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($belongsToCounts, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($belongsToRelations, 10, true);
		return $topLevelModels;
	}

	/**
	 *
	 * @param type $topTierWithChildren
	 * @param type $mRelationsByModelNames
	 * @return type
	 */
	protected static function fetchRelationTree_LowerTiers($topTierWithChildren, $mRelationsByModelNames) {
		// determine if second tier models have other owners besides the top tier models
		$relationsSkipped=[];
		$topTier = array_keys($topTierWithChildren);
		foreach($topTierWithChildren as $topTierModelName=>$relationCategories){
			if (! isset($relationCategories['byRelationType'])){
				$relationsSkipped[]=[$topTierModelName=>'byRelationType not set'];
				continue;
			}
			// Loop through the secondary models looking for more owners of the secondary models
			if( !isset($relationCategories['byModelName']) ){
				$relationsSkipped[]=[$topTierModelName=>'byModelName not set'];
				continue;
			}elseif( count($relationCategories['byModelName']) == 0){
				$relationsSkipped[]=[$topTierModelName=>'byModelName count = zero'];
				continue;
			}
			$tierTwoModelNames = array_keys($relationCategories['byModelName']);
			$tierTwoModelsToProcessFirst=[];
			$tierTwoModelsWithOtherOwners=[];
			foreach($tierTwoModelNames as $tierTwoModelName){
				$secondTierRelations = $mRelationsByModelNames[$tierTwoModelName];
				if(isset($secondTierRelations['byRelationType']['CBelongsToRelation'])){
					$secondTierOwners = array_keys($secondTierRelations['byRelationType']['CBelongsToRelation']);
					if(array_search($topTierModelName, $secondTierOwners) !== false){
						$topTierModelNameIdx = array_search($topTierModelName, $secondTierOwners);
						unset($secondTierOwners[$topTierModelNameIdx]);
					}
				}
				if (count($secondTierOwners) == 0){
					$tierTwoModelsToProcessFirst[] = $tierTwoModelName;
				} else {
					$secondTierOwnersDiffed = array_diff($secondTierOwners, $topTier);
					//$tierTwoModelsWithOtherOwners[$tierTwoModelName]=$secondTierOwners;
					$tierTwoModelsWithOtherOwners[$tierTwoModelName]=$secondTierOwnersDiffed;
				}
			}
			$topTierWithChildren[$topTierModelName]['hasNoPrerequites'] = $tierTwoModelsToProcessFirst;
			$topTierWithChildren[$topTierModelName]['hasPrerequites']  = $tierTwoModelsWithOtherOwners;
		}

		// Build model processing order
		//   level one = top tier models with no prerequisites
		//   level two = second tier no prerequisites
		//   level three = prerequisties for tier four array_values
		//   level
		//   level four remaining models
		$modelsYii = BaseModel::listYiiModels(); // get a list of all models
		// Remove sql views
		foreach ($modelsYii as $modelKey=>$modelName) {
			$viewPrefix  = 'Vw';
			$modelPrefix = substr($modelName, 0, 2);
			if ($modelPrefix === $viewPrefix){
				unset($modelsYii[$modelKey]);
			}
		}


		$modelProcessTree=[];
		foreach($topTierWithChildren as $topTierModelName=>$relationInfo){
			$modelProcessTree['tier 1'][]=$topTierModelName;

			if (isset($relationInfo['hasNoPrerequites'])){
				if (count($relationInfo['hasNoPrerequites']) > 0){
					foreach($relationInfo['hasNoPrerequites'] as $modelName){
						$modelProcessTree['tier 2'][]=$modelName;
					}
				}
			}

			if (isset($relationInfo['hasPrerequites'])){
				if (count($relationInfo['hasPrerequites']) > 0){
					foreach ($relationInfo['hasPrerequites'] as $tierTwoModelName=>$prerequisites){
						if (isset($modelProcessTree['tier 4'])){
							if(! (array_search($tierTwoModelName, $modelProcessTree['tier 4']) !== false) ){
								$modelProcessTree['tier 4'][]=$tierTwoModelName;
							}
						} else {
							$modelProcessTree['tier 4'][]=$tierTwoModelName;
						}

						foreach($prerequisites as $prereqModel){

							if (isset($modelProcessTree['tier 3'])){
								if(! (array_search($prereqModel, $modelProcessTree['tier 3']) !== false) ){
									$modelProcessTree['tier 3'][]=$prereqModel;
								}
							} else {
								$modelProcessTree['tier 3'][]=$prereqModel;
							}

						}
					}

				}
			}
		}

		$modelsInTree=[];
		foreach ($modelProcessTree as $models){
			foreach ($models as $modelName) {
				$modelsInTree[] = $modelName;
			}
		}

		$remainingModels = array_diff($modelsYii, $modelsInTree);
		foreach($remainingModels as $modelName){
			$modelProcessTree['tier 5'][] = $modelName;
		}

		ksort($modelProcessTree);

		$modelsInTreeFinal=[];
		foreach ($modelProcessTree as $models){
			foreach ($models as $modelName) {
				if(! (array_search($modelName, $modelsInTreeFinal) !== false) ){
					$modelsInTreeFinal[] = $modelName;
				}
			}
		}

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($modelProcessTree, 10, true);
		return ['tieredList'=>$modelProcessTree, 'simpleList'=>$modelsInTreeFinal];
	}

	/**
	 *
	 * @param type $returnTieredList
	 * @return type
	 * @example BaseModel::fetchRelationTree_AllTiers($returnTieredList=false);
	 */
	public static function fetchRelationTree_AllTiers($returnTieredList=false) {

		// Get models to persist first
		$topTier = self::fetchRelationTree_TopTier();

		$topTierWithChildren = $topTier;

		$modelsYii = BaseModel::listYiiModels(); // get a list of all models
		// Remove sql views because they do not have any relations
		foreach ($modelsYii as $modelKey=>$modelName) {
			$viewPrefix  = 'Vw';
			$modelPrefix = substr($modelName, 0, 2);
			if ($modelPrefix === $viewPrefix){
				unset($modelsYii[$modelKey]);
			}
		}
		// Returns an array with modelName as the level one key and level two keys as relationNames
		$mRelations = BaseModel::fetchModelRelationsMultiple($modelsYii);

		// next method returns relations array with	the keys of
		// [modelName=>['byModelName'=>[],'byRelationType'=>[],'byRelationName'=>[]]
		$mRelationsByModelNames = self::relationsArrayToModelsArray($mRelations);

		$belongsToRelations = $mRelations;
		$belongsToCounts    = [];
		foreach ($mRelations as $modelName=>$relations){
			$belongsToCount = 0;
			foreach($relations as $relatedModel=>$relationInfo){
				if($relationInfo['relationType'] !== 'CBelongsToRelation'){
					unset($belongsToRelations[$modelName][$relatedModel]);
				} else {
					$belongsToCount++;
				}
			}
			$belongsToCounts[$modelName] = $belongsToCount;
		}
		asort($belongsToCounts, SORT_NATURAL);
		$out = self::belongsToSort($belongsToCounts);

		self::aksort($belongsToCounts);
		$topLevelModels = $out;
		$valCnt = array_count_values($out);
		array_splice($topLevelModels, $valCnt[0]);

		foreach ($topTierWithChildren as $topTierModel => $topTierInt) {
			if (array_key_exists($topTierModel, $mRelations) !== false){
				// determine if second tier models have other owners besides the top tier models
				$topTierWithChildren[$topTierModel] = $mRelationsByModelNames[$topTierModel];
			}
		}
		unset($topTierModel, $topTierInt);

		$modelLists = self::fetchRelationTree_LowerTiers($topTierWithChildren, $mRelationsByModelNames);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($modelLists, 10, true);
		if ($returnTieredList == true){
			return $modelLists['tieredList'];
		} else {
			return $modelLists['simpleList'];
		}
	}

	/**
	 * Allows the storage of any model(s) for updates and|or creates and related models too
	 *   Replaces having to call Controller->actionCreate() or Controller->actionUpdate()
	 *   because the actionCreate() will NOT handle related data linkages. This method does.
	 * Will handle any number of Parent tables and thier related children ONE LEVEL deep.
	 *   This method can not handle Children of Children or Parents of Parents, only
	 *   BaseModel::storeChain() can handle such a data save operation.
	 * @param array $post If a data array is not passed the $_POST array will be used
	 * @return array $pkArray ['id'=>$id, 'modelsCreated' => $modelsCreated,
			'baseModelErrors'=>$errors,'relationsCrud'=>$relationsCrudScenario]
	 * @uses BaseModel::StoreWith()
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <danabyrd@byrdbrain.com>
	 * @todo Add ajax validation for each model processed? Just use validation on save().
	 * @todo Add handling of param $readOnly
	 * @todo Add a db transaction wrapper.
	 */
	public static function StoreGeneric($post=null,$runValidation=true, $safeOnlyAttr=false, $isReadOnly=false){
		// Determine if model data is in the post array
		$modelsYii = BaseModel::listYiiModels(); // get a list of all models
		//$_POST = array_merge($_POST, $post);  // todo-Done remove this after dev testing
		if (empty($post)){
			$post = $_POST;
		}
		if (count($post) ==0){
			throw new CException('a fieldValueArray via $post or $_POST is a required parameter. $post is empty', 707);
		}
		// Any model names in the POST will be top level items eg keys
		// Each model-data array may have relational data within it using the Yii relation names as a key for the related data.
		$modelsToProcessProposed = array_keys($post);
		$modelsToProcess = [];
		foreach ($modelsToProcessProposed as $modelNameProposed){
			//echo $modelNameProposed;
			if (array_search($modelNameProposed, $modelsYii, $strict=true) !== false){
				$modelsToProcess[] = $modelNameProposed;
			}
		}

		// Assert the assumption that the models are related
		$mRelationsOfModelsToProcess = BaseModel::fetchModelRelationsMultiple($modelsToProcess);
		//$mRelationsByModelNames      = BaseModel::relationsArrayToModelsArray($mRelationsOfModelsToProcess);
//		foreach($mRelationsByModelNames as $mRelationInfo){
//			//echo $key = $relations . "<br>";
//			//$bogusVar = $mRelationInfo; // used to add a break point
//		}
		$pkArray=[];
		$scmdaOut=[];
		foreach($modelsToProcess as $modelName){
			// Pass one top-level model and its related data
			if (count($post[$modelName]) > 0){
				//$pkArray[$modelName] = self::StoreWith([$modelName=>$post[$modelName]]);
				//$pkArray[$modelName] = self::StoreWith([$modelName=>$post[$modelName]],$runValidation,$readOnly);
				$pkArray[$modelName] = self::StoreWith($post,$runValidation,$safeOnlyAttr=false,$isReadOnly=false);
				$scmdaOut[]=$pkArray[$modelName]['scmda'];
			}
		}

		return $pkArray;
	}

	/**
	 *
	 * @param array $mmava Multiple-models-attribute-values-array. The models are
	 * stacked [org,person,player, etc] and NOT nested by relationship.
	 *
	 * @internal
	 *   Calls a function to nest the model arrays by relationship. Only BELONGS_TO
	 *   relations are nested. Then calls a generic method to store the nested models.
	 * @internal
	 *   Calls: self::convertMmavaToActiveRecordDataArray($mmava, $returnDeepArray=true)
	 *   Calls: model->saveWithParents() eg saves model and BELONGS_TO relations
	 *          OR
	 *          calls a custom store routine if found eg model::Store() a
	 *          hard-coded storage routine on the model.
	 * @uses self::convertMmavaToActiveRecordDataArray()
	 * @uses AweActiveRecord->saveWithParents()
	 * @internal Development Status = ready for testing (rft)
	 * @internal Known issues: self::convertMmavaToActiveRecordDataArray() will
	 * seek to arrange a relation stack in each way it can. This will result in
	 * duplicate record creation. The function must be provided a pre-defined scenario.
	 */
	public static function StoreMmavaAsAweActiveRecords($mmava, $runValidation=true, $formName=''){
		// @todo enable arda call below after testing
		// arda = ActiveRecordsDataArray - A model with nested relations arrays
		//        eg. [Person[org=>Org,players=>Player]]

		// Validate the model names passed in
		$yiiModels = BaseModel::listYiiModels();
		$arrayKeysMmava = array_keys($_POST);
		$validModelNamesInMmava = array_intersect($yiiModels, $arrayKeysMmava);

		// Handle the scenario where a single model is passed in
		//$modelsInMmava = array_keys($mmava);
		$modelsInMmava = $validModelNamesInMmava;
		$mmavaLocal=[];
		// maintain the order of the array keys passed in
		foreach($arrayKeysMmava as $modelNameCandidate){
			if (in_array($modelNameCandidate, $modelsInMmava)){
				$mmavaLocal[$modelNameCandidate] = $mmava[$modelNameCandidate];
			}
		}
		unset($modelNameCandidate);
		$modelsInMmavaCount = count($modelsInMmava);
		if ($modelsInMmavaCount > 1){
			$returnDeepArray=true;
			$arda = self::convertMmavaToActiveRecordDataArray($mmavaLocal, $returnDeepArray);
		} elseif ($modelsInMmavaCount === 1){
			//$arda = $mmava;
			$returnDeepArray=false;
			$arda = self::convertMmavaToActiveRecordDataArray($mmava, $returnDeepArray);

		}
		$hasFatalError=false;
		//$arda = $mmava;
		if (is_array($arda)){
			$countAll = count($arda, COUNT_RECURSIVE);
			if ($countAll === 0){
				throw new CException("A empty arda array is not allowed", 707);
			}
			$isAssoc = Barray::is_assoc($arda);
			if ($isAssoc===true){
				$modelsToProcess = array_keys($arda);
			} else {
				$modelsToProcess = [];
			}
		} else {
			throw new CException("A data array is required", 707);
		}
		$scmda  = BaseModel::getStoreChainMetaData_ArrayTemplate();
		//$result = [];
		//if (count($modelsToProcess) == 0){
			// its an indexed array, eg the keys are numeric
			// And the models to process are unknown
			foreach ($arda as $modelName=>$modelData){
				if ($hasFatalError===true){
					break;
				}

				$modelDataWithModelNameAsKey=[$modelName=>$modelData];
				$mData = $modelDataWithModelNameAsKey;
				$referenceModel = new $modelName(); // @todo remove the next criterion suffix after testing
				if (method_exists($referenceModel, 'Store') && (false===true)){
					// if a hard-coded method exists then use it
					$scmda['scenario'][$modelName] = "Called custom Store() handler on " . $modelName;
					// @todo insure all store methods
					//$scmda[$modelName] = $modelName::Store($modelData);
					$scmda[$modelName] = $referenceModel->Store($mData);

				} else {
					// if custom method doesn't exist then use the generic method
					$scmda['scenario'][$modelName] = "Called saveWithParents() handler on " . $modelName;
					// $modelName, array $data, $runValidation=true, $formName=''){
					// $pkTest = Person::model()->saveWithParents($modelName, $modelData,$runValidation);
					//$pk = $referenceModel->saveWithParents($modelName,$mData,$runValidation,$formName);
					$pk = $referenceModel->saveWithRelated($modelName,$mData,$runValidation,$formName);
					//Person::model()->save

					if (is_string($pk)){
						$pkName = $referenceModel->primaryKey();
						$scmda['pkeys'][$pkName] = $pk;
						$scmda['success'][] = "saved " . $modelName;
					} elseif (is_array($pk)){
						$scmda['errors'][$modelName] = $pk;
					}
					// throw exception or use a generic store method
					// that would work for any model such as
					// AthleteTeams::storeModel()
				}

			}
		//}
		return $scmda;
	}

	/**
	 * Used to run ad-hoc tests on the two methods in StoreMmava... call stack
	 * @param string $testType 'arda'|'save'
	 */
	public static function StoreMmavaAsAweActiveRecords_testHarness($testType='save'){
		//$testType = 'arda';
		//$testType = 'save';

		// This json string is already in a mtfva format.
		$jsonStr = BaseModel::sqlTrim('
			{
			"Player":
			{
			"player_id":"78"
			}
			,"Person":
			{
			"person_id":"157"
			}
			,"OrgType":
			{
			"org_type_id":"2"
			}
			,"Org":
			{
			"org_type_id":"2"
			}
			,"TeamPlayer":
			{
			"team_player_id":"77","team_id":"6","coach_name":"coach #4aslk","coach_id":"80"
			}
			,"TeamOrg":
			{
			"org_type_id":"2","org_id":"1177","org_website_url":"http:\/\/clubsite.org"
			}
			,"Team":
			{
			"org_id":"1177","team_id":"6","team_league_id":"1","team_age_group":"U11","team_website_url":"http:\/\/teamsite.org"
			}
			,"TeamLeague":
			{
			"team_league_id":"1"
			}

			}'
		);

		$jsonArray = CJSON::decode($jsonStr, $useArray=true);
		$mmava     = $jsonArray;

		if ($testType == 'arda'){
			$arda      = self::convertMmavaToActiveRecordDataArray($mmava);

			// echo the data for debugging
			$localDebugDataInput = false;
			if ($localDebugDataInput){
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($arda, 10, true);
			}
			return $arda;
		}

		$localDebugDataSave = false;
		if ($testType == 'save'){
			$result = self::StoreMmavaAsAweActiveRecords($mmava);
			if ($localDebugDataSave){
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($result, 10, true);
			}
			return $result;
		}

	}

	/**
	 * Convert an mmava array to an arda array (model name indexed arrays to a
	 * nested relationName indexed arrays). Uses model relationships to
	 * build the $arda Active record data array using the models in mmava array.
	 *
	 * @param array $mmava Multiple-model-attribute-values-array (indexed by model name)
	 * @return array $arda ActiveRecord-Data-Array
	 * @internal Development Status = ready for testing (rft)
	 * @todo Prevent dupes in the output array
	 * @todo Validate mmava data tiers eg
	 *   if relation is void of legit vals then flush it. Debug this upstream
	 *   first, then here.
	 */
	public static function convertMmavaToActiveRecordDataArray($mmava, $returnDeepArray=false, $formName=null){
		//$returnDeepArray=true;
		//$returnDeepArray=false;
		$yiiModels  = self::listYiiModels();
		$arrayKeys = array_keys($mmava);
		$validModelNamesInMmava = array_intersect($yiiModels, $arrayKeys);
		if (count($validModelNamesInMmava) !== count($arrayKeys)){
			throw new CException("An invalid model name was passed as a parameter", 707);
		} elseif (count($arrayKeys) == 0){
			throw new CException("An array indexed by Yii model names is required", 707);
		}

		$modelsInMmava = $validModelNamesInMmava;
		$mRelations    = self::fetchModelRelationsMultipleAsSparseList($modelsInMmava, $modelsAllowed=$modelsInMmava);

		$out=[];
		$outTemp=[];
		$relationsFound = [];
		$modelsInMmavaProcessed = [];
		foreach ($modelsInMmava as $modelName){ // consider reordering, or resetting the keys

			//$relations = $mRelations[$modelName]['byModelName'];
			$relationsTemp = [];
			$relationsTemp[$modelName]=$mmava[$modelName];

			if ( isset($mRelations[$modelName]['byRelationType']['CBelongsToRelation']) ){
				$relations = $mRelations[$modelName]['byRelationType']['CBelongsToRelation']; // CBelongsToRelation
			} else {
				continue;
			}

			$relatedModels = array_keys($relations);

			foreach ($relatedModels as $relatedModelName) {

				if (in_array($relatedModelName, $modelsInMmava) ){
					// yep its there
					//$relationsFound[]=[$relatedModelName=>[]]
				}
				if ( isset($mmava[$relatedModelName]) ){
					// A related model exists in the mmava
					$relationName = $relations[$relatedModelName]['relationName'];
					if (! isset($relationsTemp[$modelName][$relationName])){

						$relationsTemp[$modelName][$relationName] = $mmava[$relatedModelName];
						$modelsInMmavaProcessed[] = $relatedModelName;

					}
				}
			}
			unset($relatedModelName);
			$relationsFound[$modelName] = $relationsTemp[$modelName];
			if ($returnDeepArray===false){
				if ($formName === 'athlete-edit-form'){
					// @todo remove unused arrays
					$x=1;
				}
				// Return an array with only one tier of relations
				$out[$modelName] = $relationsTemp[$modelName];
				return $out;
			}
		}
		if ($returnDeepArray===false && count($relationsFound) === 0){
			//$out = $relationsTemp[$modelName];
			$out = $mmava;             // return [modleName=>[attributes]
			//$out = $mmava[$modelName]; // return only the attributes eg [attributes]
		}

		if ($returnDeepArray===true){
			// $out[] should be empty
			$modelsFound     = array_keys($relationsFound);
			//$mRelationsFound = self::fetchModelRelationsMultipleAsSparseList($modelsFound, $modelsAllowed=$modelsInMmava);
			//$potentialRootModels=[];
			foreach ($modelsFound as $modelNameTemp){
				$parentModels = array_keys(
					$mRelations[$modelNameTemp]['byRelationType']['CBelongsToRelation']
				);
				$outTemp[$modelNameTemp] = $relationsFound[$modelNameTemp];
				// if parent model is in the intermediary list then compress
				foreach($parentModels as $parentModelName){
					if (in_array($parentModelName, $modelsFound)){
						$relationName = $mRelations[$modelNameTemp]['byRelationType']
								['CBelongsToRelation'][$parentModelName]['relationName'];
						$outTemp[$modelNameTemp][$relationName] = $relationsFound[$parentModelName];
					}
				}
				unset($parentModelName);
			}
			unset($modelNameTemp);
			$out = $outTemp;
		}

		// @toto Reorder the array returned using the master relation tree
		// $relationTree = BaseModel::fetchRelationTree(); // for production
		// $relationTree = AthleteTeams::getRelationsAsSparseList();  // for construction-testing

		$arda = $out;
		return $arda;
	}

	/**
	 * @internal Development Status = code construction
	 */
	public static function StoreModels__TestHarness(){

	}

	/**
	 * Consumes data processed by self::convertMmavaToActiveRecordDataArray()
	 * or deeply related model data that comes directly from a GSM UI
	 *
	 * @param array $data mmava structure [Person=>[],Org=>[],Player=>[]]
	 *        The same structure that is retuned in a POST array in many instances.
	 * @param bool $isReadOnly
	 * @internal Development Status = ready for testing (rft)
	 * @todo if a model passed in is in the list of AthleteTeams component-models
	 *   then consider using AthleteTeams::storeModels() to save the data.
	 * @todo consume the isReadOnly flag for testing
	 * @since 0.54.3
	 * @package Athlete Resumes
	 */
	public static function StoreModelsWithERelated($data, $isReadOnly=false){
		//
		if (is_array($data)){
			$isAssoc = Barray::is_assoc($data);
			if ($isAssoc===true){
				$modelsToProcess = array_keys($data);
			} else {
				$modelsToProcess = [];
			}
		} else {
			throw new CException("A data array is required", 707);
		}
		$result = [];
		if (count($modelsToProcess) == 0){
			// its an indexed array, eg the keys are numeric
			foreach ($data as $modelName=>$modelData){
				$referenceModel = new $modelName();
				if (method_exists($referenceModel, 'Store')){
					//$result[$modelName] = $modelName::Store($modelData);
					// Use static model to load the store method
					$result[$modelName] = $modelName::model()->Store($modelData);
					// Use reference model
					$result[$modelName] = $referenceModel->Store($modelData);
				} else {
					// throw exception or use a generic store method
					// that would work for any model such as
					// AthleteTeams::storeModel()
				}

			}
		}
		return $result;
	}

	/**
	 *
	 * @param string $testName
	 * @throws CException
	 *
	 */
	public static function StoreWithERelated_TestHarness($testName){
		$validTests = [
			'teamSchool',
			'teamClub',
			'teamSelect',
			'eventCamp',
			'eventTournament',
		];
		if (! (array_search($testName, $validTests) !== false) ){
			throw new CException('Invalid test name parameter passed', 777);
		}
		// team, org, person, player, playerTeam
		$teamDeep = [
			'Team'=>[
				'org'       =>[],
				'person'    =>[],
				'player'    =>[],
				'teamPlayer'=>[],
			]
		];

		// <editor-fold defaultstate="collapsed" desc="Team School test data block">
		// Sport, School Name, School team level, current team, team website,
		// competion season, governing body, division, league, position1, position2,
		// schedule, statistical highlights, player profile.
		$teamSchool = [
			'org'       =>[
				'org_level_id'   =>1, // ?
				'org_website_url'=>'https://google.com',
			],
			'team'	=>[
				'sport_id'        =>20, // ? soccer
				'team_website_url'=>'https://google.com', // must be added
				'division_id'     =>3, // ?
			],
			'person'    =>[

			],
			'player'    =>[

			],
			'teamPlayer'=>[
				'team_id'=>14, // ?
				'team_play_sport_position_id'  => 12, // ?
				'team_play_sport_position2_id' => 1, // ?
			],
		];
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Team Club test data block">
		// Sport, School Name, School team level, current team, team website,
		// competion season, governing body, division, league, position1, position2,
		// schedule, statistical highlights, player profile.
		$teamClub = [
			'org'       =>[
				'org_level_id'   =>1, // ?
				'org_website_url'=>'https://google.com',
			],
			'team'	=>[
				'sport_id'        =>20, // ? soccer
				'team_website_url'=>'https://google.com', // must be added
				'division_id'     =>3, // ?
			],
			'person'    =>[

			],
			'player'    =>[

			],
			'teamPlayer'=>[
				'team_id'=>14, // ?
				'team_play_sport_position_id'  => 12, // ?
				'team_play_sport_position2_id' => 1, // ?
			],
		];
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Team Select test data block">
		$teamSelect = [
			'org'       =>[
				'org_level_id'   =>1, // ?
				'org_website_url'=>'https://google.com',
			],
			'team'	=>[
				'sport_id'        =>20, // ? soccer
				'team_website_url'=>'https://google.com', // must be added
				'division_id'     =>3, // ?
			],
			'person'    =>[

			],
			'player'    =>[

			],
			'teamPlayer'=>[
				'team_id'=>14, // ?
				'team_play_sport_position_id'  => 12, // ?
				'team_play_sport_position2_id' => 1, // ?
			],
		];
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Event Camp test data block">
		$eventCamp = [
			'org'       =>[
				'org_level_id'   =>1, // ?
				'org_website_url'=>'https://google.com',
			],
			'team'	=>[
				'sport_id'        =>20, // ? soccer
				'team_website_url'=>'https://google.com', // must be added
				'division_id'     =>3, // ?
			],
			'person'    =>[

			],
			'player'    =>[

			],
			'teamPlayer'=>[
				'team_id'=>14, // ?
				'team_play_sport_position_id'  => 12, // ?
				'team_play_sport_position2_id' => 1, // ?
			],
		];
		// </editor-fold>

		// <editor-fold defaultstate="collapsed" desc="Event Tournament test data block">
		$eventTournament = [
			'org'       =>[
				'org_level_id'   =>1, // ?
				'org_website_url'=>'https://google.com',
			],
			'team'	=>[
				'sport_id'        =>20, // ? soccer
				'team_website_url'=>'https://google.com', // must be added
				'division_id'     =>3, // ?
			],
			'person'    =>[

			],
			'player'    =>[

			],
			'teamPlayer'=>[
				'team_id'=>14, // ?
				'team_play_sport_position_id'  => 12, // ?
				'team_play_sport_position2_id' => 1, // ?
			],
		];
		// </editor-fold>

		$data = [
			'teamSchool'=>$teamSchool,
			'teamClub'  =>$teamClub,
			'teamSelect'=>$teamSelect,
			'eventCamp' =>$eventCamp,
			'eventTournament'=>$eventTournament,
		];

		self::StoreWithERelated($data[$testName]);

		// Test the data writes
		// The next line will not work. Only a view or custom sql select will find all the data elements
		// The models are linked together in a way that the relations can be chained which is why StoreChain was created.
		//$dbModel = TeamPlayer::model()->with(['Player','Person','Team','Org'])->findByPk($playerID);
	}

	/**
	 * saveRelated() and saveWithRelated() can only handle and a single parent model and it's children.
	 *   In the case of the Person model that is 20 tables, but saving on one Parent model and
	 *   its children is a limitation.
	 * To handle truly generic save operations GSM needs to ins/upd the Parent of Parent as well.
	 *   And up to six levels of relations deep. Thus far BaseModel::storeChain() is the only method
	 *   in our data save methodologies with this 6+ table chain capability.
	 * @param type $data Model attributes array that includes attributes to store in related models.
	 */
	public static function StoreWithERelated($data){
		// saveWithRelated has been modified to prevent the need for passing individual
		//   relations as parameters, and the need to send 'append' as a relation
		//   parameter has also been removed from the GSM version of CActiveRecord->ESaveRelatedBehavior
		// $model->saveWithRelated( array('relationName1' => array('append' => true)));
		// $model->saveWithRelated( ['relationName1' => ['append' => true]]);
		// $model->saveWithRelated( ['relationName1'=>['append'=> true], 'relationName2=>['append'=>true]]);
	}

	/**
	 * Uses model relations meta-data to create a yii model normative data array
	 * @param string CModel class name eg a CompositeModel composed from a sql view of several models
	 * @param array $mtfva (multi-table field values array)
	 * @internal Development Status = ready for testing (rft)
	 * @internal CalledBy: AthleteTeams::saveYiiNormativeModelValuesArray()
	 * @see AthleteTeams::saveYiiNormativeModelValuesArray()
	 * @internal Called by BaseModele::walkRelationsStack()
	 * @uses self::walkRelations()
	 */
	public static function walkRelationsStack($cModelName,$mtfva){
		// @todo Assert that multi-model field-value data array has been passed.
		$validcModels = ['AthleteTeams'];
		if ( !in_array($cModelName, $validcModels)){
			throw new Exception("The model name passed in {$cModelName} is not supported");
		}
		$cmModelsRef  = $cModelName::getComponentModels();
		$cmRelsSparse = BaseModel::fetchModelRelationsMultipleAsSparseList($cmModelsRef, $cmModelsRef, $eSaveOnly=true);

		$ynmTreeRef=[];
		$ynmTreeData=[];
		$ynBatch=[];
		$ynData=[];
		foreach ($cmRelsSparse as $cmModel => $cmRels) {

			foreach ($cmRels['byModelName'] as $relInfo) {
				// Data Template
				$ynmTreeData[$cmModel][$relInfo['relationName']] = [$relInfo['fkey']];
				// Reference Template
				$ynmTreeRef[$cmModel][$relInfo['relatedModel']][$relInfo['relationName']] = [$relInfo['fkey']];
			}
		}
		foreach ($cmRelsSparse as $cmModel => $cmRels) {

			if (isset($mtfva[$cmModel])){
				if (count($mtfva[$cmModel])>0){
					$mtBase[$cmModel]=$mtfva[$cmModel];
				}
				$rels = [];
				foreach ($ynmTreeRef[$cmModel] as $relatedModel => $shortRelInfo) {
					if (isset($mtfva[$relatedModel]) && count($mtfva[$relatedModel])>0){
						$rels[$cmModel][$relatedModel]=$mtfva[$relatedModel];
						$rnArray = array_keys($shortRelInfo);
						$relationName = $rnArray[0];
						$mtBase[$cmModel][$relationName] = $mtfva[$relatedModel];
					}
				}
			}
			$ynData = $ynData + $mtBase;
			$ynBatch[$cmModel] = $mtBase;

		}
		$out=[
			'yiiTargets'		=>$ynBatch,
			'sourceData'         =>$mtfva,
			'sourceModel'        =>$cModelName,
			];

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($ynData, 10, true);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($ynBatch, 10, true);
		return $out;
	}

	/**
	 *
	 * called by walkRelationsStack to handle each individual model within an mfva passed into walkRelationsStack()
	 * @see AthleteTeams::saveYiiNormativeModelValuesArray()
	 * @internal Called by BaseModele::walkRelationsStack()
	 * @internal Development Status = code construction
	 */
	public static function walkRelations($data){
		$test = [];
		// get relations
		foreach ($test as $key => $value) {

		}
	}


	/**
	 * Called by self::StoreGeneric()
	 * @param array $post Native yii data-tree with relation keys OR GSM mtfva
	 * (multi-table field value array) with model names as tree-keys
	 * @param bool $runValidation rules on save (? is there a conflict between runValidation and safeOnlyAttr?)
	 * @param bool $safeOnlyAttr true=forces setAttributes() to allow only fields with validation rules, false=allows all fields
	 * @param bool $isReadOnly true=Prevents database updates but returns meta-data. false=enables all data writes
	 * @return array
	 * @throws CException
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <danabyrd@byrdbrain.com>
	 * @version 1.2
	 * @since app v0.54
	 * @package Athlete Resumes
	 * @internal Development Status = ready for testing (rft)
	 * @todo Add dynamic modelName to relationName xlation to allow native yii data trees and mtfva structures.
	 * @todo Enable scmda[] standardized and consumable meta-data returns for data-logging and notification services
	 * @internal Development Status = ready for testing (rft)
	 */
/*
	public static function StoreWith($post,$runValidation=true,$safeOnlyAttr=false,$isReadOnly=false){
		// Make a working local copy of the $_POST array
		$POST = $post;
		$keys = array_keys($post);
		if (count($keys) == 0){
			return [];
		}
		$baseModelName = $keys[0];
		$scmda = self::getStoreChainMetaData_ArrayTemplate();

		if(isset($POST[$baseModelName])){
			// Relations() is not static method so instantiate a model purely for reference eg to get the relations
			//$relations['relationName']['primaryKeyFieldName'] = 'somePkFieldName';
			$referenceModel = new $baseModelName();
			$relations = $referenceModel->relations(); // type, modelName, pk

		} else {
			throw new CException('The base model is not present', 600);
		}

		$relatedModelRelationTypeIdx   = 0;
		$relatedModelNameIdx           = 1;
		$relatedModelPkNameIdx         = 2;

		// consider the rule that the first table in the master array is the base table.
		$baseModelAttr = ['name'=>$baseModelName, 'pkName'=>$referenceModel->primaryKey()];

		if( isset($POST[$baseModelName][$baseModelAttr['pkName']]) && (int)$POST[$baseModelName][$baseModelAttr['pkName']] > 0){
			// pk value provided so update the model
			$pk    = $POST[$baseModelName][$baseModelAttr['pkName']];
			$model = $baseModelName::model()->findByPk($pk);
		} elseif( isset($POST[$baseModelName][$baseModelAttr['pkName']]) == false ){
			// no pk provided so insert-create the model
			$model = new $baseModelName();
		}

		$relationsCrudScenario=[];
		$modelsCreated = [];
		foreach ($relations as $relationName=>$relationDetails){

			$relatedModelName = $relationDetails[$relatedModelNameIdx];
			if(isset($POST[$baseModelName][$relatedModelName])){
				// The post array is an mtfva schema so dynamically add the relation name
				if(! isset($POST[$baseModelName][$relationName])){
					$POST[$baseModelName][$relationName] = $POST[$baseModelName][$relatedModelName];
				}
			}

			if(isset($POST[$baseModelName][$relationName])){
				// test for related model pk value
				$relatedValues    = $POST[$baseModelName][$relationName];
				$relationType     = $relationDetails[$relatedModelRelationTypeIdx];
				$relatedModelName = $relationDetails[$relatedModelNameIdx];
				$relatedPkName    = $relationDetails[$relatedModelPkNameIdx];
				// $relatedPkValue   = ( ( isset($POST[$baseModelAttr['name']][$relationName][$relatedPkName]) ) ? (int)$POST[$baseModelAttr['name']][$relationName][$relatedPkName] : 0 );
				// if the primary key of the related model is in the array then fetch its value

				if ( isset($POST[$baseModelAttr['name']][$relationName][$relatedPkName]) ){
					$relatedPkValue = (int)$POST[$baseModelAttr['name']][$relationName][$relatedPkName];
				} else {
					$relatedPkValue = 0;
				}

				// if the primary key of the related model is in the array then fetch its value
				if ( isset($POST[$baseModelAttr['name']][$relationName][$relatedPkName]) ){
					$relatedPkValue = (int)$POST[$baseModelAttr['name']][$relationName][$relatedPkName];
				} else {
					$relatedPkValue = 0;
				}

				if($relatedPkValue > 0){
					// pk value provided so update the model
					$relationsCrudScenario[$relationName] = ['scenario'=>'update'];
					$scmda['scenario'][$relationName] = 'update';
				} else {
					// pk value provided so insert the related model
					$relationsCrudScenario[$relationName] = ['scenario'=>'insert'];
					$scmda['scenario'][$relationName] = 'insert';
				}

				if ($relationsCrudScenario[$relationName]['scenario'] == 'insert'){
					// Doesn't this mean that we must save the related model first?
					// Definite YES! The related models must be persisted first to get the fkey
					$relatedModel = new $relatedModelName();
					$relatedModelBeforeAttrUpdate = $referenceModel->getAttributes();
					$relatedModel->setAttributes($relatedValues,$safeOnlyAttr);
					$relatedModel->setAttributesInBaseTableOnly($relatedValues, $safeOnlyAttr);
					$relatedModelAfterAttrUpdate = $relatedModel->getAttributes();
					$relatedModelDiffBeforeSave  = array_diff($relatedModelBeforeAttrUpdate, $relatedModelAfterAttrUpdate);
					//$scmda['success'][get_class($relatedModel)] = "no changes to save on pk $pkVal";
					if ($isReadOnly===false && $relatedModel->save($runValidation)){
						$relatedPkValueAfterSave = $relatedModel->getPrimaryKey();
						$relatedModelAfterSave  = $relatedModel->getAttributes();
						$relatedModelDiffAfterSave = array_diff($relatedModelBeforeAttrUpdate, $relatedModelAfterSave);
						$modelsCreated[] = ['model'=>$relatedModelName, $relatedPkName => $relatedPkValueAfterSave];
						$relationsCrudScenario[$relationName]['errors'] = [];
						$scmda['pkeys'][$relatedModel->primaryKey()] = $relatedPkValueAfterSave;
					} else {
						$relatedPkValueAfterSave = 0;
						$relatedErrors = $relatedModel->getErrors();
						$relationsCrudScenario[$relationName]['errors'] = $relatedErrors;
						$scmda['errors'][get_class($relatedModel)] = $referenceModel->getErrors();
					}
					if ($relatedPkValueAfterSave > 0){
						$scmda['success'][get_class($relatedModel)] = "inserted pk $relatedPkValueAfterSave";
						$scmda['modelsTouched'][get_class($relatedModel)] = $relatedModelDiffAfterSave;
						// write the pk value to the fkey field in the base table
						if (! isset($POST[$baseModelAttr['name']][$relatedPkName]) ){
							$POST[$baseModelAttr['name']][$relatedPkName] = $relatedPkValueAfterSave;
						}
						// ? Set the related table's array in post to an empty array ?
						$POST[$baseModelAttr['name']][$relationName] = [];
						// ? OR write the pkey field to the related table's array ?
					}
				} elseif ($relationsCrudScenario[$relationName]['scenario'] == 'update'){
					$relatedModel = $relatedModelName::model()->findByPk($relatedPkValue);

					// Run a delta check to see if the coach values should be saved or ignored
					$relatedModelBeforeAttrUpdate = $relatedModel->attributes;
					$relatedModel->setAttributes($relatedValues, $safeOnlyAttr);
					$relatedModel->setAttributesInBaseTableOnly($relatedValues, $safeOnlyAttr);
					$relatedModelAfterAttrUpdate = $relatedModel->attributes;
					$relatedModelDiffBeforeSave = array_diff($relatedModelBeforeAttrUpdate, $relatedModelAfterAttrUpdate);

					if (count($relatedModelDiffBeforeSave) == 0){
						$scmda['success'][get_class($relatedModel)] = "no changes to save on pk $relatedPkValue";
					}

					if ($isReadOnly===false && $relatedModel->save($runValidation)){
						$relatedPkValueAfterSave = $relatedModel->getPrimaryKey();
						$relatedModelAfterSave = $relatedModel->attributes;
						$relatedModelDiffAfterSave = array_diff($relatedModelBeforeAttrUpdate, $relatedModelAfterSave);
						$relationsCrudScenario[$relationName]['errors'] = [];
						$scmda['modelsTouched'][get_class($relatedModel)] = $relatedModelDiffAfterSave;
					} else {
						$relatedPkValueAfterSave = 0;
						$relatedErrors = $relatedModel->errors;
						$relationsCrudScenario[$relationName]['errors'] = $relatedErrors;
						$scmda['errors'][get_class($relatedModel)] = $relatedModel->errors;
					}
					if ($relatedPkValueAfterSave > 0){
						// write the pk value to the fkey field in the base table
						if (! isset($POST[$baseModelAttr['name']][$relatedPkName]) ){
							$POST[$baseModelAttr['name']][$relatedPkName] = $relatedPkValueAfterSave;
						}
						// ? Set the related table's array in post to an empty array ?
						// Next line ASSUMES that there isn't any related data below relation in the incomming $post array
						$POST[$baseModelAttr['name']][$relationName] = [];
						// ? OR write the pkey field to the related table's array ?
						$baseModelBeforeAttrUpdate = $model->attributes;
						$model->$relationName = $relatedValues; // Attach
					}
				} else {
					throw new CException('unknown scenario on ' . $baseModelAttr['name'] . ' relation ' . $relationName);
				}

			} else {
				$model->$relationName = [];
			}
		}

		// Save the base model
		$baseModelBeforeAttrUpdate = $model->attributes;
		$updatedModelResult_a = $model->setAttributes($POST[$baseModelAttr['name']], $safeOnlyAttr);
		$updatedModelResult_b = $model->setAttributesInBaseTableOnly($POST[$baseModelAttr['name']], $safeOnlyAttr);
		$baseModelAfterAttrUpdate = $model->attributes;
		$baseModelDiffBeforeSave = array_diff($baseModelBeforeAttrUpdate, $baseModelAfterAttrUpdate);
		if (count($baseModelDiffBeforeSave) == 0){
			$scmda['success'][get_class($model)] = "no changes to save on pk " . $model->getPrimaryKey();
			$saveBaseModel = false;
		} else {
			$saveBaseModel = true;
		}
		if ($saveBaseModel && $model->save($runValidation)){
			$baseModelAfterSave = $model->attributes;
			$baseModelDiffAfterSave = array_diff($baseModelBeforeAttrUpdate, $baseModelAfterSave);
			$scmda['modelsTouched'][get_class($model)] = $baseModelDiffAfterSave;
			$id = $model->getPrimaryKey();
			$scmda['pkeys'][$model->primaryKey()] = $id;
			$errors = [];
		} else {
			$errors = $model->getErrors();
			$scmda['errors'][get_class($model)] = $errors;
			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($errors, 10, true);
			$id = 0;
		}

		return [
			'id'=>$id, 'modelsCreated' => $modelsCreated,
			'baseModelErrors'=>$errors,'relationsCrud'=>$relationsCrudScenario,
			'scmda'=>$scmda,
			];
	}
*/

	/**
	 *
	 * @param type $templateName
	 * @internal Development Status = code construction
	 */
	public static function storeChainTemplatesProto_v0($templateName) {
		$athleteResume = [

				'params'	=> [
					//'jede'	=> Bjede::convert_array_to_jede_str([
						'bound__model_name'		=>'Person', // used to map the value of $_POST['pk']
						'submit_widget_class'	=>'TbEditableField',
						'submit_widget_type'	=>'select2',
						'submit_widget_handler'	=>'storeChain',
						'submit_widget_usecase'	=>'numeric_key', // text_value | numeric_key
						'field_values_array'=>[
							'models'=>[
								'Org*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								'Org*upd'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
								],
								'Person'=>[
									'person_id'=>$data['Person']['person_id'],
									'org_id'=>'{{value}}',
								],
							],
						],
						'list_source'=>[
							'model_name'=>'Org',
							'text'		=>'org_name',
							'value'		=>'org_id',
							'condition'	=>'org_type_id=2',
							'order'		=>'org_name'
						],
						'data write plan'=>[
							'Org',
							'Person',
						],
				]
		];
	}


	public static function storeChainTemplatesProto($templateName) {

		// Org

		$athleteResume = [

				'params'	=> [
					//'jede'	=> Bjede::convert_array_to_jede_str([
						'bound__model_name'		=>'Person', // used to map the value of $_POST['pk']
						'submit_widget_class'	=>'TbEditableField',
						'submit_widget_type'	=>'select2',
						'submit_widget_handler'	=>'storeChain',
						'submit_widget_usecase'	=>'numeric_key', // text_value | numeric_key
						'field_values_array'=>[
							'models'=>[
								'Org*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								'Org*upd'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
								],
								'Person'=>[
									'person_id'=>$data['Person']['person_id'],
									'org_id'=>'{{value}}',
								],
							],
						],
						'list_source'=>[
							'model_name'=>'Org',
							'text'		=>'org_name',
							'value'		=>'org_id',
							'condition'	=>'org_type_id=2',
							'order'		=>'org_name'
						],
						'data write plan'=>[
							'Org',
							'Person',
						],
				]
		];

		$athleteResume2 = [

				'params'	=> [
					//'jede'	=> Bjede::convert_array_to_jede_str([
						'bound__model_name'		=>'Person', // used to map the value of $_POST['pk']
						'submit_widget_class'	=>'TbEditableField',
						'submit_widget_type'	=>'select2',
						'submit_widget_handler'	=>'storeChain',
						'submit_widget_usecase'	=>'numeric_key', // text_value | numeric_key
						'field_values_array'=>[
							'models'=>[
								'OrgType*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								// OrgType Updates disallowed by business rule
//								'OrgType*upd'=>[
//									'org_id'		=> '{{value}}',
//									'org_name'		=> '{{insert_text}}',
//								],
								'OrgLevel*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								// OrgLevel Updates disallowed by business rule
//								'OrgLevel*upd'=>[
//									'org_id'		=> '{{value}}',
//									'org_name'		=> '{{insert_text}}',
//								],
								'Org*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								'Org*upd'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
								],
								'Person'=>[
									'person_id'=>$data['Person']['person_id'],
									'org_id'=>'{{value}}',
								],
								'Player*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								'Player*upd'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
								],
								'TeamDivision*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								// TeamDivision Updates disallowed by business rule
//								'TeamDivision*upd'=>[
//									'org_id'		=> '{{value}}',
//									'org_name'		=> '{{insert_text}}',
//								],
								'TeamLeague*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								// TeamLeague Updates disallowed by business rule
//								'TeamLeague*upd'=>[
//									'org_id'		=> '{{value}}',
//									'org_name'		=> '{{insert_text}}',
//								],
								'Team*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								'Team*upd'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
								],
								'SportPosition*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								// SportPosition Updates disallowed by business rule
//								'SportPosition*upd'=>[
//									'org_id'		=> '{{value}}',
//									'org_name'		=> '{{insert_text}}',
//								],
								'TeamPlayer*ins'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
									'org_type_id'	=> 2,
								],
								'TeamPlayer*upd'=>[
									'org_id'		=> '{{value}}',
									'org_name'		=> '{{insert_text}}',
								],

							],
						],
						'list_source'=>[
							'model_name'=>'Org',
							'text'		=>'org_name',
							'value'		=>'org_id',
							'condition'	=>'org_type_id=2',
							'order'		=>'org_name'
						],
						'data write plan'=>[
							'Org',
							'Person',
						],
				]
		];
	}

	/**
	 * Enable deep data extraction via Ajax
	 *   Calling renderJsonDeep on an object, or array of objects, will encode the
	 *   object(s) in JSON including any of the relations you have pulled, like by
	 *   adding them to the 'with' param in the DbCriteria.
	 * @param CActiveRecord | array $o
	 * @return string $data Returns JSON via echo
	 * @uses getAttributesDeep() Walks the relations of a model and returns all data
	 */
	/*
	public static function renderJsonDeep($o, $preventAppEnd=true) {
		if ($preventAppEnd === false){
			header('Content-type: application/json');
		} elseif ($preventAppEnd === true){

		}
		// if it's an array, call getAttributesDeep for each record
		if (is_array($o)) {
			$data = array();
			foreach ($o as $record) {
				array_push($data, self::getAttributesDeep($record));
			}
			echo CJSON::encode($data);
		} else {
			// otherwise just do it on the passed-in object
			echo CJSON::encode( self::getAttributesDeep($o) );
		}

		if ($preventAppEnd === false){
			// prevents any other Yii code from being output
			foreach (Yii::app()->log->routes as $route) {
				if($route instanceof CWebLogRoute) {
					$route->enabled = false; // disable any weblogroutes
				}
			}
			Yii::app()->end();
		} elseif ($preventAppEnd === true){

		}
	}
	*/

	/**
	 * Enable deep data extraction via Ajax
	 *   Calling renderJsonDeep on an object, or array of objects, will encode the
	 *   object(s) in JSON including any of the relations you have pulled, like by
	 *   adding them to the 'with' param in the DbCriteria.
	 * @param CActiveRecord | array $o
	 * @return string $data Returns JSON via echo
	 * @uses getAttributesDeep() Walks the relations of a model and returns all data
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function renderJsonDeepToCaller($o, $preventAppEnd=true) {
		$out=[];
		//header('Content-type: application/json');
		// if it's an array, call getAttributesDeep for each record
		if (is_array($o)) {
			$data = array();
			foreach ($o as $record) {
				array_push($data, self::getAttributesDeep($record));
			}
			//echo CJSON::encode($data);
			$out[] = CJSON::encode($data);
		} else {
			// otherwise just do it on the passed-in object
			echo CJSON::encode( self::getAttributesDeep($o) );
			$out[] = CJSON::encode( self::getAttributesDeep($o) );
		}

		if ($preventAppEnd===false){
			// prevents any other Yii code from being output
			foreach (Yii::app()->log->routes as $route) {
				if($route instanceof CWebLogRoute) {
					$route->enabled = false; // disable any weblogroutes
				}
			}
			Yii::app()->end();
		} elseif ($preventAppEnd===true){
			return $out;
		}
	}

	/**
	 * Walks the relations of a model and returns all attributes and relations
	 * @param CActiveRecord $model
	 * @return array $data An associative array of attributes and related attributes.
	 * @internal All yii models in the GSM app can call this method via $model->attributesDeep()
	 * @version 0.8.0
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 * @since 0.53.0
	 *
	 */
	public static function getAttributesDeepAsArray($model) {
		// get the attributes and relations
		if (is_array($model)){
			return [];
		}
		elseif ( ! is_object($model))
		{
			// The next will generally output null
			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model, 10, true);
			return [];
		}
		$data = $model->attributes;
		$relations = $model->relations();
		$hasRelatedButNoGet=[];
		foreach ($relations as $relationName=>$relInfo) {
			// for each relation, if it has the data and it isn't null
			if (is_null($relationName)){
				$exceptionText = get_class($model) . ' has a null relation name';
				throw new CException($exceptionText);
			}
			//if ($model->hasRelated($relationName) && $model->getRelated($relationName) != null) {
			if ($model->hasRelated($relationName)){
				if ( ! is_null($model->getRelated($relationName))){
					$related = $model->getRelated($relationName);
				}
				else
				{
					// This relation name must be null...
					trace(get_class($model) .' '. $model->primaryKey() .' = '
						. $model->getPrimaryKey() . ' had getRelated('.$relationName .') return a null',
						__METHOD__
					);
					// loop to the next relation name
					continue;
				}

				if ( ! is_null($related)){

					$relatedData = self::getAttributesDeepAsArray($model->getRelated($relationName));
					if (is_array($relatedData)){
						$data[$relationName] = $relatedData;
					} else {
						$hasRelatedButNoGet[$relationName] = 'value returned was not an array';
					}

					//$data[$relationName] = self::getAttributesDeepAsArray($model->getRelated($relationName));
				} else {
					$hasRelatedButNoGet[$relationName] = 'getRelated() returned a null';
				}
					// add this to the attributes structure, recursively calling
					// this function to get any of the child's relations
				$relatedData = self::getAttributesDeepAsArray($model->getRelated($relationName));
				$data[$relationName] = self::getAttributesDeepAsArray($model->getRelated($relationName));
			}
		}
		unset($relInfo);
//		if (true==false){
//			$text = Barray::dumpAsString($hasRelatedButNoGet);
//			echo $text . '<br />';
//		}
		return $data;
	}

	/**
	 * Walks the relations of a model and returns all data
	 * @param CActiveRecord $model
	 * @return type
	 */
	public static function getRelationsAsCActiveRecord($model) {
		// get the attributes and relations
		$data = $model->attributes;
		$modelName = get_class($model);
		//$relations = $model->relations();
		$relAssoc  = $model->relationsAssoc($modelName);
		foreach ($relAssoc['byRelationName'] as $relationName=>$relInfo) {
			// $relationInfo is an indexed array of relation properties
			// for each relation, if it has the data and it isn't null
			if ($model->hasRelated($relationName)){
				$related = $model->getRelated($relationName);
				if ($relInfo['relationType']==='x' || $relInfo['relationType']==='y'){
					if (is_object($related)){
						$model->$relationName = $related;
					} elseif (is_null($related)){
						//$model->$relationName = [];
					} elseif (is_array($related)){
						$model->$relationName = $related;
					} elseif (is_bool($related)){
					}
				}

				//$model->$relationName = getRelated($r) != null) {
			}
					// add this to the attributes structure, recursively calling
					// this function to get any of the child's relations
				//$model[$relationName] = self::getAttributesDeepAsCActiveRecord(&$model->getRelated($relationName));
		}


        $bt   = debug_backtrace();
        $fileName = basename($bt[0]['file']);
		unset($fileName);

		return $model;
	}

	public static function getAttributesDeepAsCActiveRecord_v1($model) {
		// get the attributes and relations
		$data = $model->attributes;
		$relations = $model->relations();
		//$relAssoc  = $model->relations();
		foreach (array_keys($relations) as $r) {
			// for each relation, if it has the data and it isn't null
			if ($model->hasRelated($r) && $model->getRelated($r) != null) {
					// add this to the attributes structure, recursively calling
					// this function to get any of the child's relations
				$data[$r] = self::getAttributesDeep($model->getRelated($r));
			}
		}
		return $data;
	}

	/**
	 * Walks the relations of a model and returns all data
	 * @param CActiveRecord $model
	 * @return array
	 */
	public static function getAttributesDeep($model) {
		// get the attributes and relations
		$data = $model->attributes;
		$relations = $model->relations();
		foreach (array_keys($relations) as $r) {
			// for each relation, if it has the data and it isn't null
			if ($model->hasRelated($r) && $model->getRelated($r) != null) {
					// add this to the attributes structure, recursively calling
					// this function to get any of the child's relations
				$data[$r] = self::getAttributesDeep($model->getRelated($r));
			}
		}
		return $data;
	}
}

