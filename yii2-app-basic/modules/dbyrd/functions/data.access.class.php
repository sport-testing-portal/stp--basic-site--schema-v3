<?php
//defined('BCSYSPATH') or die('No direct script access.');
// * Project.........: EPF (Enterprise Process Framework)
// * Purpose.........: Provide connectivity to mysql databases (via PDO, mysqli has been removed)
// * CEC.............: asp-2.1.1
// * Initial Release.: v0.1.0
// * Author..........: Dana Byrd
// * History.........:
//    2013-09-04 0.0.3.4 (fea) dbyrd: Added firewall class to allow ip address checks.
//    2013-09-04 0.0.3.4 (bug) dbyrd: Enable null constructors().
//    2013-07-20 0.0.2.3 (fea) dbyrd: Add CEC 0.0.1 Prevent direct script acesss via an undefined constant trap.
//    2013-08-12 0.0.1.2 (fea) dbyrd: Allow the data config object to read $_SERVER.
//      To enable default connections per server without specifing a connection in
//      source code.
//    2013-08-08 0.0.0.1 (fea) dbyrd: Isolate the PDO data connections in one class.
// * Notes..........:
//    Use a static method to load the config into memory for an instant during instantiation.
//    CEC:
//      0.0.1 Prevent direct script access via an undefined constant trap.
//    Usage examples:
//      require_once("tools/data.access.class.php");
//      $this->db = new sdb();
//    metadata[release][["initial deployment release"]="0.1.0", ["current release"]="0.1.0"]

// * Author from....: 0.0.0.0 -> x.x.x.x
//                  : Dana Byrd

//require_once(BCSYSPATH . "tools/firewall.php");


require_once('includes/sdbConn.php');

class sdb {
    public $error_is_fatal_yn = "N";
    public $dbh; // handle of the db connection
    public $last_insert_id;
    public $rows_affected;
    static $debug_yn = "N";

// <editor-fold defaultstate="collapsed" desc="usage comments">
//    $pdo = new sPDO("sandbox-pdo","Y");
//    $qry = "select count(*) as cnt from footages;\n";
//    $pdo->exec($qry);
//    $qry = "select * from org limit 1;\n";
//    $data = $pdo->get_data_array($qry);
//    $qry = "select * from people limit 1;\n";
//    $data2 = $pdo->get_data_obj($qry);
//    echo_array($data);
//    var_dump($data2);
// </editor-fold>

    //private static $instance;
    //private function __construct($db_config_name, $persist_yn="Y"){
    public function __construct($db_config_name=null, $persist_yn="Y"){
        if (self::$debug_yn == "Y"){
            echo_array(debug_backtrace());
            echo "\$db_config_name = $db_config_name <br>";
            //echo "\$db_config_name = $db_config_name <br>";
        }
        $this->dbh = new sdb_conn($db_config_name, $persist_yn);
    }

    public function exec($query){
        try {
            $stmt = $this->dbh->prepare($query);
            if ($stmt->execute()) {
                $row_cnt              = $stmt->rowCount();
                $last_insert_id       = $this->dbh->lastInsertId();
                $this->rows_affected  = $row_cnt;
                $this->last_insert_id = (int)$last_insert_id;
                return $row_cnt;
            } else {
                return -1;
            }
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
            } elseif($_SERVER["SERVER_NAME"] == "127.0.0.1"){
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
                var_dump($exc);
            }
            return -1;
        }
    }

    public function query($query){
        $result = $this->exec($query);
        return $result;
    }


    public function exa($query, $key_field=null){
        try {
            $arr = $this->get_data_array($query, $key_field);
            return $arr;
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }


    public function exa_v0($query){
        try {
            $arr = $this->get_data_array($query);
            return $arr;
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

    public function exo($query){
        try {
            $arr_of_obj = $this->get_data_obj($query);
            return $arr_of_obj;
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

    public function exba($query, $bind_array){
        try {
            $arr = $this->get__data_array_via_data_bind($query, $bind_array);
            return $arr;
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

    public function exbo($query, $bind_array){
        try {
            $obj = $this->get__data_obj_via_data_bind($query, $bind_array);
            return $obj;
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

	/**
	 *
	 * @param string $query Sql select
	 * @return array[] mixed
	 * @deprecated since version 0.1.1
	 */
    public function get_data_array($query, $key_field=NULL){
        try {
            $stmt   = $this->dbh->prepare($query);
            $result = array();
            if ($stmt->execute()) {
                $row_cnt = $stmt->rowCount();
                $this->rows_affected  = $row_cnt;
                if (! empty($key_field) && is_string($key_field)){
                    // Use the primary key value as the row idx
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $result[$row[$key_field]] = $row;
                    }
                } else {
                    // Use a standard zero based array row index
                    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $result[] = $row;
                    }
                }
            }
            return $result;
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

	/**
	 *
	 * @param type $query
	 * @return array[] mixed
	 * @deprecated since version 0.1.1
	 */
    public function get_data_array_v0($query){
        try {
            $stmt   = $this->dbh->prepare($query);
            $result = array();
            if ($stmt->execute()) {
                $row_cnt = $stmt->rowCount();
                $this->rows_affected  = $row_cnt;

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $result[] = $row;
                }
            }
            return $result;
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

    public function get_data_obj($query, $key_field=NULL){
        try {
            $stmt   = $this->dbh->prepare($query);
            $result = array();
            if ($stmt->execute()) {
                $row_cnt = $stmt->rowCount();
                $this->rows_affected  = $row_cnt;
                if (! empty($key_field)){
                    $ref = "\$row->"."$key_field";
                    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                        $result[$ref()] = $row;
                    }
                } else {
                    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                        $result[] = $row;
                    }
                }
            }
            return $result;
        } catch (PDOException $exc) {
            // @todo: send pdo exceptions to the admin
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

	/**
	 *
	 * @param type $query
	 * @return array[] Objects
	 * @deprecated since version 0.1.1
	 */
    public function get_data_obj_v0($query){
        try {
            $stmt   = $this->dbh->prepare($query);
            $result = array();
            if ($stmt->execute()) {
                $row_cnt = $stmt->rowCount();
                $this->rows_affected  = $row_cnt;
                while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                    $result[] = $row;
                }
            }
            return $result;
        } catch (PDOException $exc) {
            // @todo: send pdo exceptions to the admin
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

    public function get__data_array_via_data_bind($query, $bind_array){
        try {
            if (empty($query)){
                return -1;
            }
            if (empty($bind_array)){
                $empty_yn = "Y";
            }

            $stmt = $this->dbh->prepare($query);
            if (is_array($bind_array) && sizeof($bind_array) > 0){

                //$stmt = $this->prepare("SELECT * FROM footages WHERE footage_id=:footage_id AND clearance=:clearance");

                if(stripos($query, "=:") !== false){
                    // throw new Exception; ?
                    $query_is_malformed_yn = "N";
                } else {
                    $query_is_malformed_yn = "Y";
                }

                if($query_is_malformed_yn == "N"){
                    foreach($bind_array as $field_name=>$field_value){
                        // prepare the statement. the place holders allow PDO to handle substituting
                        // the values, which also prevents SQL injection

                        // bind the parameters
                        //$stmt->bindValue(":" . $field_name, $field_value);  // this works
                        $bind_type = get__PDOConstantType($field_value);
                        $stmt->bindValue(":" . $field_name, $field_value , $bind_type);
                    }
                } else {
                    // throw new Exception; ?
                }
            }

            $result = array();
            if ($stmt->execute()) {
                $row_cnt = $stmt->rowCount();
                $this->rows_affected  = $row_cnt;
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $result[] = $row;
                }
            }
            return $result;
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

    public function get__data_obj_via_data_bind($query, $bind_array){
        try {
            if (empty($bind_array)){
                $empty_yn = "Y";
            }
            $stmt = $this->dbh->prepare($query);
            if (is_array($bind_array) && sizeof($bind_array) > 0){

                //$stmt = $this->prepare("SELECT * FROM footages WHERE footage_id=:footage_id AND clearance=:clearance");

                if(stripos($query, "=:") !== false){
                    // throw new Exception; ?
                    $query_is_malformed_yn = "N";
                } else {
                    $query_is_malformed_yn = "Y";
                }

                if($query_is_malformed_yn == "N"){
                    foreach($bind_array as $field_name=>$field_value){
                        // prepare the statement. the place holders allow PDO to handle substituting
                        // the values, which also prevents SQL injection

                        // bind the parameters
                        // $stmt->bindValue(":" . $field_name, $field_value);  // this works
                        $bind_type = get__PDOConstantType($field_value);
                        $stmt->bindValue(":" . $field_name, $field_value, $bind_type);
                    }
                } else {
                    // throw new Exception; ?
                }
            }

            $result = array();
            if ($stmt->execute()) {
                $row_cnt = $stmt->rowCount();
                $this->rows_affected  = $row_cnt;
                while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
                    $result[] = $row;
                }
            }
            return $result;
        } catch (PDOException $exc) {
            if ($_SERVER["SERVER_NAME"] == "sandbox"){
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            } elseif (firewall::get_allowed_client_location_name() == "aus.dev.globalsoccermetrix.com") {
                var_dump($exc);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            }
            return -1;
        }
    }

    function get__db_config_array_v1($db_config_name=null){
        if (empty($db_config_name)){
            $db_config_name = "www-local-pdo";
        }

        // <editor-fold defaultstate="collapsed" desc="db config arrays">
        $db_config['www-local-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                            /**
                             * The following options are available for PDO:
                             *
                             * string   dsn
                             * string   username
                             * string   password
                             * boolean  persistent
                             * string   identifier
                             */
                            'dsn'        => 'mysql:host=localhost;dbname=gsm_dev',
                            'username'   => 'gsm_web_admin',
                            'password'   => 'fJ5+yqKJa~NP6#G#Uf',
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );
		// next connection uses a local ssh tunnel
        $db_config['www-remote-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                            /**
                             * The following options are available for PDO:
                             *
                             * string   dsn
                             * string   username
                             * string   password
                             * boolean  persistent
                             * string   identifier
                             */
                            'dsn'        => 'mysql:host=localhost;port=4306;dbname=gsm_dev',
                            'username'   => 'gsm_web_admin',
                            'password'   => 'fJ5+yqKJa~NP6#G#Uf',
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );

        $db_config['www-dev-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                            /**
                             * The following options are available for PDO:
                             *
                             * string   dsn
                             * string   username
                             * string   password
                             * boolean  persistent
                             * string   identifier
                             */
                            'dsn'        => 'mysql:host=localhost;dbname=gsm_dev',
                            'username'   => 'gsm_web_admin',
                            'password'   => 'fJ5+yqKJa~NP6#G#Uf',
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );

        // "comments"  => "ubuntu 14.04"
        $db_config['sandbox-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                            /**
                             * The following options are available for PDO:
                             *
                             * string   dsn
                             * string   username
                             * string   password
                             * boolean  persistent
                             * string   identifier
                             */
                            'dsn'        => 'mysql:host=localhost;dbname=gsm_dev',
                            'username'   => 'gsm_web_admin',
                            'password'   => 'fJ5+yqKJa~NP6#G#Uf',
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );
        $db_config['sandbox-unused-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                            /**
                             * The following options are available for PDO:
                             *
                             * string   dsn
                             * string   username
                             * string   password
                             * boolean  persistent
                             * string   identifier
                             */
                            'dsn'        => 'mysql:host=localhost;dbname=information_schema',
                            'username'   => 'web_user',
                            'password'   => 'FJ#wwEc4PsCuMbtv8ySzKG@Da',
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );
        // </editor-fold>

        if (array_key_exists($db_config_name, $db_config)){
            return $db_config[$db_config_name];
        }

        $login_credentials[] = array(
            "user"       => "web_user"
            ,"pass"      => "4MtiZ_*h1_8rv9Hv=5#Tjy8)(M!^%A!"
            ,"host"      => "localhost"
            ,"dbname"    => "gsm"
            ,"comments"  => "ubuntu 14.04"
        );

        // Clean up the configuration vars
        unset($login_credentials);
    }

}

class sdb_conn extends PDO {
    // Originally called SafePDO. Renamed
    // Connect to the database with defined constants
    // $dbh = new SafePDO(PDO_DSN, PDO_USER, PDO_PASSWORD);
    // $dbh = new SafePDO("www-remote-pdo");
    // $dbh = new SafePDO("www-local-pdo");
    private $verbose_yn = "N";
    private $debug_yn   = "N";


    public static function exception_handler($exception) {
        // Output the exception details
        // PDOException::exception

        if ($exception){
            $server_name   = $_SERVER["SERVER_NAME"];
            $location_name = firewall::get_allowed_client_location_name();
            if ($server_name == "localhost"){
                var_dump($exception);
                exit();
            }
            if ($_SERVER["SERVER_NAME"] == "gsm_public"){
                var_dump($exception);
                //die('Uncaught exception: ' . $exception->getMessage());
            // } elseif (firewall::get_allowed_client_location_name() == "aus.dev.stalkr.com") {
            } elseif ($location_name == "aus.dev.gsm.com") {
                var_dump($exception);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            //} elseif (firewall::get_allowed_client_location_name() == "www.stalkr.com") {
            } elseif ($location_name == "local.development") {
                var_dump($exception);
            }
        }
        //die('Uncaught exception: ' . $exception->getMessage());
    }

    public function __construct($db_config_name, $persist_yn=null) {
    //public function __construct($dsn, $username='', $password='', $driver_options=array()) {

        //$config = get__db_config_array($db_config_name);
        $config = sdb_conf::get__db_config_array($db_config_name);
        if ($this->verbose_yn == "Y"){
            echo_array($config);
        }
        // Temporarily change the PHP exception handler while we . . .
        set_exception_handler(array(__CLASS__, 'exception_handler'));

        // . . . create a PDO object
        $dsn            = $config['connection']['dsn'];
        $username       = $config['connection']['username'];
        $password       = $config['connection']['password'];
        $driver_options = $config;
        parent::__construct($dsn, $username, $password, $driver_options);

        // Change the exception handler back to whatever it was before
        restore_exception_handler();

        // the following tells PDO we want it to throw Exceptions for every error.
        // this is far more useful than the default mode of throwing php errors
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if (empty($persist_yn)){
            $persist_yn = "N";
        }
        if ($persist_yn == "Y"){
            $this->setAttribute(PDO::ATTR_PERSISTENT, PDO::ATTR_PERSISTENT);
        }
    }
}
?>


