<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dana Byrd <dana@globalsoccermetreix.com>
 *   Sample call: Bjede::convert_array_to_jede_str()
 */

class bjede extends CApplicationComponent
{

    // <editor-fold defaultstate="collapsed" desc="Class Properties">

    protected static $readonly_yn     = "";
    protected static $verbose_yn      = "";
    protected static $debug_yn        = "";
    protected static $interactive_yn  = "";

    protected static $error_fatal_yn  = "N";

    // Properties for managing encryption
    protected static $regex     = '{"data":"([a-f0-9]*)"}';
//    var $password  = 's7TnzP8ng4qIhmyL2bNkba5WJkCWR3yuwG1f7YKRaAJeUpDvRCIoTk8AxtS73R8cVr0qgVGm5R6Bg7jnd0zAmKPZCq7TDF1lcRvbDhAC9GLseGP6aUr8GwBgDHh8X5a0';
    protected static $password  = '1XJCvlq8zIPAp8kqcbDAmGS0YVRwnr3nhyuBACaLfiLzo7WNsZUujQ12G25WoeS2xzU3KWGS86zbJZTd3KgrRmmmzx9qznlLIRIsffTjZaJSu3Dbaerhc8Ekga7wPi8a';
//    var $salt      = 'IhmyL2bNkba5WJkCWR3yuwG1f7YKRaAJeUpDvRCIoTk8AxtS73R8cVr0qgVGm5R6Bg7jnd0zA';
    protected static $salt      = 'lBLb5Ys8X3qR5T0g4UkGfcbjKoK1RPv2VV3MUYMh98G7UZQOKnt7RsxIZp8Pfu38F8So4PfLO';

    // </editor-fold>  // clase properties

	public function init() {
	}

// <editor-fold defaultstate="collapsed" desc="array class">
// class jede{

    public function __construct($verbose_yn=NULL, $debug_yn=NULL) {

        if (! empty($verbose_yn)){
            $this->verbose_yn = strtoupper($verbose_yn);
        } else {
            $this->verbose_yn = "N";
        }
        if (! empty($debug_yn)){
            $this->debug_yn = strtoupper($debug_yn);
        } else {
            $this->debug_yn = "N";
        }
    }

	/**
	 *
	 * @param type $jede_string '{data:"..."}'
	 * @return type
	 */
    public static function convert_jede_string_to_array($jede_string){

        // use regex to grab encrypted data string
        try {
            //$regex_mods = "i";
            $regex = (string)self::$regex;

            if (preg_match("/$regex/", $jede_string, $match) ) {
                $crypt_data  = $match[1];
            } else {
                return -1;
            }

        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

        try {
            $pass = (string) self::$password;
            $salt = (string) self::$salt;

            $cryptastic = new cryptastic;

            $key = $cryptastic->pbkdf2($pass, $salt, 1000, 32) or
                    die("Failed to generate secret key.");

            $msg =  hex2bin($crypt_data);

            $decrypted = $cryptastic->decrypt($msg, $key) or
                    die("Failed to complete decryption");

            if (self::$verbose_yn == "Y"){
                echo "Decrypted: <br>" . $decrypted . "<br /><br />\n";
            }

            $json_string = $decrypted;  // returns a json string?
            $data_array  = json_decode($json_string, $assoc=true);

            return $data_array;  // returns a standard array

        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

    }

	/**
	 * Encrypt standard JSON string
	 * @param type $json_string '{data:...}'
	 * @return JEDE - json encrypted data envelope
	 */
    public static function convert_json_str_to_jede_str($json_string){
        $verbose_yn = self::$verbose_yn;
        try {
            $pass = (string) self::$password;
            $salt = (string) self::$salt;

            $cryptastic = new cryptastic;

            $key = $cryptastic->pbkdf2($pass, $salt, 1000, 32) or
                    die("Failed to generate secret key.");

            $encrypted = $cryptastic->encrypt($json_string, $key) or
                    die("Failed to complete encryption.");

            $json_string_wcrypto =  json_encode(array("data"=>bin2hex($encrypted)));

            if ($verbose_yn == "Y"){
                //echo "encrypted json is " . strlen(bin2hex($encrypted)) . " bytes long. <br>";
                echo "encrypted json is " . strlen($json_string_wcrypto) . " bytes long. <br>";
                //echo "Encrypted: <br>" . bin2hex($encrypted) . "<br /><br />\n";
                echo wordwrap( $json_string_wcrypto ,75, "\n", true) . "<br /><br />";
                echo "json version of data follows <br>   "
                . json_encode(array("data"=>bin2hex($encrypted)));
            }

            return $json_string_wcrypto;  // returns an encrypted string

        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public static function convert_array_to_jede_str__send_via_post($target_url, $array){
        //$target_url = The URL we want to send data to

        //$content = json_encode($data_array); // will format all as a string
        $json_string      = utf8_encode(json_encode($array, JSON_NUMERIC_CHECK | JSON_FORCE_OBJECT)); // includes data type
        $jede_string      = $this->convert_json_str_to_jede_str($json_string);
        //$json_string_len  = strlen($json_string);
        $jede_string_len  = strlen($jede_string);

        $curl         = curl_init($target_url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
                array("Content-type: application/json", "Accept: application/json", "Content-Length: $jede_string_len"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jede_string);

        $json_response = curl_exec($curl);
        if (self::$debug_yn == "Y"){
            //echo $this->format_jede_str($jede_string, $width=120);
            //echo "json respons is: <br /> $json_response";
        }

        //echo_array($json_response);
        $status        = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status == 200 ) {
            echo "return status = 200 <br>";
        }
        if ( $status == 201 ) {
            $err_msg = "Error: call to URL $target_url failed with status $status, response $json_response, curl_error "
                     . curl_error($curl) . ", curl_errno "
                     . "<b>" . curl_errno($curl) . "</b>";
            //die($err_msg);
        }

        curl_close($curl);
        //$response = json_decode($json_response, true);    // if the server returns a non json string this will blank the return value
        return $json_response;  // returns an string
        //return $response;  // returns an array
    }

    public static function convert_array_to_jede_str($array){

        try {

            $json_string      = utf8_encode(json_encode($array, JSON_NUMERIC_CHECK | JSON_FORCE_OBJECT)); // includes data type
            $jede_string      = self::convert_json_str_to_jede_str($json_string);
            return $jede_string;

  /*
            $pass = (string)$this->password;
            $salt = (string)$this->salt;

            $cryptastic = new cryptastic;

            $key = $cryptastic->pbkdf2($pass, $salt, 1000, 32) or
                    die("Failed to generate secret key.");

            $json_string = json_encode_wtype($array);

            $encrypted = $cryptastic->encrypt($json_string, $key) or
                    die("Failed to complete encryption.");

            $json_crypt =  json_encode(array("data"=>bin2hex($encrypted)));

            if ($this->verbose_yn == "Y"){
                //echo "encrypted json is " . strlen(bin2hex($encrypted)) . " bytes long. <br>";
                echo "encrypted json is " . strlen($json_crypt) . " bytes long. <br>";
                //echo "Encrypted: <br>" . bin2hex($encrypted) . "<br /><br />\n";
                echo wordwrap( $json_crypt ,75, "\n", true) . "<br /><br />";
                echo "json version of data follows <br>   "
                . json_encode(array("data"=>bin2hex($encrypted)));
            }

            return $json_crypt;  // returns an encrypted string
        */
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public static function format_jede_str($jede_string, $width=null){
        if (empty($width)){
            $width = 75;
        }
        $string = wordwrap( $jede_string ,$width, "\n", true) . "<br /><br />";
        return $string;
    }

}

// </editor-fold>




