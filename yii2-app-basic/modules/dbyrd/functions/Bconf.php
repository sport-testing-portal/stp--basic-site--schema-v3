<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: fn::CDbExpression('NOW()');
 */

class bconf extends CApplicationComponent
{
	public function init() {
	}

	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
	public static function CDbExpression($exp, $db='db') {
		if($exp == 'NOW()') {
			if (substr(Yii::app()->{$db}->connectionString, 0, 7) === 'sqlsrv:') {
				return new CDbExpression('GETDATE()');
			}
		}

		//otherwise
		return new CDbExpression($exp);
	}


	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
	public static function importCsv($exp, $db='db') {
		// @todo: insert code from bc/tools/import.csv.file.php


		if($exp == 'NOW()') {
			if (substr(Yii::app()->{$db}->connectionString, 0, 7) === 'sqlsrv:') {
				return new CDbExpression('GETDATE()');
			}
		}

		//otherwise
		return new CDbExpression($exp);
	}



	// <editor-fold defaultstate="collapsed" desc="configuration class">
	/**
	 * Get paths for various byrd components
	 * @param type $pathType "base","tools","models"
	 * @internal Revised code from v0.0.1.1 2014-01-22 Handle pathing of data models in a non-root folder.
	 * @package BC ModelController Objects in a non-root folder
	 * @version 0.1.0
	 */
	public static function getPath($pathType='base'){
		$validParams=['base','tools','models'];
		$paramKey = array_search($pathType, $validParams, true);
		if ($paramKey === false){
			throw new Exception("invalid parameter passed", $code='', $previous='');
		}

		if (array_key_exists('byrdComponents', Yii::app()->params)){
			$conf = Yii::app()->params['byrdComponents'];
		} else {
			throw new Exception("byrdComponents array is missing in yii app params", __FUNCTION__, $previous='');
		}

		if (array_key_exists('basePath', $conf)){
			$basePath = $conf['basePath'];
		} else {
			throw new Exception("byrdComponents basePath is missing in yii app params", __FUNCTION__, $previous='');
		}

		if ($pathType == 'tools' && array_key_exists('toolsPath', $conf)){
			$folder = $conf['toolsPath'];
			$returnPath = $basePath . $folder;
		} else {
			throw new Exception("byrdComponents toolsPath is missing in yii app params", __FUNCTION__, $previous='');
		}

		if ($pathType == 'models' && array_key_exists('modelsPath', $conf)){
			$folder = $conf['modelsPath'];
			$returnPath = $basePath . $folder;
		} else {
			throw new Exception("byrdComponents modelsPath is missing in yii app params", __FUNCTION__, $previous='');
		}

        if (file_exists($returnPath) ==! false){
            return $returnPath;
        } else {
            throw new Exception("byrdComponents $pathType path is missing in the application folders", __FUNCTION__, $previous='');
        }
	}

	/**
	 *
	 * @example protected/extensions/bc/mc/
	 * @package BC ModelController Objects in a non-root folder
	 * @version 0.1.0
	 */
    public static function getTableModel_path(){
		//$modelSourcePath = Bfile::addSlash(Yii::getPathOfAlias('application.extensions.bc.mc'));
		$modelPath = self::getPath('models');
        return $modelPath;
    }

	/**
	 * Convert a database table name to ModelController file name
	 * @param string $tableName Example user_login_attempt becomes user.login.attempt__mc.php
	 * @return string $modelFileName
	 * @package BC ModelController Objects in a non-root folder
	 * @version 0.1.0
	 * @since 0.51.0
	 */
    public static function getTableModel_fileName($tableName){
		//$suffix = "__model.php"; // v0
		$suffix = "__mc.php";
        $modelFileName = str_ireplace("_", ".", $tableName) . $suffix;
        return $modelFileName;
    }

	/**
	 * Convert a database table name to qualified URI for ModelController file
	 * @param string $tableName Example app_parm becomes ....protected/extensions/bc/mc/app.parm__mc.php
	 * @return string $modelFileName
	 * @package BC ModelController Objects in a non-root folder
	 * @version 0.1.0
	 * @since 0.51.0
	 */
    public static function getTableModel_fileUri($tableName){
        $modelFileName   = self::getTableModel_fileName($tableName);
        $modelPath       = self::getTableModel_path();
        $modelUri        = $modelPath . $modelFileName;
        if (file_exists($modelUri) ==! false){
            return $modelUri;
        } else {
            throw new Exception("The ModelController $modelFileName is missing in the model folder ($modelPath)", __FUNCTION__, $previous='');
        }
    }

	/**
	 * Convert a database table name to qualified URI for ModelController file
	 * @param string $tableName Example app_parm becomes ....protected/extensions/bc/mc/app.parm__mc.php
	 * @return string $modelFileName
	 * @package BC ModelController Objects in a non-root folder
	 * @version 0.1.0
	 * @since 0.51.0
	 */
    public static function getTableModel_className($tableName){
        $class_prefix = "sqlTableClass__";
        return $class_prefix . $tableName ;
    }

    public static function get__host_ip_address(){
        // formerly: getIpAddress()
        $url = "http://checkip.dyndns.org/";
        $ip_address = file_get_contents($url);
        $ip_address = str_ireplace("Current IP Address: ", "", $ip_address);
        return $ip_address;
    }

    public static function get__ip_address_location($ip_address){
        // dev sts=dead @todo: an alternative function is needed.
        // This function will load ad ware on on the target site.
        echo "get__ip_address_location($ip_address) in components.functions.*.php have been disabled. Please remove it from your code. Thanks, Dana <br />";
        return;
        $url = "http://www.geoiptool.com/en/?IP=$ip_address";
        $location_details_html = file_get_contents($url);
        return $location_details_html;
    }

    public static function get__ip_address_location_alt($ip_address){
        $url = "http://www.ip-adress.com/ip_tracer/$ip_address";
        $location_details_html = file_get_contents($url);
        return $location_details_html;
    }

    public static function server_name(){
        return $_SERVER['SERVER_NAME'];
    }

    public static function getAppSetting__local_host(){
        //@todo if machine_name = "dbyrd"
        //@todo if machine_name = "public web site"

		$server_name = $_SERVER['SERVER_NAME'];
		$msg = "Server name = (*$server_name*)";
		YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
		if ($server_name == "gsm-internal"){
			return "http://127.0.0.1:8086/";

		}elseif ($server_name == "gsm-public"){
			return "http://127.0.0.1:8097/";

		}elseif ($server_name == "globalsoccermetrix"){
			return "http://127.0.0.1:8090/";

		} else {

			return "http://127.0.0.1/";
		}
    }

    public static function getAppSetting__root_folder(){
        $root_folder = $_SERVER["DOCUMENT_ROOT"];
        $root_folder = add_trailing_slash($root_folder);
        // This will return the path of the currently executing script, not the actual root for the site
        return $root_folder;
    }

    public static function getAppSetting__process_output_folder(){
        $root_folder = getAppSetting__root_folder();
        //$root_folder = add_trailing_slash($root_folder);
        return $root_folder . "__process_output/";
    }

    public static function getUserSetting__updated_by(){
        // @todo pull this value from an object like oAuth in PEAR.
		$user = Yii::app()->user;
		if (isset($user)){
			$user_id = $user->getId();
		} else {
			$user_id = 0;
		}
		if ( (int)$user_id == 0){
			$msg = "The correct \$user_id could not be determined. Returning the default value: (*1*)";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			return 1;

		} else {
			return (int)$user_id;
		}
    }
}
// </editor-fold>





