<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Prototype v0.5.0 concept of an import control
 * @deprecated since version 0.3.0
 * @todo dbyrd flesh this out if you see potential for behaviours and such
 */
class bimport extends CApplicationComponent{
	
	public function init() {
	}

	/**
	 * import pending data files by file type
	 * @param
	 * @return string
	 * @example http://gsm-public:8097/index.php/data/importSpecialReport?XDEBUG_SESSION_START=netbeans-xdebug
	 * @internal Development Status = ready for testing (rft)
	 */

	/**
	 * Process all outstanding files awaiting importation
	 * @param type $param
	 */
	protected static function processManager($param) {
		self::processFiles($param);
	}

	/**
	 * Process all outstanding files awaiting importation
	 * @param type $useCase
	 */
	protected static function fileSelection($useCase) {
		// @todo: get default folders
		// @todo: get files
		// @todo: apply filter criteria
		self::processFiles($param);
	}

	/**
	 * Process all outstanding files awaiting importation
	 * @param type $files
	 */
	protected static function processFiles($files) {
		foreach ($files as $file){
			self::processFile($file);
		}
	}

	/**
	 * Process all outstanding files awaiting importation
	 * @param type $file
	 */
	protected static function processFile($file) {
		$fileRows = []; // ?
		foreach ($fileRow as $fileRow){
			self::processFileRow($row);
		}
	}


	/**
	 * Process all outstanding files awaiting importation
	 * @param type $param
	 */
	protected static function processFileRow($param) {

	}

	/**
	 * Prototype data row event handler
	 * @param type $row
	 * @internal Development Status = pre code construction
	 */
	protected static function processCsvRow($row) {

	}
}