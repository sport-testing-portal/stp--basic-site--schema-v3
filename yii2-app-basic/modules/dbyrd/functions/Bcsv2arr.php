<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: Bcsv2arr::convert_to_array('somefile');
 */
//class csv_to_table_array{
class bcsv2arr extends CApplicationComponent
{


	// <editor-fold defaultstate="collapsed" desc="class properties">
	//class csv_to_table_array{

    private static $debug_yn     = "Y";
    private static $csv_file_uri = "";
	private static $_csv_file_uri2; // test yii getters and setters

	// </editor-fold>

	public function init() {
	}


	// <editor-fold defaultstate="collapsed" desc="csv_to_table_array class">
    function __construct($csv_file_uri=null){
        if (! empty($csv_file_uri) && file_exists($csv_file_uri)){
            $this->csv_file_uri = $csv_file_uri;
        }
    }

	/**
	 * converts a csv file into an array
	 * @param string $csv_file_uri
	 * @return array
	 */
    public static function convert_to_array($csv_file_uri){
        // This is the method to call to create a data table array
        $debug_yn = self::$debug_yn;

        // Arrays we'll use later
        $keys     = array();
        $newArray = array();

        // Do it
        if (file_exists($csv_file_uri)){
            $data = self::csvToArray($csv_file_uri, ',');
        } else {
            die("CSV data file is missing");

        }
        // Set number of elements (minus 1 because we shift off the first row)
        $count = count($data) - 1;

        //Use first row for names
        $labels = array_shift($data);

        foreach ($labels as $label) {
          $keys[] = $label;
        }

        // Add Ids, just in case we want them later
        $keys[] = 'array_row_id';

        for ($i = 0; $i < $count; $i++) {
          $data[$i][] = $i;
        }

        // Bring it all together
        for ($j = 0; $j < $count; $j++) {
          $d = array_combine($keys, $data[$j]);
          $newArray[$j] = $d;
        }

        return $newArray;
    }

	/**
	 * Create a process meta-data array. The output is typically wrapped by encryption.
	 * @param string $exp
	 * @return string
	 * @see Bjede.php
	 */
    public static function json_process_array($table_like_array, $process_name=NULL, $process_id=NULL, $process_program_name=NULL){
        if (array_key_exists("json", $table_like_array)){
            $data = $data__raw_array["json"];   // pulls the table array out
        } else {
            $data = $table_like_array;          // data['some_table_name'][row_index]= array(field=>value)
        }
        $meta_data = array("process_info"=>array(
            "process_name"                           => $process_name
            ,"process_id"                            => $process_id
            ,"host_controller__program_name"         => $process_program_name
            ,"host_controller__program_parms"        => array()
            ,"host_controller__status_msg_to_caller" => ""
            ,"callback_success__url"                 => ""
            ,"callback_success__notification_list"   => "dana@globalsoccermetrix.com;jami@globalsoccermetrix.com"
            ,"callback_failure__url"                 => ""
            ,"callback_failure__notification_list"   => "dana@globalsoccermetrix.com;jami@globalsoccermetrix.com"
            ,"misc"=>array('logs'=>array('sql'=>''))

        ));
        $data_blob = array("json"=>array("meta_data"=>$meta_data, "data"=>$data));
        return $data_blob;
    }

    // Function to convert CSV into associative array
	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
    public static function csvToArray($file_uri, $delimiter) {
        if (($handle = fopen($file_uri, 'r')) !== FALSE) {
            $arr = array();
            $i   = 0;
            while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) {
                for ($j = 0; $j < count($lineArray); $j++) {
                    $arr[$i][$j] = $lineArray[$j];
                }
                $i++;
            }
            fclose($handle);
        }
        return $arr;
    }

	/**
	 * Converts a data array to a json process array (jpa)
	 * @param string $exp
	 * @return string
	 */
    public static function convert_data_array_to_jpa($data_array, $target_table_name="", $process_name=NULL, $process_program_name=NULL, $process_id=NULL){
        //$table_like_array  = array("footages"=>$data_array);
        $table_like_array  = array($target_table_name=>$data_array);

		if (empty($process_name)){
			$process_name = "GSM File Import";
		}
		if (empty($process_program_name)){
			$process_program_name = "data/ImportSpecialReport";
		}
		if (empty($process_id)){
			$process_id = 1;
		}
        $use_process_header_array_yn      = "Y";

        if ($use_process_header_array_yn == "Y"){
            $json_preped_array = self::json_process_array(
                    $table_like_array, $process_name
                    , $process_id, $process_program_name);

        } else {
            $json_preped_array = array("json"=>$table_like_array);
        }
        return $json_preped_array;
    }

	/**
	 * write out a data array to a json file
	 * @param array $exp
	 * @return string
	 */
    public static function write_to_json_file($data_array,$table_name=NULL, $process_name=NULL, $process_program_name=NULL, $process_id=NULL){
		if (empty($table_name)){
			$table_name = "person";
		}
		$debug_yn = self::$debug_yn;
        $table_like_array  = array("$table_name"=>$data_array);

        $use_process_header_array_yn      = "Y";

        if ($use_process_header_array_yn == "Y"){
            $json_preped_array = self::json_process_array(
                    $table_like_array, $process_name
                    , $process_id, $process_program_name);
        } else {
            $json_preped_array = array("json"=>$table_like_array);
        }


        $json_string = Bjson::json_encode_wtype($json_preped_array);
        if ($debug_yn == "Y"){
            //echo_array($newArray, $encode_html_entities_yn="N");
            Barray::echo_array($json_preped_array, $encode_html_entities_yn="N");
            echo "JSON string length: " . strlen($json_string) . "<br />";
            echo $json_string ."<br />";
        }

        Bfile::write_to_file($json_target_file_uri, $json_string, $write_mode="clear-then-write");  // write out pure JSON
        //write_to_file($json_target_file_uri, $json_string);

        $jede = new Bjede();
        $jede_string = $jede->convert_json_str_to_jede_str($json_string);
        Bfile::write_to_file($json_target_file_uri . ".jede", $jede_string, $write_mode="clear-then-write");  // write out JEDE
        if ($debug_yn == "Y"){
            //echo $jede_string;
        }

    }

	/**
	 * Write a json string to an encrypted file.
	 * @param string $json_string
	 * @return string
	 * @see Bjede.php
	 */
    public static function write_json_str_to_jede_file($json_string){
		$debug_yn = self::$debug_yn;
        $jede = new Bjede();
        $jede_string = $jede->convert_json_str_to_jede_str($json_string);
        Bfile::write_to_file($json_target_file_uri . ".jede", $jede_string, $write_mode="clear-then-write");  // write out JEDE
        if ($debug_yn == "Y"){
            //echo $jede_string;
        }
    }

	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
    public static function echo_as_json(){

        //if (strlen($feed) === 0){
        //    die("feed is empty");
        //}


        // Print it out as JSON
        //echo json_encode($newArray);
        //echo_array($newArray, $encode_html_entities_yn="N");


    }
	// </editor-fold>
}
// </editor-fold>





