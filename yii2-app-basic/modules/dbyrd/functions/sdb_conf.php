<?php

class sdb_conn extends PDO {
    // Originally called SafePDO. Renamed
    // Connect to the database with defined constants
    // $dbh = new SafePDO(PDO_DSN, PDO_USER, PDO_PASSWORD);
    // $dbh = new SafePDO("www-remote-pdo");
    // $dbh = new SafePDO("www-local-pdo");
    private $verbose_yn = "N";
    private $debug_yn   = "N";


    public static function exception_handler($exception) {
        // Output the exception details
        // PDOException::exception

        if ($exception){
            $server_name   = $_SERVER["SERVER_NAME"];
            $location_name = firewall::get_allowed_client_location_name();
            if ($server_name == "localhost"){
                var_dump($exception);
                exit();
            }
            if ($_SERVER["SERVER_NAME"] == "gsm_public"){
                var_dump($exception);
                //die('Uncaught exception: ' . $exception->getMessage());
            // } elseif (firewall::get_allowed_client_location_name() == "aus.dev.stalkr.com") {
            } elseif ($location_name == "aus.dev.gsm.com") {
                var_dump($exception);
                //echo "<pre><code>".$exc->getTraceAsString()."</code></pre>";
            //} elseif (firewall::get_allowed_client_location_name() == "www.stalkr.com") {
            } elseif ($location_name == "local.development") {
                var_dump($exception);
            }
        }
        //die('Uncaught exception: ' . $exception->getMessage());
    }

    public function __construct($db_config_name, $persist_yn=null) {
    //public function __construct($dsn, $username='', $password='', $driver_options=array()) {

        //$config = get__db_config_array($db_config_name);
        $config = sdb_conf::get__db_config_array($db_config_name);
        if ($this->verbose_yn == "Y"){
            echo_array($config);
        }
        // Temporarily change the PHP exception handler while we . . .
        set_exception_handler(array(__CLASS__, 'exception_handler'));

        // . . . create a PDO object
        $dsn            = $config['connection']['dsn'];
        $username       = $config['connection']['username'];
        $password       = $config['connection']['password'];
        $driver_options = $config;
        parent::__construct($dsn, $username, $password, $driver_options);

        // Change the exception handler back to whatever it was before
        restore_exception_handler();

        // the following tells PDO we want it to throw Exceptions for every error.
        // this is far more useful than the default mode of throwing php errors
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if (empty($persist_yn)){
            $persist_yn = "N";
        }
        if ($persist_yn == "Y"){
            $this->setAttribute(PDO::ATTR_PERSISTENT, PDO::ATTR_PERSISTENT);
        }
    }
}

