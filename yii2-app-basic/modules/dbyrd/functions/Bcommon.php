<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: barray::CDbExpression('NOW()');
 */

class bcommon extends CApplicationComponent
{
	public function init() {
	}

	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
	public static function CDbExpression($exp, $db='db') {
		if($exp == 'NOW()') {
			if (substr(Yii::app()->{$db}->connectionString, 0, 7) === 'sqlsrv:') {
				return new CDbExpression('GETDATE()');
			}
		}

		//otherwise
		return new CDbExpression($exp);
	}

	/**
	 * Parses a full name into a name parts arrray
	 * @param string $full_name
	 * @return array[]
	 */
	public static function splitNameIntoParts($full_name){
		$name_parts = explode(" ", $full_name);
		$name_array = array();
		if (count($name_parts) == 2){
			$name_array["first_name"] = $name_parts[0];
			$name_array["middle_name"]= '';
			$name_array["last_name"]  = $name_parts[1];
			$name_array["name_suffix"]	= '';
			$name_array["full_name"]  = $full_name;
		}
		if (count($name_parts) == 3){
			$name_array["first_name"]	= $name_parts[0];
			$name_array["middle_name"]	= $name_parts[1];
			$name_array["last_name"]	= $name_parts[2];
			$name_array["name_suffix"]	= '';
			$name_array["full_name"]	= $full_name;
		}
		if (count($name_parts) == 4){
			$name_array["first_name"]	= $name_parts[0];
			$name_array["middle_name"]	= $name_parts[1];
			$name_array["last_name"]	= $name_parts[2];
			$name_array["name_suffix"]	= $name_parts[3];
			$name_array["full_name"]	= $full_name;
		}
		if (count($name_parts) > 4){
			$name_array["first_name"]	= $name_parts[0];
			$name_array["middle_name"]	= $name_parts[1];
			$name_array["last_name"]	= $name_parts[2];
			$name_array["name_suffix"]	= $name_parts[3];
			$name_array["full_name"]	= $full_name;
		}
		if (count($name_parts) < 2){
			$name_array["first_name"]	= $name_parts[0];
			$name_array["middle_name"]	= '';
			$name_array["last_name"]	= '';
			$name_array["name_suffix"]	= '';
			$name_array["full_name"]	= $full_name;
		}

		return $name_array;
	}

	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
	public static function importCsv($exp, $db='db') {
		// @todo: insert code from bc/tools/import.csv.file.php


		if($exp == 'NOW()') {
			if (substr(Yii::app()->{$db}->connectionString, 0, 7) === 'sqlsrv:') {
				return new CDbExpression('GETDATE()');
			}
		}

		//otherwise
		return new CDbExpression($exp);
	}



// <editor-fold defaultstate="collapsed" desc="unclassified functions">
// class common {


    public static function debug_php($debug_yn=NULL) {
        // Author: Dana Byrd
        if ( empty($debug_yn) ) {
            //$debug_yn = "N";
            return;
        }
        if (isset($_SERVER['SERVER_PORT'])){
            if ($_SERVER['SERVER_PORT'] != 8086) {
                // Never allow this routine to run in the wild... (e.g. on a public facing server).
                return;
            }
        }

        if ($debug_yn == "Y") {
            // <editor-fold defaultstate="collapsed" desc="Enable error reporting from PHP">
            // enable error reporting from PHP
            $eh_status = error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            // </editor-fold>
        } elseif ($debug_yn == "N") {
            // <editor-fold defaultstate="collapsed" desc="Disable PHP low-level error output">
            //error_reporting(E_ALL);
            $eh_status = error_reporting();
            ini_set('display_errors', FALSE);
            ini_set('display_startup_errors', FALSE);
            // </editor-fold>
        }
        return $eh_status;
    }

    public static function get_content($URL){
        //echo get_content('http://example.com');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $URL);
        $data = curl_exec($ch);
        curl_close($ch);
        $len = strlen($data);
        return $data;
    }

    public static function page__is_a_POSTBACK(){
        return ($_SERVER['REQUEST_METHOD'] == 'POST');
    }

    public static function get__image_format_list()
    {
        $image_format_list = array("jpg", "png", "gif", "bmp","jpeg","tif", "PNG","JPG","JPEG","GIF","BMP", "TIF");
        return $image_format_list;
    }

    public static function error_Handler($method_name, $line_number_with_error, $err_msg){
        // Development Status = Golden!
        echo "<span style='color:blue;'>$method_name</span> Line No: <b>$line_number_with_error</b><br />";
        echo "<span style='color:red;'><pre>".nl2br($err_msg)."</pre></span><br />";
    }

    public static function convertToMySqlValue($currentValue, $sqlType="text", $default="null") {
        // Should escape single quotes now. Needs regression testing.
        // Examples:
        //   convertToMySqlValue($currentValue, "text", "null")
        //   convertToMySqlValue($currentValue)
        // All input will be strings.
        if (strlen($currentValue) == 0) {
            $value = $default;
        }
        else
        {
            $value = $currentValue;
            if ($currentValue != "null" and ($sqlType=="text"
                    or $sqlType=="datetime"
                    or $sqlType=="timestamp"
                    or stripos($sqlType, "char") !== false)) {
                if (stripos($value, chr(39))>1) {
                    if ($value[0] == chr(39) && $value[strlen($value) -1] == chr(39)){
                        // This string is delimited with single quotes. Remove them.
                        $value = substr($value, 1, strlen($value) -2);
                    }
                    // escape single quotes
                    $value = str_ireplace("'","\\'",$value);
                    $value = $value[0]==chr(39) ? $value : "'".$value."'";
                } else {
                // if the 1st character is a single quote then return the current value, otherwise add single quotes.
                    $value = $value[0]==chr(39) ? $currentValue : "'".$currentValue."'";
                }
            }
        }
        return $value;
    }

    public static function get_ip_address() {
        // Development Status = Golden! This works very well!
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }

    public static function findStringInHaystackUsingNeedleArray($haystack, $needle_array) {
        // Returns -1 if the needle is not found
        foreach($needle_array as $key => $needle) {
           if(stripos($haystack, $needle) !== false) {
                return $key;  /// Return the zero based index of the string found.
           }
        }
        return -1; /// all lookup attempts failed
    }

    // Miscellanious //
    public static function html__create_dropdown($html_identifier, $pairs, $firstentry=""
            , $multiple="", $selected_key="" ,$visible_item_cnt="5", $html_title="select one"){
        // Development Status: Golden!
        // History:
        //   2011-12-13 0.1.3.3 bug (dab): Added padding-left: 3px; to inline style.
        //   2011-03-10 0.1.2.2 bug (dab): Added height to inline style.
        //   2010-04-21 0.0.1.1 bug (dab): corrected the select end tag.
        // Example: echo html__create_dropdown("client", client_list(), "All Clients");
        // Place the code above inside PHP tags
        // Pass $selected_key to set the default selected item. It will typically be an integer value.
        // Start the dropdown list with the select element and title
        //$dropdown = "<select id=\"$html_identifier\" name=\"select_$html_identifier\" multiple=\"$multiple\" size=\"$visible_item_cnt\" title=\"$html_title\">";
        $dropdown = "<select id=\"$html_identifier\" class=\"round_border_5px\" style=\"width:175px;height:120px;padding-left:3px;\" name=\"select_$html_identifier\" multiple=\"$multiple\" size=\"$visible_item_cnt\" title=\"$html_title\"> \n";
        if ( ! empty($firstentry) ) {
            $dropdown .= "<option name=\"\">$firstentry</option> \n";
        }
        // Create the dropdown elements
        foreach($pairs as $value => $name)
        {   // sample list output:
            $dropdown .= ($value == $selected_key) ?
                "<option value=\"$value\" selected=\"selected\">$name ($value)</option> \n"
                :"<option value=\"$value\">$name ($value)</option> \n";
        }
        // Conclude the dropdown and return it
        $dropdown .= "</select> \n";
        return $dropdown;
    }

    public static function html__create_dropdown__with_text_key($html_identifier, $pairs, $firstentry=""
            , $multiple="", $selected_key="" ,$visible_item_cnt="5", $html_title="select one"){
        // Development Status: Golden!
        // History:
        //   2011-12-13 0.1.3.3 bug (dab): Added padding-left: 3px; to inline style.
        //   2011-03-10 0.1.2.2 bug (dab): Added height to inline style.
        //   2010-04-21 0.0.1.1 bug (dab): corrected the select end tag.
        // Example: echo html__create_dropdown__with_text_key("client", client_list(), "All Clients");
        // Place the code above inside PHP tags
        // Pass $selected_key to set the default selected item. It will typically be an integer value.
        // Start the dropdown list with the select element and title
        //$dropdown = "<select id=\"$html_identifier\" name=\"select_$html_identifier\" multiple=\"$multiple\" size=\"$visible_item_cnt\" title=\"$html_title\">";
        $dropdown = "<select id=\"$html_identifier\" class=\"round_border_5px\" style=\"width:175px;height:120px;padding-left:3px;\" name=\"select_$html_identifier\" multiple=\"$multiple\" size=\"$visible_item_cnt\" title=\"$html_title\"> \n";
        if ( ! empty($firstentry) ) {
            $dropdown .= "<option name=\"\">$firstentry</option> \n";
        }
        // Create the dropdown elements
        foreach($pairs as $value => $name)
        {   // sample list output:
            $dropdown .= ($value == $selected_key) ?
                "<option value=\"$value\" selected=\"selected\">$name</option> \n"
                :"<option value=\"$value\">$name</option> \n";
        }
        // Conclude the dropdown and return it
        $dropdown .= "</select> \n";
        return $dropdown;
    }
    public static function force__HTTPS(){
        // Donated by me at tommygeorge dot com via: http://php.net/manual/en/reserved.variables.php
        if( $_SERVER['HTTPS'] != "on" )
        {
           $new_url = "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
           header("Location: $new_url");
           exit;
        }
    }

    public static function convert_data_array_to_post_url_str($field_values_array){
        //irish [-@-] ytdj [-dot-] ca3 years ago
        // When using the http_build_query function to create a URL query
        // from an array for use in something like
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $post_url), be careful about the url encoding.

        // In my case, I simply wanted to pass on the received $_POST data to
        // a CURL's POST data, which requires it to be in the URL format.
        // If something like a space [ ] goes into the http_build_query,
        // it comes out as a +. If you're then sending this off for POST
        // again, you won't get the expected result. PHP http_build_query is
        // good for GET but not POST.


        //Instead you can make your own simple function if you simply want
        // to pass along the data:

        if (empty($field_values_array) ){
            $field_values_array = $_POST;
        }

        $post_url = '';
        foreach ($field_values_array AS $key=>$value)
            $post_url .= $key.'='.$value.'&';
        $post_url = rtrim($post_url, '&');

        return $post_url;
        // You can then use this to pass along POST data in CURL.


    //    $ch = curl_init($some_url);
    //    curl_setopt($ch, CURLOPT_POST, true);
    //    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_url);
    //    curl_exec($ch);


        // Note that at the final page that processes the POST data, you
        // should be properly filtering/escaping it.

    }

    public static function get__RealIpAddr(){

        //echo_array($_SERVER);
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }

        // proxy test
        $ip_is_proxy_yn = "N";
        // @todo: work with this later
        if ($ip_is_proxy_yn == "Y"){
            $proxy = array();
            // <editor-fold defaultstate="collapsed" desc="old settings style (throws errors">

//            $proxy['HTTP_PRAGMA']           = $_SERVER['HTTP_PRAGMA'];
//            $proxy['HTTP_XONNECTION']       = $_SERVER['HTTP_XONNECTION'];
//            $proxy['HTTP_CACHE_INFO']       = $_SERVER['HTTP_CACHE_INFO'];
//            $proxy['HTTP_XPROXY']           = $_SERVER['HTTP_XPROXY'];
//            $proxy['HTTP_PROXY']            = $_SERVER['HTTP_PROXY'];
//            $proxy['HTTP_PROXY_CONNECTION'] = $_SERVER['HTTP_PROXY_CONNECTION'];
//            $proxy['HTTP_CLIENT_IP']        = $_SERVER['HTTP_CLIENT_IP'];
//            $proxy['HTTP_VIA']              = $_SERVER['HTTP_VIA'];
//            $proxy['HTTP_X_COMING_FROM']    = $_SERVER['HTTP_X_COMING_FROM'];
//            $proxy['HTTP_X_FORWARDED_FOR']  = $_SERVER['HTTP_X_FORWARDED_FOR'];
//            $proxy['HTTP_X_FORWARDED']      = $_SERVER['HTTP_X_FORWARDED'];
//            $proxy['HTTP_COMING_FROM']      = $_SERVER['HTTP_COMING_FROM'];
//            $proxy['HTTP_FORWARDED_FOR']    = $_SERVER['HTTP_FORWARDED_FOR'];
//            $proxy['HTTP_FORWARDED']        = $_SERVER['HTTP_FORWARDED'];
//            $proxy['ZHTTP_CACHE_CONTROL']   = $_SERVER['ZHTTP_CACHE_CONTROL'];
// </editor-fold>
//
            // try a different syntax
            $proxy['HTTP_PRAGMA']           = (( empty($_SERVER['HTTP_PRAGMA']) )           ? "" : $_SERVER['HTTP_PRAGMA']) ;
            $proxy['HTTP_XONNECTION']       = (( empty($_SERVER['HTTP_XONNECTION']) )       ? "" : $_SERVER['HTTP_XONNECTION']);
            $proxy['HTTP_CACHE_INFO']       = (( empty($_SERVER['HTTP_CACHE_INFO']) )       ? "" : $_SERVER['HTTP_CACHE_INFO']);
            $proxy['HTTP_XPROXY']           = (( empty($_SERVER['HTTP_XPROXY']) )           ? "" : $_SERVER['HTTP_XPROXY']);
            $proxy['HTTP_PROXY']            = (( empty($_SERVER['HTTP_PROXY']) )            ? "" : $_SERVER['HTTP_PROXY']);
            $proxy['HTTP_PROXY_CONNECTION'] = (( empty($_SERVER['HTTP_PROXY_CONNECTION']) ) ? "" : $_SERVER['HTTP_PROXY_CONNECTION']);
            $proxy['HTTP_CLIENT_IP']        = (( empty($_SERVER['HTTP_CLIENT_IP']) )        ? "" : $_SERVER['HTTP_CLIENT_IP']);
            $proxy['HTTP_VIA']              = (( empty($_SERVER['HTTP_VIA']) )              ? "" : $_SERVER['HTTP_VIA']);
            $proxy['HTTP_X_COMING_FROM']    = (( empty($_SERVER['HTTP_X_COMING_FROM']) )    ? "" : $_SERVER['HTTP_X_COMING_FROM']);
            $proxy['HTTP_X_FORWARDED_FOR']  = (( empty($_SERVER['HTTP_X_FORWARDED_FOR']) )  ? "" : $_SERVER['HTTP_X_FORWARDED_FOR']);
            $proxy['HTTP_X_FORWARDED']      = (( empty($_SERVER['HTTP_X_FORWARDED']) )      ? "" : $_SERVER['HTTP_X_FORWARDED']);
            $proxy['HTTP_COMING_FROM']      = (( empty($_SERVER['HTTP_COMING_FROM']) )      ? "" : $_SERVER['HTTP_COMING_FROM']);
            $proxy['HTTP_FORWARDED_FOR']    = (( empty($_SERVER['HTTP_FORWARDED_FOR']) )    ? "" : $_SERVER['HTTP_FORWARDED_FOR']);
            $proxy['HTTP_FORWARDED']        = (( empty($_SERVER['HTTP_FORWARDED']) )        ? "" : $_SERVER['HTTP_FORWARDED']);
            $proxy['ZHTTP_CACHE_CONTROL']   = (( empty($_SERVER['ZHTTP_CACHE_CONTROL']) )   ? "" : $_SERVER['ZHTTP_CACHE_CONTROL']);


            //echo_array($proxy);
        }
        return $ip;
    }

    public static function delay_execution($interval_min, $interval_max, $interval_type=NULL,$verbose_yn=NULL){
        // Author: Dana Byrd
        // History: 2011-07-01 0.0.0.1
        //   Delay execution of a PHP routine by seconds, hours, or days.
        if (empty ($verbose_yn)){
            $verbose_yn = "N";
        }
        //$verbose_yn = "N";
        if ( empty($interval_type) ) {
            $interval_type = "s"; // seconds
        } elseif ( $interval_type != "s" && $interval_type != "h" && $interval_type != "d" ) {
            $msg = "Valid interval types are 's', 'h', or 'd' (*$interval_type*)";
            error_Handler(__METHOD__, __LINE__, "<b>$msg</b><br />");
        }
        if ( empty($interval_min) ) {
            $interval_min = "9";
        }
        if ( empty($interval_max) ) {
            $interval_max = "56";
        }

        // Delay execution to reduce the load on the tse server.
        $now_dt = mySqlDate();
        $min_dt = dateMath($now_dt,"+".$interval_min."$interval_type", "Y-m-d H:i:s");
        $max_dt = dateMath($now_dt,"+".$interval_max."$interval_type", "Y-m-d H:i:s");
        $random_dt = date_generate_random_dt($min_dt, $max_dt, $return_array_yn="N", $array_size=NULL);
        if ($verbose_yn == "Y"){
            $time_elapsed = time_elapsed($now_dt, $random_dt);
            echo "Sleeping until $random_dt ($time_elapsed) <br />";
        }
        time_sleep_until(strtotime($random_dt));
    }

    public static function ajax_path($php_file_name=NULL){
        // Author: Dana Byrd
        // This function has a sibling with the same name in my.functions.js
        $ajax_port = $_SERVER['SERVER_PORT'];
        $ajax_addr = $_SERVER['SERVER_ADDR'];
        $ajax_protocol = server_protocol();
        $ajax_host = $_SERVER['HTTP_HOST'];  /// window.location.host;
        $ajax_file_url = $ajax_protocol . "://" . $ajax_host . "/" . $php_file_name;
        return $ajax_file_url;
    }

    public static function server_protocol(){
        if ( ! empty($_SERVER['HTTPS']) ) {
            $protocol = "https";
        } else {
            $protocol = "http";
        }
        return $protocol;
    }

    public static function GUID() {
        // returns lower case
        if (!function_exists('uuid_create'))
            return false;
        uuid_create($context);

        uuid_make($context, UUID_MAKE_V4);
        uuid_export($context, UUID_FMT_STR, $uuid);
        return trim($uuid);
    }

    public static function ForceHTTPS(){
        // Donated by me at tommygeorge dot com via: http://php.net/manual/en/reserved.variables.php
        if( $_SERVER['HTTPS'] != "on" )
        {
           $new_url = "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
           header("Location: $new_url");
           exit;
        }
    }

    public static function getDomainFromURL($url) {
        // Took the base code from: function parse_url_domain ($url) {
        //Copyright 2004 by Anand A.
        // [url]www.polurnet.com[/url]

        $url_array = parse_url($url);
        $domain = $url_array['host'];
        return str_replace ('www.','', $domain);

    }

    public static function getMasterDomainFromURL($tt_announce_url){
        $full_domain = parse_url($tt_announce_url, PHP_URL_HOST);

        if(stripos($full_domain, "www.") !== false && substr($full_domain, 0, 4) == "www."){
            $full_domain = substr($full_domain, 4);
        }
        if(substr_count($full_domain, ".") > 1){
            $tmp = strrev($full_domain);
            $domain_parts = explode(".", $tmp);
            $domain_reversed = $domain_parts[0]. "." .$domain_parts[1];
            $master_domain = strrev($domain_reversed);
        } else {
            $master_domain = $full_domain;
        }
        return $master_domain;
    }

    public static function remove_line_feed($string) {
        if ( empty($string)) {
            return $string;
        }

        // Works with Linux and Windows. There is a bug in the regex version.
        // The str_replace is rock solid!
        // BTW trim() will remove line endings too :)

        //$result = ereg_replace('/\r\n|\r|\n/', '', $string); // for some reason without the '$' all "r" are replaced.
        //$result = ereg_replace('/[\r\n]|[\\r]|[\n]/', '', $string); // This removes all "r"s as well.
        // e.g "torrent" becomes "toent"
//        $result = ereg_replace('/\r\n|$\r|\n/', '', $string);
//        return $result;

        $order   = array("\r\n", "\n", "\r");
        $replace = '';
        // Processes \r\n's first so they aren't converted twice.
        $newstr = str_replace($order, $replace, $string);
        return $newstr;
    }

    public static function str_rpad( $string, $pad_length, $pad_string = ' ' ) {
        // Original contributed by shelby@coolpage.com and modified by dbyrd.
        // via: http://php.net/manual/en/function.str-pad.php
        // see also: str_rpad_illeagal() the original source.
        if (empty($string)){
            return "";
        }
        $return_str = str_pad( $string, $pad_length, $pad_string, STR_PAD_RIGHT );
        return $return_str;
    }

    public static function str_lpad( $string, $pad_length, $pad_string = ' ' ) {
        // Original contributed by shelby@coolpage.com and modified by dbyrd.
        // via: http://php.net/manual/en/function.str-pad.php
        // see also: str_rpad_illeagal() the original source.
        if (empty($string)){
            return "";
        }
        $return_str = str_pad( $string, $pad_length, $pad_string, STR_PAD_LEFT );
        return $return_str;
    }
    public static function getAppSetting__google_php_client_library_path(){
        $google_lib_path = "lib/api/google/google-api-php-client/src/";

        return $google_lib_path;
    }

    public static function get__characters_in_string($string, $verbose_yn="Y"){
        // History:
        //   renamed from getCharactersInString
        // dev sts = golden
        //
        //$verbose_yn = "Y";
        $item_cnt = array();
        $char_dtl = "";
        for ($i = 0, $j = strlen($string); $i < $j; $i++) {
            $char      = $string[$i];
            $ascii_num = ord($string[$i]);
            if (isset($item_cnt[$char])){
                $cnt_bfr = (int)$item_cnt[$char];
                $cnt = $cnt_bfr + 1;
            } else {
                $cnt = 1;
            }
            $item_cnt[$char] = $cnt;
            $char_dtl .= "Char #".$i ." " . $char.', '.$ascii_num . "<br />";
        }
        $unique_pct = number_format(100 - ((sizeof($item_cnt) / $j) * 100),2) . "%";
        if ($verbose_yn == "Y"){
            echo "<pre><code>";
            echo "<br />The $j character string submitted contains ". sizeof($item_cnt) . " unique characters. $unique_pct unique<br />";
            echo "The unique counts follow: <br />";
            echo_array($item_cnt, $encode_html_entities_yn="Y");
            echo "Each character in the string is listed below: <br />";
            echo $char_dtl;
            echo "</code></pre>";
        }
    }

    public static function wordwrap_enforce($string, $width = 75, $break = "\n") {
        // Originally smart_wordwrap() contributed by: http://stackoverflow.com/users/283078/cbuckley
        //   and posted here: http://stackoverflow.com/questions/9815040/smarter-word-wrap-in-php-for-long-words
        // Purpose: split on problem words over the line length
        $pattern = sprintf('/([^ ]{%d,})/', $width);
        $output = '';
        $words = preg_split($pattern, $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        foreach ($words as $word) {
            if (false !== strpos($word, ' ')) {
                // normal behaviour, rebuild the string
                $output .= $word;
            } else {
                // work out how many characters would be on the current line
                $wrapped = explode($break, wordwrap($output, $width, $break));
                $count = $width - (strlen(end($wrapped)) % $width);

                // fill the current line and add a break
                $output .= substr($word, 0, $count) . $break;

                // wrap any remaining characters from the problem word
                $output .= wordwrap(substr($word, $count), $width, $break, true);
            }
        }

        // wrap the final output
        return wordwrap($output, $width, $break);
    }

// CLI Functions (PHP running in the Unix/Linux shell *************************
    public static function isCli() {
        return php_sapi_name()==="cli";
    }

// Date and File Functions Intersection ****************************************
    public static function date_to_filename($date=NULL) {
        // Returns the current datetime as a valid file name
        $dateFormat = 'Y-m-d_His';
        if ( empty($date)){
            $timeStamp = time();
        } else {
            $timeStamp = strtotime($date);
        }
        $dateTime= date($dateFormat,$timeStamp);
        return $dateTime;
    }

// SQL Metadata Functions ***************************************************
    public static function getSqlColumnNames($database_table_name, $db=NULL){
        // Purpose returns an array populated with column names to use with a foreach()
        if (strlen($database_table_name) == 0){
            return -1;
        }
        $verbose_yn = "N";
        $query_columns="select column_name from vwTableField \n"
            ."where table_name = '$database_table_name' \n"
            ." order by ordinal_position; \n";
        if ($verbose_yn == "Y") {
            echo "query: $query_columns <br />";
        }
        if (isset($db)==false){
            $db = getDBobj();
        }
        //$result_columns = mysql_query($query_columns);
        $result = $db->query($query_columns);
        if ($result){
            $i = 0;
            $column_array = array();
            while($column_rso = $result->fetch_object()) {
                $column_name  = $column_rso->column_name;
                $column_array[] = $column_name;
                $i++;
            }
        }
        return $column_array;
    }

    public static function getSqlColumnNamesAndDataType_via_view($database_table_name){
        // Purpose returns an array populated with column names and data types to use with a foreach()
        if (strlen($database_table_name) == 0){
            return -1;
        }
        $query_columns="select column_name, column_type \n"
            ." from vwTableField \n"
            ." where table_name = '$database_table_name' \n"
            ." order by ordinal_position; \n";
    //    if ($verbose_yn == "Y") {
    //        echo "query: $query <br />";
    //    }

        //$result_columns = mysql_query($query_columns);
        //$i = 0;
        //$column_array = array();
        //while($columns = mysql_fetch_array($result_columns,MYSQL_ASSOC)) {
        //    $column_array[$columns["column_name"]] = $columns["column_type"];
        //    $i++;
        //}
        //return $column_array;
        //
        if (isset($db) == false) {
            $db = getDBobj();
        }
        $rso = $db->query($query_columns);
        if ($rso){
            $i = 0;
            $column_array = array();
            while($column = $rso->fetch_object()) {
                $column_array[$column->column_name] = $column->column_type;
                $i++;
            }
        }
        return $column_array;

    }

    public static function getSqlColumnNamesAndDataType($database_table_name){
        // Purpose returns an array populated with column names and data types to use with a foreach()
        if (strlen($database_table_name) == 0){
            return -1;
        }

        $query_columns = ""
            ."select  \n"
            . "c.COLUMN_NAME AS column_name \n"
            . ",c.COLUMN_TYPE AS column_type \n"
            . ",c.ORDINAL_POSITION AS ordinal_position \n"
            . ",length(c.COLUMN_NAME) AS col_len \n"
            . ",mdd.col_name_len_max AS col_name_len_max \n"
            . ",(mdd.col_name_len_max - length(c.COLUMN_NAME)) AS pad_len  \n"
            . "from (information_schema.columns c join db137114_stalkr.meta_data__database mdd  \n"
            . "on((c.TABLE_NAME = convert(mdd.table_name using utf8))))  \n"
            . "where (c.TABLE_SCHEMA = 'db137114_stalkr' and c.TABLE_NAME = '$database_table_name')  \n"
            . "order by c.TABLE_NAME,c.ORDINAL_POSITION \n"
            ;

//        $query_columns="select column_name, column_type \n"
//            ." from vwTableField \n"
//            ." where table_name = '$database_table_name' \n"
//            ." order by ordinal_position; \n";
    //    if ($verbose_yn == "Y") {
    //        echo "query: $query <br />";
    //    }

        //    $result_columns = mysql_query($query_columns);
        //    $i = 0;
        //    $column_array = array();
        //    while($columns = mysql_fetch_array($result_columns,MYSQL_ASSOC)) {
        //        $column_array[$columns["column_name"]] = $columns["column_type"];
        //        $i++;
        //    }
        //    return $column_array;

        if (isset($db) == false) {
            //$db = getDBobj();
            $db = getDBobj__mysqli__www();
        }
        $rso = $db->query($query_columns);
        if ($rso){
            $i = 0;
            $column_array = array();
            while($column = $rso->fetch_object()) {
                $column_array[$column->column_name] = $column->column_type;
                $i++;
            }
        }
        return $column_array;

    }

    public static function getSqlColumnNamesAndDataType_order_by_field_name($database_table_name){
            // Purpose returns an array populated with column names and data types to use with a foreach()
            if (strlen($database_table_name) == 0){
                return -1;
            }

            $query_columns = ""
                ."select  \n"
                . "c.COLUMN_NAME AS column_name \n"
                . ",c.COLUMN_TYPE AS column_type \n"
                . ",c.ORDINAL_POSITION AS ordinal_position \n"
                . ",length(c.COLUMN_NAME) AS col_len \n"
                . ",mdd.col_name_len_max AS col_name_len_max \n"
                . ",(mdd.col_name_len_max - length(c.COLUMN_NAME)) AS pad_len  \n"
                . "from (information_schema.columns c join db137114_stalkr.meta_data__database mdd  \n"
                . "on((c.TABLE_NAME = convert(mdd.table_name using utf8))))  \n"
                . "where (c.TABLE_SCHEMA = 'db137114_stalkr' and c.TABLE_NAME = '$database_table_name')  \n"
                . "order by c.COLUMN_NAME \n"
                ;

    //        $query_columns="select column_name, column_type \n"
    //            ." from vwTableField \n"
    //            ." where table_name = '$database_table_name' \n"
    //            ." order by ordinal_position; \n";
        //    if ($verbose_yn == "Y") {
        //        echo "query: $query <br />";
        //    }

            //$result_columns = mysql_query($query_columns);
            //$i = 0;
            //$column_array = array();
            //while($columns = mysql_fetch_array($result_columns,MYSQL_ASSOC)) {
            //    $column_array[$columns["column_name"]] = $columns["column_type"];
            //    $i++;
            //}
            //return $column_array;

            if (isset($db) == false) {
                //$db = getDBobj();
                $db = getDBobj__mysqli__www();
            }
            $rso = $db->query($query_columns);
            if ($rso){
                $i = 0;
                $column_array = array();
                while($column = $rso->fetch_object()) {
                    $column_array[$column->column_name] = $column->column_type;
                    $i++;
                }
            }
            return $column_array;

        }

    public static function html_dropdown__with_text_key($property_list=NULL, $data_list=NULL){
        // Development Status: Golden!
        // History:
        //   Call this method without parameters to receive a sample array.
        // Example: echo html_dropdown__with_text_key();
        // Place the code above inside PHP tags
        // Pass $selected_key to set the default selected item. It will typically be an integer value.
        // Start the dropdown list with the select element and title
        //$dropdown = "<select id=\"$html_identifier\" name=\"select_$html_identifier\" multiple=\"$multiple\" size=\"$visible_item_cnt\" title=\"$html_title\">";
        $html_property_list_base = array(
            "html_identifier"        =>""
            ,"first_entry"           =>""
            ,"selected_key"          =>""
            ,"visible_item_cnt"      =>""
            ,"html_title"            =>""
            ,"multi_select"          =>""
            ,"width"                 =>""
            ,"height"                =>""
            ,"background-color"      =>""
            ,"color"                 =>""
            ,"font-family"           =>""
        );

        if (! is_array($property_list) ) {
            $msg = "property_list is a required array!";
            error_Handler(__METHOD__, __LINE__, "<b>$msg</b><br />");
            return -1;
        }
        $prop  = $property_list;

            $html_property_defaults  = $html_property_list_base;
            $html_property_defaults["html_identifier"]       = "";
            $html_property_defaults["first_entry"]           = "Non";
            $html_property_defaults["selected_key"]          = "";
            $html_property_defaults["visible_item_cnt"]      = "5";
            $html_property_defaults["html_title"]            = "please select from the list";
            $html_property_defaults["multi_select"]          = "";
            $html_property_defaults["width"]                 = "125px;";
            $html_property_defaults["height"]                = "175px;";
            $html_property_defaults["background-color"]      = "#CCCC99;";

        if ( sizeof($property_list) == 0){

            $prop = $html_property_defaults;
        }

        //$prop  = $html_property_list_base;
        foreach ($html_property_defaults as $pdef_key=>$pdef_val){
            if ( ! array_key_exists($pdef_key, $prop) ) {
                $prop[$pdef_key] = $pdef_val;
            }
        }
        //$dropdown = "<select id=\"$html_identifier\" class=\"round_border_5px\" style=\"width:175px;height:120px;padding-left:3px;\" name=\"select_$html_identifier\" multiple=\"$multiple\" size=\"$visible_item_cnt\" title=\"$html_title\"> \n";
        $dropdown = "<select id=\"" . $prop["html_identifier"]
                . "\" class=\"round_border_5px\" "
                . "style=\"width:" .$prop["width"]
                .   "height:" . $prop["height"]
                .   "background-color:" . $prop['background-color']
                .   "padding-left:3px;\" "
                ."name=\"select_" . $prop["html_identifier"]
                ."\" multiple=\"" . $prop["multi_select"]
                ."\" size=\"" . $prop["visible_item_cnt"] . "\" "
                ." title=\"" . $prop["html_title"] ."\"> \n";
        if ( ! empty($prop["first_entry"] ) ){
            $dropdown .= "<option name=\"\">" . $prop["first_entry"] . "</option> \n";
        }
        if ( ! empty($prop["selected_key"] ) ){
            $selected_key = $prop["selected_key"];
        }

        // Create the dropdown elements
        foreach($data_list as $value => $name)
        {   // sample list output:
            $dropdown .= ($value == $selected_key) ?
                "<option value=\"$value\" selected=\"selected\">$name</option> \n"
                :"<option value=\"$value\">$name</option> \n";
        }
        // Conclude the dropdown and return it
        $dropdown .= "</select> \n";
        return $dropdown;
    }


// JqGrid Metadata Functions **************************************************
    public static function getJqGridSqlTableSelectFieldList($database_table_name){
        // Purpose returns an array populated with column names to use with a foreach()
        $query_columns="select column_name from vwTableField where table_name = '$database_table_name' "
            ." order by ordinal_position;";
        $result_columns = mysql_query($query_columns);
        $i = 0;
        $column_array = array();
        while($columns = mysql_fetch_array($result_columns,MYSQL_ASSOC)) {
            $column_array[] = $columns[column_name];
             $i++;
        }
        array_unshift($column_array, $column_array[0]." as id");
        $field_list = "";
        foreach($column_array as $sql_field) {
            $field_list .= $sql_field .", ";
        }
        // remove the trailing comma
        $field_list = substr($field_list, 0, strlen($field_list) - 2);
        $field_list .= " "; // add a trailing space
        // return the field list for the SQL select
        return $field_list;
    }

    public static function getJqGridSqlTableSelectFieldListAsArray($database_table_name){
        // Purpose returns an array populated with column names to use with a foreach()
        $query_columns="select column_name, column_type"
            ." from vwTableField where table_name = '$database_table_name' "
            ." order by ordinal_position;";
        $result_columns = mysql_query($query_columns);
        $i = 0;
         // Prepend the id field for the jqGrid
        $column_array = array("id"=>"int(11)");
        while($columns = mysql_fetch_array($result_columns,MYSQL_ASSOC)) {
            $column_array[$columns[column_name]] = $columns[column_type];
            $i++;
        }
        array_unshift($column_array, "id");
        return $column_array;
    }

    public static function getMVIdFromURI($uri) {
        // 0.0.1.1
        // @todo: Enable this function!
        return 0;


        if (isset($db)==false){
            $db = getDBobj();
        }
        if (strlen($uri) == 0) {
            return "";
        }
        // MV Id is null, get MV id from mv uri's domain //
        if (! isset($mv_id) or strlen($mv_id)==0) {
        //echo "Capturing the domain from the URL...<br />";
        $mv_domain = getDomainFromURL($uri);
        if (strlen( trim($mv_domain) ) == 0){
            return 0;
        }
        if(substr_count($mv_domain, ".") > 1){
            $mv_domain = getMasterDomainFromURL($uri);
        }
        //echo "\$tse_domain = $tse_domain <br />";

        $query = "select media_vendor_id as mv_id"
                .", common_name from media_vendor "
                    ."where site_url like '%".$mv_domain."%';";
        //echo "\$query = $query <br />";
        $result = $db->query($query);
        if($result) {
            $row_cnt = $result->num_rows;
            if ($row_cnt > 0){
                $row         = $result->fetch_object();
                $mv_id       = (int)$row->mv_id;
                $common_name = $row->common_name;
            } else {
                $tse_id = 0;
            }

        } else {
            echo "TSE lookup by domain failed: ($query).<br />";
            echo "\$db->error = $db->error <br />";
        }

            }
        return $tse_id;
    }

    public static function get__row_ckey(){
        $query = "select fnSql_request_id() as row_ckey;";
        if (isset($db) == false) {
            $db = getDBobj();
        }
        $result = $db->query($query);
        if ($result) {
            $row = $result->fetch_object();
            $row_ckey = $row->row_ckey;
            return $row_ckey;
        } else {
            error_Handler(__METHOD__, __LINE__, "<b>" . $db->error . "</b><br />$query");
            return -1;
        }
    }

    public static function base64_encode_image ($filename, $filetype=NULL) {
        if(empty($filetype)) {
            $filetype = pathinfo($filename, PATHINFO_EXTENSION);
        }
        if (file_exists($filename)) {
            $imgbinary = @file_get_contents($filename);
            return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
        } else {
            return $filename;
        }
    }


/*
    function get__functions_from_file($file_name, $rtn_type="sig", $sort_yn="Y"){
        // valid rtn types: sig, name, nameparen, meta, all
        // History..:
        //   2013-08-04 0.0.2.2 Add meta return type, add natural order sorting.
        //   2013-08-04 0.0.1.1 debug sig return type, add natural order sorting.
        // Notes....:
        //    $sig_list       = get__functions_from_file($file_name, $rtn_type="sig");
        //    $name_list      = get__functions_from_file($file_name, $rtn_type="name");
        //    $nameparen_list = get__functions_from_file($file_name, $rtn_type="nameparen");
        //    $meta_list      = get__functions_from_file($file_name, $rtn_type="meta");


        if ( !file_exists($file_name)){
            return "file not found";
        }

        $content = file_get_contents($file_name);

        //$regex   = "function\s(.+?)\((.*?)\)";
        $regex   = 'function\s(.+?)\((.*?)\)';
        $regex_mods = "i";


//        preg_match_all("/$regex/$regex_mods", $content, $matches, PREG_SET_ORDER);
        $a = preg_match_all('/function\s(.+?)\((.*?)\)/i', $content, $matches, PREG_SET_ORDER);
        $match_cnt = sizeof($matches);

        $func_list = array();

        switch ($rtn_type){
            case "sig":
                foreach ($matches as $match ){
                    // the extractions need to be on match, not matches
                    $func_sig    = $match[1] . "(" . $match[2] . ")";      // test version
                    $func_name   = $match[1];                              // test version
                    //$func_list[$func_name] = $func_sig;
                    $func_list[] = $func_sig;  // return an indexed array (numeric keys)
                }

                break;
            case "meta":
                // @todo: return named rows using the function base name, this to ease sorting
                // returns the structure [0]"file_name",[0]"functions"=>array()
                $func_list               = array();
                $func_list__sort_base    = array();
                $func_list__sort_content = array();
                foreach ($matches as $match ){
                    $func_sig    = $match[1] . "(" . $match[2] . ")";      // test version
                    $func_name   = $match[1];      // test version
                    //$func_list[$func_name] = $func_sig;
                    $func_list[] = array(
                        "function_name_base"  =>$func_name
                        ,"function_name_paren"=>$func_name . "()"
                        ,"function_signature" =>$func_sig);

                    $func_list__sort_content[$func_name] = array(
                        "function_name_base"  =>$func_name
                        ,"function_name_paren"=>$func_name . "()"
                        ,"function_signature" =>$func_sig);
                    $func_list__sort_base[]   = $func_name;
                }

                sort($func_list__sort_base, SORT_NATURAL | SORT_FLAG_CASE);
                $func_list_sorted = array();
                foreach($func_list__sort_base as $key=>$function_name){
                     $func_list_sorted[] = $func_list__sort_content[$function_name];
                }

                if($sort_yn == "Y"){
                    $func_list_to_return = array(
                        "file_name"=>$file_name
                        ,"functions"=>$func_list_sorted);
                } else {
                    $func_list_to_return = array(
                        "file_name"=>$file_name
                        ,"functions"=>$func_list);
                }
                // bypas the  sort code at bottom of function that will jack up the
                // sorting of this carefully pre sorted multi-array
                return $func_list_to_return;
                break;
            case "name":
                foreach ($matches as $match ){
                    $func_list[] = $match[1] ;
                }

                break;
            case "nameparen":
                foreach ($matches as $match ){
                    $func_list[] = $match[1] . "()";
                }

                break;
            default :
                break;
        }

        if($sort_yn == "Y"){
            sort($func_list, SORT_NATURAL | SORT_FLAG_CASE);
        }
        return $func_list;

    }
*/

	/**
	 * Prototype for selecting mysql data into a json shaped array that has a root node.
	 * @param type $query
	 * @param type $root_node_name
	 * @return type
	 * @internal Should be disallowed completely due to the mysqli reference!
	 * @todo ? rebuild as a PDO function?
	 * @internal Development Status = ready for testing (rft)
	 */
    public static function get__array_from_a_sql_select($query, $root_node_name="root"){

		//        if (debug_yn == "Y") {
		//            echo "get unpublished_clips query: <pre>".nl2br($query) ."</pre><br />";
		//        }
		$verbose_yn = "N";
		$db = getDBobj();
		$result = mysqli_query($db, $query);
		$data_array = array($root_node_name, array());
		$i = 0;

		while ($row = mysqli_fetch_assoc($result)) {
			//$data_array[$row['name']] = $row['value'];
			if ($i > 0){
				exit;
			}
			$i++;

			if ($verbose_yn == "Y"){
				echo "<h1>data extraction array</h1> <br />";
				echo_array($row);
			}


			echo "<h1>data extraction array converted to JSON string for web send</h1><br />";
			$jarray = array("$root_node_name"=>$row);
			//echo_array($row);
			//$x = json_encode($jarray);
			//var_dump($x);
			return $jarray;
		}

		if (isset($db)==false) {
			$db = getDBobj();
		}

		$result = $db->query($query);
		if ($result){
			return $result;
		}
    }

	/**
	 *
	 * @global type $array
	 * @param type $table_name
	 */
    public static function array_to_mysql_update__needs_testing($table_name) {
        global $array;
        $update = 'update $table_name set ';
        foreach($array as $key => $value) {
            if ($i) { $update .= key($array);
                $check = current($array);
                if (isset($check)) {
                $update .= '=\''.current($array).'\'';
                } else {  $update .= '=null'; }
            } else { $key = key($array);
            $current = current($array);
            $end = "where $key = $current"; }

           next($array);
           $check = key($array);
           if (isset($check)) {
               if ($i) {$update .= ', '; }
            } else {$update .= ' '.$end; }
          $i++;
        }
        $result = mysql_query($update) or die(mysql_oops($update));
        echo 'Updated';
     }


    public static function array__data_set__to_sql_insert($array__dataset){
        //  expects one or more tables
        $sql_insert      = "";
        $sql_insert_list = "";
        foreach($array__dataset as $table_name=>$array__data_table__rows){
            // @todo: validate table name against schema?
            $sql_insert = array__data_table__to_sql_insert(array($table_name=>$array__data_table__rows));
            $sql_insert_list .= $sql_insert . "\n";
        }
        return $sql_insert_list;
    }

	
    public static function array__data_table__to_sql_insert($array__data_table){
        //  expects only one data tables
        $table_name      = "";
        $sql_insert      = "";
        $sql_insert_list = "";
        foreach($array__data_table as $table_name=>$array__data_rows){
            // @todo: validate the table name against the schema?
            foreach($array__data_rows as $row_key=>$array__data_row){
                $sql_insert = array__data_row__to_sql_insert($array__data_row, $table_name);
                $sql_insert_list .= $sql_insert . "\n";
            }
			unset($row_key);
        }
        return $sql_insert_list;
    }

    public static function array__data_row__to_sql_insert($array__data_row, $table_name){
        //  expects one data row
        if (array_key_exists('array_row_id', $array__data_row)){
            unset($array__data_row['array_row_id']);
        }

        $table_columns = array_keys($array__data_row);
        $table_values  = array_values($array__data_row);

        foreach ($table_values as $key=>$value){
            $table_values[$key] = mysql_real_escape_string($value);
        }
        //$fields = "'" . implode("','", $table_columns) . "'";
        $fields =  implode(",", $table_columns) ;
        $values = "'" . implode("','", $table_values) . "'";
        $sql = "insert into $table_name ("
                .$fields
                .") values ("
                .$values
                .");";
        return $sql;
    }

	/**
	 * generically convert a type name value into a type-table primary key row id
	 * @param string $type_name_value, string $type_table_name
	 * @return int type_id
	 * @internal this should be a universal component
	 * @example Bcommon::get_type_id_from_type_name('', '')
	 */
	public static function get_type_id_from_type_name($type_name_value, $type_table_name){
		// if $type_table is a model name?
		$type_table_model_name = Bcommon::generateYiiModelClassName($type_name_value);
//		if ($type_table_name == strtolower($type_name_value)){
//			// it is probably a table name and not a model name.
//			// Validate
//		}
		$type_name_field_name = $type_table_name . "_name";
		$primary_key_field    = $type_table_name . "_id";
		$model = new $type_table_model_name();
		$row = $model->findByAttribute(
				array($type_name_field_name => $type_name_value)
				);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($row, 10, true);

		$id = $row->attributes[$primary_key_field];
		return $id;
	}

	/**
	 * Convert a table name to a Yii relation name.
	 * Purpose: Generate Yii relation names to automate the use of Yii models and child record selections
	 * Source: /framework/gii/generators/model/ModelCode.php*
	 * @param string the name of the table to hold the relation
	 * @param string the foreign key name
	 * @param boolean whether the relation would contain multiple objects
	 * @return string the relation name
	 */
	protected function generateRelationName($tableName, $fkName, $multiple)
	{
		if(strcasecmp(substr($fkName,-2),'id')===0 && strcasecmp($fkName,'id'))
			$relationName=rtrim(substr($fkName, 0, -2),'_');
		else
			$relationName=$fkName;
		$relationName[0]=strtolower($relationName);

		if($multiple)
			$relationName=$this->pluralize($relationName);

		$names=preg_split('/_+/',$relationName,-1,PREG_SPLIT_NO_EMPTY);
		if(empty($names)) return $relationName;  // unlikely
		for($name=$names[0], $i=1;$i<count($names);++$i)
			$name.=ucfirst($names[$i]);

		$rawName=$name;
		$table=Yii::app()->{$this->connectionId}->schema->getTable($tableName);
		$i=0;
		while(isset($table->columns[$name]))
			$name=$rawName.($i++);

		return $name;
	}

	/**
	 * Convert a table name to a Yii model name.
	 * Purpose: Generate Yii relation names to automate the use of Yii models and child record selections
	 * Source: /framework/gii/generators/model/ModelCode.php*
	 * @param string the name of the table
	 * @return string the Yii model name
	 */
	public static function generateYiiModelClassName($tableName)
	{
		// dbyrd: table names should never be passed to this function with a schema prefix
		//if($this->tableName===$tableName || ($pos=strrpos($this->tableName,'.'))!==false && substr($this->tableName,$pos+1)===$tableName)
		//	return $this->modelClass;

		$tableName=  self::removePrefix($tableName,false);
		if(($pos=strpos($tableName,'.'))!==false) // remove schema part (e.g. remove 'public2.' from 'public2.post')
			$tableName=substr($tableName,$pos+1);
		$className='';
		foreach(explode('_',$tableName) as $name)
		{
			if($name!=='')
				$className.=ucfirst($name);
		}
		return $className;
	}


	/**
	 * Convert a table name to a Yii model name.
	 * Purpose: Generate Yii relation names to automate the use of Yii models and child record selections
	 * Source: /framework/gii/generators/model/ModelCode.php*
	 * @param string the name of the table
	 * @return string the Yii model name
	 * @internal tablePrefix is hard coded as a null length string
	 */
	protected static function removePrefix($tableName,$addBrackets=true)
	{
		$prefix='';
		//if($addBrackets && Yii::app()->{$this->connectionId}->tablePrefix=='')
		//	return $tableName;
		//$prefix=$this->tablePrefix!='' ? $this->tablePrefix : Yii::app()->{$this->connectionId}->tablePrefix;
		if($prefix!='')
		{
			if($addBrackets && Yii::app()->{$this->connectionId}->tablePrefix!='')
			{
				$prefix=Yii::app()->{$this->connectionId}->tablePrefix;
				$lb='{{';
				$rb='}}';
			}
			else
				$lb=$rb='';
			if(($pos=strrpos($tableName,'.'))!==false)
			{
				$schema=substr($tableName,0,$pos);
				$name=substr($tableName,$pos+1);
				if(strpos($name,$prefix)===0)
					return $schema.'.'.$lb.substr($name,strlen($prefix)).$rb;
			}
			elseif(strpos($tableName,$prefix)===0)
				return $lb.substr($tableName,strlen($prefix)).$rb;
		}
		return $tableName;
	}

	/**
	 *
	 * @param int $person_id
	 * @throws CHttpException
	 * @version 1.0.0
	 * @internal Development Status = Golden!
	 */
	public static function fetchDemoPicture($person_id)
	{
		/**
		 * @var ProfileForm
		 * @see protected/modules/usr/models/ProfileForm.php
		 * @see protected/modules/usr/models/BaseUsrForm.php
		 */
		// get the person id from the user
		//$user_id = Yii::app()->user->id;
		$debug = false;
		//$debug = true;

		$Person = Person::model()->findByPk($person_id);

		$image_name	= $Person->person_image_headshot_url;
		$image_alt	= $Person->person_name_full;


		if (empty($image_name)){
			if($Person->gender_id == 1){
				$image_name='anonymous.headshot.female.jpg';
			} else {
				$image_name='anonymous.headshot.male.png';
			}
		}

		$yii_unpublished_asset_path = 'application.assets.img.users.demophotos';

		$base_published_asset_path = Yii::app()->assetManager->publish(Yii::getPathOfAlias($yii_unpublished_asset_path));
		$published_asset_image_dir  = $base_published_asset_path . '/';


		$rPath = realpath(Yii::app()->request->baseUrl);

		if (file_exists(($rPath . '/' . $published_asset_image_dir . $image_name)) !== false){
			//$img_size = ["width"=>"145px" ,"height"=>"auto", "class"=>"thumbnail"];
			$img_size = [];

			$img__uri	= $published_asset_image_dir . $image_name;
			$img__tag	= CHtml::image($img__uri,
				$image_alt,
				$img_size);
			$out = $img__tag;

		} else {
			$out = '';
		}

		if($debug){
			$msg = "Image URI";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($img__uri, 10, true);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($base_published_asset_path, 10, true);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rPath, 10, true);

			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($out, 10, true);
		}
		return $out;
	}


	public static function crypto_rand_secure($min, $max) {
			$range = $max - $min;
			if ($range < 0) return $min; // not so random...
			$log = log($range, 2);
			$bytes = (int) ($log / 8) + 1; // length in bytes
			$bits = (int) $log + 1; // length in bits
			$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
			do {
				$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
				$rnd = $rnd & $filter; // discard irrelevant bits
			} while ($rnd >= $range);
			return $min + $rnd;
	}

	public static function getToken($length){
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		for($i=0;$i<$length;$i++){
			$token .= $codeAlphabet[self::crypto_rand_secure(0,strlen($codeAlphabet))];
		}
		return $token;
	}

 }



// </editor-fold>


