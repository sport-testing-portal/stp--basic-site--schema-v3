<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Notes...:
 *   Sample call: fn::CDbExpression('NOW()');
 */

class bjson extends CApplicationComponent
{
	public function init() {
	}

	/**
	 * handle NOW() function for ms-sql server
	 * @param string $exp
	 * @return string
	 */
	public static function CDbExpression($exp, $db='db') {
		if($exp == 'NOW()') {
			if (substr(Yii::app()->{$db}->connectionString, 0, 7) === 'sqlsrv:') {
				return new CDbExpression('GETDATE()');
			}
		}

		//otherwise
		return new CDbExpression($exp);
	}


	// <editor-fold defaultstate="collapsed" desc="json class">
    // Common usuage:
    //
    //

	/**
	 *
	 * @param type $array
	 * @return string utf-8 | string-thats-not-utf8
	 * @example bjson::decode_wtype() description
	 * @example bjson::encode_wtype() description
	 * @version functionVersion 1.1 since appVersion v0.54.1
	 */
    public static function encode_wtype($array,$returnUTF8=true){
        if ($returnUTF8){
            $json_string = utf8_encode(json_encode($array, JSON_NUMERIC_CHECK | JSON_FORCE_OBJECT)); // includes data type
        } else {
            $json_string = json_encode($array, JSON_NUMERIC_CHECK | JSON_FORCE_OBJECT);
        }
        return $json_string;
    }

	/**
	 *
	 * @param type $array
	 * @return string utf-8 | string-thats-not-utf8
	 * @example bjson::decode_wtype() description
	 * @example bjson::encode_wtype() description
	 * @version 1.00
	 * @todo If no issues by v0.57 then remove the depreciated method below
	 * @deprecated since version 0.54.2
	 */
    public static function encode_wtype_v0($array){
        $use_utf8_yn = "Y";
        if ($use_utf8_yn == "Y"){
            $json_string = utf8_encode(json_encode($array, JSON_NUMERIC_CHECK | JSON_FORCE_OBJECT)); // includes data type
        } else {
            $json_string = json_encode($array, JSON_NUMERIC_CHECK | JSON_FORCE_OBJECT);
        }
        return $json_string;
    }

	/**
	 *
	 * @param type $array
	 * @return type
	 */
    public static function json_encode_wtype_v0($array){
        // Doesn't have UTF8 encoding
        $json_string = json_encode($array, JSON_NUMERIC_CHECK | JSON_FORCE_OBJECT);
        return $json_string;
    }

	/**
	 *
	 * @param type $json_string
	 * @return type
	 */
    public static function decode_wtype($json_string){
        $json_array = json_decode($json_string, $assoc=true);
        return $json_array;
    }

	/**
	 *
	 * @return type
	 */
    public static function get_stream(){
        // use this at the top of the file being called with a a JSON POST text stream.
        $json_string = file_get_contents("php://input"); //read the HTTP body.$json_string = file_get_contents("php://input"); //read the HTTP body.
        return $json_string;
    }

	/**
	 *
	 * @param type $target_url
	 * @param type $data_array
	 * @return type
	 */
    public static function send_via_post($target_url, $data_array){
        //$target_url = The URL we want to send data to

        //$content = json_encode($data_array); // will format all as a string
        $content     = utf8_encode(json_encode($data_array, JSON_NUMERIC_CHECK | JSON_FORCE_OBJECT)); // includes data type
        $content_len = strlen($content);

        $curl = curl_init($target_url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
                array("Content-type: application/json", "Accept: application/json", "Content-Length: $content_len"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);

        //echo_array($json_response);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status == 200 ) {
            //echo "return status = 200 <br>";
        }
        if ( $status != 201 ) {
            $err_msg = "Error: call to URL $target_url failed with status $status, response $json_response, curl_error "
                     . curl_error($curl) . ", curl_errno "
                     . "<b>" . curl_errno($curl) . "</b>";
            //die($err_msg);
        }

        curl_close($curl);
        $response = json_decode($json_response, true);
        return $response;
    }

	/**
	 *
	 * @param type $target_url
	 * @param type $json_string
	 * @return type
	 */
    public static function send_string_via_post($target_url, $json_string){
        //$target_url = The URL you want to send data to

        //$content = json_encode($data_array); // will format all as a string
        //$content     = utf8_encode(json_encode($data_array, JSON_NUMERIC_CHECK | JSON_FORCE_OBJECT)); // includes data type
        $content_len = strlen($json_string);

        $curl = curl_init($target_url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
                array("Content-type: application/json", "Accept: application/json", "Content-Length: $content_len"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json_string);

        $json_response = curl_exec($curl);

        //echo_array($json_response);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status == 200 ) {
            //echo "return status = 200 <br>";
        }
        if ( $status != 201 ) {
            $err_msg = "Error: call to URL $target_url failed with status $status, response $json_response, curl_error "
                     . curl_error($curl) . ", curl_errno "
                     . "<b>" . curl_errno($curl) . "</b>";
            //die($err_msg);
        }

        curl_close($curl);
        return $json_response;
    }

	/**
	 * Converts a standard array into a JSON object stream and terminates Yii App
	 * @internal Used for AJAX calls that return JSON objects to TbEditableField booster.widgets
	 * @internal Sets Apache header Content-Type
	 * @param array | array[] $data_array
	 * @return object Content-Type: application/json
	 * @version 2.0.0
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, dana@byrdbrain.com
	 * @internal version 1.0.0 name = returnFormalJson($data_array)
	 */
	public static function convertArrayToJsonObjectStreamAndEndYiiApp($data_array, $encodeType=false) {
		header('Content-Type: application/json; charset="UTF-8"');
		// to return text/html rather than json
		// header('Content-type: text/html; charset=utf-8');

		// echo json_encode($data_array);
		if ($encodeType){
			$json = self::encode_wtype($data_array);
		} else {
			$json = json_encode($data_array);
		}
		echo $json;
		Yii::app()->end();
	}

	/**
	 *
	 * @internal This is typically called to format the result of a NON-Submit post
	 *    array containing a json string that needs model fields handled properly
	 */
	public static function convertFormDataJsonString2YiiModelDataSubmitArray($postArray=null){
		//if (isset($_POST['data']) && !isset($_POST['AthleteTeams'])){
		if(! is_null($postArray) && isset($postArray['data']) ){
			$jsonIputRaw = $_POST['data'];
			$jsonDecoded = CJSON::decode($jsonIputRaw);
			//$formDataYii= CJavaScript::jsonDecode($_POST['data']);  // returns good json, but not a yii modelData array structure

		} elseif(is_null($postArray) && isset($_POST['data']) ){
			$jsonIputRaw = $_POST['data'];
			$jsonDecoded = CJSON::decode($jsonIputRaw);
			//$formDataYii= CJavaScript::jsonDecode($_POST['data']);  // returns good json, but not a yii modelData array structure
			$post=[]; // An Array Built up from a serialized data string as a Native Yii Data Array Format
		} else {
			return [];
		}

		if (!is_array($jsonDecoded)){
			throw new CException("json decoding failed",707);
		}

		$post=[]; // An Array Built up from a serialized data string as a Native Yii Data Array Format
		foreach($jsonDecoded as $jsonIdx=>$row){
			// Get name
			if (isset($row['name'])){
				$fieldName  =$row['name'];
			} else {
				$fieldName = '';
				$errorMsg  = 'invalid name use case'; // @todo add exception handling
			}
			// Get value
			if (isset($row['name'])){
				$fieldValue =$row['value'];
			} else {
				$fieldValue ='';
				$errorMsg  = 'invalid value use case'; // @todo add exception handling
			}

			if (stripos($fieldName, '[') !== false){
				$modelFromFieldName = explode('[',$fieldName);
				if (isset($modelFromFieldName[0])){
					$modelName = $modelFromFieldName[0];
					$attribute = substr($modelFromFieldName[1], 0, -1);
					$post[$modelName][$attribute] = $fieldValue;
				}
			}
			else
			{	// The html control data was not bound to a yii model->attribute
				if (! empty($fieldName)){
					$post[$fieldName] = $fieldValue;
				}
			}
		}
		unset($jsonIdx);
		return $post;
	}

	public static function is_JSON($string){
		return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	}

	/**
	 * Validates JSON input
	 * @param $string
	 * @return bool
	 * @author Andreas Glaser
	 */
	public static function isValidJsonString($string)
	{
		if (is_int($string) || is_float($string)) {
			return true;
		}

		json_decode($string);

		return json_last_error() === JSON_ERROR_NONE;
	}

	/**
	 * Validates JSON input and hides any error output via '@'
	 * @param string $str JSON formatted string to validate
	 * @return bool
	 * @author Valli Pandy
	 */
	public static function validate_json($str=NULL) {
		if (is_string($str)) {
			$noReturn = @json_decode($str);
			unset($noReturn);
			return (json_last_error() === JSON_ERROR_NONE);
		}
		return false;
	}


}

// </editor-fold>





