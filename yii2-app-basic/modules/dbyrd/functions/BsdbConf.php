<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class bsdbConf extends CApplicationComponent
{
	public function init() {
	}
    static $confArray;
    public static $enable_remote_www_conn_yn = "N";
    static $enable_conf_access_logging_yn = "Y";
    static $debug_yn = "Y";

	static $dbcnAlias = [
        'sandbox'     => 'sandbox-pdo',
		'staging-dev' => 'gsm-staging-ssh-dev-pdo',
		'staging-prod'=> 'gsm-staging-ssh-prod-pdo',
		'prod-dev'    => 'gsm-staging-ssh-dev-pdo',
		'prod-prod'   => 'gsm-staging-ssh-prod-pdo',
    ];

    //*
    // The following options are available for PDO:
    //
    // string   dsn
    // string   username
    // string   password
    // boolean  persistent
    // string   identifier
    //

    public static function read($db_config_name){
        return $self::get__db_config_array($db_config_name);
    }

    public static function write($db_config_name){
        self::$confArray = $self::get__db_config_array($db_config_name);
    }

    public static function get__db_config_array($db_config_name=null){
        if (empty($db_config_name)){
            if (self::$debug_yn == "Y"){
                echo "\$db_config_name is empty <br>";
            }
            $server_name          = $_SERVER['SERVER_NAME'];
            $server_ip            = $_SERVER["SERVER_ADDR"];
            $server_port          = $_SERVER["SERVER_PORT"];
            $http_host            = $_SERVER["HTTP_HOST"];       // "sandbox:8086"
            $request_method       = $_SERVER["REQUEST_METHOD"];
            if (array_key_exists("REQUEST_TIME_FLOAT", $_SERVER)){
                $request_received_dtn = $_SERVER["REQUEST_TIME_FLOAT"];
            } else {
                $request_received_dtn = mTime_datetime_string();
            }
            if (self::$debug_yn == "Y"){
                echo "\$server_name = $server_name <br>";
            }
            if ($server_name == "www.globalsoccermetrix.com" || $server_name == "globalsoccermetrix.com"){
                switch ($server_port) {
                    case "80":
                        $db_config_name = "www-local-pdo";
                        break;
                    default:
                        $db_config_name = "www-local-pdo";
                        break;
                }

            } elseif ($server_name == "sandbox"){
                if ($server_port == "8096"){ // sandbox
                    $use_remote_yn = self::$enable_remote_www_conn_yn;
                    if ( $use_remote_yn == "Y"){
                        $db_config_name = "www-remote-pdo"; // GSM schema on production
                    }elseif ( $use_remote_yn == "N"){
                        $db_config_name = "www-dev-pdo";    // GSM schema
                    }else{
                        $db_config_name = "sandbox-pdo";    // GSM schema
                    }
                }

            } elseif ($server_name == "127.0.0.1" || $server_name == "localhost" ){
                if ($server_port == "80"){ // production mirror
                    $use_remote_yn = self::$enable_remote_www_conn_yn;
                    if ( $use_remote_yn == "Y"){
                        $db_config_name = "www-remote-pdo"; // GSM schema on production
                    }elseif ( $use_remote_yn == "N"){
                        $db_config_name = "www-dev-pdo";    // GSM schema
                    }else{
                        $db_config_name = "sandbox-pdo";    // GSM schema
                    }
                } else {
                    $db_config_name = "sandbox-pdo";    // GSM schema
                }
            } else {
                $db_config_name = "www-local-pdo";
            }

        }

        // <editor-fold defaultstate="collapsed" desc="db config arrays">
		/**
		 * The following options are available for PDO:
		 *
		 * string   dsn
		 * string   username
		 * string   password
		 * boolean  persistent
		 * string   identifier
		 */
		$gsmDbAuth=[
			'gsm_web_admin'=>[
				'password'=>'e76aX2urGT%Yu2^rrC',
				],
			'gsm_web_user'=>[
				'password'=>'FJ#wwEc4PsCuMbtv8ySzKG@Da',
				],
			];

        // "comments"  => "ubuntu 14.04"
        $db_config['sandbox-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                             'dsn'        => 'mysql:host=localhost;dbname=gsm_dev',
                            'username'   => 'gsm_web_admin',
                            'password'   => $gsmDbAuth['gsm_web_admin']['password'],
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );

		// connection to staging server gsm_dev of over SSH
        $db_config['gsm-staging-ssh-dev-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                            'dsn'        => 'mysql:host=localhost;port=4306;dbname=gsm_dev',
                            'username'   => 'gsm_web_admin',
                            //'password'   => 'FJ#wwEc4PsCuMbtv8ySzKG@Da',
							'password'	 =>$gsmDbAuth['gsm_web_admin']['password'],
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );
		// connection to staging server gsm_prod of over SSH
        $db_config['gsm-staging-ssh-prod-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                            'dsn'        => 'mysql:host=localhost;port=4306;dbname=gsm_prod',
                            'username'   => 'gsm_web_admin',
                            //'password'   => 'FJ#wwEc4PsCuMbtv8ySzKG@Da',
							'password'	 =>$gsmDbAuth['gsm_web_admin']['password'],
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );
		// connection to production server gsm_dev of over SSH
        $db_config['gsm-staging-ssh-dev-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                            'dsn'        => 'mysql:host=localhost;port=4307;dbname=gsm_dev',
                            'username'   => 'gsm_web_admin',
                            //'password'   => 'FJ#wwEc4PsCuMbtv8ySzKG@Da',
							'password'	 =>$gsmDbAuth['gsm_web_admin']['password'],
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );
		// connection to production server gsm_prod of over SSH
        $db_config['gsm-staging-ssh-prod-pdo'] = array(
                    'type'       => 'pdo',
                    'connection' => array(
                            'dsn'        => 'mysql:host=localhost;port=4307;dbname=gsm_prod',
                            'username'   => 'gsm_web_admin',
                            //'password'   => 'FJ#wwEc4PsCuMbtv8ySzKG@Da',
							'password'	 =>$gsmDbAuth['gsm_web_admin']['password'],
                            'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset'      => 'utf8',
                    'caching'      => FALSE,
                    'profiling'    => TRUE,
            );
        // </editor-fold>

        if (self::$enable_conf_access_logging_yn == "Y"){
            // @todo: log the conf access details
            if (self::$debug_yn == "Y"){
                echo "\$db_config_name = $db_config_name <br>";
            }
        };

        if (array_key_exists($db_config_name, $db_config)){
            //return $db_config[$db_config_name];
            $db_config_arr_to_return = $db_config[$db_config_name];
            return $db_config_arr_to_return;
        }


        // Clean up the configuration vars
        // unset($login_credentials);
    }

}
