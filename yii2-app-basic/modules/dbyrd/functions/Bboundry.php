<?php

/*
 *
 */

class bboundry extends CApplicationComponent
{
	public function init() {
	}
//class boundary {

    public static function firewall() {
        if (Bfirewall::client_ip_address_is_allowed() == false){
            self::force__redirect();
        }
    }

    public static function force__redirect(){
        if( $_SERVER['HTTPS'] != "on" ) {
           // Send the caller to port 80
           $new_url = "http://www.globalsoccermetrix.com";
           header("Location: $new_url");
           exit;

        } else {
            // Send the caller to port 443
           $new_url = "https://www.globalsoccermetrix.com";
           header("Location: $new_url");
           exit;
        }
    }
}

