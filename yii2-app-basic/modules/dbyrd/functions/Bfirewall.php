<?php

//defined('BCSYSPATH') or die('No direct script access.' . " to " . __FILE__);
/**
 * @package EPF (Enterprise Process Framework)
 * @author Dana Byrd <dana@byrdbrain.com>
 */
// * Project.........:
// * Purpose.........: Allow only a select list of ip_addresses to access a web page.
// * CEC.............: asp-2.1.1
// * Initial Release.: v0.1.0
// * Author..........: Dana Byrd
// * History.........:
//     2013-08-13 0.0.0.1 Combine firewall and boundary classes into one file.
// * Notes......:
//    Common usages:
//       firewall::client_ip_address_is_allowed();
//       boundary::firewall();
//    External references in this source code:
//       None!
class bfirewall extends CApplicationComponent
{
	public function init() {
	}


//class firewall{
    //firewall::client_ip_address_is_allowed();
    static $is_allowed_yn;
    static $is_allowed_bool;
    static $ip_address_list__allowed_to_operate_this_program =
        array(
            //"www.globalsoccermetrix.com"       =>"50.63.202.56"  // refuse access on the production public site
            "git.testxxlrater.info"            =>"162.243.71.111"
            ,"testxxlrater.info"               =>"198.199.83.91"
            ,"aus.dev.globalsoccermetrix.com"  =>"66.68.138.18"
            ,"local.development"               =>"127.0.0.1"
            );

    public static function client_ip_address_is_allowed(){
        $is_allowed_bool    = false;
        $callers_ip_address = self::get_the_ip_address_calling_the_server();
        $ip_address_list__allowed_to_operate_this_program = self::$ip_address_list__allowed_to_operate_this_program;
        if (in_array($callers_ip_address, $ip_address_list__allowed_to_operate_this_program, false) == false){
            $is_allowed_bool = false;
        } else {
            $is_allowed_bool = true;
        }
        return $is_allowed_bool;
    }

    public static function client_ip_address_is_allowed_yn(){
        $is_allowed_yn   = "N";
        $is_allowed_bool = false;
        $client_ip_address = self::$get_the_ip_address_calling_the_www_server();
        $ip_address_list__allowed_to_operate_this_program = $self::$ip_address_list__allowed_to_operate_this_program;
        if (in_array($client_ip_address, $ip_address_list__allowed_to_operate_this_program, false) == false){
            $is_allowed_yn   = "N";
            $is_allowed_bool = false;
            //force__redirect();
        } else {
            $is_allowed_yn   = "Y";
            $is_allowed_bool = true;
        }
        return $is_allowed_yn;
    }

    public static function get_the_ip_address_calling_the_server() {
        // Development Status = Golden! This works very well!
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED'
            , 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED'
            , 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }

    public static function get_allowed_client_location_name(){
        // Determine the callers host name via an ip address lookup sans DNS to prevent DNS spoofing
        $client_ip_addr = self::get_the_ip_address_calling_the_server();

        foreach (self::$ip_address_list__allowed_to_operate_this_program as $location_name => $ip_address) {
            if ($ip_address === $client_ip_addr){
                return $location_name;
            }
        }
        // if a valid address found the location name would have been returned by now.
        return "unknown location";
    }
}


?>