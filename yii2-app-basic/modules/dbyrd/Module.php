<?php

namespace app\modules\dbyrd;

class Module extends \yii\base\Module 
{ 
    public $controllerNamespace = 'app\modules\dbyrd\controllers'; 
    public function init() 
    { 
        parent::init(); 
        $this->setAliases([ 
            '@dbyrd-assets' => __DIR__ . '/assets' 
        ]); 
    } 
} 

