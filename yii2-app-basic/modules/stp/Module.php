<?php

namespace app\modules\stp;

class Module extends \yii\base\Module 
{ 
    public $controllerNamespace = 'app\modules\stp\controllers'; 
    public function init() 
    { 
        parent::init(); 
        $this->setAliases([ 
            '@stp-assets' => __DIR__ . '/assets' 
        ]); 
    } 
} 

