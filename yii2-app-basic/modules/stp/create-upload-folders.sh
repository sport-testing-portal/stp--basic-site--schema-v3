#! /bin/bash

# cd project-root
# create folders
sudo mkdir -p ./epf_inbox
sudo mkdir -p ./epf_inbox/data_sources__archive
sudo mkdir -p ./epf_inbox/data_sources__header_templates
sudo mkdir -p ./epf_inbox/data_sources__purged
sudo mkdir -p ./epf_inbox/data_sources__to_process
sudo mkdir -p ./epf_inbox/uploads

cd ./epf_inbox
# a Linux folder permission of 6775 allows all new folders to inherit the parent 
# owner and group no matter what user or group creates a new sub folder. This is 
# an ideal permission for folders that will be created by code
find -not \( -path "*/.git" -prune \) -type d -exec sudo chmod --changes 6775 {} \;
find -not \( -path "*/.git" -prune \) -type d -exec sudo chown --changes $USER:www-data {} \;

