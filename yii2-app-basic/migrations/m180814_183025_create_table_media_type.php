<?php

use yii\db\Migration;

class m180814_183025_create_table_media_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%media_type}}', [
            'media_type_id' => $this->primaryKey(),
            'media_type' => $this->string()->notNull()->comment('media_type_name'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('media_type__media_type_id', '{{%media_type}}', 'media_type_id', true);
    }

    public function down()
    {
        $this->dropTable('{{%media_type}}');
    }
}
