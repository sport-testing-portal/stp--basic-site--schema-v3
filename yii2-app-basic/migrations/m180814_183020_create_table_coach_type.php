<?php

use yii\db\Migration;

class m180814_183020_create_table_coach_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%coach_type}}', [
            'coach_type_id' => $this->primaryKey(),
            'coach_type' => $this->string()->comment('coach_type_name'),
            'display_order' => $this->integer(),
            'coach_type_desc_short' => $this->string(),
            'coach_type_desc_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('coach_type__coach_type_name', '{{%coach_type}}', 'coach_type', true);
    }

    public function down()
    {
        $this->dropTable('{{%coach_type}}');
    }
}
