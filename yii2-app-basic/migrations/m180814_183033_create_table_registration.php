<?php

use yii\db\Migration;

class m180814_183033_create_table_registration extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%registration}}', [
            'registration_id' => $this->primaryKey(),
            'person_id' => $this->integer()->notNull(),
            'org_id' => $this->integer(),
            'app_user_id' => $this->integer(),
            'person_type_id' => $this->integer(),
            'user_id' => $this->integer(),
            'person_name_prefix' => $this->string(),
            'person_name_first' => $this->string(),
            'person_name_middle' => $this->string(),
            'person_name_last' => $this->string(),
            'person_name_suffix' => $this->string(),
            'person_name_full' => $this->string(),
            'person_phone_personal' => $this->string(),
            'person_email_personal' => $this->string(),
            'person_phone_work' => $this->string(),
            'person_email_work' => $this->string(),
            'person_position_work' => $this->string()->comment('Owner, Director, Administrative Assistant, Other'),
            'gender_id' => $this->integer(),
            'person_image_headshot_url' => $this->string()->comment('URL to a headshot photo'),
            'person_name_nickname' => $this->string(),
            'person_date_of_birth' => $this->dateTime(),
            'person_height' => $this->string(),
            'person_weight' => $this->integer(),
            'person_tshirt_size' => $this->string(),
            'person_addr_1' => $this->string(),
            'person_addr_2' => $this->string(),
            'person_addr_3' => $this->string()->comment('Required for some institutional and international addresses'),
            'person_city' => $this->string(),
            'person_postal_code' => $this->string(),
            'person_country' => $this->string(),
            'person_country_code' => $this->string(),
            'person_state_or_region' => $this->string(),
            'player_id' => $this->integer()->notNull(),
            'player_access_code' => $this->string(),
            'player_waiver_minor_dt' => $this->dateTime(),
            'player_waiver_adult_dt' => $this->dateTime(),
            'player_parent_email' => $this->string(),
            'player_sport_preference' => $this->string(),
            'player_sport_position_preference' => $this->string(),
            'player_shot_side_preference' => $this->string(),
            'coach_id' => $this->integer()->notNull(),
            'coach_type_id' => $this->integer(),
            'coach_specialty' => $this->string(),
            'coach_certifications' => $this->string(),
            'coach_comments' => $this->string(),
            'coach_qrcode_uri' => $this->string()->comment('internal URL to the coach QR code'),
            'coach_info_source_scrape_url' => $this->string()->comment('coach info programmatic source data update url'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%registration}}');
    }
}
