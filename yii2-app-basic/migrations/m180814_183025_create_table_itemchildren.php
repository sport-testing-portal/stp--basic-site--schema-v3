<?php

use yii\db\Migration;

class m180814_183025_create_table_itemchildren extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%itemchildren}}', [
            'parent' => $this->string()->notNull(),
            'child' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%itemchildren}}', ['parent', 'child']);
        $this->createIndex('child', '{{%itemchildren}}', 'child');
        $this->addForeignKey('itemchildren_ibfk_2', '{{%itemchildren}}', 'child', '{{%items}}', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('itemchildren_ibfk_1', '{{%itemchildren}}', 'parent', '{{%items}}', 'name', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%itemchildren}}');
    }
}
