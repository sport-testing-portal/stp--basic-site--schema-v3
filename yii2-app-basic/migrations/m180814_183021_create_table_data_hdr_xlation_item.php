<?php

use yii\db\Migration;

class m180814_183021_create_table_data_hdr_xlation_item extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%data_hdr_xlation_item}}', [
            'data_hdr_xlation_item_id' => $this->primaryKey(),
            'data_hdr_xlation_id' => $this->integer(),
            'data_hdr_xlation_item_source_value' => $this->string()->comment('The column header of the 3rd party'),
            'data_hdr_xlation_item_target_value' => $this->string()->comment('The xxlrater column header of the 3rd party'),
            'ordinal_position_num' => $this->integer(),
            'data_hdr_xlation_item_target_table' => $this->string(),
            'data_hdr_source_value_type_name_source' => $this->string()->comment('table name containing the type_name value and row id'),
            'data_hdr_source_value_is_type_name' => $this->smallInteger()->comment('Source side column value links to a target side integer foreign key via a "type name" value. '),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_data_hdr_xlation_item__data_hdr_xlation_idx', '{{%data_hdr_xlation_item}}', 'data_hdr_xlation_id');
        $this->addForeignKey('fk_data_hdr_xlation_item__data_hdr_xlation', '{{%data_hdr_xlation_item}}', 'data_hdr_xlation_id', '{{%data_hdr_xlation}}', 'data_hdr_xlation_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%data_hdr_xlation_item}}');
    }
}
