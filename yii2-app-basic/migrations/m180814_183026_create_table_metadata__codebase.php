<?php

use yii\db\Migration;

class m180814_183026_create_table_metadata__codebase extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%metadata__codebase}}', [
            'codebase_id' => $this->primaryKey(),
            'file_name' => $this->string()->notNull(),
            'file_path_uri' => $this->string()->comment('A relative path. Example application/models/metaDataCodebase.php would be application/models'),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%metadata__codebase}}');
    }
}
