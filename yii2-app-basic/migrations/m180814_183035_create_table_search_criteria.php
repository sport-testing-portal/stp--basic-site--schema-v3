<?php

use yii\db\Migration;

class m180814_183035_create_table_search_criteria extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%search_criteria}}', [
            'search_criteria_id' => $this->primaryKey(),
            'table_name' => $this->string()->notNull(),
            'field_name' => $this->string()->notNull(),
            'field_is_indexed_yn' => $this->char(),
            'comments' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%search_criteria}}');
    }
}
