<?php

use yii\db\Migration;

class m180814_183018_create_table_camp_session extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%camp_session}}', [
            'camp_session_id' => $this->primaryKey(),
            'camp_id' => $this->integer(),
            'sport_id' => $this->integer(),
            'gender_id' => $this->integer(),
            'address_id' => $this->integer(),
            'season_id' => $this->integer(),
            'camp_session_location' => $this->string()->comment('A generic name like Bull Park not an address'),
            'camp_session_url' => $this->string(),
            'camp_session_ages' => $this->string(),
            'camp_session_skill_level' => $this->string()->comment('Elite, Advanced, Intermediate, Recreational'),
            'camp_session_type' => $this->string()->comment('Residential/Overnight, Commuter, Day'),
            'camp_session_begin_dt' => $this->dateTime(),
            'camp_session_end_dt' => $this->dateTime(),
            'camp_session_specialty' => $this->string()->comment('Recruiting/College ID, Attack, Goalkeepers,Team, Performance'),
            'camp_session_scholarships_available_yn' => $this->char(),
            'camp_session_description' => $this->string(),
            'camp_session_cost_regular_residential' => $this->integer(),
            'camp_session_cost_regular_commuter' => $this->integer(),
            'camp_session_cost_regular_day' => $this->integer(),
            'camp_session_cost_early_residential' => $this->integer(),
            'camp_session_cost_early_commuter' => $this->integer(),
            'camp_session_cost_early_day' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_camp_session__season_idx', '{{%camp_session}}', 'season_id');
        $this->createIndex('fk_camp_session__address_idx', '{{%camp_session}}', 'address_id');
        $this->createIndex('fk_camp_session__gender_idx', '{{%camp_session}}', 'gender_id');
        $this->createIndex('fk_camp_session__sport_idx', '{{%camp_session}}', 'sport_id');
        $this->addForeignKey('fk_camp_session__gender', '{{%camp_session}}', 'gender_id', '{{%gender}}', 'gender_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_session__season', '{{%camp_session}}', 'season_id', '{{%season}}', 'season_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_session__sport', '{{%camp_session}}', 'sport_id', '{{%sport}}', 'sport_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_session__address', '{{%camp_session}}', 'address_id', '{{%address}}', 'address_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_session__camp_id', '{{%camp_session}}', 'camp_id', '{{%camp}}', 'camp_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%camp_session}}');
    }
}
