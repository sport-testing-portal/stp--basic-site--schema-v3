<?php

use yii\db\Migration;

class m180814_183039_create_table_test_eval_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test_eval_category}}', [
            'test_eval_category_id' => $this->primaryKey(),
            'test_eval_category' => $this->string()->notNull()->comment('test_eval_category_name'),
            'test_eval_category_display_order' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('test_eval_category__test_eval_category_display_order', '{{%test_eval_category}}', 'test_eval_category_display_order');
        $this->createIndex('test_eval_category__test_eval_category_name', '{{%test_eval_category}}', 'test_eval_category', true);
    }

    public function down()
    {
        $this->dropTable('{{%test_eval_category}}');
    }
}
