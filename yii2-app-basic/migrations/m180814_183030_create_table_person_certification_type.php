<?php

use yii\db\Migration;

class m180814_183030_create_table_person_certification_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%person_certification_type}}', [
            'person_certification_type_id' => $this->primaryKey(),
            'person_type_id' => $this->integer()->comment('1=player, 2=coach'),
            'person_certification_type' => $this->string()->notNull()->comment('person_certification_type_name'),
            'person_certification_subtype_name' => $this->string(),
            'person_certification_type_display_order' => $this->integer(),
            'person_certification_type_description_short' => $this->string(),
            'person_certification_type_description_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%person_certification_type}}');
    }
}
