<?php

use yii\db\Migration;

class m180814_183045_create_table_vwTeamStatTest extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwTeamStatTest}}', [
            'team_name' => $this->string()->notNull()->comment('team_name'),
            'team_age_group' => $this->string()->comment('use this for a delimited string of age_group_names eg multiple age_groups per team'),
            'team_division' => $this->string()->comment('The division the team plays in'),
            'gender_code' => $this->string(),
            'gender_desc' => $this->string()->comment('gender_desc'),
            'coach_name' => $this->string()->comment('person_name_full'),
            'player_name' => $this->string()->comment('person_name_full'),
            'team_id' => $this->integer()->notNull()->defaultValue('0'),
            'coach_id' => $this->integer()->defaultValue('0'),
            'player_id' => $this->integer()->defaultValue('0'),
            'coach__person_id' => $this->integer()->defaultValue('0'),
            'player__person_id' => $this->integer()->defaultValue('0'),
            'test_eval_summary_log_id' => $this->integer()->notNull()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwTeamStatTest}}');
    }
}
