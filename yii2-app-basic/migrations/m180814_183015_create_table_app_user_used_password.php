<?php

use yii\db\Migration;

class m180814_183015_create_table_app_user_used_password extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%app_user_used_password}}', [
            'app_user_used_password_id' => $this->primaryKey(),
            'app_user_id' => $this->integer()->notNull(),
            'password' => $this->string()->notNull(),
            'set_on' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('app_user_used_password_app_user_id_idx', '{{%app_user_used_password}}', 'app_user_id');
        $this->addForeignKey('fk_app_user_used_password__app_user', '{{%app_user_used_password}}', 'app_user_id', '{{%app_user}}', 'app_user_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%app_user_used_password}}');
    }
}
