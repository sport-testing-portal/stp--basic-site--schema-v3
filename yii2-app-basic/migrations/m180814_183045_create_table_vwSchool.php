<?php

use yii\db\Migration;

class m180814_183045_create_table_vwSchool extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwSchool}}', [
            'org_type' => $this->string()->notNull()->comment('org_type_name = school, camp, club, vendor, venue, media, '),
            'school_name' => $this->string()->notNull()->comment('org_name'),
            'school_addr1' => $this->string(),
            'school_addr2' => $this->string(),
            'school_city' => $this->string(),
            'school_state' => $this->string(),
            'school_postal_code' => $this->string(),
            'school_website' => $this->string(),
            'school_unit_id' => $this->integer(),
            'conference_id' => $this->integer(),
            'updated_at' => $this->timestamp(),
            'org_id' => $this->integer()->notNull()->defaultValue('0'),
            'org_type_id' => $this->integer(),
            'school_id' => $this->integer()->notNull()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwSchool}}');
    }
}
