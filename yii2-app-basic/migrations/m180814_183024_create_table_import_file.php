<?php

use yii\db\Migration;

class m180814_183024_create_table_import_file extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%import_file}}', [
            'import_file_id' => $this->primaryKey(),
            'import_file_source_file_name' => $this->string(),
            'import_file_source_file_date' => $this->dateTime(),
            'import_file_source_file_size' => $this->integer(),
            'import_file_source_file_sha256' => $this->string(),
            'import_file_source_file_password' => $this->string(),
            'import_file_target_file_name' => $this->string(),
            'import_file_target_file_date' => $this->dateTime(),
            'import_file_target_file_size' => $this->integer(),
            'import_file_target_file_sha256' => $this->string(),
            'import_file_target_file_password' => $this->string(),
            'import_file_description_short' => $this->string(),
            'import_file_description_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%import_file}}');
    }
}
