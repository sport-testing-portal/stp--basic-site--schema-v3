<?php

use yii\db\Migration;

class m180814_183028_create_table_org_governing_body extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%org_governing_body}}', [
            'org_governing_body_id' => $this->primaryKey(),
            'org_id' => $this->integer()->notNull(),
            'governing_body_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_org_governing_body__governing_body_idx', '{{%org_governing_body}}', 'governing_body_id');
        $this->createIndex('idx_org_governing_body__org', '{{%org_governing_body}}', 'org_id');
        $this->addForeignKey('fk_org_governing_body__governing_body', '{{%org_governing_body}}', 'governing_body_id', '{{%governing_body}}', 'governing_body_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_org_governing_body__org', '{{%org_governing_body}}', 'org_id', '{{%org}}', 'org_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%org_governing_body}}');
    }
}
