<?php

use yii\db\Migration;

class m180814_183045_create_table_vwPlayer_verbose extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwPlayer_verbose}}', [
            'person_name' => $this->string()->comment('person_name_full'),
            'src_table0' => $this->string()->notNull()->defaultValue(''),
            'user_id' => $this->integer()->defaultValue('0'),
            'logon_email' => $this->string(),
            'last_visit_on' => $this->timestamp(),
            'src_table1' => $this->string()->notNull()->defaultValue(''),
            'player_id' => $this->integer()->notNull()->defaultValue('0'),
            'person_id' => $this->integer(),
            'player_access_code' => $this->string(),
            'player_waiver_minor_dt' => $this->dateTime(),
            'player_waiver_adult_dt' => $this->dateTime(),
            'player_parent_email' => $this->string(),
            'player_sport_preference' => $this->string(),
            'player_sport_position_preference' => $this->string(),
            'player_shot_side_preference' => $this->string(),
            'src_table2' => $this->string()->notNull()->defaultValue(''),
            'org_id' => $this->integer(),
            'app_user_id' => $this->integer(),
            'person_name_prefix' => $this->string(),
            'person_name_first' => $this->string(),
            'person_name_middle' => $this->string(),
            'person_name_last' => $this->string(),
            'person_name_suffix' => $this->string(),
            'person_name_full' => $this->string()->comment('person_name_full'),
            'person_phone_personal' => $this->string(),
            'person_email_personal' => $this->string(),
            'person_phone_work' => $this->string(),
            'person_email_work' => $this->string(),
            'person_position_work' => $this->string()->comment('Owner, Director, Administrative Assistant, Other'),
            'person_image_headshot_url' => $this->string()->comment('URL to a headshot photo'),
            'person_name_nickname' => $this->string(),
            'person_date_of_birth' => $this->dateTime(),
            'person_dob_eng' => $this->string(),
            'person_bday_short' => $this->string(),
            'person_bday_long' => $this->string(),
            'person_age' => $this->double(),
            'person_height' => $this->string()->comment('height is stored in inches'),
            'person_height_imperial' => $this->string(),
            'person_weight' => $this->integer()->comment('weight is stored in pounds'),
            'person_tshirt_size' => $this->string(),
            'person_addr_1' => $this->string(),
            'person_addr_2' => $this->string(),
            'person_addr_3' => $this->string()->comment('Required for some institutional and international addresses'),
            'person_city' => $this->string(),
            'person_postal_code' => $this->string(),
            'person_country' => $this->string(),
            'person_country_code' => $this->char(),
            'person_state_or_region' => $this->string(),
            'src_table3' => $this->string()->notNull()->defaultValue(''),
            'person_type_id' => $this->integer()->notNull()->defaultValue('0'),
            'person_type_name' => $this->string()->notNull()->comment('person_type_name=player, coach, camp contact, school contact,'),
            'person_type_desc_short' => $this->string(),
            'person_type_desc_long' => $this->string(),
            'src_table4' => $this->string()->notNull()->defaultValue(''),
            'gender_desc' => $this->string()->comment('gender_desc'),
            'gender_code' => $this->string(),
            'src_table5' => $this->string()->notNull()->defaultValue(''),
            'team_player_id' => $this->integer()->defaultValue('0'),
            'team_id' => $this->integer(),
            'team_play_begin_dt' => $this->dateTime(),
            'team_play_end_dt' => $this->dateTime(),
            'src_table6' => $this->string()->notNull()->defaultValue(''),
            'team_org_id' => $this->integer(),
            'team_school_id' => $this->integer(),
            'team_sport_id' => $this->integer(),
            'team_camp_id' => $this->integer(),
            'team_gender_id' => $this->integer(),
            'team_name' => $this->string()->comment('team_name'),
            'team_gender' => $this->char(),
            'team_division' => $this->string()->comment('The division the team plays in'),
            'team_age_group' => $this->string()->comment('use this for a delimited string of age_group_names eg multiple age_groups per team'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwPlayer_verbose}}');
    }
}
