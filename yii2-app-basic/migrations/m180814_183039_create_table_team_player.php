<?php

use yii\db\Migration;

class m180814_183039_create_table_team_player extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%team_player}}', [
            'team_player_id' => $this->primaryKey(),
            'team_id' => $this->integer()->notNull(),
            'player_id' => $this->integer()->notNull(),
            'team_play_sport_position_id' => $this->integer(),
            'team_play_sport_position2_id' => $this->integer(),
            'primary_position' => $this->string(),
            'team_play_statistical_highlights' => $this->string(),
            'coach_id' => $this->integer()->comment('This is not a related field. This a place to store the primary coach id. The related tables will still store the related data for a coach.'),
            'coach_name' => $this->string()->comment('This is not a related field. This a place to store the primary coach. The related tables will still store the related data for a coach.'),
            'team_play_current_team' => $this->tinyInteger(),
            'team_play_begin_dt' => $this->dateTime(),
            'team_play_end_dt' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('team_player__team_id__player_id', '{{%team_player}}', ['team_id', 'player_id'], true);
        $this->createIndex('team_player__end_dt_idx', '{{%team_player}}', 'team_play_end_dt');
        $this->createIndex('team_player_idx_team_play_begin_dt', '{{%team_player}}', 'team_play_begin_dt');
        $this->createIndex('team_player_idx_team_play_end_dt', '{{%team_player}}', 'team_play_end_dt');
        $this->createIndex('fk_team_player__sport_position_idx', '{{%team_player}}', 'team_play_sport_position_id');
        $this->addForeignKey('fk_team_player__player', '{{%team_player}}', 'player_id', '{{%player}}', 'player_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team_player__sport_position', '{{%team_player}}', 'team_play_sport_position_id', '{{%sport_position}}', 'sport_position_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team_player__team', '{{%team_player}}', 'team_id', '{{%team}}', 'team_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%team_player}}');
    }
}
