<?php

use yii\db\Migration;

class m180814_183044_create_table_vwCoach_Team_Assignment_Status_Summary extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwCoach_Team_Assignment_Status_Summary}}', [
            'status_cnt' => $this->bigInteger()->notNull()->defaultValue('0'),
            'coach_team_assignment_status' => $this->string()->notNull()->defaultValue(''),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwCoach_Team_Assignment_Status_Summary}}');
    }
}
