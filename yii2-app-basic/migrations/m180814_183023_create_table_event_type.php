<?php

use yii\db\Migration;

class m180814_183023_create_table_event_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event_type}}', [
            'event_type_id' => $this->primaryKey(),
            'event_type' => $this->string()->notNull()->comment('event_type_name'),
            'event_type_desc_short' => $this->string(),
            'event_type_desc_long' => $this->string(),
            'event_type_tags' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('event_type__event_type_name', '{{%event_type}}', 'event_type', true);
    }

    public function down()
    {
        $this->dropTable('{{%event_type}}');
    }
}
