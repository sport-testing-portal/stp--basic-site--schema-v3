<?php

use yii\db\Migration;

class m180814_183022_create_table_data_model extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%data_model}}', [
            'data_model_id' => $this->primaryKey(),
            'table_name' => $this->string(),
            'table_structure_digest' => $this->string(),
            'data_model_digest' => $this->string(),
            'data_model_digest_created_at' => $this->dateTime(),
            'data_model_created_at' => $this->dateTime(),
            'dupe_detection_field_list' => $this->string(),
            'trigger__bfr_insert_uri' => $this->string(),
            'trigger__bfr_update_uri' => $this->string(),
            'trigger__bfr_template_cec_ver' => $this->string(),
            'data_model_cec_ver' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%data_model}}');
    }
}
