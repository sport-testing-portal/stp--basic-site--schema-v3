<?php

use yii\db\Migration;

class m180814_183025_create_table_media extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%media}}', [
            'media_id' => $this->primaryKey(),
            'media_type_id' => $this->integer()->notNull()->comment('image, video, audio, etc'),
            'org_id' => $this->integer()->notNull(),
            'person_id' => $this->integer()->notNull(),
            'media_name_original' => $this->string()->comment('the name the media was uploaded with. Use this name for display to the player.'),
            'media_name_unique' => $this->string()->comment('sha256 key of content. use this name to store the media on disk'),
            'media_desc_short' => $this->string()->comment('short description for thumbnails and tight spaces'),
            'media_desc_long' => $this->string()->comment('long description '),
            'media_url_local' => $this->string()->comment('url pointing to an XXLrater digital storage location (file share or CDN)'),
            'media_url_remote' => $this->string()->comment('url pointing to a remote 3rd party storage location'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_media__media_type', '{{%media}}', 'media_type_id', '{{%media_type}}', 'media_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_media__org', '{{%media}}', 'org_id', '{{%org}}', 'org_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_media__person', '{{%media}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%media}}');
    }
}
