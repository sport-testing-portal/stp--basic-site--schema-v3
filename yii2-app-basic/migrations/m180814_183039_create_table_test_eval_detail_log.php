<?php

use yii\db\Migration;

class m180814_183039_create_table_test_eval_detail_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test_eval_detail_log}}', [
            'test_eval_detail_log_id' => $this->primaryKey(),
            'test_eval_summary_log_id' => $this->integer(),
            'test_eval_detail_test_order_id' => $this->integer(),
            'test_eval_detail_test_desc_id' => $this->integer(),
            'test_eval_detail_score_text' => $this->string(),
            'test_eval_detail_score_num' => $this->decimal(),
            'test_eval_detail_attempt' => $this->tinyInteger(),
            'test_eval_detail_score_int' => $this->integer(),
            'test_eval_detail_score_url' => $this->string(),
            'test_eval_detail_video_url' => $this->string(),
            'test_eval_detail_rating' => $this->integer(),
            'test_eval_detail_overall_ranking' => $this->integer(),
            'test_eval_detail_positional_ranking' => $this->integer(),
            'test_eval_detail_source_event_id' => $this->integer(),
            'test_eval_detail_source_record_id' => $this->integer(),
            'test_eval_detail_import_file_id' => $this->integer(),
            'test_eval_detail_import_file_line_num' => $this->integer(),
            'test_eval_detail_trial_status' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('test_eval_detail_log__source_event_id_idx', '{{%test_eval_detail_log}}', 'test_eval_detail_source_event_id');
        $this->createIndex('fk_test_eval_detail_log__test_eval_detail_test_desc_idx', '{{%test_eval_detail_log}}', 'test_eval_detail_test_order_id');
        $this->createIndex('fk_test_eval_detail_log__test_eval_detail_test_desc_idx1', '{{%test_eval_detail_log}}', 'test_eval_detail_test_desc_id');
        $this->createIndex('fk_test_eval_detail_log__test_eval_summary_log_idx', '{{%test_eval_detail_log}}', 'test_eval_summary_log_id');
        $this->addForeignKey('fk_test_eval_detail_log__test_eval_detail_test_desc', '{{%test_eval_detail_log}}', 'test_eval_detail_test_desc_id', '{{%test_eval_detail_test_desc}}', 'test_eval_detail_test_desc_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_test_eval_detail_log__test_eval_summary_log', '{{%test_eval_detail_log}}', 'test_eval_summary_log_id', '{{%test_eval_summary_log}}', 'test_eval_summary_log_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%test_eval_detail_log}}');
    }
}
