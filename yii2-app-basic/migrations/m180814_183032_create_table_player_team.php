<?php

use yii\db\Migration;

class m180814_183032_create_table_player_team extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_team}}', [
            'player_team_id' => $this->primaryKey(),
            'player_id' => $this->integer(),
            'team_id' => $this->integer(),
            'begin_dt' => $this->dateTime(),
            'end_dt' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_player_team__player', '{{%player_team}}', 'player_id', '{{%player}}', 'player_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player_team__team', '{{%player_team}}', 'team_id', '{{%team}}', 'team_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%player_team}}');
    }
}
