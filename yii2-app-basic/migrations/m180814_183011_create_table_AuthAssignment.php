<?php

use yii\db\Migration;

class m180814_183011_create_table_AuthAssignment extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%AuthAssignment}}', [
            'itemname' => $this->string()->notNull(),
            'userid' => $this->string()->notNull(),
            'bizrule' => $this->text(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%AuthAssignment}}', ['itemname', 'userid']);
        $this->addForeignKey('AuthAssignment_ibfk_1', '{{%AuthAssignment}}', 'itemname', '{{%AuthItem}}', 'name', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%AuthAssignment}}');
    }
}
