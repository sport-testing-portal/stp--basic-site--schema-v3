<?php

use yii\db\Migration;

class m180814_183037_create_table_subscription extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%subscription}}', [
            'subscription_id' => $this->primaryKey(),
            'subscription_type_id' => $this->integer(),
            'org_id' => $this->integer(),
            'person_id' => $this->integer(),
            'subscription_status_type_id' => $this->integer(),
            'subscription_begin_dt' => $this->dateTime(),
            'subscription_end_dt' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_subscription__person_idx', '{{%subscription}}', 'person_id');
        $this->createIndex('fk_subscription__org_idx', '{{%subscription}}', 'org_id');
        $this->addForeignKey('fk_subscription__subscription_type', '{{%subscription}}', 'subscription_type_id', '{{%subscription_type}}', 'subscription_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_subscription__org', '{{%subscription}}', 'org_id', '{{%org}}', 'org_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_subscription__person', '{{%subscription}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_subscription__sst', '{{%subscription}}', 'subscription_status_type_id', '{{%subscription_status_type}}', 'subscription_status_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%subscription}}');
    }
}
