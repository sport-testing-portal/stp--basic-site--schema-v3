<?php

use yii\db\Migration;

class m180814_183039_create_table_test_eval_detail_test_desc extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test_eval_detail_test_desc}}', [
            'test_eval_detail_test_desc_id' => $this->primaryKey(),
            'test_eval_type_id' => $this->integer(),
            'test_eval_detail_test_desc' => $this->string(),
            'test_eval_detail_test_desc_display_order' => $this->integer(),
            'rating_bias' => $this->string(),
            'units' => $this->string(),
            'format' => $this->string(),
            'splits' => $this->tinyInteger(),
            'checkpoints' => $this->tinyInteger(),
            'attempts' => $this->tinyInteger(),
            'qualifier' => $this->string(),
            'backend_calculation' => $this->string(),
            'metric_equivalent' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('test_eval_detail_test_desc_idx_rating_bias', '{{%test_eval_detail_test_desc}}', 'rating_bias');
        $this->createIndex('test_eval_detail_test_desc_idx_tedtd_display_order', '{{%test_eval_detail_test_desc}}', 'test_eval_detail_test_desc_display_order');
        $this->createIndex('fk_test_eval_detail_test_desc__test_eval_type_idx', '{{%test_eval_detail_test_desc}}', 'test_eval_type_id');
        $this->addForeignKey('fk_test_eval_detail_test_desc__test_eval_type', '{{%test_eval_detail_test_desc}}', 'test_eval_type_id', '{{%test_eval_type}}', 'test_eval_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%test_eval_detail_test_desc}}');
    }
}
