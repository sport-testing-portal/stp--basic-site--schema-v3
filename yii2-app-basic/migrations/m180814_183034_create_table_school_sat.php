<?php

use yii\db\Migration;

class m180814_183034_create_table_school_sat extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%school_sat}}', [
            'school_sat_id' => $this->primaryKey(),
            'school_id' => $this->integer(),
            'school_unit_id' => $this->integer(),
            'school_sat_i_verbal_25_pct' => $this->integer(),
            'school_sat_i_verbal_75_pct' => $this->integer(),
            'school_sat_i_math_25_pct' => $this->integer(),
            'school_sat_i_math_75_pct' => $this->integer(),
            'school_sat_student_submit_cnt' => $this->integer(),
            'school_sat_student_submit_pct' => $this->string(),
            'school_sat_report_period' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('school_sat__school_unit_id', '{{%school_sat}}', 'school_unit_id', true);
        $this->addForeignKey('fk_school_sat__school', '{{%school_sat}}', 'school_id', '{{%school}}', 'school_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%school_sat}}');
    }
}
