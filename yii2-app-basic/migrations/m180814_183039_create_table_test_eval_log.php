<?php

use yii\db\Migration;

class m180814_183039_create_table_test_eval_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test_eval_log}}', [
            'test_eval_log_id' => $this->primaryKey(),
            'test_eval_type_id' => $this->integer(),
            'test_eval_summary_log_id' => $this->integer(),
            'rfid' => $this->integer()->comment('Also called band id in STI terminology'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_test_eval_log__test_eval_type_idx', '{{%test_eval_log}}', 'test_eval_type_id');
        $this->createIndex('fk_test_eval_log__test_eval_summary_log_idx', '{{%test_eval_log}}', 'test_eval_summary_log_id');
        $this->addForeignKey('fk_test_eval_log__test_eval_summary_log', '{{%test_eval_log}}', 'test_eval_summary_log_id', '{{%test_eval_summary_log}}', 'test_eval_summary_log_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_test_eval_log__test_eval_type', '{{%test_eval_log}}', 'test_eval_type_id', '{{%test_eval_type}}', 'test_eval_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%test_eval_log}}');
    }
}
