<?php

use yii\db\Migration;

class m180814_183018_create_table_camp_location extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%camp_location}}', [
            'camp_location_id' => $this->primaryKey(),
            'camp_id' => $this->integer(),
            'address_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_camp_location__camp_idx', '{{%camp_location}}', 'camp_id');
        $this->createIndex('fk_camp_location__address_idx', '{{%camp_location}}', 'address_id');
        $this->addForeignKey('fk_camp_locations__address', '{{%camp_location}}', 'address_id', '{{%address}}', 'address_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_camp_locations__camp', '{{%camp_location}}', 'camp_id', '{{%camp}}', 'camp_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%camp_location}}');
    }
}
