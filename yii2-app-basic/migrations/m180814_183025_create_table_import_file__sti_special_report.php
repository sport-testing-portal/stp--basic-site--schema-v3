<?php

use yii\db\Migration;

class m180814_183025_create_table_import_file__sti_special_report extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%import_file__sti_special_report}}', [
            'import_file__sti_special_report_id' => $this->primaryKey(),
            'import_file_id' => $this->integer(),
            'event_id' => $this->integer(),
            'record_id' => $this->integer(),
            'name_full' => $this->string(),
            'user_id' => $this->string(),
            'drill_name' => $this->string(),
            'attempt' => $this->tinyInteger(),
            'splits_str' => $this->string(),
            'direction' => $this->string(),
            'result' => $this->decimal(),
            'drill_unit' => $this->string(),
            'trial_status' => $this->string(),
            'test_dt' => $this->dateTime(),
            'import_dt' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_import_file__sti_special_report__import_file_idx', '{{%import_file__sti_special_report}}', 'import_file_id');
        $this->addForeignKey('fk_import_file__sti_special_report__import_file', '{{%import_file__sti_special_report}}', 'import_file_id', '{{%import_file}}', 'import_file_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%import_file__sti_special_report}}');
    }
}
