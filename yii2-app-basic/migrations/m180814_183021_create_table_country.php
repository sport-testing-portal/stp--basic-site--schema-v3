<?php

use yii\db\Migration;

class m180814_183021_create_table_country extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%country}}', [
            'country_id' => $this->primaryKey(),
            'iso2' => $this->char(),
            'country' => $this->string()->notNull()->defaultValue('')->comment('short_name'),
            'long_name' => $this->string()->notNull()->defaultValue(''),
            'iso3' => $this->char(),
            'numcode' => $this->string(),
            'un_member' => $this->string(),
            'calling_code' => $this->string(),
            'cctld' => $this->string(),
        ], $tableOptions);

        $this->createIndex('country_idx_long_name', '{{%country}}', 'long_name');
        $this->createIndex('country_idx_short_name', '{{%country}}', 'country');
        $this->createIndex('country_idx_iso3', '{{%country}}', 'iso3');
    }

    public function down()
    {
        $this->dropTable('{{%country}}');
    }
}
