<?php

use yii\db\Migration;

class m180814_183030_create_table_player_academic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_academic}}', [
            'player_academic_id' => $this->primaryKey(),
            'player_id' => $this->integer(),
            'sat_composite_score_max' => $this->integer(),
            'sat_critical_reading_score_max' => $this->integer(),
            'sat_math_score_max' => $this->integer(),
            'sat_writing_score_max' => $this->integer(),
            'sat_scheduled_dt' => $this->dateTime(),
            'psat_composite_score_max' => $this->integer(),
            'psat_critical_reading_score_max' => $this->integer(),
            'psat_math_score_max' => $this->integer(),
            'psat_writing_score_max' => $this->integer(),
            'act_composite_score_max' => $this->integer(),
            'act_math_score_max' => $this->integer(),
            'act_english_score_max' => $this->integer(),
            'act_scheduled_dt' => $this->dateTime(),
            'grade_point_average' => $this->decimal(),
            'class_rank' => $this->integer(),
            'school_size' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_player_academic__player', '{{%player_academic}}', 'player_id', '{{%player}}', 'player_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%player_academic}}');
    }
}
