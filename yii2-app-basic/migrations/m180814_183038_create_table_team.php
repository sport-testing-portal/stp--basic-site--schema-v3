<?php

use yii\db\Migration;

class m180814_183038_create_table_team extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%team}}', [
            'team_id' => $this->primaryKey(),
            'org_id' => $this->integer(),
            'school_id' => $this->integer(),
            'sport_id' => $this->integer(),
            'camp_id' => $this->integer(),
            'gender_id' => $this->integer(),
            'age_group_id' => $this->integer(),
            'team' => $this->string()->notNull()->comment('team_name'),
            'team_gender' => $this->char(),
            'team_division_id' => $this->integer(),
            'team_league_id' => $this->integer(),
            'team_division' => $this->string()->comment('The division the team plays in'),
            'team_league' => $this->string(),
            'organizational_level' => $this->string()->comment('National Team, Professional'),
            'team_governing_body' => $this->string(),
            'team_age_group' => $this->string()->comment('use this for a delimited string of age_group_names eg multiple age_groups per team'),
            'team_website_url' => $this->string()->comment('This is used when org_website_url is used for the club website'),
            'team_schedule_url' => $this->string()->comment('A link to a remote website'),
            'team_schedule_uri' => $this->string()->comment('A link to a file uploaded our site'),
            'team_statistical_highlights' => $this->string()->comment('a free form text field'),
            'team_competition_season_id' => $this->integer()->comment('used for the primary season, eg a single link to the season table
'),
            'team_competition_season' => $this->string()->comment('used for tags, eg multiple seasons'),
            'team_city' => $this->string(),
            'team_state_id' => $this->integer(),
            'team_country_id' => $this->integer(),
            'team_wins' => $this->integer()->comment('wins, losses, draws are often displayed as a coposite.'),
            'team_losses' => $this->integer(),
            'team_draws' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_team__country_idx', '{{%team}}', 'team_country_id');
        $this->createIndex('fk_team__gender_idx', '{{%team}}', 'gender_id');
        $this->createIndex('fk_team__age_group_idx', '{{%team}}', 'age_group_id');
        $this->createIndex('team_idx_team_losses', '{{%team}}', 'team_losses');
        $this->createIndex('fk_team__league_idx', '{{%team}}', 'team_league_id');
        $this->createIndex('fk_team__division_idx', '{{%team}}', 'team_division_id');
        $this->createIndex('fk_team_1_idx', '{{%team}}', 'team_competition_season_id');
        $this->createIndex('fk_team__camp_idx', '{{%team}}', 'camp_id');
        $this->createIndex('fk_team__org_idx', '{{%team}}', 'org_id');
        $this->createIndex('team_idx_team_wins', '{{%team}}', 'team_wins');
        $this->createIndex('team_idx_team_draws', '{{%team}}', 'team_draws');
        $this->addForeignKey('fk_team__school', '{{%team}}', 'school_id', '{{%school}}', 'school_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team__season', '{{%team}}', 'team_competition_season_id', '{{%season}}', 'season_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team__sport', '{{%team}}', 'sport_id', '{{%sport}}', 'sport_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team__age_group', '{{%team}}', 'age_group_id', '{{%age_group}}', 'age_group_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team__camp', '{{%team}}', 'camp_id', '{{%camp}}', 'camp_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team__country', '{{%team}}', 'team_country_id', '{{%country}}', 'country_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team__division', '{{%team}}', 'team_division_id', '{{%team_division}}', 'team_division_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team__gender', '{{%team}}', 'gender_id', '{{%gender}}', 'gender_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team__league', '{{%team}}', 'team_league_id', '{{%team_league}}', 'team_league_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team__org', '{{%team}}', 'org_id', '{{%org}}', 'org_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%team}}');
    }
}
