<?php

use yii\db\Migration;

class m180814_183030_create_table_player_camp_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_camp_log}}', [
            'player_camp_log_id' => $this->primaryKey(),
            'player_id' => $this->integer(),
            'camp_id' => $this->integer()->comment('If the player doesnt know the exact session then use the camp id'),
            'camp_session_id' => $this->integer()->comment('If the player knows the exact session then use it'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_player_camp_log__camp_session_idx', '{{%player_camp_log}}', 'camp_session_id');
        $this->addForeignKey('fk_player_camp_log__camp_session', '{{%player_camp_log}}', 'camp_session_id', '{{%camp_session}}', 'camp_session_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_pcl__camp', '{{%player_camp_log}}', 'camp_id', '{{%camp}}', 'camp_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_pcl__player', '{{%player_camp_log}}', 'player_id', '{{%player}}', 'player_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%player_camp_log}}');
    }
}
