<?php

use yii\db\Schema;

class m180912_190101_metadata_tables extends \yii\db\Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('metadata__codebase', [
            'codebase_id' => $this->primaryKey(),
            'file_name' => $this->string(75)->notNull(),
            'file_path_uri' => $this->string(175),
            'created_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(4)->defaultValue(0),
            ], $tableOptions);
                $this->createTable('metadata__codebase_cec', [
            'codebase_cec_id' => $this->primaryKey(),
            'codebase_cec' => $this->string(75)->notNull(),
            'codebase_cec_desc_short' => $this->string(45),
            'codebase_cec_desc_long' => $this->string(175),
            'cec_status_tag' => $this->string(45),
            'cec_scope' => $this->string(253),
            'rule_file_uri' => $this->string(200),
            'created_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(4)->defaultValue(0),
            ], $tableOptions);
                $this->createTable('metadata__codebase_cec_status', [
            'cec_status_id' => $this->primaryKey(),
            'codebase_id' => $this->integer(11)->notNull(),
            'codebase_cec_id' => $this->integer(11)->notNull(),
            'cec_status' => $this->string(45)->notNull(),
            'cec_status_tag' => $this->string(45),
            'cec_status_bfr' => $this->string(45),
            'cec_status_at' => $this->timestamp(),
            'created_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(4)->defaultValue(0),
            'FOREIGN KEY ([[codebase_id]]) REFERENCES metadata__codebase ([[codebase_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[codebase_cec_id]]) REFERENCES metadata__codebase_cec ([[codebase_cec_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                $this->createTable('metadata__codebase_dev', [
            'codebase_dev_id' => $this->primaryKey(),
            'codebase_dev' => $this->string(75)->notNull(),
            'codebase_dev_desc_short' => $this->string(45),
            'codebase_dev_desc_long' => $this->string(175),
            'dev_status_tag' => $this->string(45),
            'dev_scope' => $this->string(253),
            'rule_file_uri' => $this->string(200),
            'created_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(4)->defaultValue(0),
            ], $tableOptions);
                $this->createTable('metadata__codebase_dev_status', [
            'dev_status_id' => $this->primaryKey(),
            'codebase_id' => $this->integer(11)->notNull(),
            'codebase_dev_id' => $this->integer(11)->notNull(),
            'dev_status' => $this->string(45)->notNull(),
            'dev_status_tag' => $this->string(45),
            'dev_status_bfr' => $this->string(45),
            'dev_status_at' => $this->timestamp(),
            'created_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(4)->defaultValue(0),
            'FOREIGN KEY ([[codebase_id]]) REFERENCES metadata__codebase ([[codebase_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[codebase_dev_id]]) REFERENCES metadata__codebase_dev ([[codebase_dev_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                $this->createTable('metadata__codebase_function', [
            'codebase_function_id' => $this->primaryKey(),
            'codebase_function' => $this->string(45),
            'codebase_function_short_desc' => $this->string(45),
            'codebase_function_long_desc' => $this->string(150),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            ], $tableOptions);
                $this->createTable('metadata__codebase_function_items', [
            'metadata__codebase_function_items_id' => $this->primaryKey(),
            ], $tableOptions);
                $this->createTable('metadata__codebase_mvc_control', [
            'codebase_mvc_control_id' => $this->primaryKey(),
            'codebase_id' => $this->integer(11)->notNull(),
            'controller_actions' => $this->string(45),
            'action_function' => $this->string(45),
            'action_params' => $this->string(45),
            'action_url' => $this->string(45),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            'FOREIGN KEY ([[codebase_id]]) REFERENCES metadata__codebase ([[codebase_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                $this->createTable('metadata__codebase_mvc_model', [
            'codebase_mvc_model_id' => $this->primaryKey(),
            'codebase_id' => $this->integer(11)->notNull(),
            'json_data_structure' => $this->string(45),
            'json_data_sample' => $this->string(45),
            'json_data_example' => $this->string(45),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            'FOREIGN KEY ([[codebase_id]]) REFERENCES metadata__codebase ([[codebase_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                $this->createTable('metadata__codebase_mvc_view', [
            'codebase_mvc_view_id' => $this->primaryKey(),
            'codebase_id' => $this->integer(11)->notNull(),
            'view_function' => $this->string(45),
            'view_file' => $this->string(45),
            'view_params' => $this->string(45),
            'view_url' => $this->string(45),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            'FOREIGN KEY ([[codebase_id]]) REFERENCES metadata__codebase ([[codebase_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                $this->createTable('metadata__database', [
            'database_id' => $this->primaryKey(),
            'table_name' => $this->string(150)->notNull(),
            'schema_name' => $this->string(150)->notNull(),
            'col_name_len_max' => $this->integer(11),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            ], $tableOptions);
                $this->createTable('metadata__database_cec', [
            'database_cec_id' => $this->primaryKey(),
            'database_cec' => $this->string(75)->notNull(),
            'database_cec_desc_short' => $this->string(45),
            'database_cec_desc_long' => $this->string(175),
            'cec_status_tag' => $this->string(45),
            'cec_scope' => $this->string(253),
            'rule_file_uri' => $this->string(200),
            'created_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(4)->defaultValue(0),
            ], $tableOptions);
                $this->createTable('metadata__database_cec_status', [
            'cec_status_id' => $this->primaryKey(),
            'database_id' => $this->integer(11)->notNull(),
            'database_cec_id' => $this->integer(11)->notNull(),
            'cec_status' => $this->string(45)->notNull(),
            'cec_status_tag' => $this->string(45),
            'cec_status_bfr' => $this->string(45),
            'cec_status_at' => $this->timestamp(),
            'created_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(4)->defaultValue(0),
            'FOREIGN KEY ([[database_id]]) REFERENCES metadata__database ([[database_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[database_cec_id]]) REFERENCES metadata__database_cec ([[database_cec_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                $this->createTable('metadata__database_dev', [
            'database_dev_id' => $this->primaryKey(),
            'database_dev' => $this->string(75)->notNull(),
            'database_dev_desc_short' => $this->string(45),
            'database_dev_desc_long' => $this->string(175),
            'dev_status_tag' => $this->string(45),
            'dev_scope' => $this->string(253),
            'rule_file_uri' => $this->string(200),
            'created_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(4)->defaultValue(0),
            ], $tableOptions);
                $this->createTable('metadata__database_dev_status', [
            'dev_status_id' => $this->primaryKey(),
            'database_id' => $this->integer(11)->notNull(),
            'database_dev_id' => $this->integer(11)->notNull(),
            'dev_status' => $this->string(45)->notNull(),
            'dev_status_tag' => $this->string(45),
            'dev_status_bfr' => $this->string(45),
            'dev_status_at' => $this->timestamp(),
            'created_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(4)->defaultValue(0),
            'FOREIGN KEY ([[database_id]]) REFERENCES metadata__database ([[database_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY ([[database_dev_id]]) REFERENCES metadata__database_dev ([[database_dev_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                
    }

    public function safeDown()
    {
        $this->dropTable('metadata__database_dev_status');
        $this->dropTable('metadata__database_dev');
        $this->dropTable('metadata__database_cec_status');
        $this->dropTable('metadata__database_cec');
        $this->dropTable('metadata__database');
        $this->dropTable('metadata__codebase_mvc_view');
        $this->dropTable('metadata__codebase_mvc_model');
        $this->dropTable('metadata__codebase_mvc_control');
        $this->dropTable('metadata__codebase_function_items');
        $this->dropTable('metadata__codebase_function');
        $this->dropTable('metadata__codebase_dev_status');
        $this->dropTable('metadata__codebase_dev');
        $this->dropTable('metadata__codebase_cec_status');
        $this->dropTable('metadata__codebase_cec');
        $this->dropTable('metadata__codebase');
    }
}
