<?php

use yii\db\Migration;

class m180814_183042_create_table_user_used_password extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_used_password}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'password' => $this->string()->notNull(),
            'set_on' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex('user_used_password_user_id_idx', '{{%user_used_password}}', 'user_id');
        $this->addForeignKey('fk_user_used_password__user', '{{%user_used_password}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%user_used_password}}');
    }
}
