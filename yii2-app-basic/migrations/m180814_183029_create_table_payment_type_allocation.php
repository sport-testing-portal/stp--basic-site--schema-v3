<?php

use yii\db\Migration;

class m180814_183029_create_table_payment_type_allocation extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%payment_type_allocation}}', [
            'payment_type_allocation_id' => $this->primaryKey(),
            'payment_type_id' => $this->integer(),
            'payment_type_allocation' => $this->string()->comment('payment_type_allocation'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_payment_type_allocation__payment_type_idx', '{{%payment_type_allocation}}', 'payment_type_id');
        $this->addForeignKey('fk_payment_type_allocation__payment_type', '{{%payment_type_allocation}}', 'payment_type_id', '{{%payment_type}}', 'payment_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%payment_type_allocation}}');
    }
}
