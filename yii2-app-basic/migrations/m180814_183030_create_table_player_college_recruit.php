<?php

use yii\db\Migration;

class m180814_183030_create_table_player_college_recruit extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_college_recruit}}', [
            'player_college_recruit_id' => $this->primaryKey(),
            'player_id' => $this->integer(),
            'school_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_pcr__player', '{{%player_college_recruit}}', 'player_id', '{{%player}}', 'player_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_pcr__school', '{{%player_college_recruit}}', 'school_id', '{{%school}}', 'school_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%player_college_recruit}}');
    }
}
