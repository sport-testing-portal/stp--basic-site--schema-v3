<?php

use yii\db\Migration;

class m180814_183024_create_table_import_file__sti_coach_report extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%import_file__sti_coach_report}}', [
            'import_file__sti_coach_report_id' => $this->primaryKey(),
            'import_file_id' => $this->integer(),
            'field_0001' => $this->string(),
            'field_0002' => $this->string(),
            'field_0003' => $this->string(),
            'field_0004' => $this->string(),
            'field_0005' => $this->string(),
            'field_0006' => $this->string(),
            'field_0007' => $this->string(),
            'field_0008' => $this->string(),
            'field_0009' => $this->string(),
            'field_0010' => $this->string(),
            'field_0011' => $this->string(),
            'field_0012' => $this->string(),
            'field_0013' => $this->string(),
            'field_0014' => $this->string(),
            'field_0015' => $this->string(),
            'field_0016' => $this->string(),
            'field_0017' => $this->string(),
            'field_0018' => $this->string(),
            'field_0019' => $this->string(),
            'field_0020' => $this->string(),
            'field_0021' => $this->string(),
            'field_0022' => $this->string(),
            'field_0023' => $this->string(),
            'field_0024' => $this->string(),
            'field_0025' => $this->string(),
            'field_0026' => $this->string(),
            'field_0027' => $this->string(),
            'field_0028' => $this->string(),
            'field_0029' => $this->string(),
            'field_0030' => $this->string(),
            'field_0031' => $this->string(),
            'field_0032' => $this->string(),
            'field_0033' => $this->string(),
            'field_0034' => $this->string(),
            'field_0035' => $this->string(),
            'field_0036' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_import_file__sti_coach_report__import_file_idx', '{{%import_file__sti_coach_report}}', 'import_file_id');
        $this->addForeignKey('fk_import_file__sti_coach_report__import_file', '{{%import_file__sti_coach_report}}', 'import_file_id', '{{%import_file}}', 'import_file_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%import_file__sti_coach_report}}');
    }
}
