<?php

use yii\db\Schema;

class m181015_150101_metadata__api_tables extends \yii\db\Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('metadata__api_provider', [
            'metadata__api_provider_id' => $this->primaryKey(),
            'metadata__api_provider' => $this->string(75),
            'metadata__api_provider_desc' => $this->string(150),
            'metadata__api_provider_url' => $this->string(253),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            ], $tableOptions);
        $this->createTable('metadata__api_class', [
            'metadata__api_class_id' => $this->primaryKey(),
            'metadata__api_provider_id' => $this->integer(11),
            'metadata__api_class' => $this->string(253),
            'metadata__api_class_desc' => $this->string(253),
            'metadata__api_class_docs_url' => $this->string(253),
            'metadata__api_reference_example' => $this->string(253),
            'metadata__api_regex_find' => $this->string(253),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            'FOREIGN KEY ([[metadata__api_provider_id]]) REFERENCES metadata__api_provider ([[metadata__api_provider_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
        $this->createTable('metadata__api_class_func', [
            'metadata__api_class_func_id' => $this->primaryKey(),
            'metadata__api_class_id' => $this->integer(11)->notNull(),
            'metadata__api_class_func' => $this->string(253),
            'metadata__api_class_func_desc' => $this->string(253),
            'metadata__api_class_func_docs_url' => $this->string(253),
            'metadata__api_class_func_reference_example' => $this->string(253),
            'metadata__api_class_func_regex_find' => $this->string(253),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            'FOREIGN KEY ([[metadata__api_class_id]]) REFERENCES metadata__api_class ([[metadata__api_class_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                $this->createTable('metadata__api_class_xlat', [
            'metadata__api_class_xlat_id' => $this->primaryKey(),
            'metadata__api_class_id_1' => $this->integer(11)->notNull(),
            'metadata__api_class_id_2' => $this->integer(11)->notNull(),
            'metadata__api_class_xlat' => $this->string(253),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            'FOREIGN KEY ([[metadata__api_class_id_1]]) REFERENCES metadata__api_class ([[metadata__api_class_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
        $this->createTable('metadata__api_class_xlat_item', [
            'metadata__api_class_xlat_item_id' => $this->primaryKey(),
            'metadata__api_class_xlat_id' => $this->integer(11)->notNull(),
            'metadata__api_class_xlat_item' => $this->string(253),
            'metadata__api_class_func_1_reference_example' => $this->string(253),
            'metadata__api_class_func_2_reference_example' => $this->string(253),
            'metadata__api_class_func_1_regex_find' => $this->string(253),
            'metadata__api_class_func_2_regex_find' => $this->string(253),
            'created_at' => $this->datetime(),
            'updated_at' => $this->timestamp()->defaultValue(CURRENT_TIMESTAMP),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
            'lock' => $this->tinyint(1)->defaultValue(0),
            'FOREIGN KEY ([[metadata__api_class_xlat_id]]) REFERENCES metadata__api_class_xlat ([[metadata__api_class_xlat_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
            ], $tableOptions);
                
    }

    public function safeDown()
    {
        $this->dropTable('metadata__api_class_xlat_item');
        $this->dropTable('metadata__api_class_xlat');
        $this->dropTable('metadata__api_class_func');
        $this->dropTable('metadata__api_class');
        $this->dropTable('metadata__api_provider');
    }
}
