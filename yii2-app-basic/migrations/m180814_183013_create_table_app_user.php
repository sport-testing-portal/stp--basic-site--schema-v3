<?php

use yii\db\Migration;

class m180814_183013_create_table_app_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%app_user}}', [
            'app_user_id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'password' => $this->string(),
            'email' => $this->string(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'activation_key' => $this->string(),
            'created_on' => $this->timestamp(),
            'updated_on' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'last_visit_on' => $this->timestamp(),
            'password_set_on' => $this->timestamp(),
            'email_verified' => $this->tinyInteger()->defaultValue('0'),
            'is_active' => $this->tinyInteger()->defaultValue('0'),
            'is_disabled' => $this->tinyInteger()->defaultValue('0'),
            'one_time_password_secret' => $this->string(),
            'one_time_password_code' => $this->string(),
            'one_time_password_counter' => $this->integer()->defaultValue('0'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->addForeignKey('fk_app_user__user', '{{%app_user}}', 'app_user_id', '{{%user}}', 'id', 'CASCADE', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%app_user}}');
    }
}
