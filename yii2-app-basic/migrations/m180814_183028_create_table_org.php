<?php

use yii\db\Migration;

class m180814_183028_create_table_org extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%org}}', [
            'org_id' => $this->primaryKey(),
            'org_type_id' => $this->integer(),
            'org_level_id' => $this->integer(),
            'org' => $this->string()->notNull()->comment('org_name'),
            'org_governing_body' => $this->string(),
            'org_ncaa_clearing_house_id' => $this->string(),
            'org_website_url' => $this->string(),
            'org_twitter_url' => $this->string(),
            'org_facebook_url' => $this->string(),
            'org_phone_main' => $this->string(),
            'org_email_main' => $this->string(),
            'org_addr1' => $this->string(),
            'org_addr2' => $this->string(),
            'org_addr3' => $this->string(),
            'org_city' => $this->string(),
            'org_state_or_region' => $this->string(),
            'org_postal_code' => $this->string(),
            'org_country_code_iso3' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_org__org_type_idx', '{{%org}}', 'org_type_id');
        $this->createIndex('fk_org__org_level_idx', '{{%org}}', 'org_level_id');
        $this->createIndex('org__org_name_idx', '{{%org}}', 'org');
        $this->addForeignKey('fk_org__org_level', '{{%org}}', 'org_level_id', '{{%org_level}}', 'org_level_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_org__org_type', '{{%org}}', 'org_type_id', '{{%org_type}}', 'org_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%org}}');
    }
}
