<?php

use yii\db\Migration;

class m180814_183034_create_table_school_stat extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%school_stat}}', [
            'school_stat_id' => $this->primaryKey(),
            'school_id' => $this->integer(),
            'college_cost_rating' => $this->integer()->comment('A summary value that is used to categorize and sort colleges'),
            'college_cost_tuition_instate' => $this->integer(),
            'college_cost_tuition_outofstate' => $this->integer(),
            'college_cost_room_and_board' => $this->integer(),
            'college_cost_books_and_supplies' => $this->integer(),
            'college_early_decision_dt' => $this->dateTime(),
            'college_early_action_dt' => $this->dateTime(),
            'college_regular_decision_dt' => $this->dateTime(),
            'college_pct_applicants_admitted' => $this->integer()->comment('percentage of applicants admitted'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_school_stat__school', '{{%school_stat}}', 'school_id', '{{%school}}', 'school_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%school_stat}}');
    }
}
