<?php

use yii\db\Migration;

class m180814_183023_create_table_gender extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%gender}}', [
            'gender_id' => $this->primaryKey(),
            'gender' => $this->string()->comment('gender_desc'),
            'gender_code' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('gender_idx_gender_code', '{{%gender}}', 'gender_code');
        $this->createIndex('gender_idx_gender_desc', '{{%gender}}', 'gender');
        $this->createIndex('gender_UNIQUE', '{{%gender}}', 'gender', true);
    }

    public function down()
    {
        $this->dropTable('{{%gender}}');
    }
}
