<?php

use yii\db\Migration;

class m180814_183033_create_table_school_act extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%school_act}}', [
            'school_act_id' => $this->primaryKey(),
            'school_id' => $this->integer(),
            'school_unit_id' => $this->integer(),
            'school_act_composite_25_pct' => $this->integer(),
            'school_act_composite_75_pct' => $this->integer(),
            'school_act_english_25_pct' => $this->integer(),
            'school_act_english_75_pct' => $this->integer(),
            'school_act_math_25_pct' => $this->integer(),
            'school_act_math_75_pct' => $this->integer(),
            'school_act_student_submit_cnt' => $this->integer(),
            'school_act_student_submit_pct' => $this->string(),
            'school_act_report_period' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('school_act__school_unit_id', '{{%school_act}}', 'school_unit_id', true);
        $this->addForeignKey('fk_school_act__school', '{{%school_act}}', 'school_id', '{{%school}}', 'school_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%school_act}}');
    }
}
