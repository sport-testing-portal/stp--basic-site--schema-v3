<?php

use yii\db\Migration;

class m180814_183021_create_table_contact_message_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contact_message_type}}', [
            'contact_message_type_id' => $this->primaryKey(),
            'contact_message_type' => $this->string()->notNull()->comment('contact_message_type_name'),
            'contact_message_type_short_desc' => $this->string(),
            'contact_message_type_long_desc' => $this->string(),
        ], $tableOptions);

        $this->createIndex('contact_message_type_UNIQUE', '{{%contact_message_type}}', 'contact_message_type', true);
    }

    public function down()
    {
        $this->dropTable('{{%contact_message_type}}');
    }
}
