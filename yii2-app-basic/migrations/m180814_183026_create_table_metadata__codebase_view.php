<?php

use yii\db\Migration;

class m180814_183026_create_table_metadata__codebase_view extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%metadata__codebase_view}}', [
            'codebase_view_id' => $this->primaryKey(),
            'codebase_id' => $this->integer()->notNull(),
            'view_function' => $this->string(),
            'view_file' => $this->string(),
            'view_params' => $this->string(),
            'view_url' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_metadata__codebase_view', '{{%metadata__codebase_view}}', 'codebase_id', '{{%metadata__codebase}}', 'codebase_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%metadata__codebase_view}}');
    }
}
