<?php

use yii\db\Migration;

class m180814_183011_create_table_AuthItem extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%AuthItem}}', [
            'name' => $this->string()->notNull()->append('PRIMARY KEY'),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'bizrule' => $this->text(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->createIndex('authitem_idx_type', '{{%AuthItem}}', 'type');
    }

    public function down()
    {
        $this->dropTable('{{%AuthItem}}');
    }
}
