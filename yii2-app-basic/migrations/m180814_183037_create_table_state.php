<?php

use yii\db\Migration;

class m180814_183037_create_table_state extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%state}}', [
            'state_id' => $this->primaryKey()->comment('PK: Unique state ID'),
            'state' => $this->string()->notNull()->comment('state_name=State name with first letter capital'),
            'state_code' => $this->string()->comment('Optional state abbreviation (US is 2 capital letters)'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%state}}');
    }
}
