<?php

use yii\db\Migration;

class m180814_183038_create_table_subscription_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%subscription_type}}', [
            'subscription_type_id' => $this->primaryKey(),
            'subscription_type' => $this->string()->comment('subscription_type_name'),
            'subscription_desc_short' => $this->string()->comment('Used in drop down lists'),
            'subscription_desc_long' => $this->string()->comment('Used for tool tip descriptions'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('subscription_type__subscription_type_name', '{{%subscription_type}}', 'subscription_type');
    }

    public function down()
    {
        $this->dropTable('{{%subscription_type}}');
    }
}
