<?php

use yii\db\Migration;

class m180814_183028_create_table_org_school extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%org_school}}', [
            'org_school_id' => $this->primaryKey(),
            'org_id' => $this->integer(),
            'school_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_org_school__school_idx', '{{%org_school}}', 'school_id');
        $this->createIndex('fk_org_school__org_idx', '{{%org_school}}', 'org_id');
        $this->addForeignKey('fk_org_school__org', '{{%org_school}}', 'org_id', '{{%org}}', 'org_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_org_school__school', '{{%org_school}}', 'school_id', '{{%school}}', 'school_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%org_school}}');
    }
}
