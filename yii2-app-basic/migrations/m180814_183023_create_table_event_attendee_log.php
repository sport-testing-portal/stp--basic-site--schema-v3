<?php

use yii\db\Migration;

class m180814_183023_create_table_event_attendee_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event_attendee_log}}', [
            'event_attendee_log_id' => $this->primaryKey(),
            'event_id' => $this->integer(),
            'person_id' => $this->integer()->comment('player, coach, camp contact via person
'),
            'event_attendee_type_id' => $this->integer()->comment('the persons purpose of attendance'),
            'event_rsvp_dt' => $this->dateTime(),
            'event_attendance_dt' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_event_attendee_log__person_idx', '{{%event_attendee_log}}', 'person_id');
        $this->createIndex('fk_event_attendee_log__event_idx', '{{%event_attendee_log}}', 'event_id');
        $this->createIndex('fk_event_attendee_log__event_attendee_type_idx', '{{%event_attendee_log}}', 'event_attendee_type_id');
        $this->addForeignKey('fk_event_attendee_log__event', '{{%event_attendee_log}}', 'event_id', '{{%event}}', 'event_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_event_attendee_log__event_attendee_type', '{{%event_attendee_log}}', 'event_attendee_type_id', '{{%event_attendee_type}}', 'event_attendee_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_event_attendee_log__person', '{{%event_attendee_log}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%event_attendee_log}}');
    }
}
