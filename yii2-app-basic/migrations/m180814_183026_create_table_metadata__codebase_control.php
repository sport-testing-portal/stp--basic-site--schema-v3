<?php

use yii\db\Migration;

class m180814_183026_create_table_metadata__codebase_control extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%metadata__codebase_control}}', [
            'codebase_control_id' => $this->primaryKey(),
            'codebase_id' => $this->integer()->notNull(),
            'controller_actions' => $this->string(),
            'action_function' => $this->string(),
            'action_params' => $this->string(),
            'action_url' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_metadata__codebase_control_idx', '{{%metadata__codebase_control}}', 'codebase_id');
        $this->addForeignKey('fk_metadata__codebase_control', '{{%metadata__codebase_control}}', 'codebase_id', '{{%metadata__codebase}}', 'codebase_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%metadata__codebase_control}}');
    }
}
