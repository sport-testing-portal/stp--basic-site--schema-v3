<?php

use yii\db\Migration;

class m180814_183020_create_table_conference extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%conference}}', [
            'conference_id' => $this->primaryKey(),
            'conference' => $this->string()->comment('conference_name'),
            'ncaa_division' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('conference__name', '{{%conference}}', 'conference', true);
    }

    public function down()
    {
        $this->dropTable('{{%conference}}');
    }
}
