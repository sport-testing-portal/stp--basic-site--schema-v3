<?php

use yii\db\Migration;

class m180814_183019_create_table_camp_session_player_eval extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%camp_session_player_eval}}', [
            'camp_session_player_eval_id' => $this->primaryKey(),
            'camp_session_id' => $this->integer(),
            'coach_id' => $this->integer(),
            'player_id' => $this->integer(),
            'camp_session_coach_comment' => $this->string(),
            'camp_session_player_comment' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_cspe__camp_session', '{{%camp_session_player_eval}}', 'camp_session_id', '{{%camp_session}}', 'camp_session_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_cspe__coach', '{{%camp_session_player_eval}}', 'coach_id', '{{%coach}}', 'coach_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_cspe__player', '{{%camp_session_player_eval}}', 'player_id', '{{%player}}', 'player_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%camp_session_player_eval}}');
    }
}
