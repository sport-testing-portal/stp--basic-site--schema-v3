<?php

use yii\db\Migration;

class m180814_183030_create_table_player extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player}}', [
            'player_id' => $this->primaryKey(),
            'person_id' => $this->integer(),
            'age_group_id' => $this->integer()->comment('Used to select appropriate age appropriate teams and set default age_group_id for inesrted team records'),
            'player_team_player_id' => $this->integer(),
            'player_sport_position_id' => $this->integer(),
            'player_sport_position2_id' => $this->integer(),
            'player_access_code' => $this->string(),
            'player_waiver_minor_dt' => $this->dateTime(),
            'player_waiver_adult_dt' => $this->dateTime(),
            'player_parent_email' => $this->string(),
            'player_sport_preference' => $this->string(),
            'player_sport_position_preference' => $this->string(),
            'player_shot_side_preference' => $this->string(),
            'player_dominant_side' => $this->string(),
            'player_dominant_foot' => $this->string(),
            'player_statistical_highlights' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()->comment('The currently selected team_player_id as the player can have many teams attached to them via team_player'),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_player__sport_position_idx', '{{%player}}', 'player_sport_position_id');
        $this->createIndex('fk_player_age_group_idx', '{{%player}}', 'age_group_id');
        $this->createIndex('fk_player__team_player_idx', '{{%player}}', 'player_team_player_id');
        $this->createIndex('fk_player__person_idx', '{{%player}}', 'person_id');
        $this->addForeignKey('fk_player__age_group', '{{%player}}', 'age_group_id', '{{%age_group}}', 'age_group_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player__person', '{{%player}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player__sport_position', '{{%player}}', 'player_sport_position_id', '{{%sport_position}}', 'sport_position_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player__team_player', '{{%player}}', 'player_team_player_id', '{{%team_player}}', 'team_player_id', 'SET NULL', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%player}}');
    }
}
