<?php

use yii\db\Migration;

class m180814_183038_create_table_team_league extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%team_league}}', [
            'team_league_id' => $this->primaryKey(),
            'team_league' => $this->string()->notNull()->comment('team_league_name'),
            'team_league_desc_short' => $this->string(),
            'team_league_desc_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%team_league}}');
    }
}
