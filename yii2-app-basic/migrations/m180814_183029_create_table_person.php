<?php

use yii\db\Migration;

class m180814_183029_create_table_person extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%person}}', [
            'person_id' => $this->primaryKey(),
            'org_id' => $this->integer(),
            'app_user_id' => $this->integer(),
            'person_type_id' => $this->integer(),
            'user_id' => $this->integer()->comment('Only one registered user per person'),
            'person_name_prefix' => $this->string(),
            'person_name_first' => $this->string(),
            'person_name_middle' => $this->string(),
            'person_name_last' => $this->string(),
            'person_name_suffix' => $this->string(),
            'person' => $this->string()->comment('person_name_full'),
            'person_phone_personal' => $this->string(),
            'person_email_personal' => $this->string(),
            'person_phone_work' => $this->string(),
            'person_email_work' => $this->string(),
            'person_position_work' => $this->string()->comment('Owner, Director, Administrative Assistant, Other'),
            'gender_id' => $this->integer(),
            'person_image_headshot_url' => $this->string()->comment('URL to a headshot photo'),
            'person_name_nickname' => $this->string(),
            'person_date_of_birth' => $this->dateTime(),
            'person_height' => $this->string()->comment('height is stored in inches'),
            'person_weight' => $this->integer()->comment('weight is stored in pounds'),
            'person_tshirt_size' => $this->string(),
            'person_high_school__graduation_year' => $this->integer()->comment('This will typically be used with players and contain a future value'),
            'person_college_graduation_year' => $this->integer(),
            'person_college_commitment_status' => $this->string(),
            'person_addr_1' => $this->string(),
            'person_addr_2' => $this->string(),
            'person_addr_3' => $this->string()->comment('Required for some institutional and international addresses'),
            'person_city' => $this->string(),
            'person_postal_code' => $this->string(),
            'person_country' => $this->string(),
            'person_country_code' => $this->char(),
            'person_state_or_region' => $this->string(),
            'org_affiliation_begin_dt' => $this->dateTime(),
            'org_affiliation_end_dt' => $this->dateTime(),
            'person_website' => $this->string(),
            'person_profile_url' => $this->string()->comment('This is a link to a remote website'),
            'person_profile_uri' => $this->string()->comment('This is a link to a file uploaded onto our own website'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('user_id_UNIQUE', '{{%person}}', 'user_id', true);
        $this->createIndex('fk_person__person_type_idx', '{{%person}}', 'person_type_id');
        $this->createIndex('fk_person__gender_idx', '{{%person}}', 'gender_id');
        $this->createIndex('person_name_full_idx', '{{%person}}', 'person');
        $this->createIndex('person_idx_person_country_code', '{{%person}}', 'person_country_code');
        $this->createIndex('fk_person__org_idx', '{{%person}}', 'org_id');
        $this->createIndex('fk_person__app_user_idx', '{{%person}}', 'app_user_id');
        $this->createIndex('fk_person__user_idx', '{{%person}}', 'user_id');
        $this->addForeignKey('fk_person__app_user', '{{%person}}', 'app_user_id', '{{%app_user}}', 'app_user_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_person__country', '{{%person}}', 'person_country_code', '{{%country}}', 'iso3', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_person__gender', '{{%person}}', 'gender_id', '{{%gender}}', 'gender_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_person__org', '{{%person}}', 'org_id', '{{%org}}', 'org_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_person__person_type', '{{%person}}', 'person_type_id', '{{%person_type}}', 'person_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%person}}');
    }
}
