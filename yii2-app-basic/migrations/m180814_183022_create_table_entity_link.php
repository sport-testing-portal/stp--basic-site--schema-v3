<?php

use yii\db\Migration;

class m180814_183022_create_table_entity_link extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%entity_link}}', [
            'entity_link_id' => $this->primaryKey(),
            'entity_link_type_id' => $this->integer(),
            'entity1_id' => $this->integer(),
            'entity2_id' => $this->integer(),
            'entity_link_comment' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_entity_link__entity_link_type_idx', '{{%entity_link}}', 'entity_link_type_id');
        $this->addForeignKey('fk_entity_link__entity_link_type', '{{%entity_link}}', 'entity_link_type_id', '{{%entity_link_type}}', 'entity_link_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%entity_link}}');
    }
}
