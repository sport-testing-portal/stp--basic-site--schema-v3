<?php

use yii\db\Migration;

class m180814_183041_create_table_user_login_attempt extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_login_attempt}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'performed_on' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'is_successful' => $this->tinyInteger()->notNull()->defaultValue('0'),
            'session_id' => $this->string(),
            'ipv4' => $this->integer(),
            'user_agent' => $this->string(),
        ], $tableOptions);

        $this->createIndex('user_login_attemp_user_id_idx', '{{%user_login_attempt}}', 'user_id');
        $this->addForeignKey('fk_user_login_attempt__user', '{{%user_login_attempt}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%user_login_attempt}}');
    }
}
