<?php

use yii\db\Migration;

class m180814_183025_create_table_items extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%items}}', [
            'name' => $this->string()->notNull()->append('PRIMARY KEY'),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'bizrule' => $this->text(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->createIndex('items_idx_type', '{{%items}}', 'type');
    }

    public function down()
    {
        $this->dropTable('{{%items}}');
    }
}
