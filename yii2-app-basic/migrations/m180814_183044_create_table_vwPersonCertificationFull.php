<?php

use yii\db\Migration;

class m180814_183044_create_table_vwPersonCertificationFull extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwPersonCertificationFull}}', [
            'person_certification_id' => $this->integer()->notNull()->defaultValue('0')->comment('person_certification_year'),
            'person_id' => $this->integer(),
            'person_certification_type_id' => $this->integer(),
            'person_certification_year' => $this->integer(),
            'person_type_id' => $this->integer()->comment('1=player, 2=coach'),
            'person_certification_type_name' => $this->string()->notNull()->comment('person_certification_type_name'),
            'person_certification_subtype_name' => $this->string(),
            'person_certification_type_display_order' => $this->integer(),
            'person_certification_type_description_short' => $this->string(),
            'person_certification_type_description_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwPersonCertificationFull}}');
    }
}
