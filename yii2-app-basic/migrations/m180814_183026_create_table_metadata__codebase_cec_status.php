<?php

use yii\db\Migration;

class m180814_183026_create_table_metadata__codebase_cec_status extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%metadata__codebase_cec_status}}', [
            'cec_status_id' => $this->primaryKey(),
            'codebase_id' => $this->integer()->notNull(),
            'codebase_cec_id' => $this->integer()->notNull(),
            'cec_status' => $this->string()->notNull(),
            'cec_status_tag' => $this->string(),
            'cec_status_bfr' => $this->string(),
            'cec_status_at' => $this->timestamp(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('fk_metadata__codebase_cec_idx', '{{%metadata__codebase_cec_status}}', 'codebase_cec_id');
        $this->createIndex('fk_metadata__codebase_idx', '{{%metadata__codebase_cec_status}}', 'codebase_id');
        $this->addForeignKey('fk_metadata__codebase', '{{%metadata__codebase_cec_status}}', 'codebase_id', '{{%metadata__codebase}}', 'codebase_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_metadata__codebase_cec', '{{%metadata__codebase_cec_status}}', 'codebase_cec_id', '{{%metadata__codebase_cec}}', 'codebase_cec_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%metadata__codebase_cec_status}}');
    }
}
