<?php

use yii\db\Migration;

class m180814_183045_create_table_vwSiteTaskGroups extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwSiteTaskGroups}}', [
            'name' => $this->string()->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'bizrule' => $this->text(),
            'data' => $this->text(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwSiteTaskGroups}}');
    }
}
