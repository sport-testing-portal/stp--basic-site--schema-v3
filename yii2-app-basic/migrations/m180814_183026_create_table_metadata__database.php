<?php

use yii\db\Migration;

class m180814_183026_create_table_metadata__database extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%metadata__database}}', [
            'database_id' => $this->primaryKey(),
            'table_name' => $this->string()->notNull(),
            'schema_name' => $this->string()->notNull(),
            'col_name_len_max' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('idx_metadata_tablename', '{{%metadata__database}}', 'table_name');
    }

    public function down()
    {
        $this->dropTable('{{%metadata__database}}');
    }
}
