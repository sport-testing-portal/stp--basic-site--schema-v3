<?php

use yii\db\Migration;

class m180814_183012_create_table_address_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%address_type}}', [
            'address_type_id' => $this->primaryKey(),
            'address_type' => $this->string()->notNull()->comment('Delivery, postal, home, permanent, student housing, etc.'),
            'address_type_description_short' => $this->string(),
            'address_type_description_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('address_type__address_type_name', '{{%address_type}}', 'address_type', true);
    }

    public function down()
    {
        $this->dropTable('{{%address_type}}');
    }
}
