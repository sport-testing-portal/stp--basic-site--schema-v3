<?php

use yii\db\Migration;

class m180814_183020_create_table_contact_message extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contact_message}}', [
            'contact_message_id' => $this->primaryKey(),
            'contact_message_type_id' => $this->integer(),
            'contact_message_from' => $this->string(),
            'contact_message_to' => $this->string(),
            'contact_message_subject' => $this->string(),
            'contact_message_body' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%contact_message}}');
    }
}
