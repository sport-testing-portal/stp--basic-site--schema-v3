<?php

use yii\db\Migration;

class m180814_183018_create_table_camp_session_location extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%camp_session_location}}', [
            'camp_session_location_id' => $this->primaryKey(),
            'camp_session_id' => $this->integer(),
            'address_id' => $this->integer(),
            'season_id' => $this->integer(),
            'gender_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_camp_session_location__gender_idx', '{{%camp_session_location}}', 'gender_id');
        $this->createIndex('fk_camp_session_location__address_idx', '{{%camp_session_location}}', 'address_id');
        $this->createIndex('fk_camp_session_location__season_idx', '{{%camp_session_location}}', 'season_id');
        $this->createIndex('fk_camp_session_location__camp_session_idx', '{{%camp_session_location}}', 'camp_session_id');
        $this->addForeignKey('fk_camp_session_location__address', '{{%camp_session_location}}', 'address_id', '{{%address}}', 'address_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_session_location__camp_session', '{{%camp_session_location}}', 'camp_session_id', '{{%camp_session}}', 'camp_session_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_session_location__gender', '{{%camp_session_location}}', 'gender_id', '{{%gender}}', 'gender_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_session_location__season', '{{%camp_session_location}}', 'season_id', '{{%season}}', 'season_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%camp_session_location}}');
    }
}
