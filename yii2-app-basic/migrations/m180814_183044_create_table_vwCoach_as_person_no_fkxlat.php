<?php

use yii\db\Migration;

class m180814_183044_create_table_vwCoach_as_person_no_fkxlat extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwCoach_as_person_no_fkxlat}}', [
            'person_id' => $this->integer()->notNull()->defaultValue('0'),
            'org_id' => $this->integer(),
            'coach_id' => $this->integer()->notNull()->defaultValue('0'),
            'app_user_id' => $this->integer(),
            'person_type_id' => $this->integer(),
            'person_name_prefix' => $this->string(),
            'person_name_first' => $this->string(),
            'person_name_middle' => $this->string(),
            'person_name_last' => $this->string(),
            'person_name_suffix' => $this->string(),
            'person_name_full' => $this->string()->comment('person_name_full'),
            'person_phone_personal' => $this->string(),
            'person_email_personal' => $this->string(),
            'person_phone_work' => $this->string(),
            'person_email_work' => $this->string(),
            'person_position_work' => $this->string()->comment('Owner, Director, Administrative Assistant, Other'),
            'gender_id' => $this->integer(),
            'person_image_headshot_url' => $this->string()->comment('URL to a headshot photo'),
            'person_name_nickname' => $this->string(),
            'person_date_of_birth' => $this->dateTime(),
            'person_height' => $this->string()->comment('height is stored in inches'),
            'person_weight' => $this->integer()->comment('weight is stored in pounds'),
            'person_tshirt_size' => $this->string(),
            'person_addr_1' => $this->string(),
            'person_addr_2' => $this->string(),
            'person_addr_3' => $this->string()->comment('Required for some institutional and international addresses'),
            'person_city' => $this->string(),
            'person_postal_code' => $this->string(),
            'person_country' => $this->string(),
            'person_country_code' => $this->char(),
            'person_state_or_region' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'coach_type_id' => $this->integer(),
            'coach_specialty' => $this->string(),
            'coach_certifications' => $this->string(),
            'coach_comments' => $this->string(),
            'coach_qrcode_uri' => $this->string()->comment('internal URL to the coach QR code'),
            'coach_info_source_scrape_url' => $this->string()->comment('coach info programmatic source data update url'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwCoach_as_person_no_fkxlat}}');
    }
}
