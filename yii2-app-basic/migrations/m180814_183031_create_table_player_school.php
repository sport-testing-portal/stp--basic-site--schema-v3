<?php

use yii\db\Migration;

class m180814_183031_create_table_player_school extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_school}}', [
            'player_school_id' => $this->primaryKey(),
            'player_id' => $this->integer(),
            'school_id' => $this->integer(),
            'player_school_comment' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_player_school__player', '{{%player_school}}', 'player_id', '{{%player}}', 'player_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player_school__school', '{{%player_school}}', 'school_id', '{{%school}}', 'school_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%player_school}}');
    }
}
