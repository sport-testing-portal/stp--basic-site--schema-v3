<?php

use yii\db\Migration;

class m180814_183045_create_table_vwSiteTaskGroupMembers extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwSiteTaskGroupMembers}}', [
            'site_task_group' => $this->string()->notNull(),
            'site_operation' => $this->string()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwSiteTaskGroupMembers}}');
    }
}
