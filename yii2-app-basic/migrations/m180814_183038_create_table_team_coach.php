<?php

use yii\db\Migration;

class m180814_183038_create_table_team_coach extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%team_coach}}', [
            'team_coach_id' => $this->primaryKey(),
            'team_id' => $this->integer()->notNull(),
            'coach_id' => $this->integer()->notNull(),
            'team_coach_coach_type_id' => $this->integer()->comment('functions as the primary_position of the coach on the team'),
            'primary_position' => $this->string()->comment('This text field will be replaced by coach_type_id in MVP v2.0'),
            'team_coach_begin_dt' => $this->dateTime(),
            'team_coach_end_dt' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_team_coach__team_idx', '{{%team_coach}}', 'team_id');
        $this->createIndex('fk_team_coach__coach_idx', '{{%team_coach}}', 'coach_id');
        $this->createIndex('team_coach__team_id__coach_id', '{{%team_coach}}', ['team_id', 'coach_id'], true);
        $this->addForeignKey('fk_team_coach__coach', '{{%team_coach}}', 'coach_id', '{{%coach}}', 'coach_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_team_coach__team', '{{%team_coach}}', 'team_id', '{{%team}}', 'team_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%team_coach}}');
    }
}
