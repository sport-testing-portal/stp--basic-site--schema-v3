<?php

use yii\db\Migration;

class m180814_183022_create_table_entity_link_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%entity_link_type}}', [
            'entity_link_type_id' => $this->primaryKey(),
            'entity_link_type' => $this->string()->notNull()->comment('entity_link_type_name = per2per, org2org, org2per, etc,'),
            'entity_link_type_desc_short' => $this->string(),
            'entity_link_type_desc_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('entity_link_type__entity_link_type_name', '{{%entity_link_type}}', 'entity_link_type', true);
    }

    public function down()
    {
        $this->dropTable('{{%entity_link_type}}');
    }
}
