<?php

use yii\db\Migration;

class m180814_183019_create_table_camp_sport extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%camp_sport}}', [
            'camp_sport_id' => $this->primaryKey(),
            'camp_id' => $this->integer(),
            'sport_id' => $this->integer(),
            'gender_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_camp_sport__gender_idx', '{{%camp_sport}}', 'gender_id');
        $this->addForeignKey('fk_camp_sport__camp', '{{%camp_sport}}', 'camp_id', '{{%camp}}', 'camp_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_sport__gender', '{{%camp_sport}}', 'gender_id', '{{%gender}}', 'gender_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_sport__sport', '{{%camp_sport}}', 'sport_id', '{{%sport}}', 'sport_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%camp_sport}}');
    }
}
