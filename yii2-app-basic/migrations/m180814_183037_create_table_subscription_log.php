<?php

use yii\db\Migration;

class m180814_183037_create_table_subscription_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%subscription_log}}', [
            'subscription_log_id' => $this->primaryKey(),
            'subscription_id' => $this->integer(),
            'subscription_type_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_subscription_log__subscription_idx', '{{%subscription_log}}', 'subscription_id');
        $this->createIndex('fk_subscription_log__subscription_type_idx', '{{%subscription_log}}', 'subscription_type_id');
        $this->addForeignKey('fk_subscription_log__subscription', '{{%subscription_log}}', 'subscription_id', '{{%subscription}}', 'subscription_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_subscription_log__subscription_type', '{{%subscription_log}}', 'subscription_type_id', '{{%subscription_type}}', 'subscription_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%subscription_log}}');
    }
}
