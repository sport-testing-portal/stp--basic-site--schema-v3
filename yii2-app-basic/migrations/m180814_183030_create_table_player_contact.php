<?php

use yii\db\Migration;

class m180814_183030_create_table_player_contact extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_contact}}', [
            'player_contact_id' => $this->primaryKey(),
            'player_contact_type_id' => $this->integer(),
            'player_id' => $this->integer(),
            'person_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_player_contact__player_contact_type_idx', '{{%player_contact}}', 'player_contact_type_id');
        $this->createIndex('fk_player_contact__person_idx', '{{%player_contact}}', 'person_id');
        $this->addForeignKey('fk_player_contact__player', '{{%player_contact}}', 'player_id', '{{%player}}', 'player_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player_contact__player_contact_type', '{{%player_contact}}', 'player_contact_type_id', '{{%player_contact_type}}', 'player_contact_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player_contact__pct', '{{%player_contact}}', 'player_contact_id', '{{%player_contact_type}}', 'player_contact_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player_contact__person', '{{%player_contact}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%player_contact}}');
    }
}
