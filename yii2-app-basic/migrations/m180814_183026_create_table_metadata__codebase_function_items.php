<?php

use yii\db\Migration;

class m180814_183026_create_table_metadata__codebase_function_items extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%metadata__codebase_function_items}}', [
            'metadata__codebase_function_items_id' => $this->primaryKey(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%metadata__codebase_function_items}}');
    }
}
