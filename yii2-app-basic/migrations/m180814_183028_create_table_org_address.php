<?php

use yii\db\Migration;

class m180814_183028_create_table_org_address extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%org_address}}', [
            'org_id' => $this->integer()->notNull(),
            'address_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%org_address}}', ['org_id', 'address_id']);
    }

    public function down()
    {
        $this->dropTable('{{%org_address}}');
    }
}
