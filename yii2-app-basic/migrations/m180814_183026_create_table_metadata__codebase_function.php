<?php

use yii\db\Migration;

class m180814_183026_create_table_metadata__codebase_function extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%metadata__codebase_function}}', [
            'codebase_function_id' => $this->primaryKey(),
            'codebase_function' => $this->string(),
            'codebase_function_short_desc' => $this->string(),
            'codebase_function_long_desc' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%metadata__codebase_function}}');
    }
}
