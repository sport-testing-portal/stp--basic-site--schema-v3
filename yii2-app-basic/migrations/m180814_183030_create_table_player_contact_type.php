<?php

use yii\db\Migration;

class m180814_183030_create_table_player_contact_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_contact_type}}', [
            'player_contact_type_id' => $this->primaryKey(),
            'player_contact_type' => $this->string()->notNull()->comment('player_contact_type_name'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('player_contact_type__player_contact_type_name', '{{%player_contact_type}}', 'player_contact_type', true);
    }

    public function down()
    {
        $this->dropTable('{{%player_contact_type}}');
    }
}
