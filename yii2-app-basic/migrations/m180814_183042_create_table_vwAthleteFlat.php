<?php

use yii\db\Migration;

class m180814_183042_create_table_vwAthleteFlat extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwAthleteFlat}}', [
            'person_name_full' => $this->string()->comment('person_name_full'),
            'person_age' => $this->double(),
            'person_city' => $this->string(),
            'person_state_or_region' => $this->string(),
            'gsm_staff' => $this->string(),
            'player_id' => $this->integer()->notNull()->defaultValue('0'),
            'player_person_id' => $this->integer(),
            'person_id' => $this->integer()->notNull()->defaultValue('0'),
            'person_org_id' => $this->integer(),
            'person_app_user_id' => $this->integer(),
            'person_person_type_id' => $this->integer(),
            'person_user_id' => $this->integer()->comment('Only one registered user per person'),
            'player_age_group_id' => $this->integer()->comment('Used to select appropriate age appropriate teams and set default age_group_id for inesrted team records'),
            'player_team_player_id' => $this->integer(),
            'player_sport_position_id' => $this->integer(),
            'player_sport_position2_id' => $this->integer(),
            'player_sport_position_name' => $this->string()->comment('sport_position_name=Forward, Midfielder, Defender, Goalkeeper, etc'),
            'player_access_code' => $this->string(),
            'player_waiver_minor_dt' => $this->dateTime(),
            'player_waiver_adult_dt' => $this->dateTime(),
            'player_parent_email' => $this->string(),
            'player_sport_preference' => $this->string(),
            'player_sport_position_preference' => $this->string(),
            'player_shot_side_preference' => $this->string(),
            'player_dominant_side' => $this->string(),
            'player_dominant_foot' => $this->string(),
            'player_statistical_highlights' => $this->string(),
            'person_name_prefix' => $this->string(),
            'person_name_first' => $this->string(),
            'person_name_middle' => $this->string(),
            'person_name_last' => $this->string(),
            'person_name_suffix' => $this->string(),
            'person_phone_personal' => $this->string(),
            'person_email_personal' => $this->string(),
            'person_phone_work' => $this->string(),
            'person_email_work' => $this->string(),
            'person_position_work' => $this->string()->comment('Owner, Director, Administrative Assistant, Other'),
            'person_gender_id' => $this->integer(),
            'person_image_headshot_url' => $this->string()->comment('URL to a headshot photo'),
            'person_name_nickname' => $this->string(),
            'person_date_of_birth' => $this->dateTime(),
            'person_dob_eng' => $this->string(),
            'person_bday_long' => $this->string(),
            'person_height' => $this->string()->comment('height is stored in inches'),
            'person_height_imperial' => $this->string(),
            'person_weight' => $this->integer()->comment('weight is stored in pounds'),
            'person_bmi' => $this->double(),
            'person_tshirt_size' => $this->string(),
            'person_addr_1' => $this->string(),
            'person_addr_2' => $this->string(),
            'person_addr_3' => $this->string()->comment('Required for some institutional and international addresses'),
            'person_postal_code' => $this->string(),
            'person_country' => $this->string(),
            'person_country_code' => $this->char(),
            'person_profile_url' => $this->string()->comment('This is a link to a remote website'),
            'person_profile_uri' => $this->string()->comment('This is a link to a file uploaded onto our own website'),
            'person_high_school__graduation_year' => $this->integer()->comment('This will typically be used with players and contain a future value'),
            'person_college_graduation_year' => $this->integer(),
            'person_college_commitment_status' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'person_type_id' => $this->integer()->notNull()->defaultValue('0'),
            'person_type_name' => $this->string()->notNull()->comment('person_type_name=player, coach, camp contact, school contact,'),
            'person_type_desc_short' => $this->string(),
            'person_type_desc_long' => $this->string(),
            'gender_id' => $this->integer()->defaultValue('0'),
            'gender_desc' => $this->string()->comment('gender_desc'),
            'gender_code' => $this->string(),
            'org_id' => $this->integer()->defaultValue('0'),
            'org_org_type_id' => $this->integer(),
            'org_org_level_id' => $this->integer(),
            'org_name' => $this->string()->comment('org_name'),
            'org_type_name' => $this->string()->comment('org_type_name = school, camp, club, vendor, venue, media, '),
            'org_website_url' => $this->string(),
            'org_twitter_url' => $this->string(),
            'org_facebook_url' => $this->string(),
            'org_phone_main' => $this->string(),
            'org_email_main' => $this->string(),
            'org_addr1' => $this->string(),
            'org_addr2' => $this->string(),
            'org_addr3' => $this->string(),
            'org_city' => $this->string(),
            'org_state_or_region' => $this->string(),
            'org_postal_code' => $this->string(),
            'org_country_code_iso3' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwAthleteFlat}}');
    }
}
