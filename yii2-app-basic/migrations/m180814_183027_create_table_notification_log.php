<?php

use yii\db\Migration;

class m180814_183027_create_table_notification_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notification_log}}', [
            'notification_log_id' => $this->primaryKey(),
            'notification_id' => $this->integer(),
            'notification_template_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_notification_log__notification', '{{%notification_log}}', 'notification_id', '{{%notification}}', 'notification_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_notification_log__ntt', '{{%notification_log}}', 'notification_template_id', '{{%notification_template}}', 'notification_template_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%notification_log}}');
    }
}
