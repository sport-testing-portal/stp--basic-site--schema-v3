<?php

use yii\db\Migration;

class m180814_183026_create_table_metadata__codebase_cec extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%metadata__codebase_cec}}', [
            'codebase_cec_id' => $this->primaryKey(),
            'codebase_cec' => $this->string()->notNull(),
            'codebase_cec_desc_short' => $this->string(),
            'codebase_cec_desc_long' => $this->string(),
            'cec_status_tag' => $this->string(),
            'cec_scope' => $this->string()->comment('sql statement defines the files included by the CEC'),
            'rule_file_uri' => $this->string(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%metadata__codebase_cec}}');
    }
}
