<?php

use yii\db\Migration;

class m180814_183013_create_table_app_parm extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%app_parm}}', [
            'app_parm_id' => $this->primaryKey(),
            'app_parm_type' => $this->string(),
            'ap_int' => $this->integer(),
            'ap_varchar' => $this->string(),
            'ap_datetime' => $this->dateTime(),
            'ap_comment' => $this->string(),
            'ap_display_order' => $this->integer(),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
            'created_by' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('app_parm__app_parm_type', '{{%app_parm}}', 'app_parm_type');
    }

    public function down()
    {
        $this->dropTable('{{%app_parm}}');
    }
}
