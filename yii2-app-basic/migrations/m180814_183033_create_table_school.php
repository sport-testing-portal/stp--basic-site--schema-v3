<?php

use yii\db\Migration;

class m180814_183033_create_table_school extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%school}}', [
            'school_id' => $this->primaryKey(),
            'org_id' => $this->integer(),
            'religious_affiliation_id' => $this->integer(),
            'conference_id__female' => $this->integer(),
            'conference_id__male' => $this->integer(),
            'conference_id__main' => $this->integer(),
            'school_ipeds_id' => $this->integer(),
            'school_unit_id' => $this->integer(),
            'school_ope_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('school__conference_id_male', '{{%school}}', 'conference_id__male');
        $this->createIndex('school__school_unit_id', '{{%school}}', 'school_unit_id');
        $this->createIndex('fk_school__org_idx', '{{%school}}', 'org_id');
        $this->createIndex('school__conference_id_female', '{{%school}}', 'conference_id__female');
        $this->addForeignKey('fk_school__org', '{{%school}}', 'org_id', '{{%org}}', 'org_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_school__religious_affiliation', '{{%school}}', 'religious_affiliation_id', '{{%religious_affiliation}}', 'religious_affiliation_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%school}}');
    }
}
