<?php

use yii\db\Migration;

class m180814_183046_create_table_vwTestResultCategory extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwTestResultCategory}}', [
            'id' => $this->integer()->defaultValue('0'),
            'person' => $this->string()->comment('person_name_full'),
            'ptype' => $this->string()->comment('person_type_name=player, coach, camp contact, school contact,'),
            'gender_code' => $this->string(),
            'rating_bias' => $this->string(),
            'tedtd_display_order' => $this->integer(),
            'test_desc' => $this->string(),
            'category' => $this->string()->comment('test_eval_category_name'),
            'test_category' => $this->string()->comment('test_eval_category_name'),
            'test_type' => $this->string()->comment('test_eval_type_name=This is category#1 in Jami - lingo (the language of the domain)'),
            'test_subtype' => $this->string()->comment('This is category#2 in Jami - lingo (the language of the domain)'),
            'split' => $this->string(),
            'score' => $this->decimal(),
            'test_units' => $this->string(),
            'attempt' => $this->tinyInteger(),
            'trial_status' => $this->string(),
            'overall_ranking' => $this->integer(),
            'positional_ranking' => $this->integer(),
            'total_overall_ranking' => $this->decimal(),
            'total_positional_ranking' => $this->decimal(),
            'score_url' => $this->string(),
            'video_url' => $this->string(),
            'test_date' => $this->string(),
            'test_date_iso' => $this->dateTime(),
            'tester' => $this->string()->comment('test_eval_provider_name'),
            'person_name_first' => $this->string(),
            'person_name_last' => $this->string(),
            'person_city' => $this->string(),
            'person_country_code' => $this->char(),
            'person_state_or_region' => $this->string(),
            'person_postal_code' => $this->string(),
            'player_sport_position_id' => $this->integer(),
            'sport_position_name' => $this->string()->comment('sport_position_name=Forward, Midfielder, Defender, Goalkeeper, etc'),
            'gender_desc' => $this->string()->comment('gender_desc'),
            'gender_id' => $this->integer()->defaultValue('0'),
            'age_group_name' => $this->string(),
            'age_group_id' => $this->integer()->defaultValue('0'),
            'age_group_min' => $this->integer(),
            'age_group_max' => $this->integer(),
            'country_long_name' => $this->string()->defaultValue(''),
            'latitude' => $this->string(),
            'longitude' => $this->string(),
            'combine_name' => $this->string()->notNull()->defaultValue(''),
            'user_id' => $this->integer()->comment('Only one registered user per person'),
            'person_id' => $this->integer()->notNull()->defaultValue('0'),
            'player_id' => $this->integer()->defaultValue('0'),
            'source_event_id' => $this->integer(),
            'summary_id' => $this->integer()->notNull()->defaultValue('0'),
            'detail_id' => $this->integer()->defaultValue('0'),
            'provider_id' => $this->integer()->defaultValue('0'),
            'category_id' => $this->integer()->defaultValue('0'),
            'type_id' => $this->integer()->defaultValue('0'),
            'desc_id' => $this->integer()->defaultValue('0'),
            'file_id' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwTestResultCategory}}');
    }
}
