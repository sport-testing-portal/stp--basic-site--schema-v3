<?php

use yii\db\Migration;

class m180814_183046_create_table_vwTestResult extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwTestResult}}', [
            'person' => $this->string()->comment('person_name_full'),
            'ptype' => $this->string()->comment('person_type_name=player, coach, camp contact, school contact,'),
            'gender' => $this->string(),
            'test_desc' => $this->string(),
            'test_type' => $this->string()->comment('test_eval_type_name=This is category#1 in Jami - lingo (the language of the domain)'),
            'split' => $this->string(),
            'score' => $this->decimal(),
            'test_units' => $this->string(),
            'attempt' => $this->tinyInteger(),
            'trial_status' => $this->string(),
            'overall_ranking' => $this->integer(),
            'positional_ranking' => $this->integer(),
            'total_overall_ranking' => $this->decimal(),
            'total_positional_ranking' => $this->decimal(),
            'score_url' => $this->string(),
            'video_url' => $this->string(),
            'test_date' => $this->string(),
            'test_date_iso' => $this->dateTime(),
            'tester' => $this->string()->comment('test_eval_provider_name'),
            'person_id' => $this->integer()->notNull()->defaultValue('0'),
            'player_id' => $this->integer()->defaultValue('0'),
            'test_eval_summary_log_id' => $this->integer()->notNull()->defaultValue('0'),
            'test_eval_detail_log_id' => $this->integer()->defaultValue('0'),
            'source_event_id' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwTestResult}}');
    }
}
