<?php

use yii\db\Migration;

class m180814_183025_create_table_location extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%location}}', [
            'location_id' => $this->primaryKey(),
            'city' => $this->string(),
            'state_or_region' => $this->string(),
            'country' => $this->string()->defaultValue('USA'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('ix_location__city', '{{%location}}', 'city');
        $this->createIndex('ux_location__city_state_country', '{{%location}}', ['city', 'state_or_region', 'country'], true);
    }

    public function down()
    {
        $this->dropTable('{{%location}}');
    }
}
