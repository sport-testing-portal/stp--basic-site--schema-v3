<?php

use yii\db\Migration;

class m180814_183036_create_table_search_history extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%search_history}}', [
            'search_history_id' => $this->primaryKey(),
            'search_criteria_template_id' => $this->integer(),
            'person_id' => $this->integer(),
            'user_group_name' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_search_history__person_idx', '{{%search_history}}', 'person_id');
        $this->createIndex('fk_search_history__search_template_idx', '{{%search_history}}', 'search_criteria_template_id');
        $this->addForeignKey('fk_search_history__person', '{{%search_history}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_search_history__search_template', '{{%search_history}}', 'search_criteria_template_id', '{{%search_criteria_template}}', 'search_criteria_template_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%search_history}}');
    }
}
