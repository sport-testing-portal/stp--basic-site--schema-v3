<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CampLocation */

$this->title = 'Create Camp Location';
$this->params['breadcrumbs'][] = ['label' => 'Camp Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camp-location-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
