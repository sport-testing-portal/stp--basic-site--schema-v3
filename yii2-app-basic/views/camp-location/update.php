<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CampLocation */

$this->title = 'Update Camp Location: ' . ' ' . $model->camp_location_id;
$this->params['breadcrumbs'][] = ['label' => 'Camp Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->camp_location_id, 'url' => ['view', 'id' => $model->camp_location_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="camp-location-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
