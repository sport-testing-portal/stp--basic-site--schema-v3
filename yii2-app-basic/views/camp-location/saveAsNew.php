<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CampLocation */

$this->title = 'Save As New Camp Location: '. ' ' . $model->camp_location_id;
$this->params['breadcrumbs'][] = ['label' => 'Camp Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->camp_location_id, 'url' => ['view', 'id' => $model->camp_location_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="camp-location-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
