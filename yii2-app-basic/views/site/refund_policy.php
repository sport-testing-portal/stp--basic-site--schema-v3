<?php

use yii\helpers\Html;
use app\modules\gsm\assets\StpAsset;

$assets = \app\modules\gsm\assets\StpAsset::register($this);

$this->title = \Yii::$app->name . ' - Refund Policy';
$this->params['breadcrumbs'][] = 'Refund Policy';
?>

<!-- Page content -->
<div id="page-content" class="animation-pullDown" style="max-width: 1200px;">
    <!-- Article Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media" style="height: 150px;">
        <div class="header-section">
            <a href="/" class="pull-right">
                <img height="100" width="100" 
                     src="<?php echo $assets->baseUrl; ?>/img/GSMlogoFINALv2.png" 
                     alt="logo" class="img-circle">
            </a>
            <h1>Refund Policy</h1>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img style="width:100%; height: 160px; top:0px; left:0px; 
             right: -55px; margin-left: 0px; margin-right: 0px; 
             bottom: -15px; margin-bottom: 15px; overflow: hidden;" 
             height="150" src="<?php echo $assets->baseUrl; ?>/img/soccerfield_2560x248.png" 
             alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Article Header -->

    <!-- Article Content -->
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <!-- Article Block -->
            <div class="block block-alt-noborder">
                <!-- Article Content -->
                <article>
                    <h3 class="sub-header text-center">
                        <a href="page_ready_user_profile.php"><strong>Refund Policy</strong></a>
                        <br><strong>January 20, 2018</strong>
                    </h3>
                    <p>
                        You may cancel your registration for the Example Company (EC) Combine by 
                        sending an e-mail to info@example.com no later than 48 hours prior to 
                        the start time of the EC Combine for which you registered. No refunds will be made 
                        for cancellation request received after that time, unless you have exceptional 
                        circumstances. In the event of exceptional circumstance EC may, at their own 
                        discretion, grant a refund. Please email info@example.com to discuss 
                        exceptional circumstances. 
                        <a	href="mailto:info@example.com?subject=Refund%20Policy" 
                           data-toggle="tooltip" data-placement="bottom" title=""
                           data-original-title="Send email regarding our refund policy">
                            email us
                        </a>
                    </p>
                    <p>
                        Refunds will be issued in the same form that payment was made minus any handling 
                        fees incurred by Example Company during the registration process. Handling Fees 
                        are things like non-refundable payment gateway charges and registration fees and 
                        average around $10 per registration.
                    </p>
                    <p>
                        We reserve the right to modify the cost of registration for any Combine and/or 
                        any other services/products offered for purchase through the Site. We are not 
                        responsible for any error in copy or images relating to services/products offered 
                        for purchase through the Site.
                    </p>
                    <p>
                        We reserve the right to cancel any Combine.
                    </p>

                </article>
                <!-- END Article Content -->
            </div>
            <!-- END Article Block -->

            <!-- Author and More Row -->
            <div class="row">
                <div class="col-md-6">
                    <!-- More Block -->
                    <div class="block block-alt-noborder full">
                        <!-- More Content -->
                        <h3 class="sub-header">Read More</h3>
                        <ul class="fa-ul list-li-push">
                            <li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
                                    href="page?view=terms_of_use">Terms of Use</a></li>
                            <li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
                                    href="page?view=privacy_policy#standard_privacy_policy">Standard Website Privacy Policy</a>
                            </li>
                            <li style="font-size: larger"><i class="fa fa-angle-right fa-li"></i> <a 
                                    href="page?view=privacy_policy#children__child_privacy_policy">Child Privacy Policy</a>
                            </li>
                        </ul>
                        <!-- END More Content -->
                    </div>
                    <!-- END More Block -->
                </div>
            </div>
            <!-- END Author and More Row -->
        </div>
    </div>
    <!-- END Article Content -->
</div>
<!-- END Page Content -->
