<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use alcea\yii2PrismSyntaxHighlighter\PrismSyntaxHighlighter;


//use cebe\markdown;

$name = 'Syntax Highlight Test';
$this->title = $name;

// Dark is good. It creates its own window
// Solarized light is is good too, has a postit note background 
PrismSyntaxHighlighter::widget([
    'theme' => PrismSyntaxHighlighter::THEME_DEFAULT,
    'languages' => ['php', 'php-extras', 'css'],
    //'plugins' => ['copy-to-clipboard']
]);

$md1 = <<<MD_FILE1
$(document).on('focusout', 'div', function(event) {
	event.preventDefault();
	console.log("event failed");
});
MD_FILE1;

$md = <<<MD_FILE
$(document).on('focusout', 'input[name="testme"]', function(event) {
	event.preventDefault();
	console.log("event failed");
});
MD_FILE;
 
?>


<div class="site">
    <?php $form = ActiveForm::begin(); ?>
    <?php 
    //$this->registerJs($md, View::POS_END, 'my-options');  
    $this->registerJs($md);
    ?>
    
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode("Some text goes here!")) ?>
    </div>

    <input name="testme" value="$text = '';" width="100%" height="100px">
    <p>Some PHP code</p>
    <pre class="language-php">
        <strong>Debug</strong><br>
        <code>nl2br(Html::encode("Some text goes here!"));</code>
    </pre>

</div>

<div>
    <p>
        <!--echo kartik\markdown\Markdown::process($md, 'gfm-comment')-->
        <?php //echo kartik\markdown\Markdown::process($md); ?>
    </p>
    <?php ActiveForm::end(); ?>    
</div>


