<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseCecStatus */

$this->title = $model->cec_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Database Cec Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-database-cec-status-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Metadata Database Cec Status'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->cec_status_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->cec_status_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->cec_status_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'cec_status_id',
        [
            'attribute' => 'database.database_id',
            'label' => 'Database',
        ],
        [
            'attribute' => 'databaseCec.database_cec_id',
            'label' => 'Database Cec',
        ],
        'cec_status',
        'cec_status_tag',
        'cec_status_bfr',
        'cec_status_at',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MetadataDatabase<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataDatabase = [
        'table_name',
        'schema_name',
        'col_name_len_max',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->database,
        'attributes' => $gridColumnMetadataDatabase    ]);
    ?>
    <div class="row">
        <h4>MetadataDatabaseCec<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataDatabaseCec = [
        'database_cec',
        'database_cec_desc_short',
        'database_cec_desc_long',
        'cec_status_tag',
        'cec_scope',
        'rule_file_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->databaseCec,
        'attributes' => $gridColumnMetadataDatabaseCec    ]);
    ?>
</div>
