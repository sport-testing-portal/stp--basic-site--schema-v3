<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseCecStatus */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="metadata-database-cec-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'cec_status_id')->textInput(['placeholder' => 'Cec Status']) ?>

    <?= $form->field($model, 'database_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabase::find()->orderBy('database_id')->asArray()->all(), 'database_id', 'database_id'),
        'options' => ['placeholder' => 'Choose Metadata  database'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'database_cec_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabaseCec::find()->orderBy('database_cec_id')->asArray()->all(), 'database_cec_id', 'database_cec_id'),
        'options' => ['placeholder' => 'Choose Metadata  database cec'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'cec_status')->textInput(['maxlength' => true, 'placeholder' => 'Cec Status']) ?>

    <?= $form->field($model, 'cec_status_tag')->textInput(['maxlength' => true, 'placeholder' => 'Cec Status Tag']) ?>

    <?= $form->field($model, 'cec_status_bfr')->textInput(['maxlength' => true, 'placeholder' => 'Cec Status Bfr']) ?>

    <?= $form->field($model, 'cec_status_at')->textInput(['placeholder' => 'Cec Status At']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
