<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseCecStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-database-cec-status-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cec_status_id')->textInput(['placeholder' => 'Cec Status']) ?>

    <?= $form->field($model, 'database_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabase::find()->orderBy('database_id')->asArray()->all(), 'database_id', 'database_id'),
        'options' => ['placeholder' => 'Choose Metadata  database'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'database_cec_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabaseCec::find()->orderBy('database_cec_id')->asArray()->all(), 'database_cec_id', 'database_cec_id'),
        'options' => ['placeholder' => 'Choose Metadata  database cec'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'cec_status')->textInput(['maxlength' => true, 'placeholder' => 'Cec Status']) ?>

    <?= $form->field($model, 'cec_status_tag')->textInput(['maxlength' => true, 'placeholder' => 'Cec Status Tag']) ?>

    <?php /* echo $form->field($model, 'cec_status_bfr')->textInput(['maxlength' => true, 'placeholder' => 'Cec Status Bfr']) */ ?>

    <?php /* echo $form->field($model, 'cec_status_at')->textInput(['placeholder' => 'Cec Status At']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
