<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseCecStatus */

$this->title = 'Save As New Metadata Database Cec Status: '. ' ' . $model->cec_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Database Cec Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cec_status_id, 'url' => ['view', 'id' => $model->cec_status_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-database-cec-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
