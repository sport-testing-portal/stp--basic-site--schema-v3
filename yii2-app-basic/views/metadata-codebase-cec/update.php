<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseCec */

$this->title = 'Update Codebase CEC: ' . ' ' . $model->codebase_cec_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata', 'url' => '/metadata-codebase/index'];
$this->params['breadcrumbs'][] = ['label' => 'Codebase CEC', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codebase_cec_id, 'url' => ['view', 'id' => $model->codebase_cec_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-codebase-cec-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
