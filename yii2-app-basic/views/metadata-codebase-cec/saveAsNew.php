<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseCec */

$this->title = 'Save As New Metadata Codebase Cec: '. ' ' . $model->codebase_cec_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Cecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codebase_cec_id, 'url' => ['view', 'id' => $model->codebase_cec_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-codebase-cec-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
