<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseCec */

$this->title = $model->codebase_cec_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Cecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-codebase-cec-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Metadata Codebase Cec'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->codebase_cec_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->codebase_cec_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->codebase_cec_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'codebase_cec_id',
        'codebase_cec',
        'codebase_cec_desc_short',
        'codebase_cec_desc_long',
        'cec_status_tag',
        'cec_scope',
        'rule_file_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerMetadataCodebaseCecStatus->totalCount){
    $gridColumnMetadataCodebaseCecStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            'cec_status_id',
            [
                'attribute' => 'codebase.codebase_id',
                'label' => 'Codebase'
            ],
                        'cec_status',
            'cec_status_tag',
            'cec_status_bfr',
            'cec_status_at',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataCodebaseCecStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--codebase-cec-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Codebase Cec Status'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataCodebaseCecStatus
    ]);
}
?>

    </div>
</div>
