<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseCec */

?>
<div class="metadata-codebase-cec-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->codebase_cec_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'codebase_cec_id',
        'codebase_cec',
        //'codebase_cec_desc_short',
        [
            'attribute'=>'codebase_cec_desc_short',
            'label'=>'Description Short',
            'width'=>'70px'
        ],
        //'codebase_cec_desc_long',
        [
            'attribute'=>'codebase_cec_desc_long',
            'label'=>'Description Long',
            'width'=>'50px'
        ],
        'cec_status_tag',
        'cec_scope',
        'rule_file_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>