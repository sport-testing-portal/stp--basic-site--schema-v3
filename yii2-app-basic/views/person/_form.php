<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Coach', 
        'relID' => 'coach', 
        'value' => \yii\helpers\Json::encode($model->coaches),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'EventAttendeeLog', 
        'relID' => 'event-attendee-log', 
        'value' => \yii\helpers\Json::encode($model->eventAttendeeLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Media', 
        'relID' => 'media', 
        'value' => \yii\helpers\Json::encode($model->media),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Note', 
        'relID' => 'note', 
        'value' => \yii\helpers\Json::encode($model->notes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PaymentLog', 
        'relID' => 'payment-log', 
        'value' => \yii\helpers\Json::encode($model->paymentLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PersonAddress', 
        'relID' => 'person-address', 
        'value' => \yii\helpers\Json::encode($model->personAddresses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PersonCertification', 
        'relID' => 'person-certification', 
        'value' => \yii\helpers\Json::encode($model->personCertifications),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Player', 
        'relID' => 'player', 
        'value' => \yii\helpers\Json::encode($model->players),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerContact', 
        'relID' => 'player-contact', 
        'value' => \yii\helpers\Json::encode($model->playerContacts),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerResultsSearchLog', 
        'relID' => 'player-results-search-log', 
        'value' => \yii\helpers\Json::encode($model->playerResultsSearchLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SearchCriteriaTemplate', 
        'relID' => 'search-criteria-template', 
        'value' => \yii\helpers\Json::encode($model->searchCriteriaTemplates),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SearchHistory', 
        'relID' => 'search-history', 
        'value' => \yii\helpers\Json::encode($model->searchHistories),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Subscription', 
        'relID' => 'subscription', 
        'value' => \yii\helpers\Json::encode($model->subscriptions),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'TestEvalSummaryLog', 
        'relID' => 'test-eval-summary-log', 
        'value' => \yii\helpers\Json::encode($model->testEvalSummaryLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="person-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'person_id')->textInput(['placeholder' => 'Person']) ?>

    <?= $form->field($model, 'org_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org'),
        'options' => ['placeholder' => 'Choose Org'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'app_user_id')->textInput(['placeholder' => 'App User']) ?>

    <?= $form->field($model, 'person_type_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\PersonType::find()->orderBy('person_type_id')->asArray()->all(), 'person_type_id', 'person_type'),
        'options' => ['placeholder' => 'Choose Person type'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'user_id')->textInput(['placeholder' => 'User']) ?>

    <?= $form->field($model, 'person_name_prefix')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Prefix']) ?>

    <?= $form->field($model, 'person_name_first')->textInput(['maxlength' => true, 'placeholder' => 'Person Name First']) ?>

    <?= $form->field($model, 'person_name_middle')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Middle']) ?>

    <?= $form->field($model, 'person_name_last')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Last']) ?>

    <?= $form->field($model, 'person_name_suffix')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Suffix']) ?>

    <?= $form->field($model, 'person')->textInput(['maxlength' => true, 'placeholder' => 'Person']) ?>

    <?= $form->field($model, 'person_phone_personal')->textInput(['maxlength' => true, 'placeholder' => 'Person Phone Personal']) ?>

    <?= $form->field($model, 'person_email_personal')->textInput(['maxlength' => true, 'placeholder' => 'Person Email Personal']) ?>

    <?= $form->field($model, 'person_phone_work')->textInput(['maxlength' => true, 'placeholder' => 'Person Phone Work']) ?>

    <?= $form->field($model, 'person_email_work')->textInput(['maxlength' => true, 'placeholder' => 'Person Email Work']) ?>

    <?= $form->field($model, 'person_position_work')->textInput(['maxlength' => true, 'placeholder' => 'Person Position Work']) ?>

    <?= $form->field($model, 'gender_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->orderBy('gender_id')->asArray()->all(), 'gender_id', 'gender'),
        'options' => ['placeholder' => 'Choose Gender'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'person_image_headshot_url')->textInput(['maxlength' => true, 'placeholder' => 'Person Image Headshot Url']) ?>

    <?= $form->field($model, 'person_name_nickname')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Nickname']) ?>

    <?= $form->field($model, 'person_date_of_birth')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Person Date Of Birth',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'person_height')->textInput(['maxlength' => true, 'placeholder' => 'Person Height']) ?>

    <?= $form->field($model, 'person_weight')->textInput(['placeholder' => 'Person Weight']) ?>

    <?= $form->field($model, 'person_tshirt_size')->textInput(['maxlength' => true, 'placeholder' => 'Person Tshirt Size']) ?>

    <?= $form->field($model, 'person_high_school__graduation_year')->textInput(['placeholder' => 'Person High School  Graduation Year']) ?>

    <?= $form->field($model, 'person_college_graduation_year')->textInput(['placeholder' => 'Person College Graduation Year']) ?>

    <?= $form->field($model, 'person_college_commitment_status')->textInput(['maxlength' => true, 'placeholder' => 'Person College Commitment Status']) ?>

    <?= $form->field($model, 'person_addr_1')->textInput(['maxlength' => true, 'placeholder' => 'Person Addr 1']) ?>

    <?= $form->field($model, 'person_addr_2')->textInput(['maxlength' => true, 'placeholder' => 'Person Addr 2']) ?>

    <?= $form->field($model, 'person_addr_3')->textInput(['maxlength' => true, 'placeholder' => 'Person Addr 3']) ?>

    <?= $form->field($model, 'person_city')->textInput(['maxlength' => true, 'placeholder' => 'Person City']) ?>

    <?= $form->field($model, 'person_postal_code')->textInput(['maxlength' => true, 'placeholder' => 'Person Postal Code']) ?>

    <?= $form->field($model, 'person_country')->textInput(['maxlength' => true, 'placeholder' => 'Person Country']) ?>

    <?= $form->field($model, 'person_country_code')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy('iso3')->asArray()->all(), 'iso3', 'country'),
        'options' => ['placeholder' => 'Choose Country'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'person_state_or_region')->textInput(['maxlength' => true, 'placeholder' => 'Person State Or Region']) ?>

    <?= $form->field($model, 'org_affiliation_begin_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Org Affiliation Begin Dt',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'org_affiliation_end_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Org Affiliation End Dt',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'person_website')->textInput(['maxlength' => true, 'placeholder' => 'Person Website']) ?>

    <?= $form->field($model, 'person_profile_url')->textInput(['maxlength' => true, 'placeholder' => 'Person Profile Url']) ?>

    <?= $form->field($model, 'person_profile_uri')->textInput(['maxlength' => true, 'placeholder' => 'Person Profile Uri']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Coach'),
            'content' => $this->render('_formCoach', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->coaches),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('EventAttendeeLog'),
            'content' => $this->render('_formEventAttendeeLog', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->eventAttendeeLogs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Media'),
            'content' => $this->render('_formMedia', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->media),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Note'),
            'content' => $this->render('_formNote', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->notes),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PaymentLog'),
            'content' => $this->render('_formPaymentLog', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->paymentLogs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PersonAddress'),
            'content' => $this->render('_formPersonAddress', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->personAddresses),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PersonCertification'),
            'content' => $this->render('_formPersonCertification', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->personCertifications),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Player'),
            'content' => $this->render('_formPlayer', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->players),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerContact'),
            'content' => $this->render('_formPlayerContact', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerContacts),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerResultsSearchLog'),
            'content' => $this->render('_formPlayerResultsSearchLog', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerResultsSearchLogs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SearchCriteriaTemplate'),
            'content' => $this->render('_formSearchCriteriaTemplate', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->searchCriteriaTemplates),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SearchHistory'),
            'content' => $this->render('_formSearchHistory', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->searchHistories),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Subscription'),
            'content' => $this->render('_formSubscription', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->subscriptions),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('TestEvalSummaryLog'),
            'content' => $this->render('_formTestEvalSummaryLog', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->testEvalSummaryLogs),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
