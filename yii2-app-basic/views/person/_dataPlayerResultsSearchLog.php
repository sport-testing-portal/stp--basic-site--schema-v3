<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->playerResultsSearchLogs,
        'key' => 'player_results_search_log_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'player_results_search_log_id',
        [
                'attribute' => 'user.username',
                'label' => 'User'
            ],
        'row_count',
        'gender',
        'age_range_min',
        'age_range_max',
        'first_name',
        'last_name',
        'test_provider',
        'team',
        'position',
        'test_type',
        'test_category',
        'test_description',
        'country',
        'state_province',
        'zip_code',
        'zip_code_radius',
        'city',
        'city_radius',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'player-results-search-log'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
