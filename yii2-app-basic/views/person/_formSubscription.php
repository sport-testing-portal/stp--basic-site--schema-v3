<div class="form-group" id="add-subscription">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Subscription',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'subscription_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'subscription_type_id' => [
            'label' => 'Subscription type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\SubscriptionType::find()->orderBy('subscription_type')->asArray()->all(), 'subscription_type_id', 'subscription_type'),
                'options' => ['placeholder' => 'Choose Subscription type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'org_id' => [
            'label' => 'Org',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org')->asArray()->all(), 'org_id', 'org'),
                'options' => ['placeholder' => 'Choose Org'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'subscription_status_type_id' => [
            'label' => 'Subscription status type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\SubscriptionStatusType::find()->orderBy('subscription_status_type')->asArray()->all(), 'subscription_status_type_id', 'subscription_status_type'),
                'options' => ['placeholder' => 'Choose Subscription status type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'subscription_begin_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Subscription Begin Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'subscription_end_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Subscription End Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowSubscription(' . $key . '); return false;', 'id' => 'subscription-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Subscription', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowSubscription()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

