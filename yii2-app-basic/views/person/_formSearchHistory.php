<div class="form-group" id="add-search-history">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'SearchHistory',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'search_history_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'search_criteria_template_id' => [
            'label' => 'Search criteria template',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\SearchCriteriaTemplate::find()->orderBy('search_criteria_template_id')->asArray()->all(), 'search_criteria_template_id', 'search_criteria_template_id'),
                'options' => ['placeholder' => 'Choose Search criteria template'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'user_group_name' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowSearchHistory(' . $key . '); return false;', 'id' => 'search-history-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Search History', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowSearchHistory()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

