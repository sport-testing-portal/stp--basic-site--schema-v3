<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-person-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'person_id')->textInput(['placeholder' => 'Person']) ?>

    <?= $form->field($model, 'org_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org'),
        'options' => ['placeholder' => 'Choose Org'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'app_user_id')->textInput(['placeholder' => 'App User']) ?>

    <?= $form->field($model, 'person_type_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\PersonType::find()->orderBy('person_type_id')->asArray()->all(), 'person_type_id', 'person_type'),
        'options' => ['placeholder' => 'Choose Person type'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'user_id')->textInput(['placeholder' => 'User']) ?>

    <?php /* echo $form->field($model, 'person_name_prefix')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Prefix']) */ ?>

    <?php /* echo $form->field($model, 'person_name_first')->textInput(['maxlength' => true, 'placeholder' => 'Person Name First']) */ ?>

    <?php /* echo $form->field($model, 'person_name_middle')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Middle']) */ ?>

    <?php /* echo $form->field($model, 'person_name_last')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Last']) */ ?>

    <?php /* echo $form->field($model, 'person_name_suffix')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Suffix']) */ ?>

    <?php /* echo $form->field($model, 'person')->textInput(['maxlength' => true, 'placeholder' => 'Person']) */ ?>

    <?php /* echo $form->field($model, 'person_phone_personal')->textInput(['maxlength' => true, 'placeholder' => 'Person Phone Personal']) */ ?>

    <?php /* echo $form->field($model, 'person_email_personal')->textInput(['maxlength' => true, 'placeholder' => 'Person Email Personal']) */ ?>

    <?php /* echo $form->field($model, 'person_phone_work')->textInput(['maxlength' => true, 'placeholder' => 'Person Phone Work']) */ ?>

    <?php /* echo $form->field($model, 'person_email_work')->textInput(['maxlength' => true, 'placeholder' => 'Person Email Work']) */ ?>

    <?php /* echo $form->field($model, 'person_position_work')->textInput(['maxlength' => true, 'placeholder' => 'Person Position Work']) */ ?>

    <?php /* echo $form->field($model, 'gender_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->orderBy('gender_id')->asArray()->all(), 'gender_id', 'gender'),
        'options' => ['placeholder' => 'Choose Gender'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'person_image_headshot_url')->textInput(['maxlength' => true, 'placeholder' => 'Person Image Headshot Url']) */ ?>

    <?php /* echo $form->field($model, 'person_name_nickname')->textInput(['maxlength' => true, 'placeholder' => 'Person Name Nickname']) */ ?>

    <?php /* echo $form->field($model, 'person_date_of_birth')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Person Date Of Birth',
                'autoclose' => true,
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'person_height')->textInput(['maxlength' => true, 'placeholder' => 'Person Height']) */ ?>

    <?php /* echo $form->field($model, 'person_weight')->textInput(['placeholder' => 'Person Weight']) */ ?>

    <?php /* echo $form->field($model, 'person_tshirt_size')->textInput(['maxlength' => true, 'placeholder' => 'Person Tshirt Size']) */ ?>

    <?php /* echo $form->field($model, 'person_high_school__graduation_year')->textInput(['placeholder' => 'Person High School  Graduation Year']) */ ?>

    <?php /* echo $form->field($model, 'person_college_graduation_year')->textInput(['placeholder' => 'Person College Graduation Year']) */ ?>

    <?php /* echo $form->field($model, 'person_college_commitment_status')->textInput(['maxlength' => true, 'placeholder' => 'Person College Commitment Status']) */ ?>

    <?php /* echo $form->field($model, 'person_addr_1')->textInput(['maxlength' => true, 'placeholder' => 'Person Addr 1']) */ ?>

    <?php /* echo $form->field($model, 'person_addr_2')->textInput(['maxlength' => true, 'placeholder' => 'Person Addr 2']) */ ?>

    <?php /* echo $form->field($model, 'person_addr_3')->textInput(['maxlength' => true, 'placeholder' => 'Person Addr 3']) */ ?>

    <?php /* echo $form->field($model, 'person_city')->textInput(['maxlength' => true, 'placeholder' => 'Person City']) */ ?>

    <?php /* echo $form->field($model, 'person_postal_code')->textInput(['maxlength' => true, 'placeholder' => 'Person Postal Code']) */ ?>

    <?php /* echo $form->field($model, 'person_country')->textInput(['maxlength' => true, 'placeholder' => 'Person Country']) */ ?>

    <?php /* echo $form->field($model, 'person_country_code')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy('iso3')->asArray()->all(), 'iso3', 'country'),
        'options' => ['placeholder' => 'Choose Country'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'person_state_or_region')->textInput(['maxlength' => true, 'placeholder' => 'Person State Or Region']) */ ?>

    <?php /* echo $form->field($model, 'org_affiliation_begin_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Org Affiliation Begin Dt',
                'autoclose' => true,
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'org_affiliation_end_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Org Affiliation End Dt',
                'autoclose' => true,
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'person_website')->textInput(['maxlength' => true, 'placeholder' => 'Person Website']) */ ?>

    <?php /* echo $form->field($model, 'person_profile_url')->textInput(['maxlength' => true, 'placeholder' => 'Person Profile Url']) */ ?>

    <?php /* echo $form->field($model, 'person_profile_uri')->textInput(['maxlength' => true, 'placeholder' => 'Person Profile Uri']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
