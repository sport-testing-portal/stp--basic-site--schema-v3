<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrgGoverningBody */

$this->title = 'Save As New Org Governing Body: '. ' ' . $model->org_governing_body_id;
$this->params['breadcrumbs'][] = ['label' => 'Org Governing Bodies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->org_governing_body_id, 'url' => ['view', 'id' => $model->org_governing_body_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="org-governing-body-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
