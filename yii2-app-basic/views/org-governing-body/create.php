<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrgGoverningBody */

$this->title = 'Create Org Governing Body';
$this->params['breadcrumbs'][] = ['label' => 'Org Governing Bodies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-governing-body-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
