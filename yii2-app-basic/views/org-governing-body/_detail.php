<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\OrgGoverningBody */

?>
<div class="org-governing-body-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->org_governing_body_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        //'org_governing_body_id',
        ['attribute' => 'org_governing_body_id', 'visible' => false],
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'governingBody.governing_body',
            'label' => 'Governing Body',
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>