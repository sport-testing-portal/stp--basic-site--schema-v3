<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\OrgGoverningBody */

$this->title = $model->org_governing_body_id;
$this->params['breadcrumbs'][] = ['label' => 'Org Governing Bodies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-governing-body-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Org Governing Body'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->org_governing_body_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->org_governing_body_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->org_governing_body_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'org_governing_body_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'governingBody.governing_body',
            'label' => 'Governing Body',
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>GoverningBody<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnGoverningBody = [
        'sport_id',
        'governing_body',
        'governing_body_short_desc',
        'governing_body_long_desc',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->governingBody,
        'attributes' => $gridColumnGoverningBody    ]);
    ?>
    <div class="row">
        <h4>Org<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrg = [
        'org_type_id',
        'org_level_id',
        'org',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url',
        'org_twitter_url',
        'org_facebook_url',
        'org_phone_main',
        'org_email_main',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->org,
        'attributes' => $gridColumnOrg    ]);
    ?>
</div>
