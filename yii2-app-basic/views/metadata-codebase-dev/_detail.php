<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseDev */

?>
<div class="metadata-codebase-dev-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->codebase_dev_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'codebase_dev_id',
        'codebase_dev',
        'codebase_dev_desc_short',
        'codebase_dev_desc_long',
        'dev_status_tag',
        'dev_scope',
        'rule_file_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>