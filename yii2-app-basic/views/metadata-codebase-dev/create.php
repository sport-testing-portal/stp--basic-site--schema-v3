<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseDev */

$this->title = 'Create Metadata Codebase Dev';
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Devs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-codebase-dev-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
