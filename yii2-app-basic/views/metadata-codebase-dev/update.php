<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseDev */

$this->title = 'Update Metadata Codebase Dev: ' . ' ' . $model->codebase_dev_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Devs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codebase_dev_id, 'url' => ['view', 'id' => $model->codebase_dev_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-codebase-dev-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
