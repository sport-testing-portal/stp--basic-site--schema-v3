<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->metadataCodebaseDevStatuses,
        'key' => 'dev_status_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'dev_status_id',
        [
                'attribute' => 'codebase.codebase_id',
                'label' => 'Codebase'
            ],
        'dev_status',
        'dev_status_tag',
        'dev_status_bfr',
        'dev_status_at',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'metadata-codebase-dev-status'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
