<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->teamPlayers,
        'key' => 'team_player_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'team_player_id',
        [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
        [
                'attribute' => 'teamPlaySportPosition.sport_position',
                'label' => 'Team Play Sport Position'
            ],
        'team_play_sport_position2_id',
        'primary_position',
        'team_play_statistical_highlights',
        'coach_id',
        'coach_name',
        'team_play_current_team',
        'team_play_begin_dt',
        'team_play_end_dt',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'team-player'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
