<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Team */

?>
<div class="team-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->team) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'school.school_id',
            'label' => 'School',
        ],
        [
            'attribute' => 'sport.sport',
            'label' => 'Sport',
        ],
        [
            'attribute' => 'camp.camp',
            'label' => 'Camp',
        ],
        [
            'attribute' => 'gender.gender',
            'label' => 'Gender',
        ],
        [
            'attribute' => 'ageGroup.age_group',
            'label' => 'Age Group',
        ],
        'team',
        'team_gender',
        [
            'attribute' => 'teamDivision.team_division',
            'label' => 'Team Division',
        ],
        [
            'attribute' => 'teamLeague.team_league',
            'label' => 'Team League',
        ],
        'team_division',
        'team_league',
        'organizational_level',
        'team_governing_body',
        'team_age_group',
        'team_website_url:url',
        'team_schedule_url:url',
        'team_schedule_uri',
        'team_statistical_highlights',
        [
            'attribute' => 'teamCompetitionSeason.season',
            'label' => 'Team Competition Season',
        ],
        'team_competition_season',
        'team_city',
        'team_state_id',
        [
            'attribute' => 'teamCountry.country',
            'label' => 'Team Country',
        ],
        'team_wins',
        'team_losses',
        'team_draws',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>