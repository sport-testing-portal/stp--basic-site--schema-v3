<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeamSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-team-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'team_id')->textInput(['placeholder' => 'Team']) ?>

    <?= $form->field($model, 'org_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org'),
        'options' => ['placeholder' => 'Choose Org'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'school_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\School::find()->orderBy('school_id')->asArray()->all(), 'school_id', 'school_id'),
        'options' => ['placeholder' => 'Choose School'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'sport_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Sport::find()->orderBy('sport_id')->asArray()->all(), 'sport_id', 'sport'),
        'options' => ['placeholder' => 'Choose Sport'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'camp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Camp::find()->orderBy('camp_id')->asArray()->all(), 'camp_id', 'camp'),
        'options' => ['placeholder' => 'Choose Camp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php /* echo $form->field($model, 'gender_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->orderBy('gender_id')->asArray()->all(), 'gender_id', 'gender'),
        'options' => ['placeholder' => 'Choose Gender'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'age_group_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\AgeGroup::find()->orderBy('age_group_id')->asArray()->all(), 'age_group_id', 'age_group'),
        'options' => ['placeholder' => 'Choose Age group'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'team')->textInput(['maxlength' => true, 'placeholder' => 'Team']) */ ?>

    <?php /* echo $form->field($model, 'team_gender')->textInput(['maxlength' => true, 'placeholder' => 'Team Gender']) */ ?>

    <?php /* echo $form->field($model, 'team_division_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamDivision::find()->orderBy('team_division_id')->asArray()->all(), 'team_division_id', 'team_division'),
        'options' => ['placeholder' => 'Choose Team division'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'team_league_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamLeague::find()->orderBy('team_league_id')->asArray()->all(), 'team_league_id', 'team_league'),
        'options' => ['placeholder' => 'Choose Team league'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'team_division')->textInput(['maxlength' => true, 'placeholder' => 'Team Division']) */ ?>

    <?php /* echo $form->field($model, 'team_league')->textInput(['maxlength' => true, 'placeholder' => 'Team League']) */ ?>

    <?php /* echo $form->field($model, 'organizational_level')->textInput(['maxlength' => true, 'placeholder' => 'Organizational Level']) */ ?>

    <?php /* echo $form->field($model, 'team_governing_body')->textInput(['maxlength' => true, 'placeholder' => 'Team Governing Body']) */ ?>

    <?php /* echo $form->field($model, 'team_age_group')->textInput(['maxlength' => true, 'placeholder' => 'Team Age Group']) */ ?>

    <?php /* echo $form->field($model, 'team_website_url')->textInput(['maxlength' => true, 'placeholder' => 'Team Website Url']) */ ?>

    <?php /* echo $form->field($model, 'team_schedule_url')->textInput(['maxlength' => true, 'placeholder' => 'Team Schedule Url']) */ ?>

    <?php /* echo $form->field($model, 'team_schedule_uri')->textInput(['maxlength' => true, 'placeholder' => 'Team Schedule Uri']) */ ?>

    <?php /* echo $form->field($model, 'team_statistical_highlights')->textInput(['maxlength' => true, 'placeholder' => 'Team Statistical Highlights']) */ ?>

    <?php /* echo $form->field($model, 'team_competition_season_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Season::find()->orderBy('season_id')->asArray()->all(), 'season_id', 'season'),
        'options' => ['placeholder' => 'Choose Season'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'team_competition_season')->textInput(['maxlength' => true, 'placeholder' => 'Team Competition Season']) */ ?>

    <?php /* echo $form->field($model, 'team_city')->textInput(['maxlength' => true, 'placeholder' => 'Team City']) */ ?>

    <?php /* echo $form->field($model, 'team_state_id')->textInput(['placeholder' => 'Team State']) */ ?>

    <?php /* echo $form->field($model, 'team_country_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy('country_id')->asArray()->all(), 'country_id', 'country'),
        'options' => ['placeholder' => 'Choose Country'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'team_wins')->textInput(['placeholder' => 'Team Wins']) */ ?>

    <?php /* echo $form->field($model, 'team_losses')->textInput(['placeholder' => 'Team Losses']) */ ?>

    <?php /* echo $form->field($model, 'team_draws')->textInput(['placeholder' => 'Team Draws']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
