<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\TeamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Teams';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="team-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Team', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        'team_id',
        [
                'attribute' => 'org_id',
                'label' => 'Org',
                'value' => function($model){
                    if ($model->org)
                    {return $model->org->org;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->asArray()->all(), 'org_id', 'org'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Org', 'id' => 'grid-team-search-org_id']
            ],
        [
                'attribute' => 'school_id',
                'label' => 'School',
                'value' => function($model){
                    if ($model->school)
                    {return $model->school->school_id;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\School::find()->asArray()->all(), 'school_id', 'school_id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'School', 'id' => 'grid-team-search-school_id']
            ],
        [
                'attribute' => 'sport_id',
                'label' => 'Sport',
                'value' => function($model){
                    if ($model->sport)
                    {return $model->sport->sport;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Sport::find()->asArray()->all(), 'sport_id', 'sport'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Sport', 'id' => 'grid-team-search-sport_id']
            ],
        [
                'attribute' => 'camp_id',
                'label' => 'Camp',
                'value' => function($model){
                    if ($model->camp)
                    {return $model->camp->camp;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Camp::find()->asArray()->all(), 'camp_id', 'camp'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Camp', 'id' => 'grid-team-search-camp_id']
            ],
        [
                'attribute' => 'gender_id',
                'label' => 'Gender',
                'value' => function($model){
                    if ($model->gender)
                    {return $model->gender->gender;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->asArray()->all(), 'gender_id', 'gender'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Gender', 'id' => 'grid-team-search-gender_id']
            ],
        [
                'attribute' => 'age_group_id',
                'label' => 'Age Group',
                'value' => function($model){
                    if ($model->ageGroup)
                    {return $model->ageGroup->age_group;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\AgeGroup::find()->asArray()->all(), 'age_group_id', 'age_group'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Age group', 'id' => 'grid-team-search-age_group_id']
            ],
        'team',
        'team_gender',
        [
                'attribute' => 'team_division_id',
                'label' => 'Team Division',
                'value' => function($model){
                    if ($model->teamDivision)
                    {return $model->teamDivision->team_division;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\TeamDivision::find()->asArray()->all(), 'team_division_id', 'team_division'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Team division', 'id' => 'grid-team-search-team_division_id']
            ],
        [
                'attribute' => 'team_league_id',
                'label' => 'Team League',
                'value' => function($model){
                    if ($model->teamLeague)
                    {return $model->teamLeague->team_league;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\TeamLeague::find()->asArray()->all(), 'team_league_id', 'team_league'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Team league', 'id' => 'grid-team-search-team_league_id']
            ],
        'team_division',
        'team_league',
        'organizational_level',
        'team_governing_body',
        'team_age_group',
        'team_website_url:url',
        'team_schedule_url:url',
        'team_schedule_uri',
        'team_statistical_highlights',
        [
                'attribute' => 'team_competition_season_id',
                'label' => 'Team Competition Season',
                'value' => function($model){
                    if ($model->teamCompetitionSeason)
                    {return $model->teamCompetitionSeason->season;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Season::find()->asArray()->all(), 'season_id', 'season'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Season', 'id' => 'grid-team-search-team_competition_season_id']
            ],
        'team_competition_season',
        'team_city',
        'team_state_id',
        [
                'attribute' => 'team_country_id',
                'label' => 'Team Country',
                'value' => function($model){
                    if ($model->teamCountry)
                    {return $model->teamCountry->country;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->asArray()->all(), 'country_id', 'country'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Country', 'id' => 'grid-team-search-team_country_id']
            ],
        'team_wins',
        'team_losses',
        'team_draws',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
