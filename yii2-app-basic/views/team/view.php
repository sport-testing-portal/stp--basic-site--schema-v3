<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Team */

$this->title = $model->team;
$this->params['breadcrumbs'][] = ['label' => 'Teams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Team'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->team_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->team_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->team_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'school.school_id',
            'label' => 'School',
        ],
        [
            'attribute' => 'sport.sport',
            'label' => 'Sport',
        ],
        [
            'attribute' => 'camp.camp',
            'label' => 'Camp',
        ],
        [
            'attribute' => 'gender.gender',
            'label' => 'Gender',
        ],
        [
            'attribute' => 'ageGroup.age_group',
            'label' => 'Age Group',
        ],
        'team',
        'team_gender',
        [
            'attribute' => 'teamDivision.team_division',
            'label' => 'Team Division',
        ],
        [
            'attribute' => 'teamLeague.team_league',
            'label' => 'Team League',
        ],
        'team_division',
        'team_league',
        'organizational_level',
        'team_governing_body',
        'team_age_group',
        'team_website_url:url',
        'team_schedule_url:url',
        'team_schedule_uri',
        'team_statistical_highlights',
        [
            'attribute' => 'teamCompetitionSeason.season',
            'label' => 'Team Competition Season',
        ],
        'team_competition_season',
        'team_city',
        'team_state_id',
        [
            'attribute' => 'teamCountry.country',
            'label' => 'Team Country',
        ],
        'team_wins',
        'team_losses',
        'team_draws',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerPlayerTeam->totalCount){
    $gridColumnPlayerTeam = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_team_id',
            [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
                        'begin_dt',
            'end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerTeam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Team'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerTeam
    ]);
}
?>

    </div>
    <div class="row">
        <h4>AgeGroup<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAgeGroup = [
        'sport_id',
        'age_group',
        'age_group_desc',
        'age_group_min',
        'age_group_max',
        'display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->ageGroup,
        'attributes' => $gridColumnAgeGroup    ]);
    ?>
    <div class="row">
        <h4>Camp<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCamp = [
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        'camp',
        'camp_specialty',
        'camp_cost_regular',
        'camp_cost_early_registration',
        'camp_team_discounts_available_yn',
        'camp_scholarships_available_yn',
        'camp_session_desc',
        'camp_website_url',
        'camp_session_url',
        'camp_registration_url',
        'camp_twitter_url',
        'camp_facebook_url',
        'camp_scholarship_application_info',
        'camp_scholarship_application_request',
        'camp_organizer_description',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->camp,
        'attributes' => $gridColumnCamp    ]);
    ?>
    <div class="row">
        <h4>Country<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCountry = [
        'country_id',
        'iso2',
        'country',
        'long_name',
        'iso3',
        'numcode',
        'un_member',
        'calling_code',
        'cctld',
    ];
    echo DetailView::widget([
        'model' => $model->teamCountry,
        'attributes' => $gridColumnCountry    ]);
    ?>
    <div class="row">
        <h4>TeamDivision<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnTeamDivision = [
        'team_division',
        'team_division_desc_short',
        'team_division_desc_long',
        'team_division_display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->teamDivision,
        'attributes' => $gridColumnTeamDivision    ]);
    ?>
    <div class="row">
        <h4>Gender<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnGender = [
        'gender',
        'gender_code',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->gender,
        'attributes' => $gridColumnGender    ]);
    ?>
    <div class="row">
        <h4>TeamLeague<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnTeamLeague = [
        'team_league',
        'team_league_desc_short',
        'team_league_desc_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->teamLeague,
        'attributes' => $gridColumnTeamLeague    ]);
    ?>
    <div class="row">
        <h4>Org<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrg = [
        'org_type_id',
        'org_level_id',
        'org',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url',
        'org_twitter_url',
        'org_facebook_url',
        'org_phone_main',
        'org_email_main',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->org,
        'attributes' => $gridColumnOrg    ]);
    ?>
    <div class="row">
        <h4>School<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnSchool = [
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        'religious_affiliation_id',
        'conference_id__female',
        'conference_id__male',
        'conference_id__main',
        'school_ipeds_id',
        'school_unit_id',
        'school_ope_id',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->school,
        'attributes' => $gridColumnSchool    ]);
    ?>
    <div class="row">
        <h4>Season<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnSeason = [
        'season_id',
        'season',
        'season_display_order',
        'season_comment',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->teamCompetitionSeason,
        'attributes' => $gridColumnSeason    ]);
    ?>
    <div class="row">
        <h4>Sport<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnSport = [
        'sport',
        'sport_desc_short',
        'sport_desc_long',
        'high_school_yn',
        'ncaa_yn',
        'olympic_yn',
        'xgame_sport_yn',
        'ncaa_sport_name',
        'olympic_sport_name',
        'xgame_sport_name',
        'ncaa_sport_type',
        'ncaa_sport_season_male',
        'ncaa_sport_season_female',
        'ncaa_sport_season_coed',
        'olympic_sport_season',
        'xgame_sport_season',
        'olympic_sport_gender',
        'xgame_sport_gender',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->sport,
        'attributes' => $gridColumnSport    ]);
    ?>
    
    <div class="row">
<?php
if($providerTeamCoach->totalCount){
    $gridColumnTeamCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_coach_id',
                        [
                'attribute' => 'coach.coach_id',
                'label' => 'Coach'
            ],
            'team_coach_coach_type_id',
            'primary_position',
            'team_coach_begin_dt',
            'team_coach_end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeamCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnTeamCoach
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTeamPlayer->totalCount){
    $gridColumnTeamPlayer = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_player_id',
                        [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
            [
                'attribute' => 'teamPlaySportPosition.sport_position',
                'label' => 'Team Play Sport Position'
            ],
            'team_play_sport_position2_id',
            'primary_position',
            'team_play_statistical_highlights',
            'coach_id',
            'coach_name',
            'team_play_current_team',
            'team_play_begin_dt',
            'team_play_end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeamPlayer,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team-player']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team Player'),
        ],
        'export' => false,
        'columns' => $gridColumnTeamPlayer
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTestEvalSummaryLog->totalCount){
    $gridColumnTestEvalSummaryLog = [
        ['class' => 'yii\grid\SerialColumn'],
            'test_eval_summary_log_id',
            [
                'attribute' => 'person.person',
                'label' => 'Person'
            ],
                        [
                'attribute' => 'testEvalType.test_eval_type',
                'label' => 'Test Eval Type'
            ],
            'source_event_id',
            'test_eval_overall_ranking',
            'test_eval_positional_ranking',
            'test_eval_total_score',
            'test_eval_avg_score',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTestEvalSummaryLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-test-eval-summary-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Test Eval Summary Log'),
        ],
        'export' => false,
        'columns' => $gridColumnTestEvalSummaryLog
    ]);
}
?>

    </div>
</div>
