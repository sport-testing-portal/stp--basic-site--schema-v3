<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerTeam', 
        'relID' => 'player-team', 
        'value' => \yii\helpers\Json::encode($model->playerTeams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'TeamCoach', 
        'relID' => 'team-coach', 
        'value' => \yii\helpers\Json::encode($model->teamCoaches),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'TeamPlayer', 
        'relID' => 'team-player', 
        'value' => \yii\helpers\Json::encode($model->teamPlayers),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'TestEvalSummaryLog', 
        'relID' => 'test-eval-summary-log', 
        'value' => \yii\helpers\Json::encode($model->testEvalSummaryLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="team-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'team_id')->textInput(['placeholder' => 'Team']) ?>

    <?= $form->field($model, 'org_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org'),
        'options' => ['placeholder' => 'Choose Org'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'school_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\School::find()->orderBy('school_id')->asArray()->all(), 'school_id', 'school_id'),
        'options' => ['placeholder' => 'Choose School'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'sport_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Sport::find()->orderBy('sport_id')->asArray()->all(), 'sport_id', 'sport'),
        'options' => ['placeholder' => 'Choose Sport'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'camp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Camp::find()->orderBy('camp_id')->asArray()->all(), 'camp_id', 'camp'),
        'options' => ['placeholder' => 'Choose Camp'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'gender_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->orderBy('gender_id')->asArray()->all(), 'gender_id', 'gender'),
        'options' => ['placeholder' => 'Choose Gender'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'age_group_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\AgeGroup::find()->orderBy('age_group_id')->asArray()->all(), 'age_group_id', 'age_group'),
        'options' => ['placeholder' => 'Choose Age group'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'team')->textInput(['maxlength' => true, 'placeholder' => 'Team']) ?>

    <?= $form->field($model, 'team_gender')->textInput(['maxlength' => true, 'placeholder' => 'Team Gender']) ?>

    <?= $form->field($model, 'team_division_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamDivision::find()->orderBy('team_division_id')->asArray()->all(), 'team_division_id', 'team_division'),
        'options' => ['placeholder' => 'Choose Team division'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'team_league_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamLeague::find()->orderBy('team_league_id')->asArray()->all(), 'team_league_id', 'team_league'),
        'options' => ['placeholder' => 'Choose Team league'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'team_division')->textInput(['maxlength' => true, 'placeholder' => 'Team Division']) ?>

    <?= $form->field($model, 'team_league')->textInput(['maxlength' => true, 'placeholder' => 'Team League']) ?>

    <?= $form->field($model, 'organizational_level')->textInput(['maxlength' => true, 'placeholder' => 'Organizational Level']) ?>

    <?= $form->field($model, 'team_governing_body')->textInput(['maxlength' => true, 'placeholder' => 'Team Governing Body']) ?>

    <?= $form->field($model, 'team_age_group')->textInput(['maxlength' => true, 'placeholder' => 'Team Age Group']) ?>

    <?= $form->field($model, 'team_website_url')->textInput(['maxlength' => true, 'placeholder' => 'Team Website Url']) ?>

    <?= $form->field($model, 'team_schedule_url')->textInput(['maxlength' => true, 'placeholder' => 'Team Schedule Url']) ?>

    <?= $form->field($model, 'team_schedule_uri')->textInput(['maxlength' => true, 'placeholder' => 'Team Schedule Uri']) ?>

    <?= $form->field($model, 'team_statistical_highlights')->textInput(['maxlength' => true, 'placeholder' => 'Team Statistical Highlights']) ?>

    <?= $form->field($model, 'team_competition_season_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Season::find()->orderBy('season_id')->asArray()->all(), 'season_id', 'season'),
        'options' => ['placeholder' => 'Choose Season'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'team_competition_season')->textInput(['maxlength' => true, 'placeholder' => 'Team Competition Season']) ?>

    <?= $form->field($model, 'team_city')->textInput(['maxlength' => true, 'placeholder' => 'Team City']) ?>

    <?= $form->field($model, 'team_state_id')->textInput(['placeholder' => 'Team State']) ?>

    <?= $form->field($model, 'team_country_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy('country_id')->asArray()->all(), 'country_id', 'country'),
        'options' => ['placeholder' => 'Choose Country'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'team_wins')->textInput(['placeholder' => 'Team Wins']) ?>

    <?= $form->field($model, 'team_losses')->textInput(['placeholder' => 'Team Losses']) ?>

    <?= $form->field($model, 'team_draws')->textInput(['placeholder' => 'Team Draws']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerTeam'),
            'content' => $this->render('_formPlayerTeam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerTeams),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('TeamCoach'),
            'content' => $this->render('_formTeamCoach', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teamCoaches),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('TeamPlayer'),
            'content' => $this->render('_formTeamPlayer', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teamPlayers),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('TestEvalSummaryLog'),
            'content' => $this->render('_formTestEvalSummaryLog', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->testEvalSummaryLogs),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
