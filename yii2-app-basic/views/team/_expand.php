<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Team'),
        'content' => $this->render('_dataPlayerTeam', [
            'model' => $model,
            'row' => $model->playerTeams,
        ]),
    ],
                                                    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team Coach'),
        'content' => $this->render('_dataTeamCoach', [
            'model' => $model,
            'row' => $model->teamCoaches,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team Player'),
        'content' => $this->render('_dataTeamPlayer', [
            'model' => $model,
            'row' => $model->teamPlayers,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Test Eval Summary Log'),
        'content' => $this->render('_dataTestEvalSummaryLog', [
            'model' => $model,
            'row' => $model->testEvalSummaryLogs,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
