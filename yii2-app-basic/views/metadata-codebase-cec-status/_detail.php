<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseCecStatus */

?>
<div class="metadata-codebase-cec-status-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->cec_status_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'cec_status_id',
        [
            'attribute' => 'codebase.codebase_id',
            'label' => 'Codebase',
        ],
        [
            'attribute' => 'codebaseCec.codebase_cec_id',
            'label' => 'Codebase Cec',
        ],
        'cec_status',
        'cec_status_tag',
        'cec_status_bfr',
        'cec_status_at',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>