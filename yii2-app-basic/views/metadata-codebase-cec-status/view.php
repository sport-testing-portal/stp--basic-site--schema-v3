<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseCecStatus */

$this->title = $model->cec_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Cec Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-codebase-cec-status-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Metadata Codebase Cec Status'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->cec_status_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->cec_status_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->cec_status_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'cec_status_id',
        [
            'attribute' => 'codebase.codebase_id',
            'label' => 'Codebase',
        ],
        [
            'attribute' => 'codebaseCec.codebase_cec_id',
            'label' => 'Codebase Cec',
        ],
        'cec_status',
        'cec_status_tag',
        'cec_status_bfr',
        'cec_status_at',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MetadataCodebase<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataCodebase = [
        'file_name',
        'file_path_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->codebase,
        'attributes' => $gridColumnMetadataCodebase    ]);
    ?>
    <div class="row">
        <h4>MetadataCodebaseCec<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataCodebaseCec = [
        'codebase_cec',
        'codebase_cec_desc_short',
        'codebase_cec_desc_long',
        'cec_status_tag',
        'cec_scope',
        'rule_file_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->codebaseCec,
        'attributes' => $gridColumnMetadataCodebaseCec    ]);
    ?>
</div>
