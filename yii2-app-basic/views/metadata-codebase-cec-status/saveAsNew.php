<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseCecStatus */

$this->title = 'Save As New Metadata Codebase Cec Status: '. ' ' . $model->cec_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Cec Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cec_status_id, 'url' => ['view', 'id' => $model->cec_status_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-codebase-cec-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
