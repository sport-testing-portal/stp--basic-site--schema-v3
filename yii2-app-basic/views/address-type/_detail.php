<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\AddressType */

?>
<div class="address-type-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->address_type) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'address_type_id',
        'address_type',
        'address_type_description_short',
        'address_type_description_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>