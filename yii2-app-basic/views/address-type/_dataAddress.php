<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->addresses,
        'key' => 'address_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'address_id',
        [
                'attribute' => 'org.org',
                'label' => 'Org'
            ],
        [
                'attribute' => 'person.person',
                'label' => 'Person'
            ],
        'addr1',
        'addr2',
        'addr3',
        'city',
        'state_or_region',
        'postal_code',
        'country',
        'country_code',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'address'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
