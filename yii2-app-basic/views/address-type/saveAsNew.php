<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AddressType */

$this->title = 'Save As New Address Type: '. ' ' . $model->address_type;
$this->params['breadcrumbs'][] = ['label' => 'Address Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->address_type, 'url' => ['view', 'id' => $model->address_type_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="address-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
