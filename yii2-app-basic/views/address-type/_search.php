<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddressTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-address-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'address_type_id')->textInput(['placeholder' => 'Address Type']) ?>

    <?= $form->field($model, 'address_type')->textInput(['maxlength' => true, 'placeholder' => 'Address Type']) ?>

    <?= $form->field($model, 'address_type_description_short')->textInput(['maxlength' => true, 'placeholder' => 'Address Type Description Short']) ?>

    <?= $form->field($model, 'address_type_description_long')->textInput(['maxlength' => true, 'placeholder' => 'Address Type Description Long']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
