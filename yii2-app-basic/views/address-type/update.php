<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AddressType */

$this->title = 'Update Address Type: ' . ' ' . $model->address_type;
$this->params['breadcrumbs'][] = ['label' => 'Address Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->address_type, 'url' => ['view', 'id' => $model->address_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="address-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
