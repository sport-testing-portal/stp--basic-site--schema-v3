<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseFunction */

$this->title = 'Save As New Metadata Codebase Function: '. ' ' . $model->codebase_function_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Functions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codebase_function_id, 'url' => ['view', 'id' => $model->codebase_function_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-codebase-function-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
