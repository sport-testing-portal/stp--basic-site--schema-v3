<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseFunction */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="metadata-codebase-function-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'codebase_function_id')->textInput(['placeholder' => 'Codebase Function']) ?>

    <?= $form->field($model, 'codebase_function')->textInput(['maxlength' => true, 'placeholder' => 'Codebase Function']) ?>

    <?= $form->field($model, 'codebase_function_short_desc')->textInput(['maxlength' => true, 'placeholder' => 'Codebase Function Short Desc']) ?>

    <?= $form->field($model, 'codebase_function_long_desc')->textInput(['maxlength' => true, 'placeholder' => 'Codebase Function Long Desc']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
