<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseFunctionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-codebase-function-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codebase_function_id')->textInput(['placeholder' => 'Codebase Function']) ?>

    <?= $form->field($model, 'codebase_function')->textInput(['maxlength' => true, 'placeholder' => 'Codebase Function']) ?>

    <?= $form->field($model, 'codebase_function_short_desc')->textInput(['maxlength' => true, 'placeholder' => 'Codebase Function Short Desc']) ?>

    <?= $form->field($model, 'codebase_function_long_desc')->textInput(['maxlength' => true, 'placeholder' => 'Codebase Function Long Desc']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
