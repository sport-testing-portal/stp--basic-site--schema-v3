<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlationItem */

?>
<div class="data-hdr-xlation-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->data_hdr_xlation_item_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
/*
    $gridColumn = [
        'data_hdr_xlation_item_id',
        [
            'attribute' => 'dataHdrXlation.data_hdr_xlation',
            'label' => 'Data Hdr Xlation',
        ],
        'data_hdr_xlation_item_source_value',
        'data_hdr_xlation_item_target_value',
        'ordinal_position_num',
        'data_hdr_xlation_item_target_table',
        'data_hdr_source_value_type_name_source',
        'data_hdr_source_value_is_type_name',
        ['attribute' => 'lock', 'visible' => false],
    ];

 * 
 */    
    
    $gridColumn = [
        //'data_hdr_xlation_item_id',
        [
            'attribute'=>'data_hdr_xlation_item_id',
            'label'=>'ID',
           // 'width'=>'50px'
        ],
        [
            'attribute' => 'dataHdrXlation.data_hdr_xlation',
            'label' => 'Data Hdr Xlation Name',
        ],
        //'data_hdr_xlation_item_source_value',
        [
            'attribute'=>'data_hdr_xlation_item_source_value',
            'label'=>'Source Value',
         //   'width'=>'70px'
        ],            
        //'data_hdr_xlation_item_target_value',
        [
            'attribute'=>'data_hdr_xlation_item_target_value',
            'label'=>'Target Value',
          //  'width'=>'70px'
        ],            
        //'ordinal_position_num',
        [
            'attribute'=>'ordinal_position_num',
            'label'=>'Ordinal Pos#',
          //  'width'=>'50px'
        ],            
        //'data_hdr_xlation_item_target_table',
        [
            'attribute'=>'data_hdr_xlation_item_target_table',
            'label'=>'Target Table',
        //    'width'=>'70px'
        ],            
        //'data_hdr_source_value_type_name_source',
        [
            'attribute'=>'data_hdr_source_value_type_name_source',
            'label'=>'SrcVal Type Name Source',
         //   'width'=>'60px'
        ],            
        //'data_hdr_source_value_is_type_name',
        [
            'attribute'=>'data_hdr_source_value_is_type_name',
            'label'=>'SrcVal Is Type Name',
         //   'width'=>'50px'
        ],    
    ];
    
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>