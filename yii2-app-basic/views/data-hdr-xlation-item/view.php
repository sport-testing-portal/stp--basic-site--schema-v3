<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlationItem */

$this->title = $model->data_hdr_xlation_item_id;
$this->params['breadcrumbs'][] = ['label' => 'Data Hdr Xlation Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-hdr-xlation-item-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Data Hdr Xlation Item'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->data_hdr_xlation_item_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->data_hdr_xlation_item_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->data_hdr_xlation_item_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'data_hdr_xlation_item_id',
        [
            'attribute' => 'dataHdrXlation.data_hdr_xlation',
            'label' => 'Data Hdr Xlation',
        ],
        'data_hdr_xlation_item_source_value',
        'data_hdr_xlation_item_target_value',
        'ordinal_position_num',
        'data_hdr_xlation_item_target_table',
        'data_hdr_source_value_type_name_source',
        'data_hdr_source_value_is_type_name',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>DataHdrXlation<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnDataHdrXlation = [
        'data_hdr_xlation',
        'data_hdr_entity_name',
        'hdr_sha1_hash',
        'data_hdr_regex',
        'data_hdr_is_trash_yn',
        'gsm_target_table_name',
        'data_hdr_entity_fetch_url',
        'data_hdr_xlation_visual_example',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->dataHdrXlation,
        'attributes' => $gridColumnDataHdrXlation    ]);
    ?>
</div>
