<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlationItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-data-hdr-xlation-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'data_hdr_xlation_item_id')->textInput(['placeholder' => 'Data Hdr Xlation Item']) ?>

    <?= $form->field($model, 'data_hdr_xlation_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\DataHdrXlation::find()->orderBy('data_hdr_xlation_id')->asArray()->all(), 'data_hdr_xlation_id', 'data_hdr_xlation'),
        'options' => ['placeholder' => 'Choose Data hdr xlation'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'data_hdr_xlation_item_source_value')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation Item Source Value']) ?>

    <?= $form->field($model, 'data_hdr_xlation_item_target_value')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation Item Target Value']) ?>

    <?= $form->field($model, 'ordinal_position_num')->textInput(['placeholder' => 'Ordinal Position Num']) ?>

    <?php /* echo $form->field($model, 'data_hdr_xlation_item_target_table')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation Item Target Table']) */ ?>

    <?php /* echo $form->field($model, 'data_hdr_source_value_type_name_source')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Source Value Type Name Source']) */ ?>

    <?php /* echo $form->field($model, 'data_hdr_source_value_is_type_name')->textInput(['placeholder' => 'Data Hdr Source Value Is Type Name']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
