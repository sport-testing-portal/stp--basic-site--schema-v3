<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\DataHdrXlationItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Data Column Mapping ([Src-Column to table-field])';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="data-hdr-xlation-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Data Hdr Xlation Item', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
  //      'data_hdr_xlation_item_id',
        [
            'attribute'=>'data_hdr_xlation_item_id',
            'label'=>'ID',
            'width'=>'50px'
        ],        
        [
                'attribute' => 'data_hdr_xlation_id',
                'label' => 'Data Hdr Xlation',
                'value' => function($model){
                    if ($model->dataHdrXlation)
                    {return $model->dataHdrXlation->data_hdr_xlation;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\DataHdrXlation::find()->asArray()->all(), 'data_hdr_xlation_id', 'data_hdr_xlation'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true, 'dropdownAutoWidth' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Data hdr xlation', 'id' => 'grid-data-hdr-xlation-item-search-data_hdr_xlation_id']
            ],
        //'data_hdr_xlation_item_source_value',
        [
            'attribute'=>'data_hdr_xlation_item_source_value',
            'label'=>'Source Value',
            'width'=>'100px'
        ],            
        //'data_hdr_xlation_item_target_value',
        [
            'attribute'=>'data_hdr_xlation_item_target_value',
            'label'=>'Target Value',
            'width'=>'70px'
        ],            
        //'ordinal_position_num',
        [
            'attribute'=>'ordinal_position_num',
            'label'=>'Ordinal Position',
            'width'=>'50px'
        ],            
        //'data_hdr_xlation_item_target_table',
        [
            'attribute'=>'data_hdr_xlation_item_target_table',
            'label'=>'Target Table',
            'width'=>'80px'
        ],            
        //'data_hdr_source_value_type_name_source',
//        [
//            'attribute'=>'data_hdr_source_value_type_name_source',
//            'label'=>'SrcVal Type Name Source',
//            'width'=>'60px'
//        ],            
        //'data_hdr_source_value_is_type_name',
//        [
//            'attribute'=>'data_hdr_source_value_is_type_name',
//            'label'=>'SrcVal Is Type Name',
//            'width'=>'50px'
//        ],            
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
           //'width'=>'60px'         
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-data-hdr-xlation-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
