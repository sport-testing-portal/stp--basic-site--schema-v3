<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlationItem */

$this->title = 'Update Data Hdr Xlation Item: ' . ' ' . $model->data_hdr_xlation_item_id;
$this->params['breadcrumbs'][] = ['label' => 'Data Hdr Xlation Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->data_hdr_xlation_item_id, 'url' => ['view', 'id' => $model->data_hdr_xlation_item_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="data-hdr-xlation-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
