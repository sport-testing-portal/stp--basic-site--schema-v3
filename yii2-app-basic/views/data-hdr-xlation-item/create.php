<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlationItem */

$this->title = 'Create Data Hdr Xlation Item';
$this->params['breadcrumbs'][] = ['label' => 'Data Hdr Xlation Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-hdr-xlation-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
