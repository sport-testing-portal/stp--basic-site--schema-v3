<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlationItem */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="data-hdr-xlation-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'data_hdr_xlation_item_id')->textInput(['placeholder' => 'Data Hdr Xlation Item']) ?>

    <?= $form->field($model, 'data_hdr_xlation_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\DataHdrXlation::find()->orderBy('data_hdr_xlation_id')->asArray()->all(), 'data_hdr_xlation_id', 'data_hdr_xlation'),
        'options' => ['placeholder' => 'Choose Data hdr xlation'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'data_hdr_xlation_item_source_value')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation Item Source Value']) ?>

    <?= $form->field($model, 'data_hdr_xlation_item_target_value')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation Item Target Value']) ?>

    <?= $form->field($model, 'ordinal_position_num')->textInput(['placeholder' => 'Ordinal Position Num']) ?>

    <?= $form->field($model, 'data_hdr_xlation_item_target_table')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation Item Target Table']) ?>

    <?= $form->field($model, 'data_hdr_source_value_type_name_source')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Source Value Type Name Source']) ?>

    <?= $form->field($model, 'data_hdr_source_value_is_type_name')->textInput(['placeholder' => 'Data Hdr Source Value Is Type Name']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
