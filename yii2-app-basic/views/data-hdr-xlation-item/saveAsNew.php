<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlationItem */

$this->title = 'Save As New Data Hdr Xlation Item: '. ' ' . $model->data_hdr_xlation_item_id;
$this->params['breadcrumbs'][] = ['label' => 'Data Hdr Xlation Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->data_hdr_xlation_item_id, 'url' => ['view', 'id' => $model->data_hdr_xlation_item_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="data-hdr-xlation-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
