<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Player */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampSessionPlayerEval', 
        'relID' => 'camp-session-player-eval', 
        'value' => \yii\helpers\Json::encode($model->campSessionPlayerEvals),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerAcademic', 
        'relID' => 'player-academic', 
        'value' => \yii\helpers\Json::encode($model->playerAcademics),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerCampLog', 
        'relID' => 'player-camp-log', 
        'value' => \yii\helpers\Json::encode($model->playerCampLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerCollegeRecruit', 
        'relID' => 'player-college-recruit', 
        'value' => \yii\helpers\Json::encode($model->playerCollegeRecruits),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerContact', 
        'relID' => 'player-contact', 
        'value' => \yii\helpers\Json::encode($model->playerContacts),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerSchool', 
        'relID' => 'player-school', 
        'value' => \yii\helpers\Json::encode($model->playerSchools),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerSport', 
        'relID' => 'player-sport', 
        'value' => \yii\helpers\Json::encode($model->playerSports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerTeam', 
        'relID' => 'player-team', 
        'value' => \yii\helpers\Json::encode($model->playerTeams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'TeamPlayer', 
        'relID' => 'team-player', 
        'value' => \yii\helpers\Json::encode($model->teamPlayers),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="player-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'player_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamPlayer::find()->orderBy('player_id')->asArray()->all(), 'player_id', 'team_player_id'),
        'options' => ['placeholder' => 'Choose Team player'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'person_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Person::find()->orderBy('person_id')->asArray()->all(), 'person_id', 'person'),
        'options' => ['placeholder' => 'Choose Person'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'age_group_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\AgeGroup::find()->orderBy('age_group_id')->asArray()->all(), 'age_group_id', 'age_group'),
        'options' => ['placeholder' => 'Choose Age group'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'player_team_player_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamPlayer::find()->orderBy('team_player_id')->asArray()->all(), 'team_player_id', 'team_player_id'),
        'options' => ['placeholder' => 'Choose Team player'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'player_sport_position_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\SportPosition::find()->orderBy('sport_position_id')->asArray()->all(), 'sport_position_id', 'sport_position'),
        'options' => ['placeholder' => 'Choose Sport position'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'player_sport_position2_id')->textInput(['placeholder' => 'Player Sport Position2']) ?>

    <?= $form->field($model, 'player_access_code')->textInput(['maxlength' => true, 'placeholder' => 'Player Access Code']) ?>

    <?= $form->field($model, 'player_waiver_minor_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Player Waiver Minor Dt',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'player_waiver_adult_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Player Waiver Adult Dt',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'player_parent_email')->textInput(['maxlength' => true, 'placeholder' => 'Player Parent Email']) ?>

    <?= $form->field($model, 'player_sport_preference')->textInput(['maxlength' => true, 'placeholder' => 'Player Sport Preference']) ?>

    <?= $form->field($model, 'player_sport_position_preference')->textInput(['maxlength' => true, 'placeholder' => 'Player Sport Position Preference']) ?>

    <?= $form->field($model, 'player_shot_side_preference')->textInput(['maxlength' => true, 'placeholder' => 'Player Shot Side Preference']) ?>

    <?= $form->field($model, 'player_dominant_side')->textInput(['maxlength' => true, 'placeholder' => 'Player Dominant Side']) ?>

    <?= $form->field($model, 'player_dominant_foot')->textInput(['maxlength' => true, 'placeholder' => 'Player Dominant Foot']) ?>

    <?= $form->field($model, 'player_statistical_highlights')->textInput(['maxlength' => true, 'placeholder' => 'Player Statistical Highlights']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampSessionPlayerEval'),
            'content' => $this->render('_formCampSessionPlayerEval', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campSessionPlayerEvals),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerAcademic'),
            'content' => $this->render('_formPlayerAcademic', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerAcademics),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerCampLog'),
            'content' => $this->render('_formPlayerCampLog', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerCampLogs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerCollegeRecruit'),
            'content' => $this->render('_formPlayerCollegeRecruit', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerCollegeRecruits),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerContact'),
            'content' => $this->render('_formPlayerContact', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerContacts),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerSchool'),
            'content' => $this->render('_formPlayerSchool', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerSchools),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerSport'),
            'content' => $this->render('_formPlayerSport', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerSports),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerTeam'),
            'content' => $this->render('_formPlayerTeam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerTeams),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('TeamPlayer'),
            'content' => $this->render('_formTeamPlayer', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teamPlayers),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
