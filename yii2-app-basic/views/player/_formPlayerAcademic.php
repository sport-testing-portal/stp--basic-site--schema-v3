<div class="form-group" id="add-player-academic">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'PlayerAcademic',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'player_academic_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'sat_composite_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'sat_critical_reading_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'sat_math_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'sat_writing_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'sat_scheduled_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Sat Scheduled Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'psat_composite_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'psat_critical_reading_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'psat_math_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'psat_writing_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'act_composite_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'act_math_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'act_english_score_max' => ['type' => TabularForm::INPUT_TEXT],
        'act_scheduled_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Act Scheduled Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'grade_point_average' => ['type' => TabularForm::INPUT_TEXT],
        'class_rank' => ['type' => TabularForm::INPUT_TEXT],
        'school_size' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowPlayerAcademic(' . $key . '); return false;', 'id' => 'player-academic-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Player Academic', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowPlayerAcademic()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

