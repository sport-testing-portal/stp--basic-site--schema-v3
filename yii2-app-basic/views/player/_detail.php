<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Player */

?>
<div class="player-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->player_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'teamPlayers.team_player_id',
            'label' => 'Player',
        ],
        [
            'attribute' => 'person.person',
            'label' => 'Person',
        ],
        [
            'attribute' => 'ageGroup.age_group',
            'label' => 'Age Group',
        ],
        [
            'attribute' => 'playerTeamPlayer.team_player_id',
            'label' => 'Player Team Player',
        ],
        [
            'attribute' => 'playerSportPosition.sport_position',
            'label' => 'Player Sport Position',
        ],
        'player_sport_position2_id',
        'player_access_code',
        'player_waiver_minor_dt',
        'player_waiver_adult_dt',
        'player_parent_email:email',
        'player_sport_preference',
        'player_sport_position_preference',
        'player_shot_side_preference',
        'player_dominant_side',
        'player_dominant_foot',
        'player_statistical_highlights',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>