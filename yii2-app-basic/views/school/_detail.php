<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\School */

?>
<div class="school-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->school_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'school_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'religiousAffiliation.religious_affiliation',
            'label' => 'Religious Affiliation',
        ],
        'conference_id__female',
        'conference_id__male',
        'conference_id__main',
        'school_ipeds_id',
        'school_unit_id',
        'school_ope_id',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>