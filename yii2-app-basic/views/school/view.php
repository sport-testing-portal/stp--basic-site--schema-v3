<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\School */

$this->title = $model->school_id;
$this->params['breadcrumbs'][] = ['label' => 'Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'School'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->school_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->school_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->school_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'school_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'religiousAffiliation.religious_affiliation',
            'label' => 'Religious Affiliation',
        ],
        'conference_id__female',
        'conference_id__male',
        'conference_id__main',
        'school_ipeds_id',
        'school_unit_id',
        'school_ope_id',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerPlayerCollegeRecruit->totalCount){
    $gridColumnPlayerCollegeRecruit = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_college_recruit_id',
            [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
                        ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerCollegeRecruit,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-college-recruit']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player College Recruit'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerCollegeRecruit
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerSchool->totalCount){
    $gridColumnPlayerSchool = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_school_id',
            [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
                        'player_school_comment',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerSchool,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-school']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player School'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerSchool
    ]);
}
?>

    </div>
    <div class="row">
        <h4>Org<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrg = [
        'org_type_id',
        'org_level_id',
        'org',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url',
        'org_twitter_url',
        'org_facebook_url',
        'org_phone_main',
        'org_email_main',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->org,
        'attributes' => $gridColumnOrg    ]);
    ?>
    <div class="row">
        <h4>ReligiousAffiliation<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnReligiousAffiliation = [
        'religious_affiliation',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->religiousAffiliation,
        'attributes' => $gridColumnReligiousAffiliation    ]);
    ?>
    
    <div class="row">
<?php
if($providerSchoolAct->totalCount){
    $gridColumnSchoolAct = [
        ['class' => 'yii\grid\SerialColumn'],
            'school_act_id',
                        'school_unit_id',
            'school_act_composite_25_pct',
            'school_act_composite_75_pct',
            'school_act_english_25_pct',
            'school_act_english_75_pct',
            'school_act_math_25_pct',
            'school_act_math_75_pct',
            'school_act_student_submit_cnt',
            'school_act_student_submit_pct',
            'school_act_report_period',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSchoolAct,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school-act']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('School Act'),
        ],
        'export' => false,
        'columns' => $gridColumnSchoolAct
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSchoolCoach->totalCount){
    $gridColumnSchoolCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            'school_coach_id',
                        [
                'attribute' => 'coach.coach_id',
                'label' => 'Coach'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSchoolCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('School Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnSchoolCoach
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSchoolSat->totalCount){
    $gridColumnSchoolSat = [
        ['class' => 'yii\grid\SerialColumn'],
            'school_sat_id',
                        'school_unit_id',
            'school_sat_i_verbal_25_pct',
            'school_sat_i_verbal_75_pct',
            'school_sat_i_math_25_pct',
            'school_sat_i_math_75_pct',
            'school_sat_student_submit_cnt',
            'school_sat_student_submit_pct',
            'school_sat_report_period',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSchoolSat,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school-sat']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('School Sat'),
        ],
        'export' => false,
        'columns' => $gridColumnSchoolSat
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSchoolSport->totalCount){
    $gridColumnSchoolSport = [
        ['class' => 'yii\grid\SerialColumn'],
            'school_sport_id',
                        [
                'attribute' => 'sport.sport',
                'label' => 'Sport'
            ],
            'gender_id',
            'conference_id',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSchoolSport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school-sport']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('School Sport'),
        ],
        'export' => false,
        'columns' => $gridColumnSchoolSport
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSchoolStat->totalCount){
    $gridColumnSchoolStat = [
        ['class' => 'yii\grid\SerialColumn'],
            'school_stat_id',
                        'college_cost_rating',
            'college_cost_tuition_instate',
            'college_cost_tuition_outofstate',
            'college_cost_room_and_board',
            'college_cost_books_and_supplies',
            'college_early_decision_dt',
            'college_early_action_dt',
            'college_regular_decision_dt',
            'college_pct_applicants_admitted',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSchoolStat,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school-stat']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('School Stat'),
        ],
        'export' => false,
        'columns' => $gridColumnSchoolStat
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTeam->totalCount){
    $gridColumnTeam = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_id',
            [
                'attribute' => 'org.org',
                'label' => 'Org'
            ],
                        [
                'attribute' => 'sport.sport',
                'label' => 'Sport'
            ],
            [
                'attribute' => 'camp.camp',
                'label' => 'Camp'
            ],
            [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            [
                'attribute' => 'ageGroup.age_group',
                'label' => 'Age Group'
            ],
            'team',
            'team_gender',
            [
                'attribute' => 'teamDivision.team_division',
                'label' => 'Team Division'
            ],
            [
                'attribute' => 'teamLeague.team_league',
                'label' => 'Team League'
            ],
            'team_division',
            'team_league',
            'organizational_level',
            'team_governing_body',
            'team_age_group',
            'team_website_url:url',
            'team_schedule_url:url',
            'team_schedule_uri',
            'team_statistical_highlights',
            [
                'attribute' => 'teamCompetitionSeason.season',
                'label' => 'Team Competition Season'
            ],
            'team_competition_season',
            'team_city',
            'team_state_id',
            [
                'attribute' => 'teamCountry.country',
                'label' => 'Team Country'
            ],
            'team_wins',
            'team_losses',
            'team_draws',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team'),
        ],
        'export' => false,
        'columns' => $gridColumnTeam
    ]);
}
?>

    </div>
</div>
