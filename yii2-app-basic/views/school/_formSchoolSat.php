<div class="form-group" id="add-school-sat">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'SchoolSat',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'school_sat_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'school_unit_id' => ['type' => TabularForm::INPUT_TEXT],
        'school_sat_i_verbal_25_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_sat_i_verbal_75_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_sat_i_math_25_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_sat_i_math_75_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_sat_student_submit_cnt' => ['type' => TabularForm::INPUT_TEXT],
        'school_sat_student_submit_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_sat_report_period' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowSchoolSat(' . $key . '); return false;', 'id' => 'school-sat-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add School Sat', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowSchoolSat()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

