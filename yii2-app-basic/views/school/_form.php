<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\School */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerCollegeRecruit', 
        'relID' => 'player-college-recruit', 
        'value' => \yii\helpers\Json::encode($model->playerCollegeRecruits),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerSchool', 
        'relID' => 'player-school', 
        'value' => \yii\helpers\Json::encode($model->playerSchools),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SchoolAct', 
        'relID' => 'school-act', 
        'value' => \yii\helpers\Json::encode($model->schoolActs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SchoolCoach', 
        'relID' => 'school-coach', 
        'value' => \yii\helpers\Json::encode($model->schoolCoaches),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SchoolSat', 
        'relID' => 'school-sat', 
        'value' => \yii\helpers\Json::encode($model->schoolSats),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SchoolSport', 
        'relID' => 'school-sport', 
        'value' => \yii\helpers\Json::encode($model->schoolSports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SchoolStat', 
        'relID' => 'school-stat', 
        'value' => \yii\helpers\Json::encode($model->schoolStats),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Team', 
        'relID' => 'team', 
        'value' => \yii\helpers\Json::encode($model->teams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="school-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'school_id')->textInput(['placeholder' => 'School']) ?>

    <?= $form->field($model, 'org_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org'),
        'options' => ['placeholder' => 'Choose Org'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'religious_affiliation_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\ReligiousAffiliation::find()->orderBy('religious_affiliation_id')->asArray()->all(), 'religious_affiliation_id', 'religious_affiliation'),
        'options' => ['placeholder' => 'Choose Religious affiliation'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'conference_id__female')->textInput(['placeholder' => 'Conference Id  Female']) ?>

    <?= $form->field($model, 'conference_id__male')->textInput(['placeholder' => 'Conference Id  Male']) ?>

    <?= $form->field($model, 'conference_id__main')->textInput(['placeholder' => 'Conference Id  Main']) ?>

    <?= $form->field($model, 'school_ipeds_id')->textInput(['placeholder' => 'School Ipeds']) ?>

    <?= $form->field($model, 'school_unit_id')->textInput(['placeholder' => 'School Unit']) ?>

    <?= $form->field($model, 'school_ope_id')->textInput(['placeholder' => 'School Ope']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerCollegeRecruit'),
            'content' => $this->render('_formPlayerCollegeRecruit', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerCollegeRecruits),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerSchool'),
            'content' => $this->render('_formPlayerSchool', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerSchools),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SchoolAct'),
            'content' => $this->render('_formSchoolAct', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->schoolActs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SchoolCoach'),
            'content' => $this->render('_formSchoolCoach', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->schoolCoaches),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SchoolSat'),
            'content' => $this->render('_formSchoolSat', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->schoolSats),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SchoolSport'),
            'content' => $this->render('_formSchoolSport', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->schoolSports),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SchoolStat'),
            'content' => $this->render('_formSchoolStat', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->schoolStats),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Team'),
            'content' => $this->render('_formTeam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teams),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
