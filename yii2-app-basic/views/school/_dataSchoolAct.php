<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->schoolActs,
        'key' => 'school_act_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'school_act_id',
        'school_unit_id',
        'school_act_composite_25_pct',
        'school_act_composite_75_pct',
        'school_act_english_25_pct',
        'school_act_english_75_pct',
        'school_act_math_25_pct',
        'school_act_math_75_pct',
        'school_act_student_submit_cnt',
        'school_act_student_submit_pct',
        'school_act_report_period',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'school-act'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
