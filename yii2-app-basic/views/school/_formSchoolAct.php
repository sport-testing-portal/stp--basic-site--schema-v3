<div class="form-group" id="add-school-act">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'SchoolAct',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'school_act_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'school_unit_id' => ['type' => TabularForm::INPUT_TEXT],
        'school_act_composite_25_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_act_composite_75_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_act_english_25_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_act_english_75_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_act_math_25_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_act_math_75_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_act_student_submit_cnt' => ['type' => TabularForm::INPUT_TEXT],
        'school_act_student_submit_pct' => ['type' => TabularForm::INPUT_TEXT],
        'school_act_report_period' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowSchoolAct(' . $key . '); return false;', 'id' => 'school-act-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add School Act', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowSchoolAct()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

