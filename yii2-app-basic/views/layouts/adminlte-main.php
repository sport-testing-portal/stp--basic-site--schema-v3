<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$identity = Yii::$app->user->getIdentity($autoRenew=true);
//$test = ['userId' => Yii::$app->getUser() ? Yii::$app->getUser()->getId() : null];

if (Yii::$app->user->isGuest === false){
        
    //echo '<pre><code>' . \yii\helpers\VarDumper::dumpAsString($identity) . '</pre></code>';
    // next line returns an active query
    //$user = new \dektrium\user\models\User();
    $user = \dektrium\user\models\User::find(['user_id', Yii::$app->user->id])->with('profile')->one();
    $profile = $user['profile'];
    //var_dump($profile);
} else {
    $user = '';
    $profile = '';
    //var_dump($identity); // identity will be equal to the User model for the logged in user
}

if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'adminlte-main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-black sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'adminlte-header.php',
            ['directoryAsset' => $directoryAsset, 'profile'=>$profile]
        ) ?>

        <?= 
            $this->render(
            'adminlte-left.php',
            ['directoryAsset' => $directoryAsset, 'profile'=>$profile]
        )
        ?>

        <?= $this->render(
            'adminlte-content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
