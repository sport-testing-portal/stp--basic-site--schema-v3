<?php 
use \yii\widgets\Block;
use kartik\sidenav\SideNav;


$this->blocks['sidebar'] = '';
$this->blocks['toolbar'] = '';

//  //layouts/main
$this->beginContent('@app/views/layouts/column2.php'); ?>
<!--<div class="container">
	<div class="span-6">-->
		<p>
			
<?php Block::begin(array('id'=>'sidebar')); ?>
<?php

 // <editor-fold defaultstate="collapsed" desc="sidebar nav">
echo SideNav::widget([
    'type' => SideNav::TYPE_INFO,
    'heading' => Yii::t('app','Metadata'),
    'activateItems'=> true,
    'items' => 
    [
        [
            'url' => ['/metadata-dashboard/index'],
            'label' => 'Dashboard',
            'icon' => ''
        ],   
        //test sub-items
//        [
//            //'url' => ['/metadata-database/index'],
//            'label' => 'Codebase',
//            'icon' => '',
//            //'active'=>true,
//            'activateItems'=>true,
//            'items'=>[
//                ['label'=>'General', 'icon'=>'', 'url'=>'/metadata-codebase/index'],
//                ['label'=>'CEC', 'icon'=>'', 'url'=>'/metadata-codebase-cec/index'],
//                ['label'=>'Development Status', 'icon'=>'', 'url'=>'/metadata-codebase-dev/index'],
//                ['label'=>'CEC Items', 'icon'=>'', 'url'=>'/metadata-codebase-cec-status/index'],
//                ['label'=>'Dev Status Items', 'icon'=>'', 'url'=>'/metadata-codebase-dev-status/index'],
//            ]
//        ],        
        [
            'url' => ['/metadata-database/index'],
            'label' => 'Database',
            'icon' => ''
        ], 
        //test sub-items
//        [
//            //'url' => ['/metadata-database/index'],
//            'label' => 'Database',
//            'icon' => '',
//            'items'=>[
//                ['label'=>'General', 'icon'=>'', 'url'=>'/metadata-database/index'],
//                ['label'=>'CEC', 'icon'=>'', 'url'=>'/metadata-database-cec/index'],
//                ['label'=>'Development Status', 'icon'=>'', 'url'=>'/metadata-database-dev/index'],
//                ['label'=>'CEC Items', 'icon'=>'', 'url'=>'/metadata-database-cec-status/index'],
//                ['label'=>'Dev Status Items', 'icon'=>'', 'url'=>'/metadata-database-dev-status/index'],
//            ]
//        ],        
        
        [
            'url' => ['/metadata-codebase'],
            'label' => 'Codebase',
            'icon' => ''
        ],                    
        [
            'url' => ['/metadata-codebase-cec/index'],
            'label' => 'CEC Codebase',
            'icon' => ''
        ],
        [
            'url' => ['/metadata-database-cec/index'],
            'label' => 'CEC Database',
            'icon' => ''
        ],         
        [
            'url' => ['/metadata-codebase-cec-status/index'],
            'label' => 'Codebase CEC Status',
            'icon' => ''
        ],        
        [
            'url' => ['/metadata-database-cec-status/index'],
            'label' => 'Database CEC Status',
            'icon' => ''
        ],         
                   

        [
            'url' => ['/metadata-api-provider/index'],
            'label' => 'Codebase API Providers',
            'icon' => ''
        ],         
        [
            'url' => ['/metadata-api-class/index'],
            'label' => 'Codebase API Classes',
            'icon' => ''
        ],         
        [
            'url' => ['/metadata-api-class-func/index'],
            'label' => 'Codebase API Class Functions',
            'icon' => ''
        ],         
        [
            'url' => ['/metadata-api-class-xlat/index'],
            'label' => 'Codebase API Class Xlations',
            'icon' => ''
        ],         
        [
            'url' => ['/metadata-api-class-xlat-item/index'],
            'label' => 'Codebase API Class Item Xlations',
            'icon' => ''
        ],         
        
//                    ['label' => 'Codebase API Providers','url' => Url::toRoute('metadata-api-provider/index')],
//                    ['label' => 'Codebase API Classes',     'url' => Url::toRoute('metadata-api-class/index')],
//                    ['label' => 'Codebase API Class Functions', 'url' => Url::toRoute('metadata-api-class-func/index')],
//                    ['label' => 'Codebase API Class Xlations', 'url' => Url::toRoute('metadata-api-class-xlat/index')],
//                    ['label' => 'Codebase API Class Item Xlations', 'url' => Url::toRoute('metadata-api-class-xlat-item/index')],

        
        
        
        
                   // ['label' => Yii::t('app','Create'), 'icon'=>'plus', 'url'=>['create']]
                  ]
        ]);
// </editor-fold>
?>
   
<?php Block::end(); ?>
		</p>

<?php Block::begin(array('id'=>'toolbar')); ?>  
<?php 
/*
echo SideNav::widget([
    'type' => SideNav::TYPE_DEFAULT,
    'heading' => Yii::t('app','Toolbar'),
    'items' => 
    [
        [
            'url' => ['/wiki/index'],
            'label' => 'Wiki Home',
            'icon' => 'home'
        ],   
        [
            'url' => ['/wiki/faq'],
            'label' => 'FAQ',
            'icon' => ''
        ],                    
        [
            'url' => ['/wiki/extensions'],
            'label' => 'Extensions',
            'icon' => ''
        ],                    
        [
            'url' => ['/wiki/database-relations'],
            'label' => 'Database Relations',
            'icon' => ''
        ],
        [
            'url' => ['/wiki/database-requirements'],
            'label' => 'Database Requirements',
            'icon' => ''
        ],                         
        [
                'label' => 'Installed Yii2 Extensions',
                'icon' => '',
                'items' => [
                        ['label' => 'yii2 Migration Creator', 'icon'=>''
                            , 'url'=>['/wiki/migration-creator']],
                        ['label' => 'yii2 Save Relations Behavior', 'icon'=>'',
                            'url'=>['/wiki/save-relations']],
                ],
        ],                    
                    
                    ['label' => Yii::t('app','Create'), 'icon'=>'plus', 'url'=>['create']]
                  ]
        ]);
 * 
 */
?>                
                
<?php Block::end(); ?>
                
<?php echo $content; ?>

<?php $this->endContent(); ?>

