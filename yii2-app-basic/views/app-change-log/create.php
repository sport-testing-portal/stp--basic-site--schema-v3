<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AppChangeLog */

$this->title = 'Create App Change Log';
$this->params['breadcrumbs'][] = ['label' => 'App Change Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-change-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
