<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppChangeLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-app-change-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'app_change_log_id')->textInput(['placeholder' => 'App Change Log']) ?>

    <?= $form->field($model, 'app_semantic_version')->textInput(['maxlength' => true, 'placeholder' => 'App Semantic Version']) ?>

    <?= $form->field($model, 'app_change_desc')->textInput(['maxlength' => true, 'placeholder' => 'App Change Desc']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
