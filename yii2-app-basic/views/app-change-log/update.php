<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppChangeLog */

$this->title = 'Update App Change Log: ' . ' ' . $model->app_change_log_id;
$this->params['breadcrumbs'][] = ['label' => 'App Change Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->app_change_log_id, 'url' => ['view', 'id' => $model->app_change_log_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="app-change-log-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
