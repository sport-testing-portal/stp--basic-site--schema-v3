<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcView */

$this->title = 'Create Metadata Codebase Mvc View';
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Mvc Views', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-codebase-mvc-view-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
