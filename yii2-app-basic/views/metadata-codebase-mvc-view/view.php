<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcView */

$this->title = $model->codebase_mvc_view_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Mvc Views', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-codebase-mvc-view-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Metadata Codebase Mvc View'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->codebase_mvc_view_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->codebase_mvc_view_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->codebase_mvc_view_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'codebase_mvc_view_id',
        [
            'attribute' => 'codebase.codebase_id',
            'label' => 'Codebase',
        ],
        'view_function',
        'view_file',
        'view_params',
        'view_url:url',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MetadataCodebase<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataCodebase = [
        'file_name',
        'file_path_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->codebase,
        'attributes' => $gridColumnMetadataCodebase    ]);
    ?>
</div>
