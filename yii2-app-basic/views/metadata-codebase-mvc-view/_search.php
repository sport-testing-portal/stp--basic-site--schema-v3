<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcViewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-codebase-mvc-view-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codebase_mvc_view_id')->textInput(['placeholder' => 'Codebase Mvc View']) ?>

    <?= $form->field($model, 'codebase_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataCodebase::find()->orderBy('codebase_id')->asArray()->all(), 'codebase_id', 'codebase_id'),
        'options' => ['placeholder' => 'Choose Metadata  codebase'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'view_function')->textInput(['maxlength' => true, 'placeholder' => 'View Function']) ?>

    <?= $form->field($model, 'view_file')->textInput(['maxlength' => true, 'placeholder' => 'View File']) ?>

    <?= $form->field($model, 'view_params')->textInput(['maxlength' => true, 'placeholder' => 'View Params']) ?>

    <?php /* echo $form->field($model, 'view_url')->textInput(['maxlength' => true, 'placeholder' => 'View Url']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
