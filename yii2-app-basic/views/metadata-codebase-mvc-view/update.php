<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcView */

$this->title = 'Update Metadata Codebase Mvc View: ' . ' ' . $model->codebase_mvc_view_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Mvc Views', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codebase_mvc_view_id, 'url' => ['view', 'id' => $model->codebase_mvc_view_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-codebase-mvc-view-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
