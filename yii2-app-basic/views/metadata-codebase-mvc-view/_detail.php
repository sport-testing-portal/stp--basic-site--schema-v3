<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcView */

?>
<div class="metadata-codebase-mvc-view-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->codebase_mvc_view_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'codebase_mvc_view_id',
        [
            'attribute' => 'codebase.codebase_id',
            'label' => 'Codebase',
        ],
        'view_function',
        'view_file',
        'view_params',
        'view_url:url',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>