<div class="form-group" id="add-metadata-database-cec-status">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'MetadataDatabaseCecStatus',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'cec_status_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'database_cec_id' => [
            'label' => 'Metadata  database cec',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabaseCec::find()->orderBy('database_cec_id')->asArray()->all(), 'database_cec_id', 'database_cec_id'),
                'options' => ['placeholder' => 'Choose Metadata  database cec'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'cec_status' => ['type' => TabularForm::INPUT_TEXT],
        'cec_status_tag' => ['type' => TabularForm::INPUT_TEXT],
        'cec_status_bfr' => ['type' => TabularForm::INPUT_TEXT],
        'cec_status_at' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMetadataDatabaseCecStatus(' . $key . '); return false;', 'id' => 'metadata-database-cec-status-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Metadata  Database Cec Status', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowMetadataDatabaseCecStatus()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

