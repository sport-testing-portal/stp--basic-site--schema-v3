<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabase */

$this->title = $model->database_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Databases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-database-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Metadata Database'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->database_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->database_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->database_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'database_id',
        'table_name',
        'schema_name',
        'app_module_name',
        'developer_notes',
        'col_name_len_max',
        'has_named_primary_key',
        'has_v3_fields',
        'has_representing_field',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerMetadataDatabaseCecStatus->totalCount){
    $gridColumnMetadataDatabaseCecStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            'cec_status_id',
                        [
                'attribute' => 'databaseCec.database_cec_id',
                'label' => 'Database Cec'
            ],
            'cec_status',
            'cec_status_tag',
            'cec_status_bfr',
            'cec_status_at',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataDatabaseCecStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--database-cec-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Database Cec Status'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataDatabaseCecStatus
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMetadataDatabaseDevStatus->totalCount){
    $gridColumnMetadataDatabaseDevStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            'dev_status_id',
                        [
                'attribute' => 'databaseDev.database_dev_id',
                'label' => 'Database Dev'
            ],
            'dev_status',
            'dev_status_tag',
            'dev_status_bfr',
            'dev_status_at',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataDatabaseDevStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--database-dev-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Database Dev Status'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataDatabaseDevStatus
    ]);
}
?>

    </div>
</div>
