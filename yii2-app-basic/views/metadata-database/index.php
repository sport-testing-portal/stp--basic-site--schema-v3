<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\MetadataDatabaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Database';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="metadata-database-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Database', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        //'database_id',
        [
            'attribute'=>'database_id',
            'label'=>'ID',
            'width'=>'50px'
        ],
        'table_name',
        //'schema_name',
        //'app_module_name',
        [
            'attribute'=>'app_module_name',
            'label'=>'Module',
            'width'=>'50px'
        ],
        //'developer_notes',
        //'col_name_len_max',
        //'has_named_primary_key',
        [
            'attribute'=>'has_named_primary_key',
            'label'=>'Named ID',
            'width'=>'50px'
        ],
        //'has_v3_fields',
        [
            'attribute'=>'has_v3_fields',
            'label'=>'v3 Fields',
            'width'=>'50px'
        ],
        //'has_representing_field',
        [
            'attribute'=>'has_representing_field',
            'label'=>'Rep Field',
            'width'=>'50px'
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ]; 
    $dataProvider->pagination->pageSize=12;
    $scrollingTop = 70;
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata-database']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
        // scrolling top defaults to 50
        'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],        
        
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        
        'export' => false,
        /*
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
         * 
         */
    ]); ?>

</div>

