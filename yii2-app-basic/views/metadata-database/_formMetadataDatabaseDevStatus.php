<div class="form-group" id="add-metadata-database-dev-status">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'MetadataDatabaseDevStatus',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'dev_status_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'database_dev_id' => [
            'label' => 'Metadata  database dev',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabaseDev::find()->orderBy('database_dev_id')->asArray()->all(), 'database_dev_id', 'database_dev'),
                'options' => ['placeholder' => 'Choose Metadata  database dev'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'dev_status' => ['type' => TabularForm::INPUT_TEXT],
        'dev_status_tag' => ['type' => TabularForm::INPUT_TEXT],
        'dev_status_bfr' => ['type' => TabularForm::INPUT_TEXT],
        'dev_status_at' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMetadataDatabaseDevStatus(' . $key . '); return false;', 'id' => 'metadata-database-dev-status-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Metadata  Database Dev Status', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowMetadataDatabaseDevStatus()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

