<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-database-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'database_id')->textInput(['placeholder' => 'Database']) ?>

    <?= $form->field($model, 'table_name')->textInput(['maxlength' => true, 'placeholder' => 'Table Name']) ?>

    <?= $form->field($model, 'schema_name')->textInput(['maxlength' => true, 'placeholder' => 'Schema Name']) ?>

    <?= $form->field($model, 'app_module_name')->textInput(['maxlength' => true, 'placeholder' => 'App Module Name']) ?>

    <?= $form->field($model, 'developer_notes')->textInput(['maxlength' => true, 'placeholder' => 'Developer Notes']) ?>

    <?php /* echo $form->field($model, 'col_name_len_max')->textInput(['placeholder' => 'Col Name Len Max']) */ ?>

    <?php echo $form->field($model, 'has_named_primary_key')->textInput(['maxlength' => true, 'placeholder' => 'Has Named Primary Key']) ?>

    <?php echo $form->field($model, 'has_v3_fields')->textInput(['maxlength' => true, 'placeholder' => 'Has V3 Fields']) ?>

    <?php echo $form->field($model, 'has_representing_field')->textInput(['maxlength' => true, 'placeholder' => 'Has Representing Field']) ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
