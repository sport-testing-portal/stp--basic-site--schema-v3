<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabase */

?>
<div class="metadata-database-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->database_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'database_id',
        'table_name',
        'schema_name',
        'app_module_name',
        'developer_notes',
        'col_name_len_max',
        'has_named_primary_key',
        'has_v3_fields',
        'has_representing_field',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>