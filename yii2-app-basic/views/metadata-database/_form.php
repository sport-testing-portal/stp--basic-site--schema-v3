<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabase */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataDatabaseCecStatus', 
        'relID' => 'metadata-database-cec-status', 
        'value' => \yii\helpers\Json::encode($model->metadataDatabaseCecStatuses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataDatabaseDevStatus', 
        'relID' => 'metadata-database-dev-status', 
        'value' => \yii\helpers\Json::encode($model->metadataDatabaseDevStatuses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="metadata-database-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'database_id')->textInput(['placeholder' => 'Database']) ?>

    <?= $form->field($model, 'table_name')->textInput(['maxlength' => true, 'placeholder' => 'Table Name']) ?>

    <?= $form->field($model, 'schema_name')->textInput(['maxlength' => true, 'placeholder' => 'Schema Name']) ?>

    <?= $form->field($model, 'app_module_name')->textInput(['maxlength' => true, 'placeholder' => 'App Module Name']) ?>

    <?= $form->field($model, 'developer_notes')->textInput(['maxlength' => true, 'placeholder' => 'Developer Notes']) ?>

    <?= $form->field($model, 'col_name_len_max')->textInput(['placeholder' => 'Col Name Len Max']) ?>

    <?= $form->field($model, 'has_named_primary_key')->textInput(['maxlength' => true, 'placeholder' => 'Has Named Primary Key']) ?>

    <?= $form->field($model, 'has_v3_fields')->textInput(['maxlength' => true, 'placeholder' => 'Has V3 Fields']) ?>

    <?= $form->field($model, 'has_representing_field')->textInput(['maxlength' => true, 'placeholder' => 'Has Representing Field']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MetadataDatabaseCecStatus'),
            'content' => $this->render('_formMetadataDatabaseCecStatus', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataDatabaseCecStatuses),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MetadataDatabaseDevStatus'),
            'content' => $this->render('_formMetadataDatabaseDevStatus', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataDatabaseDevStatuses),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
