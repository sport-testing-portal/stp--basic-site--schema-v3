<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabase */

$this->title = 'Save As New Metadata Database: '. ' ' . $model->database_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Databases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->database_id, 'url' => ['view', 'id' => $model->database_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-database-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
