<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('MetadataDatabase'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Metadata Database Cec Status'),
        'content' => $this->render('_dataMetadataDatabaseCecStatus', [
            'model' => $model,
            'row' => $model->metadataDatabaseCecStatuses,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Metadata Database Dev Status'),
        'content' => $this->render('_dataMetadataDatabaseDevStatus', [
            'model' => $model,
            'row' => $model->metadataDatabaseDevStatuses,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
