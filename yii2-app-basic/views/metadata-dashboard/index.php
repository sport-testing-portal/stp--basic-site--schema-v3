<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\MetadataCodebaseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Codebase Metadata';
$this->params['breadcrumbs'][] = ['label'=>'Metadata','url'=>'/metadata-dashboard/index'];
$this->params['breadcrumbs'][] = ['label'=>'Codebase','url'=>'/metadata-dashboard/index'];
//$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
//$this->registerJs($search);
?>
<div class="metadata-codebase-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Codebase', ['populate'], ['class' => 'btn btn-success']) ?>

    </p>
    <?php 
//    $customer_persons_data = (new \yii\db\Query())
//    ->select(['customer_person_id as id', "CONCAT(name, ' (',email,')') as name"])
//    ->from('table')
//    ->where(['customer_person_id' => $customer_persons_ids])
//    ->all();
    
    $codebase_items_data = (new \yii\db\Query())
    ->select(['codebase_id as id', "CONCAT(file_path_uri,'/',file_name) as name"])
    ->from('metadata__codebase')
    //->where(['codebase_id' => 1])
    ->all();  
    
var_dump($codebase_items_data);
    ?>

    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        'codebase_id',
        'file_name',
        'file_path_uri',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata-codebase']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]);
       ?>
     

</div>
