<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Codebase'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('CEC Status'),
        'content' => $this->render('_dataMetadataCodebaseCecStatus', [
            'model' => $model,
            'row' => $model->metadataCodebaseCecStatuses,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Development Status'),
        'content' => $this->render('_dataMetadataCodebaseDevStatus', [
            'model' => $model,
            'row' => $model->metadataCodebaseDevStatuses,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('MVC Controllers'),
        'content' => $this->render('_dataMetadataCodebaseMvcControl', [
            'model' => $model,
            'row' => $model->metadataCodebaseMvcControls,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('MVC Models'),
        'content' => $this->render('_dataMetadataCodebaseMvcModel', [
            'model' => $model,
            'row' => $model->metadataCodebaseMvcModels,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('MVC Views'),
        'content' => $this->render('_dataMetadataCodebaseMvcView', [
            'model' => $model,
            'row' => $model->metadataCodebaseMvcViews,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
