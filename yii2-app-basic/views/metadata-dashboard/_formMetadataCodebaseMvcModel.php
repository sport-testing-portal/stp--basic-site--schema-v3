<div class="form-group" id="add-metadata-codebase-mvc-model">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'MetadataCodebaseMvcModel',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'codebase_mvc_model_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'json_data_structure' => ['type' => TabularForm::INPUT_TEXT],
        'json_data_sample' => ['type' => TabularForm::INPUT_TEXT],
        'json_data_example' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMetadataCodebaseMvcModel(' . $key . '); return false;', 'id' => 'metadata-codebase-mvc-model-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Metadata  Codebase Mvc Model', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowMetadataCodebaseMvcModel()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

