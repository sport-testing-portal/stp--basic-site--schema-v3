<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebase */

$this->title = $model->codebase_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-codebase-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Metadata Codebase'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->codebase_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->codebase_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->codebase_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'codebase_id',
        'file_name',
        'file_path_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerMetadataCodebaseCecStatus->totalCount){
    $gridColumnMetadataCodebaseCecStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            'cec_status_id',
                        [
                'attribute' => 'codebaseCec.codebase_cec_id',
                'label' => 'Codebase Cec'
            ],
            'cec_status',
            'cec_status_tag',
            'cec_status_bfr',
            'cec_status_at',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataCodebaseCecStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--codebase-cec-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Codebase Cec Status'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataCodebaseCecStatus
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMetadataCodebaseDevStatus->totalCount){
    $gridColumnMetadataCodebaseDevStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            'dev_status_id',
                        [
                'attribute' => 'codebaseDev.codebase_dev_id',
                'label' => 'Codebase Dev'
            ],
            'dev_status',
            'dev_status_tag',
            'dev_status_bfr',
            'dev_status_at',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataCodebaseDevStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--codebase-dev-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Codebase Dev Status'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataCodebaseDevStatus
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMetadataCodebaseMvcControl->totalCount){
    $gridColumnMetadataCodebaseMvcControl = [
        ['class' => 'yii\grid\SerialColumn'],
            'codebase_mvc_control_id',
                        'controller_actions',
            'action_function',
            'action_params',
            'action_url:url',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataCodebaseMvcControl,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--codebase-mvc-control']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Codebase Mvc Control'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataCodebaseMvcControl
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMetadataCodebaseMvcModel->totalCount){
    $gridColumnMetadataCodebaseMvcModel = [
        ['class' => 'yii\grid\SerialColumn'],
            'codebase_mvc_model_id',
                        'json_data_structure',
            'json_data_sample',
            'json_data_example',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataCodebaseMvcModel,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--codebase-mvc-model']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Codebase Mvc Model'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataCodebaseMvcModel
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMetadataCodebaseMvcView->totalCount){
    $gridColumnMetadataCodebaseMvcView = [
        ['class' => 'yii\grid\SerialColumn'],
            'codebase_mvc_view_id',
                        'view_function',
            'view_file',
            'view_params',
            'view_url:url',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataCodebaseMvcView,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--codebase-mvc-view']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Codebase Mvc View'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataCodebaseMvcView
    ]);
}
?>

    </div>
</div>
