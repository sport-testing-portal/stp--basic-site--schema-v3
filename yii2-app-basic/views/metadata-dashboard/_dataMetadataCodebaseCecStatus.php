<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->metadataCodebaseCecStatuses,
        'key' => 'cec_status_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'cec_status_id',
        [
                'attribute' => 'codebaseCec.codebase_cec_id',
                'value' => 'codebaseCec.codebase_cec',
                'label' => 'CEC Codebase'
            ],
        'cec_status',
        'cec_status_tag',
        'cec_status_bfr',
        'cec_status_at',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'metadata-codebase-cec-status'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
