<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebase */

$this->title = 'Update Metadata Codebase: ' . ' ' . $model->codebase_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codebase_id, 'url' => ['view', 'id' => $model->codebase_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-codebase-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
