<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebase */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataCodebaseCecStatus', 
        'relID' => 'metadata-codebase-cec-status', 
        'value' => \yii\helpers\Json::encode($model->metadataCodebaseCecStatuses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataCodebaseDevStatus', 
        'relID' => 'metadata-codebase-dev-status', 
        'value' => \yii\helpers\Json::encode($model->metadataCodebaseDevStatuses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataCodebaseMvcControl', 
        'relID' => 'metadata-codebase-mvc-control', 
        'value' => \yii\helpers\Json::encode($model->metadataCodebaseMvcControls),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataCodebaseMvcModel', 
        'relID' => 'metadata-codebase-mvc-model', 
        'value' => \yii\helpers\Json::encode($model->metadataCodebaseMvcModels),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataCodebaseMvcView', 
        'relID' => 'metadata-codebase-mvc-view', 
        'value' => \yii\helpers\Json::encode($model->metadataCodebaseMvcViews),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="metadata-codebase-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'codebase_id')->textInput(['placeholder' => 'Codebase']) ?>

    <?= $form->field($model, 'file_name')->textInput(['maxlength' => true, 'placeholder' => 'File Name']) ?>

    <?= $form->field($model, 'file_path_uri')->textInput(['maxlength' => true, 'placeholder' => 'File Path Uri']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CEC Status'),
            'content' => $this->render('_formMetadataCodebaseCecStatus', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataCodebaseCecStatuses),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Development Status'),
            'content' => $this->render('_formMetadataCodebaseDevStatus', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataCodebaseDevStatuses),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MVC Controllers'),
            'content' => $this->render('_formMetadataCodebaseMvcControl', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataCodebaseMvcControls),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MVC Models'),
            'content' => $this->render('_formMetadataCodebaseMvcModel', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataCodebaseMvcModels),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MVC Views'),
            'content' => $this->render('_formMetadataCodebaseMvcView', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataCodebaseMvcViews),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
