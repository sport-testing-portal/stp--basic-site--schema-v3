<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClass */

?>
<div class="metadata-api-class-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->metadata__api_class) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        //'metadata__api_class_id',
        [
            'attribute' => 'metadata__api_class_id',
            'label' => 'ID',
        ],        
        [
            'attribute' => 'metadataApiProvider.metadata__api_provider',
            'label' => 'API Provider',
        ],
        'metadata__api_class',
        'metadata__api_class_desc',
        'metadata__api_class_docs_url:url',
        'metadata__api_reference_example',
        'metadata__api_regex_find',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn,
        //'containerOptions' => ['style' => 'overflow: auto'],
    ]); 
?>
    </div>
</div>