<div class="form-group" id="add-metadata-api-class-func">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'MetadataApiClassFunc',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'metadata__api_class_func_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'metadata__api_class_func' => ['type' => TabularForm::INPUT_TEXT],
        'metadata__api_class_func_desc' => ['type' => TabularForm::INPUT_TEXT],
        'metadata__api_class_func_docs_url' => ['type' => TabularForm::INPUT_TEXT],
        'metadata__api_class_func_reference_example' => ['type' => TabularForm::INPUT_TEXT],
        'metadata__api_class_func_regex_find' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMetadataApiClassFunc(' . $key . '); return false;', 'id' => 'metadata-api-class-func-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add API Class Function',
                ['type' => 'button', 'class' => 'btn btn-success kv-batch-create',
                    'onClick' => 'addRowMetadataApiClassFunc()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

