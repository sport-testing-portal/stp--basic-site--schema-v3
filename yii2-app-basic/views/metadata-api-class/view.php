<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClass */

$this->title = $model->metadata__api_class;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Api Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-api-class-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Metadata Api Class'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->metadata__api_class_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->metadata__api_class_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->metadata__api_class_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'metadata__api_class_id',
        [
            'attribute' => 'metadataApiProvider.metadata__api_provider',
            'label' => 'Metadata  Api Provider',
        ],
        'metadata__api_class',
        'metadata__api_class_desc',
        'metadata__api_class_docs_url:url',
        'metadata__api_reference_example',
        'metadata__api_regex_find',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MetadataApiProvider<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataApiProvider = [
        'metadata__api_provider',
        'metadata__api_provider_desc',
        'metadata__api_provider_url',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->metadataApiProvider,
        'attributes' => $gridColumnMetadataApiProvider    ]);
    ?>
    
    <div class="row">
<?php
if($providerMetadataApiClassFunc->totalCount){
    $gridColumnMetadataApiClassFunc = [
        ['class' => 'yii\grid\SerialColumn'],
            'metadata__api_class_func_id',
                        'metadata__api_class_func',
            'metadata__api_class_func_desc',
            'metadata__api_class_func_docs_url:url',
            'metadata__api_class_func_reference_example',
            'metadata__api_class_func_regex_find',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataApiClassFunc,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--api-class-func']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Api Class Func'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataApiClassFunc
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMetadataApiClassXlat->totalCount){
    $gridColumnMetadataApiClassXlat = [
        ['class' => 'yii\grid\SerialColumn'],
            'metadata__api_class_xlat_id',
                        'metadata__api_class_id_2',
            'metadata__api_class_xlat',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataApiClassXlat,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--api-class-xlat']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Api Class Xlat'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataApiClassXlat
    ]);
}
?>

    </div>
</div>
