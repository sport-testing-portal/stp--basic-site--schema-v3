<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClass */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataApiClassFunc', 
        'relID' => 'metadata-api-class-func', 
        'value' => \yii\helpers\Json::encode($model->metadataApiClassFuncs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataApiClassXlat', 
        'relID' => 'metadata-api-class-xlat', 
        'value' => \yii\helpers\Json::encode($model->metadataApiClassXlats),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="metadata-api-class-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'metadata__api_class_id')->textInput(['placeholder' => 'Metadata  Api Class']) ?>

    <?= $form->field($model, 'metadata__api_provider_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiProvider::find()->orderBy('metadata__api_provider_id')->asArray()->all(), 'metadata__api_provider_id', 'metadata__api_provider'),
        'options' => ['placeholder' => 'Choose Metadata  api provider'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'metadata__api_class')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Class']) ?>

    <?= $form->field($model, 'metadata__api_class_desc')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Class Desc']) ?>

    <?= $form->field($model, 'metadata__api_class_docs_url')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Class Docs Url']) ?>

    <?= $form->field($model, 'metadata__api_reference_example')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Reference Example']) ?>

    <?= $form->field($model, 'metadata__api_regex_find')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Regex Find']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MetadataApiClassFunc'),
            'content' => $this->render('_formMetadataApiClassFunc', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataApiClassFuncs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MetadataApiClassXlat'),
            'content' => $this->render('_formMetadataApiClassXlat', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataApiClassXlats),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
