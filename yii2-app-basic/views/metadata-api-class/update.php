<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClass */

$this->title = 'Update Metadata Api Class: ' . ' ' . $model->metadata__api_class;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Api Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metadata__api_class, 'url' => ['view', 'id' => $model->metadata__api_class_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-api-class-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
