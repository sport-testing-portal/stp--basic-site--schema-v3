<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\MetadataApiClassSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @since 0.8.0 */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'API Classes';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="metadata-api-class-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create API Class', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        //'metadata__api_class_id',
        [
            'attribute'=>'metadata__api_class_id',
            'label'    =>'ID',
            'width'=>'50px'
        ],
        [
                'attribute' => 'metadata__api_provider_id',
                'label' => 'API Provider',
                'value' => function($model){
                    if ($model->metadataApiProvider)
                    {return $model->metadataApiProvider->metadata__api_provider;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiProvider::find()
                    ->asArray()->all(), 'metadata__api_provider_id', 'metadata__api_provider'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true, 'dropdownAutoWidth' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'API Provider', 'id' => 'grid-metadata-api-class-search-metadata__api_provider_id']
            ],
        'metadata__api_class',
        'metadata__api_class_desc',
        'metadata__api_class_docs_url:url',
        'metadata__api_reference_example',
        'metadata__api_regex_find',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        // next 3 settings added by dbyrd
        'containerOptions' => ['style' => 'overflow: auto'],
        // next two settings cause a header overflow bug
        //'floatHeader'=>true,
        //'floatHeaderOptions'=>['scrollingTop'=>'50'],
        
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata-api-class']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
