<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-api-class-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'metadata__api_class_id')
        ->textInput(['placeholder' => 'API Class ID']) ?>

    <?= $form->field($model, 'metadata__api_provider_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiProvider::find()
            ->orderBy('metadata__api_provider_id')->asArray()->all(),
            'metadata__api_provider_id', 'metadata__api_provider'),
        'options' => ['placeholder' => 'Choose API Provider'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'metadata__api_class')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class']) ?>

    <?= $form->field($model, 'metadata__api_class_desc')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Desc']) ?>

    <?= $form->field($model, 'metadata__api_class_docs_url')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Docs URL']) ?>

    <?php /* echo $form->field($model, 'metadata__api_reference_example')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Reference Example']) */ ?>

    <?php /* echo $form->field($model, 'metadata__api_regex_find')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Regex Find']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
