<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->metadataApiClassXlats,
        'key' => 'metadata__api_class_xlat_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'], // no workie ,'width'=>'50px'
        //'metadata__api_class_xlat_id',
        [
            'attribute'=>'metadata__api_class_xlat_id',
            'label'=>'ID',
            'width'=>'50px'
        ],  
        //'metadata__api_class_id_1',
        [
            'attribute'=>'metadata__api_class_id_1',
            'label'=>'Class ID #1',
            'width'=>'70px'
        ],        
        //'metadata__api_class_id_2',
        [
            'attribute'=>'metadata__api_class_id_2',
            'label'=>'Class ID #2',
            'width'=>'70px'
        ],         
        //'metadata__api_class_xlat',
        [
            'attribute'=>'metadata__api_class_xlat',
            'label'=>'Xlation Name',
            'width'=>'150px'
        ],         
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'metadata-api-class-xlat'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
