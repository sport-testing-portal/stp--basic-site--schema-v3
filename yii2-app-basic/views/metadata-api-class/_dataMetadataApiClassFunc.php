<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->metadataApiClassFuncs,
        'key' => 'metadata__api_class_func_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        //'metadata__api_class_func_id',
        [
            'attribute'=>'metadata__api_class_func_id',
            'label'=>'ID',
            'width'=>'50px'
        ],        
        //'metadata__api_class_func',
        [
            'attribute'=>'metadata__api_class_func',
            'label'=>'Function Name',
            'width'=>'150px'
        ],         
        //'metadata__api_class_func_desc',
        [
            'attribute'=>'metadata__api_class_func_desc',
            'label'=>'Function Description',
            'width'=>'150px'
        ],         
        [
            'attribute'=>'metadata__api_class_func_docs_url:url',
            'label'=>'Function Docs URL',
            'width'=>'150px'
        ],        
        'metadata__api_class_func_docs_url:url',
         
        //'metadata__api_class_func_reference_example',
        [
            'attribute'=>'metadata__api_class_func_reference_example',
            'label'=>'Function Str Reference Example',
            'width'=>'150px'
        ],         
        //'metadata__api_class_func_regex_find',
        [
            'attribute'=>'metadata__api_class_func_regex_find',
            'label'=>'Function Rgx Find',
            'width'=>'150px'
        ],         
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'metadata-api-class-func'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
