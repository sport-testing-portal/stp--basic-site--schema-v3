<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiProviderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-api-provider-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'metadata__api_provider_id')->textInput(['placeholder' => 'Metadata  Api Provider']) ?>

    <?= $form->field($model, 'metadata__api_provider')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Provider']) ?>

    <?= $form->field($model, 'metadata__api_provider_desc')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Provider Desc']) ?>

    <?= $form->field($model, 'metadata__api_provider_url')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Provider Url']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
