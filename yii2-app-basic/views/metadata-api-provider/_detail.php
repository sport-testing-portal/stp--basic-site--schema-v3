<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiProvider */

?>
<div class="metadata-api-provider-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->metadata__api_provider) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        //'metadata__api_provider_id',
        [
            'attribute'=>'metadata__api_provider_id',
            'label'=>'ID'
        ],
        'metadata__api_provider',
        'metadata__api_provider_desc',
        'metadata__api_provider_url:url',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>