<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiProvider */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataApiClass', 
        'relID' => 'metadata-api-class', 
        'value' => \yii\helpers\Json::encode($model->metadataApiClasses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="metadata-api-provider-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'metadata__api_provider_id')->textInput(['placeholder' => 'API Provider ID']) ?>

    <?= $form->field($model, 'metadata__api_provider')->textInput(['maxlength' => true, 'placeholder' => 'API Provider Name']) ?>

    <?= $form->field($model, 'metadata__api_provider_desc')->textInput(['maxlength' => true, 'placeholder' => 'API Provider Desc']) ?>

    <?= $form->field($model, 'metadata__api_provider_url')->textInput(['maxlength' => true, 'placeholder' => 'API Provider URL']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MetadataApiClass'),
            'content' => $this->render('_formMetadataApiClass', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataApiClasses),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
