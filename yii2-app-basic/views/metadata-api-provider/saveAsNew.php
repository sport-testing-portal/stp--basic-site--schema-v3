<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiProvider */

$this->title = 'Save As New Metadata Api Provider: '. ' ' . $model->metadata__api_provider;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Api Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metadata__api_provider, 'url' => ['view', 'id' => $model->metadata__api_provider_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-api-provider-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
