<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiProvider */

$this->title = $model->metadata__api_provider;
$this->params['breadcrumbs'][] = ['label' => 'API Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-api-provider-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'API Provider'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->metadata__api_provider_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->metadata__api_provider_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->metadata__api_provider_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'metadata__api_provider_id',
        'metadata__api_provider',
        'metadata__api_provider_desc',
        'metadata__api_provider_url:url',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerMetadataApiClass->totalCount){
    $gridColumnMetadataApiClass = [
        ['class' => 'yii\grid\SerialColumn'],
            'metadata__api_class_id',
                        'metadata__api_class',
            'metadata__api_class_desc',
            'metadata__api_class_docs_url:url',
            'metadata__api_reference_example',
            'metadata__api_regex_find',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataApiClass,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--api-class']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Api Class'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataApiClass
    ]);
}
?>

    </div>
</div>
