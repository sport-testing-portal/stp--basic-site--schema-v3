<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->metadataApiClasses,
        'key' => 'metadata__api_class_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'metadata__api_class_id',
        'metadata__api_class',
        'metadata__api_class_desc',
        'metadata__api_class_docs_url:url',
        'metadata__api_reference_example',
        'metadata__api_regex_find',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'metadata-api-class'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
