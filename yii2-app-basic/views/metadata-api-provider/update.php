<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiProvider */

$this->title = 'Update Metadata Api Provider: ' . ' ' . $model->metadata__api_provider;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Api Providers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metadata__api_provider, 'url' => ['view', 'id' => $model->metadata__api_provider_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-api-provider-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
