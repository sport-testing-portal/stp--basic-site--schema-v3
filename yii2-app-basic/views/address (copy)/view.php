<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Address */

$this->title = $model->address_id;
$this->params['breadcrumbs'][] = ['label' => 'Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="address-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Address'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->address_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->address_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->address_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'address_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'person.person',
            'label' => 'Person',
        ],
        [
            'attribute' => 'addressType.address_type',
            'label' => 'Address Type',
        ],
        'addr1',
        'addr2',
        'addr3',
        'city',
        'state_or_region',
        'postal_code',
        'country',
        'country_code',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>AddressType<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAddressType = [
        'address_type',
        'address_type_description_short',
        'address_type_description_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->addressType,
        'attributes' => $gridColumnAddressType    ]);
    ?>
    <div class="row">
        <h4>Org<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrg = [
        'org_type_id',
        'org_level_id',
        'org',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url',
        'org_twitter_url',
        'org_facebook_url',
        'org_phone_main',
        'org_email_main',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->org,
        'attributes' => $gridColumnOrg    ]);
    ?>
    <div class="row">
        <h4>Person<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPerson = [
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        'app_user_id',
        'person_type_id',
        'user_id',
        'person_name_prefix',
        'person_name_first',
        'person_name_middle',
        'person_name_last',
        'person_name_suffix',
        'person',
        'person_phone_personal',
        'person_email_personal',
        'person_phone_work',
        'person_email_work',
        'person_position_work',
        'gender_id',
        'person_image_headshot_url',
        'person_name_nickname',
        'person_date_of_birth',
        'person_height',
        'person_weight',
        'person_tshirt_size',
        'person_high_school__graduation_year',
        'person_college_graduation_year',
        'person_college_commitment_status',
        'person_addr_1',
        'person_addr_2',
        'person_addr_3',
        'person_city',
        'person_postal_code',
        'person_country',
        'person_country_code',
        'person_state_or_region',
        'org_affiliation_begin_dt',
        'org_affiliation_end_dt',
        'person_website',
        'person_profile_url',
        'person_profile_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    if (is_null($model->person) === false){
        //$person = new Person()
    echo DetailView::widget([
        'model' => $model->person,
        'attributes' => $gridColumnPerson    ]);
    }
    ?>
    
    <div class="row">
<?php
if($providerCampLocation->totalCount){
    $gridColumnCampLocation = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_location_id',
            [
                'attribute' => 'camp.camp',
                'label' => 'Camp'
            ],
                        ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampLocation,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-location']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Location'),
        ],
        'export' => false,
        'columns' => $gridColumnCampLocation
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCampSession->totalCount){
    $gridColumnCampSession = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_session_id',
            [
                'attribute' => 'camp.camp',
                'label' => 'Camp'
            ],
            [
                'attribute' => 'sport.sport',
                'label' => 'Sport'
            ],
            [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
                        [
                'attribute' => 'season.season',
                'label' => 'Season'
            ],
            'camp_session_location',
            'camp_session_url:url',
            'camp_session_ages',
            'camp_session_skill_level',
            'camp_session_type',
            'camp_session_begin_dt',
            'camp_session_end_dt',
            'camp_session_specialty',
            'camp_session_scholarships_available_yn',
            'camp_session_description',
            'camp_session_cost_regular_residential',
            'camp_session_cost_regular_commuter',
            'camp_session_cost_regular_day',
            'camp_session_cost_early_residential',
            'camp_session_cost_early_commuter',
            'camp_session_cost_early_day',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampSession,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-session']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Session'),
        ],
        'export' => false,
        'columns' => $gridColumnCampSession
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCampSessionLocation->totalCount){
    $gridColumnCampSessionLocation = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_session_location_id',
            [
                'attribute' => 'campSession.camp_session_id',
                'label' => 'Camp Session'
            ],
                        [
                'attribute' => 'season.season',
                'label' => 'Season'
            ],
            [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampSessionLocation,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-session-location']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Session Location'),
        ],
        'export' => false,
        'columns' => $gridColumnCampSessionLocation
    ]);
}
?>

    </div>
</div>
