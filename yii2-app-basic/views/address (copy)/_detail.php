<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Address */

?>
<div class="address-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->address_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'address_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'person.person',
            'label' => 'Person',
        ],
        [
            'attribute' => 'addressType.address_type',
            'label' => 'Address Type',
        ],
        'addr1',
        'addr2',
        'addr3',
        'city',
        'state_or_region',
        'postal_code',
        'country',
        'country_code',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>