<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrgAddress */

$this->title = 'Save As New Org Address: '. ' ' . $model->org_id;
$this->params['breadcrumbs'][] = ['label' => 'Org Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->org_id, 'url' => ['view', 'org_id' => $model->org_id, 'address_id' => $model->address_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="org-address-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
