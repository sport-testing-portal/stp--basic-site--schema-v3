<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\OrgAddress */

$this->title = 'Create Org Address';
$this->params['breadcrumbs'][] = ['label' => 'Org Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-address-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
