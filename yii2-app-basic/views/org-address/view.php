<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\OrgAddress */

$this->title = $model->org_id;
$this->params['breadcrumbs'][] = ['label' => 'Org Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-address-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Org Address'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'org_id' => $model->org_id, 'address_id' => $model->address_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'org_id' => $model->org_id, 'address_id' => $model->address_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'org_id' => $model->org_id, 'address_id' => $model->address_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'org_id',
        'address_id',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
