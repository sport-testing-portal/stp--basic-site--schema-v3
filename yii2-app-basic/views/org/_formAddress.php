<div class="form-group" id="add-address">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Address',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'address_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'person_id' => [
            'label' => 'Person',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Person::find()->orderBy('person')->asArray()->all(), 'person_id', 'person'),
                'options' => ['placeholder' => 'Choose Person'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'address_type_id' => [
            'label' => 'Address type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\AddressType::find()->orderBy('address_type')->asArray()->all(), 'address_type_id', 'address_type'),
                'options' => ['placeholder' => 'Choose Address type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'addr1' => ['type' => TabularForm::INPUT_TEXT],
        'addr2' => ['type' => TabularForm::INPUT_TEXT],
        'addr3' => ['type' => TabularForm::INPUT_TEXT],
        'city' => ['type' => TabularForm::INPUT_TEXT],
        'state_or_region' => ['type' => TabularForm::INPUT_TEXT],
        'postal_code' => ['type' => TabularForm::INPUT_TEXT],
        'country' => ['type' => TabularForm::INPUT_TEXT],
        'country_code' => ['type' => TabularForm::INPUT_TEXT],
        'effective_from_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Effective From Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'effective_to_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Effective To Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowAddress(' . $key . '); return false;', 'id' => 'address-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Address', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowAddress()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

