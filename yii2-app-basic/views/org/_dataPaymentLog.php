<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->paymentLogs,
        'key' => 'payment_log_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'payment_log_id',
        [
                'attribute' => 'paymentType.payment_type',
                'label' => 'Payment Type'
            ],
        'data_entity_id',
        [
                'attribute' => 'subscription.subscription_id',
                'label' => 'Subscription'
            ],
        [
                'attribute' => 'person.person',
                'label' => 'Person'
            ],
        'payment_amt',
        'payment_dt',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'payment-log'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
