<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->orgGoverningBodies,
        'key' => 'org_governing_body_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        //'org_governing_body_id',
        // dbyrd added next line
        ['attribute' => 'org_governing_body_id', 'visible' => false],        
        [
                'attribute' => 'governingBody.governing_body',
                'label' => 'Governing Bodies'
            ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'org-governing-body'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
