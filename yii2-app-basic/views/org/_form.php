<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Org */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Address', 
        'relID' => 'address', 
        'value' => \yii\helpers\Json::encode($model->addresses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Camp', 
        'relID' => 'camp', 
        'value' => \yii\helpers\Json::encode($model->camps),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Media', 
        'relID' => 'media', 
        'value' => \yii\helpers\Json::encode($model->media),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Note', 
        'relID' => 'note', 
        'value' => \yii\helpers\Json::encode($model->notes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'OrgGoverningBody', 
        'relID' => 'org-governing-body', 
        'value' => \yii\helpers\Json::encode($model->orgGoverningBodies),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'OrgSchool', 
        'relID' => 'org-school', 
        'value' => \yii\helpers\Json::encode($model->orgSchools),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PaymentLog', 
        'relID' => 'payment-log', 
        'value' => \yii\helpers\Json::encode($model->paymentLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Person', 
        'relID' => 'person', 
        'value' => \yii\helpers\Json::encode($model->people),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'School', 
        'relID' => 'school', 
        'value' => \yii\helpers\Json::encode($model->schools),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Subscription', 
        'relID' => 'subscription', 
        'value' => \yii\helpers\Json::encode($model->subscriptions),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Team', 
        'relID' => 'team', 
        'value' => \yii\helpers\Json::encode($model->teams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="org-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'org_id')->textInput(['placeholder' => 'Org']) ?>

    <?= $form->field($model, 'org_type_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\OrgType::find()->orderBy('org_type_id')->asArray()->all(), 'org_type_id', 'org_type'),
        'options' => ['placeholder' => 'Choose Org type'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'org_level_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\OrgLevel::find()->orderBy('org_level_id')->asArray()->all(), 'org_level_id', 'org_level'),
        'options' => ['placeholder' => 'Choose Org level'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'org')->textInput(['maxlength' => true, 'placeholder' => 'Org']) ?>

    <?= $form->field($model, 'org_governing_body')->textInput(['maxlength' => true, 'placeholder' => 'Org Governing Body']) ?>

    <?= $form->field($model, 'org_ncaa_clearing_house_id')->textInput(['maxlength' => true, 'placeholder' => 'Org Ncaa Clearing House']) ?>

    <?= $form->field($model, 'org_website_url')->textInput(['maxlength' => true, 'placeholder' => 'Org Website Url']) ?>

    <?= $form->field($model, 'org_twitter_url')->textInput(['maxlength' => true, 'placeholder' => 'Org Twitter Url']) ?>

    <?= $form->field($model, 'org_facebook_url')->textInput(['maxlength' => true, 'placeholder' => 'Org Facebook Url']) ?>

    <?= $form->field($model, 'org_phone_main')->textInput(['maxlength' => true, 'placeholder' => 'Org Phone Main']) ?>

    <?= $form->field($model, 'org_email_main')->textInput(['maxlength' => true, 'placeholder' => 'Org Email Main']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    // dbyrd test
    $counts = [
        'Address'=> count(\yii\helpers\ArrayHelper::toArray($model->addresses)),
        'Camp'   => count(\yii\helpers\ArrayHelper::toArray($model->camps)),
        'Media'  => count(\yii\helpers\ArrayHelper::toArray($model->media)),
        'Note'   => count(\yii\helpers\ArrayHelper::toArray($model->notes)),
        'OrgGoverningBody'=> count(\yii\helpers\ArrayHelper::toArray($model->orgGoverningBodies)),
        'OrgSchool'=> count(\yii\helpers\ArrayHelper::toArray($model->orgSchools)),
        'PaymentLog'=> count(\yii\helpers\ArrayHelper::toArray($model->paymentLogs)),
        'Person'=> count(\yii\helpers\ArrayHelper::toArray($model->people)),
        'School'=> count(\yii\helpers\ArrayHelper::toArray($model->schools)),
        'Subscription'=> count(\yii\helpers\ArrayHelper::toArray($model->subscriptions)),
        'Team'=> count(\yii\helpers\ArrayHelper::toArray($model->teams)),        
    ];
    yii::trace($counts, 'counts of org links');
    
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Address') . ' ('.$counts['Address'] .')',
            'content' => $this->render('_formAddress', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->addresses),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Camp') . ' ('.$counts['Camp'] .')',
            'content' => $this->render('_formCamp', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->camps),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Media') . ' ('.$counts['Media'] .')',
            'content' => $this->render('_formMedia', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->media),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Note') . ' ('.$counts['Note'] .')',
            'content' => $this->render('_formNote', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->notes),
            ]),
        ],
        [
            //'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('OrgGoverningBody') . ' ('.$counts['OrgGoverningBody'] .')',
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Governing Bodies') . ' ('.$counts['OrgGoverningBody'] .')',
            'content' => $this->render('_formOrgGoverningBody', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->orgGoverningBodies),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('OrgSchool') . ' ('.$counts['OrgSchool'] .')',
            'content' => $this->render('_formOrgSchool', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->orgSchools),
            ]),
        ],
        [
            //'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PaymentLog') . ' ('.$counts['PaymentLog'] .')',
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Payments') . ' ('.$counts['PaymentLog'] .')',
            'content' => $this->render('_formPaymentLog', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->paymentLogs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Person') . ' ('.$counts['Person'] .')',
            'content' => $this->render('_formPerson', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->people),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('School') . ' ('.$counts['School'] .')',
            'content' => $this->render('_formSchool', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->schools),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Subscription') . ' ('.$counts['Subscription'] .')',
            'content' => $this->render('_formSubscription', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->subscriptions),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Team') . ' ('.$counts['Team'] .')',
            'content' => $this->render('_formTeam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teams),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
