<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->people,
        'key' => 'person_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'person_id',
//        [
//                'attribute' => 'appUser.username',
//                'label' => 'App User'
//            ],
        [
                'attribute' => 'personType.person_type',
                'label' => 'Person Type'
            ],
        'user_id',
        'person_name_prefix',
        'person_name_first',
        'person_name_middle',
        'person_name_last',
        'person_name_suffix',
        'person',
        'person_phone_personal',
        'person_email_personal:email',
        'person_phone_work',
        'person_email_work:email',
        'person_position_work',
        [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
        'person_image_headshot_url:url',
        'person_name_nickname',
        'person_date_of_birth',
        'person_height',
        'person_weight',
        'person_tshirt_size',
        'person_high_school__graduation_year',
        'person_college_graduation_year',
        'person_college_commitment_status',
        'person_addr_1',
        'person_addr_2',
        'person_addr_3',
        'person_city',
        'person_postal_code',
        'person_country',
        [
                'attribute' => 'personCountryCode.country',
                'label' => 'Person Country Code'
            ],
        'person_state_or_region',
        'org_affiliation_begin_dt',
        'org_affiliation_end_dt',
        'person_website',
        'person_profile_url:url',
        'person_profile_uri',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'person'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
