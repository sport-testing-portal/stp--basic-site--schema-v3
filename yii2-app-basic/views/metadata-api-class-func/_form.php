<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassFunc */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="metadata-api-class-func-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'metadata__api_class_func_id')->textInput(['placeholder' => 'API Class Func']) ?>

    <?= $form->field($model, 'metadata__api_class_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiClass::find()->orderBy('metadata__api_class_id')->asArray()->all(), 'metadata__api_class_id', 'metadata__api_class'),
        'options' => ['placeholder' => 'Choose API class'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'metadata__api_class_func')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Func']) ?>

    <?= $form->field($model, 'metadata__api_class_func_desc')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Func Desc']) ?>

    <?= $form->field($model, 'metadata__api_class_func_docs_url')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Func Docs Url']) ?>

    <?= $form->field($model, 'metadata__api_class_func_reference_example')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Func Reference Example']) ?>

    <?= $form->field($model, 'metadata__api_class_func_regex_find')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Func Regex Find']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
