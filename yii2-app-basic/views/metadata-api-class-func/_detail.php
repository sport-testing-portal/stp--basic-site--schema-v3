<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassFunc */

?>
<div class="metadata-api-class-func-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->metadata__api_class_func) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        //'metadata__api_class_func_id',
        [
            'attribute' => 'metadata__api_class_func_id',
            'label'     => 'ID'
        ],
        [
            'attribute' => 'metadataApiClass.metadata__api_class',
            'label' => 'API Class',
        ],
        'metadata__api_class_func',
        'metadata__api_class_func_desc',
        'metadata__api_class_func_docs_url:url',
        'metadata__api_class_func_reference_example',
        'metadata__api_class_func_regex_find',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>