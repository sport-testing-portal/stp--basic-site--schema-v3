<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassFunc */

$this->title = $model->metadata__api_class_func;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Api Class Funcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-api-class-func-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'API Class Func'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->metadata__api_class_func_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->metadata__api_class_func_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->metadata__api_class_func_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        //'metadata__api_class_func_id',
        [
            'attribute' => 'metadata__api_class_func_id',
            'label' => 'ID',
        ],        
        [
            'attribute' => 'metadataApiClass.metadata__api_class',
            'label' => 'Metadata  Api Class',
        ],
        'metadata__api_class_func',
        'metadata__api_class_func_desc',
        'metadata__api_class_func_docs_url:url',
        'metadata__api_class_func_reference_example',
        'metadata__api_class_func_regex_find',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>API Class<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataApiClass = [
        'metadata__api_provider_id',
        'metadata__api_class',
        'metadata__api_class_desc',
        'metadata__api_class_docs_url',
        'metadata__api_reference_example',
        'metadata__api_regex_find',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->metadataApiClass,
        'attributes' => $gridColumnMetadataApiClass    ]);
    ?>
</div>
