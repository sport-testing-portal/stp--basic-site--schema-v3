<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassFunc */

$this->title = 'Save As New Metadata Api Class Func: '. ' ' . $model->metadata__api_class_func;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Api Class Funcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metadata__api_class_func, 'url' => ['view', 'id' => $model->metadata__api_class_func_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-api-class-func-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
