<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassFuncSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-api-class-func-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'metadata__api_class_func_id')->textInput(['placeholder' => 'Metadata  Api Class Func']) ?>

    <?= $form->field($model, 'metadata__api_class_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiClass::find()->orderBy('metadata__api_class_id')->asArray()->all(), 'metadata__api_class_id', 'metadata__api_class'),
        'options' => ['placeholder' => 'Choose Metadata  api class'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'metadata__api_class_func')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Class Func']) ?>

    <?= $form->field($model, 'metadata__api_class_func_desc')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Class Func Desc']) ?>

    <?= $form->field($model, 'metadata__api_class_func_docs_url')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Class Func Docs Url']) ?>

    <?php /* echo $form->field($model, 'metadata__api_class_func_reference_example')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Class Func Reference Example']) */ ?>

    <?php /* echo $form->field($model, 'metadata__api_class_func_regex_find')->textInput(['maxlength' => true, 'placeholder' => 'Metadata  Api Class Func Regex Find']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
