<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-data-hdr-xlation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'data_hdr_xlation_id')->textInput(['placeholder' => 'Data Hdr Xlation']) ?>

    <?= $form->field($model, 'data_hdr_xlation')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation']) ?>

    <?= $form->field($model, 'data_hdr_entity_name')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Entity Name']) ?>

    <?= $form->field($model, 'hdr_sha1_hash')->textInput(['maxlength' => true, 'placeholder' => 'Hdr Sha1 Hash']) ?>

    <?= $form->field($model, 'data_hdr_regex')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Regex']) ?>

    <?php /* echo $form->field($model, 'data_hdr_is_trash_yn')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Is Trash Yn']) */ ?>

    <?php /* echo $form->field($model, 'gsm_target_table_name')->textInput(['maxlength' => true, 'placeholder' => 'Gsm Target Table Name']) */ ?>

    <?php /* echo $form->field($model, 'data_hdr_entity_fetch_url')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Entity Fetch Url']) */ ?>

    <?php /* echo $form->field($model, 'data_hdr_xlation_visual_example')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation Visual Example']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
