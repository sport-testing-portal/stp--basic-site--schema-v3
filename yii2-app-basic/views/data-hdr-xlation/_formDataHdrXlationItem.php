<div class="form-group" id="add-data-hdr-xlation-item">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'DataHdrXlationItem',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'data_hdr_xlation_item_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'data_hdr_xlation_item_source_value' => ['type' => TabularForm::INPUT_TEXT],
        'data_hdr_xlation_item_target_value' => ['type' => TabularForm::INPUT_TEXT],
        'ordinal_position_num' => ['type' => TabularForm::INPUT_TEXT],
        'data_hdr_xlation_item_target_table' => ['type' => TabularForm::INPUT_TEXT],
        'data_hdr_source_value_type_name_source' => ['type' => TabularForm::INPUT_TEXT],
        'data_hdr_source_value_is_type_name' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowDataHdrXlationItem(' . $key . '); return false;', 'id' => 'data-hdr-xlation-item-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Data Hdr Xlation Item', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowDataHdrXlationItem()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

