<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlation */

?>
<div class="data-hdr-xlation-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->data_hdr_xlation) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'data_hdr_xlation_id',
        'data_hdr_xlation',
        'data_hdr_entity_name',
        'hdr_sha1_hash',
        'data_hdr_regex',
        'data_hdr_is_trash_yn',
        'gsm_target_table_name',
        'data_hdr_entity_fetch_url:url',
        'data_hdr_xlation_visual_example',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>