<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlation */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'DataHdrXlationItem', 
        'relID' => 'data-hdr-xlation-item', 
        'value' => \yii\helpers\Json::encode($model->dataHdrXlationItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="data-hdr-xlation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'data_hdr_xlation_id')->textInput(['placeholder' => 'Data Hdr Xlation']) ?>

    <?= $form->field($model, 'data_hdr_xlation')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation']) ?>

    <?= $form->field($model, 'data_hdr_entity_name')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Entity Name']) ?>

    <?= $form->field($model, 'hdr_sha1_hash')->textInput(['maxlength' => true, 'placeholder' => 'Hdr Sha1 Hash']) ?>

    <?= $form->field($model, 'data_hdr_regex')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Regex']) ?>

    <?= $form->field($model, 'data_hdr_is_trash_yn')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Is Trash Yn']) ?>

    <?= $form->field($model, 'gsm_target_table_name')->textInput(['maxlength' => true, 'placeholder' => 'Gsm Target Table Name']) ?>

    <?= $form->field($model, 'data_hdr_entity_fetch_url')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Entity Fetch Url']) ?>

    <?= $form->field($model, 'data_hdr_xlation_visual_example')->textInput(['maxlength' => true, 'placeholder' => 'Data Hdr Xlation Visual Example']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('DataHdrXlationItem'),
            'content' => $this->render('_formDataHdrXlationItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->dataHdrXlationItems),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
