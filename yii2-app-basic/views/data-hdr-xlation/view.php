<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlation */

$this->title = $model->data_hdr_xlation;
$this->params['breadcrumbs'][] = ['label' => 'Data Hdr Xlation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-hdr-xlation-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Data Hdr Xlation'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->data_hdr_xlation_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->data_hdr_xlation_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->data_hdr_xlation_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'data_hdr_xlation_id',
        'data_hdr_xlation',
        'data_hdr_entity_name',
        'hdr_sha1_hash',
        'data_hdr_regex',
        'data_hdr_is_trash_yn',
        'gsm_target_table_name',
        'data_hdr_entity_fetch_url:url',
        'data_hdr_xlation_visual_example',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerDataHdrXlationItem->totalCount){
    $gridColumnDataHdrXlationItem = [
        ['class' => 'yii\grid\SerialColumn'],
            'data_hdr_xlation_item_id',
                        'data_hdr_xlation_item_source_value',
            'data_hdr_xlation_item_target_value',
            'ordinal_position_num',
            'data_hdr_xlation_item_target_table',
            'data_hdr_source_value_type_name_source',
            'data_hdr_source_value_is_type_name',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerDataHdrXlationItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-data-hdr-xlation-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Data Hdr Xlation Item'),
        ],
        'export' => false,
        'columns' => $gridColumnDataHdrXlationItem
    ]);
}
?>

    </div>
</div>
