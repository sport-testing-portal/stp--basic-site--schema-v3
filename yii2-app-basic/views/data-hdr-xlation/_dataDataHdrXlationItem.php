<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->dataHdrXlationItems,
        'key' => 'data_hdr_xlation_item_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'data_hdr_xlation_item_id',
        'data_hdr_xlation_item_source_value',
        'data_hdr_xlation_item_target_value',
        'ordinal_position_num',
        'data_hdr_xlation_item_target_table',
        'data_hdr_source_value_type_name_source',
        'data_hdr_source_value_is_type_name',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'data-hdr-xlation-item'
        ],
    ];

    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        //'data_hdr_xlation_item_id',
        [
            'attribute'=>'data_hdr_xlation_item_id',
            'label'=>'ID',
            'width'=>'50px'
        ],
        //'data_hdr_xlation_item_source_value',
        [
            'attribute'=>'data_hdr_xlation_item_source_value',
            'label'=>'Source Value',
            'width'=>'150px'
        ],
        //'data_hdr_xlation_item_target_value',
        [
            'attribute'=>'data_hdr_xlation_item_target_value',
            'label'=>'Target Value',
            'width'=>'150px'
        ],
        //'ordinal_position_num',
        [
            'attribute'=>'ordinal_position_num',
            'label'=>'Ordinal Pos#',
            'width'=>'50px'
        ],
        //'data_hdr_xlation_item_target_table',
        [
            'attribute'=>'data_hdr_xlation_item_target_table',
            'label'=>'Target Table',
            'width'=>'80px'
        ],
        //'data_hdr_source_value_type_name_source',
        [
            'attribute'=>'data_hdr_source_value_type_name_source',
            'label'=>'SrcVal Type Name Source',
            'width'=>'75px'
        ],
        //'data_hdr_source_value_is_type_name',
        [
            'attribute'=>'data_hdr_source_value_is_type_name',
            'label'=>'SrcVal Is Type Name?',
            'width'=>'50px'
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'data-hdr-xlation-item'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
