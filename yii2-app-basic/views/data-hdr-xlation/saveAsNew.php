<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlation */

$this->title = 'Save As New Data Hdr Xlation: '. ' ' . $model->data_hdr_xlation;
$this->params['breadcrumbs'][] = ['label' => 'Data Hdr Xlation', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->data_hdr_xlation, 'url' => ['view', 'id' => $model->data_hdr_xlation_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="data-hdr-xlation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
