<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataHdrXlation */

$this->title = 'Create Data Hdr Xlation';
$this->params['breadcrumbs'][] = ['label' => 'Data Hdr Xlation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-hdr-xlation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
