<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseCec */

$this->title = 'Update Metadata Database Cec: ' . ' ' . $model->database_cec_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Database Cecs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->database_cec_id, 'url' => ['view', 'id' => $model->database_cec_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-database-cec-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
