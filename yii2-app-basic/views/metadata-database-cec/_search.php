<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseCecSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-database-cec-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'database_cec_id')->textInput(['placeholder' => 'Database Cec']) ?>

    <?= $form->field($model, 'database_cec')->textInput(['maxlength' => true, 'placeholder' => 'Database Cec']) ?>

    <?= $form->field($model, 'database_cec_desc_short')->textInput(['maxlength' => true, 'placeholder' => 'Database Cec Desc Short']) ?>

    <?= $form->field($model, 'database_cec_desc_long')->textInput(['maxlength' => true, 'placeholder' => 'Database Cec Desc Long']) ?>

    <?= $form->field($model, 'cec_status_tag')->textInput(['maxlength' => true, 'placeholder' => 'Cec Status Tag']) ?>

    <?php /* echo $form->field($model, 'cec_scope')->textInput(['maxlength' => true, 'placeholder' => 'Cec Scope']) */ ?>

    <?php /* echo $form->field($model, 'rule_file_uri')->textInput(['maxlength' => true, 'placeholder' => 'Rule File Uri']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
