<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseCec */

?>
<div class="metadata-database-cec-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->database_cec_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'database_cec_id',
        'database_cec',
        'database_cec_desc_short',
        'database_cec_desc_long',
        'cec_status_tag',
        'cec_scope',
        'rule_file_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>