<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseCec */

$this->title = $model->database_cec_id;
// next line by dbyrd
//$this->title = $model->database_cec;
$this->params['breadcrumbs'][] = ['label' => 'Database CEC', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-database-cec-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Database CEC'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->database_cec_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->database_cec_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->database_cec_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        //'database_cec_id',
        [
            'attribute'=>'database_cec_id',
            'label'=>'ID',
            //'width'=>'50px'
        ],          
        //'database_cec',
        [
            'attribute'=>'database_cec',
            'label'=>'CEC Name',
            //'width'=>'50px'
        ],
        //'database_cec_desc_short',
        [
            'attribute'=>'database_cec_desc_short',
            'label'=>'Short Description',
            //'width'=>'50px'
        ],
        //'database_cec_desc_long',
        [
            'attribute'=>'database_cec_desc_long',
            'label'=>'Long Description',
            //'width'=>'50px'
        ],
        //'cec_status_tag',
        [
            'attribute'=>'cec_status_tag',
            'label'=>'CEC Status Tag',
            //'width'=>'50px'
        ],
        //'cec_scope',
        [
            'attribute'=>'cec_scope',
            'label'=>'CEC Scope',
            //'width'=>'50px'
        ],
        //'rule_file_uri',
        [
            'attribute'=>'rule_file_uri',
            'label'=>'Rule File URI',
            //'width'=>'150px'
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerMetadataDatabaseCecStatus->totalCount){
    $gridColumnMetadataDatabaseCecStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            'cec_status_id',
            [
                'attribute' => 'database.database_id',
                'label' => 'Database'
            ],
                        'cec_status',
            'cec_status_tag',
            'cec_status_bfr',
            'cec_status_at',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataDatabaseCecStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--database-cec-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Database Cec Status'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataDatabaseCecStatus
    ]);
}
?>

    </div>
</div>
