<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseCec */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataDatabaseCecStatus', 
        'relID' => 'metadata-database-cec-status', 
        'value' => \yii\helpers\Json::encode($model->metadataDatabaseCecStatuses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="metadata-database-cec-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'database_cec_id')->textInput(['placeholder' => 'Database Cec']) ?>

    <?= $form->field($model, 'database_cec')->textInput(['maxlength' => true, 'placeholder' => 'Database Cec']) ?>

    <?= $form->field($model, 'database_cec_desc_short')->textInput(['maxlength' => true, 'placeholder' => 'Database Cec Desc Short']) ?>

    <?= $form->field($model, 'database_cec_desc_long')->textInput(['maxlength' => true, 'placeholder' => 'Database Cec Desc Long']) ?>

    <?= $form->field($model, 'cec_status_tag')->textInput(['maxlength' => true, 'placeholder' => 'Cec Status Tag']) ?>

    <?= $form->field($model, 'cec_scope')->textInput(['maxlength' => true, 'placeholder' => 'Cec Scope']) ?>

    <?= $form->field($model, 'rule_file_uri')->textInput(['maxlength' => true, 'placeholder' => 'Rule File Uri']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MetadataDatabaseCecStatus'),
            'content' => $this->render('_formMetadataDatabaseCecStatus', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataDatabaseCecStatuses),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
