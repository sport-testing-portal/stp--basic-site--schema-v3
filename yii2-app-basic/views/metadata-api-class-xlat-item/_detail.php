<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlatItem */
/* @calledBy _expand */
/* @calls DetailView::widget */

?>
<div class="metadata-api-class-xlat-item-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->metadata__api_class_xlat_item) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'metadata__api_class_xlat_item_id',
        [
            'attribute'=>'metadata__api_class_xlat_item_id',
            'label'=>'ID',
            'width'=>'50px'
        ],        
        [
            'attribute' => 'metadataApiClassXlat.metadata__api_class_xlat',
            'label' => 'API Class Xlat',
        ],
        //'metadata__api_class_xlat_item',
        [
            'attribute'=>'metadata__api_class_xlat_item',
            'label'=>'API Class Xlat Item Name',
            'width'=>'150px'
        ],        
        //'metadata__api_class_func_1_reference_example',
        [
            'attribute'=>'metadata__api_class_func_1_reference_example',
            'label'=>'API Class Func1 Str Ref Example',
            'width'=>'150px'
        ],        
        //'metadata__api_class_func_2_reference_example',
        [
            'attribute'=>'metadata__api_class_func_2_reference_example',
            'label'=>'API Class Func2 Str Ref Example',
            'width'=>'150px'
        ],        
        //'metadata__api_class_func_1_regex_find',
        [
            'attribute'=>'metadata__api_class_func_1_regex_find',
            'label'=>'API Class Func1 Rgx Ref Example',
            'width'=>'150px'
        ],        
        //'metadata__api_class_func_2_regex_find',
        [
            'attribute'=>'metadata__api_class_func_2_regex_find',
            'label'=>'API Class Func2 Rgx Ref Example',
            'width'=>'150px'
        ],         
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>