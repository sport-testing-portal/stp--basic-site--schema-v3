<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlatItem */

$this->title = $model->metadata__api_class_xlat_item;
$this->params['breadcrumbs'][] = ['label' => 'API Class Xlat Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-api-class-xlat-item-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'API Class Xlat Item'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->metadata__api_class_xlat_item_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->metadata__api_class_xlat_item_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->metadata__api_class_xlat_item_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        //'metadata__api_class_xlat_item_id',
        [
            'attribute' => 'metadata__api_class_xlat_item_id',
            'label' => 'ID',
        ],
        [
            'attribute' => 'metadataApiClassXlat.metadata__api_class_xlat',
            'label' => 'API Class Xlat',
        ],
        'metadata__api_class_xlat_item',
        'metadata__api_class_func_1_reference_example',
        'metadata__api_class_func_2_reference_example',
        'metadata__api_class_func_1_regex_find',
        'metadata__api_class_func_2_regex_find',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>API Class Xlation<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataApiClassXlat = [
        'metadata__api_class_id_1',
        'metadata__api_class_id_2',
        'metadata__api_class_xlat',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->metadataApiClassXlat,
        'attributes' => $gridColumnMetadataApiClassXlat    ]);
    ?>
</div>
