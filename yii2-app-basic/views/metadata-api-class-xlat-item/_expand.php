<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
/* @calls _detail */
/* @loads TabsX::widget */
/* @internal purpose loads a tabx widget using content from //_detail */
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('API Class Xlation Item'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
