<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlatItem */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="metadata-api-class-xlat-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'metadata__api_class_xlat_item_id')->textInput(['placeholder' => 'API Class Xlat Item']) ?>

    <?= $form->field($model, 'metadata__api_class_xlat_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiClassXlat::find()
            ->orderBy('metadata__api_class_xlat_id')->asArray()->all(),
            'metadata__api_class_xlat_id', 'metadata__api_class_xlat'),
        'options' => ['placeholder' => 'Choose API class xlat'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'metadata__api_class_xlat_item')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Xlat Item']) ?>

    <?= $form->field($model, 'metadata__api_class_func_1_reference_example')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Function 1 Reference Example']) ?>

    <?= $form->field($model, 'metadata__api_class_func_2_reference_example')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Function 2 Reference Example']) ?>

    <?= $form->field($model, 'metadata__api_class_func_1_regex_find')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Function 1 Regex Find']) ?>

    <?= $form->field($model, 'metadata__api_class_func_2_regex_find')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Function 2 Regex Find']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
