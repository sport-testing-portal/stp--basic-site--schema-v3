<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlatItem */

$this->title = 'Save As New Metadata Api Class Xlat Item: '. ' ' . $model->metadata__api_class_xlat_item;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Api Class Xlat Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metadata__api_class_xlat_item, 'url' => ['view', 'id' => $model->metadata__api_class_xlat_item_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-api-class-xlat-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
