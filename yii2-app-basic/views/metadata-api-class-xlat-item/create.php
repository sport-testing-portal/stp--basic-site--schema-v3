<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlatItem */

$this->title = 'Create API Class Xlat Item';
$this->params['breadcrumbs'][] = ['label' => 'API Class Xlat Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-api-class-xlat-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
