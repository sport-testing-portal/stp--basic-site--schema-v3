<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlatItem */

$this->title = 'Update API Class Xlat Item: ' . ' ' . $model->metadata__api_class_xlat_item;
$this->params['breadcrumbs'][] = ['label' => 'API Class Xlat Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metadata__api_class_xlat_item, 'url' => ['view', 'id' => $model->metadata__api_class_xlat_item_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-api-class-xlat-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
