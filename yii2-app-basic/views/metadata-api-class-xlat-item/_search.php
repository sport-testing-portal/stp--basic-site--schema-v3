<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlatItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-api-class-xlat-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'metadata__api_class_xlat_item_id')
        ->textInput(['placeholder' => 'API Class Xlat Item']) ?>

    <?= $form->field($model, 'metadata__api_class_xlat_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiClassXlat::find()
            ->orderBy('metadata__api_class_xlat_id')->asArray()->all(),
            'metadata__api_class_xlat_id', 'metadata__api_class_xlat'),
        'options' => ['placeholder' => 'Choose Metadata  api class xlat'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'metadata__api_class_xlat_item')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Xlat Item']) ?>

    <?= $form->field($model, 'metadata__api_class_func_1_reference_example')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Func 1 Reference Example']) ?>

    <?= $form->field($model, 'metadata__api_class_func_2_reference_example')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Func 2 Reference Example']) ?>

    <?= $form->field($model, 'metadata__api_class_func_1_regex_find')
        ->textInput(['maxlength' => true,
            'placeholder' => 'API Class Func 1 Regex Find']) ?>

    <?php echo $form->field($model, 'metadata__api_class_func_2_regex_find')
        ->textInput(['maxlength' => true, 
            'placeholder' => 'API Class Func 2 Regex Find']) ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
