<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\MetadataApiClassXlatItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'API Class Xlat Items';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="metadata-api-class-xlat-item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create API Xlation Item', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        //'metadata__api_class_xlat_item_id',
        [
            'attribute'=>'metadata__api_class_xlat_item_id',
            'label'=>'ID',
            'width'=>'50px'
        ],
        [
                'attribute' => 'metadata__api_class_xlat_id',
                'label' => 'API Class Xlat',
                'value' => function($model){                   
                    return $model->metadataApiClassXlat->metadata__api_class_xlat;                   
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiClassXlat::find()
                    ->asArray()->all(), 'metadata__api_class_xlat_id', 'metadata__api_class_xlat'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true, 'dropdownAutoWidth' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'API class xlat', 'id' => 'grid-metadata-api-class-xlat-item-search-metadata__api_class_xlat_id']
            ],
        //'metadata__api_class_xlat_item',
        [
            'attribute'=>'metadata__api_class_xlat_item',
            'label'=>'API Class Xlat Item Name',
            'width'=>'150px'
        ],
        //'metadata__api_class_func_1_reference_example',
        [
            'attribute'=>'metadata__api_class_func_1_reference_example',
            'label'=>'API Class Func1 Str Ref Example',
            'width'=>'150px'
        ],                    
        //'metadata__api_class_func_2_reference_example',
        [
            'attribute'=>'metadata__api_class_func_2_reference_example',
            'label'=>'API Class Func2 Str Ref Example',
            'width'=>'150px'
        ],                    
        //'metadata__api_class_func_1_regex_find',
        [
            'attribute'=>'metadata__api_class_func_1_regex_find',
            'label'=>'API Class Func1 Rgx Ref Example',
            'width'=>'150px'
        ],                    
        //'metadata__api_class_func_2_regex_find',
        [
            'attribute'=>'metadata__api_class_func_2_regex_find',
            'label'=>'API Class Func2 Rgx Ref Example',
            'width'=>'150px'
        ],                    
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata-api-class-xlat-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
