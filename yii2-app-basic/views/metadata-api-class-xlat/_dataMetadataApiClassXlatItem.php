<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
/* @calledBy _expand */
/* @calls GridView::widget */

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->metadataApiClassXlatItems,
        'key' => 'metadata__api_class_xlat_item_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
//        'metadata__api_class_xlat_item_id',
        [
            'attribute'=>'metadata__api_class_xlat_item_id',
            'label'=>'ID'
        ],
        //'metadata__api_class_xlat_item',
        [
            'attribute'=>'metadata__api_class_xlat_item',
            'label'=>'API Class Xlat Item Name',
            'width'=>'150px'
        ],
        //'metadata__api_class_func_1_reference_example',
        [
            'attribute'=>'metadata__api_class_func_1_reference_example',
            'label'=>'API Class Func1 Str Ref Example',
            'width'=>'150px'
        ],    
        //'metadata__api_class_func_2_reference_example',
        [
            'attribute'=>'metadata__api_class_func_2_reference_example',
            'label'=>'API Class Func2 Str Ref Example',
            'width'=>'150px'
        ],         
        //'metadata__api_class_func_1_regex_find',
        [
            'attribute'=>'metadata__api_class_func_1_regex_find',
            'label'=>'API Class Func1 Rgx Ref Example',
            'width'=>'150px'
        ],         
        //'metadata__api_class_func_2_regex_find',
        [
            'attribute'=>'metadata__api_class_func_2_regex_find',
            'label'=>'API Class Func2 Rgx Ref Example',
            'width'=>'150px'
        ],        
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'metadata-api-class-xlat-item'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
