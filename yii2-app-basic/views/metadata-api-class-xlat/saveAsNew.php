<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlat */

$this->title = 'Save As New Metadata Api Class Xlat: '. ' ' . $model->metadata__api_class_xlat;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Api Class Xlats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metadata__api_class_xlat, 'url' => ['view', 'id' => $model->metadata__api_class_xlat_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-api-class-xlat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
