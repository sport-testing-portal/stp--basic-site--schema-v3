<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlat */
/* @calls _form */

$this->title = 'Update API Class Xlat: ' . ' ' . $model->metadata__api_class_xlat;
$this->params['breadcrumbs'][] = ['label' => 'API Class Xlats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metadata__api_class_xlat,
    'url' => ['view', 'id' => $model->metadata__api_class_xlat_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-api-class-xlat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
