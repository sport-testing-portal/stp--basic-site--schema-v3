<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
/* @calls: _detail, _dataMetadataApiClassXlatItem */
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('API Class Xlation'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('API Class Xlat Items'),
        'content' => $this->render('_dataMetadataApiClassXlatItem', [
            'model' => $model,
            'row' => $model->metadataApiClassXlatItems,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
