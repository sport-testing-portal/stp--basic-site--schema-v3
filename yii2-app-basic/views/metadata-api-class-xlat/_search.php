<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlatSearch */
/* @var $form yii\widgets\ActiveForm */
/* @since 0.8.0 */
?>

<div class="form-metadata-api-class-xlat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'metadata__api_class_xlat_id')
        ->textInput(['placeholder' => 'API Class Xlat ID']) ?>

    <?= $form->field($model, 'metadata__api_class_id_1')
        ->widget(\kartik\widgets\Select2::classname(), [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiClass::find()
                ->where(['metadata__api_provider_id'=>1])
                ->orderBy('metadata__api_class_id')
                ->asArray()->all(), 'metadata__api_class_id', 'metadata__api_class'),
            'options' => ['placeholder' => 'Choose API class'],
            'pluginOptions' => [
                'allowClear' => true, 'dropdownAutoWidth' => true
            ],
    ]); ?>

    <?php // Add a second drop down ?>
    <?php //$form->field($model, 'metadata__api_class_id_2')->textInput(['placeholder' => 'Metadata  Api Class Id 2']) ?>

    <?= $form->field($model, 'metadata__api_class_id_2')
        ->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiClass::find()
            ->where(['metadata__api_provider_id'=>2])
            ->orderBy('metadata__api_class_id')
            ->asArray()->all(), 'metadata__api_class_id', 'metadata__api_class'),
        'options' => ['placeholder' => 'Choose API class'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>
    
    <?= $form->field($model, 'metadata__api_class_xlat')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Xlat']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])
        ->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
