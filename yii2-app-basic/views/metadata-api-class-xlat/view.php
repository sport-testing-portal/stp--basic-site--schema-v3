<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlat */

$this->title = $model->metadata__api_class_xlat;
$this->params['breadcrumbs'][] = ['label' => 'API Class Xlation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-api-class-xlat-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'API Class Xlat'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->metadata__api_class_xlat_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->metadata__api_class_xlat_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->metadata__api_class_xlat_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'metadata__api_class_xlat_id',
        [
            'attribute' => 'metadataApiClassId1.metadata__api_class',
            'label' => 'Metadata  Api Class Id 1',
        ],
        'metadata__api_class_id_2',
        'metadata__api_class_xlat',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MetadataApiClass<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataApiClass = [
        'metadata__api_class_id',
        'metadata__api_provider_id',
        'metadata__api_class',
        'metadata__api_class_desc',
        'metadata__api_class_docs_url',
        'metadata__api_reference_example',
        'metadata__api_regex_find',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->metadataApiClassId1,
        'attributes' => $gridColumnMetadataApiClass    ]);
    ?>
    
    <div class="row">
<?php
if($providerMetadataApiClassXlatItem->totalCount){
    $gridColumnMetadataApiClassXlatItem = [
        ['class' => 'yii\grid\SerialColumn'],
            'metadata__api_class_xlat_item_id',
                        'metadata__api_class_xlat_item',
            'metadata__api_class_func_1_reference_example',
            'metadata__api_class_func_2_reference_example',
            'metadata__api_class_func_1_regex_find',
            'metadata__api_class_func_2_regex_find',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataApiClassXlatItem,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--api-class-xlat-item']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Api Class Xlat Item'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataApiClassXlatItem
    ]);
}
?>

    </div>
</div>
