<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlat */
/* @calls _form */

$this->title = 'Create API Class Xlation';
$this->params['breadcrumbs'][] = ['label' => 'API Class Xlations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-api-class-xlat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
