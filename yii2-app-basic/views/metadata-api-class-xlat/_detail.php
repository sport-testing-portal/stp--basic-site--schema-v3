<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlat */
/* @calledBy: _expand  */
/* @calls: DetailView::widget */
/* @since 0.8.0 */

?>
<div class="metadata-api-class-xlat-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->metadata__api_class_xlat) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        //'metadata__api_class_xlat_id',
        [
            'attribute' => 'metadata__api_class_xlat_id',
            'label' => 'ID',
        ],        
        [
            'attribute' => 'metadataApiClassId1.metadata__api_class',
            'label' => 'API Class ID 1',
        ],
        //'metadata__api_class_id_2',
        [
            'attribute' => 'metadataApiClassId2.metadata__api_class',
            'label' => 'API Class ID 2',
        ],        
        //'metadata__api_class_xlat',
        [
            'attribute' => 'metadata__api_class_xlat',
            'label' => 'API Class Xlat Name',
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>