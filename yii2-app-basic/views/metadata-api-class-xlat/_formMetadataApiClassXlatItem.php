<div class="form-group" id="add-metadata-api-class-xlat-item">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;
/* @calledBy _form */
/* @loads TabularForm::widget, */

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'MetadataApiClassXlatItem',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'metadata__api_class_xlat_item_id' => [
            'type' => TabularForm::INPUT_HIDDEN,
            'label' => 'ID'],
        'metadata__api_class_xlat_item' => [
            'type' => TabularForm::INPUT_TEXT,
            'label' => 'API Class Xlat Item Name'],
        'metadata__api_class_func_1_reference_example' => [
            'type' => TabularForm::INPUT_TEXT,
            'label' => 'Class Func1 Str Find'],
        'metadata__api_class_func_2_reference_example' => [
            'type' => TabularForm::INPUT_TEXT,
            'label' => 'Class Func2 Str Find'],
        'metadata__api_class_func_1_regex_find' => [
            'type' => TabularForm::INPUT_TEXT,
            'label' => 'Class Func1 Rgx Find'],
        'metadata__api_class_func_2_regex_find' => [
            'type' => TabularForm::INPUT_TEXT,
            'label' => 'Class Func2 Rgx Find'],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMetadataApiClassXlatItem(' . $key . '); return false;', 'id' => 'metadata-api-class-xlat-item-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add API Class Xlat Item',
                ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowMetadataApiClassXlatItem()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

