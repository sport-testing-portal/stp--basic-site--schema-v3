<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataApiClassXlat */
/* @var $form yii\widgets\ActiveForm */
/* @calls _formMetadataApiClassXlatItem */
/* @calls javascript function addRowMetadataApiClassXlatItem() */
/* @loads ActiveForm, kartik\tabs\TabsX::widget */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataApiClassXlatItem', 
        'relID' => 'metadata-api-class-xlat-item', 
        'value' => \yii\helpers\Json::encode($model->metadataApiClassXlatItems),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="metadata-api-class-xlat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'metadata__api_class_xlat_id')
        ->textInput(['placeholder' => 'API Class Xlat']) ?>

    <?= $form->field($model, 'metadata__api_class_id_1')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiClass::find()
            ->where(['metadata__api_provider_id'=>1])->orderBy('metadata__api_class_id')->asArray()->all(), 'metadata__api_class_id', 'metadata__api_class'),
        'options' => ['placeholder' => 'Choose API Class'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    
    <?php //$form->field($model, 'metadata__api_class_id_2')->textInput(['placeholder' => 'Metadata  Api Class Id 2']) ?>

    <?= $form->field($model, 'metadata__api_class_id_2')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataApiClass::find()
            ->where(['metadata__api_provider_id'=>2])->orderBy('metadata__api_class_id')->asArray()->all(), 
            'metadata__api_class_id', 'metadata__api_class'),
        'options' => ['placeholder' => 'Choose API Class'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>    
    
    <?= $form->field($model, 'metadata__api_class_xlat')
        ->textInput(['maxlength' => true, 'placeholder' => 'API Class Xlat']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MetadataApiClassXlatItem'),
            'content' => $this->render('_formMetadataApiClassXlatItem', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataApiClassXlatItems),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
