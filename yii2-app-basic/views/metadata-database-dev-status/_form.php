<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDevStatus */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="metadata-database-dev-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'dev_status_id')->textInput(['placeholder' => 'Dev Status']) ?>

    <?= $form->field($model, 'database_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabase::find()->orderBy('database_id')->asArray()->all(), 'database_id', 'database_id'),
        'options' => ['placeholder' => 'Choose Metadata  database'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'database_dev_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabaseDev::find()->orderBy('database_dev_id')->asArray()->all(), 'database_dev_id', 'database_dev_id'),
        'options' => ['placeholder' => 'Choose Metadata  database dev'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'dev_status')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status']) ?>

    <?= $form->field($model, 'dev_status_tag')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status Tag']) ?>

    <?= $form->field($model, 'dev_status_bfr')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status Bfr']) ?>

    <?= $form->field($model, 'dev_status_at')->textInput(['placeholder' => 'Dev Status At']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
