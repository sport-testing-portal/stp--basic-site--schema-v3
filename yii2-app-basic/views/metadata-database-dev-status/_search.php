<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDevStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-database-dev-status-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'dev_status_id')->textInput(['placeholder' => 'Dev Status']) ?>

    <?= $form->field($model, 'database_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabase::find()->orderBy('database_id')->asArray()->all(), 'database_id', 'database_id'),
        'options' => ['placeholder' => 'Choose Metadata  database'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'database_dev_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabaseDev::find()->orderBy('database_dev_id')->asArray()->all(), 'database_dev_id', 'database_dev_id'),
        'options' => ['placeholder' => 'Choose Metadata  database dev'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'dev_status')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status']) ?>

    <?= $form->field($model, 'dev_status_tag')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status Tag']) ?>

    <?php /* echo $form->field($model, 'dev_status_bfr')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status Bfr']) */ ?>

    <?php /* echo $form->field($model, 'dev_status_at')->textInput(['placeholder' => 'Dev Status At']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
