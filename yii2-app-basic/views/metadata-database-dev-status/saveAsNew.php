<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDevStatus */

$this->title = 'Save As New Metadata Database Dev Status: '. ' ' . $model->dev_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Database Dev Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dev_status_id, 'url' => ['view', 'id' => $model->dev_status_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-database-dev-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
