<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\MetadataDatabaseDevStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Database Development Status';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="metadata-database-dev-status-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Database Development Status', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        // dbyrd hide id columns
        //'dev_status_id',
        ['attribute' => 'dev_status_id', 'visible' => false],                
        [
                'attribute' => 'database_id',
                'label' => 'Database',
//                'value' => function($model){                   
//                    return $model->database->database_id;                   
//                },
                'value' => function($model){                   
                    return $model->database->table_name;                   
                },                    
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabase::find()->asArray()->all(), 'database_id', 'table_name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true, 'dropdownAutoWidth' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'database table name', 'id' => 'grid-metadata-database-dev-status-search-database_id']
            ],
        [
                'attribute' => 'database_dev_id',
                'label' => 'Database Dev',
//                'value' => function($model){                   
//                    return $model->databaseDev->database_dev_id;                   
//                },
                'value' => function($model){                   
                    return $model->databaseDev->database_dev;                   
                },                    
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\MetadataDatabaseDev::find()->asArray()->all(), 'database_dev_id', 'database_dev'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true, 'dropdownAutoWidth' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'development status', 'id' => 'grid-metadata-database-dev-status-search-database_dev_id']
            ],
        'dev_status',
        'dev_status_tag',
        'dev_status_bfr',
        'dev_status_at',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata-database-dev-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
