<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDevStatus */

?>
<div class="metadata-database-dev-status-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->dev_status_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'dev_status_id',
        [
            'attribute' => 'database.database_id',
            'label' => 'Database',
        ],
        [
            'attribute' => 'databaseDev.database_dev_id',
            'label' => 'Database Dev',
        ],
        'dev_status',
        'dev_status_tag',
        'dev_status_bfr',
        'dev_status_at',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>