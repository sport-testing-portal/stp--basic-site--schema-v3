<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDevStatus */

$this->title = $model->dev_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Database Dev Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-database-dev-status-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Metadata Database Dev Status'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->dev_status_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->dev_status_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->dev_status_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'dev_status_id',
        [
            'attribute' => 'database.database_id',
            'label' => 'Database',
        ],
        [
            'attribute' => 'databaseDev.database_dev_id',
            'label' => 'Database Dev',
        ],
        'dev_status',
        'dev_status_tag',
        'dev_status_bfr',
        'dev_status_at',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MetadataDatabase<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataDatabase = [
        'table_name',
        'schema_name',
        'col_name_len_max',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->database,
        'attributes' => $gridColumnMetadataDatabase    ]);
    ?>
    <div class="row">
        <h4>MetadataDatabaseDev<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMetadataDatabaseDev = [
        'database_dev',
        'database_dev_desc_short',
        'database_dev_desc_long',
        'dev_status_tag',
        'dev_scope',
        'rule_file_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->databaseDev,
        'attributes' => $gridColumnMetadataDatabaseDev    ]);
    ?>
</div>
