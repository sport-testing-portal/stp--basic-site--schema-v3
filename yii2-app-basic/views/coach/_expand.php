<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Coach'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Coach'),
        'content' => $this->render('_dataCampCoach', [
            'model' => $model,
            'row' => $model->campCoaches,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Session Coach'),
        'content' => $this->render('_dataCampSessionCoach', [
            'model' => $model,
            'row' => $model->campSessionCoaches,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Session Player Eval'),
        'content' => $this->render('_dataCampSessionPlayerEval', [
            'model' => $model,
            'row' => $model->campSessionPlayerEvals,
        ]),
    ],
                            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Coach Sport'),
        'content' => $this->render('_dataCoachSport', [
            'model' => $model,
            'row' => $model->coachSports,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School Coach'),
        'content' => $this->render('_dataSchoolCoach', [
            'model' => $model,
            'row' => $model->schoolCoaches,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team Coach'),
        'content' => $this->render('_dataTeamCoach', [
            'model' => $model,
            'row' => $model->teamCoaches,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
