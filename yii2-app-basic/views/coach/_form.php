<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Coach */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampCoach', 
        'relID' => 'camp-coach', 
        'value' => \yii\helpers\Json::encode($model->campCoaches),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampSessionCoach', 
        'relID' => 'camp-session-coach', 
        'value' => \yii\helpers\Json::encode($model->campSessionCoaches),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampSessionPlayerEval', 
        'relID' => 'camp-session-player-eval', 
        'value' => \yii\helpers\Json::encode($model->campSessionPlayerEvals),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CoachSport', 
        'relID' => 'coach-sport', 
        'value' => \yii\helpers\Json::encode($model->coachSports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SchoolCoach', 
        'relID' => 'school-coach', 
        'value' => \yii\helpers\Json::encode($model->schoolCoaches),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'TeamCoach', 
        'relID' => 'team-coach', 
        'value' => \yii\helpers\Json::encode($model->teamCoaches),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="coach-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'coach_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamCoach::find()->orderBy('coach_id')->asArray()->all(), 'coach_id', 'team_coach_id'),
        'options' => ['placeholder' => 'Choose Team coach'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'person_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Person::find()->orderBy('person_id')->asArray()->all(), 'person_id', 'person'),
        'options' => ['placeholder' => 'Choose Person'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'coach_type_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\CoachType::find()->orderBy('coach_type_id')->asArray()->all(), 'coach_type_id', 'coach_type'),
        'options' => ['placeholder' => 'Choose Coach type'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'coach_team_coach_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamCoach::find()->orderBy('team_coach_id')->asArray()->all(), 'team_coach_id', 'team_coach_id'),
        'options' => ['placeholder' => 'Choose Team coach'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'age_group_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\AgeGroup::find()->orderBy('age_group_id')->asArray()->all(), 'age_group_id', 'age_group'),
        'options' => ['placeholder' => 'Choose Age group'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'coach_specialty')->textInput(['maxlength' => true, 'placeholder' => 'Coach Specialty']) ?>

    <?= $form->field($model, 'coach_certifications')->textInput(['maxlength' => true, 'placeholder' => 'Coach Certifications']) ?>

    <?= $form->field($model, 'coach_comments')->textInput(['maxlength' => true, 'placeholder' => 'Coach Comments']) ?>

    <?= $form->field($model, 'coach_qrcode_uri')->textInput(['maxlength' => true, 'placeholder' => 'Coach Qrcode Uri']) ?>

    <?= $form->field($model, 'coach_info_source_scrape_url')->textInput(['maxlength' => true, 'placeholder' => 'Coach Info Source Scrape Url']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampCoach'),
            'content' => $this->render('_formCampCoach', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campCoaches),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampSessionCoach'),
            'content' => $this->render('_formCampSessionCoach', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campSessionCoaches),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampSessionPlayerEval'),
            'content' => $this->render('_formCampSessionPlayerEval', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campSessionPlayerEvals),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CoachSport'),
            'content' => $this->render('_formCoachSport', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->coachSports),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SchoolCoach'),
            'content' => $this->render('_formSchoolCoach', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->schoolCoaches),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('TeamCoach'),
            'content' => $this->render('_formTeamCoach', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teamCoaches),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
