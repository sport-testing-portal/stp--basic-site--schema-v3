<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Note */

$this->title = $model->note;
$this->params['breadcrumbs'][] = ['label' => 'Notes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="note-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Note'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->note_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->note_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->note_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'note_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'person.person',
            'label' => 'Person',
        ],
        [
            'attribute' => 'noteType.note_type',
            'label' => 'Note Type',
        ],
        'note',
        'note_tags',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>NoteType<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnNoteType = [
        'note_type',
        'display_order',
        'note_type_desc_short',
        'note_type_desc_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->noteType,
        'attributes' => $gridColumnNoteType    ]);
    ?>
    <div class="row">
        <h4>Org<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrg = [
        'org_type_id',
        'org_level_id',
        'org',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url',
        'org_twitter_url',
        'org_facebook_url',
        'org_phone_main',
        'org_email_main',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->org,
        'attributes' => $gridColumnOrg    ]);
    ?>
    <div class="row">
        <h4>Person<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPerson = [
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        'app_user_id',
        'person_type_id',
        'user_id',
        'person_name_prefix',
        'person_name_first',
        'person_name_middle',
        'person_name_last',
        'person_name_suffix',
        'person',
        'person_phone_personal',
        'person_email_personal',
        'person_phone_work',
        'person_email_work',
        'person_position_work',
        'gender_id',
        'person_image_headshot_url',
        'person_name_nickname',
        'person_date_of_birth',
        'person_height',
        'person_weight',
        'person_tshirt_size',
        'person_high_school__graduation_year',
        'person_college_graduation_year',
        'person_college_commitment_status',
        'person_addr_1',
        'person_addr_2',
        'person_addr_3',
        'person_city',
        'person_postal_code',
        'person_country',
        'person_country_code',
        'person_state_or_region',
        'org_affiliation_begin_dt',
        'org_affiliation_end_dt',
        'person_website',
        'person_profile_url',
        'person_profile_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->person,
        'attributes' => $gridColumnPerson    ]);
    ?>
</div>
