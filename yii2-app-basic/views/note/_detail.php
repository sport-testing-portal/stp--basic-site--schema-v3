<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Note */

?>
<div class="note-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->note) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'note_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        [
            'attribute' => 'person.person',
            'label' => 'Person',
        ],
        [
            'attribute' => 'noteType.note_type',
            'label' => 'Note Type',
        ],
        'note',
        'note_tags',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>