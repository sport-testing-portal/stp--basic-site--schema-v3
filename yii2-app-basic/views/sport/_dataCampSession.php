<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->campSessions,
        'key' => 'camp_session_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'camp_session_id',
        [
                'attribute' => 'camp.camp',
                'label' => 'Camp'
            ],
        [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
        [
                'attribute' => 'address.address_id',
                'label' => 'Address'
            ],
        [
                'attribute' => 'season.season',
                'label' => 'Season'
            ],
        'camp_session_location',
        'camp_session_url:url',
        'camp_session_ages',
        'camp_session_skill_level',
        'camp_session_type',
        'camp_session_begin_dt',
        'camp_session_end_dt',
        'camp_session_specialty',
        'camp_session_scholarships_available_yn',
        'camp_session_description',
        'camp_session_cost_regular_residential',
        'camp_session_cost_regular_commuter',
        'camp_session_cost_regular_day',
        'camp_session_cost_early_residential',
        'camp_session_cost_early_commuter',
        'camp_session_cost_early_day',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'camp-session'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
