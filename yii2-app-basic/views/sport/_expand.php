<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Sport'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Session'),
        'content' => $this->render('_dataCampSession', [
            'model' => $model,
            'row' => $model->campSessions,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Sport'),
        'content' => $this->render('_dataCampSport', [
            'model' => $model,
            'row' => $model->campSports,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Coach Sport'),
        'content' => $this->render('_dataCoachSport', [
            'model' => $model,
            'row' => $model->coachSports,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Governing Body'),
        'content' => $this->render('_dataGoverningBody', [
            'model' => $model,
            'row' => $model->governingBodies,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Sport'),
        'content' => $this->render('_dataPlayerSport', [
            'model' => $model,
            'row' => $model->playerSports,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School Sport'),
        'content' => $this->render('_dataSchoolSport', [
            'model' => $model,
            'row' => $model->schoolSports,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Sport Gender'),
        'content' => $this->render('_dataSportGender', [
            'model' => $model,
            'row' => $model->sportGenders,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Sport Position'),
        'content' => $this->render('_dataSportPosition', [
            'model' => $model,
            'row' => $model->sportPositions,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team'),
        'content' => $this->render('_dataTeam', [
            'model' => $model,
            'row' => $model->teams,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
