<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sport */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampSession', 
        'relID' => 'camp-session', 
        'value' => \yii\helpers\Json::encode($model->campSessions),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampSport', 
        'relID' => 'camp-sport', 
        'value' => \yii\helpers\Json::encode($model->campSports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CoachSport', 
        'relID' => 'coach-sport', 
        'value' => \yii\helpers\Json::encode($model->coachSports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'GoverningBody', 
        'relID' => 'governing-body', 
        'value' => \yii\helpers\Json::encode($model->governingBodies),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerSport', 
        'relID' => 'player-sport', 
        'value' => \yii\helpers\Json::encode($model->playerSports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SchoolSport', 
        'relID' => 'school-sport', 
        'value' => \yii\helpers\Json::encode($model->schoolSports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SportGender', 
        'relID' => 'sport-gender', 
        'value' => \yii\helpers\Json::encode($model->sportGenders),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SportPosition', 
        'relID' => 'sport-position', 
        'value' => \yii\helpers\Json::encode($model->sportPositions),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Team', 
        'relID' => 'team', 
        'value' => \yii\helpers\Json::encode($model->teams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="sport-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'sport_id')->textInput(['placeholder' => 'Sport']) ?>

    <?= $form->field($model, 'sport')->textInput(['maxlength' => true, 'placeholder' => 'Sport']) ?>

    <?= $form->field($model, 'sport_desc_short')->textInput(['maxlength' => true, 'placeholder' => 'Sport Desc Short']) ?>

    <?= $form->field($model, 'sport_desc_long')->textInput(['maxlength' => true, 'placeholder' => 'Sport Desc Long']) ?>

    <?= $form->field($model, 'high_school_yn')->textInput(['maxlength' => true, 'placeholder' => 'High School Yn']) ?>

    <?= $form->field($model, 'ncaa_yn')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Yn']) ?>

    <?= $form->field($model, 'olympic_yn')->textInput(['maxlength' => true, 'placeholder' => 'Olympic Yn']) ?>

    <?= $form->field($model, 'xgame_sport_yn')->textInput(['maxlength' => true, 'placeholder' => 'Xgame Sport Yn']) ?>

    <?= $form->field($model, 'ncaa_sport_name')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Name']) ?>

    <?= $form->field($model, 'olympic_sport_name')->textInput(['maxlength' => true, 'placeholder' => 'Olympic Sport Name']) ?>

    <?= $form->field($model, 'xgame_sport_name')->textInput(['maxlength' => true, 'placeholder' => 'Xgame Sport Name']) ?>

    <?= $form->field($model, 'ncaa_sport_type')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Type']) ?>

    <?= $form->field($model, 'ncaa_sport_season_male')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Season Male']) ?>

    <?= $form->field($model, 'ncaa_sport_season_female')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Season Female']) ?>

    <?= $form->field($model, 'ncaa_sport_season_coed')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Season Coed']) ?>

    <?= $form->field($model, 'olympic_sport_season')->textInput(['maxlength' => true, 'placeholder' => 'Olympic Sport Season']) ?>

    <?= $form->field($model, 'xgame_sport_season')->textInput(['maxlength' => true, 'placeholder' => 'Xgame Sport Season']) ?>

    <?= $form->field($model, 'olympic_sport_gender')->textInput(['maxlength' => true, 'placeholder' => 'Olympic Sport Gender']) ?>

    <?= $form->field($model, 'xgame_sport_gender')->textInput(['maxlength' => true, 'placeholder' => 'Xgame Sport Gender']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampSession'),
            'content' => $this->render('_formCampSession', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campSessions),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampSport'),
            'content' => $this->render('_formCampSport', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campSports),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CoachSport'),
            'content' => $this->render('_formCoachSport', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->coachSports),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('GoverningBody'),
            'content' => $this->render('_formGoverningBody', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->governingBodies),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerSport'),
            'content' => $this->render('_formPlayerSport', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerSports),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SchoolSport'),
            'content' => $this->render('_formSchoolSport', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->schoolSports),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SportGender'),
            'content' => $this->render('_formSportGender', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->sportGenders),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('SportPosition'),
            'content' => $this->render('_formSportPosition', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->sportPositions),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Team'),
            'content' => $this->render('_formTeam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teams),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
