<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Sport */

$this->title = $model->sport;
$this->params['breadcrumbs'][] = ['label' => 'Sports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sport-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Sport'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->sport_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->sport_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'sport_id',
        'sport',
        'sport_desc_short',
        'sport_desc_long',
        'high_school_yn',
        'ncaa_yn',
        'olympic_yn',
        'xgame_sport_yn',
        'ncaa_sport_name',
        'olympic_sport_name',
        'xgame_sport_name',
        'ncaa_sport_type',
        'ncaa_sport_season_male',
        'ncaa_sport_season_female',
        'ncaa_sport_season_coed',
        'olympic_sport_season',
        'xgame_sport_season',
        'olympic_sport_gender',
        'xgame_sport_gender',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerCampSession->totalCount){
    $gridColumnCampSession = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_session_id',
            [
                'attribute' => 'camp.camp',
                'label' => 'Camp'
            ],
                        [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            [
                'attribute' => 'address.address_id',
                'label' => 'Address'
            ],
            [
                'attribute' => 'season.season',
                'label' => 'Season'
            ],
            'camp_session_location',
            'camp_session_url:url',
            'camp_session_ages',
            'camp_session_skill_level',
            'camp_session_type',
            'camp_session_begin_dt',
            'camp_session_end_dt',
            'camp_session_specialty',
            'camp_session_scholarships_available_yn',
            'camp_session_description',
            'camp_session_cost_regular_residential',
            'camp_session_cost_regular_commuter',
            'camp_session_cost_regular_day',
            'camp_session_cost_early_residential',
            'camp_session_cost_early_commuter',
            'camp_session_cost_early_day',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampSession,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-session']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Session'),
        ],
        'export' => false,
        'columns' => $gridColumnCampSession
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCampSport->totalCount){
    $gridColumnCampSport = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_sport_id',
            [
                'attribute' => 'camp.camp',
                'label' => 'Camp'
            ],
                        [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampSport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-sport']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Sport'),
        ],
        'export' => false,
        'columns' => $gridColumnCampSport
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCoachSport->totalCount){
    $gridColumnCoachSport = [
        ['class' => 'yii\grid\SerialColumn'],
            'coach_sport_id',
            [
                'attribute' => 'coach.coach_id',
                'label' => 'Coach'
            ],
                        [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCoachSport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-coach-sport']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Coach Sport'),
        ],
        'export' => false,
        'columns' => $gridColumnCoachSport
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerGoverningBody->totalCount){
    $gridColumnGoverningBody = [
        ['class' => 'yii\grid\SerialColumn'],
            'governing_body_id',
                        'governing_body',
            'governing_body_short_desc',
            'governing_body_long_desc',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerGoverningBody,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-governing-body']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Governing Body'),
        ],
        'export' => false,
        'columns' => $gridColumnGoverningBody
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerSport->totalCount){
    $gridColumnPlayerSport = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_sport_id',
            [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
                        [
                'attribute' => 'sportPosition.sport_position',
                'label' => 'Sport Position'
            ],
            [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerSport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-sport']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Sport'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerSport
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSchoolSport->totalCount){
    $gridColumnSchoolSport = [
        ['class' => 'yii\grid\SerialColumn'],
            'school_sport_id',
            [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
                        'gender_id',
            'conference_id',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSchoolSport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school-sport']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('School Sport'),
        ],
        'export' => false,
        'columns' => $gridColumnSchoolSport
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSportGender->totalCount){
    $gridColumnSportGender = [
        ['class' => 'yii\grid\SerialColumn'],
            'sport_gender_id',
                        [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSportGender,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-sport-gender']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Sport Gender'),
        ],
        'export' => false,
        'columns' => $gridColumnSportGender
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSportPosition->totalCount){
    $gridColumnSportPosition = [
        ['class' => 'yii\grid\SerialColumn'],
            'sport_position_id',
                        'sport_position',
            'display_order',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSportPosition,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-sport-position']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Sport Position'),
        ],
        'export' => false,
        'columns' => $gridColumnSportPosition
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTeam->totalCount){
    $gridColumnTeam = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_id',
            [
                'attribute' => 'org.org',
                'label' => 'Org'
            ],
            [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
                        [
                'attribute' => 'camp.camp',
                'label' => 'Camp'
            ],
            [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            [
                'attribute' => 'ageGroup.age_group',
                'label' => 'Age Group'
            ],
            'team',
            'team_gender',
            [
                'attribute' => 'teamDivision.team_division',
                'label' => 'Team Division'
            ],
            [
                'attribute' => 'teamLeague.team_league',
                'label' => 'Team League'
            ],
            'team_division',
            'team_league',
            'organizational_level',
            'team_governing_body',
            'team_age_group',
            'team_website_url:url',
            'team_schedule_url:url',
            'team_schedule_uri',
            'team_statistical_highlights',
            [
                'attribute' => 'teamCompetitionSeason.season',
                'label' => 'Team Competition Season'
            ],
            'team_competition_season',
            'team_city',
            'team_state_id',
            [
                'attribute' => 'teamCountry.country',
                'label' => 'Team Country'
            ],
            'team_wins',
            'team_losses',
            'team_draws',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team'),
        ],
        'export' => false,
        'columns' => $gridColumnTeam
    ]);
}
?>

    </div>
</div>
