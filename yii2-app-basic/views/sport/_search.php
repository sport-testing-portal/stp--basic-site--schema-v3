<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-sport-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sport_id')->textInput(['placeholder' => 'Sport']) ?>

    <?= $form->field($model, 'sport')->textInput(['maxlength' => true, 'placeholder' => 'Sport']) ?>

    <?= $form->field($model, 'sport_desc_short')->textInput(['maxlength' => true, 'placeholder' => 'Sport Desc Short']) ?>

    <?= $form->field($model, 'sport_desc_long')->textInput(['maxlength' => true, 'placeholder' => 'Sport Desc Long']) ?>

    <?= $form->field($model, 'high_school_yn')->textInput(['maxlength' => true, 'placeholder' => 'High School Yn']) ?>

    <?php /* echo $form->field($model, 'ncaa_yn')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Yn']) */ ?>

    <?php /* echo $form->field($model, 'olympic_yn')->textInput(['maxlength' => true, 'placeholder' => 'Olympic Yn']) */ ?>

    <?php /* echo $form->field($model, 'xgame_sport_yn')->textInput(['maxlength' => true, 'placeholder' => 'Xgame Sport Yn']) */ ?>

    <?php /* echo $form->field($model, 'ncaa_sport_name')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Name']) */ ?>

    <?php /* echo $form->field($model, 'olympic_sport_name')->textInput(['maxlength' => true, 'placeholder' => 'Olympic Sport Name']) */ ?>

    <?php /* echo $form->field($model, 'xgame_sport_name')->textInput(['maxlength' => true, 'placeholder' => 'Xgame Sport Name']) */ ?>

    <?php /* echo $form->field($model, 'ncaa_sport_type')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Type']) */ ?>

    <?php /* echo $form->field($model, 'ncaa_sport_season_male')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Season Male']) */ ?>

    <?php /* echo $form->field($model, 'ncaa_sport_season_female')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Season Female']) */ ?>

    <?php /* echo $form->field($model, 'ncaa_sport_season_coed')->textInput(['maxlength' => true, 'placeholder' => 'Ncaa Sport Season Coed']) */ ?>

    <?php /* echo $form->field($model, 'olympic_sport_season')->textInput(['maxlength' => true, 'placeholder' => 'Olympic Sport Season']) */ ?>

    <?php /* echo $form->field($model, 'xgame_sport_season')->textInput(['maxlength' => true, 'placeholder' => 'Xgame Sport Season']) */ ?>

    <?php /* echo $form->field($model, 'olympic_sport_gender')->textInput(['maxlength' => true, 'placeholder' => 'Olympic Sport Gender']) */ ?>

    <?php /* echo $form->field($model, 'xgame_sport_gender')->textInput(['maxlength' => true, 'placeholder' => 'Xgame Sport Gender']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
