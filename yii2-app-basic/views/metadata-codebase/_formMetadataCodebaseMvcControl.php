<div class="form-group" id="add-metadata-codebase-mvc-control">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'MetadataCodebaseMvcControl',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'codebase_mvc_control_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'controller_actions' => ['type' => TabularForm::INPUT_TEXT],
        'action_function' => ['type' => TabularForm::INPUT_TEXT],
        'action_params' => ['type' => TabularForm::INPUT_TEXT],
        'action_url' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMetadataCodebaseMvcControl(' . $key . '); return false;', 'id' => 'metadata-codebase-mvc-control-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Metadata  Codebase Mvc Control', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowMetadataCodebaseMvcControl()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

