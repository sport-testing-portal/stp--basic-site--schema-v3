<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebase */

$this->title = 'Save As New Metadata Codebase: '. ' ' . $model->codebase_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codebase_id, 'url' => ['view', 'id' => $model->codebase_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-codebase-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
