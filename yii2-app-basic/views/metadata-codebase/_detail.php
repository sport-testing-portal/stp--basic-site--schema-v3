<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebase */

?>
<div class="metadata-codebase-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->codebase_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'codebase_id',
        'file_name',
        'file_path_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>