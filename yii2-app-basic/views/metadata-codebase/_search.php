<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-codebase-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codebase_id')->textInput(['placeholder' => 'Codebase']) ?>

    <?= $form->field($model, 'file_name')->textInput(['maxlength' => true, 'placeholder' => 'File Name']) ?>

    <?= $form->field($model, 'file_path_uri')->textInput(['maxlength' => true, 'placeholder' => 'File Path Uri']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
