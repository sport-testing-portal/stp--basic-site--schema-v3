<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDev */

$this->title = $model->database_dev_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Database Devs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-database-dev-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Metadata Database Dev'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->database_dev_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->database_dev_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->database_dev_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'database_dev_id',
        'database_dev',
        'database_dev_desc_short',
        'database_dev_desc_long',
        'dev_status_tag',
        'dev_scope',
        'rule_file_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerMetadataDatabaseDevStatus->totalCount){
    $gridColumnMetadataDatabaseDevStatus = [
        ['class' => 'yii\grid\SerialColumn'],
            'dev_status_id',
            [
                'attribute' => 'database.database_id',
                'label' => 'Database'
            ],
                        'dev_status',
            'dev_status_tag',
            'dev_status_bfr',
            'dev_status_at',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMetadataDatabaseDevStatus,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-metadata--database-dev-status']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Metadata Database Dev Status'),
        ],
        'export' => false,
        'columns' => $gridColumnMetadataDatabaseDevStatus
    ]);
}
?>

    </div>
</div>
