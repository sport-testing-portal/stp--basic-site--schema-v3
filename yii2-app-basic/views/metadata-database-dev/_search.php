<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDevSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-database-dev-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'database_dev_id')->textInput(['placeholder' => 'Database Dev']) ?>

    <?= $form->field($model, 'database_dev')->textInput(['maxlength' => true, 'placeholder' => 'Database Dev']) ?>

    <?= $form->field($model, 'database_dev_desc_short')->textInput(['maxlength' => true, 'placeholder' => 'Database Dev Desc Short']) ?>

    <?= $form->field($model, 'database_dev_desc_long')->textInput(['maxlength' => true, 'placeholder' => 'Database Dev Desc Long']) ?>

    <?= $form->field($model, 'dev_status_tag')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status Tag']) ?>

    <?php /* echo $form->field($model, 'dev_scope')->textInput(['maxlength' => true, 'placeholder' => 'Dev Scope']) */ ?>

    <?php /* echo $form->field($model, 'rule_file_uri')->textInput(['maxlength' => true, 'placeholder' => 'Rule File Uri']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
