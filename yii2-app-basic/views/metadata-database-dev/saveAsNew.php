<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDev */

$this->title = 'Save As New Metadata Database Dev: '. ' ' . $model->database_dev_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Database Devs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->database_dev_id, 'url' => ['view', 'id' => $model->database_dev_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-database-dev-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
