<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDev */

?>
<div class="metadata-database-dev-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->database_dev_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'database_dev_id',
        'database_dev',
        'database_dev_desc_short',
        'database_dev_desc_long',
        'dev_status_tag',
        'dev_scope',
        'rule_file_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>