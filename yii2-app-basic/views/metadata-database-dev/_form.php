<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataDatabaseDev */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'MetadataDatabaseDevStatus', 
        'relID' => 'metadata-database-dev-status', 
        'value' => \yii\helpers\Json::encode($model->metadataDatabaseDevStatuses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="metadata-database-dev-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'database_dev_id')->textInput(['placeholder' => 'Database Dev']) ?>

    <?= $form->field($model, 'database_dev')->textInput(['maxlength' => true, 'placeholder' => 'Database Dev']) ?>

    <?= $form->field($model, 'database_dev_desc_short')->textInput(['maxlength' => true, 'placeholder' => 'Database Dev Desc Short']) ?>

    <?= $form->field($model, 'database_dev_desc_long')->textInput(['maxlength' => true, 'placeholder' => 'Database Dev Desc Long']) ?>

    <?= $form->field($model, 'dev_status_tag')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status Tag']) ?>

    <?= $form->field($model, 'dev_scope')->textInput(['maxlength' => true, 'placeholder' => 'Dev Scope']) ?>

    <?= $form->field($model, 'rule_file_uri')->textInput(['maxlength' => true, 'placeholder' => 'Rule File Uri']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MetadataDatabaseDevStatus'),
            'content' => $this->render('_formMetadataDatabaseDevStatus', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->metadataDatabaseDevStatuses),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
