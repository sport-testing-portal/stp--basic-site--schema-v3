<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcControl */

$this->title = 'Create Metadata Codebase Mvc Control';
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Mvc Controls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-codebase-mvc-control-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
