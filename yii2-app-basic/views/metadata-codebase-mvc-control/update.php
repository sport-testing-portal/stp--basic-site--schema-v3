<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcControl */

$this->title = 'Update Metadata Codebase Mvc Control: ' . ' ' . $model->codebase_mvc_control_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Mvc Controls', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codebase_mvc_control_id, 'url' => ['view', 'id' => $model->codebase_mvc_control_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-codebase-mvc-control-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
