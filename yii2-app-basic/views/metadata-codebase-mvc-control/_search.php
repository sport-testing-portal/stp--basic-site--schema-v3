<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcControlSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-codebase-mvc-control-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'codebase_mvc_control_id')->textInput(['placeholder' => 'Codebase Mvc Control']) ?>

    <?= $form->field($model, 'codebase_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataCodebase::find()->orderBy('codebase_id')->asArray()->all(), 'codebase_id', 'codebase_id'),
        'options' => ['placeholder' => 'Choose Metadata  codebase'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'controller_actions')->textInput(['maxlength' => true, 'placeholder' => 'Controller Actions']) ?>

    <?= $form->field($model, 'action_function')->textInput(['maxlength' => true, 'placeholder' => 'Action Function']) ?>

    <?= $form->field($model, 'action_params')->textInput(['maxlength' => true, 'placeholder' => 'Action Params']) ?>

    <?php /* echo $form->field($model, 'action_url')->textInput(['maxlength' => true, 'placeholder' => 'Action Url']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
