<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcModel */

?>
<div class="metadata-codebase-mvc-model-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->codebase_mvc_model_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'codebase_mvc_model_id',
        [
            'attribute' => 'codebase.codebase_id',
            'label' => 'Codebase',
        ],
        'json_data_structure',
        'json_data_sample',
        'json_data_example',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>