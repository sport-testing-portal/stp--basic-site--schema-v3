<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcModel */

$this->title = 'Create Metadata Codebase Mvc Model';
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Mvc Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-codebase-mvc-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
