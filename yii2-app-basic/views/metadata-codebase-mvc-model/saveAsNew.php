<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseMvcModel */

$this->title = 'Save As New Metadata Codebase Mvc Model: '. ' ' . $model->codebase_mvc_model_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Mvc Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codebase_mvc_model_id, 'url' => ['view', 'id' => $model->codebase_mvc_model_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-codebase-mvc-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
