<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;

$relationNames = $model->relationNames();
Yii::trace($relationNames, 'Org Controller - Views _expand.php');
    
    // dbyrd test
    $counts = [
        'Address'=> count(\yii\helpers\ArrayHelper::toArray($model->addresses)),
        'Camp'   => count(\yii\helpers\ArrayHelper::toArray($model->camps)),
        'Media'  => count(\yii\helpers\ArrayHelper::toArray($model->media)),
        'Note'   => count(\yii\helpers\ArrayHelper::toArray($model->notes)),
        'OrgGoverningBody'=> count(\yii\helpers\ArrayHelper::toArray($model->orgGoverningBodies)),
        'OrgSchool'=> count(\yii\helpers\ArrayHelper::toArray($model->orgSchools)),
        'PaymentLog'=> count(\yii\helpers\ArrayHelper::toArray($model->paymentLogs)),
        'Person'=> count(\yii\helpers\ArrayHelper::toArray($model->people)),
        'School'=> count(\yii\helpers\ArrayHelper::toArray($model->schools)),
        'Subscription'=> count(\yii\helpers\ArrayHelper::toArray($model->subscriptions)),
        'Team'=> count(\yii\helpers\ArrayHelper::toArray($model->teams)),        
    ];
    yii::trace($counts, 'counts of org links in org._expand');

$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Org'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Address') . ' ('.$counts['Address'] .')',
        'content' => $this->render('_dataAddress', [
            'model' => $model,
            'row' => $model->addresses,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp') . ' ('.$counts['Camp'] .')',
        'content' => $this->render('_dataCamp', [
            'model' => $model,
            'row' => $model->camps,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Media') . ' ('.$counts['Media'] .')',
        'content' => $this->render('_dataMedia', [
            'model' => $model,
            'row' => $model->media,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Note') . ' ('.$counts['Note'] .')',
        'content' => $this->render('_dataNote', [
            'model' => $model,
            'row' => $model->notes,
        ]),
    ],
                    [
        //'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Org Governing Body') . ' ('.$counts['OrgGoverningBody'] .')',
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Governing Bodies') . ' ('.$counts['OrgGoverningBody'] .')',
        'content' => $this->render('_dataOrgGoverningBody', [
            'model' => $model,
            'row' => $model->orgGoverningBodies,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Org School') . ' ('.$counts['OrgSchool'] .')',
        'content' => $this->render('_dataOrgSchool', [
            'model' => $model,
            'row' => $model->orgSchools,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Payment Log') . ' ('.$counts['PaymentLog'] .')',
        'content' => $this->render('_dataPaymentLog', [
            'model' => $model,
            'row' => $model->paymentLogs,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Person') . ' ('.$counts['Person'] .')',
        'content' => $this->render('_dataPerson', [
            'model' => $model,
            'row' => $model->people,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School') . ' ('.$counts['School'] .')',
        'content' => $this->render('_dataSchool', [
            'model' => $model,
            'row' => $model->schools,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Subscription') . ' ('.$counts['Subscription'] .')',
        'content' => $this->render('_dataSubscription', [
            'model' => $model,
            'row' => $model->subscriptions,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team') . ' ('.$counts['Team'] .')',
        'content' => $this->render('_dataTeam', [
            'model' => $model,
            'row' => $model->teams,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
