<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Org */

?>
<div class="org-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->org) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'org_id',
        [
            'attribute' => 'orgType.org_type',
            'label' => 'Org Type',
        ],
        [
            'attribute' => 'orgLevel.org_level',
            'label' => 'Org Level',
        ],
        'org',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url:url',
        'org_twitter_url:url',
        'org_facebook_url:url',
        'org_phone_main',
        'org_email_main:email',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>