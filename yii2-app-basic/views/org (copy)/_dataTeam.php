<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->teams,
        'key' => 'team_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'team_id',
        [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
        [
                'attribute' => 'sport.sport',
                'label' => 'Sport'
            ],
        [
                'attribute' => 'camp.camp',
                'label' => 'Camp'
            ],
        [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
        [
                'attribute' => 'ageGroup.age_group',
                'label' => 'Age Group'
            ],
        'team',
        'team_gender',
        [
                'attribute' => 'teamDivision.team_division',
                'label' => 'Team Division'
            ],
        [
                'attribute' => 'teamLeague.team_league',
                'label' => 'Team League'
            ],
        'team_division',
        'team_league',
        'organizational_level',
        'team_governing_body',
        'team_age_group',
        'team_website_url:url',
        'team_schedule_url:url',
        'team_schedule_uri',
        'team_statistical_highlights',
        [
                'attribute' => 'teamCompetitionSeason.season',
                'label' => 'Team Competition Season'
            ],
        'team_competition_season',
        'team_city',
        'team_state_id',
        [
                'attribute' => 'teamCountry.country',
                'label' => 'Team Country'
            ],
        'team_wins',
        'team_losses',
        'team_draws',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'team'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
