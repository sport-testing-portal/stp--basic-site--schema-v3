<div class="form-group" id="add-school">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'School',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'school_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'religious_affiliation_id' => [
            'label' => 'Religious affiliation',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\ReligiousAffiliation::find()->orderBy('religious_affiliation')->asArray()->all(), 'religious_affiliation_id', 'religious_affiliation'),
                'options' => ['placeholder' => 'Choose Religious affiliation'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'conference_id__female' => ['type' => TabularForm::INPUT_TEXT],
        'conference_id__male' => ['type' => TabularForm::INPUT_TEXT],
        'conference_id__main' => ['type' => TabularForm::INPUT_TEXT],
        'school_ipeds_id' => ['type' => TabularForm::INPUT_TEXT],
        'school_unit_id' => ['type' => TabularForm::INPUT_TEXT],
        'school_ope_id' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowSchool(' . $key . '); return false;', 'id' => 'school-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add School', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowSchool()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

