<div class="form-group" id="add-person">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Person',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'person_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'app_user_id' => [
            'label' => 'App user',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\AppUser::find()->orderBy('username')->asArray()->all(), 'app_user_id', 'username'),
                'options' => ['placeholder' => 'Choose App user'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'person_type_id' => [
            'label' => 'Person type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\PersonType::find()->orderBy('person_type')->asArray()->all(), 'person_type_id', 'person_type'),
                'options' => ['placeholder' => 'Choose Person type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'user_id' => ['type' => TabularForm::INPUT_TEXT],
        'person_name_prefix' => ['type' => TabularForm::INPUT_TEXT],
        'person_name_first' => ['type' => TabularForm::INPUT_TEXT],
        'person_name_middle' => ['type' => TabularForm::INPUT_TEXT],
        'person_name_last' => ['type' => TabularForm::INPUT_TEXT],
        'person_name_suffix' => ['type' => TabularForm::INPUT_TEXT],
        'person' => ['type' => TabularForm::INPUT_TEXT],
        'person_phone_personal' => ['type' => TabularForm::INPUT_TEXT],
        'person_email_personal' => ['type' => TabularForm::INPUT_TEXT],
        'person_phone_work' => ['type' => TabularForm::INPUT_TEXT],
        'person_email_work' => ['type' => TabularForm::INPUT_TEXT],
        'person_position_work' => ['type' => TabularForm::INPUT_TEXT],
        'gender_id' => [
            'label' => 'Gender',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->orderBy('gender')->asArray()->all(), 'gender_id', 'gender'),
                'options' => ['placeholder' => 'Choose Gender'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'person_image_headshot_url' => ['type' => TabularForm::INPUT_TEXT],
        'person_name_nickname' => ['type' => TabularForm::INPUT_TEXT],
        'person_date_of_birth' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Person Date Of Birth',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'person_height' => ['type' => TabularForm::INPUT_TEXT],
        'person_weight' => ['type' => TabularForm::INPUT_TEXT],
        'person_tshirt_size' => ['type' => TabularForm::INPUT_TEXT],
        'person_high_school__graduation_year' => ['type' => TabularForm::INPUT_TEXT],
        'person_college_graduation_year' => ['type' => TabularForm::INPUT_TEXT],
        'person_college_commitment_status' => ['type' => TabularForm::INPUT_TEXT],
        'person_addr_1' => ['type' => TabularForm::INPUT_TEXT],
        'person_addr_2' => ['type' => TabularForm::INPUT_TEXT],
        'person_addr_3' => ['type' => TabularForm::INPUT_TEXT],
        'person_city' => ['type' => TabularForm::INPUT_TEXT],
        'person_postal_code' => ['type' => TabularForm::INPUT_TEXT],
        'person_country' => ['type' => TabularForm::INPUT_TEXT],
        'person_country_code' => [
            'label' => 'Country',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy('country')->asArray()->all(), 'iso3', 'country'),
                'options' => ['placeholder' => 'Choose Country'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'person_state_or_region' => ['type' => TabularForm::INPUT_TEXT],
        'org_affiliation_begin_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Org Affiliation Begin Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'org_affiliation_end_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Org Affiliation End Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'person_website' => ['type' => TabularForm::INPUT_TEXT],
        'person_profile_url' => ['type' => TabularForm::INPUT_TEXT],
        'person_profile_uri' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowPerson(' . $key . '); return false;', 'id' => 'person-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Person', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowPerson()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

