<div class="form-group" id="add-media">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Media',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'media_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'media_type_id' => [
            'label' => 'Media type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\MediaType::find()->orderBy('media_type')->asArray()->all(), 'media_type_id', 'media_type'),
                'options' => ['placeholder' => 'Choose Media type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'person_id' => [
            'label' => 'Person',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Person::find()->orderBy('person')->asArray()->all(), 'person_id', 'person'),
                'options' => ['placeholder' => 'Choose Person'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'media_name_original' => ['type' => TabularForm::INPUT_TEXT],
        'media_name_unique' => ['type' => TabularForm::INPUT_TEXT],
        'media_desc_short' => ['type' => TabularForm::INPUT_TEXT],
        'media_desc_long' => ['type' => TabularForm::INPUT_TEXT],
        'media_url_local' => ['type' => TabularForm::INPUT_TEXT],
        'media_url_remote' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMedia(' . $key . '); return false;', 'id' => 'media-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Media', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowMedia()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

