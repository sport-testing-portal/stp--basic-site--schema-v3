<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseFunctionItems */

$this->title = 'Create Metadata Codebase Function Items';
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Function Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="metadata-codebase-function-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
