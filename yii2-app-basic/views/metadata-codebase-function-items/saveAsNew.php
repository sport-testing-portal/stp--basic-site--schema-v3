<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseFunctionItems */

$this->title = 'Save As New Metadata Codebase Function Items: '. ' ' . $model->metadata__codebase_function_items_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Function Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metadata__codebase_function_items_id, 'url' => ['view', 'id' => $model->metadata__codebase_function_items_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="metadata-codebase-function-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
