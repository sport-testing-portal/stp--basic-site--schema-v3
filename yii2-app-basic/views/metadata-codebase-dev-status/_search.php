<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseDevStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-metadata-codebase-dev-status-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'dev_status_id')->textInput(['placeholder' => 'Dev Status']) ?>

    <?= $form->field($model, 'codebase_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataCodebase::find()->orderBy('codebase_id')->asArray()->all(), 'codebase_id', 'codebase_id'),
        'options' => ['placeholder' => 'Choose Metadata  codebase'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'codebase_dev_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\MetadataCodebaseDev::find()->orderBy('codebase_dev_id')->asArray()->all(), 'codebase_dev_id', 'codebase_dev_id'),
        'options' => ['placeholder' => 'Choose Metadata  codebase dev'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'dev_status')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status']) ?>

    <?= $form->field($model, 'dev_status_tag')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status Tag']) ?>

    <?php /* echo $form->field($model, 'dev_status_bfr')->textInput(['maxlength' => true, 'placeholder' => 'Dev Status Bfr']) */ ?>

    <?php /* echo $form->field($model, 'dev_status_at')->textInput(['placeholder' => 'Dev Status At']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
