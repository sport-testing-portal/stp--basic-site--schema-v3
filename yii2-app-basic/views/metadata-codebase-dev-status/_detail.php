<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseDevStatus */

?>
<div class="metadata-codebase-dev-status-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->dev_status_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'dev_status_id',
        [
            'attribute' => 'codebase.codebase_id',
            'label' => 'Codebase',
        ],
        [
            'attribute' => 'codebaseDev.codebase_dev_id',
            'label' => 'Codebase Dev',
        ],
        'dev_status',
        'dev_status_tag',
        'dev_status_bfr',
        'dev_status_at',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>