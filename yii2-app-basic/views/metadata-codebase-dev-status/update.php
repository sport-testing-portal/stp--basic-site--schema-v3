<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MetadataCodebaseDevStatus */

$this->title = 'Update Metadata Codebase Dev Status: ' . ' ' . $model->dev_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Metadata Codebase Dev Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dev_status_id, 'url' => ['view', 'id' => $model->dev_status_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="metadata-codebase-dev-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
