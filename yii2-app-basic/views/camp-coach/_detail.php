<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\CampCoach */

?>
<div class="camp-coach-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->camp_coach_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'camp_coach_id',
        [
            'attribute' => 'camp.camp',
            'label' => 'Camp',
        ],
        [
            'attribute' => 'coach.coach_id',
            'label' => 'Coach',
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>