<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\CampCoach */

$this->title = $model->camp_coach_id;
$this->params['breadcrumbs'][] = ['label' => 'Camp Coaches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camp-coach-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Camp Coach'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->camp_coach_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->camp_coach_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->camp_coach_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'camp_coach_id',
        [
            'attribute' => 'camp.camp',
            'label' => 'Camp',
        ],
        [
            'attribute' => 'coach.coach_id',
            'label' => 'Coach',
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Camp<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCamp = [
        'org_id',
        'camp',
        'camp_specialty',
        'camp_cost_regular',
        'camp_cost_early_registration',
        'camp_team_discounts_available_yn',
        'camp_scholarships_available_yn',
        'camp_session_desc',
        'camp_website_url',
        'camp_session_url',
        'camp_registration_url',
        'camp_twitter_url',
        'camp_facebook_url',
        'camp_scholarship_application_info',
        'camp_scholarship_application_request',
        'camp_organizer_description',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->camp,
        'attributes' => $gridColumnCamp    ]);
    ?>
    <div class="row">
        <h4>Coach<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCoach = [
        'person_id',
        'coach_type_id',
        'coach_team_coach_id',
        'age_group_id',
        'coach_specialty',
        'coach_certifications',
        'coach_comments',
        'coach_qrcode_uri',
        'coach_info_source_scrape_url',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->coach,
        'attributes' => $gridColumnCoach    ]);
    ?>
</div>
