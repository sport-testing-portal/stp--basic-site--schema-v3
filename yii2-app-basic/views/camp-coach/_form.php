<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CampCoach */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="camp-coach-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'camp_coach_id')->textInput(['placeholder' => 'Camp Coach']) ?>

    <?= $form->field($model, 'camp_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Camp::find()->orderBy('camp_id')->asArray()->all(), 'camp_id', 'camp'),
        'options' => ['placeholder' => 'Choose Camp'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'coach_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Coach::find()->orderBy('coach_id')->asArray()->all(), 'coach_id', 'coach_id'),
        'options' => ['placeholder' => 'Choose Coach'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
