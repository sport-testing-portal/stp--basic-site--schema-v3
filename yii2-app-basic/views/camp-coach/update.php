<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CampCoach */

$this->title = 'Update Camp Coach: ' . ' ' . $model->camp_coach_id;
$this->params['breadcrumbs'][] = ['label' => 'Camp Coaches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->camp_coach_id, 'url' => ['view', 'id' => $model->camp_coach_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="camp-coach-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
