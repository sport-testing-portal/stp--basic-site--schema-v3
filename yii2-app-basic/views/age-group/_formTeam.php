<div class="form-group" id="add-team">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Team',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'team_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'org_id' => [
            'label' => 'Org',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org')->asArray()->all(), 'org_id', 'org'),
                'options' => ['placeholder' => 'Choose Org'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'school_id' => [
            'label' => 'School',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\School::find()->orderBy('school_id')->asArray()->all(), 'school_id', 'school_id'),
                'options' => ['placeholder' => 'Choose School'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'sport_id' => [
            'label' => 'Sport',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Sport::find()->orderBy('sport')->asArray()->all(), 'sport_id', 'sport'),
                'options' => ['placeholder' => 'Choose Sport'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'camp_id' => [
            'label' => 'Camp',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Camp::find()->orderBy('camp')->asArray()->all(), 'camp_id', 'camp'),
                'options' => ['placeholder' => 'Choose Camp'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'gender_id' => [
            'label' => 'Gender',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->orderBy('gender')->asArray()->all(), 'gender_id', 'gender'),
                'options' => ['placeholder' => 'Choose Gender'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team' => ['type' => TabularForm::INPUT_TEXT],
        'team_gender' => ['type' => TabularForm::INPUT_TEXT],
        'team_division_id' => [
            'label' => 'Team division',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamDivision::find()->orderBy('team_division')->asArray()->all(), 'team_division_id', 'team_division'),
                'options' => ['placeholder' => 'Choose Team division'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_league_id' => [
            'label' => 'Team league',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamLeague::find()->orderBy('team_league')->asArray()->all(), 'team_league_id', 'team_league'),
                'options' => ['placeholder' => 'Choose Team league'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_division' => ['type' => TabularForm::INPUT_TEXT],
        'team_league' => ['type' => TabularForm::INPUT_TEXT],
        'organizational_level' => ['type' => TabularForm::INPUT_TEXT],
        'team_governing_body' => ['type' => TabularForm::INPUT_TEXT],
        'team_age_group' => ['type' => TabularForm::INPUT_TEXT],
        'team_website_url' => ['type' => TabularForm::INPUT_TEXT],
        'team_schedule_url' => ['type' => TabularForm::INPUT_TEXT],
        'team_schedule_uri' => ['type' => TabularForm::INPUT_TEXT],
        'team_statistical_highlights' => ['type' => TabularForm::INPUT_TEXT],
        'team_competition_season_id' => [
            'label' => 'Season',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Season::find()->orderBy('season')->asArray()->all(), 'season_id', 'season'),
                'options' => ['placeholder' => 'Choose Season'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_competition_season' => ['type' => TabularForm::INPUT_TEXT],
        'team_city' => ['type' => TabularForm::INPUT_TEXT],
        'team_state_id' => ['type' => TabularForm::INPUT_TEXT],
        'team_country_id' => [
            'label' => 'Country',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy('country')->asArray()->all(), 'country_id', 'country'),
                'options' => ['placeholder' => 'Choose Country'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_wins' => ['type' => TabularForm::INPUT_TEXT],
        'team_losses' => ['type' => TabularForm::INPUT_TEXT],
        'team_draws' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowTeam(' . $key . '); return false;', 'id' => 'team-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Team', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowTeam()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

