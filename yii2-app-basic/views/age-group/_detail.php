<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\AgeGroup */

?>
<div class="age-group-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->age_group) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'age_group_id',
        'sport_id',
        'age_group',
        'age_group_desc',
        'age_group_min',
        'age_group_max',
        'display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>