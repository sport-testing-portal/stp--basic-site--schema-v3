<div class="form-group" id="add-player">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Player',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'player_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'person_id' => [
            'label' => 'Person',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Person::find()->orderBy('person')->asArray()->all(), 'person_id', 'person'),
                'options' => ['placeholder' => 'Choose Person'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'player_team_player_id' => [
            'label' => 'Team player',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamPlayer::find()->orderBy('team_player_id')->asArray()->all(), 'team_player_id', 'team_player_id'),
                'options' => ['placeholder' => 'Choose Team player'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'player_sport_position_id' => [
            'label' => 'Sport position',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\SportPosition::find()->orderBy('sport_position')->asArray()->all(), 'sport_position_id', 'sport_position'),
                'options' => ['placeholder' => 'Choose Sport position'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'player_sport_position2_id' => ['type' => TabularForm::INPUT_TEXT],
        'player_access_code' => ['type' => TabularForm::INPUT_TEXT],
        'player_waiver_minor_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Player Waiver Minor Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'player_waiver_adult_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Player Waiver Adult Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'player_parent_email' => ['type' => TabularForm::INPUT_TEXT],
        'player_sport_preference' => ['type' => TabularForm::INPUT_TEXT],
        'player_sport_position_preference' => ['type' => TabularForm::INPUT_TEXT],
        'player_shot_side_preference' => ['type' => TabularForm::INPUT_TEXT],
        'player_dominant_side' => ['type' => TabularForm::INPUT_TEXT],
        'player_dominant_foot' => ['type' => TabularForm::INPUT_TEXT],
        'player_statistical_highlights' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowPlayer(' . $key . '); return false;', 'id' => 'player-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Player', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowPlayer()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

