<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgeGroup */

$this->title = 'Update Age Group: ' . ' ' . $model->age_group;
$this->params['breadcrumbs'][] = ['label' => 'Age Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->age_group, 'url' => ['view', 'id' => $model->age_group_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="age-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
