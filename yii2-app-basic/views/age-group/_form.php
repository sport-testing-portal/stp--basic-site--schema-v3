<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgeGroup */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Coach', 
        'relID' => 'coach', 
        'value' => \yii\helpers\Json::encode($model->coaches),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Player', 
        'relID' => 'player', 
        'value' => \yii\helpers\Json::encode($model->players),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Team', 
        'relID' => 'team', 
        'value' => \yii\helpers\Json::encode($model->teams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="age-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'age_group_id')->textInput(['placeholder' => 'Age Group']) ?>

    <?= $form->field($model, 'sport_id')->textInput(['placeholder' => 'Sport']) ?>

    <?= $form->field($model, 'age_group')->textInput(['maxlength' => true, 'placeholder' => 'Age Group']) ?>

    <?= $form->field($model, 'age_group_desc')->textInput(['maxlength' => true, 'placeholder' => 'Age Group Desc']) ?>

    <?= $form->field($model, 'age_group_min')->textInput(['placeholder' => 'Age Group Min']) ?>

    <?= $form->field($model, 'age_group_max')->textInput(['placeholder' => 'Age Group Max']) ?>

    <?= $form->field($model, 'display_order')->textInput(['placeholder' => 'Display Order']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Coach'),
            'content' => $this->render('_formCoach', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->coaches),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Player'),
            'content' => $this->render('_formPlayer', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->players),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Team'),
            'content' => $this->render('_formTeam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teams),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
