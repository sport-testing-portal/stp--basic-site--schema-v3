<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\AgeGroup */

$this->title = $model->age_group;
$this->params['breadcrumbs'][] = ['label' => 'Age Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="age-group-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Age Group'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->age_group_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->age_group_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'age_group_id',
        'sport_id',
        'age_group',
        'age_group_desc',
        'age_group_min',
        'age_group_max',
        'display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerCoach->totalCount){
    $gridColumnCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'teamCoaches.team_coach_id',
                'label' => 'Coach'
            ],
            [
                'attribute' => 'person.person',
                'label' => 'Person'
            ],
            [
                'attribute' => 'coachType.coach_type',
                'label' => 'Coach Type'
            ],
            [
                'attribute' => 'coachTeamCoach.team_coach_id',
                'label' => 'Coach Team Coach'
            ],
                        'coach_specialty',
            'coach_certifications',
            'coach_comments',
            'coach_qrcode_uri',
            'coach_info_source_scrape_url:url',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnCoach
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayer->totalCount){
    $gridColumnPlayer = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'teamPlayers.team_player_id',
                'label' => 'Player'
            ],
            [
                'attribute' => 'person.person',
                'label' => 'Person'
            ],
                        [
                'attribute' => 'playerTeamPlayer.team_player_id',
                'label' => 'Player Team Player'
            ],
            [
                'attribute' => 'playerSportPosition.sport_position',
                'label' => 'Player Sport Position'
            ],
            'player_sport_position2_id',
            'player_access_code',
            'player_waiver_minor_dt',
            'player_waiver_adult_dt',
            'player_parent_email:email',
            'player_sport_preference',
            'player_sport_position_preference',
            'player_shot_side_preference',
            'player_dominant_side',
            'player_dominant_foot',
            'player_statistical_highlights',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayer,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayer
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTeam->totalCount){
    $gridColumnTeam = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_id',
            [
                'attribute' => 'org.org',
                'label' => 'Org'
            ],
            [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
            [
                'attribute' => 'sport.sport',
                'label' => 'Sport'
            ],
            [
                'attribute' => 'camp.camp',
                'label' => 'Camp'
            ],
            [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
                        'team',
            'team_gender',
            [
                'attribute' => 'teamDivision.team_division',
                'label' => 'Team Division'
            ],
            [
                'attribute' => 'teamLeague.team_league',
                'label' => 'Team League'
            ],
            'team_division',
            'team_league',
            'organizational_level',
            'team_governing_body',
            'team_age_group',
            'team_website_url:url',
            'team_schedule_url:url',
            'team_schedule_uri',
            'team_statistical_highlights',
            [
                'attribute' => 'teamCompetitionSeason.season',
                'label' => 'Team Competition Season'
            ],
            'team_competition_season',
            'team_city',
            'team_state_id',
            [
                'attribute' => 'teamCountry.country',
                'label' => 'Team Country'
            ],
            'team_wins',
            'team_losses',
            'team_draws',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team'),
        ],
        'export' => false,
        'columns' => $gridColumnTeam
    ]);
}
?>

    </div>
</div>
