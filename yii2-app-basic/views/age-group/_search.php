<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AgeGroupSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-age-group-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'age_group_id')->textInput(['placeholder' => 'Age Group']) ?>

    <?= $form->field($model, 'sport_id')->textInput(['placeholder' => 'Sport']) ?>

    <?= $form->field($model, 'age_group')->textInput(['maxlength' => true, 'placeholder' => 'Age Group']) ?>

    <?= $form->field($model, 'age_group_desc')->textInput(['maxlength' => true, 'placeholder' => 'Age Group Desc']) ?>

    <?= $form->field($model, 'age_group_min')->textInput(['placeholder' => 'Age Group Min']) ?>

    <?php /* echo $form->field($model, 'age_group_max')->textInput(['placeholder' => 'Age Group Max']) */ ?>

    <?php /* echo $form->field($model, 'display_order')->textInput(['placeholder' => 'Display Order']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
