<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Coach'),
        'content' => $this->render('_dataCampCoach', [
            'model' => $model,
            'row' => $model->campCoaches,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Location'),
        'content' => $this->render('_dataCampLocation', [
            'model' => $model,
            'row' => $model->campLocations,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Session'),
        'content' => $this->render('_dataCampSession', [
            'model' => $model,
            'row' => $model->campSessions,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Sport'),
        'content' => $this->render('_dataCampSport', [
            'model' => $model,
            'row' => $model->campSports,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Camp Log'),
        'content' => $this->render('_dataPlayerCampLog', [
            'model' => $model,
            'row' => $model->playerCampLogs,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team'),
        'content' => $this->render('_dataTeam', [
            'model' => $model,
            'row' => $model->teams,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
