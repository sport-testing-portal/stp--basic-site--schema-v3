<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Camp */

$this->title = $model->camp;
$this->params['breadcrumbs'][] = ['label' => 'Camps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camp-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Camp'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->camp_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->camp_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->camp_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'camp_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        'camp',
        'camp_specialty',
        'camp_cost_regular',
        'camp_cost_early_registration',
        'camp_team_discounts_available_yn',
        'camp_scholarships_available_yn',
        'camp_session_desc',
        'camp_website_url:url',
        'camp_session_url:url',
        'camp_registration_url:url',
        'camp_twitter_url:url',
        'camp_facebook_url:url',
        'camp_scholarship_application_info',
        'camp_scholarship_application_request',
        'camp_organizer_description',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Org<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrg = [
        'org_type_id',
        'org_level_id',
        'org',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url',
        'org_twitter_url',
        'org_facebook_url',
        'org_phone_main',
        'org_email_main',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->org,
        'attributes' => $gridColumnOrg    ]);
    ?>
    
    <div class="row">
<?php
if($providerCampCoach->totalCount){
    $gridColumnCampCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_coach_id',
                        [
                'attribute' => 'coach.coach_id',
                'label' => 'Coach'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnCampCoach
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCampLocation->totalCount){
    $gridColumnCampLocation = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_location_id',
                        [
                'attribute' => 'address.address_id',
                'label' => 'Address'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampLocation,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-location']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Location'),
        ],
        'export' => false,
        'columns' => $gridColumnCampLocation
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCampSession->totalCount){
    $gridColumnCampSession = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_session_id',
                        [
                'attribute' => 'sport.sport',
                'label' => 'Sport'
            ],
            [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            [
                'attribute' => 'address.address_id',
                'label' => 'Address'
            ],
            [
                'attribute' => 'season.season',
                'label' => 'Season'
            ],
            'camp_session_location',
            'camp_session_url:url',
            'camp_session_ages',
            'camp_session_skill_level',
            'camp_session_type',
            'camp_session_begin_dt',
            'camp_session_end_dt',
            'camp_session_specialty',
            'camp_session_scholarships_available_yn',
            'camp_session_description',
            'camp_session_cost_regular_residential',
            'camp_session_cost_regular_commuter',
            'camp_session_cost_regular_day',
            'camp_session_cost_early_residential',
            'camp_session_cost_early_commuter',
            'camp_session_cost_early_day',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampSession,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-session']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Session'),
        ],
        'export' => false,
        'columns' => $gridColumnCampSession
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCampSport->totalCount){
    $gridColumnCampSport = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_sport_id',
                        [
                'attribute' => 'sport.sport',
                'label' => 'Sport'
            ],
            [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampSport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-sport']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Sport'),
        ],
        'export' => false,
        'columns' => $gridColumnCampSport
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerCampLog->totalCount){
    $gridColumnPlayerCampLog = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_camp_log_id',
            [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
                        [
                'attribute' => 'campSession.camp_session_id',
                'label' => 'Camp Session'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerCampLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-camp-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Camp Log'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerCampLog
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTeam->totalCount){
    $gridColumnTeam = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_id',
            [
                'attribute' => 'org.org',
                'label' => 'Org'
            ],
            [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
            [
                'attribute' => 'sport.sport',
                'label' => 'Sport'
            ],
                        [
                'attribute' => 'gender.gender',
                'label' => 'Gender'
            ],
            [
                'attribute' => 'ageGroup.age_group',
                'label' => 'Age Group'
            ],
            'team',
            'team_gender',
            [
                'attribute' => 'teamDivision.team_division',
                'label' => 'Team Division'
            ],
            [
                'attribute' => 'teamLeague.team_league',
                'label' => 'Team League'
            ],
            'team_division',
            'team_league',
            'organizational_level',
            'team_governing_body',
            'team_age_group',
            'team_website_url:url',
            'team_schedule_url:url',
            'team_schedule_uri',
            'team_statistical_highlights',
            [
                'attribute' => 'teamCompetitionSeason.season',
                'label' => 'Team Competition Season'
            ],
            'team_competition_season',
            'team_city',
            'team_state_id',
            [
                'attribute' => 'teamCountry.country',
                'label' => 'Team Country'
            ],
            'team_wins',
            'team_losses',
            'team_draws',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team'),
        ],
        'export' => false,
        'columns' => $gridColumnTeam
    ]);
}
?>

    </div>
</div>
