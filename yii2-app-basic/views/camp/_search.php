<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CampSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-camp-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'camp_id')->textInput(['placeholder' => 'Camp']) ?>

    <?= $form->field($model, 'org_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org'),
        'options' => ['placeholder' => 'Choose Org'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'camp')->textInput(['maxlength' => true, 'placeholder' => 'Camp']) ?>

    <?= $form->field($model, 'camp_specialty')->textInput(['maxlength' => true, 'placeholder' => 'Camp Specialty']) ?>

    <?= $form->field($model, 'camp_cost_regular')->textInput(['placeholder' => 'Camp Cost Regular']) ?>

    <?php /* echo $form->field($model, 'camp_cost_early_registration')->textInput(['placeholder' => 'Camp Cost Early Registration']) */ ?>

    <?php /* echo $form->field($model, 'camp_team_discounts_available_yn')->textInput(['maxlength' => true, 'placeholder' => 'Camp Team Discounts Available Yn']) */ ?>

    <?php /* echo $form->field($model, 'camp_scholarships_available_yn')->textInput(['maxlength' => true, 'placeholder' => 'Camp Scholarships Available Yn']) */ ?>

    <?php /* echo $form->field($model, 'camp_session_desc')->textInput(['maxlength' => true, 'placeholder' => 'Camp Session Desc']) */ ?>

    <?php /* echo $form->field($model, 'camp_website_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Website Url']) */ ?>

    <?php /* echo $form->field($model, 'camp_session_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Session Url']) */ ?>

    <?php /* echo $form->field($model, 'camp_registration_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Registration Url']) */ ?>

    <?php /* echo $form->field($model, 'camp_twitter_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Twitter Url']) */ ?>

    <?php /* echo $form->field($model, 'camp_facebook_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Facebook Url']) */ ?>

    <?php /* echo $form->field($model, 'camp_scholarship_application_info')->textInput(['maxlength' => true, 'placeholder' => 'Camp Scholarship Application Info']) */ ?>

    <?php /* echo $form->field($model, 'camp_scholarship_application_request')->textInput(['maxlength' => true, 'placeholder' => 'Camp Scholarship Application Request']) */ ?>

    <?php /* echo $form->field($model, 'camp_organizer_description')->textInput(['maxlength' => true, 'placeholder' => 'Camp Organizer Description']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
