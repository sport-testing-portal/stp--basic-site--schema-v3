<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Camp */

$this->title = 'Save As New Camp: '. ' ' . $model->camp;
$this->params['breadcrumbs'][] = ['label' => 'Camps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->camp, 'url' => ['view', 'id' => $model->camp_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="camp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
