<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Camp */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampCoach', 
        'relID' => 'camp-coach', 
        'value' => \yii\helpers\Json::encode($model->campCoaches),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampLocation', 
        'relID' => 'camp-location', 
        'value' => \yii\helpers\Json::encode($model->campLocations),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampSession', 
        'relID' => 'camp-session', 
        'value' => \yii\helpers\Json::encode($model->campSessions),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CampSport', 
        'relID' => 'camp-sport', 
        'value' => \yii\helpers\Json::encode($model->campSports),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PlayerCampLog', 
        'relID' => 'player-camp-log', 
        'value' => \yii\helpers\Json::encode($model->playerCampLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Team', 
        'relID' => 'team', 
        'value' => \yii\helpers\Json::encode($model->teams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="camp-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'camp_id')->textInput(['placeholder' => 'Camp']) ?>

    <?= $form->field($model, 'org_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org'),
        'options' => ['placeholder' => 'Choose Org'],
        'pluginOptions' => [
            'allowClear' => true, 'dropdownAutoWidth' => true
        ],
    ]); ?>

    <?= $form->field($model, 'camp')->textInput(['maxlength' => true, 'placeholder' => 'Camp']) ?>

    <?= $form->field($model, 'camp_specialty')->textInput(['maxlength' => true, 'placeholder' => 'Camp Specialty']) ?>

    <?= $form->field($model, 'camp_cost_regular')->textInput(['placeholder' => 'Camp Cost Regular']) ?>

    <?= $form->field($model, 'camp_cost_early_registration')->textInput(['placeholder' => 'Camp Cost Early Registration']) ?>

    <?= $form->field($model, 'camp_team_discounts_available_yn')->textInput(['maxlength' => true, 'placeholder' => 'Camp Team Discounts Available Yn']) ?>

    <?= $form->field($model, 'camp_scholarships_available_yn')->textInput(['maxlength' => true, 'placeholder' => 'Camp Scholarships Available Yn']) ?>

    <?= $form->field($model, 'camp_session_desc')->textInput(['maxlength' => true, 'placeholder' => 'Camp Session Desc']) ?>

    <?= $form->field($model, 'camp_website_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Website Url']) ?>

    <?= $form->field($model, 'camp_session_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Session Url']) ?>

    <?= $form->field($model, 'camp_registration_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Registration Url']) ?>

    <?= $form->field($model, 'camp_twitter_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Twitter Url']) ?>

    <?= $form->field($model, 'camp_facebook_url')->textInput(['maxlength' => true, 'placeholder' => 'Camp Facebook Url']) ?>

    <?= $form->field($model, 'camp_scholarship_application_info')->textInput(['maxlength' => true, 'placeholder' => 'Camp Scholarship Application Info']) ?>

    <?= $form->field($model, 'camp_scholarship_application_request')->textInput(['maxlength' => true, 'placeholder' => 'Camp Scholarship Application Request']) ?>

    <?= $form->field($model, 'camp_organizer_description')->textInput(['maxlength' => true, 'placeholder' => 'Camp Organizer Description']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampCoach'),
            'content' => $this->render('_formCampCoach', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campCoaches),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampLocation'),
            'content' => $this->render('_formCampLocation', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campLocations),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampSession'),
            'content' => $this->render('_formCampSession', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campSessions),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CampSport'),
            'content' => $this->render('_formCampSport', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->campSports),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PlayerCampLog'),
            'content' => $this->render('_formPlayerCampLog', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->playerCampLogs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Team'),
            'content' => $this->render('_formTeam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teams),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
