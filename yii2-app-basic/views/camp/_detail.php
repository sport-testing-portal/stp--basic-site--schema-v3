<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Camp */

?>
<div class="camp-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->camp) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'camp_id',
        [
            'attribute' => 'org.org',
            'label' => 'Org',
        ],
        'camp',
        'camp_specialty',
        'camp_cost_regular',
        'camp_cost_early_registration',
        'camp_team_discounts_available_yn',
        'camp_scholarships_available_yn',
        'camp_session_desc',
        'camp_website_url:url',
        'camp_session_url:url',
        'camp_registration_url:url',
        'camp_twitter_url:url',
        'camp_facebook_url:url',
        'camp_scholarship_application_info',
        'camp_scholarship_application_request',
        'camp_organizer_description',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>