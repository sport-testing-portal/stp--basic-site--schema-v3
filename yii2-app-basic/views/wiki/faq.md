# Frequently Asked Questions

##From Developers

### Models aren't generating on Linux
** If you're running Apache2 as your web server**
Are you a member of the Apache2 group eg www-data?
Is your username in the output of this command?

```shell
grep "^www-data" /etc/group
```

If you do see your username then you are in the www-data group

How to add yourself to the www-data group on your development workstation

```shell
sudo useradd -G www-data
```

Change the 'group' of the code generation folders

```shell
sudo chown -Rcv dbyrd:www-data models, controllers, views
```

Change the 'group' of the runtime folder where logs and assets are copied to.

```shell
sudo chown -Rcv www-data:www-data runtime
```

**See Also** https://www.cyberciti.biz/faq/howto-linux-add-user-to-group/