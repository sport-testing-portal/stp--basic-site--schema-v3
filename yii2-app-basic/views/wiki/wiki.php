<?php

/* Test the use of markdown to display wiki pages 
 * @var $this yii\web\View 
 */





use yii\helpers\Html;
use kartik\markdown\Markdown;
//use kartik\markdown\MarkdownEditor;
     


$this->title = 'Wiki';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the Wiki page. You may modify the following file to customize its content:

      
    </p>
    <list>
    <li>database-relations.md</li>
    
    </list>
// default call
<?php 
$relativeWikiPath = '/../wiki/';
$path = realpath(__DIR__);
$content = file_get_contents(realpath($path . '/../wiki/home.md'));
$contentLibrary=[];

echo Markdown::convert($content);    




 
// usage with Active Form 
//echo $form->field($model, 'markdown')->widget(
//	MarkdownEditor::classname(), 
//	['height' => 300, 'encodeLabels' => false]
//);

?>
    
    
    <code><?= __FILE__ ?></code>
</div>







