<?php

/* Test the use of markdown to display wiki pages 
 * @var $this yii\web\View 
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\markdown\Markdown;
//use kartik\markdown\MarkdownEditor;

$this->title = 'Yii2 Migration Creator';
$this->params['breadcrumbs'][] = [
    'label'=>'wiki',
    'url'=>Url::to(['wiki/'])
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

<?php 
$relativeWikiPath = '/../wiki/';
$path = realpath(__DIR__);
$content = file_get_contents(realpath($path . '/../wiki/extension-yii2-save-relations-behavior.md'));
$contentLibrary=[];

echo Markdown::convert($content);    




 
// usage with Active Form 
//echo $form->field($model, 'markdown')->widget(
//	MarkdownEditor::classname(), 
//	['height' => 300, 'encodeLabels' => false]
//);

?>
    
    
    <code><?= __FILE__ ?></code>
</div>







