Each table must have the following fields.

sport__dev_design_003

```sql
ALTER TABLE `sport__dev_design_003`.`org_governing_body` 
ADD COLUMN `created_at` DATETIME NULL AFTER `governing_body_id`,
ADD COLUMN `updated_at` DATETIME NULL AFTER `created_at`,
ADD COLUMN `created_by` INT NULL AFTER `updated_at`,
ADD COLUMN `updated_by` INT NULL AFTER `created_by`,
ADD COLUMN `lock` tinyint AFTER `updated_by`;
```