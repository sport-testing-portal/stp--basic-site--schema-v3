<?php

namespace app\models;

use Yii;
use \app\models\base\CampLocations as BaseCampLocations;

/**
 * This is the model class for table "camp_locations".
 */
class CampLocations extends BaseCampLocations
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['camp_id', 'address_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'camp_locations_id' => 'Camp Locations ID',
            'camp_id' => 'Camp ID',
            'address_id' => 'Address ID',
            'lock' => 'Lock',
        ];
    }
}
