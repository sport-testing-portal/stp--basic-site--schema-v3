<?php

namespace app\models;

use Yii;
use \app\models\base\OrgAddress as BaseOrgAddress;

/**
 * This is the model class for table "org_address".
 */
class OrgAddress extends BaseOrgAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'address_id'], 'required'],
            [['org_id', 'address_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_id' => 'Org ID',
            'address_id' => 'Address ID',
            'lock' => 'Lock',
        ];
    }
}
