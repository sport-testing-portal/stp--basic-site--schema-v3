<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Coach;

/**
 * app\models\CoachSearch represents the model behind the search form about `app\models\Coach`.
 */
 class CoachSearch extends Coach
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coach_id', 'person_id', 'coach_type_id', 'coach_team_coach_id', 'age_group_id', 'created_by', 'updated_by'], 'integer'],
            [['coach_specialty', 'coach_certifications', 'coach_comments', 'coach_qrcode_uri', 'coach_info_source_scrape_url', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Coach::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'coach_id' => $this->coach_id,
            'person_id' => $this->person_id,
            'coach_type_id' => $this->coach_type_id,
            'coach_team_coach_id' => $this->coach_team_coach_id,
            'age_group_id' => $this->age_group_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'coach_specialty', $this->coach_specialty])
            ->andFilterWhere(['like', 'coach_certifications', $this->coach_certifications])
            ->andFilterWhere(['like', 'coach_comments', $this->coach_comments])
            ->andFilterWhere(['like', 'coach_qrcode_uri', $this->coach_qrcode_uri])
            ->andFilterWhere(['like', 'coach_info_source_scrape_url', $this->coach_info_source_scrape_url])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
