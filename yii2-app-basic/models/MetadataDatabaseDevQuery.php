<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetadataDatabaseDev]].
 *
 * @see MetadataDatabaseDev
 */
class MetadataDatabaseDevQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataDatabaseDev[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataDatabaseDev|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
