<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PaymentAllocation]].
 *
 * @see PaymentAllocation
 */
class PaymentAllocationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PaymentAllocation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PaymentAllocation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
