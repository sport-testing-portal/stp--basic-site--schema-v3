<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Address;

/**
 * app\models\AddressSearch represents the model behind the search form about `app\models\Address`.
 */
 class AddressSearch extends Address
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_id', 'org_id', 'person_id', 'address_type_id', 'created_by', 'updated_by'], 'integer'],
            [['addr1', 'addr2', 'addr3', 'city', 'state_or_region', 'postal_code', 'country', 'country_code', 'effective_from_dt', 'effective_to_dt', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Address::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'address_id' => $this->address_id,
            'org_id' => $this->org_id,
            'person_id' => $this->person_id,
            'address_type_id' => $this->address_type_id,
            'effective_from_dt' => $this->effective_from_dt,
            'effective_to_dt' => $this->effective_to_dt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'addr1', $this->addr1])
            ->andFilterWhere(['like', 'addr2', $this->addr2])
            ->andFilterWhere(['like', 'addr3', $this->addr3])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'state_or_region', $this->state_or_region])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'country_code', $this->country_code])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
