<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetadataCodebaseMvcView]].
 *
 * @see MetadataCodebaseMvcView
 */
class MetadataCodebaseMvcViewQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataCodebaseMvcView[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataCodebaseMvcView|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
