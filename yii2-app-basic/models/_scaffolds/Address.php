<?php

namespace app\models\_scaffolds;

use Yii;
use \app\models\_scaffolds\base\Address as BaseAddress;

/**
 * This is the model class for table "address".
 */
class Address extends BaseAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'person_id', 'address_type_id', 'created_by', 'updated_by'], 'integer'],
            [['effective_from_dt', 'effective_to_dt', 'created_at', 'updated_at'], 'safe'],
            [['addr1', 'addr2', 'addr3'], 'string', 'max' => 100],
            [['city', 'state_or_region', 'postal_code', 'country'], 'string', 'max' => 45],
            [['country_code'], 'string', 'max' => 5],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'address_id' => 'Address ID',
            'org_id' => 'Org ID',
            'person_id' => 'Person ID',
            'address_type_id' => 'Address Type ID',
            'addr1' => 'Addr1',
            'addr2' => 'Addr2',
            'addr3' => 'Needed for some international addresses',
            'city' => 'City',
            'state_or_region' => 'State Or Region',
            'postal_code' => 'Postal Code',
            'country' => 'Country',
            'country_code' => 'Country Code',
            'effective_from_dt' => 'Effective From Dt',
            'effective_to_dt' => 'Effective To Dt',
            'lock' => 'Lock',
        ];
    }
}
