<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataCodebaseView;

/**
 * app\models\MetadataCodebaseViewSearch represents the model behind the search form about `app\models\MetadataCodebaseView`.
 */
 class MetadataCodebaseViewSearch extends MetadataCodebaseView
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_view_id', 'codebase_id', 'created_by', 'updated_by'], 'integer'],
            [['view_function', 'view_file', 'view_params', 'view_url', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataCodebaseView::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codebase_view_id' => $this->codebase_view_id,
            'codebase_id' => $this->codebase_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'view_function', $this->view_function])
            ->andFilterWhere(['like', 'view_file', $this->view_file])
            ->andFilterWhere(['like', 'view_params', $this->view_params])
            ->andFilterWhere(['like', 'view_url', $this->view_url])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
