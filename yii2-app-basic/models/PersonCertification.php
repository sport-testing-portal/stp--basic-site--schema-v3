<?php

namespace app\models;

use Yii;
use \app\models\base\PersonCertification as BasePersonCertification;

/**
 * This is the model class for table "person_certification".
 */
class PersonCertification extends BasePersonCertification
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_id', 'person_certification_type_id', 'person_certification_year', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person_certification_id' => 'Person Certification ID',
            'person_id' => 'Person ID',
            'person_certification_type_id' => 'Person Certification Type ID',
            'person_certification_year' => 'Person Certification Year',
            'lock' => 'Lock',
        ];
    }
}
