<?php

namespace app\models;

use Yii;
use \app\models\base\ContactMessageType as BaseContactMessageType;

/**
 * This is the model class for table "contact_message_type".
 */
class ContactMessageType extends BaseContactMessageType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['contact_message_type'], 'required'],
            [['contact_message_type', 'contact_message_type_short_desc', 'contact_message_type_long_desc'], 'string', 'max' => 45],
            [['contact_message_type'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'contact_message_type_id' => 'Contact Message Type ID',
            'contact_message_type' => 'Contact Message Type',
            'contact_message_type_short_desc' => 'Contact Message Type Short Desc',
            'contact_message_type_long_desc' => 'Contact Message Type Long Desc',
        ];
    }
}
