<?php

namespace app\models;

use Yii;
use \app\models\base\SubscriptionLog as BaseSubscriptionLog;

/**
 * This is the model class for table "subscription_log".
 */
class SubscriptionLog extends BaseSubscriptionLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['subscription_id', 'subscription_type_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'subscription_log_id' => 'Subscription Log ID',
            'subscription_id' => 'Subscription ID',
            'subscription_type_id' => 'Subscription Type ID',
            'lock' => 'Lock',
        ];
    }
}
