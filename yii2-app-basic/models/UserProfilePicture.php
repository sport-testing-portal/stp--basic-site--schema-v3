<?php

namespace app\models;

use Yii;
use \app\models\base\UserProfilePicture as BaseUserProfilePicture;

/**
 * This is the model class for table "user_profile_picture".
 */
class UserProfilePicture extends BaseUserProfilePicture
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'filename', 'width', 'height', 'mimetype', 'contents'], 'required'],
            [['user_id', 'original_picture_id', 'width', 'height'], 'integer'],
            [['created_on'], 'safe'],
            [['contents'], 'string'],
            [['filename', 'mimetype'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'original_picture_id' => 'Original Picture ID',
            'filename' => 'Filename',
            'width' => 'Width',
            'height' => 'Height',
            'mimetype' => 'Mimetype',
            'created_on' => 'Created On',
            'contents' => 'Contents',
        ];
    }
}
