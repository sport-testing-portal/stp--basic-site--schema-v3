<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataCodebase as BaseMetadataCodebase;

/**
 * This is the model class for table "metadata__codebase".
 */
class MetadataCodebase extends BaseMetadataCodebase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['file_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['file_name'], 'string', 'max' => 75],
            [['file_path_uri'], 'string', 'max' => 175],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'codebase_id' => 'Codebase ID',
            'file_name' => 'File Name',
            'file_path_uri' => 'File Path Uri',
            'lock' => 'Lock',
        ];
    }
}
