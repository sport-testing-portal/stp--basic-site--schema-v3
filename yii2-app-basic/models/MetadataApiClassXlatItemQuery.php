<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetadataApiClassXlatItem]].
 *
 * @see MetadataApiClassXlatItem
 */
class MetadataApiClassXlatItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataApiClassXlatItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataApiClassXlatItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
