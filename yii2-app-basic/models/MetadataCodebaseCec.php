<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataCodebaseCec as BaseMetadataCodebaseCec;

/**
 * This is the model class for table "metadata__codebase_cec".
 */
class MetadataCodebaseCec extends BaseMetadataCodebaseCec
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['codebase_cec'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['codebase_cec'], 'string', 'max' => 75],
            [['codebase_cec_desc_short', 'cec_status_tag'], 'string', 'max' => 45],
            [['codebase_cec_desc_long'], 'string', 'max' => 175],
            [['cec_scope'], 'string', 'max' => 253],
            [['rule_file_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'codebase_cec_id' => 'CEC ID',
            'codebase_cec' => 'Codebase CEC',
            'codebase_cec_desc_short' => 'CEC Desc Short',
            'codebase_cec_desc_long' => 'CEC Desc Long',
            'cec_status_tag' => 'Cec Status Tag',
            'cec_scope' => 'CEC Scope',
            'rule_file_uri' => 'Rule File Uri',
            'lock' => 'Lock',
        ];
    }
    
    
}
