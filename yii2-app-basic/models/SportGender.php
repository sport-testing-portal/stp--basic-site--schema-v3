<?php

namespace app\models;

use Yii;
use \app\models\base\SportGender as BaseSportGender;

/**
 * This is the model class for table "sport_gender".
 */
class SportGender extends BaseSportGender
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['sport_id', 'gender_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'sport_gender_id' => 'Sport Gender ID',
            'sport_id' => 'Sport ID',
            'gender_id' => 'Gender ID',
            'lock' => 'Lock',
        ];
    }
}
