<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwTestResultFullDebug]].
 *
 * @see VwTestResultFullDebug
 */
class VwTestResultFullDebugQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwTestResultFullDebug[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwTestResultFullDebug|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
