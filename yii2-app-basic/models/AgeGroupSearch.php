<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AgeGroup;

/**
 * app\models\AgeGroupSearch represents the model behind the search form about `app\models\AgeGroup`.
 */
 class AgeGroupSearch extends AgeGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['age_group_id', 'sport_id', 'age_group_min', 'age_group_max', 'display_order', 'created_by', 'updated_by'], 'integer'],
            [['age_group', 'age_group_desc', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgeGroup::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'age_group_id' => $this->age_group_id,
            'sport_id' => $this->sport_id,
            'age_group_min' => $this->age_group_min,
            'age_group_max' => $this->age_group_max,
            'display_order' => $this->display_order,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'age_group', $this->age_group])
            ->andFilterWhere(['like', 'age_group_desc', $this->age_group_desc])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
