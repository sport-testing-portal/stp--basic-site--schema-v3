<?php

namespace app\models;

use Yii;
use \app\models\base\OrgGoverningBody as BaseOrgGoverningBody;

/**
 * This is the model class for table "org_governing_body".
 */
class OrgGoverningBody extends BaseOrgGoverningBody
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'governing_body_id'], 'required'],
            [['org_id', 'governing_body_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_governing_body_id' => 'Org Governing Body ID',
            'org_id' => 'Org ID',
            'governing_body_id' => 'Governing Body ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'org_governing_body_id' => 'ID',
            'org_id' => 'Org ID',
            'governing_body_id' => 'Governing Body',
            'lock' => 'Lock',
        ];
    }    
}
