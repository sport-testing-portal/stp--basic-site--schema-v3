<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataApiProvider;

/**
 * app\models\MetadataApiProviderSearch represents the model behind the search form about `app\models\MetadataApiProvider`.
 */
 class MetadataApiProviderSearch extends MetadataApiProvider
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metadata__api_provider_id', 'created_by', 'updated_by'], 'integer'],
            [['metadata__api_provider', 'metadata__api_provider_desc', 'metadata__api_provider_url', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataApiProvider::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'metadata__api_provider_id' => $this->metadata__api_provider_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'metadata__api_provider', $this->metadata__api_provider])
            ->andFilterWhere(['like', 'metadata__api_provider_desc', $this->metadata__api_provider_desc])
            ->andFilterWhere(['like', 'metadata__api_provider_url', $this->metadata__api_provider_url])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
