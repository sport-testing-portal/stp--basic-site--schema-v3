<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwTeam]].
 *
 * @see VwTeam
 */
class VwTeamQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwTeam[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwTeam|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
