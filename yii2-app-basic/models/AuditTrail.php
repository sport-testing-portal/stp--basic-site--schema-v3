<?php

namespace app\models;

use Yii;
use \app\models\base\AuditTrail as BaseAuditTrail;

/**
 * This is the model class for table "audit_trail".
 */
class AuditTrail extends BaseAuditTrail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['entry_id', 'user_id'], 'integer'],
            [['action', 'model', 'model_id', 'created'], 'required'],
            [['old_value', 'new_value'], 'string'],
            [['created'], 'safe'],
            [['action', 'model', 'model_id', 'field'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'entry_id' => 'Entry ID',
            'user_id' => 'User ID',
            'action' => 'Action',
            'model' => 'Model',
            'model_id' => 'Model ID',
            'field' => 'Field',
            'old_value' => 'Old Value',
            'new_value' => 'New Value',
        ];
    }
}
