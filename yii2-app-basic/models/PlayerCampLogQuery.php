<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PlayerCampLog]].
 *
 * @see PlayerCampLog
 */
class PlayerCampLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PlayerCampLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PlayerCampLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
