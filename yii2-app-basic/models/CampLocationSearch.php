<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CampLocation;

/**
 * app\models\CampLocationSearch represents the model behind the search form about `app\models\CampLocation`.
 */
 class CampLocationSearch extends CampLocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_location_id', 'camp_id', 'address_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CampLocation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'camp_location_id' => $this->camp_location_id,
            'camp_id' => $this->camp_id,
            'address_id' => $this->address_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
