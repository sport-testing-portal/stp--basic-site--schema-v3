<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserRemoteIdentity]].
 *
 * @see UserRemoteIdentity
 */
class UserRemoteIdentityQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UserRemoteIdentity[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserRemoteIdentity|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
