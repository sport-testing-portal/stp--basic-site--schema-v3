<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PlayerSport]].
 *
 * @see PlayerSport
 */
class PlayerSportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PlayerSport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PlayerSport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
