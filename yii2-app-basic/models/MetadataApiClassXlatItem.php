<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataApiClassXlatItem as BaseMetadataApiClassXlatItem;

/**
 * This is the model class for table "metadata__api_class_xlat_item".
 */
class MetadataApiClassXlatItem extends BaseMetadataApiClassXlatItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['metadata__api_class_xlat_id'], 'required'],
            [['metadata__api_class_xlat_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['metadata__api_class_xlat_item', 'metadata__api_class_func_1_reference_example', 'metadata__api_class_func_2_reference_example', 'metadata__api_class_func_1_regex_find', 'metadata__api_class_func_2_regex_find'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'metadata__api_class_xlat_item_id' => 'API Class Xlat Item ID',
            'metadata__api_class_xlat_id' => 'API Class Xlat ID',
            'metadata__api_class_xlat_item' => 'API Class Xlat Item',
            'metadata__api_class_func_1_reference_example' => 'API Class Func 1 Reference Example',
            'metadata__api_class_func_2_reference_example' => 'API Class Func 2 Reference Example',
            'metadata__api_class_func_1_regex_find' => 'API Class Func 1 Regex Find',
            'metadata__api_class_func_2_regex_find' => 'API Class Func 2 Regex Find',
            'lock' => 'Lock',
        ];
    }
}
