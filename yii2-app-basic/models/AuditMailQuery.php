<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AuditMail]].
 *
 * @see AuditMail
 */
class AuditMailQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AuditMail[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AuditMail|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
