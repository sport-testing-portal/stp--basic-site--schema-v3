<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TeamCoach]].
 *
 * @see TeamCoach
 */
class TeamCoachQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TeamCoach[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TeamCoach|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
