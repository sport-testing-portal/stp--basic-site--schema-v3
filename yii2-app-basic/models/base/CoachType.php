<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "coach_type".
 *
 * @property integer $coach_type_id
 * @property string $coach_type
 * @property integer $display_order
 * @property string $coach_type_desc_short
 * @property string $coach_type_desc_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Coach[] $coaches
 */
class CoachType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'coaches'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['coach_type'], 'string', 'max' => 75],
            [['coach_type_desc_short'], 'string', 'max' => 45],
            [['coach_type_desc_long'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['coach_type'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coach_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'coach_type_id' => 'Coach Type ID',
            'coach_type' => 'Coach Type',
            'display_order' => 'Display Order',
            'coach_type_desc_short' => 'Coach Type Desc Short',
            'coach_type_desc_long' => 'Coach Type Desc Long',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoaches()
    {
        return $this->hasMany(\app\models\Coach::className(), ['coach_type_id' => 'coach_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\CoachTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CoachTypeQuery(get_called_class());
    }
}
