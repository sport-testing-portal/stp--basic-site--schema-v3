<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__api_class_xlat_item".
 *
 * @property integer $metadata__api_class_xlat_item_id
 * @property integer $metadata__api_class_xlat_id
 * @property string $metadata__api_class_xlat_item
 * @property string $metadata__api_class_func_1_reference_example
 * @property string $metadata__api_class_func_2_reference_example
 * @property string $metadata__api_class_func_1_regex_find
 * @property string $metadata__api_class_func_2_regex_find
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataApiClassXlat $metadataApiClassXlat
 * @since 0.8.0
 */
class MetadataApiClassXlatItem extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataApiClassXlat'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metadata__api_class_xlat_id'], 'required'],
            [['metadata__api_class_xlat_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['metadata__api_class_xlat_item', 'metadata__api_class_func_1_reference_example', 'metadata__api_class_func_2_reference_example', 'metadata__api_class_func_1_regex_find', 'metadata__api_class_func_2_regex_find'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__api_class_xlat_item';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'metadata__api_class_xlat_item_id' => 'API Class Xlat Item ID',
            'metadata__api_class_xlat_id' => 'API Class Xlat ID',
            'metadata__api_class_xlat_item' => 'API Class Xlat Item',
            'metadata__api_class_func_1_reference_example' => 'API Class Func 1 Reference Example',
            'metadata__api_class_func_2_reference_example' => 'API Class Func 2 Reference Example',
            'metadata__api_class_func_1_regex_find' => 'API Class Func 1 Regex Find',
            'metadata__api_class_func_2_regex_find' => 'API Class Func 2 Regex Find',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataApiClassXlat()
    {
        return $this->hasOne(\app\models\MetadataApiClassXlat::className(),
            ['metadata__api_class_xlat_id' => 'metadata__api_class_xlat_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataApiClassXlatItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataApiClassXlatItemQuery(get_called_class());
    }
}
