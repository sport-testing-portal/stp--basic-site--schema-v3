<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__database".
 *
 * @property integer $database_id
 * @property string $table_name
 * @property string $schema_name
 * @property string $app_module_name
 * @property string $developer_notes
 * @property integer $col_name_len_max
 * @property string $has_named_primary_key
 * @property string $has_v3_fields
 * @property string $has_representing_field
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataDatabaseCecStatus[] $metadataDatabaseCecStatuses
 * @property \app\models\MetadataDatabaseDevStatus[] $metadataDatabaseDevStatuses
 */
class MetadataDatabase extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataDatabaseCecStatuses',
            'metadataDatabaseDevStatuses'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_name', 'schema_name'], 'required'],
            [['col_name_len_max', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['table_name', 'schema_name'], 'string', 'max' => 150],
            [['app_module_name'], 'string', 'max' => 75],
            [['developer_notes'], 'string', 'max' => 100],
            [['has_named_primary_key', 'has_v3_fields', 'has_representing_field'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__database';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'database_id' => 'Database ID',
            'table_name' => 'Table Name',
            'schema_name' => 'Schema Name',
            'app_module_name' => 'App Module Name',
            'developer_notes' => 'Developer Notes',
            'col_name_len_max' => 'Col Name Len Max',
            'has_named_primary_key' => 'Has Named Primary Key',
            'has_v3_fields' => 'Has V3 Fields',
            'has_representing_field' => 'Has Representing Field',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataDatabaseCecStatuses()
    {
        return $this->hasMany(\app\models\MetadataDatabaseCecStatus::className(), ['database_id' => 'database_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataDatabaseDevStatuses()
    {
        return $this->hasMany(\app\models\MetadataDatabaseDevStatus::className(), ['database_id' => 'database_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataDatabaseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataDatabaseQuery(get_called_class());
    }
}
