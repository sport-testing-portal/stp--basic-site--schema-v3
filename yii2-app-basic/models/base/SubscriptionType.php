<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "subscription_type".
 *
 * @property integer $subscription_type_id
 * @property string $subscription_type
 * @property string $subscription_desc_short
 * @property string $subscription_desc_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Subscription[] $subscriptions
 * @property \app\models\SubscriptionLog[] $subscriptionLogs
 */
class SubscriptionType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'subscriptions',
            'subscriptionLogs'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['subscription_type'], 'string', 'max' => 75],
            [['subscription_desc_short'], 'string', 'max' => 45],
            [['subscription_desc_long'], 'string', 'max' => 150],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subscription_type_id' => 'Subscription Type ID',
            'subscription_type' => 'Subscription Type',
            'subscription_desc_short' => 'Subscription Desc Short',
            'subscription_desc_long' => 'Subscription Desc Long',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(\app\models\Subscription::className(), ['subscription_type_id' => 'subscription_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionLogs()
    {
        return $this->hasMany(\app\models\SubscriptionLog::className(), ['subscription_type_id' => 'subscription_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SubscriptionTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SubscriptionTypeQuery(get_called_class());
    }
}
