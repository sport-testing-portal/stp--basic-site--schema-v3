<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "event".
 *
 * @property integer $event_id
 * @property integer $event_type_id
 * @property string $event
 * @property string $event_access_code
 * @property string $event_location_name
 * @property string $event_location_map_url
 * @property string $event_scheduled_dt
 * @property string $event_participation_fee
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\EventType $eventType
 * @property \app\models\EventAttendeeLog[] $eventAttendeeLogs
 * @property \app\models\EventCombine[] $eventCombines
 */
class Event extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'eventType',
            'eventAttendeeLogs',
            'eventCombines'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_type_id', 'created_by', 'updated_by'], 'integer'],
            [['event_scheduled_dt', 'created_at', 'updated_at'], 'safe'],
            [['event_participation_fee'], 'number'],
            [['event'], 'string', 'max' => 150],
            [['event_access_code'], 'string', 'max' => 25],
            [['event_location_name'], 'string', 'max' => 100],
            [['event_location_map_url'], 'string', 'max' => 175],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'event_id' => 'Event ID',
            'event_type_id' => 'Event Type ID',
            'event' => 'Event',
            'event_access_code' => 'Event Access Code',
            'event_location_name' => 'Event Location Name',
            'event_location_map_url' => 'Event Location Map Url',
            'event_scheduled_dt' => 'Event Scheduled Dt',
            'event_participation_fee' => 'Event Participation Fee',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventType()
    {
        return $this->hasOne(\app\models\EventType::className(), ['event_type_id' => 'event_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventAttendeeLogs()
    {
        return $this->hasMany(\app\models\EventAttendeeLog::className(), ['event_id' => 'event_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventCombines()
    {
        return $this->hasMany(\app\models\EventCombine::className(), ['event_id' => 'event_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\EventQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EventQuery(get_called_class());
    }
}
