<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "search_criteria_template".
 *
 * @property integer $search_criteria_template_id
 * @property integer $person_id
 * @property string $template_name
 * @property string $template_comment
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Person $person
 * @property \app\models\SearchCriteriaTemplateItem[] $searchCriteriaTemplateItems
 * @property \app\models\SearchHistory[] $searchHistories
 */
class SearchCriteriaTemplate extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'person',
            'searchCriteriaTemplateItems',
            'searchHistories'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['template_name'], 'string', 'max' => 75],
            [['template_comment'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search_criteria_template';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'search_criteria_template_id' => 'Search Criteria Template ID',
            'person_id' => 'Person ID',
            'template_name' => 'Template Name',
            'template_comment' => 'Template Comment',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchCriteriaTemplateItems()
    {
        return $this->hasMany(\app\models\SearchCriteriaTemplateItem::className(), ['search_criteria_template_id' => 'search_criteria_template_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchHistories()
    {
        return $this->hasMany(\app\models\SearchHistory::className(), ['search_criteria_template_id' => 'search_criteria_template_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SearchCriteriaTemplateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SearchCriteriaTemplateQuery(get_called_class());
    }
}
