<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "test_eval_summary_log".
 *
 * @property integer $test_eval_summary_log_id
 * @property integer $person_id
 * @property integer $team_id
 * @property integer $test_eval_type_id
 * @property integer $source_event_id
 * @property string $test_eval_overall_ranking
 * @property string $test_eval_positional_ranking
 * @property string $test_eval_total_score
 * @property string $test_eval_avg_score
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\TestEvalDetailLog[] $testEvalDetailLogs
 * @property \app\models\TestEvalLog[] $testEvalLogs
 * @property \app\models\Person $person
 * @property \app\models\TestEvalType $testEvalType
 * @property \app\models\Team $team
 */
class TestEvalSummaryLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'testEvalDetailLogs',
            'testEvalLogs',
            'person',
            'testEvalType',
            'team'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'team_id', 'test_eval_type_id', 'source_event_id', 'created_by', 'updated_by'], 'integer'],
            [['test_eval_overall_ranking', 'test_eval_positional_ranking', 'test_eval_total_score', 'test_eval_avg_score'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_eval_summary_log';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'person_id' => 'Person ID',
            'team_id' => 'Team ID',
            'test_eval_type_id' => 'Test Eval Type ID',
            'source_event_id' => 'Source Event ID',
            'test_eval_overall_ranking' => 'Test Eval Overall Ranking',
            'test_eval_positional_ranking' => 'Test Eval Positional Ranking',
            'test_eval_total_score' => 'Test Eval Total Score',
            'test_eval_avg_score' => 'Test Eval Avg Score',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalDetailLogs()
    {
        return $this->hasMany(\app\models\TestEvalDetailLog::className(), ['test_eval_summary_log_id' => 'test_eval_summary_log_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalLogs()
    {
        return $this->hasMany(\app\models\TestEvalLog::className(), ['test_eval_summary_log_id' => 'test_eval_summary_log_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalType()
    {
        return $this->hasOne(\app\models\TestEvalType::className(), ['test_eval_type_id' => 'test_eval_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(\app\models\Team::className(), ['team_id' => 'team_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TestEvalSummaryLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TestEvalSummaryLogQuery(get_called_class());
    }
}
