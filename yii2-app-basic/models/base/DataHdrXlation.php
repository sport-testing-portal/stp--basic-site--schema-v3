<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "data_hdr_xlation".
 *
 * @property integer $data_hdr_xlation_id
 * @property string $data_hdr_xlation
 * @property string $data_hdr_entity_name
 * @property string $hdr_sha1_hash
 * @property string $data_hdr_regex
 * @property string $data_hdr_is_trash_yn
 * @property string $gsm_target_table_name
 * @property string $data_hdr_entity_fetch_url
 * @property string $data_hdr_xlation_visual_example
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\DataHdrXlationItem[] $dataHdrXlationItems
 * 
 * @since 0.8.0
 */
class DataHdrXlation extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'dataHdrXlationItems'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['data_hdr_xlation', 'data_hdr_entity_fetch_url'], 'string', 'max' => 150],
            [['data_hdr_entity_name'], 'string', 'max' => 75],
            [['hdr_sha1_hash'], 'string', 'max' => 40],
            [['data_hdr_regex'], 'string', 'max' => 65],
            [['data_hdr_is_trash_yn', 'lock'], 'string', 'max' => 1],
            [['gsm_target_table_name', 'data_hdr_xlation_visual_example'], 'string', 'max' => 253],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_hdr_xlation';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'data_hdr_xlation_id' => 'Hdr Xlation ID',
            'data_hdr_xlation' => 'Hdr Xlation',
            'data_hdr_entity_name' => 'Hdr Entity Name',
            'hdr_sha1_hash' => 'Hdr Sha1 Hash',
            'data_hdr_regex' => 'Hdr Regex',
            'data_hdr_is_trash_yn' => 'Hdr Is Trash Yn',
            'gsm_target_table_name' => 'Target Table Name',
            'data_hdr_entity_fetch_url' => 'Hdr Entity Fetch Url',
            'data_hdr_xlation_visual_example' => 'Hdr Xlation Visual Example',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataHdrXlationItems()
    {
        return $this->hasMany(\app\models\DataHdrXlationItem::className(), ['data_hdr_xlation_id' => 'data_hdr_xlation_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\DataHdrXlationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\DataHdrXlationQuery(get_called_class());
    }
}
