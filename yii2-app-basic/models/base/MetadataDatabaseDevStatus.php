<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__database_dev_status".
 *
 * @property integer $dev_status_id
 * @property integer $database_id
 * @property integer $database_dev_id
 * @property string $dev_status
 * @property string $dev_status_tag
 * @property string $dev_status_bfr
 * @property string $dev_status_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataDatabase $database
 * @property \app\models\MetadataDatabaseDev $databaseDev
 */
class MetadataDatabaseDevStatus extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'database',
            'databaseDev'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['database_id', 'database_dev_id', 'dev_status'], 'required'],
            [['database_id', 'database_dev_id', 'created_by', 'updated_by'], 'integer'],
            [['dev_status_at', 'created_at', 'updated_at'], 'safe'],
            [['dev_status', 'dev_status_tag', 'dev_status_bfr'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__database_dev_status';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dev_status_id' => 'Dev Status ID',
            'database_id' => 'Database ID',
            'database_dev_id' => 'Database Dev ID',
            'dev_status' => 'Dev Status',
            'dev_status_tag' => 'Dev Status Tag',
            'dev_status_bfr' => 'Dev Status Bfr',
            'dev_status_at' => 'Dev Status At',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatabase()
    {
        return $this->hasOne(\app\models\MetadataDatabase::className(), ['database_id' => 'database_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatabaseDev()
    {
        return $this->hasOne(\app\models\MetadataDatabaseDev::className(), ['database_dev_id' => 'database_dev_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataDatabaseDevStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataDatabaseDevStatusQuery(get_called_class());
    }
}
