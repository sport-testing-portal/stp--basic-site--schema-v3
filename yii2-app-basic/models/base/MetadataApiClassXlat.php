<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__api_class_xlat".
 *
 * @property integer $metadata__api_class_xlat_id
 * @property integer $metadata__api_class_id_1
 * @property integer $metadata__api_class_id_2
 * @property string $metadata__api_class_xlat
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataApiClass $metadataApiClassId1
 * @property \app\models\MetadataApiClassXlatItem[] $metadataApiClassXlatItems
 * @since 0.8.0
 */
class MetadataApiClassXlat extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataApiClassId1',
            'metadataApiClassXlatItems'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metadata__api_class_id_1', 'metadata__api_class_id_2'], 'required'],
            [['metadata__api_class_id_1', 'metadata__api_class_id_2', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['metadata__api_class_xlat'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__api_class_xlat';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'metadata__api_class_xlat_id' => 'API Class Xlat ID',
            'metadata__api_class_id_1' => 'API Class Id 1',
            'metadata__api_class_id_2' => 'API Class Id 2',
            'metadata__api_class_xlat' => 'API Class Xlat',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataApiClassId1()
    {
        return $this->hasOne(\app\models\MetadataApiClass::className(), ['metadata__api_class_id' => 'metadata__api_class_id_1']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataApiClassXlatItems()
    {
        return $this->hasMany(\app\models\MetadataApiClassXlatItem::className(), ['metadata__api_class_xlat_id' => 'metadata__api_class_xlat_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataApiClassXlatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataApiClassXlatQuery(get_called_class());
    }
}
