<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "player_academic".
 *
 * @property integer $player_academic_id
 * @property integer $player_id
 * @property integer $sat_composite_score_max
 * @property integer $sat_critical_reading_score_max
 * @property integer $sat_math_score_max
 * @property integer $sat_writing_score_max
 * @property string $sat_scheduled_dt
 * @property integer $psat_composite_score_max
 * @property integer $psat_critical_reading_score_max
 * @property integer $psat_math_score_max
 * @property integer $psat_writing_score_max
 * @property integer $act_composite_score_max
 * @property integer $act_math_score_max
 * @property integer $act_english_score_max
 * @property string $act_scheduled_dt
 * @property string $grade_point_average
 * @property integer $class_rank
 * @property integer $school_size
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Player $player
 */
class PlayerAcademic extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'player'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id', 'sat_composite_score_max', 'sat_critical_reading_score_max', 'sat_math_score_max', 'sat_writing_score_max', 'psat_composite_score_max', 'psat_critical_reading_score_max', 'psat_math_score_max', 'psat_writing_score_max', 'act_composite_score_max', 'act_math_score_max', 'act_english_score_max', 'class_rank', 'school_size', 'created_by', 'updated_by'], 'integer'],
            [['sat_scheduled_dt', 'act_scheduled_dt', 'created_at', 'updated_at'], 'safe'],
            [['grade_point_average'], 'number'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_academic';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'player_academic_id' => 'Player Academic ID',
            'player_id' => 'Player ID',
            'sat_composite_score_max' => 'Sat Composite Score Max',
            'sat_critical_reading_score_max' => 'Sat Critical Reading Score Max',
            'sat_math_score_max' => 'Sat Math Score Max',
            'sat_writing_score_max' => 'Sat Writing Score Max',
            'sat_scheduled_dt' => 'Sat Scheduled Dt',
            'psat_composite_score_max' => 'Psat Composite Score Max',
            'psat_critical_reading_score_max' => 'Psat Critical Reading Score Max',
            'psat_math_score_max' => 'Psat Math Score Max',
            'psat_writing_score_max' => 'Psat Writing Score Max',
            'act_composite_score_max' => 'Act Composite Score Max',
            'act_math_score_max' => 'Act Math Score Max',
            'act_english_score_max' => 'Act English Score Max',
            'act_scheduled_dt' => 'Act Scheduled Dt',
            'grade_point_average' => 'Grade Point Average',
            'class_rank' => 'Class Rank',
            'school_size' => 'School Size',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(\app\models\Player::className(), ['player_id' => 'player_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PlayerAcademicQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PlayerAcademicQuery(get_called_class());
    }
}
