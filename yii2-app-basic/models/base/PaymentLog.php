<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "payment_log".
 *
 * @property integer $payment_log_id
 * @property integer $payment_type_id
 * @property integer $data_entity_id
 * @property integer $subscription_id
 * @property integer $org_id
 * @property integer $person_id
 * @property string $payment_amt
 * @property string $payment_dt
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Org $org
 * @property \app\models\PaymentType $paymentType
 * @property \app\models\Person $person
 * @property \app\models\Subscription $subscription
 * @property \app\models\PaymentXfer[] $paymentXfers
 */
class PaymentLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'org',
            'paymentType',
            'person',
            'subscription',
            'paymentXfers'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_type_id', 'data_entity_id', 'subscription_id', 'org_id', 'person_id', 'created_by', 'updated_by'], 'integer'],
            [['payment_amt'], 'number'],
            [['payment_dt', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_log';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_log_id' => 'Payment Log ID',
            'payment_type_id' => 'Payment Type ID',
            'data_entity_id' => 'Data Entity ID',
            'subscription_id' => 'Subscription ID',
            'org_id' => 'Org ID',
            'person_id' => 'Person ID',
            'payment_amt' => 'Payment Amt',
            'payment_dt' => 'Payment Dt',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(\app\models\Org::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentType()
    {
        return $this->hasOne(\app\models\PaymentType::className(), ['payment_type_id' => 'payment_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(\app\models\Subscription::className(), ['subscription_id' => 'subscription_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentXfers()
    {
        return $this->hasMany(\app\models\PaymentXfer::className(), ['payment_log_id' => 'payment_log_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PaymentLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PaymentLogQuery(get_called_class());
    }
}
