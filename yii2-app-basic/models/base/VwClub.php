<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwClub".
 *
 * @property string $org_type_name
 * @property integer $org_id
 * @property integer $org_type_id
 * @property string $org_name
 * @property string $org_website_url
 * @property string $org_twitter_url
 * @property string $org_facebook_url
 * @property string $org_phone_main
 * @property string $org_email_main
 * @property string $org_addr1
 * @property string $org_addr2
 * @property string $org_addr3
 * @property string $org_city
 * @property string $org_state_or_region
 * @property string $org_postal_code
 * @property string $org_country_code_iso3
 * @property string $created_at
 * @property string $updated_at
 * @property string $org_created_by_username
 * @property string $org_updated_by_username
 * @property integer $created_by_user_id
 * @property integer $updated_by_user_id
 */
class VwClub extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'org_type_id', 'created_by_user_id', 'updated_by_user_id'], 'integer'],
            [['org_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_type_name', 'org_state_or_region'], 'string', 'max' => 45],
            [['org_name', 'org_addr1', 'org_addr2', 'org_addr3'], 'string', 'max' => 100],
            [['org_website_url', 'org_twitter_url', 'org_facebook_url'], 'string', 'max' => 150],
            [['org_phone_main', 'org_postal_code'], 'string', 'max' => 25],
            [['org_email_main', 'org_city'], 'string', 'max' => 75],
            [['org_country_code_iso3'], 'string', 'max' => 3],
            [['org_created_by_username', 'org_updated_by_username'], 'string', 'max' => 131],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwClub';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'org_type_name' => 'Org Type Name',
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'org_name' => 'Org Name',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'org_addr1' => 'Org Addr1',
            'org_addr2' => 'Org Addr2',
            'org_addr3' => 'Org Addr3',
            'org_city' => 'Org City',
            'org_state_or_region' => 'Org State Or Region',
            'org_postal_code' => 'Org Postal Code',
            'org_country_code_iso3' => 'Org Country Code Iso3',
            'org_created_by_username' => 'Org Created By Username',
            'org_updated_by_username' => 'Org Updated By Username',
            'created_by_user_id' => 'Created By User ID',
            'updated_by_user_id' => 'Updated By User ID',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwClubQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwClubQuery(get_called_class());
    }
}
