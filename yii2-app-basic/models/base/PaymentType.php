<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "payment_type".
 *
 * @property integer $payment_type_id
 * @property integer $payment_type_allocation_template_id
 * @property string $payment_type
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\PaymentLog[] $paymentLogs
 * @property \app\models\PaymentTypeAllocationTemplate $paymentTypeAllocationTemplate
 * @property \app\models\PaymentTypeAllocation[] $paymentTypeAllocations
 */
class PaymentType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'paymentLogs',
            'paymentTypeAllocationTemplate',
            'paymentTypeAllocations'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_type_allocation_template_id', 'created_by', 'updated_by'], 'integer'],
            [['payment_type'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['payment_type'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['payment_type'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_type_id' => 'Payment Type ID',
            'payment_type_allocation_template_id' => 'Payment Type Allocation Template ID',
            'payment_type' => 'Payment Type',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentLogs()
    {
        return $this->hasMany(\app\models\PaymentLog::className(), ['payment_type_id' => 'payment_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTypeAllocationTemplate()
    {
        return $this->hasOne(\app\models\PaymentTypeAllocationTemplate::className(), ['payment_type_allocation_template_id' => 'payment_type_allocation_template_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTypeAllocations()
    {
        return $this->hasMany(\app\models\PaymentTypeAllocation::className(), ['payment_type_id' => 'payment_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PaymentTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PaymentTypeQuery(get_called_class());
    }
}
