<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "team_player".
 *
 * @property integer $team_player_id
 * @property integer $team_id
 * @property integer $player_id
 * @property integer $team_play_sport_position_id
 * @property integer $team_play_sport_position2_id
 * @property string $primary_position
 * @property string $team_play_statistical_highlights
 * @property integer $coach_id
 * @property string $coach_name
 * @property integer $team_play_current_team
 * @property string $team_play_begin_dt
 * @property string $team_play_end_dt
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Player[] $players
 * @property \app\models\Player $player
 * @property \app\models\SportPosition $teamPlaySportPosition
 * @property \app\models\Team $team
 */
class TeamPlayer extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'players',
            'player',
            'teamPlaySportPosition',
            'team'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'player_id'], 'required'],
            [['team_id', 'player_id', 'team_play_sport_position_id', 'team_play_sport_position2_id', 'coach_id', 'created_by', 'updated_by'], 'integer'],
            [['team_play_begin_dt', 'team_play_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['primary_position', 'coach_name'], 'string', 'max' => 45],
            [['team_play_statistical_highlights'], 'string', 'max' => 300],
            [['team_play_current_team'], 'string', 'max' => 4],
            [['lock'], 'string', 'max' => 1],
            [['team_id', 'player_id'], 'unique', 'targetAttribute' => ['team_id', 'player_id'], 'message' => 'The combination of Team ID and Player ID has already been taken.'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team_player';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_player_id' => 'Team Player ID',
            'team_id' => 'Team ID',
            'player_id' => 'Player ID',
            'team_play_sport_position_id' => 'Team Play Sport Position ID',
            'team_play_sport_position2_id' => 'Team Play Sport Position2 ID',
            'primary_position' => 'Primary Position',
            'team_play_statistical_highlights' => 'Team Play Statistical Highlights',
            'coach_id' => 'Coach ID',
            'coach_name' => 'Coach Name',
            'team_play_current_team' => 'Team Play Current Team',
            'team_play_begin_dt' => 'Team Play Begin Dt',
            'team_play_end_dt' => 'Team Play End Dt',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(\app\models\Player::className(), ['player_team_player_id' => 'team_player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(\app\models\Player::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamPlaySportPosition()
    {
        return $this->hasOne(\app\models\SportPosition::className(), ['sport_position_id' => 'team_play_sport_position_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(\app\models\Team::className(), ['team_id' => 'team_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TeamPlayerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TeamPlayerQuery(get_called_class());
    }
}
