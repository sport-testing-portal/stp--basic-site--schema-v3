<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwTestDescription".
 *
 * @property string $test_category
 * @property string $test_type
 * @property string $test_desc
 * @property integer $provider_id
 * @property integer $cat_id
 * @property integer $type_id
 * @property integer $desc_id
 * @property integer $cat_order
 * @property integer $type_order
 * @property integer $desc_order
 * @property string $test_desc_to_display
 * @property string $provider_code
 */
class VwTestDescription extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_category'], 'required'],
            [['provider_id', 'cat_id', 'type_id', 'desc_id', 'cat_order', 'type_order', 'desc_order'], 'integer'],
            [['test_category', 'test_desc'], 'string', 'max' => 45],
            [['test_type'], 'string', 'max' => 150],
            [['test_desc_to_display'], 'string', 'max' => 131],
            [['provider_code'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwTestDescription';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'test_category' => 'Test Category',
            'test_type' => 'Test Type',
            'test_desc' => 'Test Desc',
            'provider_id' => 'Provider ID',
            'cat_id' => 'Cat ID',
            'type_id' => 'Type ID',
            'desc_id' => 'Desc ID',
            'cat_order' => 'Cat Order',
            'type_order' => 'Type Order',
            'desc_order' => 'Desc Order',
            'test_desc_to_display' => 'Test Desc To Display',
            'provider_code' => 'Provider Code',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwTestDescriptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwTestDescriptionQuery(get_called_class());
    }
}
