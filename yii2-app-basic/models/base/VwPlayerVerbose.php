<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwPlayer_verbose".
 *
 * @property string $person_name
 * @property string $src_table0
 * @property integer $user_id
 * @property string $logon_email
 * @property string $last_visit_on
 * @property string $src_table1
 * @property integer $player_id
 * @property integer $person_id
 * @property string $player_access_code
 * @property string $player_waiver_minor_dt
 * @property string $player_waiver_adult_dt
 * @property string $player_parent_email
 * @property string $player_sport_preference
 * @property string $player_sport_position_preference
 * @property string $player_shot_side_preference
 * @property string $src_table2
 * @property integer $org_id
 * @property integer $app_user_id
 * @property string $person_name_prefix
 * @property string $person_name_first
 * @property string $person_name_middle
 * @property string $person_name_last
 * @property string $person_name_suffix
 * @property string $person_name_full
 * @property string $person_phone_personal
 * @property string $person_email_personal
 * @property string $person_phone_work
 * @property string $person_email_work
 * @property string $person_position_work
 * @property string $person_image_headshot_url
 * @property string $person_name_nickname
 * @property string $person_date_of_birth
 * @property string $person_dob_eng
 * @property string $person_bday_short
 * @property string $person_bday_long
 * @property double $person_age
 * @property string $person_height
 * @property string $person_height_imperial
 * @property integer $person_weight
 * @property string $person_tshirt_size
 * @property string $person_addr_1
 * @property string $person_addr_2
 * @property string $person_addr_3
 * @property string $person_city
 * @property string $person_postal_code
 * @property string $person_country
 * @property string $person_country_code
 * @property string $person_state_or_region
 * @property string $src_table3
 * @property integer $person_type_id
 * @property string $person_type_name
 * @property string $person_type_desc_short
 * @property string $person_type_desc_long
 * @property string $src_table4
 * @property string $gender_desc
 * @property string $gender_code
 * @property string $src_table5
 * @property integer $team_player_id
 * @property integer $team_id
 * @property string $team_play_begin_dt
 * @property string $team_play_end_dt
 * @property string $src_table6
 * @property integer $team_org_id
 * @property integer $team_school_id
 * @property integer $team_sport_id
 * @property integer $team_camp_id
 * @property integer $team_gender_id
 * @property string $team_name
 * @property string $team_gender
 * @property string $team_division
 * @property string $team_age_group
 */
class VwPlayerVerbose extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'player_id', 'person_id', 'org_id', 'app_user_id', 'person_weight', 'person_type_id', 'team_player_id', 'team_id', 'team_org_id', 'team_school_id', 'team_sport_id', 'team_camp_id', 'team_gender_id'], 'integer'],
            [['last_visit_on', 'player_waiver_minor_dt', 'player_waiver_adult_dt', 'person_date_of_birth', 'team_play_begin_dt', 'team_play_end_dt'], 'safe'],
            [['person_age'], 'number'],
            [['person_type_name'], 'required'],
            [['person_name', 'person_name_full'], 'string', 'max' => 90],
            [['src_table0', 'src_table6'], 'string', 'max' => 4],
            [['logon_email'], 'string', 'max' => 255],
            [['src_table1', 'src_table2', 'src_table4'], 'string', 'max' => 6],
            [['player_access_code', 'player_sport_position_preference', 'person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_height_imperial', 'person_country', 'person_state_or_region', 'person_type_name', 'person_type_desc_short', 'person_type_desc_long', 'team_age_group'], 'string', 'max' => 45],
            [['player_parent_email', 'person_email_personal', 'person_email_work'], 'string', 'max' => 60],
            [['player_sport_preference'], 'string', 'max' => 35],
            [['player_shot_side_preference', 'person_name_prefix', 'person_postal_code'], 'string', 'max' => 25],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3'], 'string', 'max' => 100],
            [['person_dob_eng', 'person_tshirt_size', 'team_division'], 'string', 'max' => 10],
            [['person_bday_short'], 'string', 'max' => 37],
            [['person_bday_long'], 'string', 'max' => 69],
            [['person_height', 'gender_code'], 'string', 'max' => 5],
            [['person_city', 'team_name'], 'string', 'max' => 75],
            [['person_country_code'], 'string', 'max' => 3],
            [['src_table3', 'src_table5'], 'string', 'max' => 11],
            [['gender_desc'], 'string', 'max' => 30],
            [['team_gender'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwPlayer_verbose';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_name' => 'Person Name',
            'src_table0' => 'Src Table0',
            'user_id' => 'User ID',
            'logon_email' => 'Logon Email',
            'last_visit_on' => 'Last Visit On',
            'src_table1' => 'Src Table1',
            'player_id' => 'Player ID',
            'person_id' => 'Person ID',
            'player_access_code' => 'Player Access Code',
            'player_waiver_minor_dt' => 'Player Waiver Minor Dt',
            'player_waiver_adult_dt' => 'Player Waiver Adult Dt',
            'player_parent_email' => 'Player Parent Email',
            'player_sport_preference' => 'Player Sport Preference',
            'player_sport_position_preference' => 'Player Sport Position Preference',
            'player_shot_side_preference' => 'Player Shot Side Preference',
            'src_table2' => 'Src Table2',
            'org_id' => 'Org ID',
            'app_user_id' => 'App User ID',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_name_full' => 'Person Name Full',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_dob_eng' => 'Person Dob Eng',
            'person_bday_short' => 'Person Bday Short',
            'person_bday_long' => 'Person Bday Long',
            'person_age' => 'Person Age',
            'person_height' => 'Person Height',
            'person_height_imperial' => 'Person Height Imperial',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'src_table3' => 'Src Table3',
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
            'person_type_desc_short' => 'Person Type Desc Short',
            'person_type_desc_long' => 'Person Type Desc Long',
            'src_table4' => 'Src Table4',
            'gender_desc' => 'Gender Desc',
            'gender_code' => 'Gender Code',
            'src_table5' => 'Src Table5',
            'team_player_id' => 'Team Player ID',
            'team_id' => 'Team ID',
            'team_play_begin_dt' => 'Team Play Begin Dt',
            'team_play_end_dt' => 'Team Play End Dt',
            'src_table6' => 'Src Table6',
            'team_org_id' => 'Team Org ID',
            'team_school_id' => 'Team School ID',
            'team_sport_id' => 'Team Sport ID',
            'team_camp_id' => 'Team Camp ID',
            'team_gender_id' => 'Team Gender ID',
            'team_name' => 'Team Name',
            'team_gender' => 'Team Gender',
            'team_division' => 'Team Division',
            'team_age_group' => 'Team Age Group',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwPlayerVerboseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwPlayerVerboseQuery(get_called_class());
    }
}
