<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "entity_link_type".
 *
 * @property integer $entity_link_type_id
 * @property string $entity_link_type
 * @property string $entity_link_type_desc_short
 * @property string $entity_link_type_desc_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\EntityLink[] $entityLinks
 */
class EntityLinkType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'entityLinks'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_link_type'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['entity_link_type', 'entity_link_type_desc_short', 'entity_link_type_desc_long'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['entity_link_type'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entity_link_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entity_link_type_id' => 'Entity Link Type ID',
            'entity_link_type' => 'Entity Link Type',
            'entity_link_type_desc_short' => 'Entity Link Type Desc Short',
            'entity_link_type_desc_long' => 'Entity Link Type Desc Long',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntityLinks()
    {
        return $this->hasMany(\app\models\EntityLink::className(), ['entity_link_type_id' => 'entity_link_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\EntityLinkTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EntityLinkTypeQuery(get_called_class());
    }
}
