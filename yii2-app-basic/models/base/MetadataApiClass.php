<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__api_class".
 *
 * @property integer $metadata__api_class_id
 * @property integer $metadata__api_provider_id
 * @property string $metadata__api_class
 * @property string $metadata__api_class_desc
 * @property string $metadata__api_class_docs_url
 * @property string $metadata__api_reference_example
 * @property string $metadata__api_regex_find
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataApiProvider $metadataApiProvider
 * @property \app\models\MetadataApiClassFunc[] $metadataApiClassFuncs
 * @property \app\models\MetadataApiClassXlat[] $metadataApiClassXlats
 * 
 * @since 0.8.0
 */
class MetadataApiClass extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataApiProvider',
            'metadataApiClassFuncs',
            'metadataApiClassXlats'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metadata__api_provider_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['metadata__api_class', 'metadata__api_class_desc', 'metadata__api_class_docs_url', 'metadata__api_reference_example', 'metadata__api_regex_find'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__api_class';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'metadata__api_class_id' => 'API Class ID',
            'metadata__api_provider_id' => 'API Provider ID',
            'metadata__api_class' => 'API Class',
            'metadata__api_class_desc' => 'API Class Desc',
            'metadata__api_class_docs_url' => 'API Class Docs Url',
            'metadata__api_reference_example' => 'API Reference Example',
            'metadata__api_regex_find' => 'API Regex Find',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataApiProvider()
    {
        return $this->hasOne(\app\models\MetadataApiProvider::className(),
            ['metadata__api_provider_id' => 'metadata__api_provider_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataApiClassFuncs()
    {
        return $this->hasMany(\app\models\MetadataApiClassFunc::className(),
            ['metadata__api_class_id' => 'metadata__api_class_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataApiClassXlats()
    {
        return $this->hasMany(\app\models\MetadataApiClassXlat::className(),
            ['metadata__api_class_id_1' => 'metadata__api_class_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataApiClassQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataApiClassQuery(get_called_class());
    }
}
