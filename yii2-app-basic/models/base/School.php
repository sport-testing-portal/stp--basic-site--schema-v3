<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "school".
 *
 * @property integer $school_id
 * @property integer $org_id
 * @property integer $religious_affiliation_id
 * @property integer $conference_id__female
 * @property integer $conference_id__male
 * @property integer $conference_id__main
 * @property integer $school_ipeds_id
 * @property integer $school_unit_id
 * @property integer $school_ope_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\OrgSchool[] $orgSchools
 * @property \app\models\PlayerCollegeRecruit[] $playerCollegeRecruits
 * @property \app\models\PlayerSchool[] $playerSchools
 * @property \app\models\Org $org
 * @property \app\models\ReligiousAffiliation $religiousAffiliation
 * @property \app\models\SchoolAct[] $schoolActs
 * @property \app\models\SchoolCoach[] $schoolCoaches
 * @property \app\models\SchoolSat[] $schoolSats
 * @property \app\models\SchoolSport[] $schoolSports
 * @property \app\models\SchoolStat[] $schoolStats
 * @property \app\models\Team[] $teams
 */
class School extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'orgSchools',
            'playerCollegeRecruits',
            'playerSchools',
            'org',
            'religiousAffiliation',
            'schoolActs',
            'schoolCoaches',
            'schoolSats',
            'schoolSports',
            'schoolStats',
            'teams'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'religious_affiliation_id', 'conference_id__female', 'conference_id__male', 'conference_id__main', 'school_ipeds_id', 'school_unit_id', 'school_ope_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_id' => 'School ID',
            'org_id' => 'Org ID',
            'religious_affiliation_id' => 'Religious Affiliation ID',
            'conference_id__female' => 'Conference Id  Female',
            'conference_id__male' => 'Conference Id  Male',
            'conference_id__main' => 'Conference Id  Main',
            'school_ipeds_id' => 'School Ipeds ID',
            'school_unit_id' => 'School Unit ID',
            'school_ope_id' => 'School Ope ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgSchools()
    {
        return $this->hasMany(\app\models\OrgSchool::className(), ['school_id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerCollegeRecruits()
    {
        return $this->hasMany(\app\models\PlayerCollegeRecruit::className(), ['school_id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerSchools()
    {
        return $this->hasMany(\app\models\PlayerSchool::className(), ['school_id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(\app\models\Org::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReligiousAffiliation()
    {
        return $this->hasOne(\app\models\ReligiousAffiliation::className(), ['religious_affiliation_id' => 'religious_affiliation_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolActs()
    {
        return $this->hasMany(\app\models\SchoolAct::className(), ['school_id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolCoaches()
    {
        return $this->hasMany(\app\models\SchoolCoach::className(), ['school_id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolSats()
    {
        return $this->hasMany(\app\models\SchoolSat::className(), ['school_id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolSports()
    {
        return $this->hasMany(\app\models\SchoolSport::className(), ['school_id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolStats()
    {
        return $this->hasMany(\app\models\SchoolStat::className(), ['school_id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(\app\models\Team::className(), ['school_id' => 'school_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SchoolQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SchoolQuery(get_called_class());
    }
}
