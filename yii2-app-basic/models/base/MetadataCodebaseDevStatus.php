<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__codebase_dev_status".
 *
 * @property integer $dev_status_id
 * @property integer $codebase_id
 * @property integer $codebase_dev_id
 * @property string $dev_status
 * @property string $dev_status_tag
 * @property string $dev_status_bfr
 * @property string $dev_status_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataCodebase $codebase
 * @property \app\models\MetadataCodebaseDev $codebaseDev
 */
class MetadataCodebaseDevStatus extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'codebase',
            'codebaseDev'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_id', 'codebase_dev_id', 'dev_status'], 'required'],
            [['codebase_id', 'codebase_dev_id', 'created_by', 'updated_by'], 'integer'],
            [['dev_status_at', 'created_at', 'updated_at'], 'safe'],
            [['dev_status', 'dev_status_tag', 'dev_status_bfr'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__codebase_dev_status';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dev_status_id' => 'Dev Status ID',
            'codebase_id' => 'Codebase ID',
            'codebase_dev_id' => 'Codebase Dev ID',
            'dev_status' => 'Dev Status',
            'dev_status_tag' => 'Dev Status Tag',
            'dev_status_bfr' => 'Dev Status Bfr',
            'dev_status_at' => 'Dev Status At',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodebase()
    {
        return $this->hasOne(\app\models\MetadataCodebase::className(), ['codebase_id' => 'codebase_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodebaseDev()
    {
        return $this->hasOne(\app\models\MetadataCodebaseDev::className(), ['codebase_dev_id' => 'codebase_dev_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataCodebaseDevStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataCodebaseDevStatusQuery(get_called_class());
    }
}
