<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__codebase_mvc_view".
 *
 * @property integer $codebase_mvc_view_id
 * @property integer $codebase_id
 * @property string $view_function
 * @property string $view_file
 * @property string $view_params
 * @property string $view_url
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataCodebase $codebase
 */
class MetadataCodebaseMvcView extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'codebase'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_id'], 'required'],
            [['codebase_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['view_function', 'view_file', 'view_params', 'view_url'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__codebase_mvc_view';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codebase_mvc_view_id' => 'Codebase Mvc View ID',
            'codebase_id' => 'Codebase ID',
            'view_function' => 'View Function',
            'view_file' => 'View File',
            'view_params' => 'View Params',
            'view_url' => 'View Url',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodebase()
    {
        return $this->hasOne(\app\models\MetadataCodebase::className(), ['codebase_id' => 'codebase_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataCodebaseMvcViewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataCodebaseMvcViewQuery(get_called_class());
    }
}
