<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "camp".
 *
 * @property integer $camp_id
 * @property integer $org_id
 * @property string $camp
 * @property string $camp_specialty
 * @property integer $camp_cost_regular
 * @property integer $camp_cost_early_registration
 * @property string $camp_team_discounts_available_yn
 * @property string $camp_scholarships_available_yn
 * @property string $camp_session_desc
 * @property string $camp_website_url
 * @property string $camp_session_url
 * @property string $camp_registration_url
 * @property string $camp_twitter_url
 * @property string $camp_facebook_url
 * @property string $camp_scholarship_application_info
 * @property string $camp_scholarship_application_request
 * @property string $camp_organizer_description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Org $org
 * @property \app\models\CampCoach[] $campCoaches
 * @property \app\models\CampLocation[] $campLocations
 * @property \app\models\CampSession[] $campSessions
 * @property \app\models\CampSport[] $campSports
 * @property \app\models\PlayerCampLog[] $playerCampLogs
 * @property \app\models\Team[] $teams
 */
class Camp extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'org',
            'campCoaches',
            'campLocations',
            'campSessions',
            'campSports',
            'playerCampLogs',
            'teams'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'camp_cost_regular', 'camp_cost_early_registration', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['camp'], 'string', 'max' => 110],
            [['camp_specialty', 'camp_session_desc', 'camp_scholarship_application_info', 'camp_scholarship_application_request', 'camp_organizer_description'], 'string', 'max' => 45],
            [['camp_team_discounts_available_yn', 'camp_scholarships_available_yn', 'lock'], 'string', 'max' => 1],
            [['camp_website_url', 'camp_session_url', 'camp_registration_url', 'camp_twitter_url', 'camp_facebook_url'], 'string', 'max' => 175],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'camp';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'camp_id' => 'Camp ID',
            'org_id' => 'Org ID',
            'camp' => 'Camp',
            'camp_specialty' => 'Camp Specialty',
            'camp_cost_regular' => 'Camp Cost Regular',
            'camp_cost_early_registration' => 'Camp Cost Early Registration',
            'camp_team_discounts_available_yn' => 'Camp Team Discounts Available Yn',
            'camp_scholarships_available_yn' => 'Camp Scholarships Available Yn',
            'camp_session_desc' => 'Camp Session Desc',
            'camp_website_url' => 'Camp Website Url',
            'camp_session_url' => 'Camp Session Url',
            'camp_registration_url' => 'Camp Registration Url',
            'camp_twitter_url' => 'Camp Twitter Url',
            'camp_facebook_url' => 'Camp Facebook Url',
            'camp_scholarship_application_info' => 'Camp Scholarship Application Info',
            'camp_scholarship_application_request' => 'Camp Scholarship Application Request',
            'camp_organizer_description' => 'Camp Organizer Description',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(\app\models\Org::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampCoaches()
    {
        return $this->hasMany(\app\models\CampCoach::className(), ['camp_id' => 'camp_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampLocations()
    {
        return $this->hasMany(\app\models\CampLocation::className(), ['camp_id' => 'camp_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessions()
    {
        return $this->hasMany(\app\models\CampSession::className(), ['camp_id' => 'camp_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSports()
    {
        return $this->hasMany(\app\models\CampSport::className(), ['camp_id' => 'camp_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerCampLogs()
    {
        return $this->hasMany(\app\models\PlayerCampLog::className(), ['camp_id' => 'camp_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(\app\models\Team::className(), ['camp_id' => 'camp_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\CampQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CampQuery(get_called_class());
    }
}
