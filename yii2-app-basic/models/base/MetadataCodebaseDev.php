<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__codebase_dev".
 *
 * @property integer $codebase_dev_id
 * @property string $codebase_dev
 * @property string $codebase_dev_desc_short
 * @property string $codebase_dev_desc_long
 * @property string $dev_status_tag
 * @property string $dev_scope
 * @property string $rule_file_uri
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataCodebaseDevStatus[] $metadataCodebaseDevStatuses
 */
class MetadataCodebaseDev extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataCodebaseDevStatuses'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_dev'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['codebase_dev'], 'string', 'max' => 75],
            [['codebase_dev_desc_short', 'dev_status_tag'], 'string', 'max' => 45],
            [['codebase_dev_desc_long'], 'string', 'max' => 175],
            [['dev_scope'], 'string', 'max' => 253],
            [['rule_file_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__codebase_dev';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codebase_dev_id' => 'Codebase Dev ID',
            'codebase_dev' => 'Codebase Dev',
            'codebase_dev_desc_short' => 'Codebase Dev Desc Short',
            'codebase_dev_desc_long' => 'Codebase Dev Desc Long',
            'dev_status_tag' => 'Dev Status Tag',
            'dev_scope' => 'Dev Scope',
            'rule_file_uri' => 'Rule File Uri',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataCodebaseDevStatuses()
    {
        return $this->hasMany(\app\models\MetadataCodebaseDevStatus::className(), ['codebase_dev_id' => 'codebase_dev_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataCodebaseDevQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataCodebaseDevQuery(get_called_class());
    }
}
