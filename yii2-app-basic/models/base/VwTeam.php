<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwTeam".
 *
 * @property string $org_name
 * @property string $team_name
 * @property string $team_age_group
 * @property string $team_division
 * @property string $organizational_level
 * @property string $gender_code
 * @property string $gender_desc
 * @property string $coach_name
 * @property string $player_name
 * @property integer $org_id
 * @property integer $team_id
 * @property integer $coach_id
 * @property integer $player_id
 * @property integer $coach__person_id
 * @property integer $player__person_id
 * @property string $team_created_by_username
 * @property string $team_updated_by_username
 * @property string $created_at
 * @property string $updated_at
 */
class VwTeam extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_name'], 'required'],
            [['org_id', 'team_id', 'coach_id', 'player_id', 'coach__person_id', 'player__person_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_name'], 'string', 'max' => 100],
            [['team_name'], 'string', 'max' => 75],
            [['team_age_group', 'organizational_level'], 'string', 'max' => 45],
            [['team_division'], 'string', 'max' => 10],
            [['gender_code'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['coach_name', 'player_name'], 'string', 'max' => 90],
            [['team_created_by_username', 'team_updated_by_username'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwTeam';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'org_name' => 'Org Name',
            'team_name' => 'Team Name',
            'team_age_group' => 'Team Age Group',
            'team_division' => 'Team Division',
            'organizational_level' => 'Organizational Level',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'coach_name' => 'Coach Name',
            'player_name' => 'Player Name',
            'org_id' => 'Org ID',
            'team_id' => 'Team ID',
            'coach_id' => 'Coach ID',
            'player_id' => 'Player ID',
            'coach__person_id' => 'Coach  Person ID',
            'player__person_id' => 'Player  Person ID',
            'team_created_by_username' => 'Team Created By Username',
            'team_updated_by_username' => 'Team Updated By Username',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwTeamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwTeamQuery(get_called_class());
    }
}
