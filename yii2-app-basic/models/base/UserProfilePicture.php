<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "user_profile_picture".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $original_picture_id
 * @property string $filename
 * @property integer $width
 * @property integer $height
 * @property string $mimetype
 * @property string $created_on
 * @property string $contents
 *
 * @property \app\models\UserProfilePicture $originalPicture
 * @property \app\models\UserProfilePicture[] $userProfilePictures
 * @property \app\models\User $user
 */
class UserProfilePicture extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'originalPicture',
            'userProfilePictures',
            'user'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'filename', 'width', 'height', 'mimetype', 'contents'], 'required'],
            [['user_id', 'original_picture_id', 'width', 'height'], 'integer'],
            [['created_on'], 'safe'],
            [['contents'], 'string'],
            [['filename', 'mimetype'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_profile_picture';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'original_picture_id' => 'Original Picture ID',
            'filename' => 'Filename',
            'width' => 'Width',
            'height' => 'Height',
            'mimetype' => 'Mimetype',
            'created_on' => 'Created On',
            'contents' => 'Contents',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOriginalPicture()
    {
        return $this->hasOne(\app\models\UserProfilePicture::className(), ['id' => 'original_picture_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfilePictures()
    {
        return $this->hasMany(\app\models\UserProfilePicture::className(), ['original_picture_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\UserProfilePictureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\UserProfilePictureQuery(get_called_class());
    }
}
