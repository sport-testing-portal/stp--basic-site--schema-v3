<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__api_provider".
 *
 * @property integer $metadata__api_provider_id
 * @property string $metadata__api_provider
 * @property string $metadata__api_provider_desc
 * @property string $metadata__api_provider_url
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataApiClass[] $metadataApiClasses
 */
class MetadataApiProvider extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataApiClasses'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['metadata__api_provider'], 'string', 'max' => 75],
            [['metadata__api_provider_desc'], 'string', 'max' => 150],
            [['metadata__api_provider_url'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__api_provider';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'metadata__api_provider_id' => 'API Provider ID',
            'metadata__api_provider' => 'API Provider',
            'metadata__api_provider_desc' => 'API Provider Desc',
            'metadata__api_provider_url' => 'API Provider URL',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataApiClasses()
    {
        return $this->hasMany(\app\models\MetadataApiClass::className(), ['metadata__api_provider_id' => 'metadata__api_provider_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataApiProviderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataApiProviderQuery(get_called_class());
    }
}
