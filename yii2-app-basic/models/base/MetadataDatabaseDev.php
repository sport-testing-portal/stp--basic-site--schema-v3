<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__database_dev".
 *
 * @property integer $database_dev_id
 * @property string $database_dev
 * @property string $database_dev_desc_short
 * @property string $database_dev_desc_long
 * @property string $dev_status_tag
 * @property string $dev_scope
 * @property string $rule_file_uri
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataDatabaseDevStatus[] $metadataDatabaseDevStatuses
 */
class MetadataDatabaseDev extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataDatabaseDevStatuses'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['database_dev'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['database_dev'], 'string', 'max' => 75],
            [['database_dev_desc_short', 'dev_status_tag'], 'string', 'max' => 45],
            [['database_dev_desc_long'], 'string', 'max' => 175],
            [['dev_scope'], 'string', 'max' => 253],
            [['rule_file_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__database_dev';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'database_dev_id' => 'Database Dev ID',
            'database_dev' => 'Database Dev',
            'database_dev_desc_short' => 'Database Dev Desc Short',
            'database_dev_desc_long' => 'Database Dev Desc Long',
            'dev_status_tag' => 'Dev Status Tag',
            'dev_scope' => 'Dev Scope',
            'rule_file_uri' => 'Rule File Uri',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataDatabaseDevStatuses()
    {
        return $this->hasMany(\app\models\MetadataDatabaseDevStatus::className(), ['database_dev_id' => 'database_dev_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataDatabaseDevQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataDatabaseDevQuery(get_called_class());
    }
}
