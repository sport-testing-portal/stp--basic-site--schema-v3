<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "player_results_search_log".
 *
 * @property integer $player_results_search_log_id
 * @property integer $user_id
 * @property integer $person_id
 * @property integer $row_count
 * @property string $gender
 * @property integer $age_range_min
 * @property integer $age_range_max
 * @property string $first_name
 * @property string $last_name
 * @property string $test_provider
 * @property string $team
 * @property string $position
 * @property string $test_type
 * @property string $test_category
 * @property string $test_description
 * @property string $country
 * @property string $state_province
 * @property string $zip_code
 * @property string $zip_code_radius
 * @property string $city
 * @property string $city_radius
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\User $user
 * @property \app\models\Person $person
 */
class PlayerResultsSearchLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'user',
            'person'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'person_id', 'row_count', 'age_range_min', 'age_range_max', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['gender', 'first_name', 'last_name', 'test_provider', 'team', 'position', 'test_type', 'test_category', 'test_description', 'country', 'state_province', 'zip_code', 'zip_code_radius', 'city', 'city_radius'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_results_search_log';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'player_results_search_log_id' => 'Player Results Search Log ID',
            'user_id' => 'User ID',
            'person_id' => 'Person ID',
            'row_count' => 'Row Count',
            'gender' => 'Gender',
            'age_range_min' => 'Age Range Min',
            'age_range_max' => 'Age Range Max',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'test_provider' => 'Test Provider',
            'team' => 'Team',
            'position' => 'Position',
            'test_type' => 'Test Type',
            'test_category' => 'Test Category',
            'test_description' => 'Test Description',
            'country' => 'Country',
            'state_province' => 'State Province',
            'zip_code' => 'Zip Code',
            'zip_code_radius' => 'Zip Code Radius',
            'city' => 'City',
            'city_radius' => 'City Radius',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PlayerResultsSearchLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PlayerResultsSearchLogQuery(get_called_class());
    }
}
