<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwDataHdrXlation".
 *
 * @property integer $dhx_id
 * @property integer $dhxi_id
 * @property string $source_entity
 * @property string $source_value
 * @property string $target_value
 * @property integer $col_ord
 * @property string $target_table
 * @property string $type_name_source
 * @property integer $type_name
 */
class VwDataHdrXlation extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dhx_id', 'dhxi_id', 'col_ord', 'type_name'], 'integer'],
            [['source_entity', 'source_value', 'target_value'], 'string', 'max' => 75],
            [['target_table', 'type_name_source'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwDataHdrXlation';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dhx_id' => 'Dhx ID',
            'dhxi_id' => 'Dhxi ID',
            'source_entity' => 'Source Entity',
            'source_value' => 'Source Value',
            'target_value' => 'Target Value',
            'col_ord' => 'Col Ord',
            'target_table' => 'Target Table',
            'type_name_source' => 'Type Name Source',
            'type_name' => 'Type Name',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwDataHdrXlationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwDataHdrXlationQuery(get_called_class());
    }
}
