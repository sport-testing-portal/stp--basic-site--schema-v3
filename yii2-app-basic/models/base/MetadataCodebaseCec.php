<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__codebase_cec".
 *
 * @property integer $codebase_cec_id
 * @property string $codebase_cec
 * @property string $codebase_cec_desc_short
 * @property string $codebase_cec_desc_long
 * @property string $cec_status_tag
 * @property string $cec_scope
 * @property string $rule_file_uri
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataCodebaseCecStatus[] $metadataCodebaseCecStatuses
 */
class MetadataCodebaseCec extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataCodebaseCecStatuses'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_cec'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['codebase_cec'], 'string', 'max' => 75],
            [['codebase_cec_desc_short', 'cec_status_tag'], 'string', 'max' => 45],
            [['codebase_cec_desc_long'], 'string', 'max' => 175],
            [['cec_scope'], 'string', 'max' => 253],
            [['rule_file_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__codebase_cec';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codebase_cec_id' => 'CEC ID',
            'codebase_cec' => 'Codebase CEC',
            'codebase_cec_desc_short' => 'CEC Desc Short',
            'codebase_cec_desc_long' => 'CEC Desc Long',
            'cec_status_tag' => 'CEC Status Tag',
            'cec_scope' => 'CEC Scope',
            'rule_file_uri' => 'Rule File Uri',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataCodebaseCecStatuses()
    {
        return $this->hasMany(\app\models\MetadataCodebaseCecStatus::className(), ['codebase_cec_id' => 'codebase_cec_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataCodebaseCecQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataCodebaseCecQuery(get_called_class());
    }
}
