<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "school_sat".
 *
 * @property integer $school_sat_id
 * @property integer $school_id
 * @property integer $school_unit_id
 * @property integer $school_sat_i_verbal_25_pct
 * @property integer $school_sat_i_verbal_75_pct
 * @property integer $school_sat_i_math_25_pct
 * @property integer $school_sat_i_math_75_pct
 * @property integer $school_sat_student_submit_cnt
 * @property string $school_sat_student_submit_pct
 * @property string $school_sat_report_period
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\School $school
 */
class SchoolSat extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'school'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'school_unit_id', 'school_sat_i_verbal_25_pct', 'school_sat_i_verbal_75_pct', 'school_sat_i_math_25_pct', 'school_sat_i_math_75_pct', 'school_sat_student_submit_cnt', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['school_sat_student_submit_pct'], 'string', 'max' => 5],
            [['school_sat_report_period'], 'string', 'max' => 12],
            [['lock'], 'string', 'max' => 1],
            [['school_unit_id'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_sat';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_sat_id' => 'School Sat ID',
            'school_id' => 'School ID',
            'school_unit_id' => 'School Unit ID',
            'school_sat_i_verbal_25_pct' => 'School Sat I Verbal 25 Pct',
            'school_sat_i_verbal_75_pct' => 'School Sat I Verbal 75 Pct',
            'school_sat_i_math_25_pct' => 'School Sat I Math 25 Pct',
            'school_sat_i_math_75_pct' => 'School Sat I Math 75 Pct',
            'school_sat_student_submit_cnt' => 'School Sat Student Submit Cnt',
            'school_sat_student_submit_pct' => 'School Sat Student Submit Pct',
            'school_sat_report_period' => 'School Sat Report Period',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\app\models\School::className(), ['school_id' => 'school_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SchoolSatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SchoolSatQuery(get_called_class());
    }
}
