<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "import_file".
 *
 * @property integer $import_file_id
 * @property string $import_file_source_file_name
 * @property string $import_file_source_file_date
 * @property integer $import_file_source_file_size
 * @property string $import_file_source_file_sha256
 * @property string $import_file_source_file_password
 * @property string $import_file_target_file_name
 * @property string $import_file_target_file_date
 * @property integer $import_file_target_file_size
 * @property string $import_file_target_file_sha256
 * @property string $import_file_target_file_password
 * @property string $import_file_description_short
 * @property string $import_file_description_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\ImportFileStiCoachReport[] $importFileStiCoachReports
 * @property \app\models\ImportFileStiSpecialReport[] $importFileStiSpecialReports
 */
class ImportFile extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'importFileStiCoachReports',
            'importFileStiSpecialReports'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['import_file_source_file_date', 'import_file_target_file_date', 'created_at', 'updated_at'], 'safe'],
            [['import_file_source_file_size', 'import_file_target_file_size', 'created_by', 'updated_by'], 'integer'],
            [['import_file_source_file_name', 'import_file_target_file_name'], 'string', 'max' => 150],
            [['import_file_source_file_sha256', 'import_file_target_file_sha256'], 'string', 'max' => 64],
            [['import_file_source_file_password', 'import_file_target_file_password'], 'string', 'max' => 256],
            [['import_file_description_short'], 'string', 'max' => 45],
            [['import_file_description_long'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'import_file';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'import_file_id' => 'Import File ID',
            'import_file_source_file_name' => 'Import File Source File Name',
            'import_file_source_file_date' => 'Import File Source File Date',
            'import_file_source_file_size' => 'Import File Source File Size',
            'import_file_source_file_sha256' => 'Import File Source File Sha256',
            'import_file_source_file_password' => 'Import File Source File Password',
            'import_file_target_file_name' => 'Import File Target File Name',
            'import_file_target_file_date' => 'Import File Target File Date',
            'import_file_target_file_size' => 'Import File Target File Size',
            'import_file_target_file_sha256' => 'Import File Target File Sha256',
            'import_file_target_file_password' => 'Import File Target File Password',
            'import_file_description_short' => 'Import File Description Short',
            'import_file_description_long' => 'Import File Description Long',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportFileStiCoachReports()
    {
        return $this->hasMany(\app\models\ImportFileStiCoachReport::className(), ['import_file_id' => 'import_file_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportFileStiSpecialReports()
    {
        return $this->hasMany(\app\models\ImportFileStiSpecialReport::className(), ['import_file_id' => 'import_file_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\ImportFileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ImportFileQuery(get_called_class());
    }
}
