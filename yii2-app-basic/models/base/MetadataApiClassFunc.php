<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__api_class_func".
 *
 * @property integer $metadata__api_class_func_id
 * @property integer $metadata__api_class_id
 * @property string $metadata__api_class_func
 * @property string $metadata__api_class_func_desc
 * @property string $metadata__api_class_func_docs_url
 * @property string $metadata__api_class_func_reference_example
 * @property string $metadata__api_class_func_regex_find
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataApiClass $metadataApiClass
 * @since 0.8.0
 * @internal changelog attribute labels and attribute hints edited
 */
class MetadataApiClassFunc extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataApiClass'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metadata__api_class_id'], 'required'],
            [['metadata__api_class_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['metadata__api_class_func', 'metadata__api_class_func_desc', 'metadata__api_class_func_docs_url', 'metadata__api_class_func_reference_example', 'metadata__api_class_func_regex_find'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__api_class_func';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'metadata__api_class_func_id' => 'API Class Func ID',
            'metadata__api_class_id' => 'API Class ID',
            'metadata__api_class_func' => 'API Class Func',
            'metadata__api_class_func_desc' => 'API Class Func Desc',
            'metadata__api_class_func_docs_url' => 'API Class Func Docs Url',
            'metadata__api_class_func_reference_example' => 'API Class Func Reference Example',
            'metadata__api_class_func_regex_find' => 'API Class Func Regex Find',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataApiClass()
    {
        return $this->hasOne(\app\models\MetadataApiClass::className(),
            ['metadata__api_class_id' => 'metadata__api_class_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataApiClassFuncQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataApiClassFuncQuery(get_called_class());
    }
}
