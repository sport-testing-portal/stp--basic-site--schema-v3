<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "app_parm".
 *
 * @property integer $app_parm_id
 * @property string $app_parm_type
 * @property integer $ap_int
 * @property string $ap_varchar
 * @property string $ap_datetime
 * @property string $ap_comment
 * @property integer $ap_display_order
 * @property string $updated_at
 * @property string $created_at
 * @property integer $updated_by
 * @property integer $lock
 * @property integer $created_by
 */
class AppParm extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ap_int', 'ap_display_order', 'updated_by', 'created_by'], 'integer'],
            [['ap_datetime', 'updated_at', 'created_at'], 'safe'],
            [['app_parm_type'], 'string', 'max' => 100],
            [['ap_varchar', 'ap_comment'], 'string', 'max' => 1024],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_parm';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'app_parm_id' => 'App Parm ID',
            'app_parm_type' => 'App Parm Type',
            'ap_int' => 'Ap Int',
            'ap_varchar' => 'Ap Varchar',
            'ap_datetime' => 'Ap Datetime',
            'ap_comment' => 'Ap Comment',
            'ap_display_order' => 'Ap Display Order',
            'lock' => 'Lock',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\AppParmQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AppParmQuery(get_called_class());
    }
}
