<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "person".
 *
 * @property integer $person_id
 * @property integer $org_id
 * @property integer $app_user_id
 * @property integer $person_type_id
 * @property integer $user_id
 * @property string $person_name_prefix
 * @property string $person_name_first
 * @property string $person_name_middle
 * @property string $person_name_last
 * @property string $person_name_suffix
 * @property string $person
 * @property string $person_phone_personal
 * @property string $person_email_personal
 * @property string $person_phone_work
 * @property string $person_email_work
 * @property string $person_position_work
 * @property integer $gender_id
 * @property string $person_image_headshot_url
 * @property string $person_name_nickname
 * @property string $person_date_of_birth
 * @property string $person_height
 * @property integer $person_weight
 * @property string $person_tshirt_size
 * @property integer $person_high_school__graduation_year
 * @property integer $person_college_graduation_year
 * @property string $person_college_commitment_status
 * @property string $person_addr_1
 * @property string $person_addr_2
 * @property string $person_addr_3
 * @property string $person_city
 * @property string $person_postal_code
 * @property string $person_country
 * @property string $person_country_code
 * @property string $person_state_or_region
 * @property string $org_affiliation_begin_dt
 * @property string $org_affiliation_end_dt
 * @property string $person_website
 * @property string $person_profile_url
 * @property string $person_profile_uri
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Address[] $addresses
 * @property \app\models\Coach[] $coaches
 * @property \app\models\EventAttendeeLog[] $eventAttendeeLogs
 * @property \app\models\Media[] $media
 * @property \app\models\Note[] $notes
 * @property \app\models\PaymentLog[] $paymentLogs
 * @property \app\models\Country $personCountryCode
 * @property \app\models\Gender $gender
 * @property \app\models\Org $org
 * @property \app\models\PersonType $personType
 * @property \app\models\PersonAddress[] $personAddresses
 * @property \app\models\PersonCertification[] $personCertifications
 * @property \app\models\Player[] $players
 * @property \app\models\PlayerContact[] $playerContacts
 * @property \app\models\PlayerResultsSearchLog[] $playerResultsSearchLogs
 * @property \app\models\SearchCriteriaTemplate[] $searchCriteriaTemplates
 * @property \app\models\SearchHistory[] $searchHistories
 * @property \app\models\Subscription[] $subscriptions
 * @property \app\models\TestEvalSummaryLog[] $testEvalSummaryLogs
 */
class Person extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'addresses',
            'coaches',
            'eventAttendeeLogs',
            'media',
            'notes',
            'paymentLogs',
            'personCountryCode',
            'gender',
            'org',
            'personType',
            'personAddresses',
            'personCertifications',
            'players',
            'playerContacts',
            'playerResultsSearchLogs',
            'searchCriteriaTemplates',
            'searchHistories',
            'subscriptions',
            'testEvalSummaryLogs'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'app_user_id', 'person_type_id', 'user_id', 'gender_id', 'person_weight', 'person_high_school__graduation_year', 'person_college_graduation_year', 'created_by', 'updated_by'], 'integer'],
            [['person_date_of_birth', 'org_affiliation_begin_dt', 'org_affiliation_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['person_name_prefix', 'person_college_commitment_status', 'person_postal_code'], 'string', 'max' => 25],
            [['person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_country', 'person_state_or_region'], 'string', 'max' => 45],
            [['person', 'person_profile_url'], 'string', 'max' => 90],
            [['person_email_personal', 'person_email_work', 'person_profile_uri'], 'string', 'max' => 60],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3'], 'string', 'max' => 100],
            [['person_height'], 'string', 'max' => 5],
            [['person_tshirt_size'], 'string', 'max' => 10],
            [['person_city'], 'string', 'max' => 75],
            [['person_country_code'], 'string', 'max' => 3],
            [['person_website'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['user_id'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'person';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_id' => 'Person ID',
            'org_id' => 'Org ID',
            'app_user_id' => 'App User ID',
            'person_type_id' => 'Person Type ID',
            'user_id' => 'User ID',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person' => 'Person',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'gender_id' => 'Gender ID',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_height' => 'Person Height',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_high_school__graduation_year' => 'Person High School  Graduation Year',
            'person_college_graduation_year' => 'Person College Graduation Year',
            'person_college_commitment_status' => 'Person College Commitment Status',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'org_affiliation_begin_dt' => 'Org Affiliation Begin Dt',
            'org_affiliation_end_dt' => 'Org Affiliation End Dt',
            'person_website' => 'Person Website',
            'person_profile_url' => 'Person Profile Url',
            'person_profile_uri' => 'Person Profile Uri',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(\app\models\Address::className(), ['address_id' => 'address_id'])->viaTable('person_address', ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoaches()
    {
        return $this->hasMany(\app\models\Coach::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventAttendeeLogs()
    {
        return $this->hasMany(\app\models\EventAttendeeLog::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasMany(\app\models\Media::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotes()
    {
        return $this->hasMany(\app\models\Note::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentLogs()
    {
        return $this->hasMany(\app\models\PaymentLog::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonCountryCode()
    {
        return $this->hasOne(\app\models\Country::className(), ['iso3' => 'person_country_code']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGender()
    {
        return $this->hasOne(\app\models\Gender::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(\app\models\Org::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonType()
    {
        return $this->hasOne(\app\models\PersonType::className(), ['person_type_id' => 'person_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonAddresses()
    {
        return $this->hasMany(\app\models\PersonAddress::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonCertifications()
    {
        return $this->hasMany(\app\models\PersonCertification::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(\app\models\Player::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerContacts()
    {
        return $this->hasMany(\app\models\PlayerContact::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerResultsSearchLogs()
    {
        return $this->hasMany(\app\models\PlayerResultsSearchLog::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchCriteriaTemplates()
    {
        return $this->hasMany(\app\models\SearchCriteriaTemplate::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchHistories()
    {
        return $this->hasMany(\app\models\SearchHistory::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(\app\models\Subscription::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalSummaryLogs()
    {
        return $this->hasMany(\app\models\TestEvalSummaryLog::className(), ['person_id' => 'person_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PersonQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PersonQuery(get_called_class());
    }
}
