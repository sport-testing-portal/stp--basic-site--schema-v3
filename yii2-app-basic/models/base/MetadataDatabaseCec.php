<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__database_cec".
 *
 * @property integer $database_cec_id
 * @property string $database_cec
 * @property string $database_cec_desc_short
 * @property string $database_cec_desc_long
 * @property string $cec_status_tag
 * @property string $cec_scope
 * @property string $rule_file_uri
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataDatabaseCecStatus[] $metadataDatabaseCecStatuses
 */
class MetadataDatabaseCec extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataDatabaseCecStatuses'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['database_cec'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['database_cec'], 'string', 'max' => 75],
            [['database_cec_desc_short', 'cec_status_tag'], 'string', 'max' => 45],
            [['database_cec_desc_long'], 'string', 'max' => 175],
            [['cec_scope'], 'string', 'max' => 253],
            [['rule_file_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__database_cec';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'database_cec_id' => 'Database Cec ID',
            'database_cec' => 'Database Cec',
            'database_cec_desc_short' => 'Database Cec Desc Short',
            'database_cec_desc_long' => 'Database Cec Desc Long',
            'cec_status_tag' => 'Cec Status Tag',
            'cec_scope' => 'Cec Scope',
            'rule_file_uri' => 'Rule File Uri',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataDatabaseCecStatuses()
    {
        return $this->hasMany(\app\models\MetadataDatabaseCecStatus::className(), ['database_cec_id' => 'database_cec_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataDatabaseCecQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataDatabaseCecQuery(get_called_class());
    }
}
