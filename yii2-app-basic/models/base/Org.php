<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "org".
 *
 * @property integer $org_id
 * @property integer $org_type_id
 * @property integer $org_level_id
 * @property string $org
 * @property string $org_governing_body
 * @property string $org_ncaa_clearing_house_id
 * @property string $org_website_url
 * @property string $org_twitter_url
 * @property string $org_facebook_url
 * @property string $org_phone_main
 * @property string $org_email_main
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Address[] $addresses
 * @property \app\models\Camp[] $camps
 * @property \app\models\Media[] $media
 * @property \app\models\Note[] $notes
 * @property \app\models\OrgLevel $orgLevel
 * @property \app\models\OrgType $orgType
 * @property \app\models\OrgGoverningBody[] $orgGoverningBodies
 * @property \app\models\OrgSchool[] $orgSchools
 * @property \app\models\PaymentLog[] $paymentLogs
 * @property \app\models\Person[] $people
 * @property \app\models\School[] $schools
 * @property \app\models\Subscription[] $subscriptions
 * @property \app\models\Team[] $teams
 */
class Org extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'addresses',
            'camps',
            'media',
            'notes',
            'orgLevel',
            'orgType',
            'orgGoverningBodies',
            'orgSchools',
            'paymentLogs',
            'people',
            'schools',
            'subscriptions',
            'teams'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_type_id', 'org_level_id', 'created_by', 'updated_by'], 'integer'],
            [['org'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['org'], 'string', 'max' => 100],
            [['org_governing_body'], 'string', 'max' => 45],
            [['org_ncaa_clearing_house_id', 'org_phone_main'], 'string', 'max' => 25],
            [['org_website_url', 'org_twitter_url', 'org_facebook_url'], 'string', 'max' => 150],
            [['org_email_main'], 'string', 'max' => 75],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'org';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'org_level_id' => 'Org Level ID',
            'org' => 'Org',
            'org_governing_body' => 'Org Governing Body',
            'org_ncaa_clearing_house_id' => 'Org Ncaa Clearing House ID',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(\app\models\Address::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCamps()
    {
        return $this->hasMany(\app\models\Camp::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasMany(\app\models\Media::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotes()
    {
        return $this->hasMany(\app\models\Note::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgLevel()
    {
        return $this->hasOne(\app\models\OrgLevel::className(), ['org_level_id' => 'org_level_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgType()
    {
        return $this->hasOne(\app\models\OrgType::className(), ['org_type_id' => 'org_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgGoverningBodies()
    {
        return $this->hasMany(\app\models\OrgGoverningBody::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgSchools()
    {
        return $this->hasMany(\app\models\OrgSchool::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentLogs()
    {
        return $this->hasMany(\app\models\PaymentLog::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(\app\models\Person::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchools()
    {
        return $this->hasMany(\app\models\School::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(\app\models\Subscription::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(\app\models\Team::className(), ['org_id' => 'org_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\OrgQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OrgQuery(get_called_class());
    }
}
