<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__database_cec_status".
 *
 * @property integer $cec_status_id
 * @property integer $database_id
 * @property integer $database_cec_id
 * @property string $cec_status
 * @property string $cec_status_tag
 * @property string $cec_status_bfr
 * @property string $cec_status_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\MetadataDatabase $database
 * @property \app\models\MetadataDatabaseCec $databaseCec
 */
class MetadataDatabaseCecStatus extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'database',
            'databaseCec'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['database_id', 'database_cec_id', 'cec_status'], 'required'],
            [['database_id', 'database_cec_id', 'created_by', 'updated_by'], 'integer'],
            [['cec_status_at', 'created_at', 'updated_at'], 'safe'],
            [['cec_status', 'cec_status_tag', 'cec_status_bfr'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__database_cec_status';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cec_status_id' => 'Cec Status ID',
            'database_id' => 'Database ID',
            'database_cec_id' => 'Database Cec ID',
            'cec_status' => 'Cec Status',
            'cec_status_tag' => 'Cec Status Tag',
            'cec_status_bfr' => 'Cec Status Bfr',
            'cec_status_at' => 'Cec Status At',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatabase()
    {
        return $this->hasOne(\app\models\MetadataDatabase::className(), ['database_id' => 'database_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatabaseCec()
    {
        return $this->hasOne(\app\models\MetadataDatabaseCec::className(), ['database_cec_id' => 'database_cec_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataDatabaseCecStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataDatabaseCecStatusQuery(get_called_class());
    }
}
