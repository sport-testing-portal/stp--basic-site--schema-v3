<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "app_user_registration".
 *
 * @property integer $app_user_registration_id
 * @property integer $person_id
 * @property integer $org_id
 * @property integer $app_user_id
 * @property integer $person_type_id
 * @property integer $user_id
 * @property string $person_name_prefix
 * @property string $person_name_first
 * @property string $person_name_middle
 * @property string $person_name_last
 * @property string $person_name_suffix
 * @property string $person_name_full
 * @property string $person_phone_personal
 * @property string $person_email_personal
 * @property string $person_phone_work
 * @property string $person_email_work
 * @property string $person_position_work
 * @property integer $gender_id
 * @property string $person_image_headshot_url
 * @property string $person_name_nickname
 * @property string $person_date_of_birth
 * @property string $person_height
 * @property integer $person_weight
 * @property string $person_tshirt_size
 * @property string $person_addr_1
 * @property string $person_addr_2
 * @property string $person_addr_3
 * @property string $person_city
 * @property string $person_postal_code
 * @property string $person_country
 * @property string $person_country_code
 * @property string $person_state_or_region
 * @property integer $player_id
 * @property string $player_access_code
 * @property string $player_waiver_minor_dt
 * @property string $player_waiver_adult_dt
 * @property string $player_parent_email
 * @property string $player_sport_preference
 * @property string $player_sport_position_preference
 * @property string $player_shot_side_preference
 * @property integer $coach_id
 * @property integer $coach_type_id
 * @property string $coach_specialty
 * @property string $coach_certifications
 * @property string $coach_comments
 * @property string $coach_qrcode_uri
 * @property string $coach_info_source_scrape_url
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 */
class AppUserRegistration extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'player_id', 'coach_id'], 'required'],
            [['person_id', 'org_id', 'app_user_id', 'person_type_id', 'user_id', 'gender_id', 'person_weight', 'player_id', 'coach_id', 'coach_type_id', 'created_by', 'updated_by'], 'integer'],
            [['person_date_of_birth', 'player_waiver_minor_dt', 'player_waiver_adult_dt', 'created_at', 'updated_at'], 'safe'],
            [['person_name_prefix', 'person_postal_code', 'player_shot_side_preference'], 'string', 'max' => 25],
            [['person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_country', 'person_state_or_region', 'player_access_code', 'player_sport_position_preference'], 'string', 'max' => 45],
            [['person_name_full'], 'string', 'max' => 90],
            [['person_email_personal', 'person_email_work', 'player_parent_email'], 'string', 'max' => 60],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'coach_info_source_scrape_url'], 'string', 'max' => 100],
            [['person_height', 'person_country_code'], 'string', 'max' => 5],
            [['person_tshirt_size'], 'string', 'max' => 10],
            [['person_city'], 'string', 'max' => 75],
            [['player_sport_preference'], 'string', 'max' => 35],
            [['coach_specialty'], 'string', 'max' => 95],
            [['coach_certifications', 'coach_qrcode_uri'], 'string', 'max' => 150],
            [['coach_comments'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_user_registration';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'app_user_registration_id' => 'App User Registration ID',
            'person_id' => 'Person ID',
            'org_id' => 'Org ID',
            'app_user_id' => 'App User ID',
            'person_type_id' => 'Person Type ID',
            'user_id' => 'User ID',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_name_full' => 'Person Name Full',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'gender_id' => 'Gender ID',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_height' => 'Person Height',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'player_id' => 'Player ID',
            'player_access_code' => 'Player Access Code',
            'player_waiver_minor_dt' => 'Player Waiver Minor Dt',
            'player_waiver_adult_dt' => 'Player Waiver Adult Dt',
            'player_parent_email' => 'Player Parent Email',
            'player_sport_preference' => 'Player Sport Preference',
            'player_sport_position_preference' => 'Player Sport Position Preference',
            'player_shot_side_preference' => 'Player Shot Side Preference',
            'coach_id' => 'Coach ID',
            'coach_type_id' => 'Coach Type ID',
            'coach_specialty' => 'Coach Specialty',
            'coach_certifications' => 'Coach Certifications',
            'coach_comments' => 'Coach Comments',
            'coach_qrcode_uri' => 'Coach Qrcode Uri',
            'coach_info_source_scrape_url' => 'Coach Info Source Scrape Url',
            'lock' => 'Lock',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\AppUserRegistrationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AppUserRegistrationQuery(get_called_class());
    }
}
