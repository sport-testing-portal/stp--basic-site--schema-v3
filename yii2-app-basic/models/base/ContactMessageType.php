<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "contact_message_type".
 *
 * @property integer $contact_message_type_id
 * @property string $contact_message_type
 * @property string $contact_message_type_short_desc
 * @property string $contact_message_type_long_desc
 */
class ContactMessageType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_message_type'], 'required'],
            [['contact_message_type', 'contact_message_type_short_desc', 'contact_message_type_long_desc'], 'string', 'max' => 45],
            [['contact_message_type'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_message_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'contact_message_type_id' => 'Contact Message Type ID',
            'contact_message_type' => 'Contact Message Type',
            'contact_message_type_short_desc' => 'Contact Message Type Short Desc',
            'contact_message_type_long_desc' => 'Contact Message Type Long Desc',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\ContactMessageTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ContactMessageTypeQuery(get_called_class());
    }
}
