<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ImportFileStiCoachReport]].
 *
 * @see ImportFileStiCoachReport
 */
class ImportFileStiCoachReportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ImportFileStiCoachReport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ImportFileStiCoachReport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
