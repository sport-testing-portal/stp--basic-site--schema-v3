<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwCoachTeamAssignmentStatus]].
 *
 * @see VwCoachTeamAssignmentStatus
 */
class VwCoachTeamAssignmentStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwCoachTeamAssignmentStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwCoachTeamAssignmentStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
