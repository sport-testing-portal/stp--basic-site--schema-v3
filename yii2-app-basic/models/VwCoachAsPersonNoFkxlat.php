<?php

namespace app\models;

use Yii;
use \app\models\base\VwCoachAsPersonNoFkxlat as BaseVwCoachAsPersonNoFkxlat;

/**
 * This is the model class for table "vwCoach_as_person_no_fkxlat".
 */
class VwCoachAsPersonNoFkxlat extends BaseVwCoachAsPersonNoFkxlat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_id', 'org_id', 'coach_id', 'app_user_id', 'person_type_id', 'gender_id', 'person_weight', 'created_by', 'updated_by', 'coach_type_id'], 'integer'],
            [['person_date_of_birth', 'created_at', 'updated_at'], 'safe'],
            [['person_name_prefix', 'person_postal_code'], 'string', 'max' => 25],
            [['person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_country', 'person_state_or_region'], 'string', 'max' => 45],
            [['person_name_full'], 'string', 'max' => 90],
            [['person_email_personal', 'person_email_work'], 'string', 'max' => 60],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'coach_info_source_scrape_url'], 'string', 'max' => 100],
            [['person_height'], 'string', 'max' => 5],
            [['person_tshirt_size'], 'string', 'max' => 10],
            [['person_city'], 'string', 'max' => 75],
            [['person_country_code'], 'string', 'max' => 3],
            [['coach_specialty'], 'string', 'max' => 95],
            [['coach_certifications', 'coach_qrcode_uri'], 'string', 'max' => 150],
            [['coach_comments'], 'string', 'max' => 253],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person_id' => 'Person ID',
            'org_id' => 'Org ID',
            'coach_id' => 'Coach ID',
            'app_user_id' => 'App User ID',
            'person_type_id' => 'Person Type ID',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_name_full' => 'Person Name Full',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'gender_id' => 'Gender ID',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_height' => 'Person Height',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'coach_type_id' => 'Coach Type ID',
            'coach_specialty' => 'Coach Specialty',
            'coach_certifications' => 'Coach Certifications',
            'coach_comments' => 'Coach Comments',
            'coach_qrcode_uri' => 'Coach Qrcode Uri',
            'coach_info_source_scrape_url' => 'Coach Info Source Scrape Url',
        ];
    }
}
