<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SchoolSat]].
 *
 * @see SchoolSat
 */
class SchoolSatQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SchoolSat[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SchoolSat|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
