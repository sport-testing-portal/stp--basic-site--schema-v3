<?php

namespace app\models;

use Yii;
use \app\models\base\Note as BaseNote;

/**
 * This is the model class for table "note".
 */
class Note extends BaseNote
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'person_id', 'note_type_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['note'], 'string', 'max' => 1024],
            [['note_tags'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'note_id' => 'Note ID',
            'org_id' => 'Org ID',
            'person_id' => 'Person ID',
            'note_type_id' => 'Note Type ID',
            'note' => 'Note',
            'note_tags' => 'Note Tags',
            'lock' => 'Lock',
        ];
    }
}
