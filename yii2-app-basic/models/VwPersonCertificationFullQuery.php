<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwPersonCertificationFull]].
 *
 * @see VwPersonCertificationFull
 */
class VwPersonCertificationFullQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwPersonCertificationFull[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwPersonCertificationFull|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
