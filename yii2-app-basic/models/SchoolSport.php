<?php

namespace app\models;

use Yii;
use \app\models\base\SchoolSport as BaseSchoolSport;

/**
 * This is the model class for table "school_sport".
 */
class SchoolSport extends BaseSchoolSport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['school_id', 'sport_id', 'gender_id', 'conference_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['school_id', 'sport_id', 'gender_id', 'conference_id'], 'unique', 'targetAttribute' => ['school_id', 'sport_id', 'gender_id', 'conference_id'], 'message' => 'The combination of School ID, Sport ID, Gender ID and Conference ID has already been taken.'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'school_sport_id' => 'School Sport ID',
            'school_id' => 'School ID',
            'sport_id' => 'Sport ID',
            'gender_id' => 'Gender ID',
            'conference_id' => 'Conference ID',
            'lock' => 'Lock',
        ];
    }
}
