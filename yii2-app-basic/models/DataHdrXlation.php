<?php

namespace app\models;

use Yii;
use \app\models\base\DataHdrXlation as BaseDataHdrXlation;

/**
 * This is the model class for table "data_hdr_xlation".
 * @since 0.8.0
 * @todo change field name
 */
class DataHdrXlation extends BaseDataHdrXlation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['data_hdr_xlation', 'data_hdr_entity_fetch_url'], 'string', 'max' => 150],
            [['data_hdr_entity_name'], 'string', 'max' => 75],
            [['hdr_sha1_hash'], 'string', 'max' => 40],
            [['data_hdr_regex'], 'string', 'max' => 65],
            [['data_hdr_is_trash_yn', 'lock'], 'string', 'max' => 1],
            [['gsm_target_table_name', 'data_hdr_xlation_visual_example'], 'string', 'max' => 253],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'data_hdr_xlation_id' => 'Hdr Xlation ID',
            'data_hdr_xlation' => 'Hdr Xlation',
            'data_hdr_entity_name' => 'Hdr Entity Name',
            'hdr_sha1_hash' => 'Hdr Sha1 Hash',
            'data_hdr_regex' => 'Hdr Regex',
            'data_hdr_is_trash_yn' => 'Hdr Is Trash Yn',
            'gsm_target_table_name' => 'Gsm Target Table Name',
            'data_hdr_entity_fetch_url' => 'Hdr Entity Fetch Url',
            'data_hdr_xlation_visual_example' => 'Hdr Xlation Visual Example',
            'lock' => 'Lock',
        ];
    }
}
