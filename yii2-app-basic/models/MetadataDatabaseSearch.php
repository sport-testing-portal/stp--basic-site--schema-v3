<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataDatabase;

/**
 * app\models\MetadataDatabaseSearch represents the model behind the search form about `app\models\MetadataDatabase`.
 */
 class MetadataDatabaseSearch extends MetadataDatabase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['database_id', 'col_name_len_max', 'created_by', 'updated_by'], 'integer'],
            [['table_name', 'schema_name', 'app_module_name', 'developer_notes', 'has_named_primary_key', 'has_v3_fields', 'has_representing_field', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataDatabase::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'database_id' => $this->database_id,
            'col_name_len_max' => $this->col_name_len_max,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'table_name', $this->table_name])
            ->andFilterWhere(['like', 'schema_name', $this->schema_name])
            ->andFilterWhere(['like', 'app_module_name', $this->app_module_name])
            ->andFilterWhere(['like', 'developer_notes', $this->developer_notes])
            ->andFilterWhere(['like', 'has_named_primary_key', $this->has_named_primary_key])
            ->andFilterWhere(['like', 'has_v3_fields', $this->has_v3_fields])            
            ->andFilterWhere(['like', 'has_representing_field', $this->has_representing_field])            
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
