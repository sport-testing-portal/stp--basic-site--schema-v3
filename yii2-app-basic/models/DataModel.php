<?php

namespace app\models;

use Yii;
use \app\models\base\DataModel as BaseDataModel;

/**
 * This is the model class for table "data_model".
 * @todo rename this table to match the other metadata tables
 */
class DataModel extends BaseDataModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['data_model_digest_created_at', 'data_model_created_at', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['table_name'], 'string', 'max' => 75],
            [['table_structure_digest', 'data_model_digest'], 'string', 'max' => 40],
            [['dupe_detection_field_list'], 'string', 'max' => 250],
            [['trigger__bfr_insert_uri', 'trigger__bfr_update_uri'], 'string', 'max' => 150],
            [['trigger__bfr_template_cec_ver', 'data_model_cec_ver'], 'string', 'max' => 10],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'data_model_id' => 'Data Model ID',
            'table_name' => 'Table Name',
            'table_structure_digest' => 'Table Structure Digest',
            'data_model_digest' => 'Data Model Digest',
            'data_model_digest_created_at' => 'Data Model Digest Created At',
            'data_model_created_at' => 'Data Model Created At',
            'dupe_detection_field_list' => 'Dupe Detection Field List',
            'trigger__bfr_insert_uri' => 'Trigger  Bfr Insert Uri',
            'trigger__bfr_update_uri' => 'Trigger  Bfr Update Uri',
            'trigger__bfr_template_cec_ver' => 'Trigger  Bfr Template Cec Ver',
            'data_model_cec_ver' => 'Data Model Cec Ver',
            'lock' => 'Lock',
        ];
    }
}
