<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ZipCodeDistinct]].
 *
 * @see ZipCodeDistinct
 */
class ZipCodeDistinctQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ZipCodeDistinct[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ZipCodeDistinct|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
