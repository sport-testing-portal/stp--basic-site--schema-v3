<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwAthleteTeams6]].
 *
 * @see VwAthleteTeams6
 */
class VwAthleteTeams6Query extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwAthleteTeams6[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwAthleteTeams6|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
