<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataCodebaseFunctionItems;

/**
 * app\models\MetadataCodebaseFunctionItemsSearch represents the model behind the search form about `app\models\MetadataCodebaseFunctionItems`.
 */
 class MetadataCodebaseFunctionItemsSearch extends MetadataCodebaseFunctionItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metadata__codebase_function_items_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataCodebaseFunctionItems::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'metadata__codebase_function_items_id' => $this->metadata__codebase_function_items_id,
        ]);

        return $dataProvider;
    }
}
