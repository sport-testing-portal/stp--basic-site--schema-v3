<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataDatabase as BaseMetadataDatabase;

/**
 * This is the model class for table "metadata__database".
 */
class MetadataDatabase extends BaseMetadataDatabase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['table_name', 'schema_name'], 'required'],
            [['col_name_len_max', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['table_name', 'schema_name'], 'string', 'max' => 150],
            [['app_module_name'], 'string', 'max' => 75],
            [['developer_notes'], 'string', 'max' => 100],
            [['has_named_primary_key', 'has_v3_fields', 'has_representing_field'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'database_id' => 'Database ID',
            'table_name' => 'Table Name',
            'schema_name' => 'Schema Name',
            'app_module_name' => 'App Module Name',
            'developer_notes' => 'Developer Notes',
            'col_name_len_max' => 'Col Name Len Max',
            'has_named_primary_key' => 'Has Named Primary Key',
            'has_v3_fields' => 'Has V3 Fields',
            'has_representing_field' => 'Has Representing Field',
            'lock' => 'Lock',
        ];
    }
}
