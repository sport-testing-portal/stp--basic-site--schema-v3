<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SubscriptionLog]].
 *
 * @see SubscriptionLog
 */
class SubscriptionLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SubscriptionLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SubscriptionLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
