<?php

namespace app\models;

use Yii;
use \app\models\base\VwTestResultFull as BaseVwTestResultFull;

/**
 * This is the model class for table "vwTestResultFull".
 */
class VwTestResultFull extends BaseVwTestResultFull
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'tedtd_display_order', 'overall_ranking', 'positional_ranking', 'player_sport_position_id', 'gender_id', 'age_group_id', 'age_group_min', 'age_group_max', 'person_id', 'player_id', 'test_eval_summary_log_id', 'test_eval_detail_log_id', 'source_event_id', 'provider_id', 'category_id', 'type_id', 'desc_id'], 'integer'],
            [['score', 'total_overall_ranking', 'total_positional_ranking'], 'number'],
            [['test_date_iso'], 'safe'],
            [['person'], 'string', 'max' => 90],
            [['ptype', 'test_desc', 'test_type', 'split', 'test_units', 'trial_status', 'person_name_first', 'person_name_last', 'person_state_or_region', 'sport_position_name', 'age_group_name', 'latitude', 'longitude'], 'string', 'max' => 45],
            [['gender_code', 'rating_bias'], 'string', 'max' => 5],
            [['test_category', 'test_category2', 'score_url', 'video_url'], 'string', 'max' => 150],
            [['attempt'], 'string', 'max' => 4],
            [['test_date'], 'string', 'max' => 40],
            [['tester', 'person_city'], 'string', 'max' => 75],
            [['person_country_code'], 'string', 'max' => 3],
            [['person_postal_code'], 'string', 'max' => 25],
            [['gender_desc'], 'string', 'max' => 30],
            [['country_long_name'], 'string', 'max' => 80],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'person' => 'Person',
            'ptype' => 'Ptype',
            'gender_code' => 'Gender Code',
            'rating_bias' => 'Rating Bias',
            'tedtd_display_order' => 'Tedtd Display Order',
            'test_desc' => 'Test Desc',
            'test_type' => 'Test Type',
            'test_category' => 'Test Category',
            'test_category2' => 'Test Category2',
            'split' => 'Split',
            'score' => 'Score',
            'test_units' => 'Test Units',
            'attempt' => 'Attempt',
            'trial_status' => 'Trial Status',
            'overall_ranking' => 'Overall Ranking',
            'positional_ranking' => 'Positional Ranking',
            'total_overall_ranking' => 'Total Overall Ranking',
            'total_positional_ranking' => 'Total Positional Ranking',
            'score_url' => 'Score Url',
            'video_url' => 'Video Url',
            'test_date' => 'Test Date',
            'test_date_iso' => 'Test Date Iso',
            'tester' => 'Tester',
            'person_name_first' => 'Person Name First',
            'person_name_last' => 'Person Name Last',
            'person_city' => 'Person City',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'person_postal_code' => 'Person Postal Code',
            'player_sport_position_id' => 'Player Sport Position ID',
            'sport_position_name' => 'Sport Position Name',
            'gender_desc' => 'Gender Desc',
            'gender_id' => 'Gender ID',
            'age_group_name' => 'Age Group Name',
            'age_group_id' => 'Age Group ID',
            'age_group_min' => 'Age Group Min',
            'age_group_max' => 'Age Group Max',
            'country_long_name' => 'Country Long Name',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'person_id' => 'Person ID',
            'player_id' => 'Player ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'test_eval_detail_log_id' => 'Test Eval Detail Log ID',
            'source_event_id' => 'Source Event ID',
            'provider_id' => 'Provider ID',
            'category_id' => 'Category ID',
            'type_id' => 'Type ID',
            'desc_id' => 'Desc ID',
        ];
    }
}
