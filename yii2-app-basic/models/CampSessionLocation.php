<?php

namespace app\models;

use Yii;
use \app\models\base\CampSessionLocation as BaseCampSessionLocation;

/**
 * This is the model class for table "camp_session_location".
 */
class CampSessionLocation extends BaseCampSessionLocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['camp_session_id', 'address_id', 'season_id', 'gender_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'camp_session_location_id' => 'Camp Session Location ID',
            'camp_session_id' => 'Camp Session ID',
            'address_id' => 'Address ID',
            'season_id' => 'Season ID',
            'gender_id' => 'Gender ID',
            'lock' => 'Lock',
        ];
    }
}
