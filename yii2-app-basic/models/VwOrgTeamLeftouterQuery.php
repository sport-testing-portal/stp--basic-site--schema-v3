<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwOrgTeamLeftouter]].
 *
 * @see VwOrgTeamLeftouter
 */
class VwOrgTeamLeftouterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwOrgTeamLeftouter[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwOrgTeamLeftouter|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
