<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\OrgGoverningBody;

/**
 * app\models\OrgGoverningBodySearch represents the model behind the search form about `app\models\OrgGoverningBody`.
 */
 class OrgGoverningBodySearch extends OrgGoverningBody
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_governing_body_id', 'org_id', 'governing_body_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrgGoverningBody::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'org_governing_body_id' => $this->org_governing_body_id,
            'org_id' => $this->org_id,
            'governing_body_id' => $this->governing_body_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
