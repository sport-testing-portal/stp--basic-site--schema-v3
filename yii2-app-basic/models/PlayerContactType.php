<?php

namespace app\models;

use Yii;
use \app\models\base\PlayerContactType as BasePlayerContactType;

/**
 * This is the model class for table "player_contact_type".
 */
class PlayerContactType extends BasePlayerContactType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['player_contact_type'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['player_contact_type'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['player_contact_type'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_contact_type_id' => 'Player Contact Type ID',
            'player_contact_type' => 'Player Contact Type',
            'lock' => 'Lock',
        ];
    }
}
