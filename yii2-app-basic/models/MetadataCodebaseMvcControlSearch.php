<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataCodebaseMvcControl;

/**
 * app\models\MetadataCodebaseMvcControlSearch represents the model behind the search form about `app\models\MetadataCodebaseMvcControl`.
 */
 class MetadataCodebaseMvcControlSearch extends MetadataCodebaseMvcControl
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_mvc_control_id', 'codebase_id', 'created_by', 'updated_by'], 'integer'],
            [['controller_actions', 'action_function', 'action_params', 'action_url', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataCodebaseMvcControl::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codebase_mvc_control_id' => $this->codebase_mvc_control_id,
            'codebase_id' => $this->codebase_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'controller_actions', $this->controller_actions])
            ->andFilterWhere(['like', 'action_function', $this->action_function])
            ->andFilterWhere(['like', 'action_params', $this->action_params])
            ->andFilterWhere(['like', 'action_url', $this->action_url])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
