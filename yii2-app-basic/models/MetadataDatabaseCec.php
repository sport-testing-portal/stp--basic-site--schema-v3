<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataDatabaseCec as BaseMetadataDatabaseCec;

/**
 * This is the model class for table "metadata__database_cec".
 */
class MetadataDatabaseCec extends BaseMetadataDatabaseCec
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['database_cec'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['database_cec'], 'string', 'max' => 75],
            [['database_cec_desc_short', 'cec_status_tag'], 'string', 'max' => 45],
            [['database_cec_desc_long'], 'string', 'max' => 175],
            [['cec_scope'], 'string', 'max' => 253],
            [['rule_file_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'database_cec_id' => 'Database Cec ID',
            'database_cec' => 'Database Cec',
            'database_cec_desc_short' => 'Database Cec Desc Short',
            'database_cec_desc_long' => 'Database Cec Desc Long',
            'cec_status_tag' => 'Cec Status Tag',
            'cec_scope' => 'Cec Scope',
            'rule_file_uri' => 'Rule File Uri',
            'lock' => 'Lock',
        ];
    }
}
