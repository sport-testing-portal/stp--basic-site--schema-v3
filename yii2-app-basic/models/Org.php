<?php

namespace app\models;

use Yii;
use \app\models\base\Org as BaseOrg;

/**
 * This is the model class for table "org".
 */
class Org extends BaseOrg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_type_id', 'org_level_id', 'created_by', 'updated_by'], 'integer'],
            [['org'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['org'], 'string', 'max' => 100],
            [['org_governing_body'], 'string', 'max' => 45],
            [['org_ncaa_clearing_house_id', 'org_phone_main'], 'string', 'max' => 25],
            [['org_website_url', 'org_twitter_url', 'org_facebook_url'], 'string', 'max' => 150],
            [['org_email_main'], 'string', 'max' => 75],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'org_level_id' => 'Org Level ID',
            'org' => 'Org',
            'org_governing_body' => 'Org Governing Body',
            'org_ncaa_clearing_house_id' => 'Org Ncaa Clearing House ID',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'lock' => 'Lock',
        ];
    }
    
		    
   /** 
    * @inheritdoc 
    * dbyrd copied this method from the base model definition to reduce overwrites of the labels  
    */ 
   public function attributeLabels() 
   { 
       return [ 
           'org_id' => 'ID', 
           'org_type_id' => 'Org Type', 
           'org_level_id' => 'Org Level', 
           'org' => 'Org', 
           'org_governing_body' => 'Governing Body', 
           'org_ncaa_clearing_house_id' => 'NCAA Clearing House ID', 
           'org_website_url' => 'Website Url', 
           'org_twitter_url' => 'Twitter Url', 
           'org_facebook_url' => 'Facebook Url', 
           'org_phone_main' => 'Phone Main', 
           'org_email_main' => 'Email Main', 
           'org_addr1' => 'Addr1', 
           'org_addr2' => 'Addr2', 
           'org_addr3' => 'Addr3', 
           'org_city' => 'City', 
           'org_state_or_region' => 'State', 
           'org_postal_code' => 'Zip Code', 
           'org_country_code_iso3' => 'ISO3', 
           'lock' => 'Lock', 
       ]; 
   }     
}
