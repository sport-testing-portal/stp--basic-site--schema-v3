<?php

namespace app\models;

use Yii;
use \app\models\base\VwPlayerVerbose as BaseVwPlayerVerbose;

/**
 * This is the model class for table "vwPlayer_verbose".
 */
class VwPlayerVerbose extends BaseVwPlayerVerbose
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'player_id', 'person_id', 'org_id', 'app_user_id', 'person_weight', 'person_type_id', 'team_player_id', 'team_id', 'team_org_id', 'team_school_id', 'team_sport_id', 'team_camp_id', 'team_gender_id'], 'integer'],
            [['last_visit_on', 'player_waiver_minor_dt', 'player_waiver_adult_dt', 'person_date_of_birth', 'team_play_begin_dt', 'team_play_end_dt'], 'safe'],
            [['person_age'], 'number'],
            [['person_type_name'], 'required'],
            [['person_name', 'person_name_full'], 'string', 'max' => 90],
            [['src_table0', 'src_table6'], 'string', 'max' => 4],
            [['logon_email'], 'string', 'max' => 255],
            [['src_table1', 'src_table2', 'src_table4'], 'string', 'max' => 6],
            [['player_access_code', 'player_sport_position_preference', 'person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_height_imperial', 'person_country', 'person_state_or_region', 'person_type_name', 'person_type_desc_short', 'person_type_desc_long', 'team_age_group'], 'string', 'max' => 45],
            [['player_parent_email', 'person_email_personal', 'person_email_work'], 'string', 'max' => 60],
            [['player_sport_preference'], 'string', 'max' => 35],
            [['player_shot_side_preference', 'person_name_prefix', 'person_postal_code'], 'string', 'max' => 25],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3'], 'string', 'max' => 100],
            [['person_dob_eng', 'person_tshirt_size', 'team_division'], 'string', 'max' => 10],
            [['person_bday_short'], 'string', 'max' => 37],
            [['person_bday_long'], 'string', 'max' => 69],
            [['person_height', 'gender_code'], 'string', 'max' => 5],
            [['person_city', 'team_name'], 'string', 'max' => 75],
            [['person_country_code'], 'string', 'max' => 3],
            [['src_table3', 'src_table5'], 'string', 'max' => 11],
            [['gender_desc'], 'string', 'max' => 30],
            [['team_gender'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person_name' => 'Person Name',
            'src_table0' => 'Src Table0',
            'user_id' => 'User ID',
            'logon_email' => 'Logon Email',
            'last_visit_on' => 'Last Visit On',
            'src_table1' => 'Src Table1',
            'player_id' => 'Player ID',
            'person_id' => 'Person ID',
            'player_access_code' => 'Player Access Code',
            'player_waiver_minor_dt' => 'Player Waiver Minor Dt',
            'player_waiver_adult_dt' => 'Player Waiver Adult Dt',
            'player_parent_email' => 'Player Parent Email',
            'player_sport_preference' => 'Player Sport Preference',
            'player_sport_position_preference' => 'Player Sport Position Preference',
            'player_shot_side_preference' => 'Player Shot Side Preference',
            'src_table2' => 'Src Table2',
            'org_id' => 'Org ID',
            'app_user_id' => 'App User ID',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_name_full' => 'Person Name Full',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_dob_eng' => 'Person Dob Eng',
            'person_bday_short' => 'Person Bday Short',
            'person_bday_long' => 'Person Bday Long',
            'person_age' => 'Person Age',
            'person_height' => 'Person Height',
            'person_height_imperial' => 'Person Height Imperial',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'src_table3' => 'Src Table3',
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
            'person_type_desc_short' => 'Person Type Desc Short',
            'person_type_desc_long' => 'Person Type Desc Long',
            'src_table4' => 'Src Table4',
            'gender_desc' => 'Gender Desc',
            'gender_code' => 'Gender Code',
            'src_table5' => 'Src Table5',
            'team_player_id' => 'Team Player ID',
            'team_id' => 'Team ID',
            'team_play_begin_dt' => 'Team Play Begin Dt',
            'team_play_end_dt' => 'Team Play End Dt',
            'src_table6' => 'Src Table6',
            'team_org_id' => 'Team Org ID',
            'team_school_id' => 'Team School ID',
            'team_sport_id' => 'Team Sport ID',
            'team_camp_id' => 'Team Camp ID',
            'team_gender_id' => 'Team Gender ID',
            'team_name' => 'Team Name',
            'team_gender' => 'Team Gender',
            'team_division' => 'Team Division',
            'team_age_group' => 'Team Age Group',
        ];
    }
}
