<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SubscriptionStatusType]].
 *
 * @see SubscriptionStatusType
 */
class SubscriptionStatusTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SubscriptionStatusType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SubscriptionStatusType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
