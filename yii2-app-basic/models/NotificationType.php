<?php

namespace app\models;

use Yii;
use \app\models\base\NotificationType as BaseNotificationType;

/**
 * This is the model class for table "notification_type".
 */
class NotificationType extends BaseNotificationType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['notification_type'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['notification_type'], 'string', 'max' => 45],
            [['notification_type_desc_short'], 'string', 'max' => 75],
            [['notification_type_desc_long'], 'string', 'max' => 175],
            [['lock'], 'string', 'max' => 1],
            [['notification_type'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'notification_type_id' => 'Notification Type ID',
            'notification_type' => 'Notification Type',
            'notification_type_desc_short' => 'Notification Type Desc Short',
            'notification_type_desc_long' => 'Notification Type Desc Long',
            'lock' => 'Lock',
        ];
    }
}
