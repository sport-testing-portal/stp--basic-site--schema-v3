<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataCodebaseFunctionItems as BaseMetadataCodebaseFunctionItems;

/**
 * This is the model class for table "metadata__codebase_function_items".
 */
class MetadataCodebaseFunctionItems extends BaseMetadataCodebaseFunctionItems
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'metadata__codebase_function_items_id' => 'Metadata  Codebase Function Items ID',
        ];
    }
}
