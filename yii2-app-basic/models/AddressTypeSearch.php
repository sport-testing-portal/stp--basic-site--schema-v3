<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AddressType;

/**
 * app\models\AddressTypeSearch represents the model behind the search form about `app\models\AddressType`.
 */
 class AddressTypeSearch extends AddressType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_type_id', 'created_by', 'updated_by'], 'integer'],
            [['address_type', 'address_type_description_short', 'address_type_description_long', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AddressType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'address_type_id' => $this->address_type_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'address_type', $this->address_type])
            ->andFilterWhere(['like', 'address_type_description_short', $this->address_type_description_short])
            ->andFilterWhere(['like', 'address_type_description_long', $this->address_type_description_long])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
