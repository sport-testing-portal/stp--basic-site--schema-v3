<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetadataCodebaseMvcModel]].
 *
 * @see MetadataCodebaseMvcModel
 */
class MetadataCodebaseMvcModelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataCodebaseMvcModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataCodebaseMvcModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
