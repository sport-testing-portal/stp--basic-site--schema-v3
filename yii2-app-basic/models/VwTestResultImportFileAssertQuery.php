<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwTestResultImportFileAssert]].
 *
 * @see VwTestResultImportFileAssert
 */
class VwTestResultImportFileAssertQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwTestResultImportFileAssert[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwTestResultImportFileAssert|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
