<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Camp;

/**
 * app\models\CampSearch represents the model behind the search form about `app\models\Camp`.
 */
 class CampSearch extends Camp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_id', 'org_id', 'camp_cost_regular', 'camp_cost_early_registration', 'created_by', 'updated_by'], 'integer'],
            [['camp', 'camp_specialty', 'camp_team_discounts_available_yn', 'camp_scholarships_available_yn', 'camp_session_desc', 'camp_website_url', 'camp_session_url', 'camp_registration_url', 'camp_twitter_url', 'camp_facebook_url', 'camp_scholarship_application_info', 'camp_scholarship_application_request', 'camp_organizer_description', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Camp::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'camp_id' => $this->camp_id,
            'org_id' => $this->org_id,
            'camp_cost_regular' => $this->camp_cost_regular,
            'camp_cost_early_registration' => $this->camp_cost_early_registration,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'camp', $this->camp])
            ->andFilterWhere(['like', 'camp_specialty', $this->camp_specialty])
            ->andFilterWhere(['like', 'camp_team_discounts_available_yn', $this->camp_team_discounts_available_yn])
            ->andFilterWhere(['like', 'camp_scholarships_available_yn', $this->camp_scholarships_available_yn])
            ->andFilterWhere(['like', 'camp_session_desc', $this->camp_session_desc])
            ->andFilterWhere(['like', 'camp_website_url', $this->camp_website_url])
            ->andFilterWhere(['like', 'camp_session_url', $this->camp_session_url])
            ->andFilterWhere(['like', 'camp_registration_url', $this->camp_registration_url])
            ->andFilterWhere(['like', 'camp_twitter_url', $this->camp_twitter_url])
            ->andFilterWhere(['like', 'camp_facebook_url', $this->camp_facebook_url])
            ->andFilterWhere(['like', 'camp_scholarship_application_info', $this->camp_scholarship_application_info])
            ->andFilterWhere(['like', 'camp_scholarship_application_request', $this->camp_scholarship_application_request])
            ->andFilterWhere(['like', 'camp_organizer_description', $this->camp_organizer_description])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
