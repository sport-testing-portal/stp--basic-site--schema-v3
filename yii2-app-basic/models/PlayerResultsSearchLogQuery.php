<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PlayerResultsSearchLog]].
 *
 * @see PlayerResultsSearchLog
 */
class PlayerResultsSearchLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PlayerResultsSearchLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PlayerResultsSearchLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
