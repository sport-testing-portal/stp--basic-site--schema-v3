<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Org;

/**
 * app\models\OrgSearch represents the model behind the search form about `app\models\Org`.
 */
 class OrgSearch extends Org
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'org_type_id', 'org_level_id', 'created_by', 'updated_by'], 'integer'],
            [['org', 'org_governing_body', 'org_ncaa_clearing_house_id', 'org_website_url', 'org_twitter_url', 'org_facebook_url', 'org_phone_main', 'org_email_main', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Org::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'org_id' => $this->org_id,
            'org_type_id' => $this->org_type_id,
            'org_level_id' => $this->org_level_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'org', $this->org])
            ->andFilterWhere(['like', 'org_governing_body', $this->org_governing_body])
            ->andFilterWhere(['like', 'org_ncaa_clearing_house_id', $this->org_ncaa_clearing_house_id])
            ->andFilterWhere(['like', 'org_website_url', $this->org_website_url])
            ->andFilterWhere(['like', 'org_twitter_url', $this->org_twitter_url])
            ->andFilterWhere(['like', 'org_facebook_url', $this->org_facebook_url])
            ->andFilterWhere(['like', 'org_phone_main', $this->org_phone_main])
            ->andFilterWhere(['like', 'org_email_main', $this->org_email_main])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
