<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataApiClass as BaseMetadataApiClass;

/**
 * This is the model class for table "metadata__api_class".
 * @since 0.8.0
 */
class MetadataApiClass extends BaseMetadataApiClass
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['metadata__api_provider_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['metadata__api_class', 'metadata__api_class_desc', 'metadata__api_class_docs_url', 'metadata__api_reference_example', 'metadata__api_regex_find'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'metadata__api_class_id' => 'API Class ID',
            'metadata__api_provider_id' => 'API Provider ID',
            'metadata__api_class' => 'API Class',
            'metadata__api_class_desc' => 'API Class Desc',
            'metadata__api_class_docs_url' => 'API Class Docs Url',
            'metadata__api_reference_example' => 'API Reference Example',
            'metadata__api_regex_find' => 'API Regex Find',
            'lock' => 'Lock',
        ];
    }
}
