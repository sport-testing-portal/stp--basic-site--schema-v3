<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[NotificationType]].
 *
 * @see NotificationType
 */
class NotificationTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NotificationType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NotificationType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
