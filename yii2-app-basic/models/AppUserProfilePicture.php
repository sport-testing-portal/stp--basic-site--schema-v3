<?php

namespace app\models;

use Yii;
use \app\models\base\AppUserProfilePicture as BaseAppUserProfilePicture;

/**
 * This is the model class for table "app_user_profile_picture".
 */
class AppUserProfilePicture extends BaseAppUserProfilePicture
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['app_user_id', 'app_user_profile_picture_filename', 'app_user_profile_picture_width', 'app_user_profile_picture_height', 'app_user_profile_picture_mimetype', 'app_user_profile_picture_contents'], 'required'],
            [['app_user_id', 'app_user_profile_original_picture_id', 'app_user_profile_picture_width', 'app_user_profile_picture_height', 'created_by', 'updated_by'], 'integer'],
            [['app_user_profile_picture_contents'], 'string'],
            [['created_on', 'created_at', 'updated_at'], 'safe'],
            [['app_user_profile_picture_filename', 'app_user_profile_picture_mimetype'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_user_profile_picture_id' => 'App User Profile Picture ID',
            'app_user_id' => 'App User ID',
            'app_user_profile_original_picture_id' => 'App User Profile Original Picture ID',
            'app_user_profile_picture_filename' => 'App User Profile Picture Filename',
            'app_user_profile_picture_width' => 'App User Profile Picture Width',
            'app_user_profile_picture_height' => 'App User Profile Picture Height',
            'app_user_profile_picture_mimetype' => 'App User Profile Picture Mimetype',
            'app_user_profile_picture_contents' => 'App User Profile Picture Contents',
            'created_on' => 'Created On',
            'lock' => 'Lock',
        ];
    }
}
