<?php

namespace app\models;

use Yii;
use \app\models\base\EventAttendeeLog as BaseEventAttendeeLog;

/**
 * This is the model class for table "event_attendee_log".
 */
class EventAttendeeLog extends BaseEventAttendeeLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['event_id', 'person_id', 'event_attendee_type_id', 'created_by', 'updated_by'], 'integer'],
            [['event_rsvp_dt', 'event_attendance_dt', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'event_attendee_log_id' => 'Event Attendee Log ID',
            'event_id' => 'Event ID',
            'person_id' => 'Person ID',
            'event_attendee_type_id' => 'Event Attendee Type ID',
            'event_rsvp_dt' => 'Event Rsvp Dt',
            'event_attendance_dt' => 'Event Attendance Dt',
            'lock' => 'Lock',
        ];
    }
}
