<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwPlayer]].
 *
 * @see VwPlayer
 */
class VwPlayerQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwPlayer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwPlayer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
