<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataCodebaseDevStatus as BaseMetadataCodebaseDevStatus;

/**
 * This is the model class for table "metadata__codebase_dev_status".
 */
class MetadataCodebaseDevStatus extends BaseMetadataCodebaseDevStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['codebase_id', 'codebase_dev_id', 'dev_status'], 'required'],
            [['codebase_id', 'codebase_dev_id', 'created_by', 'updated_by'], 'integer'],
            [['dev_status_at', 'created_at', 'updated_at'], 'safe'],
            [['dev_status', 'dev_status_tag', 'dev_status_bfr'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'dev_status_id' => 'Dev Status ID',
            'codebase_id' => 'Codebase ID',
            'codebase_dev_id' => 'Codebase Dev ID',
            'dev_status' => 'Dev Status',
            'dev_status_tag' => 'Dev Status Tag',
            'dev_status_bfr' => 'Dev Status Bfr',
            'dev_status_at' => 'Dev Status At',
            'lock' => 'Lock',
        ];
    }
}
