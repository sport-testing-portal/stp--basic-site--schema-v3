<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataApiClassFunc as BaseMetadataApiClassFunc;

/**
 * This is the model class for table "metadata__api_class_func".
 */
class MetadataApiClassFunc extends BaseMetadataApiClassFunc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['metadata__api_class_id'], 'required'],
            [['metadata__api_class_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['metadata__api_class_func', 'metadata__api_class_func_desc', 'metadata__api_class_func_docs_url', 'metadata__api_class_func_reference_example', 'metadata__api_class_func_regex_find'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'metadata__api_class_func_id' => 'API Class Func ID',
            'metadata__api_class_id' => 'API Class ID',
            'metadata__api_class_func' => 'API Class Func',
            'metadata__api_class_func_desc' => 'API Class Func Desc',
            'metadata__api_class_func_docs_url' => 'API Class Func Docs Url',
            'metadata__api_class_func_reference_example' => 'API Class Func Reference Example',
            'metadata__api_class_func_regex_find' => 'API Class Func Regex Find',
            'lock' => 'Lock',
        ];
    }
}
