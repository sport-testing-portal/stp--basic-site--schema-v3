<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataHdrXlationItem;

/**
 * app\models\DataHdrXlationItemSearch represents the model behind the search form about `app\models\DataHdrXlationItem`.
 */
 class DataHdrXlationItemSearch extends DataHdrXlationItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_hdr_xlation_item_id', 'data_hdr_xlation_id', 'ordinal_position_num', 'data_hdr_source_value_is_type_name', 'created_by', 'updated_by'], 'integer'],
            [['data_hdr_xlation_item_source_value', 'data_hdr_xlation_item_target_value', 'data_hdr_xlation_item_target_table', 'data_hdr_source_value_type_name_source', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataHdrXlationItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'data_hdr_xlation_item_id' => $this->data_hdr_xlation_item_id,
            'data_hdr_xlation_id' => $this->data_hdr_xlation_id,
            'ordinal_position_num' => $this->ordinal_position_num,
            'data_hdr_source_value_is_type_name' => $this->data_hdr_source_value_is_type_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'data_hdr_xlation_item_source_value', $this->data_hdr_xlation_item_source_value])
            ->andFilterWhere(['like', 'data_hdr_xlation_item_target_value', $this->data_hdr_xlation_item_target_value])
            ->andFilterWhere(['like', 'data_hdr_xlation_item_target_table', $this->data_hdr_xlation_item_target_table])
            ->andFilterWhere(['like', 'data_hdr_source_value_type_name_source', $this->data_hdr_source_value_type_name_source])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
