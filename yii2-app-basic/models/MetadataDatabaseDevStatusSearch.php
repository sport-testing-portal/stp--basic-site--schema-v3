<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataDatabaseDevStatus;

/**
 * app\models\MetadataDatabaseDevStatusSearch represents the model behind the search form about `app\models\MetadataDatabaseDevStatus`.
 */
 class MetadataDatabaseDevStatusSearch extends MetadataDatabaseDevStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dev_status_id', 'database_id', 'database_dev_id', 'created_by', 'updated_by'], 'integer'],
            [['dev_status', 'dev_status_tag', 'dev_status_bfr', 'dev_status_at', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataDatabaseDevStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'dev_status_id' => $this->dev_status_id,
            'database_id' => $this->database_id,
            'database_dev_id' => $this->database_dev_id,
            'dev_status_at' => $this->dev_status_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'dev_status', $this->dev_status])
            ->andFilterWhere(['like', 'dev_status_tag', $this->dev_status_tag])
            ->andFilterWhere(['like', 'dev_status_bfr', $this->dev_status_bfr])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
