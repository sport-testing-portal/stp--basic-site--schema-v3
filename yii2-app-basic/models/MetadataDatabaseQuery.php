<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetadataDatabase]].
 *
 * @see MetadataDatabase
 */
class MetadataDatabaseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataDatabase[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataDatabase|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
