<?php

namespace app\models;

use Yii;
use \app\models\base\UserLoginAttempt as BaseUserLoginAttempt;

/**
 * This is the model class for table "user_login_attempt".
 */
class UserLoginAttempt extends BaseUserLoginAttempt
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['username', 'user_id'], 'required'],
            [['user_id', 'ipv4'], 'integer'],
            [['performed_on'], 'safe'],
            [['username', 'session_id', 'user_agent'], 'string', 'max' => 255],
            [['is_successful'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'user_id' => 'User ID',
            'performed_on' => 'Performed On',
            'is_successful' => 'Is Successful',
            'session_id' => 'Session ID',
            'ipv4' => 'Ipv4',
            'user_agent' => 'User Agent',
        ];
    }
}
