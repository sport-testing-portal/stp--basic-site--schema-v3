<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[NotificationTemplateType]].
 *
 * @see NotificationTemplateType
 */
class NotificationTemplateTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NotificationTemplateType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NotificationTemplateType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
