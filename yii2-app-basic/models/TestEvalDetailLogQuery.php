<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TestEvalDetailLog]].
 *
 * @see TestEvalDetailLog
 */
class TestEvalDetailLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TestEvalDetailLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TestEvalDetailLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
