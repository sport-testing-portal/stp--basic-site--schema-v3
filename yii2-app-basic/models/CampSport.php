<?php

namespace app\models;

use Yii;
use \app\models\base\CampSport as BaseCampSport;

/**
 * This is the model class for table "camp_sport".
 */
class CampSport extends BaseCampSport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['camp_id', 'sport_id', 'gender_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'camp_sport_id' => 'Camp Sport ID',
            'camp_id' => 'Camp ID',
            'sport_id' => 'Sport ID',
            'gender_id' => 'Gender ID',
            'lock' => 'Lock',
        ];
    }
}
