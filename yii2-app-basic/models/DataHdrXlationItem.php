<?php

namespace app\models;

use Yii;
use \app\models\base\DataHdrXlationItem as BaseDataHdrXlationItem;

/**
 * This is the model class for table "data_hdr_xlation_item".
 * @since 0.8.0
 */
class DataHdrXlationItem extends BaseDataHdrXlationItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['data_hdr_xlation_id', 'ordinal_position_num', 'data_hdr_source_value_is_type_name', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['data_hdr_xlation_item_source_value', 'data_hdr_xlation_item_target_value'], 'string', 'max' => 75],
            [['data_hdr_xlation_item_target_table', 'data_hdr_source_value_type_name_source'], 'string', 'max' => 150],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'data_hdr_xlation_item_id' => 'Hdr Xlation Item ID',
            'data_hdr_xlation_id' => 'Hdr Xlation ID',
            'data_hdr_xlation_item_source_value' => 'Hdr Xlation Item Source Value',
            'data_hdr_xlation_item_target_value' => 'Hdr Xlation Item Target Value',
            'ordinal_position_num' => 'Ordinal Position Num',
            'data_hdr_xlation_item_target_table' => 'Hdr Xlation Item Target Table',
            'data_hdr_source_value_type_name_source' => 'Hdr Source Value Type Name Source',
            'data_hdr_source_value_is_type_name' => 'Hdr Source Value Is Type Name',
            'lock' => 'Lock',
        ];
    }
}
