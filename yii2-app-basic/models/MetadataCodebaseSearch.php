<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataCodebase;

/**
 * app\models\MetadataCodebaseSearch represents the model behind the search form about `app\models\MetadataCodebase`.
 */
 class MetadataCodebaseSearch extends MetadataCodebase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_id', 'created_by', 'updated_by'], 'integer'],
            [['file_name', 'file_path_uri', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataCodebase::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codebase_id' => $this->codebase_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name])
            ->andFilterWhere(['like', 'file_path_uri', $this->file_path_uri])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
