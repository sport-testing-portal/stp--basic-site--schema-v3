<?php

namespace app\models;

use Yii;
use \app\models\base\AppUserRegistration as BaseAppUserRegistration;

/**
 * This is the model class for table "app_user_registration".
 */
class AppUserRegistration extends BaseAppUserRegistration
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_id', 'player_id', 'coach_id'], 'required'],
            [['person_id', 'org_id', 'app_user_id', 'person_type_id', 'user_id', 'gender_id', 'person_weight', 'player_id', 'coach_id', 'coach_type_id', 'created_by', 'updated_by'], 'integer'],
            [['person_date_of_birth', 'player_waiver_minor_dt', 'player_waiver_adult_dt', 'created_at', 'updated_at'], 'safe'],
            [['person_name_prefix', 'person_postal_code', 'player_shot_side_preference'], 'string', 'max' => 25],
            [['person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_country', 'person_state_or_region', 'player_access_code', 'player_sport_position_preference'], 'string', 'max' => 45],
            [['person_name_full'], 'string', 'max' => 90],
            [['person_email_personal', 'person_email_work', 'player_parent_email'], 'string', 'max' => 60],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'coach_info_source_scrape_url'], 'string', 'max' => 100],
            [['person_height', 'person_country_code'], 'string', 'max' => 5],
            [['person_tshirt_size'], 'string', 'max' => 10],
            [['person_city'], 'string', 'max' => 75],
            [['player_sport_preference'], 'string', 'max' => 35],
            [['coach_specialty'], 'string', 'max' => 95],
            [['coach_certifications', 'coach_qrcode_uri'], 'string', 'max' => 150],
            [['coach_comments'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_user_registration_id' => 'App User Registration ID',
            'person_id' => 'Person ID',
            'org_id' => 'Org ID',
            'app_user_id' => 'App User ID',
            'person_type_id' => 'Person Type ID',
            'user_id' => 'User ID',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_name_full' => 'Person Name Full',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'gender_id' => 'Gender ID',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_height' => 'Person Height',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'player_id' => 'Player ID',
            'player_access_code' => 'Player Access Code',
            'player_waiver_minor_dt' => 'Player Waiver Minor Dt',
            'player_waiver_adult_dt' => 'Player Waiver Adult Dt',
            'player_parent_email' => 'Player Parent Email',
            'player_sport_preference' => 'Player Sport Preference',
            'player_sport_position_preference' => 'Player Sport Position Preference',
            'player_shot_side_preference' => 'Player Shot Side Preference',
            'coach_id' => 'Coach ID',
            'coach_type_id' => 'Coach Type ID',
            'coach_specialty' => 'Coach Specialty',
            'coach_certifications' => 'Coach Certifications',
            'coach_comments' => 'Coach Comments',
            'coach_qrcode_uri' => 'Coach Qrcode Uri',
            'coach_info_source_scrape_url' => 'Coach Info Source Scrape Url',
            'lock' => 'Lock',
        ];
    }
}
