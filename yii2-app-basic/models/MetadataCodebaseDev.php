<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataCodebaseDev as BaseMetadataCodebaseDev;

/**
 * This is the model class for table "metadata__codebase_dev".
 */
class MetadataCodebaseDev extends BaseMetadataCodebaseDev
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['codebase_dev'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['codebase_dev'], 'string', 'max' => 75],
            [['codebase_dev_desc_short', 'dev_status_tag'], 'string', 'max' => 45],
            [['codebase_dev_desc_long'], 'string', 'max' => 175],
            [['dev_scope'], 'string', 'max' => 253],
            [['rule_file_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'codebase_dev_id' => 'Codebase Dev ID',
            'codebase_dev' => 'Codebase Dev',
            'codebase_dev_desc_short' => 'Codebase Dev Desc Short',
            'codebase_dev_desc_long' => 'Codebase Dev Desc Long',
            'dev_status_tag' => 'Dev Status Tag',
            'dev_scope' => 'Dev Scope',
            'rule_file_uri' => 'Rule File Uri',
            'lock' => 'Lock',
        ];
    }
}
