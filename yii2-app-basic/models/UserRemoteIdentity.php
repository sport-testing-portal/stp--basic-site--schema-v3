<?php

namespace app\models;

use Yii;
use \app\models\base\UserRemoteIdentity as BaseUserRemoteIdentity;

/**
 * This is the model class for table "user_remote_identity".
 */
class UserRemoteIdentity extends BaseUserRemoteIdentity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'provider', 'identifier'], 'required'],
            [['user_id'], 'integer'],
            [['created_on', 'last_used_on'], 'safe'],
            [['provider', 'identifier'], 'string', 'max' => 100],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'provider' => 'Provider',
            'identifier' => 'Identifier',
            'created_on' => 'Created On',
            'last_used_on' => 'Last Used On',
        ];
    }
}
