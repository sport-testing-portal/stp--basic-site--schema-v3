<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwTestResult2]].
 *
 * @see VwTestResult2
 */
class VwTestResult2Query extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwTestResult2[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwTestResult2|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
