<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserUsedPassword]].
 *
 * @see UserUsedPassword
 */
class UserUsedPasswordQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UserUsedPassword[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserUsedPassword|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
