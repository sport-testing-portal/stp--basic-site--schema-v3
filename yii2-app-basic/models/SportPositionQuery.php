<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SportPosition]].
 *
 * @see SportPosition
 */
class SportPositionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SportPosition[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SportPosition|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
