<?php

namespace app\models;

use Yii;
use \app\models\base\EventCombineItem as BaseEventCombineItem;

/**
 * This is the model class for table "event_combine_item".
 */
class EventCombineItem extends BaseEventCombineItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['event_combine_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'event_combine_item_id' => 'Event Combine Item ID',
            'event_combine_id' => 'Event Combine ID',
            'lock' => 'Lock',
        ];
    }
}
