<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetadataCodebaseModel]].
 *
 * @see MetadataCodebaseModel
 */
class MetadataCodebaseModelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataCodebaseModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataCodebaseModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
