<?php

namespace app\models;

use Yii;
use \app\models\base\VwCoachTeamAssignmentStatusSummary as BaseVwCoachTeamAssignmentStatusSummary;

/**
 * This is the model class for table "vwCoach_Team_Assignment_Status_Summary".
 */
class VwCoachTeamAssignmentStatusSummary extends BaseVwCoachTeamAssignmentStatusSummary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status_cnt'], 'integer'],
            [['coach_team_assignment_status'], 'string', 'max' => 10],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'status_cnt' => 'Status Cnt',
            'coach_team_assignment_status' => 'Coach Team Assignment Status',
        ];
    }
}
