<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PlayerContact]].
 *
 * @see PlayerContact
 */
class PlayerContactQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PlayerContact[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PlayerContact|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
