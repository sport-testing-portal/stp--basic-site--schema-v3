<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataApiClassXlat as BaseMetadataApiClassXlat;

/**
 * This is the model class for table "metadata__api_class_xlat"
 * @since 0.8.0.
 */
class MetadataApiClassXlat extends BaseMetadataApiClassXlat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['metadata__api_class_id_1', 'metadata__api_class_id_2'], 'required'],
            [['metadata__api_class_id_1', 'metadata__api_class_id_2', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['metadata__api_class_xlat'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'metadata__api_class_xlat_id' => 'API Class Xlat ID',
            'metadata__api_class_id_1' => 'API Class Id 1',
            'metadata__api_class_id_2' => 'API Class Id 2',
            'metadata__api_class_xlat' => 'API Class Xlat',
            'lock' => 'Lock',
        ];
    }
}
