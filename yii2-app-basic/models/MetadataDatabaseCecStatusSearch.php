<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataDatabaseCecStatus;

/**
 * app\models\MetadataDatabaseCecStatusSearch represents the model behind the search form about `app\models\MetadataDatabaseCecStatus`.
 */
 class MetadataDatabaseCecStatusSearch extends MetadataDatabaseCecStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cec_status_id', 'database_id', 'database_cec_id', 'created_by', 'updated_by'], 'integer'],
            [['cec_status', 'cec_status_tag', 'cec_status_bfr', 'cec_status_at', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataDatabaseCecStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'cec_status_id' => $this->cec_status_id,
            'database_id' => $this->database_id,
            'database_cec_id' => $this->database_cec_id,
            'cec_status_at' => $this->cec_status_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'cec_status', $this->cec_status])
            ->andFilterWhere(['like', 'cec_status_tag', $this->cec_status_tag])
            ->andFilterWhere(['like', 'cec_status_bfr', $this->cec_status_bfr])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
