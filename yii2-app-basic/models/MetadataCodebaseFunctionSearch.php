<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataCodebaseFunction;

/**
 * app\models\MetadataCodebaseFunctionSearch represents the model behind the search form about `app\models\MetadataCodebaseFunction`.
 */
 class MetadataCodebaseFunctionSearch extends MetadataCodebaseFunction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_function_id', 'created_by', 'updated_by'], 'integer'],
            [['codebase_function', 'codebase_function_short_desc', 'codebase_function_long_desc', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataCodebaseFunction::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codebase_function_id' => $this->codebase_function_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'codebase_function', $this->codebase_function])
            ->andFilterWhere(['like', 'codebase_function_short_desc', $this->codebase_function_short_desc])
            ->andFilterWhere(['like', 'codebase_function_long_desc', $this->codebase_function_long_desc])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
