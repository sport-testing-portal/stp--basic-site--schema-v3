<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Itemchildren]].
 *
 * @see Itemchildren
 */
class ItemchildrenQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Itemchildren[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Itemchildren|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
