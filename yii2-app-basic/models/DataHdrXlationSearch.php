<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataHdrXlation;

/**
 * app\models\DataHdrXlationSearch represents the model behind the search form about `app\models\DataHdrXlation`.
 */
 class DataHdrXlationSearch extends DataHdrXlation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_hdr_xlation_id', 'created_by', 'updated_by'], 'integer'],
            [['data_hdr_xlation', 'data_hdr_entity_name', 'hdr_sha1_hash', 'data_hdr_regex', 'data_hdr_is_trash_yn', 'gsm_target_table_name', 'data_hdr_entity_fetch_url', 'data_hdr_xlation_visual_example', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataHdrXlation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'data_hdr_xlation_id' => $this->data_hdr_xlation_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'data_hdr_xlation', $this->data_hdr_xlation])
            ->andFilterWhere(['like', 'data_hdr_entity_name', $this->data_hdr_entity_name])
            ->andFilterWhere(['like', 'hdr_sha1_hash', $this->hdr_sha1_hash])
            ->andFilterWhere(['like', 'data_hdr_regex', $this->data_hdr_regex])
            ->andFilterWhere(['like', 'data_hdr_is_trash_yn', $this->data_hdr_is_trash_yn])
            ->andFilterWhere(['like', 'gsm_target_table_name', $this->gsm_target_table_name])
            ->andFilterWhere(['like', 'data_hdr_entity_fetch_url', $this->data_hdr_entity_fetch_url])
            ->andFilterWhere(['like', 'data_hdr_xlation_visual_example', $this->data_hdr_xlation_visual_example])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
