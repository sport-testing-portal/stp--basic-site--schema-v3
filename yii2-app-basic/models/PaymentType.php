<?php

namespace app\models;

use Yii;
use \app\models\base\PaymentType as BasePaymentType;

/**
 * This is the model class for table "payment_type".
 */
class PaymentType extends BasePaymentType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['payment_type_allocation_template_id', 'created_by', 'updated_by'], 'integer'],
            [['payment_type'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['payment_type'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['payment_type'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'payment_type_id' => 'Payment Type ID',
            'payment_type_allocation_template_id' => 'Payment Type Allocation Template ID',
            'payment_type' => 'Payment Type',
            'lock' => 'Lock',
        ];
    }
}
