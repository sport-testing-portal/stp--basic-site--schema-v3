<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetadataDatabaseCec]].
 *
 * @see MetadataDatabaseCec
 */
class MetadataDatabaseCecQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataDatabaseCec[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataDatabaseCec|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
