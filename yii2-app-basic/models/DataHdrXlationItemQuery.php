<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DataHdrXlationItem]].
 *
 * @see DataHdrXlationItem
 */
class DataHdrXlationItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return DataHdrXlationItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DataHdrXlationItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
