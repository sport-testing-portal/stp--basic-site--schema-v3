<?php

namespace app\models;

use Yii;
use \app\models\base\TeamPlayer as BaseTeamPlayer;

/**
 * This is the model class for table "team_player".
 */
class TeamPlayer extends BaseTeamPlayer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['team_id', 'player_id'], 'required'],
            [['team_id', 'player_id', 'team_play_sport_position_id', 'team_play_sport_position2_id', 'coach_id', 'created_by', 'updated_by'], 'integer'],
            [['team_play_begin_dt', 'team_play_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['primary_position', 'coach_name'], 'string', 'max' => 45],
            [['team_play_statistical_highlights'], 'string', 'max' => 300],
            [['team_play_current_team'], 'string', 'max' => 4],
            [['lock'], 'string', 'max' => 1],
            [['team_id', 'player_id'], 'unique', 'targetAttribute' => ['team_id', 'player_id'], 'message' => 'The combination of Team ID and Player ID has already been taken.'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'team_player_id' => 'Team Player ID',
            'team_id' => 'Team ID',
            'player_id' => 'Player ID',
            'team_play_sport_position_id' => 'Team Play Sport Position ID',
            'team_play_sport_position2_id' => 'Team Play Sport Position2 ID',
            'primary_position' => 'Primary Position',
            'team_play_statistical_highlights' => 'Team Play Statistical Highlights',
            'coach_id' => 'Coach ID',
            'coach_name' => 'Coach Name',
            'team_play_current_team' => 'Team Play Current Team',
            'team_play_begin_dt' => 'Team Play Begin Dt',
            'team_play_end_dt' => 'Team Play End Dt',
            'lock' => 'Lock',
        ];
    }
}
