<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppChangeLog;

/**
 * app\models\AppChangeLogSearch represents the model behind the search form about `app\models\AppChangeLog`.
 */
 class AppChangeLogSearch extends AppChangeLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['app_change_log_id', 'created_by', 'updated_by'], 'integer'],
            [['app_semantic_version', 'app_change_desc', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppChangeLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'app_change_log_id' => $this->app_change_log_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'app_semantic_version', $this->app_semantic_version])
            ->andFilterWhere(['like', 'app_change_desc', $this->app_change_desc])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
