<?php

namespace app\models;

use Yii;
use \app\models\base\SchoolAct as BaseSchoolAct;

/**
 * This is the model class for table "school_act".
 */
class SchoolAct extends BaseSchoolAct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['school_id', 'school_unit_id', 'school_act_composite_25_pct', 'school_act_composite_75_pct', 'school_act_english_25_pct', 'school_act_english_75_pct', 'school_act_math_25_pct', 'school_act_math_75_pct', 'school_act_student_submit_cnt', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['school_act_student_submit_pct'], 'string', 'max' => 5],
            [['school_act_report_period'], 'string', 'max' => 12],
            [['lock'], 'string', 'max' => 1],
            [['school_unit_id'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'school_act_id' => 'School Act ID',
            'school_id' => 'School ID',
            'school_unit_id' => 'School Unit ID',
            'school_act_composite_25_pct' => 'School Act Composite 25 Pct',
            'school_act_composite_75_pct' => 'School Act Composite 75 Pct',
            'school_act_english_25_pct' => 'School Act English 25 Pct',
            'school_act_english_75_pct' => 'School Act English 75 Pct',
            'school_act_math_25_pct' => 'School Act Math 25 Pct',
            'school_act_math_75_pct' => 'School Act Math 75 Pct',
            'school_act_student_submit_cnt' => 'School Act Student Submit Cnt',
            'school_act_student_submit_pct' => 'School Act Student Submit Pct',
            'school_act_report_period' => 'School Act Report Period',
            'lock' => 'Lock',
        ];
    }
}
