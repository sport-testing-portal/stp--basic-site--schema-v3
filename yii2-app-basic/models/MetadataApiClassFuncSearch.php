<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataApiClassFunc;

/**
 * app\models\MetadataApiClassFuncSearch represents the model behind the search form about `app\models\MetadataApiClassFunc`.
 */
 class MetadataApiClassFuncSearch extends MetadataApiClassFunc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metadata__api_class_func_id', 'metadata__api_class_id', 'created_by', 'updated_by'], 'integer'],
            [['metadata__api_class_func', 'metadata__api_class_func_desc', 'metadata__api_class_func_docs_url', 'metadata__api_class_func_reference_example', 'metadata__api_class_func_regex_find', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataApiClassFunc::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'metadata__api_class_func_id' => $this->metadata__api_class_func_id,
            'metadata__api_class_id' => $this->metadata__api_class_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'metadata__api_class_func', $this->metadata__api_class_func])
            ->andFilterWhere(['like', 'metadata__api_class_func_desc', $this->metadata__api_class_func_desc])
            ->andFilterWhere(['like', 'metadata__api_class_func_docs_url', $this->metadata__api_class_func_docs_url])
            ->andFilterWhere(['like', 'metadata__api_class_func_reference_example', $this->metadata__api_class_func_reference_example])
            ->andFilterWhere(['like', 'metadata__api_class_func_regex_find', $this->metadata__api_class_func_regex_find])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
