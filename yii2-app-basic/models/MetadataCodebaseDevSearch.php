<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataCodebaseDev;

/**
 * app\models\MetadataCodebaseDevSearch represents the model behind the search form about `app\models\MetadataCodebaseDev`.
 */
 class MetadataCodebaseDevSearch extends MetadataCodebaseDev
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_dev_id', 'created_by', 'updated_by'], 'integer'],
            [['codebase_dev', 'codebase_dev_desc_short', 'codebase_dev_desc_long', 'dev_status_tag', 'dev_scope', 'rule_file_uri', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataCodebaseDev::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codebase_dev_id' => $this->codebase_dev_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'codebase_dev', $this->codebase_dev])
            ->andFilterWhere(['like', 'codebase_dev_desc_short', $this->codebase_dev_desc_short])
            ->andFilterWhere(['like', 'codebase_dev_desc_long', $this->codebase_dev_desc_long])
            ->andFilterWhere(['like', 'dev_status_tag', $this->dev_status_tag])
            ->andFilterWhere(['like', 'dev_scope', $this->dev_scope])
            ->andFilterWhere(['like', 'rule_file_uri', $this->rule_file_uri])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
