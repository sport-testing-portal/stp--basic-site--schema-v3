<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataCodebaseMvcModel as BaseMetadataCodebaseMvcModel;

/**
 * This is the model class for table "metadata__codebase_mvc_model".
 */
class MetadataCodebaseMvcModel extends BaseMetadataCodebaseMvcModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['codebase_id'], 'required'],
            [['codebase_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['json_data_structure', 'json_data_sample', 'json_data_example'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'codebase_mvc_model_id' => 'Codebase Mvc Model ID',
            'codebase_id' => 'Codebase ID',
            'json_data_structure' => 'Json Data Structure',
            'json_data_sample' => 'Json Data Sample',
            'json_data_example' => 'Json Data Example',
            'lock' => 'Lock',
        ];
    }
}
