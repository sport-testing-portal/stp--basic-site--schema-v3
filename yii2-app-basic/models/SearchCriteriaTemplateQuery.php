<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SearchCriteriaTemplate]].
 *
 * @see SearchCriteriaTemplate
 */
class SearchCriteriaTemplateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SearchCriteriaTemplate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SearchCriteriaTemplate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
