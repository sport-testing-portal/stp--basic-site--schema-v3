<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetadataDatabaseDevStatus]].
 *
 * @see MetadataDatabaseDevStatus
 */
class MetadataDatabaseDevStatusQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataDatabaseDevStatus[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataDatabaseDevStatus|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
