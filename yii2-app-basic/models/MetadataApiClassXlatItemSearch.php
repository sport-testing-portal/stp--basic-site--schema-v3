<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataApiClassXlatItem;

/**
 * app\models\MetadataApiClassXlatItemSearch represents the model behind the search form about `app\models\MetadataApiClassXlatItem`.
 */
 class MetadataApiClassXlatItemSearch extends MetadataApiClassXlatItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metadata__api_class_xlat_item_id', 'metadata__api_class_xlat_id', 'created_by', 'updated_by'], 'integer'],
            [['metadata__api_class_xlat_item', 'metadata__api_class_func_1_reference_example', 'metadata__api_class_func_2_reference_example', 'metadata__api_class_func_1_regex_find', 'metadata__api_class_func_2_regex_find', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataApiClassXlatItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'metadata__api_class_xlat_item_id' => $this->metadata__api_class_xlat_item_id,
            'metadata__api_class_xlat_id' => $this->metadata__api_class_xlat_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'metadata__api_class_xlat_item', $this->metadata__api_class_xlat_item])
            ->andFilterWhere(['like', 'metadata__api_class_func_1_reference_example', $this->metadata__api_class_func_1_reference_example])
            ->andFilterWhere(['like', 'metadata__api_class_func_2_reference_example', $this->metadata__api_class_func_2_reference_example])
            ->andFilterWhere(['like', 'metadata__api_class_func_1_regex_find', $this->metadata__api_class_func_1_regex_find])
            ->andFilterWhere(['like', 'metadata__api_class_func_2_regex_find', $this->metadata__api_class_func_2_regex_find])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
