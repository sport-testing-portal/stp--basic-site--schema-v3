<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AgeGroup]].
 *
 * @see AgeGroup
 */
class AgeGroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AgeGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AgeGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
