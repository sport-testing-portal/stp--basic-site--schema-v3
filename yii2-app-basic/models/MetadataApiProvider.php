<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataApiProvider as BaseMetadataApiProvider;

/**
 * This is the model class for table "metadata__api_provider"
 * @since 0.8.0.
 */
class MetadataApiProvider extends BaseMetadataApiProvider
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['metadata__api_provider'], 'string', 'max' => 75],
            [['metadata__api_provider_desc'], 'string', 'max' => 150],
            [['metadata__api_provider_url'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'metadata__api_provider_id' => 'API Provider ID',
            'metadata__api_provider' => 'API Provider Name',
            'metadata__api_provider_desc' => 'API Provider Desc',
            'metadata__api_provider_url' => 'API Provider URL',
            'lock' => 'Lock',
        ];
    }
}
