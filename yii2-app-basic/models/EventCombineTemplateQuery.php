<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[EventCombineTemplate]].
 *
 * @see EventCombineTemplate
 */
class EventCombineTemplateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EventCombineTemplate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EventCombineTemplate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
