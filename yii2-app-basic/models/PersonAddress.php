<?php

namespace app\models;

use Yii;
use \app\models\base\PersonAddress as BasePersonAddress;

/**
 * This is the model class for table "person_address".
 */
class PersonAddress extends BasePersonAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_id', 'address_id'], 'required'],
            [['person_id', 'address_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person_id' => 'Person ID',
            'address_id' => 'Address ID',
            'lock' => 'Lock',
        ];
    }
}
