<?php

namespace app\models;

use Yii;
use \app\models\base\ReligiousAffiliation as BaseReligiousAffiliation;

/**
 * This is the model class for table "religious_affiliation".
 */
class ReligiousAffiliation extends BaseReligiousAffiliation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['religious_affiliation'], 'string', 'max' => 85],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'religious_affiliation_id' => 'Religious Affiliation ID',
            'religious_affiliation' => 'Religious Affiliation',
            'lock' => 'Lock',
        ];
    }
}
