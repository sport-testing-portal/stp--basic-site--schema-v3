<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AppUserRemoteIdentity]].
 *
 * @see AppUserRemoteIdentity
 */
class AppUserRemoteIdentityQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AppUserRemoteIdentity[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AppUserRemoteIdentity|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
