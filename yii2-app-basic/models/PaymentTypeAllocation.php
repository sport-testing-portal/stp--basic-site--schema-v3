<?php

namespace app\models;

use Yii;
use \app\models\base\PaymentTypeAllocation as BasePaymentTypeAllocation;

/**
 * This is the model class for table "payment_type_allocation".
 */
class PaymentTypeAllocation extends BasePaymentTypeAllocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['payment_type_allocation_id'], 'required'],
            [['payment_type_allocation_id', 'payment_type_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['payment_type_allocation'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'payment_type_allocation_id' => 'Payment Type Allocation ID',
            'payment_type_id' => 'Payment Type ID',
            'payment_type_allocation' => 'Payment Type Allocation',
            'lock' => 'Lock',
        ];
    }
}
