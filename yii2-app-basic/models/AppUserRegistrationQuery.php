<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AppUserRegistration]].
 *
 * @see AppUserRegistration
 */
class AppUserRegistrationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AppUserRegistration[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AppUserRegistration|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
