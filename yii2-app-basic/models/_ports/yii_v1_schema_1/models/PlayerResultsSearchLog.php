<?php

Yii::import('application.models._base.BasePlayerResultsSearchLog');

class PlayerResultsSearchLog extends BasePlayerResultsSearchLog
{
    /**
     * @return PlayerResultsSearchLog
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Coach/Scout Search Log|Coach/Scout Searches Log', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

	protected function beforeValidate() {
		// remove chars from numeric fields
		$regex	='/\D(\d*)/i'; // U17 becomes 17
		$matches=[];
		if (! empty($this->age_range_min)){
			if (preg_match($regex, $this->age_range_min, $matches) !== false){
				if (isset($matches[1])){
					$this->age_range_min = (int)$matches[1];
				}
			}
		}
		if (! empty($this->age_range_max)){
			if (preg_match($regex, $this->age_range_max, $matches) !== false){
				if (isset($matches[1])){
					$this->age_range_max = (int)$matches[1];
				}
			}
		}
		return parent::beforeValidate();
	}

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
		// remove chars from numeric fields
		$regex	='/\D(\d*)/i'; // U17 becomes 17
		$matches=[];
		if (! empty($this->age_range_min)){
			if (preg_match($regex, $this->age_range_min, $matches) !== false){
				if (isset($matches[1])){
					$this->age_range_min = (int)$matches[1];
				}
			}
		}
		if (! empty($this->age_range_max)){
			if (preg_match($regex, $this->age_range_max, $matches) !== false){
				if (isset($matches[1])){
					$this->age_range_max = (int)$matches[1];
				}
			}
		}
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'player_results_search_log_id' => Yii::t('app', 'Coach/Scout Search ID'),
                'user_id' => Yii::t('app', 'User'),
                'person_id' => Yii::t('app', 'Person'),
                'row_count' => Yii::t('app', 'Row Count'),
                'gender' => Yii::t('app', 'Gender'),
                'age_range_min' => Yii::t('app', 'Age Range Min'),
                'age_range_max' => Yii::t('app', 'Age Range Max'),
                'first_name' => Yii::t('app', 'First Name'),
                'last_name' => Yii::t('app', 'Last Name'),
                'test_provider' => Yii::t('app', 'Test Provider'),
                'team' => Yii::t('app', 'Team'),
                'position' => Yii::t('app', 'Position'),
                'test_type' => Yii::t('app', 'Test Type'),
                'test_category' => Yii::t('app', 'Test Category'),
                'test_description' => Yii::t('app', 'Test Description'),
                'country' => Yii::t('app', 'Country'),
                'state_province' => Yii::t('app', 'State Province'),
                'zip_code' => Yii::t('app', 'Zip Code'),
                'zip_code_radius' => Yii::t('app', 'Zip Code Radius'),
                'city' => Yii::t('app', 'City'),
                'city_radius' => Yii::t('app', 'City Radius'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'person' => null,
                'user' => null,
        );
    }


}
