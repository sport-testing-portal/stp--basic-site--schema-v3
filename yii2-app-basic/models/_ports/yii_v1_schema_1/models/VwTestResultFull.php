<?php

Yii::import('application.models._base.BaseVwTestResultFull');

/**
 * Extends the super-view to enable data saves to any table
 * @todo update fetchTargetModelFieldNameXlation() so it works on this view
 * @category data-IO +read +write
 * @example fetch test results by distance
 *	fetchByDistance($origLat, $origLong, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N')
 * @example
 *  Creates a multi-table field-value array (mtfva)
 *  $mtfva = self::generateModelValuesArray($view_field_value_array);
 *         calls: self::fetchTargetModelFieldNameXlation($view_field_value_array)
 * @internal Add athlete methods to this model then merge in VwTestResult methods later.
 * @internal   Consider merging the views and models at a later version number
 */
class VwTestResultFull extends BaseVwTestResultFull
{
    /**
     * @return VwTestResultFull
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Test Result|Test Results', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        //return $this->tableName() . '_id';
		return 'test_eval_detail_log_id';
    }


    /**
	 * Prevent a save to the view itself by returning false
	 * @return boolean false
	 */
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        //return parent::beforeSave();
		return false; // data updates disallowed
    }

	/**
	 * Select data by distance
	 * @param decimal $origLat
	 * @param decimal $origLong
	 * @param decimal $maxDistance
	 * @param string $rtnKilometers
	 * @param string $rtnAssocArrYN
	 * @return CActiveRecord | array[]
	 * @internal An index optimized select calcs in 0.034 secs and fetches dataset in 0.005 secs
	 * @see: BaseModel::convertPostalCodeToLatLong()
	 * @version 0.1.0 base code copied from models.VwPlayer->fetchByDistance()
	 * @internal Development Status = ready for testing (rft)
	 */
	protected function fetchByDistance($origLat, $origLong, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {

		if ($rtnKilometers == 'Y'){
			$distance_return_type_factor = KILOMETER_FACTOR;
		} else {
			$distance_return_type_factor = MILES_FACTOR;
		}

		$distance_calc = "$distance_return_type_factor * DEGREES(ACOS(COS(RADIANS(latpoint)) "
			."* COS(RADIANS(latitude)) "
			."* COS(RADIANS(longpoint) - RADIANS(longitude)) "
			." + SIN(RADIANS(latpoint)) "
			." * SIN(RADIANS(latitude)))) AS distance ";

        $query = ""
        . "select c.*  "
        . "	from ".$this->tableName()." t "
        . "	inner join  "
        . "	( "
        . "	select postal_code, place_name, state_name, latitude, longitude, "
        . "	 $distance_calc "
        . "	 from zip_code  "
        . "	 join ( "
        . "		 select  $origLat  AS latpoint,  $origLong AS longpoint "
        . "	   ) AS p ON 1=1 "
        . "		where longitude  "
        . "		BETWEEN longpoint - ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "			AND longpoint + ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "		having distance_in_miles < $maxDistance "
        . "		order by distance_in_miles desc "
        . "	) z on left(t.person_postal_code,5) = z.postal_code "
        . "; "
        ;

		$cmd = Yii::app()->db->createCommand($query);
		if ($rtnAssocArrYN === 'Y'){
			return $cmd->queryAll($fetchAssociative=true);
		} else {
			return $cmd->queryAll();
		}
	}



	/**
	 * These are the table aliases pulled from
	 * @param type $alias
	 * @return string|array[]
	 * @example tableAliases('zcd') returns 'zip_code_distinct'
	 * @example tableAliases() returns all aliases[]
	 */
	protected function tableAliases($alias=null) {

		$aliasList=[
			'per'	=>'person',
			'tesl'	=>'test_eval_summary_log',
			'pt'	=>'person_type',
			'g'		=>'gender',
			'tedl'	=>'test_eval_detail_log',
			'tedtd'	=>'test_eval_detail_test_desc',
			'tet'	=>'test_eval_type',
			'tec'	=>'test_eval_category',
			'tep'	=>'test_eval_provider',
			'pl'	=>'player',
			'sp'	=>'sport_position',
			'ag'	=>'age_group',
			'country'=>'country',
			'zcd'	=>'zip_code_distinct',

		];
		if (!is_null($alias) && isset($alias) && array_key_exists($alias, $aliasList) !== false){
			return $aliasList[$alias];
		} else {
			return $aliasList;
		}

	}

	/**
	 * Made to bulk assign view attribute values to base model attribute values
	 * @param array[] $field_value_array
	 * @version 4.5.0 synced with vwPlayer v4.5.0
	 * @todo must pull field list from the view to compare. this source pulled from VwPlayer and not updated yet 03/17/2015
	 * @internal Development Status = code construction
	 */
	public static function fetchTargetModelFieldNameXlation($field_value_array) {


		//return null; // @todo must validate the fields below using sql view definition

		//$fva = $field_value_array;
		$readonly_attr	= self::fetchReadOnlyAttributes(); // attributes like 'person_age' that are calculated values and can't be written back to the database
		$fva			= array_diff_key( $field_value_array, array_flip( $readonly_attr) ); // remove readonly fields
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($field_value_array, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fva, 10, true);
		//$field_list = array_keys($fva);
		$xlat=[
			'id'=>['model'=>'TestEvalDetailLog','attribute'=>'test_eval_detail_log_id','table'=>'test_eval_detail_log',],

			'person'					=> ['model'=>'Person',	'attribute'=>'person_name_full'],
			//,pt.person_type_name                         as ptype			// readonly
			//,g.gender_code                               as gender_code	// readonly
			'tedtd_display_order'			=>['model'=>'TestEvalDetailTestDesc',
					'attribute'=>'test_eval_detail_test_desc_display_order','table'=>'test_eval_detail_test_desc',] ,
			'test_desc'=>['model'=>'TestEvalDetailTestDesc','attribute'=>'test_eval_detail_test_desc',],
			'test_type'=>['model'=>'TestEvalCategory','attribute'=>'test_eval_category_name',],

			'test_category' =>['model'=>'TestEvalType','attribute'=>'test_eval_type_name','table'=>'test_eval_type'],
			'test_category2'=>['model'=>'TestEvalType','attribute'=>'test_eval_type_name_sub_type','table'=>'test_eval_type'],

			'split'=>['model'=>'TestEvalDetailLog',
					'attribute'=>'test_eval_detail_score_text','table'=>'test_eval_detail_log',] ,
			//'score'=>	round(tedl.test_eval_detail_score_num, 3)   // readonly
			'test_units'=>['model'=>'TestEvalDetailTestDesc','attribute'=>'units',],
			'attempt'=>['model'=>'TestEvalDetailLog',
					'attribute'=>'test_eval_detail_attempt','table'=>'test_eval_detail_log',] ,
			'trial_status'=>['model'=>'TestEvalDetailLog',
					'attribute'=>'test_eval_detail_trial_status','table'=>'test_eval_detail_log',] ,
			'overall_ranking'=>['model'=>'TestEvalDetailLog',
					'attribute'=>'test_eval_detail_overall_ranking','table'=>'test_eval_detail_log',] ,
			'positional_ranking'=>['model'=>'TestEvalDetailLog',
					'attribute'=>'test_eval_detail_positional_ranking','table'=>'test_eval_detail_log',] ,
			'total_overall_ranking'=>['model'=>'TestEvalSummaryLog',
					'attribute'=>'test_eval_overall_ranking','table'=>'test_eval_summary_log',] ,
			'total_positional_ranking'=>['model'=>'TestEvalSummaryLog',
					'attribute'=>'test_eval_positional_ranking','table'=>'test_eval_summary_log',] ,

			'score_url'=>['model'=>'TestEvalDetailLog',
					'attribute'=>'test_eval_detail_score_url','table'=>'test_eval_detail_log',] ,
			'video_url'=>['model'=>'TestEvalDetailLog',
					'attribute'=>'test_eval_detail_video_url','table'=>'test_eval_detail_log',] ,


			'test_date'=>['model'=>'TestEvalDetailLog',
					'attribute'=>'created_dt','table'=>'test_eval_detail_log','formula'=>"DATE_FORMAT(tedl.created_dt,'%b %d %Y')"] ,
			'test_date_iso'=>['model'=>'TestEvalDetailLog',
					'attribute'=>'created_dt','table'=>'test_eval_detail_log',] ,

			// RO 	,tep.test_eval_provider_name                 as tester

			'person_name_first'=>['model'=>'Person',
					'attribute'=>'person_name_first','table'=>'person',] ,
			'person_name_last'=>['model'=>'Person',
					'attribute'=>'person_name_last','table'=>'person',] ,
			'person_city'=>['model'=>'Person',
					'attribute'=>'person_city','table'=>'person',] ,
			'person_country_code'=>['model'=>'Person',
					'attribute'=>'person_country_code','table'=>'person',] ,
			'person_state_or_region'=>['model'=>'Person',
					'attribute'=>'person_state_or_region','table'=>'person',] ,
			'person_postal_code'=>['model'=>'Person',
					'attribute'=>'person_postal_code','table'=>'person',] ,

			'person_name_first'=>['model'=>'Person',
					'attribute'=>'person_name_first','table'=>'person',] ,
		];

		// RO? 	,pl.player_sport_position_id                 as player_sport_position_id
		// RO? 	,sp.sport_position_name                      as sport_position_name
		// RO?	,g.gender_desc                               as gender_desc
		// RO?	,g.gender_id 	                             as gender_id
		// RO?	,ag.age_group_name                           as age_group_name
		// RO?	,ag.age_group_id                             as age_group_id
		// RO?	,ag.age_group_min                            as age_group_min
		// RO?	,ag.age_group_max                            as age_group_max
		// RO?	,country.long_name                           as country_long_name
		// RO?	,zcd.latitude                                as latitude
		// RO?	,zcd.longitude                               as longitude
		// RO?	,per.person_id
		// RO?	,pl.player_id
		// RO?	,tesl.test_eval_summary_log_id
		// RO?	,tedl.test_eval_detail_log_id
		// RO?	,tedl.test_eval_detail_source_event_id       as source_event_id


/*
		// 'person_age`
		'team_name'							=> ['model'=>'Team',	'attribute'=>'team_name'],
		//'age_group_name' 					=> ['model'=>'AgeGroup','attribute'=>''],
		'person_city'						=> ['model'=>'Person',	'attribute'=>'person_city'],
		'person_state_or_region'			=> ['model'=>'Person',	'attribute'=>'person_state_or_region'],
		'gsm_staff'							=> ['model'=>'User',	'attribute'=>'first_name'],
		'player_id'							=> ['model'=>'Player',	'attribute'=>'player_id'],
		'person_id'							=> ['model'=>'Person',	'attribute'=>'person_id'],
		// confirmed
		'player_team_player_id'				=> ['model'=>'Player','attribute'=>'player_team_player_id'],
		'player_access_code'				=> ['model'=>'Player','attribute'=>'player_access_code'],
		'player_waiver_minor_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_minor_dt'],
		'player_waiver_adult_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_adult_dt'],
		'player_parent_email'				=> ['model'=>'Player','attribute'=>'player_parent_email'],
		'player_sport_preference'			=> ['model'=>'Player','attribute'=>'player_sport_preference'],
		'player_sport_position_preference'	=> ['model'=>'Player','attribute'=>'player_sport_position_preference'],
		'player_shot_side_preference' 		=> ['model'=>'Player','attribute'=>'player_shot_side_preference'],
		// confirmed
		'org_id'							=> ['model'=>'Person','attribute'=>'org_id'],
		'app_user_id'						=> ['model'=>'Person','attribute'=>'app_user_id'],
		'person_name_prefix'				=> ['model'=>'Person','attribute'=>'person_name_prefix'],
		'person_name_first'					=> ['model'=>'Person','attribute'=>'person_name_first'],
		'person_name_middle'				=> ['model'=>'Person','attribute'=>'person_name_middle'],
		'person_name_last'					=> ['model'=>'Person','attribute'=>'person_name_last'],
		'person_name_suffix'				=> ['model'=>'Person','attribute'=>'person_name_suffix'],
		'person_phone_personal'				=> ['model'=>'Person','attribute'=>'person_phone_personal'],
		'person_email_personal'				=> ['model'=>'Person','attribute'=>'person_email_personal'],
		'person_phone_work'					=> ['model'=>'Person','attribute'=>'person_phone_work'],
		'person_email_work'					=> ['model'=>'Person','attribute'=>'person_email_work'],
		'person_position_work'				=> ['model'=>'Person','attribute'=>'person_position_work'],
		'person_image_headshot_url' 		=> ['model'=>'Person','attribute'=>'person_image_headshot_url'],
		'person_name_nickname'				=> ['model'=>'Person','attribute'=>'person_name_nickname'],
		'person_date_of_birth'				=> ['model'=>'Person','attribute'=>'person_date_of_birth'],
		'person_height'						=> ['model'=>'Person','attribute'=>'person_height'],
		'person_weight'						=> ['model'=>'Person','attribute'=>'person_weight'],
		'person_tshirt_size'				=> ['model'=>'Person','attribute'=>'person_tshirt_size'],
		'person_addr_1'						=> ['model'=>'Person','attribute'=>'person_addr_1'],
		'person_addr_2'						=> ['model'=>'Person','attribute'=>'person_addr_2'],
		'person_addr_3'						=> ['model'=>'Person','attribute'=>'person_addr_3'],
		'person_postal_code'				=> ['model'=>'Person','attribute'=>'person_postal_code'],
		'person_country'					=> ['model'=>'Person','attribute'=>'person_country'],
		'person_country_code'				=> ['model'=>'Person','attribute'=>'person_country_code'],
		'person_type_id'					=> ['model'=>'Person','attribute'=>'person_type_id'],
		//'person_type_name'				=> ['model'=>'PersonType','attribute'=>'person_type_name'],  // readonly
		//'person_type_desc_short'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_short'],  // readonly
		//'person_type_desc_long'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_long'],  // readonly
		//'gender_desc'						=> ['model'=>'Gender',	'attribute'=>'gender_desc'], // readonly
		//'gender_code'						=> ['model'=>'Gender',	'attribute'=>'gender_code'], // readonly
		'org_type_id'						=> ['model'=>'Org','attribute'=>'org_type_id'],
		'org_name'							=> ['model'=>'Org','attribute'=>'org_name'],
		'org_type_name'						=> ['model'=>'OrgType','attribute'=>'org_type_name'],
		'org_website_url'					=> ['model'=>'Org','attribute'=>'org_website_url'],
		'org_twitter_url'					=> ['model'=>'Org','attribute'=>'org_twitter_url'],
		'org_facebook_url'					=> ['model'=>'Org','attribute'=>'org_facebook_url'],
		'org_phone_main'					=> ['model'=>'Org','attribute'=>'org_phone_main'],
		'org_email_main'					=> ['model'=>'Org','attribute'=>'org_email_main'],
		'org_addr1'							=> ['model'=>'Org','attribute'=>'org_addr1'],
		'org_addr2'							=> ['model'=>'Org','attribute'=>'org_addr2'],
		'org_addr3'							=> ['model'=>'Org','attribute'=>'org_addr3'],
		'org_city'							=> ['model'=>'Org','attribute'=>'org_city'],
		'org_state_or_region'				=> ['model'=>'Org','attribute'=>'org_state_or_region'],
		'org_postal_code'					=> ['model'=>'Org','attribute'=>'org_postal_code'],
		'org_country_code_iso3'				=> ['model'=>'Org','attribute'=>'org_country_code_iso3'],
		// confirmed
		'team_player_id'					=> ['model'=>'TeamPlayer','attribute'=>'team_player_id'],
		'team_id'							=> ['model'=>'TeamPlayer','attribute'=>'team_id'],
		'team_play_primary_position'		=> ['model'=>'TeamPlayer','attribute'=>'primary_position'],
		'team_play_begin_dt'				=> ['model'=>'TeamPlayer','attribute'=>'team_play_begin_dt'],
		'team_play_end_dt'					=> ['model'=>'TeamPlayer','attribute'=>'team_play_end_dt'],
		'team_play_coach_id'				=> ['model'=>'TeamPlayer','attribute'=>'coach_id'],
		'team_play_coach_name'				=> ['model'=>'TeamPlayer','attribute'=>'coach_name'],
		'team_play_created_dt'				=> ['model'=>'TeamPlayer','attribute'=>'created_dt'],
		'team_play_updated_dt'				=> ['model'=>'TeamPlayer','attribute'=>'updated_dt'],
		// confirmed
		'team_org_id'						=> ['model'=>'Team','attribute'=>'org_id'],
		'team_school_id'					=> ['model'=>'Team','attribute'=>'school_id'],
		'team_sport_id'						=> ['model'=>'Team','attribute'=>'sport_id'],
		'team_camp_id'						=> ['model'=>'Team','attribute'=>'camp_id'],
		'team_gender_id'					=> ['model'=>'Team','attribute'=>'gender_id'],
		'age_group_id'						=> ['model'=>'Team','attribute'=>'age_group_id'],
		'team_gender'						=> ['model'=>'Team','attribute'=>'team_gender'],
		'team_division'						=> ['model'=>'Team','attribute'=>'team_division'],
		//'team_age_group_depreciated' 		=> ['model'=>'Team','attribute'=>''],
		];
*/
		return $xlat;
	}


	public static function fetchUsingAttemptSort($returnArray=true){
		$sql = BaseModel::sqlTrim("select a.test_desc, a.person, a.attempt1, a.attempt2, a.attempt3
			, b.score_avg, b.score_min, b.score_max, a.ratings_bias
			,case when a.ratings_bias = 'H' then b.score_max else b.score_min end as player_best
			from (
				select
						tr1.test_desc, tr1.person, concat(tr1.test_desc,'-',tr1.person) as rowkey,
						sum(case when tr1.attempt = 1 then tr1.score else null end) as attempt1,
						sum(case when tr1.attempt = 2 then tr1.score else null end) as attempt2,
						sum(case when tr1.attempt = 3 then tr1.score else null end) as attempt3,
						tr1.ratings_bias
				from test_result tr1
				where tr1.event_id = 'one' and tr1.attempt between 1 and 3
				group by tr1.test_desc, tr1.person
				order by tr1.test_desc, tr1.person
				) a
				join (
					select
						tr.test_desc, tr.person, count(*) as cnt, round(avg(score),3) as score_avg,
						round(min(score),3) as score_min , round(max(score),3) as score_max,
						concat(tr.test_desc,'-',tr.person) as rowkey

					from test_result tr
					where tr.event_id = 'one' and attempt between 1 and 3
					group by tr.test_desc, tr.person
					order by event_id, test_desc, person, test_result_id
				) b
				on a.rowkey = b.rowkey;
			}");
		if ($returnArray == true){
			$cmd = Yii::app()->db->createCommand($sql);
			$result = $cmd->queryAll($fetchAssociative=true);
		} else {
			$result = VwTestResultFull::model()->findAll($sql);
		}
		$debug_local = true;
		if ($debug_local == true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($result, 10, true);
		}
		return $result;
	}

	/**
	 *
	 * @param type $view_field_value_array
	 * @return Bmfva array[]
	 */
	public static function generateModelValuesArray($view_field_value_array) {
		$xlat	= self::fetchTargetModelFieldNameXlation($view_field_value_array);
		$rtn	= [];
		//$view_fields_passed_in = array_keys($$view_field_values_array);
		foreach ($view_field_value_array as $view_field_name => $view_field_value) {
			if(array_key_exists($view_field_name, $xlat)){
				//$models[] = $xlat[$view_field_name]['model'];
				$rtn[$xlat[$view_field_name]['model']][$xlat[$view_field_name]['attribute']]
						= $view_field_value;
			}
		}
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rtn, 10, true);
		return $rtn;
	}

	/**
	 * Returns an array of attributes like 'person_age' that are calculated values and can't be written back to the database
	 * @param type $field_value_array
	 */
	protected static function fetchReadOnlyAttributes() {
		//		$readonly_attr[]; //
		// return ['person_age', 'person_dob_eng', 'person_bday_long', 'person_height_imperial','person_bmi',];
		$readonly_attr[] = 'person_age';
		$readonly_attr[] = 'person_dob_eng';
		$readonly_attr[] = 'person_bday_long';
		$readonly_attr[] = 'person_height_imperial';
		$readonly_attr[] = 'person_bmi';
		$readonly_attr[] = 'age_group_name';
		$readonly_attr[] = 'person_type_name';		// ['model'=>'PersonType','attribute'=>'person_type_name'],  // readonly
		$readonly_attr[] = 'person_type_desc_short'; // ['model'=>'PersonType','attribute'=>'person_type_desc_short'],  // readonly
		$readonly_attr[] = 'person_type_desc_long';	//	=> ['model'=>'PersonType','attribute'=>'person_type_desc_long'],  // readonly
		$readonly_attr[] = 'gender_desc';			//	=> ['model'=>'Gender','attribute'=>'gender_desc'], // readonly
		$readonly_attr[] = 'gender_code';			//	=> ['model'=>'Gender','attribute'=>'gender_code'], // readonly
		$readonly_attr[] = 'gsm_staff';				//	=> ['model'=>'User','attribute'=>'first_name'], // readonly
		return $readonly_attr;
	}

	/**
	 * Saves a specific model the view is composed of.
	 * @todo Assumes that all models referenced have a model->Store() method
	 * The record is inserted as a row into the database table if its {@link isNewRecord}
	 * property is true (usually the case when the record is created using the 'new'
	 * operator). Otherwise, it will be used to update the corresponding row in the table
	 * (usually the case if the record is obtained using one of those 'find' methods.)
	 *
	 * Validation will be performed before saving the record. If the validation fails,
	 * the record will not be saved. You can call {@link getErrors()} to retrieve the
	 * validation errors.
	 *
	 * If the record is saved via insertion, its {@link isNewRecord} property will be
	 * set false, and its {@link scenario} property will be set to be 'update'.
	 * And if its primary key is auto-incremental and is not set before insertion,
	 * the primary key will be populated with the automatically generated key value.
	 *
	 * @param string | array $models
	 * @param boolean $runValidation whether to perform validation before saving the record.
	 * If the validation fails, the record will not be saved to database.
	 * @param array $attributes list of attributes that need to be saved. Defaults to null,
	 * meaning all attributes that are loaded from DB will be saved.
	 * @return boolean whether the saving succeeds
	 */
	public function saveModels($models='all', $runValidation=true,$attributes=null)
	{
		$filter = null;
		// <editor-fold defaultstate="collapsed" desc="parse attributes param">
		if (! is_null($attributes)){
			// filter specific attributes
			if (is_array($attributes) && count($attributes) > 0){
				// loop
				foreach ($attributes as $attribute) {
					$filter[] = $attribute;
				}
			} elseif (is_string($attributes) && strlen($attributes) > 0){
				// filter items passed in
				if (stripos($attributes, ',') !== false){
					// parse string
					$fields = explode(',', $attributes);
					foreach ($fields as $field){
						$filter[] = $field;
					}
				} else {
					// its a non-delimited string
					$filter[] = $attributes;
				}
			}
		}
		// </editor-fold>

		$vwAttr	= $this->attributes;  // Get this model's attribute values
		$mtfva	= self::generateModelValuesArray($vwAttr); // data envelope array

		if ($models == 'all'){
			foreach ($mtfva as $model => $fva) { // mtfva = multi-table fva
				$model_filter[] = $model;
			}
		} elseif(is_array($models)){
			$model_filter = $models;
		}
		foreach ($mfva as $model => $fva) {
			if(in_array($model, $model_filter)){
				// process model
				$model::store($fva);
			}
		}
//		if(!$runValidation || $this->validate($attributes))
//			return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
//		else
//			return false;
	}


    /**
	 * Override the gii generated labels in BaseVwTestResultFull
	 * @version CEC CActiveRecord::atrributeLables() mods
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
		// capture any new fields added at the gii generated level
		$generated_labels = parent::attributeLabels(); // gii generated

		$filtered_labels = array(
                'id' => Yii::t('app', 'ID'),
                'person' => Yii::t('app', 'Person'),
                'ptype' => Yii::t('app', 'Ptype'),
                'gender_code' => Yii::t('app', 'Gender Code'),
                'rating_bias' => Yii::t('app', 'Rating Bias'),
                'tedtd_display_order' => Yii::t('app', 'Tedtd Display Order'),
                'test_desc' => Yii::t('app', 'Test Desc'),
                'test_type' => Yii::t('app', 'Test Type'),
                'test_category' => Yii::t('app', 'Test Category'),
                'test_category2' => Yii::t('app', 'Test Category2'),
                'split' => Yii::t('app', 'Split'),
                'score' => Yii::t('app', 'Score'),
                'test_units' => Yii::t('app', 'Test Units'),
                'attempt' => Yii::t('app', 'Attempt'),
                'trial_status' => Yii::t('app', 'Trial Status'),
                'overall_ranking' => Yii::t('app', 'Overall Ranking'),
                'positional_ranking' => Yii::t('app', 'Positional Ranking'),
                'total_overall_ranking' => Yii::t('app', 'Total Overall Ranking'),
                'total_positional_ranking' => Yii::t('app', 'Total Positional Ranking'),
                'score_url' => Yii::t('app', 'Score Url'),
                'video_url' => Yii::t('app', 'Video Url'),
                'test_date' => Yii::t('app', 'Test Date'),
                'test_date_iso' => Yii::t('app', 'Test Date ISO'),
                'tester' => Yii::t('app', 'Tester'),
                'person_name_first' => Yii::t('app', 'Person Name First'),
                'person_name_last' => Yii::t('app', 'Person Name Last'),
                'person_city' => Yii::t('app', 'Person City'),
                'person_country_code' => Yii::t('app', 'Person Country Code'),
                'person_state_or_region' => Yii::t('app', 'Person State Or Region'),
                'person_postal_code' => Yii::t('app', 'Person Postal Code'),
                'player_sport_position_id' => Yii::t('app', 'Player Sport Position'),
                'sport_position_name' => Yii::t('app', 'Sport Position Name'),
                'gender_desc' => Yii::t('app', 'Gender Desc'),
                'gender_id' => Yii::t('app', 'Gender'),
                'age_group_name' => Yii::t('app', 'Age Group Name'),
                'age_group_id' => Yii::t('app', 'Age Group'),
                'age_group_min' => Yii::t('app', 'Age Group Min'),
                'age_group_max' => Yii::t('app', 'Age Group Max'),
                'country_long_name' => Yii::t('app', 'Country Long Name'),
                'latitude' => Yii::t('app', 'Latitude'),
                'longitude' => Yii::t('app', 'Longitude'),
                'person_id' => Yii::t('app', 'Person'),
                'player_id' => Yii::t('app', 'Player'),
                'test_eval_summary_log_id' => Yii::t('app', 'Test Summary ID'),
                'test_eval_detail_log_id' => Yii::t('app', 'Test Detail ID'),
                'source_event_id' => Yii::t('app', 'Source Event ID'),
        );

		$out = array_merge($generated_labels, $filtered_labels);
		return $out;

    }

	/**
	 * Parse view creation SQL to pull table aliases and field mappings
	 * @param type $sql_text
	 * @internal Development Status = ready for testing (rft)
	 */
	protected function parseViewCreationSql($sql_text) {
		$local_debug = true;
		if ($local_debug){
			echo "<pre>$sql_text</pre>";
		}
		// regex mods; m=match ends of lines eg. $=EOL
		/** @see http://php.net/manual/en/reference.pcre.pattern.modifiers.php */
		$regex = '/\s+(.+?)\.(.+?)\s+as\s(.+?)$/im';
		// table alias, table field, field alias
		$matches = [];
		$a = preg_match_all($regex, $sql_text, $matches, PREG_SET_ORDER);
		$cnt = count($a);
		if ($local_debug){
			echo "<br><b>$cnt matches </b>"."<br>";
		}
		$sql=['table alias'=>[], 'table field'=>[], 'field alias'=>[],];

		foreach ($matches as $key => $match) {
			if ($local_debug){
				$msg = "key = $key";
				YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($match, 10, true);
			}
			// $sql
			$sql=['table alias'=>[$match[0]], 'table field'=>[$match[1]], 'field alias'=>[$match[2]],];
		}
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($sql, 15, true);

	}


	/**
	 * Get the most recent source event id (test mates ID number)
	 * @param string|int $person_id
	 */
	public static function fetchMostRecentSummaryRowKeyByPersonId($person_id)
	{
		// select all summary row ids via team_coach linkage

		// -- Get all summary ids of an athlete's test mates
		// select @player_id := 309;
		// select @person_id := 326; -- demo athlete
		$sql = \BaseModel::sqlTrim("
			select
				tedl.test_eval_detail_source_event_id as source_event_id
				from test_eval_summary_log tesl
					inner join person per
						on tesl.person_id = per.person_id
					inner join test_eval_detail_log tedl
						on tesl.test_eval_summary_log_id = tedl.test_eval_summary_log_id
				where tesl.person_id = :person_id
				order by tedl.created_dt desc
				limit 1
			");
		$params = [':person_id' =>$person_id];
		$cmd = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryRow($fetchAssociative=true, $params);
		if (is_array($data) && array_key_exists('source_event_id', $data) ){
			return $data['source_event_id'];
		} else {
			return false;
		}
//		select * from vwTestResultFull
//			where source_event_id = @source_event_id
//			order by person;

	}

	/**
	 * Get the most recent source event id (test mates ID number)
	 * @param string|int $person_id
	 */
	public function fetchMostRecentSummaryRowKeyByPersonId__testHarness()
	{
		$attributes = ['person_name_full'=>'Demo Athlete'];
		$Person = Person::model()->findByAttributes($attributes);

		if (! is_null($Person)){
			$person_id = $Person->getPrimaryKey();
		}

		$source_event_id = VwTestResultFull::fetchMostRecentSummaryRowKeyByPersonId($person_id);
		if ($source_event_id){
			return $source_event_id;
		}
	}


	/**
	 *
	 * @param string|int $player_id
	 * @param string|int $person_id
	 */
//	public function fetchSummaryRowKeysByPersonlayerId($player_id=null, $person_id=null)
//	{
//		// select all summary row ids via team_coach linkage
//
//		// -- Get all summary ids of an athlete's test mates
//		// select @player_id := 309;
//		// select @person_id := 326; -- demo athlete
//		$sql = BaseModel::sqlTrim("
//			select
//				tedl.test_eval_detail_source_event_id
//				from test_eval_summary_log tesl
//					inner join person per
//						on tesl.person_id = per.person_id
//					inner join test_eval_detail_log tedl
//						on tesl.test_eval_summary_log_id = tedl.test_eval_summary_log_id
//				where tesl.person_id = :person_id
//				order by tedl.created_dt desc
//				limit 1
//			";
//
//		select * from vwTestResultFull
//			where source_event_id = @source_event_id
//			order by person;
//
//	}


	/**
	 *
	 * @param string|int $player_id
	 * @param string|int $person_id
	 */
	public function fetchSummaryRowKeysByCoachId($player_id=null, $person_id=null)
	{
		// select all summary row ids via team_coach linkage
	}
}
