<?php

/**
 * This is the model base class for the table "player_sport".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PlayerSport".
 *
 * Columns in table "player_sport" available as properties of the model,
 * followed by relations of table "player_sport" available as properties of the model.
 *
 * @property integer $player_sport_id
 * @property integer $player_id
 * @property integer $sport_id
 * @property integer $sport_position_id
 * @property integer $gender_id
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Gender $gender
 * @property Player $player
 * @property Sport $sport
 * @property SportPosition $sportPosition
 */
abstract class BasePlayerSport extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'player_sport';
    }

    public static function representingColumn() {
        return 'created_dt';
    }

    public function rules() {
        return array(
            array(	'player_id, sport_id, sport_position_id, gender_id, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'created_dt, updated_dt',
					'safe'
			),
            array('player_id, sport_id, sport_position_id, gender_id, created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('player_sport_id, player_id, sport_id, sport_position_id, gender_id, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'gender' => array(self::BELONGS_TO, 'Gender', 'gender_id'),
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
            'sport' => array(self::BELONGS_TO, 'Sport', 'sport_id'),
            'sportPosition' => array(self::BELONGS_TO, 'SportPosition', 'sport_position_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'player_sport_id' => Yii::t('app', 'ID'),
                'player_id' => Yii::t('app', 'Player'),
                'sport_id' => Yii::t('app', 'Sport'),
                'sport_position_id' => Yii::t('app', 'Sport Position'),
                'gender_id' => Yii::t('app', 'Gender'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'gender' => null,
                'player' => null,
                'sport' => null,
                'sportPosition' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('player_sport_id', $this->player_sport_id);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('sport_id', $this->sport_id);
        $criteria->compare('sport_position_id', $this->sport_position_id);
        $criteria->compare('gender_id', $this->gender_id);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}