<?php

/**
 * This is the model base class for the table "team_player".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "TeamPlayer".
 *
 * Columns in table "team_player" available as properties of the model,
 * followed by relations of table "team_player" available as properties of the model.
 *
 * @property integer $team_player_id
 * @property integer $team_id
 * @property integer $player_id
 * @property integer $team_play_sport_position_id
 * @property integer $team_play_sport_position2_id
 * @property string $primary_position
 * @property string $team_play_statistical_highlights
 * @property integer $coach_id
 * @property string $coach_name
 * @property integer $team_play_current_team
 * @property string $team_play_begin_dt
 * @property string $team_play_end_dt
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Player[] $players
 * @property Player $player
 * @property Team $team
 * @property SportPosition $teamPlaySportPosition
 */
abstract class BaseTeamPlayer extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'team_player';
    }

    public static function representingColumn() {
        return 'primary_position';
    }

    public function rules() {
        return array(
            array(	'team_id, player_id',
					'required',
					'message' => Yii::t('app', 'Field is required')
			),
            array(	'team_id, player_id, team_play_sport_position_id, team_play_sport_position2_id, coach_id, team_play_current_team, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
//            array(	'primary_position, coach_name',
//					'length',
//					'max'=>45,
//					'tooLong' => Yii::t('app', 'Field is required')
//			),
            array(	'team_play_statistical_highlights',
					'length',
					'max'=>300,
					'tooLong' => Yii::t('app', 'Field is required')
			),
			// manually added coach_id, coach_name to the list of safe attributes to update
            array(	'coach_id, coach_name, team_play_begin_dt, team_play_end_dt, created_dt, updated_dt',

					'safe'
			),
            array('team_play_sport_position_id, team_play_sport_position2_id, primary_position, team_play_statistical_highlights, coach_id, coach_name, team_play_current_team, team_play_begin_dt, team_play_end_dt, created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('team_player_id, team_id, player_id, team_play_sport_position_id, team_play_sport_position2_id, primary_position, team_play_statistical_highlights, coach_id, coach_name, team_play_current_team, team_play_begin_dt, team_play_end_dt, created_dt, updated_dt, created_by, updated_by',
				'safe',
				'on'=>'search'
			),
			// Add custom rules
			array('team_id, player_id',
				'safe',
				'except'=>'update'
			),
			array('team_id, player_id',
				'unsafe',
				'on'=>'update'
			),
        );
    }

    public function relations() {
        return array(
            'players' => array(self::HAS_MANY, 'Player', 'player_team_player_id'),
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
            'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
            'teamPlaySportPosition' => array(self::BELONGS_TO, 'SportPosition', 'team_play_sport_position_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'team_player_id' => Yii::t('app', 'Team Player'),
                'team_id' => Yii::t('app', 'Team'),
                'player_id' => Yii::t('app', 'Player'),
                'team_play_sport_position_id' => Yii::t('app', 'Team Play Sport Position'),
                'team_play_sport_position2_id' => Yii::t('app', 'Team Play Sport Position2'),
                'primary_position' => Yii::t('app', 'Primary Position'),
                'team_play_statistical_highlights' => Yii::t('app', 'Team Play Statistical Highlights'),
                'coach_id' => Yii::t('app', 'Coach'),
                'coach_name' => Yii::t('app', 'Coach Name'),
                'team_play_current_team' => Yii::t('app', 'Team Play Current Team'),
                'team_play_begin_dt' => Yii::t('app', 'Team Play Begin Dt'),
                'team_play_end_dt' => Yii::t('app', 'Team Play End Dt'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'players' => null,
                'player' => null,
                'team' => null,
                'teamPlaySportPosition' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('team_player_id', $this->team_player_id);
        $criteria->compare('team_id', $this->team_id);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('team_play_sport_position_id', $this->team_play_sport_position_id);
        $criteria->compare('team_play_sport_position2_id', $this->team_play_sport_position2_id);
        $criteria->compare('primary_position', $this->primary_position, true);
        $criteria->compare('team_play_statistical_highlights', $this->team_play_statistical_highlights, true);
        $criteria->compare('coach_id', $this->coach_id);
        $criteria->compare('coach_name', $this->coach_name, true);
        $criteria->compare('team_play_current_team', $this->team_play_current_team);
        $criteria->compare('team_play_begin_dt', $this->team_play_begin_dt, true);
        $criteria->compare('team_play_end_dt', $this->team_play_end_dt, true);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}