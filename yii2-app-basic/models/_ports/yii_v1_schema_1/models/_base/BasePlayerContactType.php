<?php

/**
 * This is the model base class for the table "player_contact_type".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PlayerContactType".
 *
 * Columns in table "player_contact_type" available as properties of the model,
 * followed by relations of table "player_contact_type" available as properties of the model.
 *
 * @property integer $player_contact_type_id
 * @property string $player_contact_type_name
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property PlayerContact $playerContact
 * @property PlayerContact[] $playerContacts
 */
abstract class BasePlayerContactType extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'player_contact_type';
    }

    public static function representingColumn() {
        return 'player_contact_type_name';
    }

    public function rules() {
        return array(
            array(	'player_contact_type_name',
					'required',
					'message' => Yii::t('app', 'Field is required')
			),
            array(	'created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'player_contact_type_name',
					'length',
					'max'=>45,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_dt, updated_dt',
					'safe'
			),
            array('created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('player_contact_type_id, player_contact_type_name, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'playerContact' => array(self::HAS_ONE, 'PlayerContact', 'player_contact_id'),
            'playerContacts' => array(self::HAS_MANY, 'PlayerContact', 'player_contact_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'player_contact_type_id' => Yii::t('app', 'ID'),
                'player_contact_type_name' => Yii::t('app', 'Name'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'playerContact' => null,
                'playerContacts' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('player_contact_type_id', $this->player_contact_type_id);
        $criteria->compare('player_contact_type_name', $this->player_contact_type_name, true);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}