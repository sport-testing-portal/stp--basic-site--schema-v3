<?php

/**
 * This is the model base class for the table "school_stat".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SchoolStat".
 *
 * Columns in table "school_stat" available as properties of the model,
 * followed by relations of table "school_stat" available as properties of the model.
 *
 * @property integer $school_stat_id
 * @property integer $school_id
 * @property integer $college_cost_rating
 * @property integer $college_cost_tuition_instate
 * @property integer $college_cost_tuition_outofstate
 * @property integer $college_cost_room_and_board
 * @property integer $college_cost_books_and_supplies
 * @property string $college_early_decision_dt
 * @property string $college_early_action_dt
 * @property string $college_regular_decision_dt
 * @property integer $college_pct_applicants_admitted
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property School $school
 */
abstract class BaseSchoolStat extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'school_stat';
    }

    public static function representingColumn() {
        return 'college_early_decision_dt';
    }

    public function rules() {
        return array(
            array(	'school_id, college_cost_rating, college_cost_tuition_instate, college_cost_tuition_outofstate, college_cost_room_and_board, college_cost_books_and_supplies, college_pct_applicants_admitted, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'college_early_decision_dt, college_early_action_dt, college_regular_decision_dt, created_dt, updated_dt',
					'safe'
			),
            array('school_id, college_cost_rating, college_cost_tuition_instate, college_cost_tuition_outofstate, college_cost_room_and_board, college_cost_books_and_supplies, college_early_decision_dt, college_early_action_dt, college_regular_decision_dt, college_pct_applicants_admitted, created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('school_stat_id, school_id, college_cost_rating, college_cost_tuition_instate, college_cost_tuition_outofstate, college_cost_room_and_board, college_cost_books_and_supplies, college_early_decision_dt, college_early_action_dt, college_regular_decision_dt, college_pct_applicants_admitted, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'school' => array(self::BELONGS_TO, 'School', 'school_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'school_stat_id' => Yii::t('app', 'ID'),
                'school_id' => Yii::t('app', 'School'),
                'college_cost_rating' => Yii::t('app', 'College Cost Rating'),
                'college_cost_tuition_instate' => Yii::t('app', 'College Cost Tuition Instate'),
                'college_cost_tuition_outofstate' => Yii::t('app', 'College Cost Tuition Outofstate'),
                'college_cost_room_and_board' => Yii::t('app', 'College Cost Room And Board'),
                'college_cost_books_and_supplies' => Yii::t('app', 'College Cost Books And Supplies'),
                'college_early_decision_dt' => Yii::t('app', 'College Early Decision Dt'),
                'college_early_action_dt' => Yii::t('app', 'College Early Action Dt'),
                'college_regular_decision_dt' => Yii::t('app', 'College Regular Decision Dt'),
                'college_pct_applicants_admitted' => Yii::t('app', 'College Pct Applicants Admitted'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'school' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('school_stat_id', $this->school_stat_id);
        $criteria->compare('school_id', $this->school_id);
        $criteria->compare('college_cost_rating', $this->college_cost_rating);
        $criteria->compare('college_cost_tuition_instate', $this->college_cost_tuition_instate);
        $criteria->compare('college_cost_tuition_outofstate', $this->college_cost_tuition_outofstate);
        $criteria->compare('college_cost_room_and_board', $this->college_cost_room_and_board);
        $criteria->compare('college_cost_books_and_supplies', $this->college_cost_books_and_supplies);
        $criteria->compare('college_early_decision_dt', $this->college_early_decision_dt, true);
        $criteria->compare('college_early_action_dt', $this->college_early_action_dt, true);
        $criteria->compare('college_regular_decision_dt', $this->college_regular_decision_dt, true);
        $criteria->compare('college_pct_applicants_admitted', $this->college_pct_applicants_admitted);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}