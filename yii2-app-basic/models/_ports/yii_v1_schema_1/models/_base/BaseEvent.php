<?php

/**
 * This is the model base class for the table "event".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Event".
 *
 * Columns in table "event" available as properties of the model,
 * followed by relations of table "event" available as properties of the model.
 *
 * @property integer $event_id
 * @property integer $event_type_id
 * @property string $event_name
 * @property string $event_access_code
 * @property string $event_location_name
 * @property string $event_location_map_url
 * @property string $event_scheduled_dt
 * @property string $event_participation_fee
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property EventType $eventType
 * @property EventAttendeeLog[] $eventAttendeeLogs
 * @property EventCombine[] $eventCombines
 */
abstract class BaseEvent extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'event';
    }

    public static function representingColumn() {
        return 'event_name';
    }

    public function rules() {
        return array(
            array(	'event_type_id, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'event_name',
					'length',
					'max'=>150,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'event_access_code',
					'length',
					'max'=>25,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'event_location_name',
					'length',
					'max'=>100,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'event_location_map_url',
					'length',
					'max'=>175,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'event_participation_fee',
					'length',
					'max'=>6,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'event_scheduled_dt, created_dt, updated_dt',
					'safe'
			),
            array('event_type_id, event_name, event_access_code, event_location_name, event_location_map_url, event_scheduled_dt, event_participation_fee, created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('event_id, event_type_id, event_name, event_access_code, event_location_name, event_location_map_url, event_scheduled_dt, event_participation_fee, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'eventType' => array(self::BELONGS_TO, 'EventType', 'event_type_id'),
            'eventAttendeeLogs' => array(self::HAS_MANY, 'EventAttendeeLog', 'event_id'),
            'eventCombines' => array(self::HAS_MANY, 'EventCombine', 'event_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'event_id' => Yii::t('app', 'ID'),
                'event_type_id' => Yii::t('app', 'Type'),
                'event_name' => Yii::t('app', 'Event Name'),
                'event_access_code' => Yii::t('app', 'Access Code'),
                'event_location_name' => Yii::t('app', 'Location Name'),
                'event_location_map_url' => Yii::t('app', 'Location Map Url'),
                'event_scheduled_dt' => Yii::t('app', 'Scheduled Dt'),
                'event_participation_fee' => Yii::t('app', 'Participation Fee'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'eventType' => null,
                'eventAttendeeLogs' => null,
                'eventCombines' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('event_id', $this->event_id);
        $criteria->compare('event_type_id', $this->event_type_id);
        $criteria->compare('event_name', $this->event_name, true);
        $criteria->compare('event_access_code', $this->event_access_code, true);
        $criteria->compare('event_location_name', $this->event_location_name, true);
        $criteria->compare('event_location_map_url', $this->event_location_map_url, true);
        $criteria->compare('event_scheduled_dt', $this->event_scheduled_dt, true);
        $criteria->compare('event_participation_fee', $this->event_participation_fee, true);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}