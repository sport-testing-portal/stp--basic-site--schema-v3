<?php

/**
 * This is the model base class for the table "data_hdr_xlation_item".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "DataHdrXlationItem".
 *
 * Columns in table "data_hdr_xlation_item" available as properties of the model,
 * followed by relations of table "data_hdr_xlation_item" available as properties of the model.
 *
 * @property integer $data_hdr_xlation_item_id
 * @property integer $data_hdr_xlation_id
 * @property string $data_hdr_xlation_item_source_value
 * @property string $data_hdr_xlation_item_target_value
 * @property integer $ordinal_position_num
 * @property string $data_hdr_xlation_item_target_table
 * @property string $data_hdr_source_value_type_name_source
 * @property integer $data_hdr_source_value_is_type_name
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property DataHdrXlation $dataHdrXlation
 */
abstract class BaseDataHdrXlationItem extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'data_hdr_xlation_item';
    }

    public static function representingColumn() {
        return 'data_hdr_xlation_item_source_value';
    }

    public function rules() {
        return array(
            array(	'data_hdr_xlation_id, ordinal_position_num, data_hdr_source_value_is_type_name, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'data_hdr_xlation_item_source_value, data_hdr_xlation_item_target_value',
					'length',
					'max'=>75,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'data_hdr_xlation_item_target_table, data_hdr_source_value_type_name_source',
					'length',
					'max'=>150,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_dt, updated_dt',
					'safe'
			),
            array('data_hdr_xlation_id, data_hdr_xlation_item_source_value, data_hdr_xlation_item_target_value, ordinal_position_num, data_hdr_xlation_item_target_table, data_hdr_source_value_type_name_source, data_hdr_source_value_is_type_name, created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('data_hdr_xlation_item_id, data_hdr_xlation_id, data_hdr_xlation_item_source_value, data_hdr_xlation_item_target_value, ordinal_position_num, data_hdr_xlation_item_target_table, data_hdr_source_value_type_name_source, data_hdr_source_value_is_type_name, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'dataHdrXlation' => array(self::BELONGS_TO, 'DataHdrXlation', 'data_hdr_xlation_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'data_hdr_xlation_item_id' => Yii::t('app', 'ID'),
                'data_hdr_xlation_id' => Yii::t('app', 'Hdr Xlation Name'),
                'data_hdr_xlation_item_source_value' => Yii::t('app', 'Source Value'),
                'data_hdr_xlation_item_target_value' => Yii::t('app', 'Target Value'),
                'ordinal_position_num' => Yii::t('app', 'Ordinal Position'),
                'data_hdr_xlation_item_target_table' => Yii::t('app', 'Target Table'),
                'data_hdr_source_value_type_name_source' => Yii::t('app', 'Type Name Source'),
                'data_hdr_source_value_is_type_name' => Yii::t('app', 'Value Is Type Name'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'dataHdrXlation' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('data_hdr_xlation_item_id', $this->data_hdr_xlation_item_id);
        $criteria->compare('data_hdr_xlation_id', $this->data_hdr_xlation_id);
        $criteria->compare('data_hdr_xlation_item_source_value', $this->data_hdr_xlation_item_source_value, true);
        $criteria->compare('data_hdr_xlation_item_target_value', $this->data_hdr_xlation_item_target_value, true);
        $criteria->compare('ordinal_position_num', $this->ordinal_position_num);
        $criteria->compare('data_hdr_xlation_item_target_table', $this->data_hdr_xlation_item_target_table, true);
        $criteria->compare('data_hdr_source_value_type_name_source', $this->data_hdr_source_value_type_name_source, true);
        $criteria->compare('data_hdr_source_value_is_type_name', $this->data_hdr_source_value_is_type_name);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}