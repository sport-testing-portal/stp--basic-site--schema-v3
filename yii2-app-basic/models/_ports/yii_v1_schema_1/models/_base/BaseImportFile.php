<?php

/**
 * This is the model base class for the table "import_file".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ImportFile".
 *
 * Columns in table "import_file" available as properties of the model,
 * followed by relations of table "import_file" available as properties of the model.
 *
 * @property integer $import_file_id
 * @property string $import_file_source_file_name
 * @property string $import_file_source_file_date
 * @property integer $import_file_source_file_size
 * @property string $import_file_source_file_sha256
 * @property string $import_file_source_file_password
 * @property string $import_file_target_file_name
 * @property string $import_file_target_file_date
 * @property integer $import_file_target_file_size
 * @property string $import_file_target_file_sha256
 * @property string $import_file_target_file_password
 * @property string $import_file_description_short
 * @property string $import_file_description_long
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property ImportFileStiCoachReport[] $importFileStiCoachReports
 * @property ImportFileStiSpecialReport[] $importFileStiSpecialReports
 */
abstract class BaseImportFile extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'import_file';
    }

    public static function representingColumn() {
        return 'updated_dt';
    }

    public function rules() {
        return array(
            array(	'import_file_source_file_size, import_file_target_file_size, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'import_file_source_file_name, import_file_target_file_name',
					'length',
					'max'=>150,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'import_file_source_file_sha256, import_file_target_file_sha256',
					'length',
					'max'=>64,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'import_file_source_file_password, import_file_target_file_password',
					'length',
					'max'=>256,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'import_file_description_short',
					'length',
					'max'=>45,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'import_file_description_long',
					'length',
					'max'=>255,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'import_file_source_file_date, import_file_target_file_date, created_dt',
					'safe'
			),
            array('import_file_source_file_name, import_file_source_file_date, import_file_source_file_size, import_file_source_file_sha256, import_file_source_file_password, import_file_target_file_name, import_file_target_file_date, import_file_target_file_size, import_file_target_file_sha256, import_file_target_file_password, import_file_description_short, import_file_description_long, created_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('import_file_id, import_file_source_file_name, import_file_source_file_date, import_file_source_file_size, import_file_source_file_sha256, import_file_source_file_password, import_file_target_file_name, import_file_target_file_date, import_file_target_file_size, import_file_target_file_sha256, import_file_target_file_password, import_file_description_short, import_file_description_long, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'importFileStiCoachReports' => array(self::HAS_MANY, 'ImportFileStiCoachReport', 'import_file_id'),
            'importFileStiSpecialReports' => array(self::HAS_MANY, 'ImportFileStiSpecialReport', 'import_file_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'import_file_id' => Yii::t('app', 'Import File'),
                'import_file_source_file_name' => Yii::t('app', 'Import File Source File Name'),
                'import_file_source_file_date' => Yii::t('app', 'Import File Source File Date'),
                'import_file_source_file_size' => Yii::t('app', 'Import File Source File Size'),
                'import_file_source_file_sha256' => Yii::t('app', 'Import File Source File Sha256'),
                'import_file_source_file_password' => Yii::t('app', 'Import File Source File Password'),
                'import_file_target_file_name' => Yii::t('app', 'Import File Target File Name'),
                'import_file_target_file_date' => Yii::t('app', 'Import File Target File Date'),
                'import_file_target_file_size' => Yii::t('app', 'Import File Target File Size'),
                'import_file_target_file_sha256' => Yii::t('app', 'Import File Target File Sha256'),
                'import_file_target_file_password' => Yii::t('app', 'Import File Target File Password'),
                'import_file_description_short' => Yii::t('app', 'Import File Description Short'),
                'import_file_description_long' => Yii::t('app', 'Import File Description Long'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'importFileStiCoachReports' => null,
                'importFileStiSpecialReports' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('import_file_id', $this->import_file_id);
        $criteria->compare('import_file_source_file_name', $this->import_file_source_file_name, true);
        $criteria->compare('import_file_source_file_date', $this->import_file_source_file_date, true);
        $criteria->compare('import_file_source_file_size', $this->import_file_source_file_size);
        $criteria->compare('import_file_source_file_sha256', $this->import_file_source_file_sha256, true);
        $criteria->compare('import_file_source_file_password', $this->import_file_source_file_password, true);
        $criteria->compare('import_file_target_file_name', $this->import_file_target_file_name, true);
        $criteria->compare('import_file_target_file_date', $this->import_file_target_file_date, true);
        $criteria->compare('import_file_target_file_size', $this->import_file_target_file_size);
        $criteria->compare('import_file_target_file_sha256', $this->import_file_target_file_sha256, true);
        $criteria->compare('import_file_target_file_password', $this->import_file_target_file_password, true);
        $criteria->compare('import_file_description_short', $this->import_file_description_short, true);
        $criteria->compare('import_file_description_long', $this->import_file_description_long, true);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}