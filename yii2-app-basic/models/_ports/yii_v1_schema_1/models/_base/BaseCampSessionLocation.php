<?php

/**
 * This is the model base class for the table "camp_session_location".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CampSessionLocation".
 *
 * Columns in table "camp_session_location" available as properties of the model,
 * followed by relations of table "camp_session_location" available as properties of the model.
 *
 * @property integer $camp_session_location_id
 * @property integer $camp_session_id
 * @property integer $address_id
 * @property integer $season_id
 * @property integer $gender_id
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Address $address
 * @property CampSession $campSession
 * @property Gender $gender
 * @property Season $season
 */
abstract class BaseCampSessionLocation extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'camp_session_location';
    }

    public static function representingColumn() {
        return 'created_dt';
    }

    public function rules() {
        return array(
            array(	'camp_session_id, address_id, season_id, gender_id, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'created_dt, updated_dt',
					'safe'
			),
            array('camp_session_id, address_id, season_id, gender_id, created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('camp_session_location_id, camp_session_id, address_id, season_id, gender_id, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'address' => array(self::BELONGS_TO, 'Address', 'address_id'),
            'campSession' => array(self::BELONGS_TO, 'CampSession', 'camp_session_id'),
            'gender' => array(self::BELONGS_TO, 'Gender', 'gender_id'),
            'season' => array(self::BELONGS_TO, 'Season', 'season_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'camp_session_location_id' => Yii::t('app', 'ID'),
                'camp_session_id' => Yii::t('app', 'Camp Session'),
                'address_id' => Yii::t('app', 'Address'),
                'season_id' => Yii::t('app', 'Season'),
                'gender_id' => Yii::t('app', 'Gender'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'address' => null,
                'campSession' => null,
                'gender' => null,
                'season' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('camp_session_location_id', $this->camp_session_location_id);
        $criteria->compare('camp_session_id', $this->camp_session_id);
        $criteria->compare('address_id', $this->address_id);
        $criteria->compare('season_id', $this->season_id);
        $criteria->compare('gender_id', $this->gender_id);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}