<?php

/**
 * This is the model base class for the table "player_college_recruit".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PlayerCollegeRecruit".
 *
 * Columns in table "player_college_recruit" available as properties of the model,
 * followed by relations of table "player_college_recruit" available as properties of the model.
 *
 * @property integer $player_college_recruit_id
 * @property integer $player_id
 * @property integer $school_id
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Player $player
 * @property School $school
 */
abstract class BasePlayerCollegeRecruit extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'player_college_recruit';
    }

    public static function representingColumn() {
        return 'created_dt';
    }

    public function rules() {
        return array(
            array(	'player_id, school_id, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'created_dt, updated_dt',
					'safe'
			),
            array('player_id, school_id, created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('player_college_recruit_id, player_id, school_id, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
            'school' => array(self::BELONGS_TO, 'School', 'school_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'player_college_recruit_id' => Yii::t('app', 'ID'),
                'player_id' => Yii::t('app', 'Player'),
                'school_id' => Yii::t('app', 'School'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'player' => null,
                'school' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('player_college_recruit_id', $this->player_college_recruit_id);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('school_id', $this->school_id);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}