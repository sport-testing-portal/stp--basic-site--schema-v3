<?php

/**
 * This is the model base class for the table "player_academic".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PlayerAcademic".
 *
 * Columns in table "player_academic" available as properties of the model,
 * followed by relations of table "player_academic" available as properties of the model.
 *
 * @property integer $player_academic_id
 * @property integer $player_id
 * @property integer $sat_composite_score_max
 * @property integer $sat_critical_reading_score_max
 * @property integer $sat_math_score_max
 * @property integer $sat_writing_score_max
 * @property string $sat_scheduled_dt
 * @property integer $psat_composite_score_max
 * @property integer $psat_critical_reading_score_max
 * @property integer $psat_math_score_max
 * @property integer $psat_writing_score_max
 * @property integer $act_composite_score_max
 * @property integer $act_math_score_max
 * @property integer $act_english_score_max
 * @property string $act_scheduled_dt
 * @property string $grade_point_average
 * @property integer $class_rank
 * @property integer $school_size
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Player $player
 */
abstract class BasePlayerAcademic extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'player_academic';
    }

    public static function representingColumn() {
        return 'sat_scheduled_dt';
    }

    public function rules() {
        return array(
            array(	'player_id, sat_composite_score_max, sat_critical_reading_score_max, sat_math_score_max, sat_writing_score_max, psat_composite_score_max, psat_critical_reading_score_max, psat_math_score_max, psat_writing_score_max, act_composite_score_max, act_math_score_max, act_english_score_max, class_rank, school_size, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'grade_point_average',
					'length',
					'max'=>5,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'sat_scheduled_dt, act_scheduled_dt, created_dt, updated_dt',
					'safe'
			),
            array('player_id, sat_composite_score_max, sat_critical_reading_score_max, sat_math_score_max, sat_writing_score_max, sat_scheduled_dt, psat_composite_score_max, psat_critical_reading_score_max, psat_math_score_max, psat_writing_score_max, act_composite_score_max, act_math_score_max, act_english_score_max, act_scheduled_dt, grade_point_average, class_rank, school_size, created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('player_academic_id, player_id, sat_composite_score_max, sat_critical_reading_score_max, sat_math_score_max, sat_writing_score_max, sat_scheduled_dt, psat_composite_score_max, psat_critical_reading_score_max, psat_math_score_max, psat_writing_score_max, act_composite_score_max, act_math_score_max, act_english_score_max, act_scheduled_dt, grade_point_average, class_rank, school_size, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'player_academic_id' => Yii::t('app', 'ID'),
                'player_id' => Yii::t('app', 'Player'),
                'sat_composite_score_max' => Yii::t('app', 'Sat Composite Score Max'),
                'sat_critical_reading_score_max' => Yii::t('app', 'Sat Critical Reading Score Max'),
                'sat_math_score_max' => Yii::t('app', 'Sat Math Score Max'),
                'sat_writing_score_max' => Yii::t('app', 'Sat Writing Score Max'),
                'sat_scheduled_dt' => Yii::t('app', 'Sat Scheduled Dt'),
                'psat_composite_score_max' => Yii::t('app', 'Psat Composite Score Max'),
                'psat_critical_reading_score_max' => Yii::t('app', 'Psat Critical Reading Score Max'),
                'psat_math_score_max' => Yii::t('app', 'Psat Math Score Max'),
                'psat_writing_score_max' => Yii::t('app', 'Psat Writing Score Max'),
                'act_composite_score_max' => Yii::t('app', 'Act Composite Score Max'),
                'act_math_score_max' => Yii::t('app', 'Act Math Score Max'),
                'act_english_score_max' => Yii::t('app', 'Act English Score Max'),
                'act_scheduled_dt' => Yii::t('app', 'Act Scheduled Dt'),
                'grade_point_average' => Yii::t('app', 'Grade Point Average'),
                'class_rank' => Yii::t('app', 'Class Rank'),
                'school_size' => Yii::t('app', 'School Size'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'player' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('player_academic_id', $this->player_academic_id);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('sat_composite_score_max', $this->sat_composite_score_max);
        $criteria->compare('sat_critical_reading_score_max', $this->sat_critical_reading_score_max);
        $criteria->compare('sat_math_score_max', $this->sat_math_score_max);
        $criteria->compare('sat_writing_score_max', $this->sat_writing_score_max);
        $criteria->compare('sat_scheduled_dt', $this->sat_scheduled_dt, true);
        $criteria->compare('psat_composite_score_max', $this->psat_composite_score_max);
        $criteria->compare('psat_critical_reading_score_max', $this->psat_critical_reading_score_max);
        $criteria->compare('psat_math_score_max', $this->psat_math_score_max);
        $criteria->compare('psat_writing_score_max', $this->psat_writing_score_max);
        $criteria->compare('act_composite_score_max', $this->act_composite_score_max);
        $criteria->compare('act_math_score_max', $this->act_math_score_max);
        $criteria->compare('act_english_score_max', $this->act_english_score_max);
        $criteria->compare('act_scheduled_dt', $this->act_scheduled_dt, true);
        $criteria->compare('grade_point_average', $this->grade_point_average, true);
        $criteria->compare('class_rank', $this->class_rank);
        $criteria->compare('school_size', $this->school_size);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}