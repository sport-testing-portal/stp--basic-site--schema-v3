<?php

/**
 * This is the model base class for the table "sport".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Sport".
 *
 * Columns in table "sport" available as properties of the model,
 * followed by relations of table "sport" available as properties of the model.
 *
 * @property integer $sport_id
 * @property string $gsm_sport_name
 * @property string $sport_desc_short
 * @property string $sport_desc_long
 * @property string $high_school_yn
 * @property string $ncaa_yn
 * @property string $olympic_yn
 * @property string $xgame_sport_yn
 * @property string $ncaa_sport_name
 * @property string $olympic_sport_name
 * @property string $xgame_sport_name
 * @property string $ncaa_sport_type
 * @property string $ncaa_sport_season_male
 * @property string $ncaa_sport_season_female
 * @property string $ncaa_sport_season_coed
 * @property string $olympic_sport_season
 * @property string $xgame_sport_season
 * @property string $olympic_sport_gender
 * @property string $xgame_sport_gender
 * @property string $created_dt
 * @property string $updated_dt
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property CampSession[] $campSessions
 * @property CampSport[] $campSports
 * @property CoachSport[] $coachSports
 * @property PlayerSport[] $playerSports
 * @property SchoolSport[] $schoolSports
 * @property SportGender[] $sportGenders
 * @property SportPosition[] $sportPositions
 * @property Team[] $teams
 */
abstract class BaseSport extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'sport';
    }

    public static function representingColumn() {
        return 'gsm_sport_name';
    }

    public function rules() {
        return array(
            array(	'created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'gsm_sport_name, ncaa_sport_name, olympic_sport_name, xgame_sport_name',
					'length',
					'max'=>45,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'sport_desc_short',
					'length',
					'max'=>75,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'sport_desc_long',
					'length',
					'max'=>175,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'high_school_yn, ncaa_yn, olympic_yn, xgame_sport_yn',
					'length',
					'max'=>1,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'ncaa_sport_type, olympic_sport_season, xgame_sport_season, olympic_sport_gender, xgame_sport_gender',
					'length',
					'max'=>15,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'ncaa_sport_season_male, ncaa_sport_season_female, ncaa_sport_season_coed',
					'length',
					'max'=>30,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_dt, updated_dt',
					'safe'
			),
            array('gsm_sport_name, sport_desc_short, sport_desc_long, high_school_yn, ncaa_yn, olympic_yn, xgame_sport_yn, ncaa_sport_name, olympic_sport_name, xgame_sport_name, ncaa_sport_type, ncaa_sport_season_male, ncaa_sport_season_female, ncaa_sport_season_coed, olympic_sport_season, xgame_sport_season, olympic_sport_gender, xgame_sport_gender, created_dt, updated_dt, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('sport_id, gsm_sport_name, sport_desc_short, sport_desc_long, high_school_yn, ncaa_yn, olympic_yn, xgame_sport_yn, ncaa_sport_name, olympic_sport_name, xgame_sport_name, ncaa_sport_type, ncaa_sport_season_male, ncaa_sport_season_female, ncaa_sport_season_coed, olympic_sport_season, xgame_sport_season, olympic_sport_gender, xgame_sport_gender, created_dt, updated_dt, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'campSessions' => array(self::HAS_MANY, 'CampSession', 'sport_id'),
            'campSports' => array(self::HAS_MANY, 'CampSport', 'sport_id'),
            'coachSports' => array(self::HAS_MANY, 'CoachSport', 'sport_id'),
            'playerSports' => array(self::HAS_MANY, 'PlayerSport', 'sport_id'),
            'schoolSports' => array(self::HAS_MANY, 'SchoolSport', 'sport_id'),
            'sportGenders' => array(self::HAS_MANY, 'SportGender', 'sport_id'),
            'sportPositions' => array(self::HAS_MANY, 'SportPosition', 'sport_id'),
            'teams' => array(self::HAS_MANY, 'Team', 'sport_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'sport_id' => Yii::t('app', 'ID'),
                'gsm_sport_name' => Yii::t('app', 'Gsm Name'),
                'sport_desc_short' => Yii::t('app', 'Desc Short'),
                'sport_desc_long' => Yii::t('app', 'Desc Long'),
                'high_school_yn' => Yii::t('app', 'High School Yn'),
                'ncaa_yn' => Yii::t('app', 'Ncaa Yn'),
                'olympic_yn' => Yii::t('app', 'Olympic Yn'),
                'xgame_sport_yn' => Yii::t('app', 'Xgame Yn'),
                'ncaa_sport_name' => Yii::t('app', 'Ncaa Name'),
                'olympic_sport_name' => Yii::t('app', 'Olympic Name'),
                'xgame_sport_name' => Yii::t('app', 'Xgame Name'),
                'ncaa_sport_type' => Yii::t('app', 'Ncaa Type'),
                'ncaa_sport_season_male' => Yii::t('app', 'Ncaa Season Male'),
                'ncaa_sport_season_female' => Yii::t('app', 'Ncaa Season Female'),
                'ncaa_sport_season_coed' => Yii::t('app', 'Ncaa Season Coed'),
                'olympic_sport_season' => Yii::t('app', 'Olympic Season'),
                'xgame_sport_season' => Yii::t('app', 'Xgame Season'),
                'olympic_sport_gender' => Yii::t('app', 'Olympic Gender'),
                'xgame_sport_gender' => Yii::t('app', 'Xgame Gender'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'campSessions' => null,
                'campSports' => null,
                'coachSports' => null,
                'playerSports' => null,
                'schoolSports' => null,
                'sportGenders' => null,
                'sportPositions' => null,
                'teams' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('sport_id', $this->sport_id);
        $criteria->compare('gsm_sport_name', $this->gsm_sport_name, true);
        $criteria->compare('sport_desc_short', $this->sport_desc_short, true);
        $criteria->compare('sport_desc_long', $this->sport_desc_long, true);
        $criteria->compare('high_school_yn', $this->high_school_yn, true);
        $criteria->compare('ncaa_yn', $this->ncaa_yn, true);
        $criteria->compare('olympic_yn', $this->olympic_yn, true);
        $criteria->compare('xgame_sport_yn', $this->xgame_sport_yn, true);
        $criteria->compare('ncaa_sport_name', $this->ncaa_sport_name, true);
        $criteria->compare('olympic_sport_name', $this->olympic_sport_name, true);
        $criteria->compare('xgame_sport_name', $this->xgame_sport_name, true);
        $criteria->compare('ncaa_sport_type', $this->ncaa_sport_type, true);
        $criteria->compare('ncaa_sport_season_male', $this->ncaa_sport_season_male, true);
        $criteria->compare('ncaa_sport_season_female', $this->ncaa_sport_season_female, true);
        $criteria->compare('ncaa_sport_season_coed', $this->ncaa_sport_season_coed, true);
        $criteria->compare('olympic_sport_season', $this->olympic_sport_season, true);
        $criteria->compare('xgame_sport_season', $this->xgame_sport_season, true);
        $criteria->compare('olympic_sport_gender', $this->olympic_sport_gender, true);
        $criteria->compare('xgame_sport_gender', $this->xgame_sport_gender, true);
        $criteria->compare('created_dt', $this->created_dt, true);
        $criteria->compare('updated_dt', $this->updated_dt, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
        ), parent::behaviors());
    }
}