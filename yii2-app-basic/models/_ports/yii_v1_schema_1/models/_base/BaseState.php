<?php

/**
 * This is the model base class for the table "state".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "State".
 *
 * Columns in table "state" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $state_id
 * @property string $state_name
 * @property string $state_code
 *
 */
abstract class BaseState extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'state';
    }

    public static function representingColumn() {
        return 'state_name';
    }

    public function rules() {
        return array(
            array(	'state_name',
					'required',
					'message' => Yii::t('app', 'Field is required')
			),
            array(	'state_name',
					'length',
					'max'=>32,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'state_code',
					'length',
					'max'=>8,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array('state_code',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('state_id, state_name, state_code', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'state_id' => Yii::t('app', 'ID'),
                'state_name' => Yii::t('app', 'Name'),
                'state_code' => Yii::t('app', 'Code'),
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('state_id', $this->state_id);
        $criteria->compare('state_name', $this->state_name, true);
        $criteria->compare('state_code', $this->state_code, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
        ), parent::behaviors());
    }
}