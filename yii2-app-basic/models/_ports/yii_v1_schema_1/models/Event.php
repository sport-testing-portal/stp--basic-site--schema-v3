<?php

Yii::import('application.models._base.BaseEvent');

class Event extends BaseEvent
{
    /**
     * @return Event
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Event|Events', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'event_id'					=> Yii::t('app', 'Event ID'),
                'event_type_id'				=> Yii::t('app', 'Event Type'),
                'event_name'				=> Yii::t('app', 'Event Name'),
                'event_access_code'			=> Yii::t('app', 'Access Code'),
                'event_location_name'		=> Yii::t('app', 'Location Name'),
                'event_location_map_url'	=> Yii::t('app', 'Location Map URL'),
                'event_scheduled_dt'		=> Yii::t('app', 'Scheduled Dt'),
                'event_participation_fee'	=> Yii::t('app', 'Participation Fee'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'eventType' => null,
                'eventAttendeeLogs' => null,
                'eventCombines' => null,
        );
    }
}
