<?php

Yii::import('application.models._base.BaseVwTestResult');

class VwTestResult extends BaseVwTestResult
{
    /**
     * @return VwTestResult
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Test Result|Test Results', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        //return $this->tableName() . '_id';
		return 'test_eval_summary_log_id';
    }
//
//    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
//    protected function beforeSave()
//    {
//        if ($this->isNewRecord) {
//			if (empty($this->created_dt)){
//				$this->created_dt = date('Y-m-d H:i:s');
//			}
//            $this->updated_dt = date('Y-m-d H:i:s');
//            $this->created_by = (int)Yii::app()->user->getId();
//            $this->updated_by = (int)Yii::app()->user->getId();
//        } else {
//            $this->updated_dt = date('Y-m-d H:i:s');
//            $this->updated_by = (int)Yii::app()->user->getId();
//        }
//        return parent::beforeSave();
//    }


	/**
	 * @param int $user_id Description
	 * @return CDbDataReader Description
	 */
	protected function select_player($user_id) {


		$cmd_text = "select tr.*
			from team_player tp
				inner join player p
					on tp.player_id = p.player_id
				inner join person per
					on p.person_id = per.person_id
				inner join vwTestResult tr
					on p.person_id = tr.person_id
			where per.user_id = $user_id
			";
		// -- 	where tc.team_id = 1
		$cmd = Yii::app()->db->createCommand($cmd_text);
		$dataReader = $cmd->query();
		// @todo: convert to an array?
		// @todo: how to attach a data reader to a grid?
		return $dataReader;
	}

	/**
	 * @param int $user_id Description
	 * @return CDbDataReader Description
	 * @internal This method is about 4x faster at the DB level than $this->select_coach_CDbCritera($user_id)
	 * @internal The end result is only about 50 milliseconds slower.
	 */
	protected function select_coach($user_id) {


		$cmd_text = "select
			,tr.*
			from team_coach tc
				inner join vwCoach c
					on tc.coach_id = c.coach_id
				inner join team_player tp
					on tc.team_id = tp.team_id
				inner join player p
					on tp.player_id = p.player_id
				inner join person per
					on p.person_id = per.person_id
				inner join vwTestResult tr
					on p.person_id = tr.person_id

			where c.user_id = $user_id
			";
		// -- 	where tc.team_id = 1
		$cmd = Yii::app()->db->createCommand($cmd_text);
		$dataReader = $cmd->query();
		// @todo: convert to an array?
		// @todo: how to attach a data reader to a grid?
		//echo $cmd->getText();
		return $dataReader;
	}

	/**
	 * This method creates a CDbCommand object
	 * @param int $user_id Description
	 * @return CDbDataReader Description
	 * @link http://chris-backhouse.com/yii-parameterising-a-sub-select-in-sql-builder/932
	 */
	//protected function select_coach_CDbCritera($user_id) {
	public static function select_coach_CDbCommand($user_id) {
		// set method to public for testing, then set back to protected
		$cmd = Yii::app()->db->createCommand();
		$cmd->select('tr.*');
		$cmd->from('team_coach tc');
		$cmd->join('vwCoach c','tc.coach_id = c.coach_id');
		$cmd->join('team_player tp','tc.team_id = tp.team_id');
		$cmd->join('player p','tp.player_id = p.player_id');
		$cmd->join('person per','p.person_id = per.person_id');
		$cmd->join('vwTestResult tr','p.person_id = tr.person_id');
		$cmd->where = 'c.user_id=:user_id';
		$cmd->params = array(':user_id'=>$user_id);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cmd->text, 10, true);
		$dr = $cmd->query();
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dr, 10, true);
		return;

		// next line is for testing
		$model = VwTestResult::model()->findAll($criteria);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model, 10, true);

		$dataProviderCriteria = array('criteria'=>$criteria);
		$dataProvider = new CActiveDataProvider('VwTestResult',$dataProviderCriteria);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dataProvider, 10, true);
		//$dataReader = $cmd->query();
		//echo $cmd->getText();
		return $dataReader;
	}

	/**
	 * This method creates a CDbCriteria object to pass to a model's findAll() method
	 *
	 * @param int $user_id Description
	 * @return CDbDataReader Description
	 * @link http://chris-backhouse.com/yii-parameterising-a-sub-select-in-sql-builder/932
	 * @internal This method was used for testing, but it has no lasting value
	 * @deprecated since version 0.0.0
	 */
	public static function select_team_coach_CDbCritera($user_id) {
		$criteria = new CDbCriteria();

		//$cmd->from('team_coach tc');
		$criteria->alias = "tc";
		$criteria->select = 'tr.person,tr.ptype,tr.gender,tr.test_desc,tr.test_type,tr.split,';
		$criteria->select .= 'tr.score,tr.test_units,tr.trial_status,tr.overall_ranking,';
		$criteria->select .= 'tr.positional_ranking,tr.total_overall_ranking,tr.total_positional_ranking,';
		$criteria->select .= 'tr.score_url,tr.video_url,tr.test_date,tr.test_date_iso,tr.tester,tr.person_id,';
		$criteria->select .= 'tr.player_id,tr.test_eval_summary_log_id,tr.test_eval_detail_log_id,tr.source_event_id';
		$criteria->join = 'INNER JOIN vwCoach c on tc.coach_id = c.coach_id ';
		$criteria->join .= 'INNER JOIN team_player tp on tc.team_id = tp.team_id ';
		$criteria->join .= 'INNER JOIN player p on tp.player_id = p.player_id ';
		$criteria->join .= 'INNER JOIN person per on p.person_id = per.person_id ';
		$criteria->join .= 'INNER JOIN vwTestResult tr on p.person_id = tr.person_id';
		$criteria->condition = 'c.user_id=:user_id';
		$criteria->params = array(':user_id'=>$user_id);

		$results = TeamCoach::model()->findAll($criteria);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($results, 10, true);
		return;

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cmd->text, 10, true);
		$dr = $cmd->query();
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dr, 10, true);
		return;

		// next line is for testing
		$model = VwTestResult::model()->findAll($criteria);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model, 10, true);

		$dataProviderCriteria = array('criteria'=>$criteria);
		$activeDataProvider = new CActiveDataProvider('VwTestResult',$dataProviderCriteria);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model2, 10, true);
		// -- 	where tc.team_id = 1
		//$cmd = Yii::app()->db->createCommand($cmd_text);
		//$dataReader = $cmd->query();
		// @todo: convert to an array?
		// @todo: how to attach a data reader to a grid?
		//echo $cmd->getText();
		//return $dataReader;
	}

	/**
	 * This method creates a CDbCriteria object to pass to a model's findAll() method
	 * This method has been modified to return a CActiveDataProvider
	 * @param int $user_id Description
	 * @return array An array of ActiveRecord objects
	 * @link http://chris-backhouse.com/yii-parameterising-a-sub-select-in-sql-builder/932
	 */
	//protected function select_coach_CDbCritera($user_id) {
	public static function select_coach_CDbCritera($user_id) {
		// set method to public for testing, then set back to protected
		$criteria = new CDbCriteria();

		//$cmd->select('tr.*');
		//$cmd->from('team_coach tc');
		$criteria->alias = "tr";
//		$criteria->select = 'tr.person,tr.ptype,tr.gender,tr.test_desc,tr.test_type,tr.split,';
//		$criteria->select .= 'tr.score,tr.test_units,tr.trial_status,tr.overall_ranking,';
//		$criteria->select .= 'tr.positional_ranking,tr.total_overall_ranking,tr.total_positional_ranking,';
//		$criteria->select .= 'tr.score_url,tr.video_url,tr.test_date,tr.test_date_iso,tr.tester,tr.person_id,';
//		$criteria->select .= 'tr.player_id,tr.test_eval_summary_log_id,tr.test_eval_detail_log_id,tr.source_event_id';
		$criteria->select = 'tr.*';
		$criteria->join = 'INNER JOIN person per on tr.person_id = per.person_id ';
		$criteria->join .= 'INNER JOIN player p on per.person_id = p.person_id ';
		$criteria->join .= 'INNER JOIN team_player tp on p.player_id = tp.player_id ';
		$criteria->join .= 'INNER JOIN team_coach tc on tp.team_id = tc.team_id ';
		$criteria->join .= 'INNER JOIN vwCoach c on tc.coach_id = c.coach_id';
		$criteria->condition = 'c.user_id=:user_id';
		$criteria->params = array(':user_id'=>$user_id);

		$models = VwTestResult::model()->findAll($criteria);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($model, 10, true);
		return $models;
	}

	/**
	 *
	 * @param integer $user_id
	 * @return CActiveDataProvider
	 */
	public static function select_player_dataProvider($user_id) {
		// set method to public for testing, then set back to protected
		$criteria = array(
			'alias' =>'tr',
			'select'=>'tr.*',
			'join'  => 'INNER JOIN person per on tr.person_id = per.person_id '
				. 'INNER JOIN player p on per.person_id = p.person_id '
				. 'INNER JOIN team_player tp on p.player_id = tp.player_id ',
			'condition'=>'per.user_id=:user_id',
			'params'   =>array(':user_id'=>$user_id),
			'order'    =>'per.person_name_last, per.person_name_first'
		);
		$dataProvider = new CActiveDataProvider('VwTestResult',
			array(
				'criteria'=>$criteria,
//				'countCriteria'=>array(
//					'condition'=>'status=1',
//					// 'order' and 'with' clauses have no meaning for the count query
//				),
				'pagination'=>array(
					'pageSize'=>20,
				),
			)
		);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dp, 10, true);
		return $dataProvider;
	}

	/**
	 *
	 * @param integer $user_id
	 * @return CActiveDataProvider
	 */
	public static function select_coach_dataProvider($user_id) {
		// set method to public for testing, then set back to protected
		$criteria = array(
			'alias' =>'tr',
			'select'=>'tr.*',
			'join'  => 'INNER JOIN person per on tr.person_id = per.person_id '
				. 'INNER JOIN player p on per.person_id = p.person_id '
				. 'INNER JOIN team_player tp on p.player_id = tp.player_id '
				. 'INNER JOIN team_coach tc on tp.team_id = tc.team_id '
				. 'INNER JOIN vwCoach c on tc.coach_id = c.coach_id',
			'condition'=>'c.user_id=:user_id',
			'params'   =>array(':user_id'=>$user_id),
			'order'    =>'per.person_name_last, per.person_name_first'
		);

		$dataProvider = new CActiveDataProvider('VwTestResult',
			array(
				'criteria'=>$criteria,
//				'countCriteria'=>array(
//					'condition'=>'status=1',
//					// 'order' and 'with' clauses have no meaning for the count query
//				),
				'pagination'=>array(
					'pageSize'=>20,
				),
			)
		);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($dataProvider, 10, true);
		return $dataProvider;
	}

	/**
	 * @deprecated since version number
	 *
	 *
	 */
    //public static function grid_data($selection_criteria)
	public static function grid_data($user_id, $person_type_id, $return_type=NULL)
    {
		if (empty($return_type)){
			$return_type = 'CActiveDataProvider';
		}

		if ($return_type == 'CActiveDataProvider' && $person_type_id == 1){
			return self::select_player_dataProvider($user_id);

		}elseif ($return_type == 'CActiveDataProvider' && $person_type_id == 2){
			return self::select_coach_dataProvider($user_id);

		} elseif($return_type == "CDbCriteria" && $person_type_id == 2){
			return self::select_coach_CDbCritera($user_id);

		} else {
			throw new CException("\$return_type is not a recognized type (*$return_type*)");
		}
		//$arr = array('coach','player','parent');

		//$user_id = 2;
		return self::select_coach_CDbCritera($user_id);

    }


	/**
	 * Returns array $data('data'=>array(),'columns'=array())
	 * @param type $user_id
	 * @param type $person_type_id 1=player,2=coach
	 * @param type $return_type defaults to null
	 * @return string $grid_definiton
	 * @internal dev sts = Golden
	 */
	public static function grid_data__as_entire_grid_config_array($user_id, $person_type_id, $return_type=NULL) {
		//$grid_data    = self::grid_data($user_id, $person_type_id, $return_type=NULL);
		$grid_data    = self::grid_data($user_id, $person_type_id, $return_type='CActiveDataProvider');
		$grid_columns = self::testResultsGridColumns($user_id, $person_type_id);
		$data         = array(
			'dataProvider'  => $grid_data,
			'columns'       => $grid_columns);

//		$view = '//testEval/ctestwlayout';
//		$grid_definition = $this->render($view, array(
//			//'model' => $model,
//			//'model'   => $data['person_detail']
//			'data'   => $grid_data
//			,'columns'=>$grid_columns
//			)
//			, $return_view_content_as_text = true
//		);
//		return $grid_definition;
		return $data;

	}

	/**
	 *
	 * @param type $user_id
	 * @param type $person_type_id
	 * @return array
	 * @throws CException
	 * @internal pulled base code from VwTestResultController.php method: testResultsColumns()
	 */
	protected static function testResultsGridColumns($user_id, $person_type_id) {
		$grid_columns = "";
//		if (empty($user_id)){
//			$user_id = Yii::app()->user->getId();
//		}
		//$model = Person::model()->findByAttributes(array('user_id'=>$user_id));
		//$person_type_id = (int)$model->person_type_id;
		//		array(
		//			'class'=>'bootstrap.widgets.TbButtonColumn',
		//		),

			//'columns' => array(
			//    array('name' => 'real_name', 'header'=> 'Trading Name','htmlOptions'=>array('width'=>'50%'),
			//        'type'=>'raw',
			//        'value' =>'CHtml::link($data->real_name, array("contractors/viewalldocuments","id"=>$data->username))',
			//        ),
			//    array('name' => 'email', 'header' => 'Email','htmlOptions'=>array('width'=>'30%')),
			//    array('name' => 'last_login', 'header' => 'Last Login', 'filter'=>false, 'htmlOptions'=>array('width'=>'20%'), 'value' => 'date("d-m-Y", strtotime($data->last_login))'),
			//
		if ($person_type_id == 1){
			// player
			$grid_columns = array(
				//'person',
				//'ptype',
				//'gender',
				'test_desc',
				'test_type',
				'split',
				'score',
				'test_units',
//				'overall_ranking',
//				'positional_ranking',
//				'total_overall_ranking',
//				'total_positional_ranking',

				//'overall_ranking',
				array('name' => 'overall_ranking', 'header' => 'OV','htmlOptions'=>array('width'=>'5%','title'=>'overall ranking within the test group')),
				//'positional_ranking',
				array('name' => 'positional_ranking', 'header' => 'PV','htmlOptions'=>array('width'=>'5%','title'=>'positional ranking within the test group')),
				//'total_overall_ranking',
				array('name' => 'total_overall_ranking', 'header' => 'Ttl OV','htmlOptions'=>array('width'=>'5%','title'=>'total overall ranking within the test group')),
				//'total_positional_ranking',
				array('name' => 'total_positional_ranking', 'header' => 'Ttl PV','htmlOptions'=>array('width'=>'5%','title'=>'total positional ranking within the test group')),


				'trial_status',
				//'score_url',
				//'video_url',
				'test_date',
				//'test_date_iso',
				//'tester',
				//'person_id',
				//'player_id',
				//'test_eval_summary_log_id',
				//'test_eval_detail_log_id',
				//'source_event_id',
			);
		}elseif ($person_type_id == 2){
			// coach
			$grid_columns = array(
				'person',
				//'ptype',
				//'gender',
				'test_desc',
				'test_type',
				'split',
				'score',
				'test_units',
				//'overall_ranking',
				array('name' => 'overall_ranking', 'header' => 'OV','htmlOptions'=>array('width'=>'5%','title'=>'overall ranking within the test group')),
				//'positional_ranking',
				array('name' => 'positional_ranking', 'header' => 'PV','htmlOptions'=>array('width'=>'5%','title'=>'positional ranking within the test group')),
				//'total_overall_ranking',
				array('name' => 'total_overall_ranking', 'header' => 'Ttl OV','htmlOptions'=>array('width'=>'5%','title'=>'total overall ranking within the test group')),
				//'total_positional_ranking',
				array('name' => 'total_positional_ranking', 'header' => 'Ttl PV','htmlOptions'=>array('width'=>'5%','title'=>'total positional ranking within the test group')),
				'trial_status',
				//'score_url',
				//'video_url',
				'test_date',
				//'test_date_iso',
				//'tester',
				//'person_id',
				//'player_id',
				//'test_eval_summary_log_id',
				//'test_eval_detail_log_id',
				//'source_event_id',
				);
		}elseif ($person_type_id == 3){
			// parent?
			$grid_columns = array(
				'person',
				//'ptype',
				//'gender',
				'test_desc',
				'test_type',
				'split',
				'score',
//				'overall_ranking',
//				'positional_ranking',
//				'total_overall_ranking',
//				'total_positional_ranking',
				//'overall_ranking',
				array('name' => 'overall_ranking', 'header' => 'OV','htmlOptions'=>array('width'=>'5%','title'=>'overall ranking within the test group')),
				//'positional_ranking',
				array('name' => 'positional_ranking', 'header' => 'PV','htmlOptions'=>array('width'=>'5%','title'=>'positional ranking within the test group')),
				//'total_overall_ranking',
				array('name' => 'total_overall_ranking', 'header' => 'Ttl OV','htmlOptions'=>array('width'=>'5%','title'=>'total overall ranking within the test group')),
				//'total_positional_ranking',
				array('name' => 'total_positional_ranking', 'header' => 'Ttl PV','htmlOptions'=>array('width'=>'5%','title'=>'total positional ranking within the test group')),

				'test_units',
				'trial_status',
				'score_url',
				'video_url',
				'test_date',
				'test_date_iso',
				'tester',
				//'person_id',
				//'player_id',
				//'test_eval_summary_log_id',
				//'test_eval_detail_log_id',
				//'source_event_id',
				//array(
				//	'class'=>'bootstrap.widgets.TbButtonColumn',
				//),
			);
		} else {
			throw new CException("person_type_id has an unknown value.");
		}
		return $grid_columns;
	}

	/**
	 * @internal dev sts = under construction
	 * @param type $selection_criteria
	 * @param type $column_list
	 */
    public static function grid_widget($selection_criteria,$column_list)
    {
		$ar_rowset = self::grid_data($selection_criteria);
		// @todo:
        //$grid = self::model()->findAllByAttributes($selection_criteria);
    }

	/**
	 * @ignore These scopes are non-functional
	 * @return array An array of named CDbCriteria
	 * @internal This is a non-working example
	 * @example $testResults = VwTestResults::model()->coach()->findAll()
	 * @example $testResults = VwTestResults::model()->player()->findAll()
	 */
	public function scopes() {
		return array(
			'coach'=>array(
				'order'=>'created_dt DESC',
				'limit'=>5,
			),
			'player'=>array(
				'order'=>'created_dt DESC',
				'limit'=>5,
			),
		);
	}

	/**
	 * Lists the test eval meta-data for all test_eval providers
	 * @param string|int $providerPk test_eval_provider_id
	 * @return array[]
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function testEvalSettingsOverview($providerPk=null) {
		$sql = \BaseModel::sqlTrim("
			select
				tep.test_eval_provider_name          as provider,
				tec.test_eval_category_name          as category,
				tet.test_eval_type_name     		 as type_name,
				tet.test_eval_type_name_sub_type     as sub_type_name,
				tet.test_eval_type_desc_short        as type_desc_short,
				tet.test_eval_type_desc_long         as type_desc_long,
				tec.test_eval_category_display_order as category_order,
				tet.test_eval_type_display_order     as type_order
				from test_eval_category tec
					inner join test_eval_type tet
						on tec.test_eval_category_id = tet.test_eval_category_id
					inner join test_eval_provider tep
						on tet.test_eval_provider_id = tep.test_eval_provider_id
				order by
					tep.test_eval_provider_name,
					tec.test_eval_category_display_order,
					tet.test_eval_type_display_order
			");
		$params = [':test_eval_provider_id'=>$providerPk];
		$cmd    = Yii::app()->db->createCommand($sql);
		$data   = $cmd->queryAll($fetchAssociative=true, $params);
		return $data;
	}

	/**
	 * Lists the test eval meta-data for a specific test_eval provider
	 * @param string|int $providerPk test_eval_provider_id
	 * @return array[]
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function testEvalSettingsOverviewByProviderPk($providerPk=null) {
		$sql = \BaseModel::sqlTrim("
			select
				tep.test_eval_provider_name          as provider,
				tec.test_eval_category_name          as category,
				tet.test_eval_type_name     		 as type_name,
				tet.test_eval_type_name_sub_type     as sub_type_name,
				tet.test_eval_type_desc_short        as type_desc_short,
				tet.test_eval_type_desc_long         as type_desc_long,
				tec.test_eval_category_display_order as category_order,
				tet.test_eval_type_display_order     as type_order
				from test_eval_category tec
					inner join test_eval_type tet
						on tec.test_eval_category_id = tet.test_eval_category_id
					inner join test_eval_provider tep
						on tet.test_eval_provider_id = tep.test_eval_provider_id
				where tep.test_eval_provider_id = :test_eval_provider_id
				order by
					tep.test_eval_provider_name,
					tec.test_eval_category_display_order,
					tet.test_eval_type_display_order
			");
		$params = [':test_eval_provider_id'=>$providerPk];
		$cmd    = Yii::app()->db->createCommand($sql);
		$data   = $cmd->queryAll($fetchAssociative=true, $params);
		return $data;
	}

	/**
	 * Lists the test eval meta-data for all specified test_eval providers
	 * @param array[] of string|int $providerPks Array of test_eval_provider_id values
	 * @return array[]
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function testEvalSettingsOverviewByProviderPks($providerPks=null) {
		// pkey format should be simple
		$providerPkList = array_values($providerPks);
		$sql = \BaseModel::sqlTrim("
			select
				tep.test_eval_provider_name          as provider,
				tec.test_eval_category_name          as category,
				tet.test_eval_type_name     		 as type_name,
				tet.test_eval_type_name_sub_type     as sub_type_name,
				tet.test_eval_type_desc_short        as type_desc_short,
				tet.test_eval_type_desc_long         as type_desc_long,
				tec.test_eval_category_display_order as category_order,
				tet.test_eval_type_display_order     as type_order
				from test_eval_category tec
					inner join test_eval_type tet
						on tec.test_eval_category_id = tet.test_eval_category_id
					inner join test_eval_provider tep
						on tet.test_eval_provider_id = tep.test_eval_provider_id
				where tep.test_eval_provider_id = :test_eval_provider_id
				order by
					tep.test_eval_provider_name,
					tec.test_eval_category_display_order,
					tet.test_eval_type_display_order
			");
		$params = [':test_eval_provider_id'=>$providerPkList];
		//$condtionTemplate = ' tep.test_eval_provider_id in ({{PROVIDERS}}) ';
		//$condition = str_ireplace('{{PROVIDERS}}', implode(',', $providerPkList), $conditionTemplate);
		$cmd    = Yii::app()->db->createCommand($sql);
		// if $cmd above doesnt work replace 'tep.test_eval_provider_id = :test_eval_provider_id' with $condition
		$data    = $cmd->queryAll($fetchAssociative=true, $params);
		$sqlText = $cmd->text;
		if ($data == false){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($providerPks, 10, true);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($sqlText, 10, true);
		}
		return $data;
	}

	/**
	 * @internal Development Status = ready for testing (rft)
	 *
	 */
	public static function testEvalSettingsOverviewByProviderPks__testHarness() {
		$attributes = [];
		$condition  = '';
		$params     = [];
		$providers = TestEvalProvider::model()->findAllByAttributes($attributes, $condition, $params);
	}

	/**
	 *
	 * @param type $playersID
	 * @return array[]
	 * @internal note that test_category, per.test_category2 cant be passed to a chart function unless they are stripped
	 */
	public static function testScoresMostRecentCombine($playersID){
		$sql = \BaseModel::sqlTrim("
			select
			@counter:=@counter+1 as id,
			per.person,
			per.test_type,
			per.test_desc,
			per.my_score,
			agr.combine_score_min,
			agr.combine_score_max,
			agr.combine_score_avg,
			per.source_event_id,
			per.test_category,
			per.test_category2,
			per.attempt,
			per.split,
			per.rating_bias,
			per.tedtd_display_order
			from (
				select max(source_event_id) as source_event_id_max
					from vwTestResult where player_id = :player_id
				) mrc
				inner join (
					select
						person,
						test_desc,
						test_type,
						test_category,
						test_category2,
						round(score,2) as my_score,
						source_event_id,
						split,
						attempt,
						test_units,
						rating_bias,
						tedtd_display_order
						from vwTestResultFull
						where player_id = :player_id
				) per
					on mrc.source_event_id_max = per.source_event_id
				inner join (
					select
						source_event_id,
						test_desc,
						round(min(score),2)  as combine_score_min,
						round(max(score),2)  as combine_score_max,
						round(avg(score),2)  as combine_score_avg
						from vwTestResult
						where score > 0 -- and trial_status != 'DQ'
						group by source_event_id, test_desc
				) agr
				on mrc.source_event_id_max = agr.source_event_id
					and per.test_desc = agr.test_desc,
				(SELECT @counter:=0) v
				order by per.tedtd_display_order, per.attempt
		");
		$cmd    = Yii::app()->db->createCommand($sql);
		$params = ['player_id'=>$playersID];
		// if $cmd above doesnt work replace 'tep.test_eval_provider_id = :test_eval_provider_id' with $condition
		$data    = $cmd->queryAll($fetchAssociative=true, $params);
		$localDebug = false;
		if ($localDebug){
			$sqlText = $cmd->text;
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($sqlText, 10, true);

		}
		if ( is_array($data) && count($data, COUNT_RECURSIVE) > 0){
			return $data;
		} else {
			return [];
		}
	}

	/**
	 *
	 * @param type $playerID
	 * @return array[]
	 * @internal note that test_category, per.test_category2 cant be passed to a chart function unless they are stripped
	 */
	public static function testScoresForAllCombinesForOneAthlete($playerID, $sort='bestToWorst'){
		if ($sort == ''){
			$order   = "order by tr.category, tr.test_type, tr.test_desc, tr.test_date_iso desc, tr.attempt";
			$grid_id = "concat(tr.test_desc, ' - ', tr.test_date_iso)";

		} elseif ($sort == 'bestToWorst'){

			$order = "order by
				tr.category, tr.test_type, tr.test_desc,
				case
					when tr.rating_bias = 'High'
						then concat(tr.rating_bias, '-', score - 100)
					when rating_bias = 'Low'
						then concat(tr.rating_bias, '-', score + 100)
					end
			";
			$grid_id = "tr.test_desc";
		} elseif ($sort == 'worstToBest'){
			$order = "order by
				tr.category, tr.test_type, tr.test_desc,
				case
					when tr.rating_bias = 'High'
						then concat(tr.rating_bias, score + 100)
					when rating_bias = 'Low'
						then concat(tr.rating_bias, score - 100)
					end
			";
			$grid_id = "tr.test_desc";
		} else {
			$order = "order by tr.category, tr.test_type, tr.test_desc, tr.test_date_iso desc, tr.attempt";
			$grid_id = "concat(tr.test_desc, ' - ', tr.test_date_iso)";
		}
		// The following fields were removed from the select because they are not in vwTestResultCategory
		// tr.test_eval_detail_log_id as id
		// as oc on tr.test_eval_detail_log_id = oc.test_eval_detail_log_id
		// ,tr.test_eval_summary_log_id
		$sql = \BaseModel::sqlTrim("
			select
				tr.id
				,concat(upper(tr.category), ' - ', tr.test_type)      as section_header
				,$grid_id                                             as grid_id
				,tr.category
				,tr.test_type
				,tr.test_subtype
				,tr.test_desc
				,tr.player_id
				,tr.test_date
				,tr.attempt
				,tr.score
				,tr.split
				,oc.split_cnt
				,case when oc.split_cnt > 0 then
					SUBSTRING_INDEX(SUBSTRING_INDEX(tr.split, ',', 1), ',', -1)
					else null end AS split_1
				,case when oc.split_cnt > 1 then
					SUBSTRING_INDEX(SUBSTRING_INDEX(tr.split, ',', 2), ',', -1)
					else null end AS split_2
				,case when oc.split_cnt = 3 then
					SUBSTRING_INDEX(SUBSTRING_INDEX(tr.split, ',', 3), ',', -1)
					else null end AS split_3
				,split_avg.split_1_avg
				,split_avg.split_2_avg
				,split_avg.split_3_avg
				,tr.rating_bias
				,tr.tedtd_display_order
				,tr.test_units
				,case when length(tr.test_units) > 2
					then lower(substring(tr.test_units,1,3))
					else lower(tr.test_units)
					end as test_unit_3char
				,case when length(tr.test_units) > 0
					then substring(tr.test_units,1,1)
					else null
					end as test_unit_1char
				,tr.summary_id
				,tr.test_date_iso
				,floor((date_format(from_days((to_days(curdate()) - to_days(`per`.`person_date_of_birth`))),
					'%Y') + 0)) 			AS `person_age`
			from vwTestResultCategory tr
				left outer join (
					select
						test_eval_detail_log_id
						,test_desc
						,(LENGTH(split) - LENGTH(REPLACE(split, ',', ''))+1) AS `split_cnt`
					from vwTestResult
					where player_id = :player_id
						and split is not null and split != 'n/a'
				) as oc on tr.id = oc.test_eval_detail_log_id

				left outer join (
					select
						tr.player_id
						,tr.test_desc
						,tr.test_date
						,round(avg(splits.split_1),3) as split_1_avg
						,round(avg(splits.split_2),3) as split_2_avg
						,round(avg(splits.split_3),3) as split_3_avg
						from vwTestResult tr
							inner join (
								select
									tri.test_eval_detail_log_id
									,case when oc.split_cnt > 0 then
										SUBSTRING_INDEX(SUBSTRING_INDEX(tri.split, ',', 1), ',', -1)
										else null end AS split_1
									,case when oc.split_cnt > 1 then
										SUBSTRING_INDEX(SUBSTRING_INDEX(tri.split, ',', 2), ',', -1)
										else null end AS split_2
									,case when oc.split_cnt = 3 then
										SUBSTRING_INDEX(SUBSTRING_INDEX(tri.split, ',', 3), ',', -1)
										else null end AS split_3
									from vwTestResult tri
										join (
											select
												test_eval_detail_log_id
												,test_desc
												,(LENGTH(split) - LENGTH(REPLACE(split, ',', ''))+1) AS `split_cnt`
											from vwTestResult
											where player_id = :player_id
												and split is not null and split != 'n/a'
										) as oc on tri.test_eval_detail_log_id = oc.test_eval_detail_log_id
								) splits on tr.test_eval_detail_log_id = splits.test_eval_detail_log_id
						where player_id = :player_id
							and split is not null and split != 'n/a'
						group by tr.player_id, tr.test_date, tr.test_desc
					) split_avg on tr.test_desc = split_avg.test_desc and tr.test_date = split_avg.test_date
					inner join person per
						on tr.person_id = per.person_id
			where tr.player_id = :player_id
			$order;
		");
		// @todo change the order statement to the next line AFTER the test_category and tedtd_display_order are FIXED
		// order by tr.test_date desc, tr.test_type, tr.test_category, tr.tedtd_display_order, tr.test_desc, tr.attempt;
		$cmd    = Yii::app()->db->createCommand($sql);
		$params = ['player_id'=>$playerID];
		// if $cmd above doesnt work replace 'tep.test_eval_provider_id = :test_eval_provider_id' with $condition
		$data    = $cmd->queryAll($fetchAssociative=true, $params);
		$localDebug = false;
		if ($localDebug){
			$sqlText = $cmd->text;
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($sqlText, 10, true);
		}
		if ( is_array($data) && count($data, COUNT_RECURSIVE) > 0){
			return $data;
		} else {
			return [];
		}
	}

	/**
	 *
	 * @param string|int $playerID
	 * @param string $category test_eval_category.test_eval_category_name
	 * @return array[]
	 * @internal note that test_category, per.test_category2 cant be passed to a chart function unless they are stripped
	 * @version 0.1.0
	 * @since 0.52.0
	 * @author Dana Byrd
	 */
	public static function testScoresForAllCombinesForOneAthleteByCategory($playerID, $category, $sort='bestToWorst'){

//			$order = "order by
//				tr.category, tr.test_type, tr.test_desc,
//				case
//					when tr.rating_bias = 'High'
//						then concat(tr.rating_bias, '-', tr.tedtd_display_order,'-', score - 100)
//					when rating_bias = 'Low'
//						then concat(tr.rating_bias, '-', tr.tedtd_display_order,'-', score + 100)
//					end
//			";

		if ($sort == ''){
			$order   = "order by tr.category, tr.test_type, tr.test_desc, tr.test_date_iso desc, tr.attempt";
			$grid_id = "concat(tr.test_desc, ' - ', tr.test_date_iso)";

		} elseif ($sort == 'bestToWorst'){

			$order = "order by
				tr.category, tr.test_type, tr.test_desc,
				case
					when tr.rating_bias = 'High'
						then concat(tr.rating_bias, '-', score - 100)
					when rating_bias = 'Low'
						then concat(tr.rating_bias, '-', score + 100)
					end
			";
			$grid_id = "tr.test_desc";
		} elseif ($sort == 'worstToBest'){
			$order = "order by
				tr.category, tr.test_type, tr.test_desc,
				case
					when tr.rating_bias = 'High'
						then concat(tr.rating_bias, score + 100)
					when rating_bias = 'Low'
						then concat(tr.rating_bias, score - 100)
					end
			";
			$grid_id = "tr.test_desc";
		} else {
			$order = "order by tr.category, tr.test_type, tr.test_desc, tr.test_date_iso desc, tr.attempt";
			$grid_id = "concat(tr.test_desc, ' - ', tr.test_date_iso)";
		}
		// The following fields were removed from the select because they are not in vwTestResultCategory
		// tr.test_eval_detail_log_id as id
		// as oc on tr.test_eval_detail_log_id = oc.test_eval_detail_log_id
		// ,tr.test_eval_summary_log_id
		$sql = \BaseModel::sqlTrim("
			select
				tr.id
				,concat(upper(tr.category), ' - ', tr.test_type)      as section_header
				,$grid_id                                             as grid_id
				,tr.category
				,tr.test_type
				,tr.test_subtype
				,tr.test_desc
				,tr.player_id
				,tr.test_date
				,tr.test_date_iso
				,tr.attempt
				,tr.score
				,tr.split
				,oc.split_cnt
				,case when oc.split_cnt > 0 then
					SUBSTRING_INDEX(SUBSTRING_INDEX(tr.split, ',', 1), ',', -1)
					else null end AS split_1
				,case when oc.split_cnt > 1 then
					SUBSTRING_INDEX(SUBSTRING_INDEX(tr.split, ',', 2), ',', -1)
					else null end AS split_2
				,case when oc.split_cnt = 3 then
					SUBSTRING_INDEX(SUBSTRING_INDEX(tr.split, ',', 3), ',', -1)
					else null end AS split_3
				,split_avg.split_1_avg
				,split_avg.split_2_avg
				,split_avg.split_3_avg
				,tr.rating_bias
				,tr.tedtd_display_order
				,tr.test_units
				,case when length(tr.test_units) > 2
					then lower(substring(tr.test_units,1,3))
					else lower(tr.test_units)
				end as test_unit_3char
				,case when length(tr.test_units) > 0
					then substring(tr.test_units,1,1)
					else null
				end as test_unit_1char
				,tr.summary_id
				,tr.summary_id as test_eval_summary_log_id
				,floor((date_format(from_days((to_days(curdate()) - to_days(`per`.`person_date_of_birth`))),
					'%Y') + 0)) 			AS `person_age`
				,tr.provider_id
				,tr.category_id
				,tr.type_id
				,tr.desc_id
			from vwTestResultCategory tr
				left outer join (
					select
						test_eval_detail_log_id
						,test_desc
						,(LENGTH(split) - LENGTH(REPLACE(split, ',', ''))+1) AS `split_cnt`
					from vwTestResult
					where player_id = :player_id
						and split is not null and split != 'n/a'
				) as oc on tr.id = oc.test_eval_detail_log_id
				left outer join (
					select
						tr.player_id
						,tr.test_desc
						,tr.test_date
						,round(avg(splits.split_1),3) as split_1_avg
						,round(avg(splits.split_2),3) as split_2_avg
						,round(avg(splits.split_3),3) as split_3_avg
						from vwTestResult tr
							inner join (
								select
									tri.test_eval_detail_log_id
									,case when oc.split_cnt > 0 then
										SUBSTRING_INDEX(SUBSTRING_INDEX(tri.split, ',', 1), ',', -1)
										else null end AS split_1
									,case when oc.split_cnt > 1 then
										SUBSTRING_INDEX(SUBSTRING_INDEX(tri.split, ',', 2), ',', -1)
										else null end AS split_2
									,case when oc.split_cnt = 3 then
										SUBSTRING_INDEX(SUBSTRING_INDEX(tri.split, ',', 3), ',', -1)
										else null end AS split_3
									from vwTestResult tri
										join (
											select
												test_eval_detail_log_id
												,test_desc
												,(LENGTH(split) - LENGTH(REPLACE(split, ',', ''))+1) AS `split_cnt`
											from vwTestResult
											where player_id = :player_id
												and split is not null and split != 'n/a'
										) as oc on tri.test_eval_detail_log_id = oc.test_eval_detail_log_id
								) splits on tr.test_eval_detail_log_id = splits.test_eval_detail_log_id
						where player_id = :player_id
							and split is not null and split != 'n/a'
						group by tr.player_id, tr.test_date, tr.test_desc
					) split_avg on tr.test_desc = split_avg.test_desc and tr.test_date = split_avg.test_date
					inner join person per
						on tr.person_id = per.person_id
			where tr.player_id = :player_id and tr.category = :category
			$order;
		");
		// @todo change the order statement to the next line AFTER the test_category and tedtd_display_order are FIXED
		// order by tr.test_date desc, tr.test_type, tr.test_category, tr.tedtd_display_order, tr.test_desc, tr.attempt;
		$cmd    = Yii::app()->db->createCommand($sql);
		$params = ['player_id'=>$playerID, ':category'=>$category];
		// if $cmd above doesnt work replace 'tep.test_eval_provider_id = :test_eval_provider_id' with $condition
		$data    = $cmd->queryAll($fetchAssociative=true, $params);
		$localDebug = false;
		if ($localDebug){
			$sqlText = $cmd->text;
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($sqlText, 10, true);
		}
		if ( is_array($data) && count($data, COUNT_RECURSIVE) > 0){
			return $data;
		} else {
			return [];
		}
	}

	// <editor-fold defaultstate="collapsed" desc="fetch lists for athlete test results dropdowns">
	/**
	 * Return a provider_id indexed array with a 'testerName (count)' as the text value
	 * @param type $playerID
	 * @return array
	 * @internal called by: various Athlete views
	 */
	public static function fetchTesterList($playerID=null){

		$tableWithFkey            = 'vwTestResultCategory';
		$fkeyFieldName            = 'provider_id';
		$tableWithPkey            = 'test_eval_provider';
		$pkeyFieldName            = 'test_eval_provider_id';
		$pkeyRepresentedTextField = 'test_eval_provider_name';
		$returnFlatArray          = true;

		if ($playerID > 0){
			$tableWithFkey_Condition  = "player_id = $playerID";
		} else {
			$tableWithFkey_Condition  = null;
		}
		$data = BaseModel::getDropdownValsWithCount($tableWithFkey, $fkeyFieldName,
				$tableWithPkey, $pkeyFieldName, $pkeyRepresentedTextField,
				$tableWithFkey_Condition, $returnFlatArray);

		return $data;
	}

	/**
	 * Return a category_id indexed array with a 'categoryName (count)' as the text value
	 * @param type $playerID
	 * @return array
	 * @internal called by: various Athlete views
	 */
	public static function fetchTestCategoryList($playerID=null){

		$tableWithFkey            = 'vwTestResultCategory';
		$fkeyFieldName            = 'category_id';
		$tableWithPkey            = 'test_eval_category';
		$pkeyFieldName            = 'test_eval_category_id';
		$pkeyRepresentedTextField = 'test_eval_category_name';
		$returnFlatArray          = true;

		if ($playerID > 0){
			$tableWithFkey_Condition  = "player_id = $playerID";
		} else {
			$tableWithFkey_Condition  = null;
		}
		$data = BaseModel::getDropdownValsWithCount($tableWithFkey, $fkeyFieldName,
				$tableWithPkey, $pkeyFieldName, $pkeyRepresentedTextField,
				$tableWithFkey_Condition, $returnFlatArray);

		return $data;
	}

	/**
	 * Return a type_id indexed array with a 'TypeName (count)' as the text value
	 * @param type $playerID
	 * @return array
	 * @internal called by: various Athlete views
	 */
	public static function fetchTestTypeList($playerID=null){

		$tableWithFkey            = 'vwTestResultCategory';
		$fkeyFieldName            = 'type_id';
		$tableWithPkey            = 'test_eval_type';
		$pkeyFieldName            = 'test_eval_type_id';
		$pkeyRepresentedTextField = 'test_eval_type_name';
		$returnFlatArray          = true;

		if ($playerID > 0){
			$tableWithFkey_Condition  = "player_id = $playerID";
		} else {
			$tableWithFkey_Condition  = null;
		}
		$data = BaseModel::getDropdownValsWithCount($tableWithFkey, $fkeyFieldName,
				$tableWithPkey, $pkeyFieldName, $pkeyRepresentedTextField,
				$tableWithFkey_Condition, $returnFlatArray);

		return $data;
	}

	/**
	 * Return a desc_id indexed array with a 'TestDesc (count)' as the text value
	 * @param type $playerID
	 * @return array
	 * @internal called by: various Athlete views
	 */
	public static function fetchTestDescList($playerID=null){

		$tableWithFkey            = 'vwTestResultCategory';
		$fkeyFieldName            = 'desc_id';
		$tableWithPkey            = 'test_eval_detail_test_desc';
		$pkeyFieldName            = 'test_eval_detail_test_desc_id';
		$pkeyRepresentedTextField = 'test_eval_detail_test_desc';
		$returnFlatArray          = true;

		if ($playerID > 0){
			$tableWithFkey_Condition  = "player_id = $playerID";
		} else {
			$tableWithFkey_Condition  = null;
		}
		$data = BaseModel::getDropdownValsWithCount($tableWithFkey, $fkeyFieldName,
				$tableWithPkey, $pkeyFieldName, $pkeyRepresentedTextField,
				$tableWithFkey_Condition, $returnFlatArray);

		return $data;
	}

	// </editor-fold>


}
