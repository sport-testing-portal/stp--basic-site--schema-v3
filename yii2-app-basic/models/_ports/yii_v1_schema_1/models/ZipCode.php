<?php

Yii::import('application.models._base.BaseZipCode');

class ZipCode extends BaseZipCode
{
    /**
     * @return ZipCode
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ZipCode|ZipCodes', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Fetch items for a drop down list
	 * @param string $search Search using case insistive like
	 * @param string $enableGroups Pass true to group cities by state
	 */
	public static function fetchCityListAsTextKey($search=null,$enableGroups=false){
		if ($enableGroups == true){
			// Use this select for grouping with multiple states
			$sql = BaseModel::sqlTrim("
				select state_name as group, place_name as id, place_name as text
					from zip_code sp
					order by place_name;");
			$cmd = Yii::app()->db->createCommand($sql);
			$data = $cmd->queryAll($fetchAssociative=true);
			$list = CHtml::listData($data, 'id', 'text','group');
		} elseif (!is_null($search) && $enableGroups == true){
			// Use this select for grouping with multiple states
			$sql = BaseModel::sqlTrim("
				select state_name as group, place_name as id, place_name as text
					from zip_code sp
					where place_name LIKE '%" . $search ."%'
					order by place_name;");
			$cmd = Yii::app()->db->createCommand($sql);
			$data = $cmd->queryAll($fetchAssociative=true);
			$list = CHtml::listData($data, 'id', 'text','group');

		} elseif (!is_null($search)){
			// Use this select for grouping with multiple states
			$sql = BaseModel::sqlTrim("
				select place_name as id, place_name as text
					from zip_code sp
					where place_name LIKE '%" . $search ."%'
					order by place_name;");
			$cmd = Yii::app()->db->createCommand($sql);
			$data = $cmd->queryAll($fetchAssociative=true);
			$list = CHtml::listData($data, 'id', 'text');
		} else {
			// sport positions via sport limited by sport name
			$sql = BaseModel::sqlTrim("
				select place_name as id, place_name as text
					from zip_code
					where place_name LIKE '%" . $search ."%'
					order by place_name;");
			$cmd = Yii::app()->db->createCommand($sql);
			$data = $cmd->queryAll($fetchAssociative=true);
			$list = CHtml::listData($data, 'id', 'text');
		}
		return $list;
	}

}
