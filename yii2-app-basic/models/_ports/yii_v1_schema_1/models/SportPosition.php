<?php

Yii::import('application.models._base.BaseSportPosition');

class SportPosition extends BaseSportPosition
{

	public static $p_tableName  = 'sport_position';
	public static $p_primaryKey = 'sport_position_id'; //'player_id' is NOT the primary key;
	public static $p_textColumn = 'sport_position_name';
	public static $p_modelName  = 'SportPosition';

    /**
     * @return SportPosition
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Sport Position|Sport Positions', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'sport_position_id'		=> Yii::t('app', 'Sport Position ID'),
                'sport_id'				=> Yii::t('app', 'Sport'),
                'sport_position_name'	=> Yii::t('app', 'Sport Position Name'),
                'display_order'			=> Yii::t('app', 'Display Order'),
                'created_dt' => Yii::t('app', 'Created Date'),
                'updated_dt' => Yii::t('app', 'Updated Date'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'players'		=> null,
                'playerSports'	=> null,
                'sport'			=> null,
                'teamPlayers'	=> null,
        );
    }

	/**
	 * Fetch items for a drop down list
	 * @param string $gsmSportName 'Soccer' etc
	 * @param string $enableGroups Pass true to enable all sports postions grouped by sport name
	 */
	public static function fetchSportPositionsList($gsmSportName='Soccer', $enableGroups=false){
		if ($enableGroups == true){
			// Use this select for grouping with multiple sports
			$sql = BaseModel::sqlTrim("
				select s.gsm_sport_name as group, sp.sport_position_id as id, sp.sport_position_name as text
					from sport_position sp
						inner join sport s on sp.sport_id = s.sport_id
					order by sp.display_order;");
			$cmd = Yii::app()->db->createCommand($sql);
			$data = $cmd->queryAll($fetchAssociative=true);
			$list = CHtml::listData($data, 'id', 'text','group');
		} else {
			// sport positions via sport limited by sport name
			$sql = BaseModel::sqlTrim("
				select sp.sport_position_id as id, sp.sport_position_name as text
					from sport_position sp
						inner join sport s on sp.sport_id = s.sport_id
					where s.gsm_sport_name = :gsm_sport_name
					order by sp.display_order;");
			$params = [':gsm_sport_name'=>$gsmSportName];
			$cmd = Yii::app()->db->createCommand($sql);
			$data = $cmd->queryAll($fetchAssociative=true, $params);
			$list = CHtml::listData($data, 'id', 'text');
		}
		return $list;
	}

	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownList() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		return $list;
	}

	/**
	 *
	 * @return array[] Example [0=>['id'=>1,'text'=>'rowValueText'], 1=>['id'=>'2', 'text'=>'anotherRowValueText'] ]
	 */
	public static function fetchAllAsSelect2List() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		return $data;
	}

}
