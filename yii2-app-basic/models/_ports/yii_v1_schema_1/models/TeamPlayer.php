<?php

Yii::import('application.models._base.BaseTeamPlayer');

class TeamPlayer extends BaseTeamPlayer
{
    /**
     * @return TeamPlayer
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Team Player|Team Players', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Override BaseTeamPlayer::representingColumn()
	 * @return string
	 */
    public static function representingColumn() {
        return 'team_name';
    }

	/**
	 * Override BaseTeamPlayer::relations() to allow model regeneration without
	 * affecting the relations defined here.
	 * @return string
	 */
    public function relations() {
        return array(
			// This relation refers to the 'last-edited' team by the player. A property stored in Player
            'players' => array(self::HAS_MANY, 'Player', 'player_team_player_id'),
			// This relation is the primary link to the Player that use's the player PK
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
            'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
            'teamPlaySportPosition' => array(self::BELONGS_TO, 'SportPosition', 'team_play_sport_position_id'),
        );
    }


	/**
	 *
	 * @param type $data_container_array
	 * @internal data_envelope_array('table1'=array('fld1'=>'val', 'f2'=>'val'), 't2'=>array('fld1'=>'val'))
	 */
//	public function StoreDataEnvelope($data_envelope_array) {
//		foreach ($data_envelope_array as $key => $value) {
//			if (is_array($value)){
//				// Multiple Model names were passed
//				$modelName = $key;
//
//				foreach ($value as $rfkey =>$values){
//					if (is_array($values)){
//						// multiple data rows are included
//						foreach($values as $rowId =>$rowValues){
//
//						}
//					}
//				}
//				$primaryKeyName = $model->primaryKey();
//				$Model = $model
//			} else {
//
//			}
//
//		}
//
//	}

		/**
	 *
	 * @param type $data_container_array
	 * @internal data_envelope_array('table1'=array('fld1'=>'val', 'f2'=>'val'), 't2'=>array('fld1'=>'val'))
	 */
//	public function StoreModelRows($data_envelope_array) {
//		foreach ($data_envelope_array as $key => $value) {
//			if (is_array($value)){
//				// Multiple Model names were passed
//				$modelName = $key;
//
//				foreach ($value as $rfkey =>$values){
//					if (is_array($values)){
//						// multiple data rows are included
//						foreach($values as $rowId =>$rowValues){
//
//						}
//					}
//				}
//				$primaryKeyName = $model->primaryKey();
//				$Model = $model
//			} else {
//
//			}
//
//		}
//
//	}

	/**
	 * Implement a store-this-model method that links to a parent table.
	 * @param array $fieldValuesArray Description
	 * @see http://www.yiiframework.com/doc/guide/1.1/en/database.arr
	 * @uses BaseModel::getStoreChainMetaData_ArrayTemplate()
	 * @internal Development Status = ready for testing (rft)
	 * @internal scenarios update|create
	 *
	 */
	public static function Store($fieldValuesArray) {
		if (!is_array($fieldValuesArray)){
			throw new CException("fieldValuesArray is a required parameter", 707);
		}
		$fva = $fieldValuesArray;

		// @todo: if no model row exists and a parent PK is not supplied via params throw exception
		// @todo:
		$pkName = self::model()->primaryKey();                // team_player_id
		$scmda  = BaseModel::getStoreChainMetaData_ArrayTemplate();

		if (array_key_exists($pkName, $fva)){
			$pkVal = $fva[$pkName];                           //'team_player_id'
		} else {
			$pkVal = '';
		}

		$model    = null;
		$scenario = '';
		if ((int)$pkVal > 0){
			$model = TeamPlayer::model()->findByPk($pkVal);
			if (is_null($model)){
				$msg = "The pkey value given ($pkVal) is not a valid TeamPlayer row id";
				$scmda['errors'][__CLASS__][] =
					['text'=>$msg, 'pkval'=>$pkVal];
			} else {
				$scenario = 'update';
			}
		}

		// Run custom data rules for this model (bus-rules-on-update)
		if ($scenario==='update'){
			// Assert that a new team id is not being passed in
			$teamIdExisting  = $model->team_id;
			if (isset($fva['team_id']) && (int)$fva['team_id'] > 0){
				$teamIdIncomming = $fva['team_id'];
			} else {
				$teamIdIncomming = '';
			}
			if ($teamIdIncomming !== $teamIdExisting && (int)$teamIdIncomming > 0){
				// insert team player record. Force scenario to 'insert' by setting $model=null
				$scmda['warnings'][$thisModelName][]=
						'Passing a new team id in a row-update scenario triggered a new row insert '
						. 'team_id bfr = ' . $teamIdExisting
						. ' team_id aft = ' . $teamIdIncomming
						;
				// we must pass a playerId and teamId to create a new record
				if(!isset($fva['player_id'])){
					$playerId = $model->player_id;
					$fva['player_id']      = $playerId;
					$fva['team_player_id'] = '';
				}
				$model=null; // force an insert
			}
		}

		if (is_null($model)){
			$scenario = 'insert';
			$model = new TeamPlayer();
		}


		// Get all related models

		$thisModelName        = __CLASS__;
		if (YII_DEBUG && YII_TRACE_LEVEL >=3) {
			$relationsAssociative = BaseModel::fetchModelRelations($thisModelName);
			$relationsIndexed     = $model->relations(); // returns an indexed array
		}

		// This set attributes ignores the related models that may be present within the field values array
		$attributesBfr = $model->getAttributes();
		$model->setAttributes($fva);
		$attributesAft = $model->getAttributes();

		$runValidation=true;
		if ($model->save($runValidation)){
			$diff1 = diff($attributesAft, $attributesBfr); // aft to bfr $diffAftToBfr
			$diff2 = diff($attributesBfr, $attributesAft); // bfr to aft $diffBfrToAft
			$scmda['modelsTouched'][$thisModelName][]=
				['diff1'=>$diff1, 'diff2'=>$diff2];
			$scmda['success']=$thisModelName;
		} else {
			// an error occured
			$errors = $model->getErrors();
			$scmda['errors'][$thisModelName][]=$errors;
		}

//		$row = TeamPlayer::model()->findAllByAttributes(
//				array('team_id'=>':team_id'), array(':team_id'=>$team_id));
	}

	/**
	 * Implement a store-this-model method that links to a parent table.
	 * @see http://www.yiiframework.com/doc/guide/1.1/en/database.arr
	 */
	public function Store_v0($field_values_array) {
		// @todo: if no model row exists and a parent PK is not supplied via params throw exception
		// @todo:
		if (array_key_exists('team_player_id', $field_values_array)){
			$pk = $field_values_array['team_player_id'];
			$model = TeamPlayer::model()->findByPk($pk);
		} else {
			$model = new TeamPlayer;
		}

		// This set attributes ignores the related models that may be present within the field values array
		$model->setAttributes($field_values_array);
		$model->save();
//		$row = TeamPlayer::model()->findAllByAttributes(
//				array('team_id'=>':team_id'), array(':team_id'=>$team_id));
	}

	/**
	 * This function lists members of a particular team
	 * @param string|int $playerID The team id.
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchWithTeamByPlayerId($playerID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        $criteria->with=array('team');
        $criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.player_id',$playerID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort;
        $sort->attributes = array(
            'team_name' => array(
            'asc' => ['team_name','',''],
            'desc' => 'team_name DESC',
            ), '*', /* Treat all other columns normally */
        );
        $sort->attributes = array(
            'team_name' => array(
            'asc' => 'team_name',
            'desc' => 'team_name DESC',
            ), '*', /* Treat all other columns normally */
        );
        /* End: Sort on related Model's columns */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }



	/**
	 * This function lists members of a particular team
	 * @param type $parentID The team id.
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchIncludingTeam($parentID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        $criteria->with=array('team');
        $criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.team_id',$parentID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort;
        $sort->attributes = array(
            'team_name' => array(
            'asc' => 'team_name',
            'desc' => 'team_name DESC',
            ), '*', /* Treat all other columns normally */
        );
        /* End: Sort on related Model's columns */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }

	/**
	 * Override the _base model field labels in case the model is programmatically re-generated (this override method will persist)
	 * @return array[]
	 * @internal the labels are consumed by view elements
	 */
    public function attributeLabels() {
        return array(
                'team_player_id' => Yii::t('app', 'Team Player'),
                'team_id' => Yii::t('app', 'Team'),
                'player_id' => Yii::t('app', 'Player'),
                'team_play_sport_position_id' => Yii::t('app', 'Sport Position1'),
                'team_play_sport_position2_id' => Yii::t('app', 'Sport Position2'),
                'primary_position' => Yii::t('app', 'Primary Position'),
                'coach_id' => Yii::t('app', 'Coach'),
                'coach_name' => Yii::t('app', 'Coach Name'),
                'team_play_begin_dt' => Yii::t('app', 'Team Play Begin Dt'),
                'team_play_end_dt' => Yii::t('app', 'Team Play End Dt'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'players' => null,
                'player' => null,
                'team' => null,
                'teamPlaySportPosition' => null,
        );
    }

	/**
	 * When a Coach->Person->person_name* is updated then update related teamPlayer records
	 * @param type $coachID
	 * @internal Development Status = Golden!
	 * @example TeamPlayer::updateAllCoachNameByCoachId($coachID);
	 */
	public static function updateAllCoachNameByCoachId($coachID) {
		$coachName = CoachFlat::coachName($coachID);
		$models = TeamPlayer::model()->
				findAll(
					"coach_id = :coach_id AND ifnull(coach_name,'') != :coach_name",
					[':coach_id'=>$coachID, ':coach_name'=>$coachName]
				);
		$errors = [];
		foreach( $models as $model){
			if ($model->coach_name !== $coachName){
				$model->setAttribute('coach_name', $coachName);
				if(!$model->save()){
					$errors['TeamPlayer'][$model->team_player_id] = $model->getErrors();
				}
			}
		}
	}

	/**
	 * When the virtual event onCoachNameUpdate() fires a Coach->Person->person_name*
	 * is updated then update related teamPlayer records
	 * @param string|int $teamPlayerID
	 * @param string|int $coachID
	 * @uses self::updateAllCoachNameByCoachId
	 * @internal Development Status = Golden!
	 * @since App version 0.54.2
	 * @version 1.0.0
	 * @throws CException
	 * @package athlete resumes
	 */
	public static function updateCoachInfoByTeamPlayerId($teamPlayerID,$coachID) {
		if ((int)$teamPlayerID <= 0 || (int)$coachID <= 0 ){
			throw new CException('teamPlayerID, and coachID are required parameters', 707);
		}
		$coachName  = CoachFlat::coachName($coachID);
		$TeamPlayer = TeamPlayer::model()->findByPk($teamPlayerID);

		$teamPlayer__isDirty=false;
		if(!is_null($TeamPlayer)){
			$team_play_coach_id_bfr   = $TeamPlayer->getAttribute('coach_id');
			$team_play_coach_name_bfr = $TeamPlayer->getAttribute('coach_name');
			if ($team_play_coach_id_bfr !== $coachID){
				$TeamPlayer->setAttribute('coach_id',$coachID);
				$TeamPlayer->setAttribute('coach_name',$coachName);
				$teamPlayer__isDirty=true;
			} elseif ($team_play_coach_id_bfr == $coachID){

				if($team_play_coach_name_bfr !== $coachName){
					// If the coach name has changed then other rows my have this issue. Fix all of them.
					// $TeamPlayer->setAttribute('team_play_coach_name',$coachName);
					// $teamPlayer__isDirty=true;
					self::updateAllCoachNameByCoachId($coachID);
					// The reason a return is NOT OK is because the line above
					// would skip this table row if the coach ID is null.
					// return;
				}
			}
			if ($teamPlayer__isDirty){
				$errors = [];
				if(! $TeamPlayer->save()){
					$errors['TeamPlayer']=$TeamPlayer->getErrors();
					return $errors;
				}
			}
		} else {
			throw new CException("teamPlayerID ($teamPlayerID) must be invalid. Find by pk returned null.", 707);
		}
	}
}
