<?php

Yii::import('application.models._base.BaseTeam');

class Team extends BaseTeam
{

	public static $p_tableName  = 'team';       // used by searchBySelect2()
	public static $p_primaryKey = 'team_id';    // used by searchBySelect2()
	public static $p_textColumn = 'team_name';  // used by searchBySelect2()
	public static $p_modelName  = 'Team';       // used by searchBySelect2()

    /**
     * @return Team
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Team|Teams', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * You may override this method to do postprocessing after record saving.
	 * @return bool
	 * @internal The sessionStore value is consumed by athlete and coach ajax controls that read-write team information
	 * @see ~/Dropbox/prj_gsm_backend/Mind Maps/Ajax Multi Model Inserts-Updates (freeplane).mm
	 */
    protected function afterSave()
    {
		BaseModel::sessionStore(['team_id'=>$this->team_id]);
        return parent::afterSave();
    }


	/**
	 * This method inserts a row in this model that is to linked to a parent-table by the parent's FK
	 *
     * @return int $id The PK of an inserted row or updated row. If (int) zero is returned the insert or update failed. If no-update was needed the PK is returned.
	 * @version v0.0.4 (see also Player->Store(), Coach->Store() models for an earlier version of this method). This version is functionaly the same but has more informative comments throughout
	 * @internal CEC: 0.2.0 (History: this rev requires $row_found_yn to be Y|N or an exception is thrown
	 * @internal Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 * @internal pulled base code from Player
	 * @internal Inserts or updates a row and adds a parent table row as needed an pulls down the parent tables PK into this model
	 * @internal dev sts = UC
	 * @internal Note: (Warning!) The team table has many parent tables of which Org is only one-of, and this method is hard-coded to only work with the Org table.
	 * @internal Note: (Warning!) This method will NOT create a parent record. It merely uses an Org PK to create/update a linked team record.
     */
    public function Store($field_value_array)
    {
		$required_linkage = array('model_name'=>'Org', 'model_pk'=>'org_id');

		$primary_key_field_name = $this->primaryKey();
		$foreign_key_field_name = 'org_id';

		$class = __CLASS__;
		// run a search in this model-table (Team) by primary key
		// if this model's pk field is in the array passed in then load an AR obj into $row
		// @use-case: caller is most likely performing an update of an existing table row
		$row_found_yn = "N";
		if (array_key_exists($primary_key_field_name, $field_value_array)) {
			$pkey_val = $field_value_array[$primary_key_field_name];
			//$row = Player::model()->findByPk($pkey_val); // returns object
			$row = $class::model()->findByPk($pkey_val); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}
		// See if a foreign key value from a parent table has been passed in the field-value array.
		//   If a FK has been passed in then search this model-table for the existence of that FK
		// @use-case: caller is doesn't know the PK of this model row, but does know the PK of a parent table row.
		//            - the caller is most likely trying to perform an insert in this table-model (Team)
		if ($row_found_yn == "N" && array_key_exists($foreign_key_field_name, $field_value_array)) {
			// run a search by foreign key
			$fkey_val = (int)$field_value_array[$foreign_key_field_name];
			//$row = Player::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
			$row = $class::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}

		if ($row_found_yn == "Y"){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			// @use-case: checks values passed in against DB values and only updates DB there is a delta between DB and values passed in.
			$bfr_upd = $row->attributes;
			$row->attributes = $field_value_array;
			$diff = array_diff($bfr_upd,$row->attributes);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$primary_key_field_name];
			}
			// The values must be different. Save the model.
			if ($row->save()){
				return (int)$row->attributes[$primary_key_field_name];
			} else {
				return 0;
			}

		} elseif($row_found_yn == "N") {
			// Insert
			//$model = new Player();
			$model = new $class();						// create an AR instance with scenario='insert'
			$model->unsetAttributes();					// clears default values
			$model->attributes = $field_value_array;	// perform a mass assignment - only fields mentioned in a validation rule of some kind will be assigned
			if ($model->save()){						// fields that fail validation will have entries in the errors[] property
				$id = (int)$model->attributes[$primary_key_field_name];
			} else {
				$id = 0;
			}
			return (int)$id;
		} else {
			throw new CException(Yii::t('yii','{class} has an invalid use-case. $row_found_yn is not equal to Y or N.',
				array('{class}'=>get_class($this))));
			//return false;
		}

		//return false;
    }

    /**
	 * Get count of team records linked to a specific Org
	 * @param int $org_id Org FK
     * @return int $cnt The count of rows found
	 * @internal Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public function RowCountByOrgFk($org_id)
    {
		$class = __CLASS__;
		$rows  = $class::model()->findAllByAttributes(array("org_id"=>$org_id));

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rows, 10, true);
		if (is_array($rows) && count($rows, COUNT_RECURSIVE) > 0){
			$cnt = count($rows[0]);
		} else {
			$cnt = 0;
		}
		//$cnt = count($rows[0]);
		return $cnt;
    }

	/**
	 * Fetch a list of team's and their member counts (athlete & coach)
	 * @param int|string $team_id
	 * @return array[]
	 * @internal Development Status = Golden!
	 */
	public static function AllChildRowCnts($team_id=null){

		if (!is_null($team_id)){
			$sql = "select t.team_id as id, t.team_id, t.team_name,
				case when tc.cnt is null then 0 else tc.cnt end as coach_cnt ,
				case when tp.cnt is null then 0 else tp.cnt end as player_cnt
				from team t
					left outer join (
						select team_id, count(*) as cnt from team_coach group by team_id
					) tc on t.team_id = tc.team_id
					left outer join (
						select team_id, count(*) as cnt from team_player group by team_id
					) tp on t.team_id = tp.team_id
				where t.team_id = $team_id
				order by team_name	";
		} else {
			$sql = "select t.team_id as id, t.team_id, t.team_name,
				case when tc.cnt is null then 0 else tc.cnt end as coach_cnt ,
				case when tp.cnt is null then 0 else tp.cnt end as player_cnt
				from team t
					left outer join (
						select team_id, count(*) as cnt from team_coach group by team_id
					) tc on t.team_id = tc.team_id
					left outer join (
						select team_id, count(*) as cnt from team_player group by team_id
					) tp on t.team_id = tp.team_id
				order by team_name	";
		}
		$cmd = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		return $data;
	}


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchBySelect2($search = null, $id = null, $raw = null, $limit=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $pkName as id, $textColumn as `text` "
				 . "from $tableName "
				 . "where $textColumn LIKE :$textColumn "
				 . "order by $textColumn";
			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%'];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


	/**
	 * Call with $raw = 1 for a Select2 initial data load and return an indexed array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchTypeBySelect2($search = null, $id = null, $raw = null, $limit=null, $typeName=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$typeTable  = $tableName . '_type';
		$fkeyName   = $tableName . '_type_id';

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $tableName.$pkName as id, $tableName.$textColumn as `text` "
				 . "from $tableName inner join $typeTable on $tableName.$fkeyName = $typeTable.$fkeyName "
				 . "where $typeTable." .$typeTable . "_name = :typeName and $tableName.$textColumn LIKE :$textColumn "
				 . "order by $tableName.$textColumn";

			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%', ':typeName'=>$typeName];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}

	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownList() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		// Place United States at the top of the list
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		// example of adding a primary item at the top of the list
//		$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
//				. "order by case when $textColumn = 'United States' then 0 else $textColumn end";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		return $list;
	}

	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownListWithGrouping() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql=[];
		// Place United States at the top of the list
		$sql[0] = "select $pkName as id, $textColumn as `text` "
				. "from $tableName order by $textColumn";

//		$sql[1] = "select t.team_id as `id`, t.team_name as `text`, o.org_name as `group`
//				from team t
//					inner join org o on t.org_id = o.org_id
//				where t.team_name LIKE '%" . $search ."%'";

		$sql[2] = BaseModel::sqlTrim("select
			concat(o.org_name, ' (', ot.org_type_name, ')') as group1,
			concat(ot.org_type_name, '-', o.org_name)       as group2,
			t.team_id as `id`, t.team_name as `text`,
			o.org_name as `group`
			from team t
				inner join org      o  on t.org_id      =  o.org_id
				inner join org_type ot on o.org_type_id = ot.org_type_id
			order by ot.org_type_name, o.org_name, t.team_name;");

		// 	-- where t.team_name LIKE '%" . $search ."%'"
		$sql[3] = BaseModel::sqlTrim("select
			concat(o.org_name, ' (', ot.org_type_name, ')') as group_oname_otype,
			concat(ot.org_type_name, '-', o.org_name)       as group_otype_oname,
			t.team_id as `id`, t.team_name as `text`,
			o.org_name as `group`
			from team t
				inner join org      o  on t.org_id      =  o.org_id
				inner join org_type ot on o.org_type_id = ot.org_type_id
			order by ot.org_type_name, o.org_name, t.team_name");

//			-- where t.team_name LIKE '%" . $search ."%'"
// 			-- inner join org_type o  on team.org_id   = org.org_id

		// example of adding a primary item at the top of the list
//		$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
//				. "order by case when $textColumn = 'United States' then 0 else $textColumn end";
		$cmd  = Yii::app()->db->createCommand($sql[2]);
		$data = $cmd->queryAll($fetchAssociative=true);
		//$list = CHtml::listData( $data, 'id', 'text'); //[]
		$list = CHtml::listData( $data, 'id', 'text','group1'); //[]
		return $list;
	}

	/**
	 * Defaults to 'joinType'=>'LEFT OUTER JOIN'
	 * @return array[]
	 * @see http://www.yiiframework.com/doc/api/1.1/CActiveRelation
	 * @see http://www.yiiframework.com/doc/api/1.1/CActiveRelation#joinType-detail
	 * @example 'profile' => array(self::BELONGS_TO, 'Profile', 'userId','joinType'=>'INNER JOIN'),
	 * @internal Contains same relations as {BaseTeam} but the relations are ordered by relationType
	 */
    public function relations() {
        return array(
            'ageGroup'            => array(self::BELONGS_TO, 'AgeGroup', 'age_group_id'),
            'camp'                => array(self::BELONGS_TO, 'Camp', 'camp_id'),
            'teamCountry'         => array(self::BELONGS_TO, 'Country', 'team_country_id'),
            'teamDivision'        => array(self::BELONGS_TO, 'TeamDivision', 'team_division_id'),
            'gender'              => array(self::BELONGS_TO, 'Gender', 'gender_id'),
            'teamLeague'          => array(self::BELONGS_TO, 'TeamLeague', 'team_league_id'),
            //'org'                 => array(self::BELONGS_TO, 'Team', 'org_id'),
			'org'                 => array(self::BELONGS_TO, 'TeamOrg', 'org_id'),
            'school'              => array(self::BELONGS_TO, 'School', 'school_id'),
            'sport'               => array(self::BELONGS_TO, 'Sport', 'sport_id'),
			'playerTeams'         => array(self::HAS_MANY,   'PlayerTeam', 'team_id'),
            'teamCoaches'         => array(self::HAS_MANY,   'TeamCoach', 'team_id'),
            'teamPlayers'         => array(self::HAS_MANY,   'TeamPlayer', 'team_id'),
            'testEvalSummaryLogs' => array(self::HAS_MANY,   'TestEvalSummaryLog', 'team_id'),

        );
    }

	/**
	 *
	 * @param string|int $id
	 *
	 */
	public function fetchTeamModelByTeamPlayerID($id){
		$condition = 'teamPlayers.team_player_id = :team_player_id';
		$params = [':team_player_id'=>$id];
		$model = Team::model()->team()->find($condition,$params);
		return $model;
	}


	// @ todo create scope for teamTestResults

	/**
	 * A scope that returns the placeholders for each team relation
	 * @return CActiveRecord (Team + Org + OrgType )
	 * @example $model = Team::model()->team()->findByPk(5); // team_id
	 * @version 0.1.0
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 * @internal Development Status = Golden!
	 */
	public function team() {

		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
				'with'=>[
					'org'=>[
						'with'=>[
							'orgType',
							'schools',
						]
					],
//					'teamPlayers',
					'teamPlayers'=>[
						'with'=>[
							'player'=>[
								'with'=>'person',
								//'with'=>'PlayerPerson',
							],

						],
					],
//					'teamCoaches',
					// two aliases of person models can
//					'teamCoaches'=>[
//						'with'=>[
//							'coach'=>[
//								'with'=>'person',
//							],
//						],
//					],
					'teamCoaches'=>[
						'with'=>[
							'coach'=>[
								'with'=>[
									'person'=>[
										'alias'=>'personCoach',
										//'condition'=> "personCoach.person_type_id = 2",
										//'on'=>"personCoach.person_type_id = 2",
									]
								]
							],
						],
					],

					'ageGroup',
					'camp',
					'teamCountry',
					'teamDivision',
					'teamLeague',
					'gender',
					'school',
					'sport',
					//Next relations joins b
					'testEvalSummaryLogs'=>[
//						'with'=>[
//							'testEvalDetailLogs',
//							'testEvalLogs',
//							'person',
//							'testEvalType',
//						],
					],
				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}

	public function teamOrg() {

		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
				'with'=>[
					'org',
//					'org'=>[
//						'with'=>[
//							'orgType',
//							'schools',
//						]
//					],
					'ageGroup',
					'camp',
					'teamCountry',
					'teamDivision',
					'teamLeague',
					'gender',
					'school',
					'sport',
					//Next relations joins b
					'testEvalSummaryLogs'=>[
//						'with'=>[
//							'testEvalDetailLogs',
//							'testEvalLogs',
//							'person',
//							'testEvalType',
//						],
					],
				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}

	/**
	 * This is NOT a model scope. This method receives an Org|Team model
	 * and appends has-many related model relations to it. The relations fetched
	 * must be defined in the Team model class or the related model will be skipped
	 *
	 * @param CActiveRecord::Org $model | CActiveRecord::Team $model
	 * @param string|int $teamID
	 * @param array $modelsToLoad A list of model names to load
	 * @param array $relationsToLoad A list of athlete|player relation Names to load
	 * @return CActiveRecord::AweActiveRecord::Athlete | CActiveRecord::AweActiveRecord::Person
	 * @todo To make the lazyLoad work in a One-To-Many scenario like org+teams the parameters
	 * of the bind are different than the one-to-one bind of person+athelete.
	 * If an Org model is passed in and not a Team model then ALL teams should be appended to
	 * the Org model unless a filtering teamID has been specified.
	 * @todo Add a corollary method called teamsLazyLoad() that doesn't have a $teamID parameter.
	 * Due to the scope increase of the one-to-many scenario, then default to requiring a teamID filter
	 * @example Athlete::model()->athleteLazyLoadByAthleteID($athleteID)->findByPk($athleteID);
	 * @internal Development Status = ready for testing (rft)
	 * @internal CEC lazyLoad v0.2.0 Allows CBelongsToRelation types to be set to be fetched
	 * and if not found the relatedModel is a new model for use in UI fields where controls
	 * and field validations are required.
	 * @internal CEC lazyLoad v0.1.0 Populate CHasManyRelation types based on model name
	 * or relationName.
	 * and field validations are required.
	 * @internal Development Status = Golden!
	 */
	public static function teamlazyLoad($model, $teamID=null, array $modelsToLoad=[], array $relationsToLoad=[]) {

		// Validate parameters
		$isOrg      = false;
		$isTeam     = false;
		$orgTypeID  = 0;
		$localDebug = false;

		if (is_a($model, 'Org')){
			$isOrg     = true;
			$orgID     = $model->org_id;
			if ( is_null($teamID) ){
				throw new CException("When an Org model is passed the teamID parameter is required", 707);
			}
			$teamID    = 0;
			$orgTypeID = $model->org_type_id;

		} elseif (is_a($model, 'Team') ){
			$isTeam = true;
			$orgID  = $model->org_id;
			$teamID = $model->team_id;
			if (isset($model->org)){
				$orgTypeID = $model->org['org_type_id'];
			}
		}

		// @todo is this really neccessary? Why not both?
		if (count($modelsToLoad) > 0 && count($relationsToLoad) > 0){
			throw new CException("specify model names OR relation names. You can not pass both", 707);
			// @todo Allow both only after intersection dupe checking has been added for the two arrays .
			// Leaving this 'as is' is perfectly fine.
		}

		if ($isOrg && ! isset($model->teams)){
			// find the team object to add
			// see @todo regarding a corollary function to handle the findAll select
			// just below, use the the function name: teamsLazyLoad() eg teams plural
			// $teamModel = Team::model()->team()->findAllByAttributes(['org_id'=>$orgID]);
			$teamModel = Team::model()->team()->findByPk($teamID);

			if ($localDebug === true){
				$attrDeep = BaseModel::getAttributesDeepAsArray($teamModel);
				$example = "\$model = Team::model()->team()->findByAttributes(['org_id'=>$orgID]);";
				echo "<h1>Team->team() test harness</h1><pre>$example</pre>";
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($attrDeep, 10, true);
				return;
				//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($athleteModel, 10, true);
			}

		}

		$relationsMeta     = BaseModel::fetchModelRelations('Team');

		// Key all operations off of model names even when relation names are passed in
		if (count($modelsToLoad) > 0){

			// Please note that the logic here can only find the Has Many relations
			//  'playerTeams'         => array(self::HAS_MANY,   'PlayerTeam', 'team_id'),
			//  'teamCoaches'         => array(self::HAS_MANY,   'TeamCoach', 'team_id'),
			//  'teamPlayers'         => array(self::HAS_MANY,   'TeamPlayer', 'team_id'),
			//  'testEvalSummaryLogs' => array(self::HAS_MANY,   'TestEvalSummaryLog', 'team_id'),
			foreach ($modelsToLoad as $modelName){
				$relationType = $relationsMeta['byModelName'][$modelName]['relationType'];
				$relationFkey = $relationsMeta['byModelName'][$modelName]['fkey'];
				$relationName = $relationsMeta['byModelName'][$modelName]['relationName'];
				// use to place a break point on a specific model or relation type
				// without using a 'watch' expression in the debugger.
//				if ($modelName === 'Org'){
//					$x=1;
//				}
				if ($relationType === 'CBelongsToRelation'){
					// note that reading up-the-relations stack requires a separate fetch
					// for each model. Use this parent of team-model fetch only
					// when a boundary use case situation requires it, Selecting Org->Team
					// using a 'with' or inner join is far more efficent.
					$fkeyVal = $model[$relationFkey];
					if ( (int)$fkeyVal > 0){
						$relatedModel = $modelName::model()->findByPk($fkeyVal);
					} else {
						$relatedModel = null;
					}
					if ( is_object($relatedModel)){
						$model[$relationsMeta['byModelName'][$modelName]['relationName']] = $relatedModel;
					} else {
						$model[$relationsMeta['byModelName'][$modelName]['relationName']] = new $modelName();
					}
					//
					continue;
				}
				$models = $modelName::model()->findAllByAttributes(['team_id'=>$teamID]);
				//foreach ($models as $lazyModel){
				foreach ($models as $lazyModel){
					$model[$relationsMeta['byModelName'][$modelName]['relationName']] =  $lazyModel;
				}
				// PHP best practice for foreach loops
				unset($lazyModel);
			}
			// PHP best practice for foreach loops
			unset($modelName);
		}

		// Key all operations off of model names even when relation names are passed in
		if (count($relationsToLoad) > 0){
			//foreach ($relationsMeta['byRelationName'] as $relationName => $relationsInfo){
			foreach ($relationsToLoad as $relationName){
				$modelName = $relationsMeta['byRelationName'][$relationName]['relatedModel'];
				$models = $modelName::model()->findAllByAttributes(['org_id'=>$orgID]);
				foreach ($models as $lazyModel){
					$model[$relationName] =  $lazyModel;
				}
				// PHP best practice for foreach loops
				unset($lazyModel);
			}
			// PHP best practice for foreach loops
			unset($relationName);
		}

		return $model;
	}


    /**
	 * Override _base attributeLabels() so this method will survive model regeneration
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'team_id' => Yii::t('app', 'Team'),
                'org_id' => Yii::t('app', 'Organization Name'),
                'school_id' => Yii::t('app', 'School'),
                'sport_id' => Yii::t('app', 'Team Sport'),
                'camp_id' => Yii::t('app', 'Camp'),
                'gender_id' => Yii::t('app', 'Team Gender'),
                'age_group_id' => Yii::t('app', 'Team Age Group'),
                'team_name' => Yii::t('app', 'Team Name'),
                'team_gender' => Yii::t('app', 'Gender'),
                'team_division_id' => Yii::t('app', 'Team Division'),
                'team_league_id' => Yii::t('app', 'Team League'),
                'team_division' => Yii::t('app', 'Division'),
                'team_league' => Yii::t('app', 'League'),
                'organizational_level' => Yii::t('app', 'Organizational Level'),
                'team_governing_body' => Yii::t('app', 'Governing Body'),
                'team_age_group' => Yii::t('app', 'Age Group'),
                'team_website_url' => Yii::t('app', 'Website Url'),
                'team_schedule_url' => Yii::t('app', 'Schedule Url'),
                'team_schedule_uri' => Yii::t('app', 'Schedule Uri'),
                'team_statistical_highlights' => Yii::t('app', 'Statistical Highlights'),
                'team_competition_season_id' => Yii::t('app', 'Competition Season ID'),
                'team_competition_season' => Yii::t('app', 'Competition Season'),
                'team_city' => Yii::t('app', 'Team City'),
                'team_state_id' => Yii::t('app', 'Team State'),
                'team_country_id' => Yii::t('app', 'Team Country'),
                'team_wins' => Yii::t('app', 'Wins'),
                'team_losses' => Yii::t('app', 'Losses'),
                'team_draws' => Yii::t('app', 'Draws'),
                'created_dt' => Yii::t('app', 'Created Date'),
                'updated_dt' => Yii::t('app', 'Updated Date'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'playerTeams' => null,
                'ageGroup' => null,
                'camp' => null,
                'teamCountry' => null,
                'teamDivision' => null,
                'gender' => null,
                'teamLeague' => null,
                'org' => null,
                'school' => null,
                'sport' => null,
                'teamCoaches' => null,
                'teamPlayers' => null,
                'testEvalSummaryLogs' => null,
        );
    }

}
