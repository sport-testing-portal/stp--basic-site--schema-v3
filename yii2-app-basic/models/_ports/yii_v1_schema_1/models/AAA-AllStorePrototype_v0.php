<?php
class temp {

	/**
	 * AWE Recordset update
	 * Handles BELONGS_TO relations. This is a template for all belongs_to relations
	 * @param string $modelName
	 * @data array $data
	 * @internal Included for reference. Base code added to AweActiveRecord->storeWithParents()
	 */
	public function store($modelName, $data, $formName=''){

		if(! isset($data[$modelName])){
			return;
		}

		if(array_key_exists($modelName, $data)){
			$modelValuesToSet = $data[$modelName];
			$modelPkName      = $modelName::model()->primaryKey();
			if(isset($modelValuesToSet[$modelPkName]) && (int)$modelValuesToSet[$modelPkName]>0){
				$scenario = 'update';
				$modelPkVal = (int)$modelValuesToSet[$modelPkName];
				$model = $modelName::model()->findByPk($modelPkVal);
			}

			if (is_null($model)) {
				$scenario = 'insert';
				$model    = new $modelName;

				// $this->performAjaxValidation($model, 'org-form');
				if(isset($_POST['ajax']) && $_POST['ajax'] === $formName)
				{
					echo CActiveForm::validate($model);
					Yii::app()->end();
				}
			}

			$relationNames = array_keys($data[$modelName]);

			$relationsAssoc = $model->relationsAssoc($bSaveOnly=true);
			foreach ($relationNames as $relationName) {
				$relatedModelName = $relationsAssoc['byRelationName'][$relationName]['relatedModel'];
				if (isset($data[$modelName][$relationName])) {
					// by relationName
					$model->{$relationName} = $data[$modelName][$relationName];

				} elseif (isset($data[$relatedModelName])) {
					// byModelName
					$model->{$relationName} = $data[$relatedModelName];

				} else {

					if ($scenario === 'update'){
						$model->{$relationName} = array();
					}
				}
			}

			$model->attributes = $data[$modelName];
			if($model->save()) {
				$this->redirect(array('view','id' => $model->org_id));
            }
		}
	}
}