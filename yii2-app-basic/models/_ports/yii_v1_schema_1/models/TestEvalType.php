<?php
/**
 * @internal Google this: define type and category
 * @internal A category is hard and sharp definition containing loosely defined types */

Yii::import('application.models._base.BaseTestEvalType');

class TestEvalType extends BaseTestEvalType
{
    /**
     * @return TestEvalType
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Test Category|Test Categories', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'test_eval_type_id'					=> Yii::t('app', 'ID'),
                'test_eval_provider_id'				=> Yii::t('app', 'Test Provider'),
//              Next three lines are pre-version 0.51
//                'test_eval_category_id'				=> Yii::t('app', 'Test Type'),
//                'test_eval_type_name'				=> Yii::t('app', 'Category #1'),
//                'test_eval_type_name_sub_type'		=> Yii::t('app', 'Category #2'),
//              Next three lines updated to match version 0.51+ SuperUser spreadsheet
                'test_eval_category_id'				=> Yii::t('app', 'Test Category'),
                'test_eval_type_name'				=> Yii::t('app', 'Test Type'),
                'test_eval_type_name_sub_type'		=> Yii::t('app', 'Test Sub-Type'),
                'test_eval_type_desc_short'			=> Yii::t('app', 'Desc Short'),
                'test_eval_type_desc_long'			=> Yii::t('app', 'Desc Long'),
                'test_eval_type_display_order'		=> Yii::t('app', 'Display Order'),
                'effective_begin_dt'				=> Yii::t('app', 'Effective Begin Dt'),
                'effective_end_dt'					=> Yii::t('app', 'Effective End Dt'),
                'units'								=> Yii::t('app', 'Units'),
                'format'							=> Yii::t('app', 'Format'),
                'splits'							=> Yii::t('app', 'Splits'),
                'checkpoints'						=> Yii::t('app', 'Checkpoints'),
                'attempts'							=> Yii::t('app', 'Attempts'),
                'qualifier'							=> Yii::t('app', 'Qualifier'),
                'backend_calculation'				=> Yii::t('app', 'Backend Calculation'),
                'metric_equivalent'					=> Yii::t('app', 'Metric Equivalent'),
                'created_dt'						=> Yii::t('app', 'Created Dt'),
                'updated_dt'						=> Yii::t('app', 'Updated Dt'),
                'created_by'						=> Yii::t('app', 'Created By'),
                'updated_by'						=> Yii::t('app', 'Updated By'),
                'testEvalDetailTestDescs' => null,
                'testEvalLogs' => null,
                'testEvalSummaryLogs' => null,
                'testEvalProvider' => null,
                'testEvalCategory' => null,
        );
    }

	/**
	 *
	 * @param string $providerName
	 * @return array[] $data
	 * @see prj_gsm_backend/source_code/SQL/gsm/SuperUser Test Category Breakdown.sql
	 * @internal output looks like 'PHYSICAL-Attribute--Body Composition---Height'
	 */
	public static function fetchTestTypeDropDownByProviderName($providerName='Global Soccer Metrix'){
		$sql = BaseModel::sqlTrim(
			"select
				tet.test_eval_type_id
				,concat(
					upper(ifnull(tec.test_eval_category_name,''))
					,'-',ifnull(tet.test_eval_type_name,'')
					,'--',ifnull(tet.test_eval_type_name_sub_type,'')
					,'---',ifnull(tet.test_eval_type_desc_short,'')
				) as test_type
				from test_eval_type tet
					inner join test_eval_category tec
						on tet.test_eval_category_id = tec.test_eval_category_id
					inner join test_eval_provider tep
						on tep.test_eval_provider_id = tet.test_eval_provider_id
				where tep.test_eval_provider_name = :provider_name
				order by tet.test_eval_type_display_order;"
		);
		$params = [':provider_name'=>$providerName];
		$cmd    = Yii::app()->db->createCommand($sql);
		$data   = $cmd->queryAll($fetchAssociative = true, $params);
		return $data;
	}

	/**
	 *
	 * @param string|int $providerID
	 * @return array[] $data
	 * @see prj_gsm_backend/source_code/SQL/gsm/SuperUser Test Category Breakdown.sql
	 * 6='Global Soccer Metrix'
	 */
	public static function fetchTestTypeDropDownByProviderId($providerID=6){
		$sql = BaseModel::sqlTrim(
			"select
				tet.test_eval_type_id
				,concat(
					upper(ifnull(tec.test_eval_category_name,''))
					,'-',ifnull(tet.test_eval_type_name,'')
					,'--',ifnull(tet.test_eval_type_name_sub_type,'')
					,'---',ifnull(tet.test_eval_type_desc_short,'')
				) as test_type
				from test_eval_type tet
					inner join test_eval_category tec
						on tet.test_eval_category_id = tec.test_eval_category_id
					inner join test_eval_provider tep
						on tep.test_eval_provider_id = tet.test_eval_provider_id
				where tep.test_eval_provider_id = :provider_id
				order by tet.test_eval_type_display_order;"
		);
		$params = [':provider_id'=>$providerID];
		$cmd    = Yii::app()->db->createCommand($sql);
		$data   = $cmd->queryAll($fetchAssociative = true, $params);
		return $data;
	}

	/**
	 *
	 * @param string $providerName
	 * @return array[] $data
	 * @see prj_gsm_backend/source_code/SQL/gsm/SuperUser Test Category Breakdown.sql
	 * @internal output looks like 'PHYSICAL-Attribute--Body Composition---Height'
	 */
	public static function fetchTestTypeSelect2GroupedByCategory($providerName='Global Soccer Metrix'){
		$sql = BaseModel::sqlTrim(
			"select
				upper(tec.test_eval_category_name)      as grouping
				,tet.test_eval_type_id                  as value_field
				,concat(
					ifnull(tet.test_eval_type_name,'')
					,'--',ifnull(tet.test_eval_type_name_sub_type,'')
					,'---',ifnull(tet.test_eval_type_desc_short,'')
				)                                       as text_field
				from test_eval_type tet
					inner join test_eval_category tec
						on tet.test_eval_category_id = tec.test_eval_category_id
					inner join test_eval_provider tep
						on tep.test_eval_provider_id = tet.test_eval_provider_id
				where tep.test_eval_provider_name = :provider_name
				order by tet.test_eval_type_display_order;"
		);
		$params = [':provider_name'=>$providerName];
		$cmd    = Yii::app()->db->createCommand($sql);
		$data   = $cmd->queryAll($fetchAssociative = true, $params);
		$list   = BaseModel::listData($data, 'value_field', 'text_field', 'grouping');
		return $list;
	}

}
