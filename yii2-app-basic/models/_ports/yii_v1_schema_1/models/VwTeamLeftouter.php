<?php

Yii::import('application.models._base.BaseVwTeamLeftouter');

class VwTeamLeftouter extends BaseVwTeamLeftouter
{
    /**
     * @return VwTeamLeftouter
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Team|Teams', $n);
    }
    
    
    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();            
        }
        return parent::beforeSave();
    }    

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'org_name' => Yii::t('app', 'Org Name'),
                'org_type_name' => Yii::t('app', 'Org Type Name'),
                'team_name' => Yii::t('app', 'Team Name'),
                'team_age_group' => Yii::t('app', 'Team Age Group'),
                'team_division' => Yii::t('app', 'Team Division'),
                'organizational_level' => Yii::t('app', 'Organizational Level'),
                'team_governing_body' => Yii::t('app', 'Team Governing Body'),
                'team_website_url' => Yii::t('app', 'Team Website Url'),
                'team_schedule_url' => Yii::t('app', 'Team Schedule Url'),
                'team_schedule_uri' => Yii::t('app', 'Team Schedule Uri'),
                'team_statistical_highlights' => Yii::t('app', 'Team Statistical Highlights'),
                'gender_code' => Yii::t('app', 'Gender Code'),
                'gender_desc' => Yii::t('app', 'Gender Desc'),
                'coach_name' => Yii::t('app', 'Coach Name'),
                'player_name' => Yii::t('app', 'Player Name'),
                'gsm_sport_name' => Yii::t('app', 'Gsm Sport Name'),
                'org_id' => Yii::t('app', 'Team Org ID'),
                'org_type_id' => Yii::t('app', 'Team Org Type ID'),
                'team_id' => Yii::t('app', 'Team ID'),
                'coach_id' => Yii::t('app', 'Coach ID'),
                'coach__person_name_full' => Yii::t('app', 'Coach Person Name Full'),
                'coach__person_email_work' => Yii::t('app', 'Coach Person Email Work'),
                'coach__person_id' => Yii::t('app', 'Coach Person ID'),
                'coach__created_by' => Yii::t('app', 'Coach Created By'),
                'player_id' => Yii::t('app', 'Player'),
                'player__created_by' => Yii::t('app', 'Player Created By'),
                'team_player_id' => Yii::t('app', 'Team Player'),
                'team_player__created_by' => Yii::t('app', 'Team Player Created By'),
                'team_coach_id' => Yii::t('app', 'Team Coach'),
                'team_coach__created_by' => Yii::t('app', 'Team Coach Created By'),
                'player__person_id' => Yii::t('app', 'Player Person ID'),
                'player__person_org_id' => Yii::t('app', 'Player Person Org ID'),
                'team_coach_player_composite_key' => Yii::t('app', 'Team Coach Player Composite Key'),
                'team_created_by_username' => Yii::t('app', 'Team Created By Username'),
                'team_updated_by_username' => Yii::t('app', 'Team Updated By Username'),
                'team_updated_by_user_flname' => Yii::t('app', 'Team Updated By User Flname'),
                'team_updated_by_user_email' => Yii::t('app', 'Team Updated By User Email'),
                'team__created_by' => Yii::t('app', 'Team Created By'),
                'team__updated_by' => Yii::t('app', 'Team Updated By'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'team__created_dt' => Yii::t('app', 'Team Created Dt'),
                'team__updated_dt' => Yii::t('app', 'Team Updated Dt'),
        );
    }

}
