<?php

Yii::import('application.models._base.BaseAppParm');

class AppParm extends BaseAppParm
{
    /**
     * @return AppParm
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'AppParm|AppParms', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }



	/* sql to update a particular list
	update app_parm
	set ap_int = CONVERT(SUBSTRING_INDEX(ap_varchar,'U',-1),UNSIGNED INTEGER)
	where app_parm_type = 'List of Player Age Groups' ;
	 *
	 */
	protected static function updatePlayerAgeGroups() {
		$sql = "update app_parm "
			. "set ap_int = CONVERT(SUBSTRING_INDEX(ap_varchar,'U',-1),UNSIGNED INTEGER) "
			." where app_parm_type = 'List of Player Age Groups' ;";
		$cmd = Yii::app()->db->createCommand($sql);
		$msg = $cmd->getText();
		YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
		// @todo: enable the execute below
		//$cmd->execute();
	}

	/**
	 *
	 * @return array({'age','age_name'},{'age','age_name'})
	 */
	public static function playerAgeGroupNames() {
		$sql = "select * from where app_parm_type = 'List of Player Age Groups' order by ap_int desc";
		$list = Yii::app()->db->createCommand($sql)->queryAll();
		$rs=array();
		foreach($list as $item){
			//process each item here
			//$rs[] = $item['ap_int'];
			$rs[]=array('age'=>$item['ap_int'], 'age_name'=>$item['ap_varchar']);
		}
		return $rs;

	}

	/**
	 *
	 * @return array({'val','text'},{'val','text'})
	 * @internal dev sts = Golden
	 * // @todo: remove dead wood
	 */
	public static function playerAgeGroupBindableList() {
		//$sql = "select ap_int as val, ap_varchar as text from where app_parm_type = 'List of Player Age Groups' order by ap_int desc";
		$sql = "select ap_int as val, ap_varchar as `text` "
				."from app_parm "
				."where app_parm_type = 'List of Player Age Groups' "
				."order by ap_int desc";
		$list = Yii::app()->db->createCommand($sql)->queryAll();
		$rs=array();
		foreach($list as $item){
			//process each item here
			//$rs[] = $item['ap_int'];
			//$rs[]=array('val'=>$item['ap_int'], 'text'=>$item['ap_varchar']);
			$rs[]=array('val'=>$item['val'], 'text'=>$item['text']);
		}
		return $rs;

	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'app_parm_id' => Yii::t('app', 'ID'),
                'app_parm_type' => Yii::t('app', 'Type'),
                'ap_int' => Yii::t('app', 'Int'),
                'ap_varchar' => Yii::t('app', 'Varchar'),
                'ap_datetime' => Yii::t('app', 'Datetime'),
                'ap_comment' => Yii::t('app', 'Comment'),
                'ap_display_order' => Yii::t('app', 'Display Order'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'created_by' => Yii::t('app', 'Created By'),
        );
    }

}
