<?php

Yii::import('application.models._base.BaseTestEvalSummaryLog');

class TestEvalSummaryLog extends BaseTestEvalSummaryLog
{
    /**
     * @return TestEvalSummaryLog
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Test Eval Summary Log|Test Eval Summary Logs', $n);
    }
    
    
    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();            
        }
        return parent::beforeSave();
    }  
	
	/**
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public function Store($field_value_array)
    {
		$primary_key_field_name = $this->primaryKey();
		$foreign_key_field_name = 'person_id';
		$dupe_key_field_name    = 'source_event_id';
		
		$class = __CLASS__;
		// run a search by primary key 
		$row_found_yn = "N";
		if (array_key_exists($primary_key_field_name, $field_value_array)) {
			$pkey_val = $field_value_array[$primary_key_field_name];
			$row = $class::model()->findByPk($pkey_val); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		
		}
		if ($row_found_yn == "N" 
				&& array_key_exists($foreign_key_field_name, $field_value_array)
				&& array_key_exists($dupe_key_field_name, $field_value_array)
				) {
			$fkey_val = (int)$field_value_array[$foreign_key_field_name];
			$dkey_val = (int)$field_value_array[$dupe_key_field_name];
			$attribs = array($foreign_key_field_name=>$fkey_val, $dupe_key_field_name=>$dkey_val);
			$row     = $class::model()->findByAttributes($attribs); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}			
		}
		
		if ($row_found_yn == "N" && array_key_exists($foreign_key_field_name, $field_value_array)) {
			// run a search by foreign key
			$fkey_val = (int)$field_value_array[$foreign_key_field_name];
			$row = $class::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}			
		}
		
		if ($row_found_yn == "Y"){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $row->attributes;
			$row->attributes = $field_value_array;
			$diff = array_diff($bfr_upd,$row->attributes);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$primary_key_field_name];
			}
			// The values must be different. Save the model.
			if ($row->save()){
				return (int)$row->attributes[$primary_key_field_name];
			} else {
				return 0;
			}
			
		} else {
			// Insert
			$model = new $class();
			$model->unsetAttributes(); // clears default values
			$model->attributes = $field_value_array;
			if ($model->save()){
				$id = (int)$model->attributes[$primary_key_field_name];
			} else {
				$id = 0;
			}
			
			return (int)$id;
		}		
		
		return false;
    } 
	
    /**
	 * @param int  
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public function RowCountByPersonId($person_id)
    {
		$class = __CLASS__;
		$rows  = $class::model()->findAllByAttributes(array("person_id"=>$person_id));

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rows, 10, true); 
		if (is_array($rows) && count($rows, COUNT_RECURSIVE) > 0){
			$cnt = count($rows[0]);
		} else {
			$cnt = 0;
		}
		//$cnt = count($rows[0]);
		return $cnt;

    }	
	

}
