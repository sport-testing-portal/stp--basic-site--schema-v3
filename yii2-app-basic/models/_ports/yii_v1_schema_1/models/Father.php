<?php
/**
 * This is the model class for table "fathers".
 *
 * The followings are the available columns in table 'fathers':
 * @property integer $id
 * @property string $name
 * @see https://tahiryasin.wordpress.com/2013/05/06/how-to-save-multiple-related-models-in-yii-complete-solution/
 * The followings are the available model relations:
 * @property Children[] $childrens
 */
class Father extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Father the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function representingColumn() {
        return 'name';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Father|Fathers', $n);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'fathers';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'children' => array(self::HAS_MANY, 'Child', 'father_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function behaviors()
    {
        return array('ESaveRelatedBehavior' => array(
                'class' => 'application.components.ESaveRelatedBehavior')
        );
    }

//	public function behaviors() {
//        return array_merge(array(
//            'ESaveRelatedBehavior' => array(
//                'class' => 'EActiveRecordRelationBehavior',
//            ),
//        ), parent::behaviors());
//    }

}
?>