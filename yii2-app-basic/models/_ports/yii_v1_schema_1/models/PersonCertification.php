<?php

Yii::import('application.models._base.BasePersonCertification');

class PersonCertification extends BasePersonCertification
{
    /**
     * @return PersonCertification
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'PersonCertification|PersonCertifications', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 *
	 * @param int|string|null $person_id
	 * @param string|null $certTypeName 'USSF'|'NSCAA'|'UEFA'
	 * @return CActiveRecord PersonCertification
	 * @internal Scenario 1 - Return a single active record for data binding controls.
	 * @internal Scenario 2 - Return an array of active records for data binding controls.
	 */
	public static function fetchAllCertifications($person_id=null, $certTypeName=null) {

		if ((int)$person_id > 0 && ! empty($certTypeName)){

			$condition	= 'personCertificationType.person_certification_type_name="'
					. $certTypeName . '"';
			$params		= [':person_id'=>$person_id];
			$certifications = PersonCertification::model()
				->with('personCertificationType')
				->findAll($condition, $params);

		} elseif ((int)$person_id > 0 && empty($certTypeName)){
			$attributes		= ['person_id'=>$person_id];
			$certifications = PersonCertification::model()
				->with('personCertificationType')
				->findAllByAttributes($attributes);
		}

		if (!is_null($certifications)){
			if (count($certifications) == 1){
				return $certifications[0];
			} else {
				return $certifications;
			}
		}
	}

	/**
	 * $person_id + $certTypeName Returns single assoc array of certification + certificationType field=>value
	 * @param int|string|null $person_id
	 * @param string|null $certTypeName 'USSF'|'NSCAA'|'UEFA'
	 * @return array[] PersonCertification + PersonCertificationType
	 * @internal $person_id + $certTypeName Returns single assoc array of certification + certificationType field=>value
	 * @internal Scenario 2 - Return an array of active records for data binding controls.
	 * @example $array = PersonCertification::fetchCertificationsArray(159, 'USSF');
	 * @example YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($array, 10, true);
	 */
	public static function fetchCertificationsArray($person_id=null, $certTypeName=null) {
		$out = [];

		$certifications = self::fetchAllCertifications($person_id, $certTypeName);

		if (!is_null($certifications)){
			if (count($certifications) > 1){
				foreach ($certifications as $certification){
					$out[] = $certification->attributes
							+ $certification
							->getRelated('personCertificationType')
							->attributes;
				}

			} else {
				// A count of one is assumed
				$out = $certifications->attributes
						+ $certifications
						->getRelated('personCertificationType')
						->attributes;

			}
		}
		return $out;

	}

	/**
	 * Insert rows to function as place holders since our web controls step on each
	 *   other's 'unique' control ID for the hidden data storage field at the DOM level.
	 *   The cross-linked control IDs cause the JS AJAX POST to fail completely
	 * @param int|string $person_id
	 * @param string     $certTypeName
	 * @internal Development Status = code construction
	 */
	public static function insertPlaceHolderRow($person_id, $certTypeName) {
		$placeHolders = ['USSF'=>36,'NSCAA'=>33,'UEFA'=>34,'Special Topics'=>32,'Other'=>35];

		if(array_key_exists($certTypeName, $placeHolders)){
			$cert = new PersonCertification();
			$cert->person_id = $person_id;
			$cert->person_certification_type_id = $placeHolders[$certTypeName];
			if ($cert->save()){
				//return $cert->attributes[$this->primaryKey()];
				return $cert;
			} else {
				return null;
			}
		}
	}

	/**
	 * Insert rows to function as place holders since our web controls step on each
	 *   other's 'unique' control ID for the hidden data storage field at the DOM level.
	 *   The cross-linked control IDs cause the JS AJAX POST to fail completely
	 * @param type $person_id
	 * @param type $certs
	 * @internal Development Status = code construction
	 */
	public static function insertPlaceHolderRows($person_id, $certTypeName) {
		$certifications = self::fetchCertificationsArray($person_id);
		$placeHolders = ['USSF'=>0,'NSCAA'=>0,'UEFA'=>0,'Special Topics'=>32,'Other'=>0];
		foreach($certifications as $certification){
			// if a place holder row doesn't exist then insert one row for each cert type
			$needle = $certification['person_certification_type_id'];
			if(array_search($needle, $placeHolders)){

			}
		}
	}

}
