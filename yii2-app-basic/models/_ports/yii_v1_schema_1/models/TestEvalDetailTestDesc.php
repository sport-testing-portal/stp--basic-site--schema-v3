<?php

Yii::import('application.models._base.BaseTestEvalDetailTestDesc');

class TestEvalDetailTestDesc extends BaseTestEvalDetailTestDesc
{
    /**
     * @return TestEvalDetailTestDesc
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Test Description|Test Descriptions', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'test_eval_detail_test_desc_id'				=> Yii::t('app', 'ID'),
                'test_eval_type_id'							=> Yii::t('app', 'Test Type'),
                'test_eval_detail_test_desc'				=> Yii::t('app', 'Test Desc'),
                'test_eval_detail_test_desc_display_order'	=> Yii::t('app', 'Display Order'),
                'rating_bias'								=> Yii::t('app', 'Rating Bias'),
                'units'										=> Yii::t('app', 'Units'),
                'format'									=> Yii::t('app', 'Format'),
                'splits'									=> Yii::t('app', 'Splits'),
                'checkpoints'								=> Yii::t('app', 'Checkpoints'),
                'attempts'									=> Yii::t('app', 'Attempts'),
                'qualifier'									=> Yii::t('app', 'Qualifier'),
                'backend_calculation'						=> Yii::t('app', 'Backend Calculation'),
                'metric_equivalent'							=> Yii::t('app', 'Metric Equivalent'),
                'created_dt'								=> Yii::t('app', 'Created Dt'),
                'updated_dt'								=> Yii::t('app', 'Updated Dt'),
                'created_by'								=> Yii::t('app', 'Created By'),
                'updated_by'								=> Yii::t('app', 'Updated By'),
                'testEvalDetailLogs' => null,
                'testEvalType' => null,
        );
    }
}
