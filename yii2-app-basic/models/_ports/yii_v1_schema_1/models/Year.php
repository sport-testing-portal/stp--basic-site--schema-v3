<?php

Yii::import('application.models._base.BaseYear');

class Year extends BaseYear
{

	public static $p_tableName  = 'year';
	public static $p_primaryKey = 'year_id';
	public static $p_textColumn = 'year';
	public static $p_modelName  = 'Year';

    /**
     * @return Year
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Year|Years', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 *
	 * @param bool $useTextAsKey Description
	 * @param int|string $minYear Description
	 * @param int|string $maxYear Description
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function fetchAllAsDropDownList($useTextAsKey=true, $minYear=null, $maxYear=null) {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$validYearParams=['minYear'=>1950,'maxYear'=>2050];
		if (!empty($minYear) && (
				(int)$minYear < $validYearParams['minYear']
				||
				(int)$minYear > $validYearParams['maxYear']
				)
		){
			throw new CException('Min year is invalid', 707, null);
		}

		if (!empty($maxYear) && (
				(int)$maxYear < $validYearParams['minYear']
				||
				(int)$maxYear > $validYearParams['maxYear']
				)
		){
			throw new CException('Max year is invalid', 707, null);
		}


		if ($useTextAsKey===false){
			// @todo populate use-cases for use pkey column as ID
			if ( empty($minYear) && empty($maxYear)){
				$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
//			}elseif ( !empty($minYear) && empty($maxYear)){
//				$sql  = "select $textColumn as id, $textColumn as `text` from $tableName order by $textColumn";
//			}elseif ( empty($minYear) && !empty($maxYear)){
//				$sql  = "select $textColumn as id, $textColumn as `text` from $tableName order by $textColumn";
//			}elseif ( !empty($minYear) && !empty($maxYear)){
//				$sql  = "select $textColumn as id, $textColumn as `text` from $tableName order by $textColumn";
			}
		} elseif ($useTextAsKey===true){
			if ( empty($minYear) && empty($maxYear)){
				$sql  = "select $textColumn as id, $textColumn as `text` from $tableName "
						. "order by $textColumn";
			}elseif ( !empty($minYear) && empty($maxYear)){
				$params=[':minYear'=>$minYear];
				$sql  = "select $textColumn as id, $textColumn as `text` from $tableName "
						. "where year >= :minYear "
						. "order by $textColumn";
			}elseif ( empty($minYear) && !empty($maxYear)){
				$params=[':maxYear'=>$maxYear];
				$sql  = "select $textColumn as id, $textColumn as `text` from $tableName "
						. "where year <= :maxYear "
						. "order by $textColumn";
			}elseif ( !empty($minYear) && !empty($maxYear)){
				$params=[':minYear'=>$minYear, ':maxYear'=>$maxYear];
				$sql  = "select $textColumn as id, $textColumn as `text` from $tableName "
						. "where year >= :minYear AND year <= :maxYear "
						. "order by $textColumn";
			}
		}

		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		return $list;
	}

	/**
	 *
	 * @return array[] Example [0=>['id'=>1,'text'=>'rowValueText'], 1=>['id'=>'2', 'text'=>'anotherRowValueText'] ]
	 */
	public static function fetchAllAsSelect2List() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		return $data;
	}

}
