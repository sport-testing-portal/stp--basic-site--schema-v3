<?php

Yii::import('application.models._base.BaseTestEvalDetailLog');

class TestEvalDetailLog extends BaseTestEvalDetailLog
{
    /**
     * @return TestEvalDetailLog
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Test Detail|Test Details', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 *
	 * @param array[] $field_value_array Associative array of field1=>value1,fld2=>val2
	 * @return int $pk eg. test_eval_detail_log_id
	 * @version 1.0.0
	 */
	public static function importInsert($field_value_array) {
		$model = new TestEvalDetailLog();
		$model->setAttributes($field_value_array);
		$model->validate();
		if ($model->save()){
			$pk = $model->attributes[$model->primaryKey()];
		} else {
			$pk = 0;
		}
		return (int) $pk;
	}

	/**
	 * @param array $field_value_array Field names and value pairs
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 * @internal Table holds one to many rows. Performs lookups based on data conditions; Summary Id, Event Id, Drill Name
	 * @internal The definition of a non-dupe record; Summary Id, Test Desc Id
	 * @version 0.1.0
	 * @author Dana Byrd <dana@globalsoccermetrix.com>,dana@byrdbrain.com
	 * @internal v1.0.0 needs to eliminate all duplicate checking!
     */
    public static function Store($field_value_array){
		// Set vars
		// Search by primary key
		// Search by secondary key (fkey + dupe key) {summary_id, test_desc_id}
		$primary_key_field_name   = 'test_eval_detail_log_id';
		$foreign_key_field_name   = 'test_eval_summary_log_id';
		// The next two fields can't be used as a dupe key since the source_event_id
		//   should actually be on the summary row and will moved there eventually.
		//$dupe_key_field_names     = array('test_eval_detail_source_event_id', 'test_eval_detail_test_desc_id');
		//$dupe_key_field_name_1    = "test_eval_detail_source_event_id";
		// @todo: remove next line for v1.0
		$dupe_key_field_name          = 'test_eval_detail_test_desc_id';

		//$type_name_source_table_name  = 'test_eval_detail_test_desc';               // test_eval_detail_test_desc.test_eval_detail_test_desc
		$type_name_source_field_name  = 'test_eval_detail_test_desc';               // test_eval_detail_test_desc.test_eval_detail_test_desc
		$type_name_target_field_name  = 'test_eval_detail_test_desc_id';
		//$type_name_target_field_value = 0;

		$class = __CLASS__;
		// run a search by primary key
		$row_found_yn = "N";
		if (array_key_exists($primary_key_field_name, $field_value_array)) {
			$pkey_val = $field_value_array[$primary_key_field_name];
			$row = $class::model()->findByPk($pkey_val); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}
		// See if data key field needs xlation (convert text to a row id)
		if (array_key_exists($type_name_target_field_name, $field_value_array) === false) {
			// Add the target field and its value
			if (array_key_exists($type_name_source_field_name, $field_value_array)) {
				// The source field exists so convert it to a type id
				$type_lookup_value = $field_value_array[$type_name_source_field_name];
				$target_value      = self::get_test_description_type_id_from_type_name($type_lookup_value);
				$field_value_array[$type_name_target_field_name] = $target_value;
			}
		}
		/*
		// See if one of the key data fields used as a row marker contains a value
		// if test_desc_id is numeric run a search with that
		// if test_desc_id is not numeric throw exception
		if ($row_found_yn == "N" && array_key_exists($foreign_key_field_name, $field_value_array)) {
			// run a search by foreign key
			$fkey_val = (int)$field_value_array[$foreign_key_field_name];
			$row = $class::model()->findByAttributes(array(
				$foreign_key_field_name=>$fkey_val,
				$dupe_key_field_name=>$data_key_field_value
					)
			); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}

		if ($row_found_yn == "N" && array_key_exists($foreign_key_field_name, $field_value_array)) {
			// run a search by foreign key
			$fkey_val = (int)$field_value_array[$foreign_key_field_name];
			$row = $class::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}
		*/
		if ($row_found_yn == "N"
				&& array_key_exists($foreign_key_field_name, $field_value_array)
				&& array_key_exists($dupe_key_field_name, $field_value_array)
			)
		{
			// Build database query criteria
			$criteria = new CDbCriteria();
			$criteria->condition = 'test_eval_summary_log_id=:test_eval_summary_log_id';
			$criteria->addCondition('test_eval_detail_test_desc_id=:test_eval_detail_test_desc_id', 'AND');
			$criteria->params    = array(
				':test_eval_summary_log_id'  =>(int)$field_value_array['test_eval_summary_log_id']
				,':test_eval_detail_test_desc_id'=>(int)$field_value_array['test_eval_detail_test_desc_id']);
			$row  = TestEvalDetailLog::model()->find($criteria);
			if ($row instanceof CActiveRecord){
				$id = $row->attributes[$primary_key_field_name];

				if ((int)$id > 0){
					$row_found_yn = "Y";
				}
			}
		}
		if ($row_found_yn == "Y"){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $row->attributes;
			$row->attributes = $field_value_array;
			$aft_upd = $row->attributes;

			$diff = array_diff($bfr_upd,$aft_upd);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$primary_key_field_name];
			}
			// fields to remove from consideration
			$diff_sans_ids = $diff;
			$sans_fields = array('test_eval_detail_import_file_id', 'test_eval_detail_import_file_line_num');
			foreach($sans_fields as $fld_to_ignore){
				if (array_key_exists($fld_to_ignore, $diff_sans_ids)){
					unset($diff_sans_ids[$fld_to_ignore]);
				}
			}
			// test for type casting issues
			if (array_key_exists('test_eval_detail_score_num', $diff_sans_ids) !== false){
				// test the before and after values
				$new = (float)$field_value_array['test_eval_detail_score_num'];
				$old = (float)$bfr_upd['test_eval_detail_score_num'];
				if ($new === $old){
					unset($diff_sans_ids['test_eval_detail_score_num']);
				}
			}
			if (count($diff_sans_ids, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$primary_key_field_name];
			} else {
				// diffs where encountered. however row id diffs should be excluded
				$msg = "Diff of TestEvalDetailLog records follows.";
				YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($msg, 10, true);
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($diff, 10, true);
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($diff_sans_ids, 10, true);
			}

			// The values must be different. Save the model.
			if ($row->save()){
				return (int)$row->attributes[$primary_key_field_name];
			} else {
				return 0;
			}

		} else {
			// Insert
			$model = new $class();
			$model->unsetAttributes(); // clears default values
			$model->attributes = $field_value_array;
			if ($model->save()){
				$id = (int)$model->attributes[$primary_key_field_name];
			} else {
				$id = 0;
			}

			return (int)$id;
		}

		return false;
    }

    public static function Store_v1($field_value_array){
		// Set vars
		// Search by primary key
		// Search by secondary key (fkey + dupe key) {summary_id, test_desc_id}
		$primary_key_field_name   = 'test_eval_detail_log_id';
		$foreign_key_field_name   = 'test_eval_summary_log_id';
		// The next two fields can't be used as a dupe key since the source_event_id
		//   should actually be on the summary row and will moved there eventually.
		//$dupe_key_field_names     = array('test_eval_detail_source_event_id', 'test_eval_detail_test_desc_id');
		//$dupe_key_field_name_1    = "test_eval_detail_source_event_id";
		$dupe_key_field_name          = 'test_eval_detail_test_desc_id';

		//$type_name_source_table_name  = 'test_eval_detail_test_desc';               // test_eval_detail_test_desc.test_eval_detail_test_desc
		$type_name_source_field_name  = 'test_eval_detail_test_desc';               // test_eval_detail_test_desc.test_eval_detail_test_desc
		$type_name_target_field_name  = 'test_eval_detail_test_desc_id';
		//$type_name_target_field_value = 0;

		$class = __CLASS__;
		// run a search by primary key
		$row_found_yn = "N";
		if (array_key_exists($primary_key_field_name, $field_value_array)) {
			$pkey_val = $field_value_array[$primary_key_field_name];
			$row = $class::model()->findByPk($pkey_val); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}
		// See if data key field needs xlation (convert text to a row id)
		if (array_key_exists($type_name_target_field_name, $field_value_array) === false) {
			// Add the target field and its value
			if (array_key_exists($type_name_source_field_name, $field_value_array)) {
				// The source field exists so convert it to a type id
				$type_lookup_value = $field_value_array[$type_name_source_field_name];
				$target_value      = self::get_test_description_type_id_from_type_name($type_lookup_value);
				$field_value_array[$type_name_target_field_name] = $target_value;
			}
		}
		/*
		// See if one of the key data fields used as a row marker contains a value
		// if test_desc_id is numeric run a search with that
		// if test_desc_id is not numeric throw exception
		if ($row_found_yn == "N" && array_key_exists($foreign_key_field_name, $field_value_array)) {
			// run a search by foreign key
			$fkey_val = (int)$field_value_array[$foreign_key_field_name];
			$row = $class::model()->findByAttributes(array(
				$foreign_key_field_name=>$fkey_val,
				$dupe_key_field_name=>$data_key_field_value
					)
			); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}

		if ($row_found_yn == "N" && array_key_exists($foreign_key_field_name, $field_value_array)) {
			// run a search by foreign key
			$fkey_val = (int)$field_value_array[$foreign_key_field_name];
			$row = $class::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}
		*/
		if ($row_found_yn == "N"
				&& array_key_exists($foreign_key_field_name, $field_value_array)
				&& array_key_exists($dupe_key_field_name, $field_value_array)
			)
		{
			// Build database query criteria
			$criteria = new CDbCriteria();
			$criteria->condition = 'test_eval_summary_log_id=:test_eval_summary_log_id';
			$criteria->addCondition('test_eval_detail_test_desc_id=:test_eval_detail_test_desc_id', 'AND');
			$criteria->params    = array(
				':test_eval_summary_log_id'  =>(int)$field_value_array['test_eval_summary_log_id']
				,':test_eval_detail_test_desc_id'=>(int)$field_value_array['test_eval_detail_test_desc_id']);
			$row  = TestEvalDetailLog::model()->find($criteria);
			if ($row instanceof CActiveRecord){
				$id = $row->attributes[$primary_key_field_name];

				if ((int)$id > 0){
					$row_found_yn = "Y";
				}
			}
		}
		if ($row_found_yn == "Y"){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $row->attributes;
			$row->attributes = $field_value_array;
			$aft_upd = $row->attributes;

			$diff = array_diff($bfr_upd,$aft_upd);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$primary_key_field_name];
			}
			// fields to remove from consideration
			$diff_sans_ids = $diff;
			$sans_fields = array('test_eval_detail_import_file_id', 'test_eval_detail_import_file_line_num');
			foreach($sans_fields as $fld_to_ignore){
				if (array_key_exists($fld_to_ignore, $diff_sans_ids)){
					unset($diff_sans_ids[$fld_to_ignore]);
				}
			}
			// test for type casting issues
			if (array_key_exists('test_eval_detail_score_num', $diff_sans_ids) !== false){
				// test the before and after values
				$new = (float)$field_value_array['test_eval_detail_score_num'];
				$old = (float)$bfr_upd['test_eval_detail_score_num'];
				if ($new === $old){
					unset($diff_sans_ids['test_eval_detail_score_num']);
				}
			}
			if (count($diff_sans_ids, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$primary_key_field_name];
			} else {
				// diffs where encountered. however row id diffs should be excluded
				$msg = "Diff of TestEvalDetailLog records follows.";
				YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($msg, 10, true);
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($diff, 10, true);
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($diff_sans_ids, 10, true);
			}

			// The values must be different. Save the model.
			if ($row->save()){
				return (int)$row->attributes[$primary_key_field_name];
			} else {
				return 0;
			}

		} else {
			// Insert
			$model = new $class();
			$model->unsetAttributes(); // clears default values
			$model->attributes = $field_value_array;
			if ($model->save()){
				$id = (int)$model->attributes[$primary_key_field_name];
			} else {
				$id = 0;
			}

			return (int)$id;
		}

		return false;
    }

    /**
	 * @param int
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public static function RowCountBySummaryLogId($fkey_id)
    {
		$class = __CLASS__;
		$rows = $class::model()->findAllByAttributes(array("test_eval_summary_log_id"=>$fkey_id));

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rows, 10, true);
		if (is_array($rows) && count($rows, COUNT_RECURSIVE) > 0){
			$cnt = count($rows[0]);
		} else {
			$cnt = 0;
		}
		//$cnt = count($rows[0]);
		return $cnt;

    }

	/**
	 * convert a test desciption type name value into a type row id
	 * notes: this should be a universal component
	 * @param string $type_name_value
	 * @return int type_id
	 * @version 1.0
	 * @internal Pulled base code from DataController.get_test_description_type_id_from_type_name() v1.0
	 * @see Also the generic form of this function in Bcommon::get_type_id_from_type_name($type_name_value, $type_table_name)
	 */
	protected static function get_test_description_type_id_from_type_name($type_name_value){
		$type_name_field_name = 'test_eval_detail_test_desc';
		//$row = TestEvalDetailTestDesc::model()->findByAttributes($attributes);
		$row = TestEvalDetailTestDesc::model()->findByAttributes(
				array($type_name_field_name => $type_name_value)
				);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($row, 10, true);
		if (empty($row) === false){
			$type_id = (int)$row->attributes['test_eval_detail_test_desc_id'];
		} else {
			$type_id = 0;
		}
		return $type_id;
	}
}
