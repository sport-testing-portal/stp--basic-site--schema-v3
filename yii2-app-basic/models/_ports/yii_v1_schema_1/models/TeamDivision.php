<?php

Yii::import('application.models._base.BaseTeamDivision');

class TeamDivision extends BaseTeamDivision
{

	public static $p_tableName  = 'team_division';
	public static $p_primaryKey = 'team_division_id';
	public static $p_textColumn = 'team_division_name';
	public static $p_modelName  = 'TeamDivision';

    /**
     * @return TeamDivision
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Team Division|Team Divisions', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'team_division_id' => Yii::t('app', 'Team Division ID'),
                'team_division_name' => Yii::t('app', 'Division Name'),
                'team_division_desc_short' => Yii::t('app', 'Desc Short'),
                'team_division_desc_long' => Yii::t('app', 'Desc Long'),
                'team_division_display_order' => Yii::t('app', 'Display Order'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'teams' => null,
        );
    }

	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownList() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		// $sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by team_division_display_order";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		return $list;
	}

	/**
	 *
	 * @return array[] Example [0=>['id'=>1,'text'=>'rowValueText'], 1=>['id'=>'2', 'text'=>'anotherRowValueText'] ]
	 */
	public static function fetchAllAsSelect2List() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		//$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by team_division_display_order";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		return $data;
	}


}
