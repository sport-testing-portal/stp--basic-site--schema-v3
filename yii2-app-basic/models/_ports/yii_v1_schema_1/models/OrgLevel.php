<?php

Yii::import('application.models._base.BaseOrgLevel');

class OrgLevel extends BaseOrgLevel
{

	public static $p_tableName  = 'org_level';
	public static $p_primaryKey = 'org_level_id';
	public static $p_textColumn = 'org_level_name';
	public static $p_modelName  = 'OrgLevel';

    /**
     * @return OrgLevel
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Org Level|Org Levels', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'org_level_id' => Yii::t('app', 'Org Level ID'),
                'org_type_id' => Yii::t('app', 'Org Type'),
                'org_level_name' => Yii::t('app', 'Level Name'),
                'org_level_desc_short' => Yii::t('app', 'Desc Short'),
                'org_level_desc_long' => Yii::t('app', 'Desc Long'),
                'org_level_display_order' => Yii::t('app', 'Org Level Display Order'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'orgs' => null,
                'orgType' => null,
        );
    }


	/**
	 * @param int|string $orgLevelID
	 * @param int|string $orgTypeID
	 * @return string
	 */
    public static function levelName($orgLevelID=2,$orgTypeID=null)
    {
		// default to 'Club'
		$params=[];
		if (! empty($orgTypeID)){
			// search by attributes
			$params['org_type_id']=$orgTypeID;
			$params['org_level_id']=$orgLevelID;
			$model = self::model()->findByAttributes($params);
		} else {
			$model = self::model()->findByPk($orgLevelID);
		}


		if (is_object($model)){
			$level_name = $model->attributes['org_level_name'];
		} else {
			$level_name = '';
		}
        return Yii::t('app', $level_name);
    }


	/**
	 * Return model values suitable for
	 *   TbActiveForm::dropDownList(), or
	 *   TbActiveForm->select2Row()
	 * @param bool $returnTags if true returns [0=>'rowValueText', 1='anotherRowValueText'] as zero based indexed array
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 * @internal A null org type will return only rows that have a null org_type_id
	 */
	public static function fetchAllAsDropDownList($orgTypeID=null, $returnTags=false) {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		// This table has to be sorted by display order since the items are not compatible with alpha sorting
		// $sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by org_level_display_order";
		if (! empty($orgTypeID)){
			$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
					. "where org_type_id = :org_type_id "
					. "order by org_level_display_order";
			$params =[':org_type_id'=>$orgTypeID];
		} else {
			$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
					. "where org_type_id is null "
					. "order by org_level_display_order, org_level_name "; // if the display order is null then use the name
		}


		$cmd  = Yii::app()->db->createCommand($sql);
		if (isset($params)){
			$data = $cmd->queryAll($fetchAssociative=true, $params);
		} else {
			$data = $cmd->queryAll($fetchAssociative=true);
		}
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		if ($returnTags){
			return array_values($list);
		} else {
			return $list;
		}
	}

}
