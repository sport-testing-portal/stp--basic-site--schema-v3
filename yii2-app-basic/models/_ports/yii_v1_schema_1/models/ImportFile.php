<?php

Yii::import('application.models._base.BaseImportFile');

class ImportFile extends BaseImportFile
{
    /**
     * @return ImportFile
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Import File|Import Files', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }


	/**
	 * @param array $field_value_array {'field1_name'=>fld1_value,'field2_name'=>fld2_value}
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public function Store($field_value_array)
    {
		$primary_key_field_name = $this->primaryKey(); // import_file_id
		$foreign_key_field_name = '';
		$dupe_key_field_name    = 'import_file_source_file_name';
		$class = __CLASS__;
		// run a search by primary key
		$row_found_yn = "N";
		if (array_key_exists($primary_key_field_name, $field_value_array)) {
			$pkey_val = $field_value_array[$primary_key_field_name];
			$row      = $class::model()->findByPk($pkey_val);   // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}

		}
		if (strlen($foreign_key_field_name) > 0){
			if ($row_found_yn == "N" && array_key_exists($foreign_key_field_name, $field_value_array)) {
				// run a search by foreign key
				$fkey_val = (int)$field_value_array[$foreign_key_field_name];
				$row      = $class::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
				if (! empty($row)){
					$row_found_yn = "Y";
				}
			}
		}

		if (strlen($dupe_key_field_name) > 0){
			if ($row_found_yn == "N" && array_key_exists($dupe_key_field_name, $field_value_array)) {
				// run a search by foreign key
				$key_val  = $field_value_array[$dupe_key_field_name];
				$row      = $class::model()->findByAttributes(array($dupe_key_field_name=>$key_val)); // returns object
				if (! empty($row)){
					$row_found_yn = "Y";
				}
			}
		}

		if ($row_found_yn == "Y"){
			// Update
			// $pk_id = (int)$row->attributes[$primary_key_field_name];
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $row->attributes;
			$row->attributes = $field_value_array;
			$diff = array_diff($bfr_upd,$row->attributes);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$primary_key_field_name];
			}
			// The values must be different. Save the model.
			if ($row->save()){
				return (int)$row->attributes[$primary_key_field_name];
			} else {
				return (int)$row->attributes[$primary_key_field_name];
			}

		} else {
			// Insert
			//$model = new Player();
			$model = new $class();
			$model->unsetAttributes(); // clears default values
			$model->attributes = $field_value_array;
			if ($model->save()){
				$id = (int)$model->attributes[$primary_key_field_name];
			} else {
				$id = 0;
			}

			return (int)$id;
		}

		return false;
    }

    /**
	 * @param string $sha256 sha 256 hash string
     * @return int
	 * @internal Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 * @version 0.1
	 * @internal proposed signature RowCountBySha256($sha256, $search_target_hash=null)
     */
    public function RowCountBySha256($sha256)
    {
		// public function RowCountBySha256($sha256, $search_target_hash=null) // proposed signature
		$class = __CLASS__;
		// search key attributes
		$sha_field_name_source = 'import_file_source_file_sha256';
		//$sha_field_name_target = 'import_file_target_file_sha256';

		$field_data = array($sha_field_name_source=>$sha256);
		$rows       = $class::model()->findAllByAttributes($field_data);

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rows, 10, true);
		if (is_array($rows) && count($rows, COUNT_RECURSIVE) > 0){
			$cnt = count($rows[0]);
		} else {
			$cnt = 0;
		}
		//$cnt = count($rows[0]);
		return $cnt;

    }


    /**
	 * @param int
     * @return int
	 * @internal Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 * @version 0.1
     */
    public function RowCountByPersonId($person_id)
    {
		$class = __CLASS__;
		$rows  = $class::model()->findAllByAttributes(array("person_id"=>$person_id));

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rows, 10, true);
		if (is_array($rows) && count($rows, COUNT_RECURSIVE) > 0){
			$cnt = count($rows[0]);
		} else {
			$cnt = 0;
		}
		//$cnt = count($rows[0]);
		return $cnt;

    }

    /**
	 * _base model override
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'import_file_id' => Yii::t('app', 'Import File'),
                'import_file_source_file_name' => Yii::t('app', 'Source File Name'),
                'import_file_source_file_date' => Yii::t('app', 'Source File Date'),
                'import_file_source_file_size' => Yii::t('app', 'Source File Size'),
                'import_file_source_file_sha256' => Yii::t('app', 'Source File Sha256'),
                'import_file_source_file_password' => Yii::t('app', 'Source File Password'),
                'import_file_target_file_name' => Yii::t('app', 'Target File Name'),
                'import_file_target_file_date' => Yii::t('app', 'Target File Date'),
                'import_file_target_file_size' => Yii::t('app', 'Target File Size'),
                'import_file_target_file_sha256' => Yii::t('app', 'Target File Sha256'),
                'import_file_target_file_password' => Yii::t('app', 'Target File Password'),
                'import_file_description_short' => Yii::t('app', 'Description Short'),
                'import_file_description_long' => Yii::t('app', 'Description Long'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'importFileStiCoachReports' => null,
                'importFileStiSpecialReports' => null,
        );
    }

}
