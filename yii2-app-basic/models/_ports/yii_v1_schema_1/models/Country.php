<?php

Yii::import('application.models._base.BaseCountry');

class Country extends BaseCountry
{
	public static $p_tableName  = 'country';
	public static $p_primaryKey = 'country_id'; //'player_id' is NOT the primary key;
	public static $p_textColumn = 'short_name';
	public static $p_modelName  = 'Country';

    /**
     * @return Country
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Country|Countries', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownList() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		// Place United States at the top of the list
		//$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
				. "order by case when $textColumn = 'United States' then 0 else $textColumn end";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		return $list;
	}

	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownListWithIso3Key() {

		$tableName  = self::$p_tableName;
		//$pkName     = self::$p_primaryKey;
		//$textColumn = self::$p_textColumn;

		// Place United States at the top of the list
		//$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
//		$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
//				. "order by case when $textColumn = 'United States' then 0 else $textColumn end";

		$sql  = "select iso3 as id, short_name as `text` from $tableName "
				. "order by case when short_name = 'United States' then 0 else short_name end";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		return $list;
	}

	/**
	 *
	 * @return array[] Example [0=>['id'=>1,'text'=>'rowValueText'], 1=>['id'=>'2', 'text'=>'anotherRowValueText'] ]
	 */
	public static function fetchAllAsSelect2List() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		return $data;
	}

}
