<?php

Yii::import('application.models._base.BaseVwClub');

class VwClub extends BaseVwClub
{
    /**
     * @return VwClub
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Club|Clubs', $n);
    }
    
    
    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        //return $this->tableName() . '_id';
		return 'org_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();            
        }
        return parent::beforeSave();
    }    
	
	/**
	 * Store Org[org_type_id=2] using an fva
     * @return int
	 * @internal Insert or update a row, add a row in a parent table if needed
	 * @internal dev sts = rft
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public function StoreClub($field_value_array)
    {
		$fva = $field_value_array;
		
		$primary_key_field_name = $this->primaryKey();
		$type_id_field_name		= 'org_type_id';
		$class = __CLASS__;
		// run a search by primary key 
		$row_found_yn = "N";
		if (array_key_exists($primary_key_field_name, $fva)) {
			$pkey_val = $fva[$primary_key_field_name];
			$row = $class::model()->findByPk($pkey_val); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}
		// assert that the type_id is consistent with an Org of Type='Club', org_type_id=2
		if (array_key_exists('org_type_id', $fva)) {
			$type_id_val = (int)$field_value_array[$type_id_field_name];
			if ($type_id_val != 2){
				$fva[$type_id_field_name] = 2;
			}
		}		
		
		if ($row_found_yn === 'Y'){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $row->attributes;
			$row->attributes = $fva;
			$diff = array_diff($bfr_upd,$row->attributes);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$primary_key_field_name];
			}
			// The values must be different. Save the model.
			if ($row->save()){
				return (int)$row->attributes[$primary_key_field_name];
			} else {
				return 0;
			}
			
		} elseif ($row_found_yn === 'N'){
			// Insert
			$model = new $class();
			$model->unsetAttributes(); // clears default values
			$model->attributes = $fva;
			if ($model->save()){
				$id = (int)$model->attributes[$primary_key_field_name];
			} else {
				$id = 0;
			}
			return (int)$id;
		}		
		
		return false;
    } 
		

}
