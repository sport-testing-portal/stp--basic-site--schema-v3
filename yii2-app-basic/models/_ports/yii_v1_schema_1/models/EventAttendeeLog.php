<?php

Yii::import('application.models._base.BaseEventAttendeeLog');

class EventAttendeeLog extends BaseEventAttendeeLog
{
    /**
     * @return EventAttendeeLog
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Event Attendee Log|Event Attendee Logs', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'event_attendee_log_id'		=> Yii::t('app', 'Log ID'),
                'event_id'					=> Yii::t('app', 'Event'),
                'person_id'					=> Yii::t('app', 'Person'),
                'event_attendee_type_id'	=> Yii::t('app', 'Event Attendee Type'),
                'event_rsvp_dt'				=> Yii::t('app', 'Event Rsvp Dt'),
                'event_attendance_dt'		=> Yii::t('app', 'Event Attendance Dt'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'event' => null,
                'eventAttendeeType' => null,
                'person' => null,
        );
    }

}
