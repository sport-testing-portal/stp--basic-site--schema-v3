<?php

Yii::import('application.models._base.BaseCoach');

/*
 * Store()
 * StoreCoachWithPerson()
 * RowCountByPersonId()
 * searchBySelect2()
 * searchTypeBySelect2()
 */
class Coach extends BaseCoach
{

	public static $p_tableName  = 'coach';
	public static $p_primaryKey = 'coach_id';
	public static $p_textColumn = 'person_name_full';
	public static $p_modelName  = 'Coach';

    /**
     * @return Coach
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

	/**
	 *
	 * @param int $n 1=singular, 2=plural
	 * @return string
	 */
    public static function label($n = 1)
    {
        return Yii::t('app', 'Coach|Coaches', $n);
    }

	/**
	 *
	 * @return string The name of the primary key
	 * @internal Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 */
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

	/**
	 *
	 * @return AweActiveRecord::Behavior
	 * @internal Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 */
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Stores this table and its parent table - Person
	 * @param type $field_value_array
	 * @see Person::StorePersonWithOrg($field_value_array)
	 */
	public static function Store($field_value_array)
	{
		//$this->StoreCoachWithPerson($field_value_array);
		self::StoreCoachWithPerson($field_value_array);
	}

	/**
	 * Stores this table and its parent table - Person
	 * @param type $mmava Multiple model attribute values array
	 * @see Person::StorePersonWithOrg($field_value_array)
	 */
	public static function StoreModels($mmava)
	{
		//$this->StoreCoachWithPerson($field_value_array);
		self::StoreCoachWithPerson($mmava);
	}

	/**
	 * AWE Recordset update
	 * Handles BELONGS_TO relations. This is used a a template for all
	 * belongs_to relations. Note that it handles normative yii object
	 * relations and data supplied via a flattened mtfva as well.
	 * @see AthleteTeams::StoreOrgByMtfva()
	 * @internal This was the base code for AthleteTeams::relationHandlerForBelongsTo()
	 * @package Athlete Resumes
	 * @since 0.54.3
	 * @internal Array to process
	 * Coach
     *   coachType => array
     *   person => array
	 *		addresses => array()
	 *		eventAttendeeLogs' => array()
	 *		medias' => array()
	 *		notes' => array()
	 *		org' => array
	 *			orgType' => array
	 *			orgLevel' => array
	 *		personType' => array
     *      personCertifications' => array()
     *      searchCriteriaTemplates' => array()
     *   teamCoaches => array()
	 *
	 */
	public static function StoreCoachByMtfva($mtfva, $allowRedirect=false){

		if(! isset($mtfva['Coach'])){

			return;
		}

		if(array_key_exists('Coach', $mtfva)){
			$coachValues = $mtfva['Coach'];
			if(isset($coachValues['coach_id']) && (int)$coachValues['coach_id']>0){
				$scenario = 'update';
				$pkVal = (int)$coachValues['coach_id'];
				//$model = Coach::model()->findByPk($pkVal);
				$model = Coach::model()->coach()->findByPk($pkVal);

			} else {
				$scenario = 'insert';
				$model    = new Coach();

				// $this->performAjaxValidation($model, 'org-form');
				if(isset($_POST['ajax']) && $_POST['ajax'] === 'coach-form')
				{
					echo CActiveForm::validate($model);
					Yii::app()->end();
				}
			}

            if (isset($mtfva['Coach']['person'])) {
				// by relationName
				$model->person = $mtfva['Coach']['person'];

			} elseif (isset($mtfva['Person'])) {
				// byModelName
				if(array_key_exists('Org', $mtfva)){
					$orgValues = $mtfva['Org'];
				} else {
					$orgValues = [];
				}
				$personValues = $mtfva['Person'];
				$personFva    = array_merge($personValues, $orgValues);
				$coach_person_id = Person::StorePersonWithOrg($personFva);
				$model->person = $mtfva['Person'];

			} else {

				if ($scenario === 'update'){
					$model->person = array();
				}
			}

            if (isset($mtfva['Coach']['coachType'])) {
				// by relationName
				$model->coachType = $mtfva['Coach']['coachType'];

			} elseif (isset($mtfva['CoachType'])) {
				// byModelName
				$model->coachType = $mtfva['CoachType'];

			} else {

				if ($scenario === 'update'){
					$model->coachType = array();
				}
			}

            if (isset($mtfva['Coach']['teamCoaches'])) {
				// by relationName
				$model->teamCoaches = $mtfva['Coach']['teamCoaches'];

			} elseif (isset($mtfva['TeamCoach']) ) {
				// byModelName
				$model->teamCoaches = $mtfva['TeamCoach'];

			} else {
				$model->teamCoaches = array();
			}

			$model->attributes = $mtfva['Coach'];
			if($model->save()) {
				if ($allowRedirect === true){
					$this->redirect(array('view','id' => $model->coach_id));
				}
            } else {
				$errors = $model->getErrors();
				return $errors;
			}
		}

//		if(array_key_exists('Org', $mtfva)){
//			$orgValues = $mtfva['Org'];
//			if(isset($orgValues['org_id']) && (int)$orgValues['org_id']>0){
//				$scenario = 'update';
//				$pkVal = (int)$orgValues['org_id'];
//				$model = Org::model()->findByPk($pkVal);
//
//			} else {
//				$scenario = 'insert';
//				$model    = new Org;
//
//				// $this->performAjaxValidation($model, 'org-form');
//				if(isset($_POST['ajax']) && $_POST['ajax'] === 'org-form')
//				{
//					echo CActiveForm::validate($model);
//					Yii::app()->end();
//				}
//			}
//
//            if (isset($mtfva['Org']['orgLevel'])) {
//				// by relationName
//				$model->orgLevel = $mtfva['Org']['orgLevel'];
//
//			} elseif (isset($mtfva['OrgLevel'])) {
//				// byModelName
//				$model->orgLevel = $mtfva['OrgLevel'];
//
//			} else {
//
//				if ($scenario === 'update'){
//					$model->orgLevel = array();
//				}
//			}
//
//
//            if (isset($mtfva['Org']['orgType'])) {
//				// by relationName
//				$model->orgType = $mtfva['Org']['orgType'];
//
//			} elseif (isset($mtfva['OrgType']) ) {
//				// byModelName
//				$model->orgLevel = $mtfva['OrgType'];
//
//			} else {
//				$model->orgLevel = array();
//			}
//
//			$model->attributes = $mtfva['Org'];
//			if($model->save()) {
//				if ($allowRedirect === true){
//					$this->redirect(array('view','id' => $model->org_id));
//				}
//            }
//		}

	}

	/**
	 *
	 */
	public static function storeCoachByMtfva_testHarness() {
		$coachData = [
			'Coach'=>[
					'coach_id' => '77',
					'person_id' => '159',
					'coach_type_id' => '1',
					'coach_team_coach_id' => '15',
					'age_group_id' => '7',
					'coach_specialty' => null,
					'coach_certifications' => 'NSCAA Level 3',
					'coach_comments' => null,
					'coach_qrcode_uri' => null,
					'coach_info_source_scrape_url' => null,
					'created_dt' => '2015-01-12 13:05:51',
					'updated_dt' => '2015-02-03 12:46:09',
					'created_by' => '2',
					'updated_by' => '2',
					'coachType' => array
					(
						'coach_type_id' => '1',
						'coach_type_name' => 'Head Coach',
						'display_order' => null,
						'coach_type_desc_short' => null,
						'coach_type_desc_long' => null,
						'created_dt' => '2014-05-29 17:28:46',
						'updated_dt' => '2014-05-29 17:28:46',
						'created_by' => '1',
						'updated_by' => '1',
					),
					'person' => array
					(
						'person_id' => '159',
						'org_id' => '1232',
						'app_user_id' => null,
						'person_type_id' => '2',
						'user_id' => '2',
						'person_name_prefix' => null,
						'person_name_first' => 'Jason',
						'person_name_middle' => 'R.',
						'person_name_last' => 'Smith',
						'person_name_suffix' => null,
						'person_name_full' => 'Jason R. Smith',
						'person_phone_personal' => null,
						'person_email_personal' => null,
						'person_phone_work' => null,
						'person_email_work' => null,
						'person_position_work' => null,
						'gender_id' => '2',
						'person_image_headshot_url' => 'DemoCoach1.jpg',
						'person_name_nickname' => null,
						'person_date_of_birth' => '1982-05-11 00:00:00',
						'person_height' => null,
						'person_weight' => null,
						'person_tshirt_size' => null,
						'person_high_school__graduation_year' => null,
						'person_college_graduation_year' => null,
						'person_college_commitment_status' => null,
						'person_addr_1' => '123 Main Street',
						'person_addr_2' => 'Unit B',
						'person_addr_3' => null,
						'person_city' => 'Raleigh',
						'person_postal_code' => '27601',
						'person_country' => null,
						'person_country_code' => 'USA',
						'person_state_or_region' => 'NC',
						'org_affiliation_begin_dt' => null,
						'org_affiliation_end_dt' => null,
						'person_website' => null,
						'person_profile_url' => null,
						'person_profile_uri' => null,
						'created_dt' => '2015-01-12 13:01:37',
						'updated_dt' => '2015-05-24 15:59:50',
						'created_by' => '2',
						'updated_by' => '11',
						'addresses' => array(),
						'eventAttendeeLogs' => array(),
						'medias' => array(),
						'notes' => array(),
						'org' => array
						(
							'org_id' => '1232',
							'org_type_id' => '2',
							'org_level_id' => null,
							'org_name' => 'CASL',
							'org_governing_body' => null,
							'org_ncaa_clearing_house_id' => null,
							'org_website_url' => null,
							'org_twitter_url' => null,
							'org_facebook_url' => null,
							'org_phone_main' => null,
							'org_email_main' => null,
							'org_addr1' => null,
							'org_addr2' => null,
							'org_addr3' => null,
							'org_city' => null,
							'org_state_or_region' => null,
							'org_postal_code' => null,
							'org_country_code_iso3' => null,
							'created_dt' => '2015-02-02 11:48:07',
							'updated_dt' => '2015-02-03 21:31:22',
							'created_by' => '2',
							'updated_by' => '11',
							'orgType' => array
							(
								'org_type_id' => '2',
								'org_type_name' => 'Club',
								'org_type_sub_type' => null,
								'org_type_desc_short' => null,
								'org_type_desc_long' => null,
								'created_dt' => '2014-12-14 08:40:55',
								'updated_dt' => '2015-12-17 12:11:15',
								'created_by' => '1',
								'updated_by' => '2',
							),
						),
						'personType' => array
						(
							'person_type_id' => '2',
							'person_type_name' => 'Coach',
							'person_type_desc_short' => null,
							'person_type_desc_long' => null,
							'created_dt' => '2014-05-31 21:25:04',
							'updated_dt' => '2014-08-17 16:52:20',
							'created_by' => '1',
							'updated_by' => '1',
						),
						//'personCertifications' => array(),
						//'searchCriteriaTemplates' => array(),
					),
					//'teamCoaches' => array(),
				],
			];

//		$result1 = Coach::StoreCoachByMtfva($coachData, $allowRedirect=false);
//		if (is_numeric($result1)){
//			return $result1;
//		} elseif (is_array($result1)){
//			throw new CException('Static Coach had an error', 700);
//		}

		$Coach = new Coach();
		$result2 = $Coach->StoreCoachByMtfva($coachData, $allowRedirect=false);
		if ($Coach->hasErrors()){
			$errors = $Coach->getErrors();
			return $errors;
		}
		return $coachData;
	}

	/**
	 * @param array[] $field_value_array Description
     * @return int $coach_id
	 * @version 1.1.0
	 * @internal CEC 1.1.0 See the person model for a more elegant solution
	 * @see Person::StorePersonWithOrg($field_value_array)
	 * @internal dev-sts = rft
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public static function StoreCoachWithPerson($field_value_array)
    {
		$pk_name	= 'coach_id'; //$this->primaryKey();
		$fk_name	= 'person_id';
		$class		= __CLASS__; // Coach
		$fva		= $field_value_array;
		// run a search by primary key
		$coach_row_found_yn = "N";
		if (array_key_exists($pk_name, $fva)) {
			$pkey_val = $fva[$pk_name];
			$coach_row = $class::model()->findByPk($pkey_val); // returns object
			if (! empty($coach_row)){
				$coach_row_found_yn = "Y";
			}
		}
		if ($coach_row_found_yn == "N" && array_key_exists($fk_name, $fva)) {
			// run a search by foreign key
			$fkey_val = (int)$fva[$fk_name];
			$coach_row = $class::model()->findByAttributes(array($fk_name=>$fkey_val)); // returns object
			if (! empty($coach_row)){
				$coach_row_found_yn = "Y";
			}
		}


		if ($coach_row_found_yn == "Y"){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $coach_row->attributes;
			$coach_row->attributes = $fva;
			$diff = array_diff($bfr_upd,$coach_row->attributes);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$coach_row->attributes[$pk_name];
			}
			// The values must be different. Save the model.
			if ($coach_row->save()){
				return (int)$coach_row->attributes[$pk_name];
			} else {
				return 0;
			}

		} else {
			// Insert person and coach
			if (!isset($fva['person_type_id'])){
				$fva['person_type_id'] = 2; // coach
			}
			$person_id = Person::StorePersonWithOrg($fva);
			if (!isset($fva['person_id'])){
				$fva['person_id'] = $person_id; // coach
			}
			$model = new $class();
			$model->unsetAttributes(); // clears default values
			$model->attributes = $fva;
			if ($model->save()){
				$id = (int)$model->attributes[$pk_name];
			} else {
				$id = 0;
			}

			return (int)$id; // coach_id
		}

		return false;
    }

	/**
	 *
	 * @return CActiveRecord (Person + PersonType + Org + OrgType + Coach + CoachType, etc.)
	 * @example $model = Coach::model()->coach()->findByPk(77); // coach_id
	 * @see $model = Person::model()->coach()->findByPk(159); // person_id
	 * @version 0.2.0
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 *
	 */
	public function coach() {
		$personTypeID = 2;
		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
				'condition'=>'person.person_type_id = :person_type_id',
				'params'=>[':person_type_id'=>$personTypeID],
				'with'=>[
					'person'=>[
						'with'=>[
							'personType',
							'org'=>[
								'with'=>[
									'orgType',
									'orgLevel',
								],
							],
							'addresses',
							'medias',
							'notes',
							'personCertifications',
							'eventAttendeeLogs',
							'searchCriteriaTemplates',
						],
					],
					'coachType',
					'teamCoaches'=>[
						'with'=>[
							'team',
						],
					],
				],
			]
		);

		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}

	/**
	 * @param int
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public function RowCountByPersonId($person_id)
    {
		$class = __CLASS__;
		$rows  = $class::model()->findAllByAttributes(array("person_id"=>$person_id));

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rows, 10, true);
		if (is_array($rows) && count($rows, COUNT_RECURSIVE) > 0){
			$cnt = count($rows[0]);
		} else {
			$cnt = 0;
		}
		//$cnt = count($rows[0]);
		return $cnt;

    }


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchBySelect2($search = null, $id = null, $raw = null, $limit=null)
	{
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$parentTable = 'person';
		$parentFkey  = 'person_id';

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$out = ['more' => false];
		if (!is_null($search)) {
			// <editor-fold defaultstate="collapsed" desc="SQL examples">
			// Default search method SQL
//			$sql = "select $pkName as id, $textColumn as `text` "
//				 . "from $tableName "
//				 . "where $textColumn LIKE :$textColumn "
//				 . "order by $textColumn";
			// Custom search SQL
//			$sql = "select coach.coach_id as id, person.person_name_full as `text` "
//				 . "from person "
//				 . "inner join coach on person.person_id = coach.person_id "
//				 . "where person.person_name_full LIKE :$textColumn "
//				 . "order by person.person_name_full";
			// </editor-fold>
			$sql = "select $tableName.$pkName as id, $parentTable.$textColumn as `text` "
				 . "from $parentTable "
				 . "inner join $tableName on $parentTable.$parentFkey = $tableName.$parentFkey "
				 . "where $parentTable.$textColumn LIKE :$textColumn "
				 . "order by $parentTable.$textColumn";
			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%'];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchTypeBySelect2($search = null, $id = null, $raw = null, $limit=null, $typeName=null)
	{
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$typeTable  = $tableName . '_type';
		$fkeyName   = $tableName . '_type_id';

		$out = ['more' => false];
		if (!is_null($search)) {
			// default search template SQL
//			$sql = "select $tableName.$pkName as id, $tableName.$textColumn as `text` "
//				 . "from $tableName inner join $typeTable on $tableName.$fkeyName = $typeTable.$fkeyName "
//				 . "where $typeTable." .$typeTable . "_name = :typeName and $tableName.$textColumn LIKE :$textColumn "
//				 . "order by $tableName.$textColumn";
			// custom search SQL
			$sql = "select coach.coach_id as id, person.person_name_full as `text` "
				 . "from person inner join coach on person.person_id = coach.person_id "
				 . "inner join coach_type on coach.coach_type_id = coach_type.coac_type_id "
				 . "where coach_type.coach_type_name = :typeName and person.person_name_full LIKE :$textColumn "
				 . "order by person.person_name_full";
			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%', ':typeName'=>$typeName];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


	/**
	 * Select unlinked coaches
	 * @package Athlete Test Results Linkage & Team Member Linkage UI
	 *
	 */
	protected static function fetchCoachesMissingEventLinkage($param)
	{

	}

	/**
	 * Select unlinked coaches
	 * @package Athlete Test Results Linkage & Team Member Linkage UI
	 *
	 */
	protected static function fetchCoachesMissingTeamLinkage($param)
	{

	}

	/**
	 * A manually revised list of Yii relation types
	 * We can thik that a PARENT table will be the one that doesn't have a foreign key,
	 *   and a CHILD table as the one who "depends" on the parent table, that is, it has a foreign key.
	 *   Given that, a CHILD BELONGS_TO a PARENT and a PARENT HAS_ONE CHILD
	 * @return array
	 * @see http://www.yiiframework.com/wiki/181/relations-belongs_to-versus-has_one/
	 */
    public function relations() {
        return array(
            'campCoaches' => array(self::HAS_MANY, 'CampCoach', 'coach_id'),
            'campSessionCoaches' => array(self::HAS_MANY, 'CampSessionCoach', 'coach_id'),
            'campSessionPlayerEvals' => array(self::HAS_MANY, 'CampSessionPlayerEval', 'coach_id'),
            'ageGroup' => array(self::BELONGS_TO, 'AgeGroup', 'age_group_id'),
            'coachType' => array(self::BELONGS_TO, 'CoachType', 'coach_type_id'),
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'coachTeamCoach' => array(self::BELONGS_TO, 'TeamCoach', 'coach_team_coach_id'),
            'coachSports' => array(self::HAS_MANY, 'CoachSport', 'coach_id'),
            'schoolCoaches' => array(self::HAS_MANY, 'SchoolCoach', 'coach_id'),
            'teamCoaches' => array(self::HAS_MANY, 'TeamCoach', 'coach_id'),
        );
    }


	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownList() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		// Place United States at the top of the list
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		// example of adding a primary item at the top of the list
//		$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
//				. "order by case when $textColumn = 'United States' then 0 else $textColumn end";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		return $list;
	}

	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownListWithGrouping() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql=[];
		// Place United States at the top of the list
		$sql[0] = "select $pkName as id, $textColumn as `text` "
				. "from $tableName order by $textColumn";

//		$sql[1] = "select t.team_id as `id`, t.team_name as `text`, o.org_name as `group`
//				from team t
//					inner join org o on t.org_id = o.org_id
//				where t.team_name LIKE '%" . $search ."%'";

		$sql[2] = BaseModel::sqlTrim("select
			concat(o.org_name, ' (', ot.org_type_name, ')') as group1,
			concat(ot.org_type_name, '-', o.org_name)       as group2,
			t.team_id as `id`, t.team_name as `text`,
			o.org_name as `group`
			from team t
				inner join org      o  on t.org_id      =  o.org_id
				inner join org_type ot on o.org_type_id = ot.org_type_id
			order by ot.org_type_name, o.org_name, t.team_name;");

		// 	-- where t.team_name LIKE '%" . $search ."%'"
		$sql[3] = BaseModel::sqlTrim("select
			concat(o.org_name, ' (', ot.org_type_name, ')') as group1,
			concat(ot.org_type_name, '-', o.org_name)       as group2,
			c.coach_id as `id`, p.person_name_full as `text`,
			o.org_name as `group`
			from coach c
				inner join person   p  on c.person_id   = p.person_id
				inner join org      o  on p.org_id      = o.org_id
				inner join org_type ot on o.org_type_id = ot.org_type_id
			order by ot.org_type_name, o.org_name, p.person_name_full");

//			-- where t.team_name LIKE '%" . $search ."%'"
// 			-- inner join org_type o  on team.org_id   = org.org_id

		// example of adding a primary item at the top of the list
//		$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
//				. "order by case when $textColumn = 'United States' then 0 else $textColumn end";
		$cmd  = Yii::app()->db->createCommand($sql[3]);
		$data = $cmd->queryAll($fetchAssociative=true);
		//$list = CHtml::listData( $data, 'id', 'text'); //[]
		$list = CHtml::listData( $data, 'id', 'text','group1'); //[]
		return $list;
	}

	/**
	 * Get a coach name using a coach pkey eg coach.coach_id
	 * @param int|string $id coach.coach_id
	 * @return string
	 * @see CoachFlat::coachName()
	 */
	public static function coachName($id) {

		// Next code block is from CoachFlat for comparison
//		$model = CoachFlat::model()->findByPk($id);
//		if (!is_null($model)){
//			$coachName = $model->person_name_full;
//		} else {
//			$coachName = '';
//		}

		$model = Coach::model()->with('person')->findByPk($id);
		$coachName = '';
		if (!is_null($model)){
			$CoachPerson = $model->getRelated('person');
			if (!is_null($CoachPerson)){
				$coachName = $CoachPerson->person_name_full;
			} else {
				//$coachName = '';
				$codeFlow = 'CoachPerson model object is null';
			}
		} else {
			//$CoachName = '';
			$codeFlow = 'Coach model object is null';
		}

		return $coachName;
	}

}
