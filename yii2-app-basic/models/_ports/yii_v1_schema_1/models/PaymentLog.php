<?php

Yii::import('application.models._base.BasePaymentLog');

class PaymentLog extends BasePaymentLog
{
    /**
     * @return PaymentLog
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Payment Log|Payment Logs', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'payment_log_id'		=> Yii::t('app', 'Log ID'),
                'payment_type_id'		=> Yii::t('app', 'Payment Type ID'),
                'data_entity_id'		=> Yii::t('app', 'Data Entity ID'),
                'subscription_id'		=> Yii::t('app', 'Subscription ID'),
                'org_id'				=> Yii::t('app', 'Org ID'),
                'person_id'				=> Yii::t('app', 'Person ID'),
                'payment_amt'			=> Yii::t('app', 'Payment Amt'),
                'payment_dt'			=> Yii::t('app', 'Payment Dt'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'org' => null,
                'paymentType' => null,
                'person' => null,
                'subscription' => null,
                'paymentXfers' => null,
        );
    }
}
