<?php

Yii::import('application.models._base.BaseCoachFlat');

class CoachFlat extends BaseCoachFlat
{

	public static $p_tableName  = 'vwCoachFlat';
	public static $p_primaryKey = 'coach_id';
	public static $p_textColumn = 'person_name_full';
	public static $p_modelName  = 'CoachFlat';

	public static $p_componentModels=[
		'Coach','CoachType','Person','PersonType','Org','OrgType','OrgLevel','Gender'];


    /**
     * @return CoachFlat
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Coach|Coaches', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        //return $this->tableName() . '_id';
		return 'coach_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
		// internal attributes need to be written to the individual tables
		// @todo consider using the fieldxlation routine here so a call to this->model->save
		//   will actually parse the model's attributes into an mfva and store all the values.
		if (true===true){
			return false; // data updates disallowed
		}
        //return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'user_id'       => Yii::t('app', 'User'),
                'username'      => Yii::t('app', 'Username'),
                'person_id'            => Yii::t('app', 'Person'),
                'org_id'               => Yii::t('app', 'Org'),
                'coach_id'             => Yii::t('app', 'Coach'),
                'app_user_id'          => Yii::t('app', 'App User'),
                'person_type_id'       => Yii::t('app', 'Person Type'),
                'person_type_name'     => Yii::t('app', 'Person Type Name'),
                'coach_type_id'        => Yii::t('app', 'Coach Type'),
                'coach_type_name'      => Yii::t('app', 'Coach Type Name'),
                'coach_team_coach_id'  => Yii::t('app', 'Coach Team Coach'),
                'org_name'             => Yii::t('app', 'Org Name'),
                'org_type_id'          => Yii::t('app', 'Org Type'),
                'org_type_name'        => Yii::t('app', 'Org Type Name'),
                'person_name_prefix'   => Yii::t('app', 'Name Prefix'),
                'person_name_first'    => Yii::t('app', 'Name First'),
                'person_name_middle'   => Yii::t('app', 'Name Middle'),
                'person_name_last'     => Yii::t('app', 'Name Last'),
                'person_name_suffix'   => Yii::t('app', 'Name Suffix'),
                'person_name_full'     => Yii::t('app', 'Name Full'),
                'gender_id'            => Yii::t('app', 'Coach Gender'),
                'gender_code'          => Yii::t('app', 'Gender Code'),
                'gender_desc'          => Yii::t('app', 'Gender Desc'),
                'person_phone_personal'     => Yii::t('app', 'Coach Phone Personal'),
                'person_email_personal'     => Yii::t('app', 'Coach Email Personal'),
                'person_phone_work'         => Yii::t('app', 'Coach Phone Work'),
                'person_email_work'         => Yii::t('app', 'Coach Email Work'),
                'person_position_work'      => Yii::t('app', 'Coach Position Work'),
                'person_image_headshot_url' => Yii::t('app', 'Coach Image Headshot Url'),
                'person_name_nickname'      => Yii::t('app', 'Coach Nickname'),
                'person_date_of_birth'      => Yii::t('app', 'Coach Date Of Birth'),
                'person_height'             => Yii::t('app', 'Coach Height'),
                'person_weight'             => Yii::t('app', 'Coach Weight'),
                'person_tshirt_size'        => Yii::t('app', 'Coach Tshirt Size'),
                'person_high_school__graduation_year' => Yii::t('app', 'High School Graduation Year'),
                'person_college_graduation_year'      => Yii::t('app', 'College Graduation Year'),
                'person_college_commitment_status'    => Yii::t('app', 'College Commitment Status'),
                'person_addr_1'             => Yii::t('app', 'Coach Addr 1'),
                'person_addr_2'             => Yii::t('app', 'Coach Addr 2'),
                'person_addr_3'             => Yii::t('app', 'Coach Addr 3'),
                'person_city'               => Yii::t('app', 'Coach City'),
                'person_postal_code'        => Yii::t('app', 'Coach Postal Code'),
                'person_country'            => Yii::t('app', 'Coach Country'),
                'person_country_code'       => Yii::t('app', 'Coach Country Code'),
                'person_state_or_region'    => Yii::t('app', 'Coach State Or Region'),
                'org_affiliation_begin_dt'  => Yii::t('app', 'Org Affiliation Begin Dt'),
                'org_affiliation_end_dt'    => Yii::t('app', 'Org Affiliation End Dt'),
                'person_website'            => Yii::t('app', 'Coach Website'),
                'person_profile_url'        => Yii::t('app', 'Coach Profile Url'),
                'person_profile_uri'        => Yii::t('app', 'Coach Profile Uri'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'person_created_by_username' => Yii::t('app', 'Person Created By Username'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'person_updated_by_username' => Yii::t('app', 'Person Updated By Username'),
                'age_group_id' => Yii::t('app', 'Age Group'),
                'coach_specialty'            => Yii::t('app', 'Coach Specialty'),
                'coach_certifications'       => Yii::t('app', 'Coach Certifications'),
                'coach_comments'             => Yii::t('app', 'Coach Comments'),
                'coach_qrcode_uri'           => Yii::t('app', 'Coach Qrcode Uri'),
                'coach_info_source_scrape_url' => Yii::t('app', 'Coach Info Source Scrape Url'),
                'coach_created_by_username'    => Yii::t('app', 'Coach Created By Username'),
                'coach_created_by'             => Yii::t('app', 'Coach Created By'),
                'coach_updated_by_username'    => Yii::t('app', 'Coach Updated By Username'),
                'coach_updated_by'             => Yii::t('app', 'Coach Updated By'),
                'org_level_id'               => Yii::t('app', 'Org Level'),
                'org_level_name'             => Yii::t('app', 'Org Level Name'),
                'org_governing_body'         => Yii::t('app', 'Org Governing Body'),
                'org_ncaa_clearing_house_id' => Yii::t('app', 'Org Ncaa Clearing House'),
                'org_website_url'            => Yii::t('app', 'Org Website Url'),
                'org_twitter_url'            => Yii::t('app', 'Org Twitter Url'),
                'org_facebook_url'           => Yii::t('app', 'Org Facebook Url'),
                'org_phone_main'             => Yii::t('app', 'Org Phone Main'),
                'org_email_main'             => Yii::t('app', 'Org Email Main'),
                'org_addr1'                  => Yii::t('app', 'Org Addr1'),
                'org_addr2'                  => Yii::t('app', 'Org Addr2'),
                'org_addr3'                  => Yii::t('app', 'Org Addr3'),
                'org_city'                   => Yii::t('app', 'Org City'),
                'org_state_or_region'        => Yii::t('app', 'Org State Or Region'),
                'org_postal_code'            => Yii::t('app', 'Org Postal Code'),
                'org_country_code_iso3'      => Yii::t('app', 'Org Country Code Iso3'),
        );
    }

	/**
	 * Stores this table and its parent tables - Org, OrgType, Person
	 * @param array $fieldValueArray An associative array of fieldName:fieldValue
	 * @return int $coach_id
	 * @version 1.0.0
	 * @internal CEC v2.0.0
	 * @internal dev sts = (upd=golden,ins=rft)
	 */
	public static function Store($fieldValueArray, $returnScmda=false){
		// @todo update|insert into the 6 base tables used in the view
		if (!is_array($fieldValueArray)){
			throw new CException("fieldValueArray is a required parameter", 707);
		}
		$fva   = $fieldValueArray;
		$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();

		$componentModels = self::$p_componentModels;

		$localDebug=false;
		if ($localDebug){
			// get all coachFlat attribute names, used for reference during debugging
			//$attibuteNames   = self::model()->attributeNames();
			// get coach relations used for reference during debugging
			//$allRelations = BaseModel::fetchModelRelationsMultiple($componentModels);
		}

		// Store person values
		$pkName = self::model()->primaryKey(); // coach_id
		if (array_key_exists($pkName, $fva)){
			$pkVal = $fva[$pkName]; // update
			$Coach = Coach::model()->findByPk($pkVal);
			if (!is_null($Coach)){
				$scmda['pkeys'][$pkName] = $pkVal;            // value verified
				$scmda['scenario']['Coach'] = $Coach->getScenario();   // update
			}
		}

		if(is_null($Coach)){
			$Coach = new Coach();
			$scmda['scenario']['Coach'] = $Coach->getScenario();      //
			$person_id = '';
		} else {
			$person_id = $Coach->person_id;
			$scmda['keys']['person_id'] = $person_id;
		}
		// if person_id is missing in the fva then add it
		if (!array_key_exists('person_id', $fva)){
			$fva['person_id']=$person_id;
		}
		// Store person and org values
		$person_id = Person::StorePersonWithOrg($fva);
		if ($Coach->isNewRecord && (int)$person_id > 0){
			$Coach->person_id = $person_id;
			$scmda['keys']['person_id'] = $person_id;
		}

		// Run a delta check to see if the coach values should be saved or ignored
		$coachBeforeAttrUpdate = $Coach->attributes;
		$Coach->setAttributes($fva);
		$coachAfterAttrUpdate = $Coach->attributes;
		$diffBeforeSave = array_diff($coachBeforeAttrUpdate, $coachAfterAttrUpdate);
		if (count($diffBeforeSave) == 0){
			$scmda['success']['Coach'] = "no changes to save on pk $pkVal";
			if ($returnScmda){
				return $scmda;
			} else {
				return $pkVal;
			}
		}

		// Store coach values
		$runValidation = true;
		if ($Coach->save($runValidation)){
			if ($scmda['scenario']['Coach']==='insert'){
				$pkVal = $Coach->getPrimaryKey();
				$scmda['pkeys'][$pkName] = $pkVal;
			}
			$coachAfterSave = $Coach->attributes;
			$diffAfterSave  = array_diff($coachBeforeAttrUpdate, $coachAfterSave);
			$scmda['modelsTouched']['Coach'][] = $diffAfterSave;
			$scmda['success']['Coach'] = "inserted pk $pkVal";
		} else {
			$scmda['errors']['Coach'] = $Coach->getErrors();
		}

		// @todo Add on-the fly notifications to support@gsm for key subscribers
//		if ($Coach->hasErrors()){
//			if ($user->subscribedToRapidResponse){
//				send notification to support desk ASAP for VIP
//			}
//		}

		if ($returnScmda){
			return $scmda;
		} else {
			return $pkVal;
		}

	}

		/**
	 * Stores this table and its parent tables - Org, OrgType, Person
	 * @param array $fieldValueArray An associative array of fieldName:fieldValue
	 * @return int $coach_id
	 * @version 1.0.0
	 * @internal CEC v3.0.0
	 * @internal Development Status = code construction
	 */
	public static function StoreModels($fieldValueArray, $returnScmda=false){
		// @todo update|insert into the 6 base tables used in the view
		if (!is_array($fieldValueArray)){
			throw new CException("fieldValueArray is a required parameter", 707);
		}
		$fva   = $fieldValueArray;
		$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();

		$componentModels = self::$p_componentModels;

		$localDebug=false;
		if ($localDebug){
			// get all coachFlat attribute names, used for reference during debugging
			//$attibuteNames   = self::model()->attributeNames();
			// get coach relations used for reference during debugging
			//$allRelations = BaseModel::fetchModelRelationsMultiple($componentModels);
		}

		// Store person values
		$pkName = self::model()->primaryKey(); // coach_id
		if (array_key_exists($pkName, $fva)){
			$pkVal = $fva[$pkName]; // update
			$Coach = Coach::model()->findByPk($pkVal);
			$CoachFlat = CoachFlat::model()->findByPk($pkVal);
			if (!is_null($CoachFlat)){
				$coach_id  = $CoachFlat->coach_id;
				$person_id = $CoachFlat->person_id;
				$org_id    = $CoachFlat->org_id;
			}
			if ((int)$org_id > 0){
				// @todo fetch org-person-coach?
				// Org
				$models = Org::model()->orgLazyLoad($model, $fva);
				$CoachYii = Coach::model()->coach()->findByPk($pkVal);
				$attrDeep = BaseModel::getAttributesDeepAsArray($CoachYii);
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($attrDeep, 10, true);
			}
			if (!is_null($Coach)){
				$person_id = $Coach->person_id;
				$org_id    = $Coach->org_id;

				$scmda['pkeys'][$pkName] = $pkVal;            // value verified
				$scmda['keys'][$pkName] = $pkVal;            // value verified
				$scmda['pkeys'][$pkName] = $pkVal;            // value verified

				$scmda['scenario']['Coach'] = $Coach->getScenario();   // update
			}
		}

		if(is_null($Coach)){
			$Coach = new Coach();
			$scmda['scenario']['Coach'] = $Coach->getScenario();      //
			$person_id = '';
		} else {
			$person_id = $Coach->person_id;
			$scmda['keys']['person_id'] = $person_id;
		}
		// if person_id is missing in the fva then add it
		if (!array_key_exists('person_id', $fva)){
			$fva['person_id']=$person_id;
		}
		// Store person and org values
		$person_id = Person::StorePersonWithOrg($fva);
		if ($Coach->isNewRecord && (int)$person_id > 0){
			$Coach->person_id = $person_id;
			$scmda['keys']['person_id'] = $person_id;
		}

		// Run a delta check to see if the coach values should be saved or ignored
		$coachBeforeAttrUpdate = $Coach->attributes;
		$Coach->setAttributes($fva);
		$coachAfterAttrUpdate = $Coach->attributes;
		$diffBeforeSave = array_diff($coachBeforeAttrUpdate, $coachAfterAttrUpdate);
		if (count($diffBeforeSave) == 0){
			$scmda['success']['Coach'] = "no changes to save on pk $pkVal";
			if ($returnScmda){
				return $scmda;
			} else {
				return $pkVal;
			}
		}

		// Store coach values
		$runValidation = true;
		if ($Coach->save($runValidation)){
			if ($scmda['scenario']['Coach']==='insert'){
				$pkVal = $Coach->getPrimaryKey();
				$scmda['pkeys'][$pkName] = $pkVal;
			}
			$coachAfterSave = $Coach->attributes;
			$diffAfterSave  = array_diff($coachBeforeAttrUpdate, $coachAfterSave);
			$scmda['modelsTouched']['Coach'][] = $diffAfterSave;
			$scmda['success']['Coach'] = "inserted pk $pkVal";
		} else {
			$scmda['errors']['Coach'] = $Coach->getErrors();
		}

		// @todo Add on-the fly notifications to support@gsm for key subscribers
//		if ($Coach->hasErrors()){
//			if ($user->subscribedToRapidResponse){
//				send notification to support desk ASAP for VIP
//			}
//		}

		if ($returnScmda){
			return $scmda;
		} else {
			return $pkVal;
		}

	}


	/**
	 * Get a coach name using a coach pkey eg coach.coach_id
	 * @param int|string $id
	 * @return string
	 * @see Coach::coachName()
	 * @example $coachName = CoachFlat::coachName($coachID);
	 * @internal Development Status = Golden!
	 */
	public static function coachName($id) {

		$model = CoachFlat::model()->findByPk($id);
		if (!is_null($model)){
			$coachName = $model->person_name_full;
		} else {
			$coachName = '';
		}

		return $coachName;
	}


	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownListWithGrouping() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql=[];
		// Place United States at the top of the list
		$sql[0] = "select $pkName as id, $textColumn as `text` "
				. "from $tableName order by $textColumn";

//		$sql[1] = "select t.team_id as `id`, t.team_name as `text`, o.org_name as `group`
//				from team t
//					inner join org o on t.org_id = o.org_id
//				where t.team_name LIKE '%" . $search ."%'";

		$sql[2] = BaseModel::sqlTrim("select
			concat(o.org_name, ' (', ot.org_type_name, ')') as group1,
			concat(ot.org_type_name, '-', o.org_name)       as group2,
			t.team_id as `id`, t.team_name as `text`,
			o.org_name as `group`
			from team t
				inner join org      o  on t.org_id      =  o.org_id
				inner join org_type ot on o.org_type_id = ot.org_type_id
			order by ot.org_type_name, o.org_name, t.team_name;");

		// 	-- where t.team_name LIKE '%" . $search ."%'"
		$sql[3] = BaseModel::sqlTrim("select
			concat(o.org_name, ' (', ot.org_type_name, ')') as group1,
			concat(ot.org_type_name, '-', o.org_name)       as group2,
			c.coach_id as `id`, p.person_name_full as `text`,
			o.org_name as `group`
			from coach c
				inner join person   p  on c.person_id   = p.person_id
				inner join org      o  on p.org_id      = o.org_id
				inner join org_type ot on o.org_type_id = ot.org_type_id
			order by ot.org_type_name, o.org_name, p.person_name_full");

//			-- where t.team_name LIKE '%" . $search ."%'"
// 			-- inner join org_type o  on team.org_id   = org.org_id

		// example of adding a primary item at the top of the list
//		$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
//				. "order by case when $textColumn = 'United States' then 0 else $textColumn end";
		$cmd  = Yii::app()->db->createCommand($sql[3]);
		$data = $cmd->queryAll($fetchAssociative=true);
		//$list = CHtml::listData( $data, 'id', 'text'); //[]
		$list = CHtml::listData( $data, 'id', 'text','group1'); //[]
		return $list;
	}

}
