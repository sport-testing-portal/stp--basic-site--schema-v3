<?php

/**
 * This is the model class for table "app_parm".
 *
 * The followings are the available columns in table 'app_parm':
 * @property integer $app_parm_id
 * @property string $app_parm_type
 * @property integer $ap_int
 * @property string $ap_varchar
 * @property string $ap_datetime
 * @property string $ap_comment
 * @property string $updated_dt
 * @property string $created_dt
 * @property integer $updated_by
 * @property integer $created_by
 */
class Checkvist extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'app_parm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ap_int, updated_by, created_by', 'numerical', 'integerOnly'=>true),
			array('app_parm_type', 'length', 'max'=>100),
			array('ap_varchar, ap_comment', 'length', 'max'=>1024),
			array('ap_datetime, updated_dt, created_dt', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('app_parm_id, app_parm_type, ap_int, ap_varchar, ap_datetime, ap_comment, updated_dt, created_dt, updated_by, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'app_parm_id' => 'App Parm',
			'app_parm_type' => 'App Parm Type',
			'ap_int' => 'Ap Int',
			'ap_varchar' => 'Ap Varchar',
			'ap_datetime' => 'Ap Datetime',
			'ap_comment' => 'Ap Comment',
			'updated_dt' => 'Updated Dt',
			'created_dt' => 'Created Dt',
			'updated_by' => 'Updated By',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('app_parm_id',$this->app_parm_id);
		$criteria->compare('app_parm_type',$this->app_parm_type,true);
		$criteria->compare('ap_int',$this->ap_int);
		$criteria->compare('ap_varchar',$this->ap_varchar,true);
		$criteria->compare('ap_datetime',$this->ap_datetime,true);
		$criteria->compare('ap_comment',$this->ap_comment,true);
		$criteria->compare('updated_dt',$this->updated_dt,true);
		$criteria->compare('created_dt',$this->created_dt,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AppParm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        // Added by Dana Byrd via: yii/framework/gii/generators/model/templates/default/model.php
        public function behaviors(){
           return array(
              'CTimestampBehavior' => array(
                 'class'               => 'zii.behaviors.CTimestampBehavior',
                 'createAttribute'     => 'created_dt',
                 'updateAttribute'     => 'updated_dt',
                 'timestampExpression' => 'date("Y-m-d H:i:s")',
                 'setUpdateOnCreate'   => true
              )
           );
        }         
}
