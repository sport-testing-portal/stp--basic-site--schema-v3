<?php

Yii::import('application.models._base.BaseTeamCoach');

class TeamCoach extends BaseTeamCoach
{
    /**
     * @return TeamCoach
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Team Coach|Team Coaches', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 *
	 * @param type $parentID
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchIncludingTeam($parentID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        $criteria->with=array('team');
        $criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.team_id',$parentID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort;
        $sort->attributes = array(
            'team_name' => array(
            'asc' => 'team_name',
            'desc' => 'team_name DESC',
            ), '*', /* Treat all other columns normally */
        );
        /* End: Sort on related Model's columns */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }

}
