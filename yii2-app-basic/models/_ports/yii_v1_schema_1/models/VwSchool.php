<?php

Yii::import('application.models._base.BaseVwSchool');

class VwSchool extends BaseVwSchool
{
	
	const MILE_FACTOR=69.1;
	const KILOMETER_FACTOR=111.045;
	
	
    /**
     * @return VwSchool
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'VwSchool|VwSchools', $n);
    }
    
    
    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();            
        }
        return parent::beforeSave();
    }  
	
	/**
	 * Select data by distance
	 * @param decimal $origLat
	 * @param decimal $origLong
	 * @param decimal $maxDistance
	 * @param string $rtnKilometers
	 * @param string $rtnAssocArrYN
	 * @return CActiveRecord | array[]
	 * @internal An index optimized select calcs in 0.034 secs and fetches dataset in 0.005 secs 	
	 * @see: BaseModel::convertPostalCodeToLatLong()
	 */
	protected function fetchByDistance($origLat, $origLong, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {

		if ($rtnKilometers == 'Y'){
			$distance_return_type_factor = KILOMETER_FACTOR;
		} else {
			$distance_return_type_factor = MILES_FACTOR;
		}

		$distance_calc = "$distance_return_type_factor * DEGREES(ACOS(COS(RADIANS(latpoint)) "
			."* COS(RADIANS(latitude)) "
			."* COS(RADIANS(longpoint) - RADIANS(longitude)) "
			." + SIN(RADIANS(latpoint)) "
			." * SIN(RADIANS(latitude)))) AS distance ";	
		
        $query = ""
        . "select c.*  " 
        . "	from ".$this->tableName()." t " 
        . "	inner join  " 
        . "	( " 
        . "	select postal_code, place_name, state_name, latitude, longitude, " 
        . "	 $distance_calc " 
        . "	 from zip_code  " 
        . "	 join ( " 
        . "		 select  $origLat  AS latpoint,  $origLong AS longpoint " 
        . "	   ) AS p ON 1=1 " 
        . "		where longitude  " 
        . "		BETWEEN longpoint - ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) " 
        . "			AND longpoint + ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) " 
        . "		having distance_in_miles < $maxDistance " 
        . "		order by distance_in_miles desc " 
        . "	) z on left(t.person_postal_code,5) = z.postal_code " 
        . "; " 
        ;                    
		
		$cmd = Yii::app()->db->createCommand($query);
		if ($rtnAssocArrYN === 'Y'){
			return $cmd->queryAll($fetchAssociative=true);
		} else {
			return $cmd->queryAll();
		}
	}	
	

}
