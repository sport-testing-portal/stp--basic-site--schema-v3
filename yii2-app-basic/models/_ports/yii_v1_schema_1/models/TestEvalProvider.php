<?php

Yii::import('application.models._base.BaseTestEvalProvider');

class TestEvalProvider extends BaseTestEvalProvider
{
    /**
     * @return TestEvalProvider
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Test Provider|Test Providers', $n);
    }

    public static function representingColumn() {
        return 'test_eval_provider_name';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

//    public static function primaryKey()
//    {
//        return 'test_eval_provider_id';
//    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
                'test_eval_provider_id'				=> Yii::t('app', 'ID'),
                'test_eval_provider_name'			=> Yii::t('app', 'Name'),
                'test_eval_provider_desc_short'		=> Yii::t('app', 'Desc Short'),
                'test_eval_provider_desc_long'		=> Yii::t('app', 'Desc Long'),
                'test_eval_provider_website_url'	=> Yii::t('app', 'Website Url'),
                'test_eval_provider_logo_url'		=> Yii::t('app', 'Logo Url'),
                'effective_begin_dt'				=> Yii::t('app', 'Effective Begin Dt'),
                'effective_end_dt'					=> Yii::t('app', 'Effective End Dt'),
                'created_dt'					=> Yii::t('app', 'Created Dt'),
                'updated_dt'					=> Yii::t('app', 'Updated Dt'),
                'created_by'					=> Yii::t('app', 'Created By'),
                'updated_by'					=> Yii::t('app', 'Updated By'),
                'testEvalTypes' => null,
        ];
    }

	/**
	 * Create an array based data provider
	 * @return \CArrayDataProvider
	 */
	protected function withUsers(){
		$sql = BaseModel::sqlTrim("
			select
				t.`test_eval_provider_id` as id,
				t.`test_eval_provider_id`,
				t.`test_eval_provider_name`,
				t.`test_eval_provider_desc_short`,
				t.`test_eval_provider_desc_long`,
				t.`test_eval_provider_website_url`,
				t.`test_eval_provider_logo_url`,
				t.`effective_begin_dt`,
				t.`effective_end_dt`,
				t.`created_dt`,
				t.`updated_dt`,
				created.person_name_full as `created_by`,
				updated.person_name_full as `updated_by`
				from test_eval_provider t
					left outer join person created
						on t.created_by = created.user_id
					left outer join person updated
						on t.updated_by = updated.user_id
				order by t.test_eval_provider_name"
			);
		$cmd = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative = true);
		$dataProvider=new CArrayDataProvider($data,['id'=>'id']);
		return $dataProvider;
	}

}
