<?php

Yii::import('application.models._base.BasePaymentXfer');

class PaymentXfer extends BasePaymentXfer
{
    /**
     * @return PaymentXfer
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Payment Transfer|Payment Transfers', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'payment_xfer_id'				=> Yii::t('app', 'Transfer ID'),
                'payment_log_id'				=> Yii::t('app', 'Payment Log ID'),
                'payment_xfer_entity_id'		=> Yii::t('app', 'Entity ID'),
                'payment_xfer_status_code_char' => Yii::t('app', 'Status Code Char'),
                'payment_xfer_status_code_num'	=> Yii::t('app', 'Status Code Num'),
                'payment_xfer_sent_dt'			=> Yii::t('app', 'Sent Data Dt'),
                'payment_xfer_recd_status_dt'	=> Yii::t('app', 'Data Status Received Dt'),
                'payment_xfer_blob_uri'			=> Yii::t('app', 'Blob URI'),
                'payment_xfer_blob_format'		=> Yii::t('app', 'Blob Format'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'paymentLog' => null,
                'paymentXferEntity' => null,
        );
    }
}
