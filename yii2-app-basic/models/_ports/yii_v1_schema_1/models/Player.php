<?php

Yii::import('application.models._base.BasePlayer');

class Player extends BasePlayer
{
    /**
     * @return Player
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Player|Players', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_dt)){
				$this->created_dt = date('Y-m-d H:i:s');
			}
            $this->updated_dt = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Stores this table and its parent table - Person
	 * @param type $field_value_array
	 * @see Person::StorePersonWithOrg($field_value_array)
	 */
	public function StorePlayerWithPerson($field_value_array){
		$this->Store($field_value_array);
	}


	/**
	 * Store Player with parent table Person
	 * @param array[] $field_value_array A name value pair array ['field_name'=>'field value',...]
     * @return int $pk player_id
	 * @internal Insert or update a row, add a row in a parent table if needed
	 * @version 2.0.0
	 * @internal CEC 2.0.0
	 * @var data[person_name_first],data[last,person_name_last] are required!
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 * @todo fix person_id bug, a passed person_id is not seen.
	 * Psuedo Code
	 * look for player by pkey
	 * look for player by fkey (person_id)
	 * if player not found then insert player
	 *    if person exists then update
	 *    if person not exist then insert person
	 *    insert player
     */
    public function Store($field_value_array)
    {
		$pk_name		= $this->primaryKey();
		$fk_name		= 'person_id';	// linkage to parent table
		$class			= __CLASS__;	// Player
		$parentModel	= 'Person';
		$debug_local	= true;

		// use this for value updates inside this method to preserve the incoming params values
		$fva = $field_value_array;

		$requiredFields = [];
		$rules =[];


		// Search for a Player by using the primary key $player_id
		$player_row_found_yn = 'N';
		if (array_key_exists($pk_name, $fva)) { // player_id was found in the fva
			$pk_val = $fva[$pk_name]; // player_id
			//$row = Player::model()->findByPk($pkey_val); // returns object
			$player_row = $class::model()->findByPk($pk_val); // returns object
			if (! empty($player_row)){
				$player_row_found_yn = 'Y';
			}
		}

		// run a search for Player by foreign key $person_id
		if ($player_row_found_yn == 'N' && array_key_exists($fk_name, $fva)) {
			$fkey_val = (int)$fva[$fk_name];
			if ($fkey_val > 0){
			//$row = Player::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
				$player_row = $class::model()->findByAttributes(array($fk_name=>$fkey_val)); // returns AR object
			} else {
				$player_row=[];
			}
			if (! empty($player_row)){
				$player_row_found_yn = 'Y';
			}
		}

		$personInsertRequirementsMetYN = $this->personInsertRequirementsMetYN($field_value_array);

		// see if the person exists already
		if ($player_row_found_yn == 'Y' && isset($player_row['person_id']) && $player_row['person_id']>0 ){
			$person_id = (int)$player_row['person_id'];
		}
		if (! isset($person_id)){
			// see if person id is the attributes passed in
			if (isset($field_value_array['person_id'])){
				$person_id = $field_value_array['person_id'];
			}
		}
		//if ($player_row_found_yn == 'Y' && $person_id > 0){ // Why do we care if player was found?
		if ($person_id > 0){
			$Person = Person::model()->findByPk($person_id);
			if ($debug_local){
				//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($Person->attributes, 10, true);
			}
		} else {
			$Person = null;
		}

/*		if ( is_null($Person) && isset($fva['person_name_first']) && isset($fva['person_name_last']) ){
			$criteria = new CDbCriteria();
			$criteria->select    = array('person_id', 'person_name_first', 'person_name_last');
			$criteria->condition = 'person_name_first=:person_name_first';
			$criteria->addCondition('person_name_last=:person_name_last', 'AND');
			$criteria->order     = 'person_id';
			$criteria->params    = array(
				':person_name_first'=>$fva['person_name_first']
				,':person_name_last'=>$fva['person_name_last']
				);
			$Persons = Person::model()->findAll($criteria);
		} else {
			$Persons = [];
		}*/

		if (is_null($Person)){
			// insert?
			$person_id = Person::StorePersonWithOrg($fva);
		//} elseif (count($Persons, COUNT_RECURSIVE) > 0){
			// update
			// if a person is changing their name then the lookup would not have found them with the search.
			// In short, no update is needed in this method
			$person_id = $Person->attributes['person_id'];
			return (int)$person_id;
		}


		if ($player_row_found_yn == 'Y'){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $player_row->attributes;
			$player_row->attributes = $fva;
			$diff = array_diff($bfr_upd,$player_row->attributes);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$player_row->attributes[$pk_name];
			}
			// The values must be different. Save the model.
			if ($player_row->save()){
				return (int)$player_row->attributes[$pk_name];
			} else {
				return 0;
			}

		} elseif ($player_row_found_yn == 'N'){
			// Insert
			//$model = new Player();
			$player = new $class();
			$player->unsetAttributes(); // clears default values
			$player->attributes = $fva;
			if ($player->save()){
				$id = (int)$player->attributes[$pk_name];
			} else {
				$id = 0;
			}

			return (int)$id;
		}

		return false;
    }

    /**
	 * @param int
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public function RowCountByPersonId($person_id)
    {
		$class = __CLASS__;
		$rows  = $class::model()->findAllByAttributes(array("person_id"=>$person_id));

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rows, 10, true);
		if (is_array($rows) && count($rows, COUNT_RECURSIVE) > 0){
			$cnt = count($rows[0]);
		} else {
			$cnt = 0;
		}
		//$cnt = count($rows[0]);
		return $cnt;

    }



	/**
	 * Tests the fva to see if an insert can be performed
	 * @param array $field_value_array
	 * @return string Y=Ok to insert, N=insert forbidden
	 * @internal asserts org_name, and org_type_id are present OR an org_id > 0 is present
	 * @internal dev sts = rft
	 * @version 1.0.0
	 */
	protected function personInsertRequirementsMetYN(array $field_value_array) {
		// @todo: see if requirements for a person insert have been met
		// The requirements below are for inserting an org that is to be linked to a person. At the SQL level only org_name is required for an org row.
		$fva = $field_value_array;
		$parent_requirements_met_yn = 'N';

		// assert that an person_id was not passed in
		if (array_key_exists('person_id', $fva) && (int)($fva['person_id'] > 0)) {
			return $parent_requirements_met_yn='N'; // no insert allowed
		}

		// get person values to test from the fva
		if (array_key_exists('person_name_first', $fva) && strlen($fva['person_name_first'] > 0)) {
			$person_fname = $fva['person_name_first'];
		}
		if (array_key_exists('person_name_last', $fva) && strlen($fva['person_name_last'] > 0)) {
			$person_lname = $fva['person_name_last'];
		}

		// Test for valid values in the fva
		if (strlen($person_fname) > 0 && strlen($person_lname) > 0){
			$parent_requirements_met_yn = 'Y'; // insert can proceed
		}
		return $parent_requirements_met_yn;
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'player_id' => Yii::t('app', 'Player ID'),  // default is Player
                'person_id' => Yii::t('app', 'Person ID'),  // default is Person
                'age_group_id' => Yii::t('app', 'Age Group'),
                'player_team_player_id' => Yii::t('app', 'Team Player'),
                'player_sport_position_id' => Yii::t('app', 'Sport Position'),
                'player_sport_position2_id' => Yii::t('app', 'Sport Position2'),
                'player_access_code' => Yii::t('app', 'Access Code'),
                'player_waiver_minor_dt' => Yii::t('app', 'Waiver Minor Dt'),
                'player_waiver_adult_dt' => Yii::t('app', 'Waiver Adult Dt'),
                'player_parent_email' => Yii::t('app', 'Parent Email'),
                'player_sport_preference' => Yii::t('app', 'Sport Preference'),
                'player_sport_position_preference' => Yii::t('app', 'Sport Position Preference'),
                'player_shot_side_preference' => Yii::t('app', 'Shot Side Preference'),
                'player_dominant_foot' => Yii::t('app', 'Dominant Foot'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'campSessionPlayerEvals' => null,
                'ageGroup' => null,
                'person' => null,
                'playerSportPosition' => null,
                'playerTeamPlayer' => null,
                'playerAcademics' => null,
                'playerCampLogs' => null,
                'playerCollegeRecruits' => null,
                'playerContacts' => null,
                'playerSchools' => null,
                'playerSports' => null,
                'playerTeams' => null,
                'teamPlayers' => null,
        );
    }

	/**
	 * Override BasePlayer::relations() to allow model regeneration without
	 * affecting the relations defined here.
	 * @return string
	 */
    public function relations() {
        return array(
            'campSessionPlayerEvals' => array(self::HAS_MANY, 'CampSessionPlayerEval', 'player_id'),
            'ageGroup' => array(self::BELONGS_TO, 'AgeGroup', 'age_group_id'),
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'playerSportPosition' => array(self::BELONGS_TO, 'SportPosition', 'player_sport_position_id'),
            'playerTeamPlayer' => array(self::BELONGS_TO, 'TeamPlayer', 'player_team_player_id'),
            'playerAcademics' => array(self::HAS_MANY, 'PlayerAcademic', 'player_id'),
            'playerCampLogs' => array(self::HAS_MANY, 'PlayerCampLog', 'player_id'),
            'playerCollegeRecruits' => array(self::HAS_MANY, 'PlayerCollegeRecruit', 'player_id'),
            'playerContacts' => array(self::HAS_MANY, 'PlayerContact', 'player_id'),
            'playerSchools' => array(self::HAS_MANY, 'PlayerSchool', 'player_id'),
            'playerSports' => array(self::HAS_MANY, 'PlayerSport', 'player_id'),
            'playerTeams' => array(self::HAS_MANY, 'PlayerTeam', 'player_id'),
            'teamPlayers' => array(self::HAS_MANY, 'TeamPlayer', 'player_id'),
        );
    }

	/**
	 * A scope that returns the placeholders for each team relation
	 * @return CActiveRecord (Team + Org + OrgType )
	 * @example $model = Team::model()->team()->findByPk(5); // team_id
	 * @version 0.1.0
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 * @internal Development Status = Golden!
	 */
	public function team() {

		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
				'with'=>[
					'org'=>[
						'with'=>[
							'schools',
							'orgType',
						]
					],
//					'teamPlayers',
					'teamPlayers'=>[
						'with'=>[
							'player'=>[
								'with'=>'person',
								//'with'=>'PlayerPerson',
							],

						],
					],
//					'teamCoaches',
					// two aliases of person models can
//					'teamCoaches'=>[
//						'with'=>[
//							'coach'=>[
//								'with'=>'person',
//							],
//						],
//					],
					'teamCoaches'=>[
						'with'=>[
							'coach'=>[
								'with'=>[
									'person'=>[
										'alias'=>'personCoach',
										//'condition'=> "personCoach.person_type_id = 2",
										//'on'=>"personCoach.person_type_id = 2",
									]
								]
							],
						],
					],

					'ageGroup',
					'camp',
					'teamCountry',
					'teamDivision',
					'teamLeague',
					'gender',
					'school',
					'sport',
					//Next relations joins b
					'testEvalSummaryLogs'=>[
//						'with'=>[
//							'testEvalDetailLogs',
//							'testEvalLogs',
//							'person',
//							'testEvalType',
//						],
					],
				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}
}
