<?php

Yii::import('application.models._base.BaseAppChangeLog');

class AppChangeLog extends BaseAppChangeLog
{
    /**
     * @return AppChangeLog
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'AppChangeLog|AppChangeLogs', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
                if (empty($this->created_at)){
                        $this->created_at = date('Y-m-d H:i:s');
                }
            $this->updated_at = date('Y-m-d H:i:s');
                // Enable GSM processes to force a user id eg impersonate a user for data imports
                if (empty($this->created_by)){
                        $this->created_by = (int)Yii::app()->user->getId();
                }
                if (empty($this->updated_by)){
                        $this->updated_by = (int)Yii::app()->user->getId();
                }
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    //public function searchOrderByAppVersion() {
    public function search() {
        //$criteria = new CDbCriteria;
	$criteria = new CDbCriteria(['order'=>'app_semantic_version DESC']);

        $criteria->compare('app_change_log_id', $this->app_change_log_id);
        $criteria->compare('app_semantic_version', $this->app_semantic_version, true);
        $criteria->compare('app_change_desc', $this->app_change_desc, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'app_change_log_id' => Yii::t('app', 'Log ID'),
                'app_semantic_version' => Yii::t('app', 'Semantic Version'),
                'app_change_desc' => Yii::t('app', 'Change Desc'),
                'created_at' => Yii::t('app', 'Created At'),
                'updated_at' => Yii::t('app', 'Updated At'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
        );
    }


    /*
     * @param string $beginDate A mysql formatted datetime string eg '2015-11-15 15:30:01'
     * @param string $endDate A mysql formatted datetime string eg '2015-11-15 15:30:01'
     * @param string 'model'|'array'
     * @return array[] $changeLogs
     *
     */
    public static function fetchWebAppChangesDuringDateRange($beginDate,$endDate, $returnType='array') {

        if($returnType == 'model'){
            // Returns an array of models or an empty array
            $criteria = new CDbCriteria();
            $criteria->addBetweenCondition('created_at', $beginDate, $endDate);
            $models	= AppChangeLog::model()->findAll($criteria);
            return $models;
        }
        elseif($returnType == 'array')
        {
            //. "where created_at BETWEEN '$beginDate' AND '$endDate' "
            $sql  = "select * from app_change_log "
                        . "where created_at BETWEEN :begin_dt AND :end_dt "
                            . "order by app_semantic_version ASC";
            $cmd  = Yii::app()->db->createCommand($sql);
            $params = [':begin_dt'=>$beginDate, ':end_dt'=>$endDate];
            $data = $cmd->queryAll($fetchAssociative=true, $params);
            return $data;
        }
        else
        {
            throw new CException('Invalid return type passed by parameter');
        }
    }
}
