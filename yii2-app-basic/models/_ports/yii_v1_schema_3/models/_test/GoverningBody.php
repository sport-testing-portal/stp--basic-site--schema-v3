<?php

Yii::import('application.models._base.BaseGoverningBody');

class GoverningBody extends BaseGoverningBody
{

	public static $p_tableName  = 'governing_body';
	public static $p_primaryKey = 'governing_body_id';
	public static $p_textColumn = 'governing_body_name';
	public static $p_modelName  = 'GoverningBody';

    /**
     * @return GoverningBody
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'GoverningBody|GoverningBodies', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'governing_body_id' => Yii::t('app', 'Governing Body ID'),
                'sport_id' => Yii::t('app', 'Sport'),
                'governing_body_name' => Yii::t('app', 'Governing Body'),
                'governing_body_short_desc' => Yii::t('app', 'Governing Body Short Desc'),
                'governing_body_long_desc' => Yii::t('app', 'Governing Body Long Desc'),
                'created_dt' => Yii::t('app', 'Created Date'),
                'updated_dt' => Yii::t('app', 'Updated Date'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'sport' => null,
        );
    }

	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownList($sportID=20, $returnTags=false) {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		//$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
				. "where sport_id = :sport_id order by $textColumn";
		$params =[':sport_id'=>$sportID];
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true, $params);
		$list = CHtml::listData( $data, 'id', 'text'); //[]

		if ($returnTags){
			return array_values($list);
		} else {
			return $list;
		}
	}

	/**
	 *
	 * @return array[] Example [0=>['id'=>1,'text'=>'rowValueText'], 1=>['id'=>'2', 'text'=>'anotherRowValueText'] ]
	 */
	public static function fetchAllAsSelect2List($sportID=20) {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		//$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName "
				. "where sport_id = :sport_id order by $textColumn";
		$params =[':sport_id'=>$sportID];

		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true, $params);
		return $data;
	}


}
