<?php

Yii::import('application.models._base.BaseOrgType');

class OrgType extends BaseOrgType
{


	public static $p_tableName  = 'org_type';
	public static $p_primaryKey = 'org_type_id';
	public static $p_textColumn = 'org_type_name';
	public static $p_modelName  = 'OrgType';

    /**
     * @return OrgType
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'OrgType|OrgTypes', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 *
	 * @param type $type_id
	 * @return type
	 */
    public static function typeName($type_id = 2)
    {
		// default to 'Club'
		$OrgType = self::model()->findByPk($type_id);
		if (is_object($OrgType)){
			$type_name = $OrgType->attributes['org_type_name'];
		} else {
			$type_name = '';
		}
        return Yii::t('app', $type_name);
    }

	/**
	 * Return model values suitable for
	 *   TbActiveForm::dropDownList(), or
	 *   TbActiveForm->select2Row()
	 * @param bool $returnTags if true returns [0=>'rowValueText', 1='anotherRowValueText'] as zero based indexed array
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownList($returnTags=false) {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		// This table has to be sorted by display order since the items are not compatible with alpha sorting
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		//$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by display_order";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		if ($returnTags){
			return array_values($list);
		} else {
			return $list;
		}
	}

	/**
	 *
	 * @return array[] Example [0=>['id'=>1,'text'=>'rowValueText'], 1=>['id'=>'2', 'text'=>'anotherRowValueText'] ]
	 */
	public static function fetchAllAsSelect2List() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		return $data;
	}

}
