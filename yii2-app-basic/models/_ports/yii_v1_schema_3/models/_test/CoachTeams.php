<?php

Yii::import('application.models._base.BaseCoachTeams');

class CoachTeams extends BaseCoachTeams
{
	const MILE_FACTOR=69.1;
	const KILOMETER_FACTOR=111.045;

	private static $initialized = false;

	public static $p_tableName  = 'vwAthleteTeams';
	public static $p_primaryKey = 'team_coach_id'; //'player_id' is NOT the primary key;
	public static $p_textColumn = 'person_name_full';
	//public static $p_modelName  = 'AthleteTeams'; VwPlayer
	public static $p_modelName  = 'CoachTeams';

	public static $p_componentModels=[
		'OrgType',
		'OrgLevel',
		'Org',
		'Person',
		'Coach',
		'TeamDivision',
		'TeamLeague',
		'SportPosition',
		'Team',
		'TeamCoach',
		'TeamOrg',
	];
	// **
	// Class level meta-data regarding data-storage transctions
	// **
	/** @var array $p_componentModelRelations consumed by multiple methods.
	 * $cmr in methods
	 */
	public static $p_componentModelRelations=[];
	/** @var array $p_componentModelRelationsESave consumed by multiple methods.
	 * $cmrES in methods
	 */
	public static $p_componentModelRelationsESave=[];
	/** @var array $p_componentModelKeys consumed by multiple methods.
	 * $cmk in methods
	 */

	/** @var array $p_componentModelKeys consumed by multiple methods.
	 * $cmk in methods
	 */
	public static $p_componentModelKeys=[];                // $cmk in methods
	/**
	 * @var array $p_componentModelTransactionMetaData consumed by multiple methods
	 * that write models relations data to the database, or handle data storage logic.
	 * $cmtm in methods. Used to track which models have been linked.
	 */
	public static $p_componentModelTransactionMetaData=[]; // $tmd in methods


	protected $hasFatalError = false;
	protected $isReadOnly    = true;

	private $_scenarioCrud   = 'read'; // 'create'|'read'|'update'|'delete'
	private $_scenarioRender = 'read'; // 'create'|'read'|'update'|'delete'

//	protected $scenarioSessionKeyCrud   = 'AthleteTeams-Scenario-CRUD';
//	protected $scenarioSessionKeyRender = 'AthleteTeams-Scenario-Render';

	protected static $scenarioSessionKeyCrud   = 'AthleteTeams-Scenario-CRUD';
	protected static $scenarioSessionKeyRender = 'AthleteTeams-Scenario-Render';

	/**
     * @return CoachTeams
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Coach Teams|Coach Teams', $n);
    }


    private static function initialize()
    {
        if (self::$initialized){
            return;
		}
		self::$initialized = true;
    }


	/**
	 * Returns a model property that uniquely identifies each model row
	 * @return	string A column name
	 * @see Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 */
    public function primaryKey()
    {
        //return $this->tableName() . '_id';
		return 'team_coach_id'; // this field is unique per data row
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    /**
	 *
	 * @return boolean
	 */
    protected function beforeSave()
    {
		// internal attributes need to be written to the individual tables
		// @todo consider using the fieldxlation routine here so a call to this->model->save
		//   will actually parse the model's attributes into an mfva and store all the values.
		if (true===true){
			return false; // data updates disallowed
		}
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Lists the writable models within the AthleteTeams model (sql view)
	 * @return array $p_componentModels
	 */
    public static function getComponentModels()
    {
        return self::$p_componentModels;
    }

	/**
	 * Select data by distance
	 * @param decimal $origLat
	 * @param decimal $origLong
	 * @param decimal $maxDistance
	 * @param string $rtnKilometers
	 * @param string $rtnAssocArrYN
	 * @return CActiveRecord | array[]
	 * @internal An index optimized select calcs in 0.034 secs and fetches dataset in 0.005 secs
	 * @see: BaseModel::convertPostalCodeToLatLong()
	 */
	protected function fetchByDistance($origLat, $origLong, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {

		if ($rtnKilometers == 'Y'){
			$distance_return_type_factor = KILOMETER_FACTOR;
		} else {
			$distance_return_type_factor = MILES_FACTOR;
		}

		$distance_calc = "$distance_return_type_factor * DEGREES(ACOS(COS(RADIANS(latpoint)) "
			."* COS(RADIANS(latitude)) "
			."* COS(RADIANS(longpoint) - RADIANS(longitude)) "
			." + SIN(RADIANS(latpoint)) "
			." * SIN(RADIANS(latitude)))) AS distance ";

        $query = ""
        . "select c.*  "
        . "	from ".$this->tableName()." t "
        . "	inner join  "
        . "	( "
        . "	select postal_code, place_name, state_name, latitude, longitude, "
        . "	 $distance_calc "
        . "	 from zip_code  "
        . "	 join ( "
        . "		 select  $origLat  AS latpoint,  $origLong AS longpoint "
        . "	   ) AS p ON 1=1 "
        . "		where longitude  "
        . "		BETWEEN longpoint - ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "			AND longpoint + ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "		having distance_in_miles < $maxDistance "
        . "		order by distance_in_miles desc "
        . "	) z on left(t.person_postal_code,5) = z.postal_code "
        . "; "
        ;

		$cmd = Yii::app()->db->createCommand($query);
		if ($rtnAssocArrYN === 'Y'){
			return $cmd->queryAll($fetchAssociative=true);
		} else {
			return $cmd->queryAll();
		}
	}

	/**
	 * Made to bulk assign view attribute values to base model attribute values
	 * @param array[] $fieldValueArray
	 * @version 4.5.0 synced with vwPlayer v4.5.0
	 * @uses self::fetchTargetModelFieldNameXlation()
	 * @todo Add OrgLevel fields
	 */
	public static function fetchTargetModelFieldNameXlation($fieldValueArray) {
		//$fva = $field_value_array;
		// attributes like 'person_age' that are calculated values and can't be written back to the database
		$readonly_attr	= self::fetchReadOnlyAttributes();
		$fva			= array_diff_key( $fieldValueArray, array_flip( $readonly_attr) ); // remove readonly fields
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fieldValueArray, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fva, 10, true);
		//$field_list = array_keys($fva);
		$xlat=[
		'person_name_full'					=> ['model'=>'Person',	'attribute'=>'person_name_full'],
		// 'person_age`
		'team_name'							=> ['model'=>'Team',	'attribute'=>'team_name'],
		//'age_group_name' 					=> ['model'=>'AgeGroup','attribute'=>''],
		'person_city'						=> ['model'=>'Person',	'attribute'=>'person_city'],
		'person_state_or_region'			=> ['model'=>'Person',	'attribute'=>'person_state_or_region'],
		'gsm_staff'							=> ['model'=>'User',	'attribute'=>'first_name'],
		'player_id'							=> ['model'=>'Player',	'attribute'=>'player_id'],
		'person_id'							=> ['model'=>'Person',	'attribute'=>'person_id'],
		// confirmed
		'player_team_player_id'				=> ['model'=>'Player','attribute'=>'player_team_player_id'],
		'player_access_code'				=> ['model'=>'Player','attribute'=>'player_access_code'],
		'player_waiver_minor_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_minor_dt'],
		'player_waiver_adult_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_adult_dt'],
		'player_parent_email'				=> ['model'=>'Player','attribute'=>'player_parent_email'],
		'player_sport_preference'			=> ['model'=>'Player','attribute'=>'player_sport_preference'],
		'player_sport_position_preference'	=> ['model'=>'Player','attribute'=>'player_sport_position_preference'],
		'player_shot_side_preference' 		=> ['model'=>'Player','attribute'=>'player_shot_side_preference'],
		'player_dominant_side'		 		=> ['model'=>'Player','attribute'=>'player_dominant_side'],
		'player_dominant_foot'		 		=> ['model'=>'Player','attribute'=>'player_dominant_foot'],
		'player_statistical_highlights'		=> ['model'=>'Player','attribute'=>'player_statistical_highlights'],
		// confirmed
		'org_id'							=> ['model'=>'Person','attribute'=>'org_id'],
		'app_user_id'						=> ['model'=>'Person','attribute'=>'app_user_id'],
		'person_name_prefix'				=> ['model'=>'Person','attribute'=>'person_name_prefix'],
		'person_name_first'					=> ['model'=>'Person','attribute'=>'person_name_first'],
		'person_name_middle'				=> ['model'=>'Person','attribute'=>'person_name_middle'],
		'person_name_last'					=> ['model'=>'Person','attribute'=>'person_name_last'],
		'person_name_suffix'				=> ['model'=>'Person','attribute'=>'person_name_suffix'],
		'person_phone_personal'				=> ['model'=>'Person','attribute'=>'person_phone_personal'],
		'person_email_personal'				=> ['model'=>'Person','attribute'=>'person_email_personal'],
		'person_phone_work'					=> ['model'=>'Person','attribute'=>'person_phone_work'],
		'person_email_work'					=> ['model'=>'Person','attribute'=>'person_email_work'],
		'person_position_work'				=> ['model'=>'Person','attribute'=>'person_position_work'],
		'person_image_headshot_url' 		=> ['model'=>'Person','attribute'=>'person_image_headshot_url'],
		'person_name_nickname'				=> ['model'=>'Person','attribute'=>'person_name_nickname'],
		'person_date_of_birth'				=> ['model'=>'Person','attribute'=>'person_date_of_birth'],
		'person_height'						=> ['model'=>'Person','attribute'=>'person_height'],
		'person_weight'						=> ['model'=>'Person','attribute'=>'person_weight'],
		'person_tshirt_size'				=> ['model'=>'Person','attribute'=>'person_tshirt_size'],
		'person_addr_1'						=> ['model'=>'Person','attribute'=>'person_addr_1'],
		'person_addr_2'						=> ['model'=>'Person','attribute'=>'person_addr_2'],
		'person_addr_3'						=> ['model'=>'Person','attribute'=>'person_addr_3'],
		'person_postal_code'				=> ['model'=>'Person','attribute'=>'person_postal_code'],
		'person_country'					=> ['model'=>'Person','attribute'=>'person_country'],
		'person_country_code'				=> ['model'=>'Person','attribute'=>'person_country_code'],
		'person_profile_url'				=> ['model'=>'Person','attribute'=>'person_profile_url'],
		'person_profile_uri'				=> ['model'=>'Person','attribute'=>'person_profile_uri'],
        'person_high_school__graduation_year' => ['model'=>'Person','attribute'=>'person_high_school__graduation_year'],
        'person_college_graduation_year'	   => ['model'=>'Person','attribute'=>'person_college_graduation_year'],
		'person_college_commitment_status'	=> ['model'=>'Person','attribute'=>'person_college_commitment_status'],
		'person_type_id'					=> ['model'=>'Person','attribute'=>'person_type_id'],
		//'person_type_name'				=> ['model'=>'PersonType','attribute'=>'person_type_name'],  // readonly
		//'person_type_desc_short'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_short'],  // readonly
		//'person_type_desc_long'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_long'],  // readonly
		//'gender_desc'						=> ['model'=>'Gender',	'attribute'=>'gender_desc'], // readonly
		//'gender_code'						=> ['model'=>'Gender',	'attribute'=>'gender_code'], // readonly
		'org_org_id'						=> ['model'=>'Org','attribute'=>'org_id'],
		'org_type_id'						=> ['model'=>'Org','attribute'=>'org_type_id'],
		'org_name'							=> ['model'=>'Org','attribute'=>'org_name'],
		'org_type_name'						=> ['model'=>'OrgType','attribute'=>'org_type_name'],
		'org_website_url'					=> ['model'=>'Org','attribute'=>'org_website_url'],
		'org_twitter_url'					=> ['model'=>'Org','attribute'=>'org_twitter_url'],
		'org_facebook_url'					=> ['model'=>'Org','attribute'=>'org_facebook_url'],
		'org_phone_main'					=> ['model'=>'Org','attribute'=>'org_phone_main'],
		'org_email_main'					=> ['model'=>'Org','attribute'=>'org_email_main'],
		'org_addr1'							=> ['model'=>'Org','attribute'=>'org_addr1'],
		'org_addr2'							=> ['model'=>'Org','attribute'=>'org_addr2'],
		'org_addr3'							=> ['model'=>'Org','attribute'=>'org_addr3'],
		'org_city'							=> ['model'=>'Org','attribute'=>'org_city'],
		'org_state_or_region'				=> ['model'=>'Org','attribute'=>'org_state_or_region'],
		'org_postal_code'					=> ['model'=>'Org','attribute'=>'org_postal_code'],
		'org_country_code_iso3'				=> ['model'=>'Org','attribute'=>'org_country_code_iso3'],
		// confirmed
		'team_player_id'					=> ['model'=>'TeamPlayer','attribute'=>'team_player_id'],
		'team_id'							=> ['model'=>'TeamPlayer','attribute'=>'team_id'],
		'team_play_primary_position'		=> ['model'=>'TeamPlayer','attribute'=>'primary_position'],
		'team_play_sport_position_id'		=> ['model'=>'TeamPlayer','attribute'=>'team_play_sport_position_id'],
		'team_play_sport_position2_id'		=> ['model'=>'TeamPlayer','attribute'=>'team_play_sport_position2_id'],
		'team_play_begin_dt'				=> ['model'=>'TeamPlayer','attribute'=>'team_play_begin_dt'],
		'team_play_end_dt'					=> ['model'=>'TeamPlayer','attribute'=>'team_play_end_dt'],
		'team_play_coach_id'				=> ['model'=>'TeamPlayer','attribute'=>'coach_id'],
		'team_play_coach_name'				=> ['model'=>'TeamPlayer','attribute'=>'coach_name'],
		'team_play_created_at'				=> ['model'=>'TeamPlayer','attribute'=>'created_at'],
		'team_play_updated_at'				=> ['model'=>'TeamPlayer','attribute'=>'updated_at'],
		'team_play_statistical_highlights'	=> ['model'=>'TeamPlayer','attribute'=>'team_play_statistical_highlights'],
		'team_play_current_team'	        => ['model'=>'TeamPlayer','attribute'=>'team_play_current_team'],

		//confirmed
		'team_play_sport_position_id'		=> ['model'=>'SportPosition_1','attribute'=>'sport_position_id'],
		'team_play_sport_position2_id'		=> ['model'=>'SportPosition_1','attribute'=>'sport_position_id'],
		'team_play_sport_position_name'		=> ['model'=>'SportPosition_2','attribute'=>'sport_position_name'],
		'team_play_sport_position2_name'	=> ['model'=>'SportPosition_2','attribute'=>'sport_position_name'],
		// confirmed
		'team_org_id'						=> ['model'=>'Team','attribute'=>'org_id'],
		'team_school_id'					=> ['model'=>'Team','attribute'=>'school_id'],
		'team_sport_id'						=> ['model'=>'Team','attribute'=>'sport_id'],
		'team_camp_id'						=> ['model'=>'Team','attribute'=>'camp_id'],
		'team_gender_id'					=> ['model'=>'Team','attribute'=>'gender_id'],
		'age_group_id'						=> ['model'=>'Team','attribute'=>'age_group_id'],
		'team_age_group'					=> ['model'=>'Team','attribute'=>'team_age_group'],
		'team_website_url'					=> ['model'=>'Team','attribute'=>'team_website_url'],
		'team_schedule_url'					=> ['model'=>'Team','attribute'=>'team_schedule_url'],
		'team_schedule_uri'					=> ['model'=>'Team','attribute'=>'team_schedule_uri'],
		'team_competition_season_id'		=> ['model'=>'Team','attribute'=>'team_competition_season_id'],
		'team_competition_season'			=> ['model'=>'Team','attribute'=>'team_competition_season'],
		'team_statistical_highlights'		=> ['model'=>'Team','attribute'=>'team_statistical_highlights'],
		'team_gender'						=> ['model'=>'Team','attribute'=>'team_gender'],
		'team_city'							=> ['model'=>'Team','attribute'=>'team_city'],
		'team_division'						=> ['model'=>'Team','attribute'=>'team_division'],
		'team_division_id'					=> ['model'=>'Team','attribute'=>'team_division_id'],
		'team_league_id'					=> ['model'=>'Team','attribute'=>'team_league_id'],
		'team_state_id'						=> ['model'=>'Team','attribute'=>'team_state_id'],
		'team_country_id'					=> ['model'=>'Team','attribute'=>'team_country_id'],
		// confirmed
		'team_org_org_id'					  => ['model'=>'TeamOrg','attribute'=>'org_id'],
		'team_org_org_type_id'				  => ['model'=>'TeamOrg','attribute'=>'org_type_id'],
		'team_org_org_type_name'			  => ['model'=>'TeamOrgType','attribute'=>'org_type_name'],
		'team_org_org_name'					  => ['model'=>'TeamOrg','attribute'=>'org_name'],
		'team_org_org_ncaa_clearing_house_id' => ['model'=>'TeamOrg','attribute'=>'org_ncaa_clearing_house_id'],
		'team_org_org_website_url'			  => ['model'=>'TeamOrg','attribute'=>'org_website_url'],
		'team_org_org_twitter_url'			  => ['model'=>'TeamOrg','attribute'=>'org_twitter_url'],
		'team_org_org_facebook_url'			  => ['model'=>'TeamOrg','attribute'=>'org_facebook_url'],
		'team_org_org_phone_main'			  => ['model'=>'TeamOrg','attribute'=>'org_phone_main'],
		'team_org_org_email_main'			 => ['model'=>'TeamOrg','attribute'=>'org_email_main'],
		'team_org_org_addr1'				=> ['model'=>'TeamOrg','attribute'=>'org_addr1'],
		'team_org_org_addr2'				=> ['model'=>'TeamOrg','attribute'=>'org_addr2'],
		'team_org_org_addr3'				=> ['model'=>'TeamOrg','attribute'=>'org_addr3'],
		'team_org_org_city'					=> ['model'=>'TeamOrg','attribute'=>'org_city'],
		'team_org_state_or_region'			=> ['model'=>'TeamOrg','attribute'=>'org_state_or_region'],
		'team_org_org_postal_code'			=> ['model'=>'TeamOrg','attribute'=>'org_postal_code'],
		'team_org_org_country_code_iso3'	=> ['model'=>'TeamOrg','attribute'=>'org_country_code_iso3'],
		'team_org_org_governing_body'	    => ['model'=>'TeamOrg','attribute'=>'org_governing_body'],
		// confirmed
		'team_division_id'					=> ['model'=>'TeamDivision','attribute'=>'team_division_id'],
		'team_division_name'				=> ['model'=>'TeamDivision','attribute'=>'team_division_name'],
		// confirmed
		// next line removed because it will be added by pkeyXref process
		//'team_league_id'					=> ['model'=>'TeamLeague','attribute'=>'team_league_id'],
		'team_league_name'					=> ['model'=>'TeamLeague','attribute'=>'team_league_name'],
		// confirmed
		'team_sport_name'					=> ['model'=>'Sport','attribute'=>'gsm_sport_name'],
		//'team_age_group_depreciated' 		=> ['model'=>'Team','attribute'=>''],

		// @todo virtual fields need to be added to enable the writing of coach and coach-person fields
		];

		return $xlat;
	}

	/**
	 * A team and team-Org data handler for the AthleteTeams model.
	 * This separate function is needed because the athlete profile pages access
	 * the org (organizations) table through two JOIN directions:
	 *   one through athlete-Org, and one through team-Org.
	 * This teamOrg method updates and inserts data using the team-to-org linkage
	 * as a handler for the self::storeModels() function which handles the athlete-to-org linkages.
	 * @param array $fieldValuesArray
	 * @throws CException
	 * @internal Development Status = ready for testing (rft)
	 * @internal Do NOT call this method directly unless you are only touching
	 *   team and team-Org values. This method will be called automatically by the
	 *   self::saveModels() method to persist team and team-Org values.
	 */
	protected function teamOrgStore($fieldValuesArray) {
		if (!is_array($fieldValuesArray)){
			throw new CException("fieldValueArray is a required parameter", 707);
		}
		$fva = $fieldValuesArray;

		$scmda        = BaseModel::getStoreChainMetaData_ArrayTemplate();
		$pkName       = 'team_id';
		$fkNameInView = 'team_org_id';

		// @todo Add code to 'link' the org and team.
		$fkName       = 'org_id';

		$targetKeys   = ['team_id','org_id'];

		if (array_key_exists($pkName, $fva)){
			$pkVal = $fva[$pkName]; // update
		}
		if (array_key_exists($fkNameInView, $fva)){
			$fkVal = $fva[$fkNameInView]; // update
		}
		$scenarios=[];
		if ( (int)$pkVal > 0){
			$scenarios['Team']='update';
		} else {
			$scenarios['Team']='create';
		}
		if ( (int)$fkVal > 0){
			$scenarios['Org']='update';
		} else {
			$scenarios['Org']='create';
		}

		if ($scenarios['Team'] === 'update'){
			$Team = Team::model()->findByPk($pkVal);
		} elseif($scenarios['Team'] === 'create') {
			$Team = new Team();
		} else {
			throw new CException('unknown use case for team scenario', 707);
		}

		if ($scenarios['Org'] === 'update'){
			$Org = Org::model()->findByPk($fkVal);
		} elseif($scenarios['Org'] === 'create') {
			$Org = new Org();
		} else {
			throw new CException('unknown use case for org scenario', 707);
		}

		//		$teamAttibutesBefore = $Team->getAttributes();
		//		$orgAttributesBefore = $Org->getAttributes();
		$attributesAndValuesBeforeDbOperations=[];
		$attributesAndValuesBeforeDbOperations['Team']     = $Team->getAttributes();
		$attributesAndValuesBeforeDbOperations['TeamOrg']  = $Org->getAttributes();

		$xlation = self::fetchTargetModelFieldNameXlation($fva);
		$modelsToInclude=['Team','TeamOrg','TeamOrgType'];
		$modelNameXlation=['Team'=>'Team','TeamOrg'=>'Org','TeamOrgType'=>'OrgType'];
		$xlationFiltered=[];
		foreach ($xlation as $viewFieldName=>$xlationInfo){
			$targetModel = $xlationInfo['model'];
			if (in_array($targetModel, $modelsToInclude)){
				$xlationFiltered[$viewFieldName] = $xlationInfo;
			}
		}

		// Now filter the fva to reduce the number of values that must be checked for a delta
		$fvaFiltered=[];
		$fvaFilteredOut=[];
		$fvaFiltered_AsModelData=[];
		$xlationFilteredKeys = array_keys($xlationFiltered);
		foreach($fva as $viewFieldName=>$viewFieldValue){
			if (in_array($viewFieldName, $xlationFilteredKeys)){
				$fvaFiltered[$viewFieldName]=$viewFieldValue;
				$viewModelName   = $xlationFiltered[$viewFieldName];

				$targetModelName = $modelNameXlation[$viewModelName];
				$targetFieldName = $xlationFiltered[$viewFieldName];

				$fvaFiltered_AsModelData[$targetModelName][$targetFieldName]=$viewFieldValue;
			} else {
				$fvaFilteredOut[$viewFieldName]=$viewFieldValue;
			}
		}

//		$fvaFiltered_AsModelData_RemovedBecauseThereIsNoDelta=[];
//		$fvaFiltered_AsModelDataWithDelta = $fvaFiltered_AsModelData;

		$fvaFiltered_AsModelData_RemovedBecauseThereIsNoDelta=[];
		$fvaFiltered_AsModelDataWithDelta = $fvaFiltered_AsModelData;

		// <editor-fold defaultstate="collapsed" desc="placeholder for data driven delta check loop">

		// A single loop for both Org and Team models using data driven code.
		// I'm going to go the hard coded route with two loops for my testing (one for org, and one for team).
//		$modelsToDeltaCheck=['Org','Team'];
//		foreach($modelsToDeltaCheck as $modelBeingChecked){
//			// @todo test a generic loop
//			// Org BELONGS_TO Team so the org table values need to be processed first
//			if (! $Org->isNewRecord){ // eg $scenarios['Team'] === 'update'
//				// set the org id
//				$teamCurrentAttibutes = $Team->getAttributes();
//				$teamAttributesProposed = $fvaFiltered_AsModelData['Team'];
//				foreach ($teamCurrentAttibutes as $targetAttributeName=>$targetAttributeValue){
//					if (array_key_exists($targetAttributeName, $teamAttributesProposed)){
//						$currentValue  = $targetAttributeValue;
//						$proposedValue = $teamAttributesProposed;
//						if ($currentValue === $proposedValue){
//							if (!in_array($targetAttributeName, $targetKeys)){
//								$fvaFiltered_AsModelData_RemovedBecauseThereIsNoDelta['Team'][$targetAttributeName][$targetAttributeValue];
//								unset($fvaFiltered_AsModelDataWithDelta[$targetModelName][$targetFieldName]);
//							}
//						}
//					}
//				}
//				// @todo Make a temp copy of the team model for reference and testing the various ways to set attributes
//				$TeamUpdate1 = $Team; // Use attribute bulk assignment via the magical __set method (used by yii controllers)
//				$TeamUpdate2 = $Team; // Use with setAttributes(safeOnly=true)
//				$TeamUpdate3 = $Team; // Use with setAttributes(safeOnly=false)
//
//				$safeOnlyTrue  = true;
//				$safeOnlyFalse = false;
//
//				if (count($fvaFiltered_AsModelDataWithDelta['Team']) > 0){
//					//$Team->setAttributes($values, $safeOnly);
//					$TeamUpdate1->attributes = $fvaFiltered_AsModelDataWithDelta['Team'];
//					$TeamUpdate2->setAttributes($fvaFiltered_AsModelDataWithDelta['Team'], $safeOnlyTrue);
//					$TeamUpdate2->setAttributes($fvaFiltered_AsModelDataWithDelta['Team'], $safeOnlyFalse);
//				}
//			}
//		}
// </editor-fold>

		$runValidation = true;
		$safeOnlyTrue  = true;
		$safeOnlyFalse = false;

		// @todo Make a temp copy of the Org model for reference and testing the various ways to set attributes
		$Org1 = $Org; // Use attribute bulk assignment via the magical __set method (used by yii controllers)
		$Org2 = $Org; // Use with setAttributes(safeOnly=true)
		$Org3 = $Org; // Use with setAttributes(safeOnly=false)


		// Org BELONGS_TO Team so the org table values need to be processed first
		if (! $Org->isNewRecord){ // eg $scenarios['Org'] === 'update'
			// set the org id
			$orgCurrentAttibutes = $Org->getAttributes();
			$orgAttributesProposed = $fvaFiltered_AsModelData['Org'];
			foreach ($orgCurrentAttibutes as $targetAttributeName=>$targetAttributeValue){
				if (array_key_exists($targetAttributeName, $orgAttributesProposed)){
					$currentValue  = $targetAttributeValue;
					$proposedValue = $orgAttributesProposed;
					if ($currentValue === $proposedValue){
						if (!in_array($targetAttributeName, $targetKeys)){
							$fvaFiltered_AsModelData_RemovedBecauseThereIsNoDelta['Org'][$targetAttributeName][$targetAttributeValue];
							unset($fvaFiltered_AsModelDataWithDelta[$targetModelName][$targetFieldName]);
						}
					}
				}
			}

			if (count($fvaFiltered_AsModelDataWithDelta['Org']) > 0){
				//$Org->setAttributes($values, $safeOnly);
				$Org1->attributes = $fvaFiltered_AsModelDataWithDelta['Org'];
				$Org2->setAttributes($fvaFiltered_AsModelDataWithDelta['Org'], $safeOnlyTrue);
				$Org3->setAttributes($fvaFiltered_AsModelDataWithDelta['Org'], $safeOnlyFalse);
			}
		} elseif ($Org->isNewRecord){ // eg $scenarios['Org'] === 'create'
			// No delta filtering is needed on row Create
			//$Org->setAttributes($values, $safeOnly);
			$Org1->attributes  = $fvaFiltered_AsModelData['Org'];
			$Org2->setAttributes($fvaFiltered_AsModelData['Org'], $safeOnlyTrue);
			$Org3->setAttributes($fvaFiltered_AsModelData['Org'], $safeOnlyFalse);
		}

		$diffOrg_1A = array_diff($Org1->attributes, $Org->attributes);
		$diffOrg_1B = array_diff($Org->attributes,  $Org1->attributes);

		$diffOrg_2A = array_diff($Org2->attributes, $Org->attributes);
		$diffOrg_2B = array_diff($Org->attributes,  $Org2->attributes);

		$diffOrg_3A = array_diff($Org3->attributes, $Org->attributes);
		$diffOrg_3B = array_diff($Org->attributes,  $Org3->attributes);


		if ( $Org->save($runValidation) ){
			$scmda['success'][] = 'TeamOrg';
			if ($scenarios['Org'] === 'insert'){
				$scmda['modelsTouched']['TeamOrg']['proposed']  = $fvaFiltered_AsModelData['Org'];

			} elseif ($scenarios['Org'] === 'update'){
				$scmda['modelsTouched']['TeamOrg']['proposed']       = $fvaFiltered_AsModelDataWithDelta['Org'];
				$scmda['modelsTouched']['TeamOrg']['ignoredNoDelta'] = $fvaFiltered_AsModelData_RemovedBecauseThereIsNoDelta['Org'];
			}
			$scmda['modelsTouched']['TeamOrg']['afterSave']  = $Org->attributes;
			$scmda['modelsTouched']['TeamOrg']['beforeSave'] = $attributesAndValuesBeforeDbOperations['TeamOrg'];

		} else {
			$scmda['errors']['TeamOrg'] = $Org->getErrors();
		}


		// @todo Make a temp copy of the team model for reference and testing the various ways to set attributes
		$Team1 = $Team; // Use attribute bulk assignment via the magical __set method (used by yii controllers)
		$Team2 = $Team; // Use with setAttributes(safeOnly=true)
		$Team3 = $Team; // Use with setAttributes(safeOnly=false)


		if (! $Team->isNewRecord){ // eg $scenarios['Team'] === 'update'
			// set the org id
			$teamCurrentAttibutes = $Team->getAttributes();
			$teamAttributesProposed = $fvaFiltered_AsModelData['Team'];
			foreach ($teamCurrentAttibutes as $targetAttributeName=>$targetAttributeValue){
				if (array_key_exists($targetAttributeName, $teamAttributesProposed)){
					$currentValue  = $targetAttributeValue;
					$proposedValue = $teamAttributesProposed;
					if ($currentValue === $proposedValue){
						if (!in_array($targetAttributeName, $targetKeys)){
							$fvaFiltered_AsModelData_RemovedBecauseThereIsNoDelta['Team'][$targetAttributeName] = $targetAttributeValue;
							unset($fvaFiltered_AsModelDataWithDelta[$targetModelName][$targetFieldName]);
						}
					}
				}
			}

			if (count($fvaFiltered_AsModelDataWithDelta['Team']) > 0){
				//$Team->setAttributes($values, $safeOnly);
				$Team1->attributes =  $fvaFiltered_AsModelDataWithDelta['Team'];
				$Team2->setAttributes($fvaFiltered_AsModelDataWithDelta['Team'], $safeOnlyTrue);
				$Team2->setAttributes($fvaFiltered_AsModelDataWithDelta['Team'], $safeOnlyFalse);
			}
		} elseif ($Team->isNewRecord){ // eg $scenarios['Team'] === 'create'
			// No delta filtering is needed on row Create
			// $Team->setAttributes($values, $safeOnly);
			$Team1->attributes  = $fvaFiltered_AsModelData['Team'];
			$Team2->setAttributes($fvaFiltered_AsModelData['Team'], $safeOnlyTrue);
			$Team3->setAttributes($fvaFiltered_AsModelData['Team'], $safeOnlyFalse);
		}


		$diffTeam_1A = array_diff($Team1->attributes, $Team->attributes);
		$diffTeam_1B = array_diff($Team->attributes,  $Team1->attributes);

		$diffTeam_2A = array_diff($Team2->attributes, $Team->attributes);
		$diffTeam_2B = array_diff($Team->attributes,  $Team2->attributes);

		$diffTeam_3A = array_diff($Team3->attributes, $Team->attributes);
		$diffTeam_3B = array_diff($Team->attributes,  $Team3->attributes);


		if ( $Team->save($runValidation) ){
			$scmda['success'][] = 'Team';
			if ($scenarios['Team'] === 'insert'){
				$scmda['modelsTouched']['Team']['proposed']  = $fvaFiltered_AsModelData['Team'];

			} elseif ($scenarios['Team'] === 'update'){
				$scmda['modelsTouched']['Team']['proposed']  = $fvaFiltered_AsModelDataWithDelta['Team'];
				$scmda['modelsTouched']['Team']['ignoredNoDelta'] = $fvaFiltered_AsModelData_RemovedBecauseThereIsNoDelta['Team'];
			}
			$scmda['modelsTouched']['Team']['afterSave']  = $Team->attributes;
			$scmda['modelsTouched']['Team']['beforeSave'] = $attributesAndValuesBeforeDbOperations['Team'];

		} else {
			$scmda['errors']['Team'] = $Team->getErrors();
		}

		return $scmda;
	}

	/**
	 *
	 * @param type $teamPlayerID
	 * @internal Called by AthleteTeams views that require coach email
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function fetchCoachPersonByTeamPlayerID($teamPlayerID) {

		$localDebug = true;
		// Get a list of coach ids that are linked to the player ID
		$sql = "select distinct coach_id from vwTeam_leftouter "
				. "where team_player_id = :team_player_id "
				. "order by coach_name";
		$cmd = Yii::app()->db->createCommand($sql);
		$params = [':team_player_id'=>$teamPlayerID];
		$fetchAssociative = true;
		$data   = $cmd->queryAll($fetchAssociative, $params);
		$coachIDs = array_values($data);
		$coachPersonRows     = Coach::model()->with('person')->findAllByAttributes(['coach_id'=>$coachIDs]);
		//$personCoachRowsTest = Person::model()->with('coach')->findAllByAttributes(['team_player_id'=>$teamPlayerID]);

		if ($localDebug){
			$msg = "coachPerson follows";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($coachPersonRows, 10, true);

//			$msg2 = "personCoach follows";
//			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg2);
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($personCoachRows, 10, true);
		}
		//return $personCoachRows;
		// index CActiveRecord array by team_player_id
		$out = [];
		foreach ($coachPersonRows as $coachPersonRow){
			$out[$coachPersonRow->getPrimaryKey()] = $coachPersonRow;
		}
		return $out;
	}

	/**
	 * Returns an array of coachPersons indexed by CoachID that are linked to a player via teamPlayer
	 * @param int|string $playerID
	 * @return CActiveRecord
	 * @internal Called by AthleteTeams views that require coach email
	 * @internal For a simple value fetch see VwTeamLeftouter
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function fetchCoachPersonsByPlayerID($playerID) {

		$localDebug = true;

		// Get a list of coach ids that are linked to the player ID
		$sql = "select distinct coach_id from vwTeam_leftouter where player_id = :player_id;";
		$cmd = Yii::app()->db->createCommand($sql);
		$params = [':player_id'=>$playerID];
		$fetchAssociative = true;
		$data   = $cmd->queryAll($fetchAssociative, $params);
		$coachIDs = array_values($data);
		$coachPersonRows     = Coach::model()->with('person')->findAllByAttributes(['coach_id'=>$coachIDs]);
		//$personCoachRows     = Person::model()->with('coach')->findAllByAttributes(['team_player_id'=>$teamPlayerID]);
		//$personCoachRowsTest = Person::model()->with('coach')->findAllByAttributes(['team_player_id'=>$teamPlayerID]);

		if ($localDebug){
			$msg = "coachPerson follows";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($coachPersonRows, 10, true);

//			$msg2 = "personCoach follows";
//			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg2);
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($personCoachRows, 10, true);
		}
		// Consider indexing the list by coach id
		$out = [];
		foreach ($coachPersonRows as $coachPersonRow){
			$out[$coachPersonRow->getPrimaryKey()] = $coachPersonRow;
		}
		//return $coachPersonRows;
		return $out;
	}


	/**
	 * Allow a SQL flatened view data structure to be consumed as the Yii
	 *   CActiveRecord structure of the Coach class. The output allows a
	 *   call to Coach->setAttributes() and Coach->saveWithRelated() with
	 *   the output of this method.
	 * @param array $fieldValueArray SQL multi-table flattened view structure
	 *   eg requires unique field names across all fields in the sql view.
	 * @return array $out[...['person][...]]
	 * @todo Convert an mtfva to a Coach->Person CActiveRecord array structure
	 */
	protected function xlatFvaToCoachPerson($fieldValueArray) {
		// @todo Call the standard xlat functions to get an known mtfva structure
		// @todo parse Coach and Coach Person and stack them using the
		//   Coach->Relations class name to stack Person under Coach so our
		//   generic saveWithRelations() Behavior on CActiveRecord
		//
		$mtfva	= self::generateModelValuesArray($fieldValueArray); // data envelope array
		if (isset($mtfva['Coach'])){
			$out = $mtfva['Coach'];
		}
		if (isset($mtfva['PersonCoach'])){
			$out['person'] = $mtfva['PersonCoach'];
		}

		return $out; // AS coachPersonFields eg array values that are yii object ready;
	}

	/**
	 *
	 * @param array $coachPersonFields An associative array using the Coach->Attributes structure
	 *   eg a CActiveRecord structure
	 * @internal Called by AthleteTeams views that require coach email
	 * @internal Development Status = ready for testing (rft)
	 * @internal Params array may include external reference fields
	 * @internal Uses the BaseModel::storeChain() internal arrays naming convention
	 * @todo receive and consume external reference fields (teamPlayerID, $teamID)
	 *
	 */
	public static function saveCoachPersonFields($coachPersonFields) {
		$scenarios      = []; // coach, person, team_player; {update|create}
		$pkeys          = []; // written to as various models are processed
		$success		= []; // written to on model save
		$errors			= []; // written to on model save
		$keys			= []; // create a holder for all keys (pkey, and fkeys)
		//$models_touched = []; // stores all saved model=>attributes
		$runValidation = true;

		if (array_key_exists('coach_id', $coachPersonFields)){
			$coach_id = $coachPersonFields['coach_id'];
		} else {
			$scenarios['Coach'] = 'create';
			$coach_id = '';
		}
		if ((int)$coach_id > 0){
			$scenarios['Coach'] = 'update';
			$Coach = Coach::model()->findByPk($coach_id);
		} else {
			$scenarios['Coach'] = 'create';
			$Coach = new Coach();
		}


		// Save the coach's person table values (coach_name, coach_email, etc)
		if (array_key_exists('person', $coachPersonFields)){
			if (isset($Coach)){
				$personValues  = $coachPersonFields['person'];
				$Coach->person = $personValues;
			}
			$Coach = $coachPersonFields;
			$coachPersonResult = $Coach->saveWithRelated('person');
			if ($Coach->person->hasErrors()){
				$errors['Person'] = $Coach->person->errors;
			}
			if ($Coach->hasErrors()){
				$errors['Coach'] = $Coach->errors;
			}
			// test
			if ($coachPersonResult == false){
				$errorTest = $Coach->errors;
			}
			$coach_id = $Coach->getPrimaryKey();
			$pkeys['coach_id'] = $coach_id ;
			$out['Coach'] = ['scenario'=>$scenarios['Coach'], 'status'=>'row created', 'coach_id'=>$coach_id,];
		}


		// If a teamPlayer row does not exist then create one
		if (isset($coachPersonFields['team_player_id'])){
			$teamPlayerID = $coachPersonFields['team_player_id'];
		}
		if ((int)$teamPlayerID == 0){
			$scenarios['TeamPlayer']='create';
			if (! isset($coachPersonFields['team_id'])){
				throw new CException('team_id is a required external reference field', 707);
			}
			if (! isset($coachPersonFields['player_id'])){
				throw new CException('player_id is a required external reference field', 707);
			}
			$TeamPlayer = new TeamPlayer();
			$TeamPlayerValuesToSet = [
				'team_id'    =>$coachPersonFields['team_id'],
				'player_id'  =>$coachPersonFields['player_id'],
				'coach_id'   =>$coach_id, // or $pkeys['Coach']
				'coach_name' =>$Coach->person->person_name_full,
			];
		} else {
			$TeamPlayer = TeamPlayer::model()->findByPk($teamPlayerID);
			$TeamPlayerValuesToSet = [
				// team_id and player_id are a unique composite key in the team_player table
				//   therefore they can never be updated after they are initially set
				//'team_id'   =>$coachPersonFields['team_id'],   // Once a team player team id is set it can never be updated
				//'player_id' =>$coachPersonFields['player_id'], // Once a team player player id is set it can never be updated
				'coach_id'   =>$coach_id, // or $pkeys['Coach']
				'coach_name' =>$Coach->person->person_name_full,
			];
		}
		// Save the teamPlayer values

		$TeamPlayer->setAttributes($TeamPlayerValuesToSet);
		if ($TeamPlayer->save($runValidation)){
			$team_player_id = $TeamPlayer->getPrimaryKey();
			$success['TeamPlayer'];
		} else {
			$team_player_id = $teamPlayerID;
			$errors['TeamPlayer'] = $TeamPlayer->errors;
		}
		$pkeys['team_player_id'] = $team_player_id;
		$keys['team_player_id']  = $team_player_id;


		$localDebug = true;
		// @todo parse coach attributes via an intersection
		$coachAttributes = Coach::model()->getAttributes();
		$fieldNamesFromModel = array_keys($coachAttributes);
		// @todo remove for loops below after testing
		foreach ($coachAttributes as $fieldName => $fieldValue) {
			if (true==true){
				continue;
			}
		}
		foreach ($fieldNamesFromModel as $fieldIdx => $fieldName) {
			if (true==true){
				continue;
			}
		}
		$coachAttributesIntersectionKeys = array_intersect_key($coachPersonFields, $coachAttributes);
		$coachAttributesIntersection     = array_intersect($coachPersonFields, $coachAttributes);

		$out = [$pkeys, $keys, $success, $errors, $scenarios];
		if ($localDebug){
			$msg = "coachPerson follows";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($coachPersonRows, 10, true);
		}

		return $out;
	}

	/**
	 * Designed to save model information using the base models and their ESaveRelatedBehavior eg
	 *  saveWithRelated() and saveRelated()
	 * @param array|CActiveRecord $viewModelAttributeVals
	 *   An associative array keyed by modelNames and sub-keyed on viewModel->attributeNames()
	 * @todo Allow the follow the paras for the vmav
	 *   An array of arrays
	 *   An AthleteTeam Model-object
	 *   An numeric indexed array of AthleteTeam Model-objects
	 *   An numeric indexed array keyed by pkVal of AthleteTeam Model-objects
	 *
	 * @param type $sparse
	 * @return bool Should this return an scmda array instead?
	 * @internal Called by self::storeModels()
	 * @internal Development Status = ready for testing (rft)
	 * @uses self::getRelationsAsSparseList()
	 * @uses ESaveRelatedBehavior
	 * @internal You can view data written in the application log by searching for
	 *   ESaveRelatedBehavior->saveWithRelated() when localDebug=true in ESaveRelatedBehavior
	 * @see protected/components/ESaveRelatedBehavior.php
	 * @see BaseModel::StoreWithERelated_TestHarness()
	 * @see BaseModel::StoreWithERelated()
	 * @uses self::walkRelationsStack()
	 * @todo Should this return an scmda array instead? No, it should not there are no
	 *        data writes.
	 */
	public static function generateYiiNormativeModelValuesArray(array $viewModelAttributeVals, $sparse=true) {
		// Assert the params data type
		// @todo Assert that multi-model field-value data array has been passed.
		$vmavInput = $viewModelAttributeVals;
		if (isset($vmavInput['team_player_id'])){
			$teamPlayerID     = $vmav['team_player_id'];
			$vmavBeforeUpdate = AthleteTeams::model()->findByPk($teamPlayerID)->getAttributes();
			$dataArchive = [
				'method'=>__METHOD__,
				'input' =>$vmavInput,
				'currentValues'=>$vmavBeforeUpdate,
			];
			self::archiveData($dataArchive);
		}
		// for debug and construction
		//$vmav =  		$team_player_id=78;
		// @todo 12-13 an mtva is being passed into this function

		$mtfva = self::generateModelValuesArray($vmavInput, $sparse=false);

		$cmModelsRef  = self::getComponentModels();
		$cmRelsSparse = BaseModel::fetchModelRelationsMultipleAsSparseList($cmModelsRef, $cmModelsRef, $eSaveOnly=true);

		$ynmTreeRef=[];
		$ynmTreeData=[];
		$ynBatch=[];
		$ynData=[];
		foreach ($cmRelsSparse as $cmModel => $cmRels) {

			foreach ($cmRels['byModelName'] as $relInfo) {
				// Data Template
				$ynmTreeData[$cmModel][$relInfo['relationName']] = [$relInfo['fkey']];
				// Reference Template
				$ynmTreeRef[$cmModel][$relInfo['relatedModel']][$relInfo['relationName']] = [$relInfo['fkey']];
			}
		}
		foreach ($cmRelsSparse as $cmModel => $cmRels) {

			if (isset($mtfva[$cmModel])){
				if (count($mtfva[$cmModel])>0){
					$mtBase[$cmModel]=$mtfva[$cmModel];
				}
				$rels = [];
				foreach ($ynmTreeRef[$cmModel] as $relatedModel => $shortRelInfo) {
					if (isset($mtfva[$relatedModel]) && count($mtfva[$relatedModel])>0){
						$rels[$cmModel][$relatedModel]=$mtfva[$relatedModel];
						$rnArray = array_keys($shortRelInfo);
						$relationName = $rnArray[0];
						$mtBase[$cmModel][$relationName] = $mtfva[$relatedModel];
					}
				}
			}
			$ynData = $ynData + $mtBase;
			//$ynBatch[$cmModel] = $mtBase;
			foreach ($mtBase as $modelName=>$modelData){
				$ynBatch[$modelName] = $modelData;
			}
		}

		$out=[
			'yiiTargets'		=>$ynBatch,
			'sourceData'        =>$mtfva,
		];

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($ynData, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($ynBatch, 10, true);

		// The following call has been depreciated
//		$results = self::walkRelationsStack($cmRelsSparse, $out);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($results, 10, true);
//		return $results;


		return $out;
	}

	/**
	 * Call yii model validations prior to committing data
	 * @param array $cmRelsSparse A relations meta-data array
	 * @param array $yiiData yii normalized data created from an mtfva
	 * @return array $scmda A standard structure for reporting on multi-step processes
	 * @internal Development Status = ready for testing (rft)
	 * @deprecated since version 0.54.3
	 */
	public static function walkRelationsStack(array $cmRelsSparse, array $yiiData){

		// @todo Assert that multi-model field-value data array has been passed.
		$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();

		// tree with model name keys that contains relations by relationType
		$relationTreesMaster = self::relationsTree($treeName='All');
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($relationTreesMaster, 10, true);

		$treeNames = array_keys($relationTreesMaster);
		$yiiModels = array_keys($yiiData['yiiTargets']);
		$yiiMmava  = $yiiData['yiiTargets']; // yii multi-model attribute values array

		$cmModels = self::getComponentModels();
		$cmAttrNames = BaseModel::fetchModelFieldNamesMuliple($cmModels);

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($treeNames, 10, true);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($yiiModels, 10, true);

		$isReadOnly = true;
		$cmRelsSparseCopy = $cmRelsSparse;

		foreach ($cmRelsSparse as $cmModel => $cmRels) {

			$cmModelChecked = false;

			//if ( ! isset($yiiModels[$cmModel]) ){
			if ( !in_array($cmModel, $yiiModels) ){
				// The yii model is not present in the data to be saved
				continue;
			}

			$pkName = $cmAttrNames[$cmModel]['keys'][0];
			$okToMassAssignCmModel=false;
			if ( $cmModelChecked === false && isset($yiiMmava[$cmModel][$pkName]) ){
				// determine insert or update
				$pkVal = $yiiMmava[$cmModel][$pkName];
				if ((int)$pkVal > 0){
					$model = $cmModel::model()->findByPk($pkVal);
					$okToMassAssignCmModel=true;
					$scmda['scenario'][$cmModel] = 'update';
					$cmModelChecked = true;
				}
			}
//				else {
//					$model = new $cmModel();
//					$scmda['scenario'][$cmModel] = 'insert';
//					//$okToMassAssignCmModel=false;
//				}
			if ($cmModelChecked === false && isset($yiiMmava[$cmModel])) {
				$cmModelTemp = new $cmModel();
				$scmda['scenario'][$cmModel] = 'insert';
				$cmModelVals = $yiiMmava[$cmModel];
				// temporarily remove the related models attributes so data validation can be performed
				foreach($cmRels as $relationNameTemp=>$relInfoTemp){
					if (isset($cmModelVals[$relationNameTemp])){
						unset($cmModelVals[$relationNameTemp]);
					}
				}
				$cmModelTemp->attributes = $cmModelVals;
				$cmModelValValid = $cmModelTemp->validate();
				if ($cmModelValValid){
					$okToMassAssignCmModel=true;
					$scmda['success'][$cmModel][]='passed validation';
					$model = new $cmModel();
					$scmda['scenario'][$cmModel] = 'insert';
				} else  {
					$scmda['errors'][$cmModel][]='failed validation';
				}
				//unset($cmModelTemp);
				$cmModelTemp = null;
			} else {
				continue;
			}

			$okToMassAssignRelated=[];
			foreach ($cmRels['byRelationName'] as $relationName=>$relInfo) {

				if ( isset($yiiMmava[$cmModel][$relationName]) ){
					// compensate for the left outer joins and verify that the
					// required values are present in the related model.

					$okToMassAssignRelated[$relationName]=false;

					$relatedModelName   = $relInfo['relatedModel'];
					$relatedModelPkName = $cmAttrNames[$relatedModelName]['keys'][0];
					$relatedModelVals   = $yiiMmava[$cmModel][$relationName];
					if (isset($relatedModelVals[$relatedModelPkName]) && (int)$relatedModelVals[$relatedModelPkName]>0){
						$okToMassAssignRelated[$relationName] = true;
					} else {
						// Run validation to verify the required fields are present
						$temp = new $relatedModelName();
						//$temp = new AthleteTeams();
						$temp->attributes = $relatedModelVals;
						$relatedModelAttrInVals = array_keys($relatedModelVals);
						$valid = $temp->validate($relatedModelAttrInVals, $clearErrors=true);
						if ($valid){
							$okToMassAssignRelated[$relationName] = true;
						} else {
							$scmda['errors'][$cmModel][$relationName][]  = 'failed validation';
							$scmda['errors'][$cmModel][$relationName][]  = $temp->getErrors();
						}
						$temp = null;
					}
					// mass assign the values  to trigger a related model to be instantiated
					if ($okToMassAssignRelated[$relationName]){
						$model->$relationName = $yiiMmava[$cmModel][$relationName];
					} else {
						$model->$relationName = [];
					}
				} else {
					$model->$relationName = [];
				}
				if ($okToMassAssignRelated[$relationName]){
					$scmda['success'][$cmModel][$relationName] = 'passed validation';
				} else {
					$scmda['errors'][$cmModel][$relationName][] = ['failed validation'];
					$scmda['errors'][$cmModel][$relationName][] = $temp->getErrors();
				}
			}

			// Now assign all values now that the related models have been
			// instantiated by the __set() and setAttributes() call that ran
			// in the background during the mass assignment.
			$model->attributes = $yiiMmava[$cmModel];
			if( $isReadOnly===false && $model->save() ){
				$scmda['modelsTouched'][$cmModel] = $yiiMmava[$cmModel];
				$scmda['pkeys'] = [$pkName=>$model->getPrimaryKey()];
				$scmda['keys']  = 'blank';
			} else {
				// all related models
				$scmda['errors'][$cmModel][] = 'failed to save';
			}
		}

		return $scmda;
	}


	/**
	 *
	 * @depreciated since version 0.54.3
	 * @throws CException
	 */
	protected function generateYiiNormative_prototypeCode_v1() {

/*
		$componentModels = self::getComponentModels();
		$cmModels = $componentModels;

		//$valuesBeforeAttrUpdate = [];
		// Use an attribute list per base model that is grouped by ['keys','non-keys','all-fields']
		// view model = $vm
		// component models  = $cm
		$vmAttrNames = AthleteTeams::model()->attributeNames();
		$cmAttrNames = BaseModel::fetchModelFieldNamesMuliple($cmModels);

		$cmRelations  = BaseModel::fetchModelRelationsMultipleAsSparseList($cmModels, $cmModels, $eSaveOnly=false);
		$cmRelsSparse = BaseModel::fetchModelRelationsMultipleAsSparseList($cmModels, $cmModels, $eSaveOnly=true);
		//$cmRelationsSparse  = BaseModel::fetchModelRelationsMultipleAsSparseList($modelsList, $modelsAllowed, $eSaveOnly);

		//array_keys(self::model()->getAttributes());
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($vmAttrNames, 10, true);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cmAttrNames, 10, true);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cmRelations, 10, true);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cmRelsSparse, 10, true);
		//$IncommingData_metaData = [];
		$localDebug = true;
		$ynmTreeRef=[];
		$ynmTreeData=[];
		foreach ($cmRelsSparse as $cmModel => $cmRels) {

			foreach ($cmRels['byModelName'] as $relInfo) {
				// Production
				$ynmTreeData[$cmModel][$relInfo['relationName']] = [$relInfo['fkey']];
				// Template
				$ynmTreeRef[$cmModel][$relInfo['relatedModel']][$relInfo['relationName']] = [$relInfo['fkey']];
			}
		}

		foreach ($cmRelsSparse as $cmModel => $cmRels) {

			foreach ($cmRels['byModelName'] as $relInfo) {
				// Production
				$ynmTreeData[$cmModel][$relInfo['relationName']] = [$relInfo['fkey']];
				// Template
				$ynmTreeRef[$cmModel][$relInfo['relatedModel']][$relInfo['relationName']] = [$relInfo['fkey']];
			}
		}

		$result = BaseModel::walkRelationsStack('AthleteTeams', $mtfva);

//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($ynmTreeRef, 10, true);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($ynmTreeData, 10, true);
		$e1 = Barray::dumpAsString($ynmTreeRef, 10);
		$e2 = Barray::dumpAsString($ynmTreeData,10);

		echo "<pre>$e1</pre><pre>$e2</pre>";

		$mtfvaModels = array_keys($mtfva);
		$ynmava = [];
		$ynBatchTemplate =[
			'Org'=>[
				'Person'=>'person',
				'Team'  =>'team',
					],
			'Person'=>[
				'Player' =>'players',
				],
			'Player' =>[
				'TeamPlayer'=>'teamPlayers',
				''=>'',
				],
			'TeamPlayer'=>'',
		];

		foreach ($mtfvaModels as $cmModelName) {
			//$cmPkName = $cmAttrNames[$cmModelName]['keys'][0];
			$cmPkVal  = '';
			$ynmava[$cmModelName];

		}



		if (isset($vmava['team_player_id'])){
			$team_player_id = $vmava['team_player_id'];

		} else {
			$team_player_id = '';
		}


		// fetch all current attribute values
		if ((int)$team_player_id > 0){
			$AthleteTeams = AthleteTeams::model()->findByPk($team_player_id);
		} elseif ((int)$team_player_id == 0){
			$vmava = $viewModelAttrVals; // view attribute
		}

		if ( ! is_null($AthleteTeams) && isset($AthleteTeams) ){

		}


		// for test data use $sparse = false
		$sparse = false;

		$vmavAllFields  = self::generateModelValuesArray($viewModelAttributeVals, $sparse=false);
		$vmavSparse     = self::generateModelValuesArray($viewModelAttributeVals, $sparse=true);

		// capture all current values
		$AthleteTeams = AthleteTeams::model()->findByPk($AthleteTeams);
		$vmavaBfrAttrUpdate = self::generateModelValuesArray(self::$viewFieldValueArray, $sparse=false);
		//$componentModels = self::getComponentModels();
		$sparseModelsInMmava_Unfiltered = array_keys($mmava);
		$sparseModelsInMmava            = array_keys($mmava);
		$temp = array_values($sparseModelsInMmava, $componentModels);

		//$diffModelList = array_diff($sparseModelsInMtfvaUnfiltered, $sparseModelsInMtfva);
		//$batchChain = $this->relationChainHierarchy($personTypeName='Athlete');



		// @todo Build relation name keyed array
		// @todo Use relation names to process the data
		//
		$chain_v01=[
			1=>['OrgType'],
			2=>['OrgLevel'],
			3=>['Org','Person'],
			4=>['Player'],
			5=>['TeamDivsion'],
			6=>['TeamLeague'],
			7=>['SportPosition'],
			8=>['Team','TeamPlayer'],
			9=>['Team','TeamOrg'],
		];

		// modelNames InRelationTrees then populate with mmav (multi-model-attribute-values) by model name

		$relationTreeByModelName =  self::relationsTree($treeName='All');
		if (! is_array($relationTreeByModelName)){
			throw new CException("AthleteTeams::relationTree failed returned an array",707);
		}

		$relationTreesByModelName_blankTemplate =  self::relationsTree($treeName='All');
		$relationTreesByTreeNameThenModelNames  = $relationTreesByModelName_blankTemplate;


		$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		$scmdaValsBfrAttrUpdate = $scmda;  // stores pre-update reference data and data values
		$scmdaTransactionReport = $scmda;  // tracks actual attribute updates
		// @todo fetch all pkeys from each model

		// @todo fetch all fkeys from each model
		$relationTreeByModelNameOut=[];
		foreach ($relationTreesByTreeNameThenModelNames as $treeName=>$relationTree){
			foreach($relationTree as $modelName){
				$scmda['keys'][$modelName]  = BaseModel::fetchModelKeyFieldNames($modelName);
				$scmda['pkeys'][$modelName] = BaseModel::fetchModelFkeyFieldNames($modelName);
				$relationTreeByModelNameOut[$modelName]['pkeys'] = $scmda['keys'][$modelName];
			}
		}

		$relationTreeByModelNameOut =  $relationTreeByModelName_blankTemplate; // self::relationsTree($treeName='All');

		$relationTreesMaster = self::relationsTree($treeName='All');
  */
	}

	/**
	 * @deprecated since version 0.54.3
	 */
	protected function generateYiiNormative_prototypeCode_v0() {

//		$eSaveOnly=true;
//		$sparseRelationsForAthleteTeams        = self::getRelationsAsSparseList($eSaveOnly=true);
//		$sparseRelationsForAthleteTeamsAsESave = self::getRelationsAsSparseList($eSaveOnly=false);
//
//		if (is_array($sparseRelationsForAthleteTeams) && is_array($sparseRelationsForAthleteTeamsAsESave)){
//			$allRelationsElementsCount = count($sparseRelationsForAthleteTeams, COUNT_RECURSIVE);
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($allRelationsElementsCount, 10, true);
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($sparseRelationsForAthleteTeams, 10, true);
//
//			$allEsaveRelationsElementsCount = count($sparseRelationsForAthleteTeams, COUNT_RECURSIVE);
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($allEsaveRelationsElementsCount, true);
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($sparseRelationsForAthleteTeamsAsESave, 10, true);
//
//			// Running a diff on the relation bundels does not work. Seems to be too deep of a tree.
//			// @todo Try separating them.
//			//$eSaveOnlyDiff1 = array_diff_assoc($sparseRelationsForAthleteTeamsAsESave, $sparseRelationsForAthleteTeams);
//			//$eSaveOnlyDiff2 = array_diff_assoc($sparseRelationsForAthleteTeams       , $sparseRelationsForAthleteTeamsAsESave);
//
//			//$eSaveOnlyDiff1 = array_diff_assoc($sparseRelationsForAthleteTeamsAsESave, $sparseRelationsForAthleteTeams);
//		} else {
//			throw new CException("relations are not arrays", 707);
//		}

		//			if ($vmavIsAssoc === true){
		//				// the array keys are associative
		//				$IncommingData_metaData['ContainerType'] = 'Associative array';
		//
		//				if ( is_string($keyValue) && in_array($keyValue, $componentModels)){
		//					// The array keys are associative model names a mmava structure
		//					$IncommingData_metaData['General Desc'] = 'Assoc array of models managed by the AthleteTeams model';
		//					$incommingDataTypeDesc = 'indexed array of models managed by the AthleteTeams modle';
		//				}
		//				if ( is_string($keyValue) && in_array($keyValue, $attributeNames)){
		//					// The array keys are associative model names a mmava structure
		//					$IncommingData_metaData['General Desc'] = 'indexed array of models managed by the AthleteTeams model';
		//					$incommingDataTypeDesc = 'indexed array of models managed by the AthleteTeams modle';
		//				}
		//			} elseif ($vmavIsAssoc === false){
		//				// the array keys are sequencial numerics eg an indexed array
		//				$IncommingData_metaData['ContainerType'] = 'Numeric indexed array';
		//
		//				if ( is_object($vmav[$keyValue]) && $vmav[$keyValue] instanceof AthleteTeams){
		//					$IncommingData_metaData['DataType']     = 'AthleteTeams';
		//					$IncommingData_metaData['General Desc'] = 'indexed array of AthleteTeams objects'; // eg a multi-row bulk update
		//
		//				} elseif ( is_object($vmav[$keyValue]) && $vmav[$keyValue] instanceof CActiveRecord){
		//					$IncommingData_metaData['DataType']     = 'CActiveRecord';
		//					$IncommingData_metaData['General Desc'] = 'indexed array of CActiveRecord objects';
		//				}
		//
		//				if ( is_numeric($keyValue) && is_array(vmav[$keyValue]) ){
		//					// The array keys are a potentionally associative model attribute
		//					// names eg an ava structure inside a numeric index wrapper array
		//					$IncommingData_metaData['General Desc'] = 'indexed array of models managed by the AthleteTeams model';
		//					$incommingDataTypeDesc = 'indexed array of models managed by the AthleteTeams modle';
		//				}
		//			}
		//		} elseif (is_object($vmav) ){
		//				$IncommingData_metaData['ContainerType'] = 'None';
		//				if ( is_object($vmav) && $vmav instanceof AthleteTeams){
		//					$IncommingData_metaData['DataType']     = 'AthleteTeams';
		//					$IncommingData_metaData['General Desc'] = 'A single AthleteTeams object being passed in'; // eg a single-row bulk update
		//
		//				} elseif ( is_object($vmav) && $vmav instanceof CActiveRecord){
		//					$IncommingData_metaData['DataType']     = 'CActiveRecord';
		//					$IncommingData_metaData['General Desc'] = "A single CActiveRecord object (".  get_class($vmav) .")";
		//				}
		//		} elseif (is_string($vmav) && Bjson::is_JSON($vmav)){
		//			$IncommingData_metaData['ContainerType'] = 'None';
		//			$IncommingData_metaData['DataType']      = 'JSON String';
		//			$IncommingData_metaData['General Desc']  = 'One or more models passed as a JSON string'; // eg a single-row bulk update
		//		} else {
		//			throw new CException("Unknown parameter type for {$viewModelAttributeVals}",707);
		//		}\

					// @todo are the Idx keys numeric
					// @todo are the Idx keys associative
					// @todo are the Idx keys associative model names
					// @todo are the Idx keys associative attribute names
		//			if (isset($vmavKeys[0]) ){
		//				// get a key value to
		//				$keyVal = $vmavKeys[0];
		//				if ( is_object($vmav[$keyVal]) && $vmav[$keyVal] instanceof AthleteTeams){
		//					$incommingDataType     = 'AthleteTeams';
		//					$incommingDataTypeDesc = 'indexed array of AthleteTeams objects';
		//
		//				} elseif ( is_object($vmav[$keyVal]) && $vmav[$keyVal] instanceof CActiveRecord){
		//					$incommingDataType     = 'CActiveRecord';
		//					$incommingDataTypeDesc = 'indexed array of CActiveRecord objects';
		//
		//				} elseif ( is_string($pkVal) && in_array($pkVal, self::getComponentModels())){
		//
		//				// the Idx keys are numeric
		//			}elseif (isset($vmavKeys[0]) && is_object($vmavKeys[0] && $vmavKeys[0] instanceof AthleteTeams) ){

		//		if (is_array($vmav) )
		//		{
		//
		//		} else {
		//			throw new CException("Unknown parameter type for {$viewModelAttributeVals}",707);
		//		}
		////
		//
		//
		//		if (array_keys($viewModelAttributeVals)){
		//			throw new CException("param viewModelAttrVals must be an array",707);
		//		} else {
		//			$vmava = $viewModelAttributeVals; //viewModel attribute value array
		//		}
	}

	/**
	 *
	 * @param string $modelName
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
 	 * @internal A small block of sample code to make a quick debug harness
	 * @internal description
	 * @todo debug eRelatedSave
	 * @version 0.3.0
	 * @package Athelete Resumes
	 * @internal refactored from storeModel to storeModelByMtvfa
	 */
	protected static function storeModelByMtvfa($modelName, array $mtfva, $scmda=null, $runValidation = true) {

		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		// @todo enable logging of updates
		$logDataWrites = true;
		// @todo Build relation name keyed array
		// @todo Use relation names to process the data
		$yiiModels = BaseModel::listYiiModels();
		if (! in_array($modelName, $yiiModels)){
			throw new CException("invalid model name passed in", 707);
		}
		if(array_key_exists($modelName, $mtfva)){
			$fva       = $mtfva[$modelName];
		} else {
			return $scmda;
		}
		$pkName          = $modelName::model()->primaryKey();
		$modelRulesIdx   = $modelName::model()->rules();
		$modelRulesAssoc = [];
		$requiredFields  = [];
		//$validated       =
		//$dupeCheck       = ['org_type_name'];
		//$isDupe = false;
		foreach($modelRulesIdx as $rule){
			$modelRuleType  = ( (isset($rule[1])) ? $rule[1] :'' );
			//$modelRuleEvent = ( (isset($rule['on'])) ? $rule['on'] :'' );
			if ($modelRuleType === 'required'){
				$modelRulesAssoc[$modelRuleType] = $rule[0];
				$requiredFields[$rule[0]] = [
					'valid'  =>false,
					'reason' => ''
				];
				break;
			}
		}
		$pkVal  = 0;
		$crudOp = '';
		// if(isset($fva['org_type_id']) && (int)$fva['org_type_id']>0){
		// Validate the pkValue passed in
		if(isset($fva[$pkName]) && (int)$fva[$pkName] > 0){
			$pkVal = $fva[$pkName];
			$model = $modelName::model()->findByPk($pkVal);
			if (! is_null($model)){
				$scmda['pkeys'][$pkName]=$pkVal;
				$scmda['scenario'][$modelName] = 'update';
				$crudOp = 'update';
			} else {
				$scmda['scenario'][$modelName] = 'insert';
				$crudOp = 'insert';
				$insertOk = false;
			}
		} else {
			$scmda['scenario'][$modelName] = 'insert';
			$crudOp   = 'insert';
			$insertOk = false; //
			$model    = new $modelName();
		}

		// Now perform a dupe check. This is mostly for *_type tables.
		// Each _type table should already have a unique index on the *_type_name field
		// Test yii model rules by commenting this block out until testing is completed
		/*
		if ($crudOp === 'insert'){
			if (isset($dupeCheck[0])){

			}
			if (isset($fva[$dupeCheck[0]]) && ! empty($fva[$dupeCheck[0]])){
				$dupeAttrs = [ $dupeCheck[0] => $fva[$dupeCheck[0]] ];
				$dupeModel = $modelName::model()->findByAttributes($dupeAttrs);
				if (! is_null($dupeModel)){
					$insertOk = false;
				}
			} else {
				$insertOk = false;
			}
		}
		*/

		// Assert the number of attribute values passed in
		if (count($fva) == 1 && isset($fva[$pkName])){
			$scmda['warnings'][$modelName][] = "Only pkey attribute ($pkName) passed in. " . __METHOD__ . " exited without writing data.";
			return $scmda;
		}

		/* commented to test required values rules on the models
		if (is_null($model) && $insertOk == true){
			// Ok to insert
			$model = new $modelName();
		} elseif ($crudOp == 'update' && is_null($model)){
			$scmda['errors'][$modelName][] = 'unknown error, model not instantiated';
			return $scmda;
		}
*/
		// @todo determine whether or not to use safeOnly updates
		// bulk set
		//$model->attributes = $fva;
		//$model->setAttributes($fva, $safeOnly=true);
		//$model->setAttributes($fva, $safeOnly=true);

		// @todo comment the next line after testing is complete
		$modelAttributesMetaData  = BaseModel::fetchModelFieldNames($modelName);
		$scmda['keys']            = $model->getAttributes($modelAttributesMetaData['keys']);
		$modelAttrNames = $model->attributeNames();
		$modelRels = $model->relations();
		$relNames  = array_keys($modelRels);
		// Remove non-attributes and relation trees

		//$fvaSansRelations = array_diff_key($fva, array_flip($relNames));
		//$fvaRelatedModels = array_keys($modelAttrMeta['byModelName']);
		//$fvaSansNonAttr   = array_diff_key($fvaSansRelations, array_flip($modelAttr));
		//$fvaRemoved       = array_diff_key($fvaSansNonAttr, $fva);
		$fvaSkipped=[];
//		$attrBfrUpd       = $model->oldValues; // set in AweActiveRecord->afterFind()
//		if (count($fvaRemoved) > 0){
//			$scmda['warnings'][$modelName][] = $fvaRemoved;
//		}
		$attrBfr = $model->getAttributes();
		$scmda['keys']            = $model->getAttributes($modelAttributesMetaData['keys']);
		$modelAttributesSet=[];
		foreach ($fva as $attrName => $attrVal){
			if (in_array($attrName, $modelAttrNames)){
				$wasSet = $model->setAttribute($attrName, $attrVal);

				if ($wasSet == false){
					$scmda['errors'][$modelName]['fieldValidation'] = [$attrName=>$model->getErrors($attrName)];
				} else {
					// Attribute was set
					$modelAttributesSet[$attrName]=$attrVal;
				}
			} else {
				// not in array of attribute names
				// Log the extraneous field
				$fvaSkipped[$attrName] = $attrVal;
			}
		}


		// diff the values

		$attrAft = $model->getAttributes();
		// Use a custom diff function to create the array structure we want on the diff
		$diff         = self::diffAttributesStatic($attrBfr, $attrAft);
		$diffReplaced = array_diff($attrBfr, $attrAft);
		$diffNew      = array_diff($attrAft, $attrBfr);
		// Verify that a data write attempt is justified.
		if (count($diff) == 0){

			$scmda['warnings'][$modelName][] = $modelName .' has no new values to record. Exited '.$crudOp. ' without writting to the database';
			return self::scmdaPack($scmda);

		} elseif (count($scmda['errors']) > 0 ){

			$scmda['warnings'][$modelName][] = $modelName .' Errors were encountered while setting attribute values. Exited '.$crudOp. ' without writting to the database';
			return self::scmdaPack($scmda);
		}
		if ($model->save($runValidation)){
			if ($crudOp == 'insert'){
				$pkVal = $model->getPrimaryKey();
				$modelAttributesSet[$pkName] = $pkVal;
			}
			$scmda['pkeys'][$pkName] = $pkVal;
			//$scmda['success'] = $modelName .' '. $crudOp;
			$scmda['success'][$modelName] = $crudOp;
			// The fva is not a valid source of data saved as it assumes ALL fields
			//   were written to and that is a potentially false assumption

			// @todo - if worthwhile add diffs to the scmda
			//$scmda['modelsTouched'][$modelName] = $values;
			$scmda['modelsTouched'][$modelName] = $modelAttributesSet;
			if (count($diffReplaced) > 0){
				$scmda['modelsTouched'][$modelName]['oldValues'] = $diffReplaced;
			}
			if (count($diffNew) > 0){
				$scmda['modelsTouched'][$modelName]['newValues'] = $diffNew;
			}
			if (count($fvaSkipped) > 0){
				$scmda['warnings'][$modelName][] = ['attributesSkipped' => $fvaSkipped];
			}
			if ($logDataWrites == true){
				$diff = self::diffAttributesStatic($attrBfr, $attrAft);
				if (count($diff) > 0){
					$scmda['modelsTouched'][$modelName]['diff'] = $diff;
				}
			}

		} else {
			$scmda['errors'][$modelName]['Not saved'] = $model->getErrors();
		}
		return self::scmdaPack($scmda);
	}

	/**
	 *
	 * @param string $modelName
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
 	 * @internal A small block of sample code to make a quick debug harness
	 * @internal description
	 * @todo debug eRelatedSave
	 * @version 0.3.0
	 * @todo 0.3.1 Add psuedo code
	 * @todo 0.4.0 Add call to BelongsTo relations handler relationHandlerForBelongsTo()
	 * @todo 0.5.0 Add call to this->componentTableKeyStorage before|during
	 * scenarios of updates or insert. Add logic for parsing the key storage
	 * as refined postRelations data updates eg afterSave() triggers to run.
	 * @todo 0.6.0 Add BelongsTo relations handler relationHandlerForBelongsTo()
     * @internal pulled base code from storeModelByMtvfa() v0.3.0
	 * @internal Development Status = code pre-construction
	 */
	protected static function storeModelByMtvaWithRelations($modelName, array $mtfva, $scmda=null, $runValidation = true) {
		// call process hook
		self::beforeStoreModelByMtva($modelName, $mtfva, $scmda, $runValidation);

		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		// @todo enable logging of updates
		$logDataWrites = true;
		// @todo Build relation name keyed array
		// @todo Use relation names to process the data
		$yiiModels = BaseModel::listYiiModels();
		if (! in_array($modelName, $yiiModels)){
			throw new CException("invalid model name passed in", 707);
		}
		if(array_key_exists($modelName, $mtfva)){
			$fva       = $mtfva[$modelName];
		} else {
			return $scmda;
		}
		Person::model()->getRelationsAsSparseList($eSaveOnly=true);
		Person::model()->getRelationsAsSparseList($eSaveOnly=false);
		$relations = $modelName::getRelationsAsSparseList($runValidation);

		$modelTreesAll = self::relationsTree($treeName='All');
		// for each component model
		// add relations to scmda input | output
		// inject relations into a copy the originally submitted data array?
		$allRelations   = self::getRelationsAsSparseList($eSaveOnly=false);
		$eSaveRelations = self::getRelationsAsSparseList($eSaveOnly=true);



		$pkName          = $modelName::model()->primaryKey();
		$modelRulesIdx   = $modelName::model()->rules();
		$modelRulesAssoc = [];
		$requiredFields  = [];
		//$validated       =
		//$dupeCheck       = ['org_type_name'];
		//$isDupe = false;
		foreach($modelRulesIdx as $rule){
			$modelRuleType  = ( (isset($rule[1])) ? $rule[1] :'' );
			//$modelRuleEvent = ( (isset($rule['on'])) ? $rule['on'] :'' );
			if ($modelRuleType === 'required'){
				$modelRulesAssoc[$modelRuleType] = $rule[0];
				$requiredFields[$rule[0]] = [
					'valid'  =>false,
					'reason' => ''
				];
				break;
			}
		}
		$pkVal  = 0;
		$crudOp = '';
		// if(isset($fva['org_type_id']) && (int)$fva['org_type_id']>0){
		// Validate the pkValue passed in
		if(isset($fva[$pkName]) && (int)$fva[$pkName] > 0){
			$pkVal = $fva[$pkName];
			$model = $modelName::model()->findByPk($pkVal);
			if (! is_null($model)){
				$scmda['pkeys'][$pkName]=$pkVal;
				$scmda['scenario'][$modelName] = 'update';
				$crudOp = 'update';
			} else {
				$scmda['scenario'][$modelName] = 'insert';
				$crudOp = 'insert';
				$insertOk = false;
			}
		} else {
			$scmda['scenario'][$modelName] = 'insert';
			$crudOp   = 'insert';
			$insertOk = false; //
			$model    = new $modelName();
		}

		// Now perform a dupe check. This is mostly for *_type tables.
		// Each _type table should already have a unique index on the *_type_name field
		// Test yii model rules by commenting this block out until testing is completed
		/*
		if ($crudOp === 'insert'){
			if (isset($dupeCheck[0])){

			}
			if (isset($fva[$dupeCheck[0]]) && ! empty($fva[$dupeCheck[0]])){
				$dupeAttrs = [ $dupeCheck[0] => $fva[$dupeCheck[0]] ];
				$dupeModel = $modelName::model()->findByAttributes($dupeAttrs);
				if (! is_null($dupeModel)){
					$insertOk = false;
				}
			} else {
				$insertOk = false;
			}
		}
		*/

		// Assert the number of attribute values passed in
		if (count($fva) == 1 && isset($fva[$pkName])){
			$scmda['warnings'][$modelName][] = "Only pkey attribute ($pkName) passed in. " . __METHOD__ . " exited without writing data.";
			return $scmda;
		}

		/* commented to test required values rules on the models
		if (is_null($model) && $insertOk == true){
			// Ok to insert
			$model = new $modelName();
		} elseif ($crudOp == 'update' && is_null($model)){
			$scmda['errors'][$modelName][] = 'unknown error, model not instantiated';
			return $scmda;
		}
*/
		// @todo determine whether or not to use safeOnly updates
		// bulk set
		//$model->attributes = $fva;
		//$model->setAttributes($fva, $safeOnly=true);
		//$model->setAttributes($fva, $safeOnly=true);

		// @todo comment the next line after testing is complete
		$modelAttributesMetaData  = BaseModel::fetchModelFieldNames($modelName);
		$scmda['keys']            = $model->getAttributes($modelAttributesMetaData['keys']);
		$modelAttrNames = $model->attributeNames();
		$modelRels = $model->relations();
		$relNames  = array_keys($modelRels);
		// Remove non-attributes and relation trees

		//$fvaSansRelations = array_diff_key($fva, array_flip($relNames));
		//$fvaRelatedModels = array_keys($modelAttrMeta['byModelName']);
		//$fvaSansNonAttr   = array_diff_key($fvaSansRelations, array_flip($modelAttr));
		//$fvaRemoved       = array_diff_key($fvaSansNonAttr, $fva);
		$fvaSkipped=[];
//		$attrBfrUpd       = $model->oldValues; // set in AweActiveRecord->afterFind()
//		if (count($fvaRemoved) > 0){
//			$scmda['warnings'][$modelName][] = $fvaRemoved;
//		}
		$attrBfr = $model->getAttributes();
		$scmda['keys']            = $model->getAttributes($modelAttributesMetaData['keys']);
		$modelAttributesSet=[];
		foreach ($fva as $attrName => $attrVal){
			if (in_array($attrName, $modelAttrNames)){
				$wasSet = $model->setAttribute($attrName, $attrVal);

				if ($wasSet == false){
					$scmda['errors'][$modelName]['fieldValidation'] = [$attrName=>$model->getErrors($attrName)];
				} else {
					// Attribute was set
					$modelAttributesSet[$attrName]=$attrVal;
				}
			} else {
				// not in array of attribute names
				// Log the extraneous field
				$fvaSkipped[$attrName] = $attrVal;
			}
		}


		// diff the values

		$attrAft = $model->getAttributes();
		// Use a custom diff function to create the array structure we want on the diff
		$diff         = self::diffAttributesStatic($attrBfr, $attrAft);
		$diffReplaced = array_diff($attrBfr, $attrAft);
		$diffNew      = array_diff($attrAft, $attrBfr);
		// Verify that a data write attempt is justified.
		if (count($diff) == 0){

			$scmda['warnings'][$modelName][] = $modelName .' has no new values to record. Exited '.$crudOp. ' without writting to the database';
			return self::scmdaPack($scmda);

		} elseif (count($scmda['errors']) > 0 ){

			$scmda['warnings'][$modelName][] = $modelName .' Errors were encountered while setting attribute values. Exited '.$crudOp. ' without writting to the database';
			return self::scmdaPack($scmda);
		}
		if ($model->save($runValidation)){
			if ($crudOp == 'insert'){
				$pkVal = $model->getPrimaryKey();
				$modelAttributesSet[$pkName] = $pkVal;
			}
			$scmda['pkeys'][$pkName] = $pkVal;
			//$scmda['success'] = $modelName .' '. $crudOp;
			$scmda['success'][$modelName] = $crudOp;
			// The fva is not a valid source of data saved as it assumes ALL fields
			//   were written to and that is a potentially false assumption

			// @todo - if worthwhile add diffs to the scmda
			//$scmda['modelsTouched'][$modelName] = $values;
			$scmda['modelsTouched'][$modelName] = $modelAttributesSet;
			if (count($diffReplaced) > 0){
				$scmda['modelsTouched'][$modelName]['oldValues'] = $diffReplaced;
			}
			if (count($diffNew) > 0){
				$scmda['modelsTouched'][$modelName]['newValues'] = $diffNew;
			}
			if (count($fvaSkipped) > 0){
				$scmda['warnings'][$modelName][] = ['attributesSkipped' => $fvaSkipped];
			}
			if ($logDataWrites == true){
				$diff = self::diffAttributesStatic($attrBfr, $attrAft);
				if (count($diff) > 0){
					$scmda['modelsTouched'][$modelName]['diff'] = $diff;
				}
			}

		} else {
			$scmda['errors'][$modelName]['Not saved'] = $model->getErrors();
		}
		// call process hook
		self::afterStoreModelByMtva();
		return self::scmdaPack($scmda);
	}

	/**
	 *
	 * @param string $modelName
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
 	 * @internal A small block of sample code to make a quick debug harness
	 * @internal description
	 * @todo debug eRelatedSave
	 * @deprecated since version 0.54.3
	 */
	protected static function storeModel_v0($modelName, array $mtfva, $scmda=null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		// @todo enable logging of updates
		$logDataWrites = true;
		// @todo Build relation name keyed array
		// @todo Use relation names to process the data
		$yiiModels = BaseModel::listYiiModels();
		if (! in_array($modelName, $yiiModels)){
			throw new CException("invalid model name passed in", 707);
		}
		if(array_key_exists($modelName, $mtfva)){
			$fva       = $mtfva[$modelName];
		} else {
			return $scmda;
		}
		$pkName          = $modelName::model()->primaryKey();
		$modelRulesIdx   = $modelName::model()->rules();
		$modelRulesAssoc = [];
		$requiredFields  = [];
		//$validated       =
		//$dupeCheck       = ['org_type_name'];
		//$isDupe = false;
		foreach($modelRulesIdx as $rule){
			$modelRuleType  = ( (isset($rule[1])) ? $rule[1] :'' );
			//$modelRuleEvent = ( (isset($rule['on'])) ? $rule['on'] :'' );
			if ($modelRuleType === 'required'){
				$modelRulesAssoc[$modelRuleType] = $rule[0];
				$requiredFields[$rule[0]] = [
					'valid'  =>false,
					'reason' => ''
				];
				break;
			}
		}
		$pkVal  = 0;
		$crudOp = '';
		// if(isset($fva['org_type_id']) && (int)$fva['org_type_id']>0){
		// Validate the pkValue passed in
		if(isset($fva[$pkName]) && (int)$fva[$pkName] > 0){
			$pkVal = $fva[$pkName];
			$model = $modelName::model()->findByPk($pkVal);
			if (! is_null($model)){
				$scmda['pkeys'][$pkName]=$pkVal;
				$scmda['scenario'][$modelName] = 'update';
				$crudOp = 'update';
			} else {
				$scmda['scenario'][$modelName] = 'insert';
				$crudOp = 'insert';
				$insertOk = false;
			}
		} else {
			$scmda['scenario'][$modelName] = 'insert';
			$crudOp   = 'insert';
			$insertOk = false; //
			$model    = new $modelName();
		}

		// Look for required attributes
		// Test yii model rules by commenting this block out until testing is completed
//		if (is_null($model)){
//			// org_type_name is required for an insert
//			// check for a duplicate or a broken data process
//			//if(isset($fva['org_type_name']) && strlen($fva['org_type_name'])>0){
//			$insertOk = true;
//			foreach ($requiredFields as $requiredField){
//				if(isset($fva[$requiredField])){
//					if (empty($fva[$requiredField])) {
//						$validated[$requiredField] = false;
//						$insertOk = false;
//					}
//				} else {
//					$validated[$requiredField] = false;
//					$insertOk = false;
//				}
//			}
//		}

		// Now perform a dupe check. This is mostly for *_type tables.
		// Each _type table should already have a unique index on the *_type_name field
		// Test yii model rules by commenting this block out until testing is completed
		/*
		if ($crudOp === 'insert'){
			if (isset($dupeCheck[0])){

			}
			if (isset($fva[$dupeCheck[0]]) && ! empty($fva[$dupeCheck[0]])){
				$dupeAttrs = [ $dupeCheck[0] => $fva[$dupeCheck[0]] ];
				$dupeModel = $modelName::model()->findByAttributes($dupeAttrs);
				if (! is_null($dupeModel)){
					$insertOk = false;
				}
			} else {
				$insertOk = false;
			}
		}
		*/

		// Assert the number of attribute values passed in
		if (count($fva) == 1 && isset($fva[$pkName])){
			$scmda['warnings'][$modelName][] = "Only pkey attribute ($pkName) passed in. " . __METHOD__ . " exited without writing data.";
			return $scmda;
		}

		/* commented to test required values rules on the models
		if (is_null($model) && $insertOk == true){
			// Ok to insert
			$model = new $modelName();
		} elseif ($crudOp == 'update' && is_null($model)){
			$scmda['errors'][$modelName][] = 'unknown error, model not instantiated';
			return $scmda;
		}
*/
		// @todo determine whether or not to use safeOnly updates
		// bulk set
		//$model->attributes = $fva;
		//$model->setAttributes($fva, $safeOnly=true);
		//$model->setAttributes($fva, $safeOnly=true);

		// @todo comment the next line after testing is complete
		$modelAttrMeta  = BaseModel::fetchModelFieldNames($modelName);
		$modelAttrNames = $model->attributeNames();
		$modelRels = $model->relations();
		$relNames  = array_keys($modelRels);
		// Remove non-attributes and relation trees
		$fvaCopied = $fva; // remove after unit testing
		//$fvaSansRelations = array_diff_key($fva, array_flip($relNames));
		//$fvaRelatedModels = array_keys($modelAttrMeta['byModelName']);
		//$fvaSansNonAttr   = array_diff_key($fvaSansRelations, array_flip($modelAttr));
		//$fvaRemoved       = array_diff_key($fvaSansNonAttr, $fva);
		$fvaSkipped=[];
//		$attrBfrUpd       = $model->oldValues; // set in AweActiveRecord->afterFind()
//		if (count($fvaRemoved) > 0){
//			$scmda['warnings'][$modelName][] = $fvaRemoved;
//		}
		$modelAttributesSet=[];
		$attrBfr = $model->getAttributes();
		foreach ($fva as $attrName => $attrVal){
			if (in_array($attrName, $modelAttrNames)){
				$wasSet = $model->setAttribute($attrName, $attrVal);

				if ($wasSet == false){
					$scmda['errors'][$modelName][] = [$attrName=>$model->getErrors($attrName)];
				} else {
					// Attribute was set
					$modelAttributesSet[$attrName]=$attrVal;
				}
			} else {
				// not in array of attribute names
				// Log the extraneous field
				$fvaSkipped[$attrName] = $attrVal;
			}
		}

		// diff the values
		//$attrBfr = $model->oldValues;
		$attrAft = $model->getAttributes();
		$diffReplaced = array_diff($attrBfr, $attrAft);
		$diffNew      = array_diff($attrAft, $attrBfr);
		if ($model->save($runValidation)){
			if ($crudOp == 'insert'){
				$pkVal = $model->getPrimaryKey();
				$modelAttributesSet[$pkName] = $pkVal;
			}
			$scmda['pkeys'][$pkName] = $pkVal;
			// The fva is not a valid source of data saved as it assumes ALL fields
			//   were written to and that is a potentially false assumption

			// @todo - if worthwhile add diffs to the scmda
			//$scmda['modelsTouched'][$modelName] = $values;
			$scmda['modelsTouched'][$modelName] = $modelAttributesSet;
			$scmda['modelsTouched'][$modelName]['oldValues'] = $diffReplaced;
			$scmda['modelsTouched'][$modelName]['newValues'] = $diffNew;
			if (count($fvaSkipped) > 0){
				$scmda['warnings'][$modelName][] = ['attributesSkipped' => $fvaSkipped];
			}
			if ($logDataWrites == true){
				$diff = self::diffAttributesStatic($attrBfr, $attrAft);
				$scmda['modelsTouched'][$modelName]['diff'] = $diff;
			}

		} else {
			$scmda['errors'][$modelName][] = $model->getErrors();
		}
		return $scmda;
	}

	/**
	 *
	 * @param type $oldValues
	 * @param type $newValues
	 * @return array $diff
	 */
	protected static function diffAttributesStatic($oldValues, $newValues){
		$diff=[];
		foreach ($newValues as $attrName => $attrValue){
			if ($attrValue !== $oldValues[$attrName]){
				$diff[$attrName] = ['old'=>$oldValues[$attrName], 'new'=>$newValues[$attrName]];
			}
		}
		return $diff;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
 	 * @internal A small block of sample code to make a quick debug harness
	 * @internal description
	 * @see self::saveYiiNormativeModelValuesArray() where this code was pulled from
	 */
	protected static function storeModelOrgType(array $mtfva, $scmda=null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		// @todo Build relation name keyed array
		// @todo Use relation names to process the data
		if(array_key_exists('OrgType', $mtfva)){
			$fva       = $mtfva['OrgType'];
			$modelName = 'OrgType';
		} else {
			return $scmda;
		}
		$pkName     = $modelName::model()->primaryKey();
		$modelRulesIdx   = $modelName::model()->rules();
		$modelRulesAssoc = [];
		$requiredFields  = [];
		//$requiredFields  = ['org_type_name'];
		//$validated       =
		$dupeCheck       = ['org_type_name'];
		//$isDupe = false;
		$attributes = BaseModel::fetchModelFieldNames($modelName);
		foreach($modelRulesIdx as $rule){
			$modelRuleType  = ( (isset($rule[1])) ? $rule[1] :'' );
			//$modelRuleEvent = ( (isset($rule['on'])) ? $rule['on'] :'' );
			if ($modelRuleType === 'required'){
				$modelRulesAssoc[$modelRuleType] = $rule[0];
				$requiredFields[$rule[0]] = [
					'valid'  =>false,
					'reason' => ''
				];
				break;
			}
		}
		$pkVal  = 0;
		$crudOp = '';
		// if(isset($fva['org_type_id']) && (int)$fva['org_type_id']>0){
		if(isset($fva[$pkName]) && (int)$fva[$pkName]>0){
			// OrgType updates are not allowed through the athlete teams model at this time
			// $OrgType = OrgType::model()->findByPk($OrgTypeValues['org_type_id']);\
			$pkVal = $fva[$pkName];
			$model = $modelName::model()->findByPk($pkVal);
			if (! is_null($model)){
				$scmda['pkeys'][$pkName]=$pkVal;
				$scmda['scenario'][$modelName] = 'update';
				$crudOp = 'update';
			} else {
				$scmda['scenario'][$modelName] = 'insert';
				$crudOp = 'insert';
				$insertOk = false;
			}
		}

		// Look for required attributes
		if (is_null($model)){
			// org_type_name is required for an insert
			// check for a duplicate or a broken data process
			//if(isset($fva['org_type_name']) && strlen($fva['org_type_name'])>0){
			$insertOk = true;
			foreach ($requiredFields as $requiredField){
				if(isset($fva[$requiredField])){
					if (empty($fva[$requiredField])) {
						$validated[$requiredField] = false;
						$insertOk = false;
					}
				} else {
					$validated[$requiredField] = false;
					$insertOk = false;
				}
			}
		}

		// Now perform a dupe check
		if ($crudOp === 'insert'){
			if (isset($dupeCheck[0])){

			}
			if (isset($fva[$dupeCheck[0]]) && ! empty($fva[$dupeCheck[0]])){
				$dupeAttrs = [ $dupeCheck[0] => $fva[$dupeCheck[0]] ];
				$dupeModel = $modelName::model()->findByAttributes($dupeAttrs);
				if (! is_null($dupeModel)){
					$insertOk = false;
				}
			} else {
				$insertOk = false;
			}
		}

		// @todo ?check for attr count sans?
		if (count($fva) == 1 && isset($fva[$pkName])){
			$scmda['warnings'][$modelName] = "Only pkey attribute ($pkName) passed in";
			return $scmda;
		}

		If (is_null($model) && $insertOk == true){
			// Ok to insert
			$model = new $modelName();
		} elseif ($crudOp == 'update' && isNull($model)){
			$scmda['errors'][$modelName] = 'unknown error, model not instantiated';
			return $scmda;
		}

		// bulk set
		$model->attributes = $fva;
		//$model->setAttributes($fva, $safeOnly=true);
		//$model->setAttributes($fva, $safeOnly=true);

		foreach ($fva as $attrName => $attrVal){
			$model->setAttribute($attrName, $attrVal);
		}

		// diff the values
		$attrBfr = $model->getAttributesOld();

		if ($model->save($runValidation)){
			$pkVal = $model->getPrimaryKey();
			$scmda['pkeys'][$pkName] = $pkVal;
			$values = $fva;
			$values[$pkName] = $pkVal;
			$scmda['modelsTouched'][$modelName] = $values;
		} else {
			$scmda['errors'][$modelName] = $model->getErrors();
		}
		return $scmda;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
	 */
	public static function storeModelOrgLevel(array $mtfva, $scmda=null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}

		if(array_key_exists('OrgLevel', $mtfva)){
			$OrgLevelValues = $mtfva['OrgLevel'];
			if(isset($OrgLevelValues['org_level_id']) && (int)$OrgLevelValues['org_level_id']>0){
				// OrgLevel updates are not allowed through the athlete teams model at this time
				// $OrgLevel = OrgLevel::model()->findByPk($OrgLevelValues['org_type_id']);
				$scmda['pkeys']['org_level_id']=$OrgLevelValues['org_level_id'];
			}elseif(isset($OrgLevelValues['org_type_name']) && strlen($OrgLevelValues['org_type_name'])>0){
				// org_level_name is required for an insert
				// check for a duplicate or a broken data process
				// @todo must have an org type id as well
				$orgLevelAttr = ['org_level_name'=>$OrgLevelValues['org_level_name']];
				$OrgLevel = OrgLevel::model()->findByAttributes($orgLevelAttr);
				if (is_null($OrgLevel)){
					// Ok to insert
					$OrgLevel = new OrgLevel();
					$OrgLevel->attributes = $OrgLevelValues;
					if ($OrgLevel->save($runValidation)){
						$pkVal = $OrgLevel->getPrimaryKey();
						$scmda['pkeys']['org_level_id']=$pkVal;
						$values = $OrgLevelValues;
						$values['org_level_id'] = $pkVal;
						$scmda['modelsTouched']['OrgLevel'] = $values;
						$scmda['scenario']['OrgLevel'] = 'insert';
						unset($pkVal, $values);
					} else {
						$scmda['errors']['OrgLevel'] = $OrgLevel->getErrors();
					}
				} else {
					// Org Level duplicates are disallowed at this time
				}
			}
		} // end of OrgLevel processing
		return $scmda;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
	 */
	public static function storeModelOrg(array $mtfva, $scmda = null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		if(array_key_exists('Org', $mtfva)){
			$OrgValues = $mtfva['Org'];
		} else {
			return $scmda;
		}

		if(isset($OrgValues['org_id']) && (int)$OrgValues['org_id']>0){
			// Org updates are allowed through the athlete teams model at this time
			$Org = Org::model()->findByPk($OrgValues['org_id']);
			$Org->attributes = $OrgValues;
			if ($Org->save($runValidation)){
				$pkVal = $Org->getPrimaryKey();
				$scmda['pkeys']['org_id']=$pkVal;
				$values = $OrgValues;
				$values['org_id'] = $pkVal;
				$scmda['modelsTouched']['Org'] = $values;
				$scmda['scenario']['Org'] = 'update';
				unset($pkVal, $values);
			} else {
				$scmda['errors']['Org'] = $Org->getErrors();
			}

		}elseif(isset($OrgValues['org_name']) && strlen($OrgValues['org_name'])>0){
			// org_name is required for an insert
			if(isset($OrgValues['org_type_id']) && (int)$OrgValues['org_type_id']>0){
				$orgOkToInsert = true;
			} else {
				$orgOkToInsert = false;
			}
			// check for a duplicate or a broken data process
			// @todo must have an org type id as well
			$orgAttr = ['org_name'=>$OrgValues['org_name']];
			$Org = Org::model()->findByAttributes($orgAttr);
			if (is_null($Org) && $orgOkToInsert){
				// Ok to insert
				$Org = new Org();
				$Org->attributes = $OrgValues;
				if ($Org->save($runValidation)){
					$pkVal = $Org->getPrimaryKey();
					$scmda['pkeys']=['org_id'=>$pkVal];
					$values = $OrgValues;
					$values['org_id'] = $pkVal;
					$scmda['modelsTouched']['Org'] = $values;
					$scmda['scenario']['Org'] = 'insert';
					unset($pkVal, $values);
				} else {
					$scmda['errors']['Org'] = $Org->getErrors();
				}
			} else {
				// Org Level duplicates are disallowed at this time
			}
		}
		 // end of Org processing
		return $scmda;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
	 */
	public static function storeModelPerson(array $mtfva, $scmda =null, $runValidation =true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}

		if(array_key_exists('Person', $mtfva)){
			$PersonValues = $mtfva['Person'];
		} else {
			return $scmda;
		}

		if(isset($PersonValues['person_id']) && (int)$PersonValues['person_id']>0){
			// Person updates are allowed through the athlete teams model at this time
			$Person = Person::model()->findByPk($PersonValues['person_id']);
			$Person->attributes = $PersonValues;
			if ($Person->save($runValidation)){
				$pkVal = $Person->getPrimaryKey();
				$scmda['pkeys']=['person_id'=>$pkVal];
				$values = $PersonValues;
				$values['person_id'] = $pkVal;
				$scmda['modelsTouched']['Person'] = $values;
				$scmda['scenario']['Person'] = 'update';
				unset($pkVal, $values);
			} else {
				$scmda['errors']['Person'] = $Person->getErrors();
			}

		}elseif(isset($PersonValues['person_name']) && strlen($PersonValues['person_name'])>0){
			// person_name is required for an insert
			if(isset($PersonValues['person_type_id']) && (int)$PersonValues['person_type_id']>0){
				$personOkToInsert = true;
			} else {
				$personOkToInsert = false;
			}
			// check for a duplicate or a broken data process
			// @todo must have an person type id as well
			$personAttr = ['person_name'=>$PersonValues['person_name']];
			$Person = Person::model()->findByAttributes($personAttr);
			if (is_null($Person) && $personOkToInsert){
				// Ok to insert
				$Person = new Person();
				$Person->attributes = $PersonValues;
				if ($Person->save($runValidation)){
					$pkVal = $Person->getPrimaryKey();
					$scmda['pkeys']=['person_id'=>$pkVal];
					$values = $PersonValues;
					$values['person_id'] = $pkVal;
					$scmda['modelsTouched']['Person'] = $values;
					$scmda['scenario']['Person'] = 'insert';
					unset($pkVal, $values);
				} else {
					$scmda['errors']['Person'] = $Person->getErrors();
				}
			} else {
				// Person Level duplicates are disallowed at this time
			}
		}
		 // end of Person processing
		return $scmda;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
	 */
	public static function storeModelPlayer(array $mtfva, $scmda = null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}

		// @todo code review the player code block
		if(array_key_exists('Player', $mtfva)){
			$PlayerValues = $mtfva['Player'];
		} else {
			return $scmda;
		}
		if(isset($PlayerValues['player_id']) && (int)$PlayerValues['player_id']>0){
			// Player updates are allowed through the athlete teams model at this time
			$Player = Player::model()->findByPk($PlayerValues['player_id']);
			$Player->attributes = $PlayerValues;
			if ($Player->save($runValidation)){
				$pkVal = $Player->getPrimaryKey();
				$scmda['pkeys']=['player_id'=>$pkVal];
				$values = $PlayerValues;
				$values['player_id'] = $pkVal;
				$scmda['modelsTouched']['Player'] = $values;
				$scmda['scenario']['Player'] = 'update';
				unset($pkVal, $values);
			} else {
				$scmda['errors']['Player'] = $Player->getErrors();
			}

		}elseif(isset($PlayerValues['player_name']) && strlen($PlayerValues['player_name'])>0){
			// player_name is required for an insert
			if(isset($PlayerValues['player_type_id']) && (int)$PlayerValues['player_type_id']>0){
				$playerOkToInsert = true;
			} else {
				$playerOkToInsert = false;
			}
			// check for a duplicate or a broken data process
			// @todo must have an player type id as well
			$playerAttr = ['player_name'=>$PlayerValues['player_name']];
			$Player = Player::model()->findByAttributes($playerAttr);
			if (is_null($Player) && $playerOkToInsert){
				// Ok to insert
				$Player = new Player();
				$Player->attributes = $PlayerValues;
				if ($Player->save($runValidation)){
					$pkVal = $Player->getPrimaryKey();
					$scmda['pkeys']=['player_id'=>$pkVal];
					$values = $PlayerValues;
					$values['player_id'] = $pkVal;
					$scmda['modelsTouched']['Player'] = $values;
					$scmda['scenario']['Player'] = 'insert';
					unset($pkVal, $values);
				} else {
					$scmda['errors']['Player'] = $Player->getErrors();
				}
			} else {
				// Player Level duplicates are disallowed at this time
			}
		}
		 // end of Player processing
		return $scmda;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
	 */
	public static function storeModelTeamDivison(array $mtfva, $scmda = null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		// @todo code review the team divsion code block
		if(array_key_exists('TeamDivision', $mtfva)){
			$TeamDivisionValues = $mtfva['TeamDivision'];
			if(isset($TeamDivisionValues['team_division_id']) && (int)$TeamDivisionValues['team_division_id']>0){
				// TeamDivision updates are not allowed through the athlete teams model at this time
				// $TeamDivision = TeamDivision::model()->findByPk($TeamDivisionValues['team_division_id']);
				$scmda['pkeys']['team_division_id']=$TeamDivisionValues['team_division_id'];
			}elseif(isset($TeamDivisionValues['team_division_name']) && strlen($TeamDivisionValues['team_division_name'])>0){
				// team_division_name is required for an insert
				// check for a duplicate or a broken data process
				$TeamDivisionAttr = ['team_division_name'=>$TeamDivisionValues['team_division_name']];
				$TeamDivision = TeamDivision::model()->findByAttributes($TeamDivisionAttr);
				if (is_null($TeamDivision)){
					// Ok to insert
					$TeamDivision = new TeamDivision();
					$TeamDivision->attributes = $TeamDivisionValues;
					if ($TeamDivision->save($runValidation)){
						$pkVal = $TeamDivision->getPrimaryKey();
						$scmda['pkeys']['team_division_id']=$pkVal;
						$values = $TeamDivisionValues;
						$values['team_division_id'] = $pkVal;
						$scmda['modelsTouched']['TeamDivision'] = $values;
						$scmda['scenario']['TeamDivision'] = 'insert';
						unset($pkVal, $values);
					} else {
						$scmda['errors']['TeamDivision'] = $TeamDivision->getErrors();
					}

				} else {
					// Org Type duplicates are disallowed at this time
				}
			}
		} // end of TeamDivision processing
		return $scmda;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
	 */
	public static function storeModelTeamLeague(array $mtfva, $scmda = null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}

		// @todo code review the team league code block
		if(array_key_exists('TeamLeague', $mtfva)){
			$TeamLeagueValues = $mtfva['TeamLeague'];
			if(isset($TeamLeagueValues['team_league_id']) && (int)$TeamLeagueValues['team_league_id']>0){
				// TeamLeague updates are not allowed through the athlete teams model at this time
				// $TeamLeague = TeamLeague::model()->findByPk($TeamLeagueValues['team_league_id']);
				$scmda['pkeys']['team_league_id']=$TeamLeagueValues['team_league_id'];
			}elseif(isset($TeamLeagueValues['team_league_name']) && strlen($TeamLeagueValues['team_league_name'])>0){
				// team_league_name is required for an insert
				// check for a duplicate or a broken data process
				$TeamLeagueAttr = ['team_league_name'=>$TeamLeagueValues['team_league_name']];
				$TeamLeague = TeamLeague::model()->findByAttributes($TeamLeagueAttr);
				if (is_null($TeamLeague)){
					// Ok to insert
					$TeamLeague = new TeamLeague();
					$TeamLeague->attributes = $TeamLeagueValues;
					if ($TeamLeague->save($runValidation)){
						$pkVal = $TeamLeague->getPrimaryKey();
						$scmda['pkeys']['team_league_id']=$pkVal;
						$values = $TeamLeagueValues;
						$values['team_league_id'] = $pkVal;
						$scmda['modelsTouched']['TeamLeague'] = $values;
						$scmda['scenario']['TeamLeague'] = 'insert';
						unset($pkVal, $values);
					} else {
						$scmda['errors']['TeamLeague'] = $TeamLeague->getErrors();
					}

				} else {
					// Org Type duplicates are disallowed at this time
				}
			}
		} // end of TeamLeague processing
		return $scmda;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
	 */
	public static function storeModelTeam(array $mtfva, $scmda = null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		// @todo code review the team code block
		if(array_key_exists('Team', $mtfva)){
			$TeamValues = $mtfva['Team'];
			if(isset($TeamValues['team_id']) && (int)$TeamValues['team_id']>0){
				// Team updates are allowed through the athlete teams model at this time
				$Team = Team::model()->findByPk($TeamValues['team_id']);
				$Team->attributes = $TeamValues;
				if ($Team->save($runValidation)){
					$pkVal = $Team->getPrimaryKey();
					$scmda['pkeys']['team_id']=$pkVal;
					$values = $TeamValues;
					$values['team_id'] = $pkVal;
					$scmda['modelsTouched']['Team'] = $values;
					$scmda['scenario']['Team'] = 'update';
					unset($pkVal, $values);
				} else {
					$scmda['errors']['Team'] = $Team->getErrors();
				}

			}elseif(isset($TeamValues['team_name']) && strlen($TeamValues['team_name'])>0){
				// team_name is required for an insert
				if(isset($TeamValues['team_type_id']) && (int)$TeamValues['team_type_id']>0){
					$teamOkToInsert = true;
				} else {
					$teamOkToInsert = false;
				}
				// check for a duplicate or a broken data process
				// @todo must have an team type id as well
				$TeamAttr = ['team_name'=>$TeamValues['team_name']];
				$Team = Team::model()->findByAttributes($TeamAttr);
				if (is_null($Team) && $teamOkToInsert){
					// Ok to insert
					$Team = new Team();
					$Team->attributes = $TeamValues;
					if ($Team->save($runValidation)){
						$pkVal = $Team->getPrimaryKey();
						$scmda['pkeys']=['team_id'=>$pkVal];
						$values = $TeamValues;
						$values['team_id'] = $pkVal;
						$scmda['modelsTouched']['Team'] = $values;
						$scmda['scenario']['Team'] = 'insert';
						unset($pkVal, $values);
					} else {
						$scmda['errors']['Team'] = $Team->getErrors();
					}
				} else {
					// Team Level duplicates are disallowed at this time
				}
			}
		} // end of Team processing
		return $scmda;
	}

	public static function storeModelTeamOrg(array $mtfva, $scmda = null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		// @todo code review the team org code block
		if(array_key_exists('TeamOrg', $mtfva)){
			$TeamOrgValues = $mtfva['TeamOrg'];
			if(isset($TeamOrgValues['org_id']) && (int)$TeamOrgValues['org_id']>0){
				// TeamOrg updates are allowed through the athlete teams model at this time
				$TeamOrg = TeamOrg::model()->findByPk($TeamOrgValues['org_id']);
				$TeamOrg->attributes = $TeamOrgValues;
				if ($TeamOrg->save($runValidation)){
					$pkVal = $TeamOrg->getPrimaryKey();
					$scmda['pkeys']['org_id']=$pkVal;
					$values = $TeamOrgValues;
					$values['org_id'] = $pkVal;
					$scmda['modelsTouched']['TeamOrg'] = $values;
					$scmda['scenario']['TeamOrg'] = 'update';
					unset($pkVal, $values);
				} else {
					$scmda['errors']['TeamOrg'] = $TeamOrg->getErrors();
				}

			}elseif(isset($TeamOrgValues['org_name']) && strlen($TeamOrgValues['org_name'])>0){
				// org_name is required for an insert
				if(isset($TeamOrgValues['org_type_id']) && (int)$TeamOrgValues['org_type_id']>0){
					$orgOkToInsert = true;
				} else {
					$orgOkToInsert = false;
				}
				// check for a duplicate or a broken data process
				// @todo must have an org type id as well
				$TeamOrgAttr = ['org_name'=>$TeamOrgValues['org_name']];
				$TeamOrg = TeamOrg::model()->findByAttributes($TeamOrgAttr);
				if (is_null($TeamOrg) && $orgOkToInsert){
					// Ok to insert
					$TeamOrg = new TeamOrg();
					$TeamOrg->attributes = $TeamOrgValues;
					if ($TeamOrg->save($runValidation)){
						$pkVal = $TeamOrg->getPrimaryKey();
						$scmda['pkeys']=['org_id'=>$pkVal];
						$values = $TeamOrgValues;
						$values['org_id'] = $pkVal;
						$scmda['modelsTouched']['TeamOrg'] = $values;
						$scmda['scenario']['TeamOrg'] = 'insert';
						unset($pkVal, $values);
					} else {
						$scmda['errors']['TeamOrg'] = $TeamOrg->getErrors();
					}
				} else {
					// TeamOrg Level duplicates are disallowed at this time
				}
			}
		} // end of TeamOrg processing
		return $scmda;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
	 */
	public static function storeModelTeamPlayer(array $mtfva, $scmda = null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		// @todo code review the team league code block
		if(array_key_exists('TeamPlayer', $mtfva)){
			$TeamPlayerValues = $mtfva['TeamPlayer'];
			if(isset($TeamPlayerValues['team_player_id']) && (int)$TeamPlayerValues['team_player_id']>0){
				// TeamPlayer updates are not allowed through the athlete teams model at this time
				// $TeamPlayer = TeamPlayer::model()->findByPk($TeamPlayerValues['team_player_id']);
				$scmda['pkeys']['team_player_id']=$TeamPlayerValues['team_player_id'];
			}elseif(isset($TeamPlayerValues['team_player_name']) && strlen($TeamPlayerValues['team_player_name'])>0){
				// team_player_name is required for an insert
				// check for a duplicate or a broken data process
				$TeamPlayerAttr = ['team_player_name'=>$TeamPlayerValues['team_player_name']];
				$TeamPlayer = TeamPlayer::model()->findByAttributes($TeamPlayerAttr);
				if (is_null($TeamPlayer)){
					// Ok to insert
					$TeamPlayer = new TeamPlayer();
					$TeamPlayer->attributes = $TeamPlayerValues;
					if ($TeamPlayer->save($runValidation)){
						$pkVal = $TeamPlayer->getPrimaryKey();
						$scmda['pkeys']['team_player_id']=$pkVal;
						$values = $TeamPlayerValues;
						$values['team_player_id'] = $pkVal;
						$scmda['modelsTouched']['TeamPlayer'] = $values;
						$scmda['scenario']['TeamPlayer'] = 'insert';
						unset($pkVal, $values);
					} else {
						$scmda['errors']['TeamPlayer'] = $TeamPlayer->getErrors();
					}

				} else {
					// Org Type duplicates are disallowed at this time
				}
			}
		} // end of TeamPlayer processing
		return $scmda;
	}

	/**
	 *
	 * @param array $mtfva
	 * @param array $scmda
	 * @param bool $runValidation
	 * @return array $scmda
	 */
	public static function storeModelTeamCoach(array $mtfva, $scmda = null, $runValidation = true) {
		if (is_null($scmda)){
			trace('called without scmda', __METHOD__);
			$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();
		}
		// @todo code review the team league code block
		if(array_key_exists('TeamCoach', $mtfva)){
			$TeamPlayerValues = $mtfva['TeamPlayer'];
			if(isset($TeamPlayerValues['team_player_id']) && (int)$TeamPlayerValues['team_player_id']>0){
				// TeamPlayer updates are not allowed through the athlete teams model at this time
				// $TeamPlayer = TeamPlayer::model()->findByPk($TeamPlayerValues['team_player_id']);
				$scmda['pkeys']['team_player_id']=$TeamPlayerValues['team_player_id'];
			}elseif(isset($TeamPlayerValues['team_player_name']) && strlen($TeamPlayerValues['team_player_name'])>0){
				// team_player_name is required for an insert
				// check for a duplicate or a broken data process
				$TeamPlayerAttr = ['team_player_name'=>$TeamPlayerValues['team_player_name']];
				$TeamPlayer = TeamCoach::model()->findByAttributes($TeamPlayerAttr);
				if (is_null($TeamPlayer)){
					// Ok to insert
					$TeamPlayer = new TeamCoach();
					$TeamPlayer->attributes = $TeamPlayerValues;
					if ($TeamPlayer->save($runValidation)){
						$pkVal = $TeamPlayer->getPrimaryKey();
						$scmda['pkeys']['team_player_id']=$pkVal;
						$values = $TeamPlayerValues;
						$values['team_player_id'] = $pkVal;
						$scmda['modelsTouched']['TeamPlayer'] = $values;
						$scmda['scenario']['TeamPlayer'] = 'insert';
						unset($pkVal, $values);
					} else {
						$scmda['errors']['TeamPlayer'] = $TeamPlayer->getErrors();
					}

				} else {
					// Org Type duplicates are disallowed at this time
				}
			}
		} // end of TeamPlayer processing
		return $scmda;
	}

	public static function storeYiiNormativeModelValuesArray($viewFieldValueArray, $sparse=true) {

	}

	/**
	 *
	 * @param type $filePrefix Used when calling self::archiveData()
	 * @internal Development Status = code construction
	 */
	protected static function callStack($filePrefix=null) {
		$bt = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, $limit=5);
		self::archiveData($bt, $filePrefix);

		if (isset($bt[1]['class'])){
			$class = $bt[1]['class'];
		} else {
			$class = '';
		}

		if (isset($bt[1]['function'])){
			$function = $bt[1]['function'];
		} else {
			$function = '';
		}
		//trace($class . '' __METHOD__ . ' was called by ' . $calledBy);
	}

	/**
	 *
	 * @param array $yava
	 * @param bool $sparse True only
	 * @return boolean
	 * @internal Development Status = ready for testing (rft
	 * @internal consider two data engines as an elective option within the code
	 *   eg context sensitive awareness with simple fetches of keys
	 * @see BaseModel::StoreWithERelated_TestHarness
	 * @see BaseModel::StoreWithERelated($data)
	 * @see BaseModel::walkRelationsStack()
	 * @see BaseModel::walkRelations()
	 * @see BaseModel::storeMtfvaAsAweActiveRecords()
	 * @uses self::storeMtvaRelations() Set relations as needed.
	 * @todo build composite scmda
	 */
	//public static function storeMtva($mtfva, $yava=null, $sparse=true) {
	public static function storeMtva($mtfva, $runValidation=true) {
		// call process hook
		self::beforeStoreMtva();

		$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();

//
//		// @todo add function: modelName to relationName
//		// @todo add function: mtfvaXlatBaseModelsByRelationName
//		// @todo add function: transaction
//		// @todo turn off local debug for production
//
//		$localDebug = true;
//
	    $result = true; $t = false;
//		if (!Yii::app()->db->currentTransaction) { // only start transaction if none is running already
//		    $t = Yii::app()->db->beginTransaction();
//		}
//		// @todo Build relation name keyed array
//		// @todo Use relation names to process the data

		$modelTreesAll = self::relationsTree($treeName='All');

		if (isset($mtfva['TeamOrg']['org_type_id'])){
			$teamOrgOrgTypeID = isset($mtfva['TeamOrg']['org_type_id']);
		}
		if (isset($mtfva['TeamOrg']['org_id'])){
			$teamOrgOrgID = isset($mtfva['TeamOrg']['org_id']);
		}else {
			$teamOrgOrgID = 0;
		}

		if ( (int)$teamOrgOrgID == 0){
			$scmdaTeamCreate = self::storeMtvaTeamCreate($mtfva, $runValidation);
			return $scmdaTeamCreate;
		}
		$out = [];
		foreach ($modelTreesAll as $modelTreeName=>$modelsTree){
			foreach ($modelsTree as $modelName=>$modelSettings){
				if (isset($modelSettings['isReadOnly']) && $modelSettings['isReadOnly'] == true){
					continue;
				}
				// CEC v0.1.0 call
				// $out[$modelTreeName][$modelName] =
				//			self::storeModelByMtva($modelName, $mtfva, $scmda, $runValidation);
				// CEC v0.2.0 call
				// @todo Disable next line after testing CEC v01 versus CEC v02 comparison testing
				$storeModelResult_v01 = self::storeModelByMtva($modelName, $mtfva, $scmda, $runValidation);
				$storeModelResult     = self::storeModelByMtvaWithRelations($modelName, $mtfva, $scmda, $runValidation); // CEC v0.2.0

				$out[$modelTreeName][$modelName] = $storeModelResult;

				// Remove empty key:value pairs from the output array
				// handled by storeModel() return;
			}
			//$flattenedScmda = self::storeMtvaCompositeScmda($modelTreeName, $out);
			// PHP best practice after any foreach
			unset($modelName, $modelSettings);
		}

		// PHP best practice after any foreach
		unset($modelTreeName, $modelsTree);

//		$localDebug = true;
//		$useRawScmda = false;
//		if($localDebug == true){
//			// raw multi-model scmda
//			if ($useRawScmda){
//				foreach ($out as $modelTreeName => $scmdaOut) {
//					echo "<h1>$modelTreeName</h1>";
//					YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($scmdaOut, 10, true);
//				}
//				unset ($modelTreeName, $scmdaOut);
//			}
//		}

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($out, 10, true);
		//return $out;
		$parsedScmda = self::storeMtvaCompositeScmdaWrapper($out);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($parsedScmda, 10, true);

		// @todo call Relations module

		// call process hook
		self::afterStoreMtva();


		return $parsedScmda;

		// <editor-fold defaultstate="collapsed" desc="hide single method update calls">

		$scmdaOrgLevel = self::storeModelOrgLevel($mtfva, $scmda, $runValidation);
		array_merge($scmda, $scmdaOrgLevel);

		$scmdaOrg = self::storeModelOrg($mtfva, $scmda, $runValidation);

		array_merge($scmda, $scmdaOrg);

		$scmdaPerson = self::storeModelPerson($mtfva, $scmda, $runValidation);

		array_merge($scmda, $scmdaPerson);

		$scmdaPlayer = self::storeModelPlayer($mtfva, $scmda, $runValidation);

		array_merge($scmda, $scmdaPlayer);

		$scmdaTeamDivision = self::storeModelTeamDivision($mtfva, $scmda, $runValidation);
		array_merge($scmda, $scmdaTeamDivision);

		$scmda = self::storeModelTeamLeague($mtfva, $scmda, $runValidation);

		$scmda = self::storeModelTeam($mtfva, $scmda, $runValidation);

		$scmda = self::storeModelTeamOrg($mtfva, $scmda, $runValidation);

		$scmda = self::storeModelTeamPlayer($mtfva, $scmda, $runValidation);

		$scmda = self::storeModelTeamCoach($mtfva, $scmda, $runValidation);
		// </editor-fold>

        if ($t && $result) {
            $t->commit(); // commit on success if transaction was started in this behavior
        }
		if ($t && !$result) {
		    $t->rollback(); // rollback on errors if transaction was started in this behavior
		}
		return $result;
	}

	/**
	 *
	 * @param array $yava
	 * @param bool $sparse True only
	 * @return boolean
	 * @internal Development Status = ready for testing (rft)
	 * @internal consider two data engines as an elective option within the code
	 *   eg context sensitive awareness with simple fetches of keys
	 * @see BaseModel::StoreWithERelated_TestHarness
	 * @see BaseModel::StoreWithERelated($data)
	 * @see BaseModel::walkRelationsStack()
	 * @see BaseModel::walkRelations()
	 * @see BaseModel::storeMtfvaAsAweActiveRecords()
	 * @uses self::storeMtvaRelations() Set relations as needed.
	 * @todo build composite scmda
	 * 	teamOrg create
	 * 	if school write school
	 *
	 * 	if coach info
	 * 	  PersonCoach create3?
	 * 	  TeamCoach create
	 *
	 * 	create teamPlayer
	 * 	   player_id
	 * 	   coach_id
	 * 	   update player with last team_player_id*
	 */
	//public static function storeMtva($mtfva, $yava=null, $sparse=true) {
	public static function storeMtvaTeamCreate($mtfva, $runValidation=true) {
		// call process hook
		self::beforeStoreMtva();

		$mmava = $mtfva;
		$scmda = BaseModel::getStoreChainMetaData_ArrayTemplate();

		$treeName = 'storeMtvaTeamCreate';
//		// @todo add function: modelName to relationName
//		// @todo add function: mtfvaXlatBaseModelsByRelationName
//		// @todo add function: transaction
//		// @todo turn off local debug for production
//
//		$localDebug = true;
//
	    $result = true; $t = false;
//		if (!Yii::app()->db->currentTransaction) { // only start transaction if none is running already
//		    $t = Yii::app()->db->beginTransaction();
//		}
//		// @todo Build relation name keyed array
//		// @todo Use relation names to process the data

		$modelTreesAll = self::relationsTree($treeName='All');

		$out  = [];
		$result=[];

		if (isset($mmava['TeamOrg']['org_type_id'])){
			$orgTypeID = isset($mmava['TeamOrg']['org_type_id']);
		}


		// TeamOrg
		$teamOrgScmda = self::storeModelByMtva('TeamOrg', $mmava, $scmda, $runValidation);
		if ( isset($teamOrgScmda['success']['TeamOrg']) ){
			$teamOrgPkVal = $teamOrgScmda['keys']['org_id'];
			//$TeamOrg=TeamOrg::model()->findByPk($teamOrgPkVal);

		}

		if ((array_key_exists($mtfva['Team']['team_org_org_id']) !== false)){
			// Pull value from $scmda
			$mmava['Team']['team_org_org_id'] = $teamOrgPkVal;
		}
		$mmava['Team']['team_org_org_id'] = $teamOrgPkVal;
		$storeTeamResult = self::storeModelByMtva('Team', $mmava, $scmda, $runValidation);
		$out[$treeName]['Team'] = $storeTeamResult;

		if ($orgTypeID == 1){ // school
			$mmava['School']['org_id'] = $teamOrgPkVal;
			$schoolScmda = self::storeModelByMtva('School', $mmava, $scmda, $runValidation);
			$out[$treeName]['School'] = $schoolScmda;
		}

		if ( isset($mmava['Coach']['org_id']) && (int) $mmava['Coach']['org_id'] == 0){
			$mmava['Coach']['org_id'] = $teamOrgPkVal;
		} elseif ( isset($mtfva['Coach']['org_id']) && (int) $mmava['Coach']['org_id'] > 0){
			$coachHasOrg = true;
		}
		$coachScmda = self::storeModelByMtva('Coach', $mmava, $scmda, $runValidation);
		$out[$treeName]['Coach'] = $coachScmda;

		$teamCoachScmda = self::storeModelByMtva('TeamCoach', $mmava, $scmda, $runValidation);
		$out[$treeName]['TeamCoach'] = $teamCoachScmda;

		$teamPlayerScmda = self::storeModelByMtva('TeamPlayer', $mmava, $scmda, $runValidation);
		if ( isset($mmava['Player']['player_id']) && (int) $mmava['Player']['player_id'] > 0){
			$mmava['TeamPlayer']['player_id'] = $mmava['Player']['player_id'];
			$teamPlayerScmda = self::storeModelByMtva('TeamPlayer', $mmava, $scmda, $runValidation);
			$out[$treeName]['TeamPlayer'] = $teamPlayerScmda;
		}
		$teamPlayerID='';
		$playerID = '';


		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($out, 10, true);
		//return $out;
		$parsedScmda = self::storeMtvaCompositeScmdaWrapper($out);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($parsedScmda, 10, true);

		// @todo call Relations module

		// call process hook
		self::afterStoreMtva();


		return $parsedScmda;

	}

	/**
	 * This is an orchestration method that is not used
	 *
	 * @param type $param
	 */
	public static function TeamCreateByOrgTypeID($param) {
		// if team clubs
		// teamOrg create
		// if school write school
		//
		// if coach info
		//   PersonCoach create3?
		//   TeamCoach create
		//
		// create teamPlayer
		//    player_id
		//    coach_id
		//  update player with last team_player_id

		// if school


	}

	/**
	 * This method asserts all relations with in the scmda and AthleteTeams composite models
	 * @param array $mtfvaScmda a fully populated scmda indexed by treeName then modelName
	 * @return true true=all relations are correcty set. false=relation errors exist.
	 * @uses self::p_componentModelsRelationErrors, self::p_componentModelKeys,
	 * self::p_componentModelRelations, self::p_componentModelTransactionMetaData
	 * @uses self::getRelationsAsSparseList()
	 *
	 */
	protected static function storeMtvaRelations($mtfva, $mtfvaScmda) {
		$modelTreesAll = self::relationsTree($treeName='All');
		// for each component model
		// add relations to scmda input | output
		// inject relations into a copy the originally submitted data array?
		$allRelations   = self::getRelationsAsSparseList($eSaveOnly=false);
		$eSaveRelations = self::getRelationsAsSparseList($eSaveOnly=true);

		$scmdaRel='';

		// foreach model inject relations AND eSaveRelatedChains within the mtfva data array?
		// An eSave potential exists in xyz models?

		$out = [];
		foreach ($modelTreesAll as $modelTreeName=>$modelsTree){
			foreach ($modelsTree as $modelName=>$modelSettings){
				if (isset($modelSettings['isReadOnly']) && $modelSettings['isReadOnly'] == true){
					continue;
				}

				$out[$modelTreeName][$modelName] =
					self::storeModelByMtvfa($modelName, $mtfva, $scmda, $runValidation);

				// if create-update TeamOrg then assert Team exists
				// if create-update Team then assert TeamPlayer exists

				// @todo version of storeModel that accepts and object rather than a model name?
				// Remove empty key:value pairs from the output array
				// handled by storeModel() return;
			}
			//$flattenedScmda = self::storeMtvaCompositeScmda($modelTreeName, $out);
			// PHP best practice after any foreach
			unset($modelName, $modelSettings);
		}

		// PHP best practice after any foreach
		unset($modelTreeName, $modelsTree);
	}

	/**
	 *
	 * @internal Called by: self::storeModelWithRelations()
	 * @todo pull the relation code from self::StoreOrgByMtfva($mtfva) and refactor
	 * it to work with any table and any BelongsTo relation. est 1 hour
	 * @todo add a generic handler for HasMany relations that calls the standard
	 * eSave related.
	 */
	public static function relationHandlerForBelongsTo(){

	}

	/**
	 *
	 * @internal Called by: self::storeModelWithRelations()
	 * @todo pull the relation code from self::StoreOrgByMtfva($mtfva) and refactor
	 * it to work with any table and any BelongsTo relation. est 1 hour
	 * @todo add a generic handler for HasMany relations that calls the standard
	 * eSave related.
	 */
	public static function relationHandlerForHasMany(){

	}

	/**
	 *
	 * @internal Called by: self::storeModelWithRelations()
	 * @todo pull the relation code from self::StoreOrgByMtfva($mtfva) and refactor
	 * it to work with any table and any BelongsTo relation. est 1 hour
	 * @todo add a generic handler for HasMany relations that calls the standard
	 * eSave related.
	 */
	public static function relationHandlerForHasOne(){

	}

	/**
	 *
	 * @internal Called by: self::storeModelWithRelations()
	 * @todo pull the relation code from self::StoreOrgByMtfva($mtfva) and refactor
	 * it to work with any table and any BelongsTo relation. est 1 hour
	 * @todo add a generic handler for HasMany relations that calls the standard
	 * eSave related.
	 */
	public static function relationHandlerForHasManyToMany(){

	}

	/**
	 * AWE Recordset update
	 * Handles BELONGS_TO relations. This is a template for all belongs_to relations
	 */
	public static function StoreOrgByMtfva($mtfva){

		if(! isset($mtfva['Org'])){

			return;
		}

		if(array_key_exists('Org', $mtfva)){
			$orgValues = $mtfva['Org'];
			if(isset($orgValues['org_id']) && (int)$orgValues['org_id']>0){
				$scenario = 'update';
				$pkVal = (int)$orgValues['org_id'];
				$model = Org::model()->findByPk($pkVal);

			} else {
				$scenario = 'insert';
				$model    = new Org;

				// $this->performAjaxValidation($model, 'org-form');
				if(isset($_POST['ajax']) && $_POST['ajax'] === 'org-form')
				{
					echo CActiveForm::validate($model);
					Yii::app()->end();
				}
			}

            if (isset($mtfva['Org']['orgLevel'])) {
				// by relationName
				$model->orgLevel = $mtfva['Org']['orgLevel'];

			} elseif (isset($mtfva['OrgLevel'])) {
				// byModelName
				$model->orgLevel = $mtfva['OrgLevel'];

			} else {

				if ($scenario === 'update'){
					$model->orgLevel = array();
				}
			}


            if (isset($mtfva['Org']['orgType'])) {
				// by relationName
				$model->orgType = $mtfva['Org']['orgType'];

			} elseif (isset($mtfva['OrgType']) ) {
				// byModelName
				$model->orgLevel = $mtfva['OrgType'];

			} else {
				$model->orgLevel = array();
			}

			$model->attributes = $mtfva['Org'];
			if($model->save()) {
				$this->redirect(array('view','id' => $model->org_id));
            }
		}

	}


	/**
	 * reduce memory overhead of the scdma array be removing empty keyName:keyValue pairs
	 * @param array $scmda
	 * @return array
	 * @internal Development Status = Golden!
	 * @since 0.54.3
	 */
	public static function scmdaPack(array $scmda){
		$scmdaOut = $scmda;
		// Remove empty key:value pairs from the output array
		$keysRemoved = [];
		foreach ($scmdaOut as $keyName => $keyValues){
			if (count($scmdaOut[$keyName]) == 0){
				$keysRemoved[] = $keyName;
				unset($scmdaOut[$keyName]);
			}
		}
		unset($keyName, $keyValues);
		return $scmdaOut;
	}

	public static function generateModelValuesArray_TestHarness($viewFieldValueArray, $sparse=true) {
		// @pass a series of test arrays reprensenting form values
		$testTeamClub=[

		];


		$json = self::jsonDataLoader($fva, $calledBy, $jsonRegistryKey);
	}

	/*
	 * @uses self::storeMtvaCompositeScmdaForMultipleModels()
	 * @internal Development Status = ready for testing (rft)
	 * @todo consider method names: scmdaUnNestMultipleModelsWrapper()
	 * @internal Development Status = Golden!
	 */
	public static function storeMtvaCompositeScmdaWrapper(array $treeScmda){
		$out=[];
		$treeNames = ['OrgPerson','OrgTeam','storeMtvaTeamCreate'];
		$treeKeys = array_keys($treeScmda);
		$treesWithinScmda=[];
		foreach ($treeKeys as $keyName){
			if (in_array($keyName, $treeNames)){
				$treesWithinScmda[] = $keyName;
			}
		}
		$treeCount = count($treesWithinScmda);

		if ($treeCount > 0 ){
			foreach ($treeScmda as $treeName => $multipleModelScmda){
				$out[$treeName] = self::storeMtvaCompositeScmdaForMultipleModels($multipleModelScmda);
			}
		} else {
			$out = self::storeMtvaCompositeScmdaForMultipleModels($treeScmda);
		}
		return $out;
	}

	/**
	 *
	 * @param type $treeName
	 * @param type $treeScmda
	 * @return array
	 * @internal remove the concept of handling multiple trees. Move that functionality
	 * up the call stack to a scmda tree hanlder that calls this multiple model method
	 * as needed.
	 * @internal $treeName is now handled by the upsteam tree handler.
	 * @internal Development Status = ready for testing (rft)
	 * @todo consider method names: scmdaUnNestMultipleModels()
	 *
	 *
	 */
	public static function storeMtvaCompositeScmdaForMultipleModels(array $treeScmda){

		$scmdaOut = BaseModel::getStoreChainMetaData_ArrayTemplate();

		$scmdaTemplateKeys = array_keys($scmdaOut);

		$keysWithMultipleItems = ['keys','errors','warnings','modelsTouched'];
		$keysWithSingleItems   = ['pkeys','scenario','success'];
		// assert that all scmda keys are being included in case a new key is added subsequently
		// first pass listed keys, errors, scenario, modelsTouched which is inaccurate
		// The next line is golden! Tested it by adding a temp category to the BaseModel
		// scmda definition and the code below caught it perfectly.
		$unhandledScmdaKeys   = array_diff($scmdaTemplateKeys, $keysWithSingleItems, $keysWithMultipleItems);
		if (count($unhandledScmdaKeys) > 0){
			// @todo enable throw
			throw new CException("unhandled scmda keys exist", 708);
		}
		$keysMissed = [];
		$scmdaOfAllModelsInTree = $treeScmda;
		// reduce if memory foot print
		$localDebug = false;
		if ($localDebug === false){
			unset($treeScmda);
		}

		foreach ($scmdaOfAllModelsInTree as $modelName=>$scmdaOfModel){
			if (isset($scmdaOfModel[$modelName]) && count($scmdaOfModel) > 0){
				// parse scmda
				//$scmdaOfModel = $modelScmda[$modelName];
			}

//				if (isset($modelsScmda[$modelName]['pkeys'])){
//					$scmdaOut['pkeys'] = $modelsScmda[$modelName]['pkeys'];
//				}
//				Example of the original creation layout of the multi-tree + multiple model scmda
//				$out[$modelTreeName][$modelName] =
//						self::storeModel($modelName, $mtfva, $scmda, $runValidation);

			foreach ($scmdaOfModel as $keyName => $keyValues){
				if (count($scmdaOfModel[$keyName]) == 0){
					// prevent keys with empty values from being in the output array
					// the key will still exist in the template array so remove the key
					// from the template if it's still empty before this method exits
					continue;
				}
				if (in_array($keyName, $keysWithSingleItems)){
					if ($keyName == 'scenario'){
						$scmdaOut[$keyName][$modelName] = $keyValues[$modelName];
					}
					elseif ($keyName == 'pkeys'){
						$keyValuesKeys = array_keys($keyValues);
						$pkName = $keyValuesKeys[0];
						$scmdaOut[$keyName][$pkName] = $keyValues[$pkName];

					} else {
						$scmdaOut[$keyName] = $keyValues;
					}
				} elseif (in_array($keyName, $keysWithMultipleItems)){
					// this is a multi-items entry
					// errors are nested crazy deep about 5 levels down so they need a special handler
					if ($keyName == 'errors'){
						if (isset($keyValues[$modelName]) && !isset($scmdaOut[$keyName][$modelName])){
							if (isset($keyValues[$modelName])){
								// reduce the depth of the array if possible
								$scmdaOut[$keyName][$modelName] = $keyValues[$modelName];
							} else {
								// use the field names as keys
								$scmdaOut[$keyName][$modelName] = $keyValues;
							}
						} else {
							// unknown use case
							$x=1;
						}
					} elseif ($keyName == 'warnings'){
						$scmdaOut[$keyName][$modelName] = $keyValues[$modelName];
					} elseif ($keyName == 'keys'){
						// use the field names as keys
						// eg scmda[keys][modelName][attributeName]=attributeValue
						foreach($keyValues as $attributeName => $attributeValue){
							$scmdaOut[$keyName][$modelName][$attributeName] = $attributeValue;
						}
					} elseif (isset($keyValues[$modelName]) && !isset($scmdaOut[$keyName][$modelName])){
						// use the model name as the next level key
						$scmdaOut[$keyName][$modelName] = $keyValues[$modelName];
					} elseif (isset($scmdaOut[$keyName][$modelName])){
						// the model already exists so append any further entries via indexed arrays
						$scmdaOut[$keyName][$modelName][] = $keyValues[$modelName];
					} else {
						$scmdaOut[$keyName][] = $keyValues;
					}
				} else {
					// An unrecognized or unhandled key name
					$keysMissed[] = $keyName;
				}
			}
			unset ($keyName, $keyValues);
		}

		// PHP best practice after any foreach
		unset($modelName, $scmdaOfModel);

		// Remove empty key:value pairs from the output array
		foreach ($scmdaOut as $keyName => $keyValues){
			if (count($scmdaOut[$keyName]) == 0){
				unset($scmdaOut[$keyName]);
			}
		}

		return $scmdaOut;
	}

	public static function storeMtvaCompositeScmdaForMultipleModelTrees($treeName, $treeScmda){

	}
	/**
	 * Parse the massive scmda into a flatter and more useful scmda
	 * Returns pkeys, success, errors, warnings, diffs[ModelName]
	 * @param string $treeName
	 * @param array $multipleModelScmda
	 * @return array $out
	 * @internal original concept was to have one function process both trees of scmda
	 * but this scenario seems unlikely in production so use a wrapper function to detect
	 * if the array passed in is a multi-table scmda or a multi-tree scmda
	 * @deprecated since version 0.54.3
	 */
	public static function storeMtvaCompositeScmda($multipleModelScmda){
		$out=[];
		$scmdaOut = BaseModel::getStoreChainMetaData_ArrayTemplate();

		$modelTreesAll = $multipleModelScmda;
		$keysWithMultipleItems = ['errors','warnings',];
		$keysWithSingleItems   = ['pkeys','scenario','success'];
		$keysMissed = [];
		foreach ($modelTreesAll as $modelTreeName=>$scmdaOfAllModelsInTree){
			foreach ($scmdaOfAllModelsInTree as $modelName=>$scmdaOfModel){
				if (isset($scmdaOfModel[$modelName]) && count($scmdaOfModel) > 0){
					// parse scmda
					//$scmdaOfModel = $modelScmda[$modelName];
				}

				foreach ($scmdaOfModel as $keyName => $keyValues){
					if (count($scmdaOfModel[$keyName]) == 0){
						continue;
					}
					if (in_array($keyName, $keysWithSingleItems)){
						$scmdaOut[$keyName] = $keyValues;
					} elseif (in_array($keyName, $keysWithMultipleItems)){
						$scmdaOut[$keyName][] = $keyValues;
					} else {
						$keysMissed[] = $keyName;
					}
				}
				unset ($keyName, $keyValues);
//				if (isset($modelsScmda[$modelName]['pkeys'])){
//					$scmdaOut['pkeys'] = $modelsScmda[$modelName]['pkeys'];
//				}
//				$out[$modelTreeName][$modelName] =
//						self::storeModel($modelName, $mtfva, $scmda, $runValidation);
			}
			//$flattenedScmda = self::storeMtvaCompositeScmda($modelTreeName, $out);
			$out[$modelTreeName] = [];
			// PHP best practice after any foreach
			unset($modelName, $modelScmda);
		}
		return $scmdaOut;
	}

	/*
	 * @param array $view_field_value_array A simple fieldname=>field-value array
	 * @param $strict Only update fields that are passed in
	 * @returns array $rtn A multi-table field value array
	 * @uses self::fetchTargetModelFieldNameXlation() Returns an xlation array to convert sql view Attributes to base table model Attributes
	 * @uses self::relationsTree() To order the models by their relations hieararchy
	 * @version 1.2
	 * @since app v0.54.0
	 * @package Athlete resumes
	 * @uses self::componentTableKeys()
	 * @internal v1.2 Uses the required values for a table to determine the key's neccessary location
	 * @internal v1.1 Has a critical bug that returns uses pkey values in fkey locations.
	 */
	public static function generateModelValuesArray($viewFieldValueArray, $sparse=true) {
		$debug  = true;
		$xlat	= self::fetchTargetModelFieldNameXlation($viewFieldValueArray);
		$xlatKeys = self::componentTableKeys();
		// attributes like 'person_age' that are calculated values and can't be written back to the database
		$readonly_attr	= self::fetchReadOnlyAttributes();
		$viewFvaSansReadOnly = array_diff_key( $viewFieldValueArray, array_flip( $readonly_attr) ); // remove readonly fields
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fieldValueArray, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fva, 10, true);
		//$field_list = array_keys($fva);
		if (YII_DEBUG && YII_TRACE_LEVEL >=3 && $debug){
			$traceWhere = "top of " . __METHOD__ ;
			$calledBy = __METHOD__;
			$fva = $viewFieldValueArray;
			self::jsonDataLog($fva, __METHOD__);
		}

		$treeOutAll    = self::relationsTree(); // Return an array populated with all related models as keys
		$treeOutSparse = [];

		$attributesNotFound = [];
//		//$view_fields_passed_in = array_keys($$view_field_values_array);
//		// @todo the logic loop must run on a non-read only list THEN be filtered
//		// to remove readonly?
		$xPkeys = $xlatKeys['pkeyXref'];
		foreach ($viewFvaSansReadOnly as $viewFieldName => $viewFieldValue) {
			// The next lines assumes the field list is truly unique and it is not unique
			// Pkey fields are often repeated as fkeys using the same name
			if ($viewFieldName == 'team_league_id'){
				$x='stop here';
				unset($x);
			}
			if(array_key_exists($viewFieldName, $xlat)){
				//$tree[$xlat[$viewFieldName]['model']][$xlat[$viewFieldName]['attribute']]
				//		= $viewFieldValue;

				$xModelName = $xlat[$viewFieldName]['model'];
				$xAttrName  = $xlat[$viewFieldName]['attribute'];
				// @todo break the next if into two parts to enable accurate debugging
				// is the viewFieldName a key field (pk or fk) any component model?
				if (array_key_exists($xAttrName, $xPkeys)){

					$pkeyXrefModelName = $xPkeys[$xAttrName];
					if ($pkeyXrefModelName !== $xModelName){
						// add the xlat pkey field to the output arrays

						// @todo Are there any other fields in the fva that
						// belong to the xpkModel? if Yes add the field
						$treeOutAll[$pkeyXrefModelName][$xAttrName] = $viewFieldValue;
						if ($sparse){
							// only update fields that are passed in
							$treeOutSparse[$pkeyXrefModelName][$xAttrName] = $viewFieldValue;
						}
					}
				}
				// add the xlat field to the output arrays
				$treeOutAll[$xModelName][$xAttrName] = $viewFieldValue;
				if ($sparse === true){
					// only update fields that are passed in
					$treeOutSparse[$xModelName][$xAttrName] = $viewFieldValue;
				}

			} else {
				$attributesNotFound[$viewFieldName] =  $viewFieldValue;
			}
		}

		// include only the fields passed in without dangling model names
		// when values for that model are not supplied

		$treeOut=[];
		if ($sparse === true){
			$treeOut = $treeOutSparse;
		} else {
			// include all xlat fields
			$treeOut = $treeOutAll;
		}
		// @todo before returning the mtfva consider removing any models that only
		// contain keys and no actual values

		// @todo before returning the mtfva consider removing any models that
		// do not contain a primary key UNLESS the fields required for an insert are present

//		$itemsRemoved = [];
//		$itemsRemoved = [];
//		$okToRemoveModel=[];
//		foreach ($treeOut as $modelName=>$attributes ){
//			$okToRemoveModel = false;
//			if (count($attributes) == 0){
//				// remove an empty model
//				$okToRemoveModel[$modelName]['reason'] = ['empty model'];
//				$itemsRemoved[$modelName] = $attributes;
//				unset($treeOut[$modelName]);
//			}
//			if ($okToRemoveModel == true);
//				$itemsRemoved[$modelName] = $attributes;
//				unset($treeOut[$modelName]);
//
//		}

		return $treeOut;
		}

	/**
	 *
	 * @param array $array Typically an fva or mtfva eg
	 * @param string $registryName null=__CLASS__ as the jsonRegistryName
	 * @param string $registryKey   null=Bdate::mtime_table_name();  // 20130904_120503_7543
	 * @return boolean   // @todo log is not persisting
	 */
	protected static function jsonDataLog($array,$registryName=null, $registryKey=null) {

		$debug = true;
		if (YII_DEBUG && YII_TRACE_LEVEL >=3 && $debug){
			$arrayAsJson = CJSON::encode($array);

			if (empty($registryName)){
				$jsonRegistryName = __CLASS__;
			} else {
				$jsonRegistryName = $registryName;
			}

			if (empty($registryKey)){
				// 20130904_120503_7543
				$jsonRegistryKey = Bdate::mtime_table_name();
			} else {
				$jsonRegistryKey = $registryKey;
			}

			$thisMethod = __METHOD__;

			trace('jsonDataSaver() calledBy='.$jsonRegistryName);

			trace("dumping incomming data array", $thisMethod);
			$dumpText = CVarDumper::dumpAsString($array, 10, true);
			trace($dumpText, $thisMethod);

/*
			// @todo add time stamp to the registry key
			$j = new JSONStorage();

			$jsonRegistryExists = $j->registryExists($jsonRegistryName);
			if (! $jsonRegistryExists){
				trace("Json registry does not exist ($jsonRegistryName). Adding it", $thisMethod);
				$j->addRegistry($jsonRegistryName);
			} else {
				trace("Json registry found ($jsonRegistryName)", $thisMethod);
			}
			$jsonRegistryKey = Bdate::mtime_table_name();  // 20130904_120503_7543
			trace("JSONStorage json registry key = " . $jsonRegistryName, $thisMethod);
			$j->setData($jsonRegistryKey, $arrayAsJson, $jsonRegistryName);
			$j->save();
*/
			// save the json registry data in the database
			if (strlen($arrayAsJson) < 1024){
				$jsonStub = $arrayAsJson;
			} else {
				$jsonStub = substr($arrayAsJson,0,1023);
			}

			$attributesToSave = [
				'app_parm_type'=>'jsonRegistryName:' . $jsonRegistryName,
				'ap_varchar'   =>'jsonRegistryKey:'  . $jsonRegistryKey,
				'ap_comment'   =>'jsonData:'         . $jsonStub,
			];

			//$model = AppParm::model()->findByAttributes($appParamAttrib1);

			$model = new AppParm();
			$model->setAttributes($attributesToSave);

			$runValidation=true;
			if (! $model->save($runValidation)){
				trace("jsonRegistryKey " . $jsonRegistryKey . " was not saved");
				trace(CVarDumper::dump($model->getErrors(), 10, true));
			}
			//echo $j->registryExists('custom');
			//$j->load();
			//echo $j->getData('super','custom');
		} else {
			return false;
		}
	}

	/**
	 *
	 *
	 * @param string $registryName null=__CLASS__ as the jsonRegistryName
	 * @param string $registrKey   null=Bdate::mtime_table_name();  // 20130904_120503_7543
	 * @param string $traceWhere
	 * @return string $json
	 */
	public static function jsonDataLoader($registryName=null, $registryKey=null) {

		$debug = true;
		if (YII_DEBUG && YII_TRACE_LEVEL >=3 && $debug){
			if (empty($registryName)){
				$jsonRegistryName = __CLASS__;
			} else {
				$jsonRegistryName = $registryName;
			}

			if (empty($registryKey)){
				// 20130904_120503_7543
				$jsonRegistryKey = Bdate::mtime_table_name();
			} else {
				$jsonRegistryKey = $registryKey;
			}

			$thisMethod = __METHOD__;
			$j = new JSONStorage();
			$jsonRegistryExists = $j->registryExists($jsonRegistryName);
			if (! $jsonRegistryExists){
				trace(" Json registry does not exist " . $jsonRegistryName, $thisMethod);
				$j->addRegistry($jsonRegistryName);
			} else {
				trace("Json registry name found ($jsonRegistryName)", $thisMethod);
				$j->load();
				$j->getData($jsonRegistryKey,$jsonRegistryName);
			}
//			$jsonRegistryKey = Bdate::mtime_table_name();
//			trace("JSONStorage json registry key = " . $jsonRegistryName, $thisMethod);
//			$j->setData($jsonRegistryKey, $incomingAsJson, $jsonRegistryName);
//			$j->save();

			//echo $j->registryExists('custom');
			//$j->load();
			//echo $j->getData('super','custom');
		} else {
			return false;
		}
	}

	/**
	 * Provides a list of every component model keyName and each model it can be found in
	 * assoc of cmPkeys by Model name $cmKeyContainer['pkey']  eg cmModelName=>cmPkeyName
	 * assoc of cmFkeys by Model name $cmKeyContainer['fkeys'] eg cmModelName=>cmFkeyName
	 * assoc of cmKeys  by Model name $cmKeyContainer['all']   eg cmModelName=>cmKeyName
	 * assoc of cmPkeys by PKey  name $cmKeyContainer['pkeyXref'] eg cmPkeyName=>cmModelName
	 * assoc of cmFkeys by FKey  name $cmKeyContainer['fkeyXref'] eg cmFkeyName=>cmModelName
	 * assoc of cmKeys  by Key   name $cmKeyContainer['keysXref'] eg cmKeyName=>cmModelName
	 * [ ['pkey'], ['fkeys'], ['all'], ['pkeyXref'], ['fkeyXref'], ['keysXref'] ]
	 *
	 * @return array $cmKeyContainer
	 * @internal Development Status = Golden!
	 */
	protected static function componentTableKeys() {
		$cmModels    = self::getComponentModels();
		// ['modelName'=>['keys','non-keys','all']]
		$cmModelAttr = BaseModel::fetchModelFieldNamesMuliple($cmModels);
		$cmModelPkeyRef  = [];
		$cmModelFkeysRef = [];
		$cmModelKeysRef  = [];
		$cmPkeysXref = [];
		$cmFkeysXref = [];
		$cmFkeysTemp = [];
		$cmKeysXref  = [];
		$cmKeysTemp  = [];
		$cmKeyContainer  = [];
		foreach ($cmModelAttr as $cmModel => $attrList) {
			$cmModelKeysRef[$cmModel] = $attrList['keys'];
			$cmFkeysTemp = [];
			$cmKeysTemp  = [];
			foreach ($attrList['keys'] as $keyIdx => $keyName) {
				if ($keyIdx === 0){
					$cmModelPkeyRef[$cmModel] = $keyName;
					$cmPkeysXref[$keyName]    = $cmModel;
					$cmKeysTemp[$keyName]     = $cmModel;
				} else {
					$cmModelFkeysRef[$cmModel][] = $keyName;
					$cmFkeysTemp[$keyName] = $cmModel;
					$cmKeysTemp[$keyName]  = $cmModel;
				}
			}
			if (count($cmFkeysTemp)>0){
				$cmFkeysXref[] = $cmFkeysTemp;
			}
			if (count($cmKeysTemp)>0){
				$cmKeysXref[] = $cmKeysTemp;
			}
		}
		// Next list contains all cmPkeys
		$cmKeyContainer['pkey']  = $cmModelPkeyRef;
		// Next list contains all cmFkeys
		$cmKeyContainer['fkeys'] = $cmModelFkeysRef;
		$cmKeyContainer['all']   = $cmModelKeysRef;

		$cmKeyContainer['pkeyXref'] = $cmPkeysXref;
		$cmKeyContainer['fkeyXref'] = $cmFkeysXref;
		$cmKeyContainer['keysXref'] = $cmKeysXref;

		return $cmKeyContainer;
	}

	/**
	 * Used to test componentTableKeys() output
	 */
	public static function componentTableKeys_testHarness() {
		$cmKeysXref = self::componentTableKeys();
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($cmKeysXref, 10, true);
	}

	/*
	 * @param array $view_field_value_array A simple fieldname=>field-value array
	 * @param $strict Only update fields that are passed in
	 * @returns array $rtn A multi-table field value array
	 * @uses self::fetchTargetModelFieldNameXlation() Returns an xlation array to convert sql view Attributes to base table model Attributes
	 * @uses self::relationsTree() To order the models by their relations hieararchy
	 * @version 1.1
	 * @since app v0.54.0
	 * @package Athlete resumes
	 */
	public static function generateModelValuesArray_v1dot1($viewFieldValueArray, $sparse=true) {
		$debug  = 0;
		$xlat	= self::fetchTargetModelFieldNameXlation($viewFieldValueArray);

		// attributes like 'person_age' that are calculated values and can't be written back to the database
		$readonly_attr	= self::fetchReadOnlyAttributes();
		$viewFvaSansReadOnly = array_diff_key( $viewFieldValueArray, array_flip( $readonly_attr) ); // remove readonly fields
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fieldValueArray, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fva, 10, true);
		//$field_list = array_keys($fva);

		$tree   = self::relationsTree(); // Return an array populated with all related models as keys
		$treeStrict = [];
		//$rtn	= [];
		$attributesNotFound = [];
		//$view_fields_passed_in = array_keys($$view_field_values_array);
		foreach ($viewFvaSansReadOnly as $viewFieldName => $viewFieldValue) {
			if(array_key_exists($viewFieldName, $xlat)){
				//$models[] = $xlat[$view_field_name]['model'];
//				$rtn[$xlat[$view_field_name]['model']][$xlat[$view_field_name]['attribute']]
//						= $view_field_value;
				$tree[$xlat[$viewFieldName]['model']][$xlat[$viewFieldName]['attribute']]
						= $viewFieldValue;
				if ($sparse){
					// only update fields that are passed in
					$treeStrict[$xlat[$viewFieldName]['model']][$xlat[$viewFieldName]['attribute']]
						= $viewFieldValue;
				}
			} else {
				$attributesNotFound[$viewFieldName] =  $viewFieldValue;
			}
		}
		if ($debug===1){
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rtn, 10, true);
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($tree, 10, true);
//			$diff = array_diff($rtn, $tree);
//			$msg = "Original return vs tree return follows";
//			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($diff, 10, true);
		}
		//return $rtn;
		if ($sparse){
			// include only the fields passed in without dangling model names
			// when values for that model are not supplied
			return $treeStrict;

//			$treeStrictSansEmptyModels = $treeStrict;
//			foreach ($treeStrict as $modelName=>$attributes ){
//				if (count($attributes) == 0){
//					unset($treeStrictSansEmptyModels[$modelName]);
//				}
//			}
//			return $treeStrictSansEmptyModels;

			// work around for handling two org models
//			$modelsInTreeStrict = array_keys($treeStrict);
//			$orgModelsInTreeStrict = array_keys($treeStrict, ['Org','TeamOrg']);
//			if (count($orgModelsInTreeStrict) == 2){
//				throw new CException("Only one Org model can be processed at a time", 707);
//			}
//
//			if (in_array('TeamOrg', $modelsInTreeStrict)) {
//				$teamOrgVals = $treeStrict['TeamOrg'];
//				$treeStrictCopy = $treeStrict;
//				unset($treeStrictCopy['TeamOrg']);
//				$treeStrictCopy['Org']=$teamOrgVals;
//				return $treeStrictCopy;
//			} else {
//				return $treeStrict;
//			}

		} else {
			// include all xlat fields
			return $tree;
		}
	}


	/**
	 * Returns an array of attributes like 'person_age' that are calculated values and can't be written back to the database
	 * @param type $field_value_array
	 * @internal Development Status = code construction
	 */
	protected static function fetchReadOnlyAttributes() {
		//		$readonly_attr[]; //
		// return ['person_age', 'person_dob_eng', 'person_bday_long', 'person_height_imperial','person_bmi',];
		$readonly_attr[] = 'person_age';
		$readonly_attr[] = 'person_dob_eng';
		$readonly_attr[] = 'person_bday_long';
		$readonly_attr[] = 'person_height_imperial';
		$readonly_attr[] = 'team_org_org_type_name'; // This field maps OrgType.org_type_name
		$readonly_attr[] = 'person_bmi';
		$readonly_attr[] = 'age_group_name';
		$readonly_attr[] = 'person_type_name';		// ['model'=>'PersonType','attribute'=>'person_type_name'],  // readonly
		$readonly_attr[] = 'person_type_desc_short'; // ['model'=>'PersonType','attribute'=>'person_type_desc_short'],  // readonly
		$readonly_attr[] = 'person_type_desc_long';	//	=> ['model'=>'PersonType','attribute'=>'person_type_desc_long'],  // readonly
		$readonly_attr[] = 'gender_desc';			//	=> ['model'=>'Gender','attribute'=>'gender_desc'], // readonly
		$readonly_attr[] = 'gender_code';			//	=> ['model'=>'Gender','attribute'=>'gender_code'], // readonly
		$readonly_attr[] = 'gsm_staff';				//	=> ['model'=>'User','attribute'=>'first_name'], // readonly

		//
		$readonly_attr[] = 'team_sport_name';
		$readonly_attr[] = 'team_basetable_team_league_id';
		$readonly_attr[] = 'team_basetable_team_league_name';

		$readonly_attr[] = 'team_basetable_team_division_id';
		$readonly_attr[] = 'team_basetable_team_division_name';

		$readonly_attr[] = 'team_org_school_conf_basetable_conference_id';
		$readonly_attr[] = 'team_org_school_conf_basetable_conference_name';
		$readonly_attr[] = 'team_org_school_conf_basetable_ncaa_division';

		$readonly_attr[] = 'team_org_basetable_org_type_id';
		$readonly_attr[] = 'team_org_basetable_org_type_name';

		$readonly_attr[] = 'team_org_basetable_org_level_id';
		$readonly_attr[] = 'team_org_basetable_org_level_name';

		// $readonly_attr[] = '';

		return $readonly_attr;
	}

	/**
	 * Uses a naming convention of base table field names
	 * GetRead
	 * @see sql view: vwAthleteTeams and its column definitions
	 */
	protected function getBaseTableAttributeNames() {
		$baseTableFields=[];
		foreach($this->attributes as $attributeName){
			$needle='_basetable_';
			if (stripos($attributeName, $needle) !== false){
				$baseTableFields[]=$attributeName;
			}
		}
		return $baseTableFields;
	}

	/**
	 *
	 * @param type $mtfva Multi-Table Field-Value-Array
	 * @internal Development Status = code construction
	 * @todo document
	 */
	protected function baseTableHandler($mtfva) {
		$extractBaseTables = $this->getBaseTableAttributeNames();


		$modelNames = [
			'OrgType'     =>[
				'xlat'=>[
					'team_org_basetable_org_type_id'  =>'org_type_id',
					'team_org_basetable_org_type_name'=>'org_type_name',
				],
				'compare'=>[
					'primaryKey'=>[
						'org_level_id'=>[
							'team_org_basetable_org_type_id'  =>'team_org_org_level_id'
						],
					],
					'dupeKey'   =>[
						'org_type_name'=>[
							'team_org_basetable_org_type_name'=>'team_org_org_level_name',
						],
					],
				],
			],
			'OrgLevel'    =>[
				'xlat'=>[
					'team_org_basetable_org_level_id'  =>'org_level_id',
					'team_org_basetable_org_level_name'=>'org_level_name',
				],
				'dupeKey'=>'org_level_name',
			],
			'TeamLeague'  =>[
				'xlat'=>[
					'team_basetable_team_league_id'  =>'team_league_id',
					'team_basetable_team_league_name'=>'team_league_name',
				],
				'dupeKey'=>'team_league_name',
			],
			'TeamDivision'=>[
				'xlat'=>[
					'team_basetable_team_division_id'  =>'team_division_id',
					'team_basetable_team_division_name'=>'team_division_name',
				],
				'dupeKey'=>'team_division_name',
			],
			'Conference'  =>[
				'xlat'=>[
					'team_org_school_conf_basetable_conference_id'  =>'conference_id',
					'team_org_school_conf_basetable_conference_name'=>'conference_name',
					'team_org_school_conf_basetable_ncaa_division'  =>'ncaa_division',
				],
				'dupeKey'=>'conference_name',
			],
		];

		$localDebug = true;
		foreach($modelNames as $modelName=>$settings){

			$xlat    = $settings['xlat'];
			if (isset($settings['compare'])){
				$compare = $settings['compare'];
			}else{
				continue;
			}

			$compare = $settings['compare'];
			$model   = new $modelName();

			$compareResult=[];
			//if ( $xlatPk == ($modelName) ){
				if (isset($mtfva[$modelName])){
					// Ok to process this model
					// was the athleteTeam model value updated?
					$pkSettings                 = $compare['primaryKey'];
					$pkBaseTableFieldName       = array_keys($pkSettings);
					$pkBaseTableViewFieldName   = array_keys($pkSettings[$pkBaseTableFieldName]);
					$pkEntityTableViewFieldName = array_values($pkSettings[$pkBaseTableFieldName]);

					$pkBaseTableViewFieldValue    = $mtfva[$pkBaseTableViewFieldName];
					$pkEntityTableViewFieldValue  = $mtfva[$pkEntityTableViewFieldName];

					$dupeSettings                 = $compare['dupeKey'];
					$dupeBaseTableFieldName       = array_keys($dupeSettings);
					$dupeBaseTableViewFieldName   = array_keys($dupeSettings[$dupeBaseTableFieldName]);
					$dupeEntityTableViewFieldName = array_values($dupeSettings[$dupeBaseTableFieldName]);

					$dupeBaseTableViewFieldValue    = $mtfva[$dupeBaseTableViewFieldName];
					$dupeEntityTableViewFieldValue  = $mtfva[$dupeEntityTableViewFieldName];

					if ($mtfva[$dupeBaseTableViewFieldName] === $mtfva[$dupeEntityTableViewFieldName]){
						// no insert of a new dupekey value
						// search model to see if a dupe exists
						//if ($model::model()->findByAttributes([$dupeBaseTableFieldName=>]))
						$compareResult[] = 'first if == true';
					}

					if ($mtfva[$pkBaseTableViewFieldName] === $mtfva[$pkEntityTableViewFieldName]){
						// no update of an existing value
						$compareResult[] = 'second if == true';
					}

					if ($dupeBaseTableViewFieldValue === $dupeEntityTableViewFieldValue){
						$compareResult[] = 'third if == true';
						// no insert of a new dupekey value
						// search model to see if a dupe exists
						//if ($model::model()->findByAttributes([$dupeBaseTableFieldName=>]))
					}
					if ($pkBaseTableViewFieldValue === $pkEntityTableViewFieldValue){
						$compareResult[] = 'fourth if == true';
						// no insert of a new dupekey value
						// search model to see if a dupe exists
						//if ($model::model()->findByAttributes([$dupeBaseTableFieldName=>]))
					}

					// if basePk empty and entityPK empty AND entityDupe IS NOT empty
					// then test if entityDupe value exists in baseTable
					//   if exists  then skip insert pulling existing Pk from Entity
					//   if NOT exists then insert

					// if basePK NOT empty AND entityDupePk IS NOT empty
					// then a simple update was performed
					// skip base table update
					if ($localDebug){
						YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($compareResult, 10, true);
					}
				}

			//}

		}


	}

	/**
	 * A list of paired tables that can saved using StoreGeneric
	 * @param string $personTypeName Athlete|Coach
	 * @return array $saveWithBatchOrder
	 * @package StoreGeneric
	 * @since v0.54
	 */
	protected function relationChainHierarchy($personTypeName='Athlete') {
		if ($personTypeName == 'Athlete' || $personTypeName == 'Player'){
			// The index is the saveGeneric batch call number
			$saveWithBatchOrder=[
				1=>['OrgType'],
				2=>['OrgLevel'],
				3=>['Org','Person'],
				4=>['Player'],
				5=>['TeamDivsion'],
				6=>['TeamLeague'],
				7=>['SportPosition'],
				8=>['Team','TeamPlayer'],
				9=>['Team','TeamOrg'],
			];
		}elseif ($personTypeName == 'Coach'){
			// The index is the saveGeneric batch call number
			$saveWithBatchOrder=[
				1=>['OrgType'],
				2=>['OrgLevel'],
				3=>['Org','Person'],
				4=>['Coach'],
				5=>['TeamDivsion'],
				6=>['TeamLeague'],
				7=>['SportPosition'],
				8=>['Team','TeamPlayer'],
				9=>['Team','TeamOrg'],

			];
		}
		return $saveWithBatchOrder;
	}

	//$this->configUtility_RelationsByModelName();


	/**
	 * Saves a specific model the view is composed of.
	 *
	 * The record is inserted as a row into the database table if its {@link isNewRecord}
	 * property is true (usually the case when the record is created using the 'new'
	 * operator). Otherwise, it will be used to update the corresponding row in the table
	 * (usually the case if the record is obtained using one of those 'find' methods.)
	 *
	 * Validation will be performed before saving the record. If the validation fails,
	 * the record will not be saved. You can call {@link getErrors()} to retrieve the
	 * validation errors.
	 *
	 * If the record is saved via insertion, its {@link isNewRecord} property will be
	 * set false, and its {@link scenario} property will be set to be 'update'.
	 * And if its primary key is auto-incremental and is not set before insertion,
	 * the primary key will be populated with the automatically generated key value.
	 *
	 * @param string | array $models
	 * @param boolean $runValidation whether to perform validation before saving the record.
	 *     If the validation fails, the record will not be saved to database.
	 * @param array $attributesFilter list of attributes that need to be saved. Defaults to null,
	 * meaning all attributes that are loaded from DB will be saved.
	 * @return boolean whether the saving succeeds
	 * @uses AthleteTeamsrelationChainHierarchy A list of batches to submit to StoreGeneric
	 * @uses self::storeMtva()
	 *
	 * @todo Add coach fields?
	 * @todo implement $modelsFilter as needed as it is not critical at this time.
	 * @internal implements $attributesFilter
	 * @internal v0.3.0 signature = public function saveModels($models='all', $runValidation=true,$attributes=null)
	 * @example $AthleteTeams->saveModels($models='all', $runValidation=true,$attributes=null);
	 */
	public function storeModels($fieldValuesArray=null, $runValidation=true, $modelsFilter=null, $attributesFilter=null)
	{
		$localAttributeFilter = [];
		$batchTimeStr = Bdate::mtime_table_name();

		$this->getRelationsAsSparseList($eSaveOnly=false);
		$this->configUtility_RelationsByModelName();

		// @todo add relations to scmda input | output
		// inject relations into a copy the originally submitted data array?
		$allRelations   = self::getRelationsAsSparseList($eSaveOnly=false);
		$eSaveRelations = self::getRelationsAsSparseList($eSaveOnly=true);
		self::$p_componentModelRelations      = $allRelations;
		self::$p_componentModelRelationsESave = $eSaveRelations;

		$scmdaRel='';
		$readOnly=true;

		$hasFatalError = $this->hasFatalError;  // Used as an 'OK to continue' flag during code testing in the debugger

		$localDebug = false;
		// @todo enable the attributeFilter and model filter after the data updates are solid
		// <editor-fold defaultstate="collapsed" desc="parse attributes param">
		if (!is_null($attributesFilter)){
			// filter specific attributes
			if (is_array($attributesFilter) && count($attributesFilter) > 0){
				// loop
				foreach ($attributesFilter as $attribute) {
					$localAttributeFilter[] = $attribute;
				}
			} elseif (is_string($attributesFilter) && strlen($attributesFilter) > 0){
				// filter items passed in
				if (stripos($attributesFilter, ',') !== false){
					// parse string
					$fields = explode(',', $attributesFilter);
					foreach ($fields as $field){
						$localAttributeFilter[] = $field;
					}
				} else {
					// its a non-delimited string
					$localAttributeFilter[] = $attributesFilter;
				}
			} else {
				throw new CException("The attributeFilter ($attributesFilter) parameter is NOT an array or string");
			}
		} else { }
		// </editor-fold>

		// @todo Process all the models in sequence
		// @todo Carry keys forward like store chain
		// @todo Determine ins|upd like StoreGeneric()?
		// if all the attributes are empty then return false
		//$valCounts = array_count_values($attributes); // This line presupposes that attributes will be an array which is a false assumption.
		$valCounts = array_count_values($localAttributeFilter);
		$valKeys   = array_keys($valCounts);
		if (count($valCounts) == 1){
			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($valCounts, 10, true);
			throw new CException('At least two attributes are required. Typically a pkey  and a value',707);
		} elseif (count($valKeys) == 2){
			if (isset($valKeys[''])  && $valKeys[''] > 0){
				$valEmptyCount = $valKeys['']; // @todo cast as int
			} else {
				$valEmptyCount = 0;
			}
			// if $valKeys array contains an key = '' then a count for empty values exists
			$valEmptyCount = ( ( array_key_exists('',$valKeys) ) ?  (int)$valKeys[''] : 0  );
		}

		// Get this model's attribute values
		//   All attribute name:value pairs
		// $valuesBeforeUpdate	= $this->attributes;
		//   All attribute values as a diffable data envelope array
		// $valuesBeforeUpdateMtfva = self::generateModelValuesArray($valuesBeforeUpdate);


		// data envelope array of the incomming data
		// $mtfva	= self::generateModelValuesArray($fieldValuesArray);
		// Gen mtfva
		if (is_array($fieldValuesArray) && count($fieldValuesArray) > 0){
			$yiiModels = BaseModel::listYiiModels();
			$array_keys = array_keys($fieldValuesArray,$yiiModels);
			if (count($array_keys) > 0){
				// The keys in the fva are yii model names.
				$mtfva = $fieldValuesArray;
			} else {
				// Use the values passed into create an mtfva
                $mtfva = self::generateModelValuesArray($fieldValuesArray);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                				$mtfva	= self::generateModelValuesArray($fieldValuesArray);
 			}
		} else {
			// The parameter field values array was empty so use the current
			// row as the data to save, eg the underlying athlete teams model has new
			// data values to write
			if (count($localAttributeFilter) > 0){
				$fva   = $this->getAttributes($localAttributeFilter);
			} else {
				$fva   = $this->getAttributes();
			}
			// The fva still needs to be converted to an mtfva
			//$mtfva = $fva;
			$mtfva = self::generateModelValuesArray($fva);

		}
		// @todo enable other data storage engines
		// $dataStorageEngines=['mtfva','yava', 'other'];
		$dataStorageEnginesActive=['mtfva'];
		//$ynxNotSparseResult = self::generateYiiNormativeModelValuesArray($mtfva, $sparse=false);
		//$ynxSparseResult    = self::generateYiiNormativeModelValuesArray($mtfva, $sparse=true);

//		$allVals=[
//			'AllValsBfrUpdate'=>  $valuesBeforeUpdate,
//			'sparseFalse'     =>  $ynxNotSparseResult,
//			'sparseTrue'      =>  $ynxSparseResult,
//		];
//		$allValsJson = json_encode($allVals);
		//self::archiveData($allValsJson);

		// Write the incoming data to an external file for data handling validation
		self::archiveData($mtfva, $isInput=true, $batchTimeStr);
		// the method yn method returns two arrays ['yiiTargets','sourceData']
		//self::storeYiiNormativeModelValuesArray($ynxNotSparseResult);
//		if (in_array('yava',$dataStorageEnginesActive)){
//			self::storeYiiNormativeModelValuesArray($ynxNotSparseResult);
//		}
		if (in_array('mtfva',$dataStorageEnginesActive)){
			$scmdaOutBeta = self::storeMtva($mtfva, $runValidation);
			$scmdaOut     = self::storeMtvaRelations($mtfva, $runValidation);
			// Write the incoming data to an external file for data handling validation
			// The batch time stamp allows the files to sort together in the OS level folder
			self::archiveData($scmdaOut, $isInput=false, $batchTimeStr);
		}

		// @todo The next line assumes the AthleteTeams model is instantiated on
		// the athleteTeams row to update, yet this assumption is invalid for inserts
		// into the TeamPlayer model as no team_player_id is is possible and that
		// is the pk of AthleteTeams model. Hence this will only work for updates?
		//$diffMtfva = array_diff_assoc($mtfva, $valuesBeforeUpdateMtfva);
//		$displayDiffMtfva = false;
//		if ($displayDiffMtfva === true){
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($diffMtfva, 10, true);
//		}
		return $scmdaOut;
		/*
		 * commmented out the following to test storeMtva
		 */
/*
		$readOnlyModels = ['User','OrgType'];
		foreach ($mtfva as $model => $fva) { // mtfva = multi-table fva, $fva= field-value-array
			if(array_search($model, $readOnlyModels) !== false){
				continue; // skip this model
			}
			// @todo the v1 code uses model_filter the v2 does not have a handler for it yet
			$modelFilter[] = $model;
		}

		if(is_array($models) && count($models) > 0){
			$modelFilterOrig = $modelFilter;
			$modelFilter=[];
			foreach ($models as $model){
				if (in_array($model, $modelFilterOrig)){
					$modelFilter[] = $model;
				}
			}
		}

		// The v2 processing loop
		// The storage chain hierararchy returned from the function below is golden
		$batchChain = $this->relationChainHierarchy($personTypeName='Athlete');
		$batchResult=[]; // tracks inputs and outputs of a single batch
		$batchLog=[];    // tracks inputs and outputs of entire process
		$isReadOnly = $this->isReadOnly; // use a local var so the value can be changed in the debugger
		foreach ($batchChain as $batchID=>$modelNames){
			if($this->hasFatalError){
				continue;
			}
			$batchData=[];
			foreach($modelNames as $modelName){
				if($this->hasFatalError){
					continue;
				}

				if( isset($mtfva[$modelName]) && count($mtfva[$modelName]) > 0 ){
					$batchData[$modelName]=$mtfva[$modelName];
				} else {
					// There are no values for this model so skip it
					continue;
				}
			}
			// Are there values to process
			if (count($batchData) == 0){
				// Skip this batch
				continue;
			}
			//$batchResult[$batchID] = BaseModel::StoreGeneric($batchData);
			// @todo test handling
			// @todo models like OrgType should have both an pk and name value eg org_type_id, org_type_name
			//$batchResult[$batchID] = BaseModel::StoreGeneric($batchData,$isReadOnly);
			//$batchResult[$batchID] = BaseModel::StoreGeneric($batchData,$runValidation,$isReadOnly);
			$safeOnlyAttr = false;
			$batchResult[$batchID] = BaseModel::StoreGeneric($batchData,$runValidation,$safeOnlyAttr,$isReadOnly);

			if (count($batchResult[$batchID]) === 0){
				$batchResult[$batchID] = $batchData;
			}

			$batchLog[$batchID]['Data Sent'] = $batchData;
			$batchLog[$batchID]['Result'] = $batchResult;
		}
		$batchLog['relation chain'] = $batchChain;
		if ($localDebug){
			// The next line will blow up ajax
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($batchLog, 10, true);
		}

		return $batchLog;
*/
	}

		/**
	 *
	 * @param type $cacheID
	 * @see https://www.digitalocean.com/community/tutorials/how-to-use-caching-with-the-yii-framework-to-improve-performance
	 */
	protected static function getCache($cacheID){

		$cacheArray = [

		];

		$cacheCount = Yii::app()->cache->get($key);
		if($cacheCount !== $data['purchase_count']) {
			$cacheArray['duration'] = 0;
				Yii::app()->cache->set($key, $data['purchase_count'], $expirytime);
		}

//		if($this->beginCache($anotherKey, $cacheArray)) {
//			$this->endCache();
//		}
	}



	/**
	 *
	 * @param string $cacheID Unique identifier for the cache. Typically a methodName is passed ie __METHOD__
	 * @param mixed $data
	 * @param int $timeOut Length of time before the cache is flushed. Default is 4 hours (3600 x 4)
	 * @see https://www.digitalocean.com/community/tutorials/how-to-use-caching-with-the-yii-framework-to-improve-performance
	 */
	protected static function setCache($cacheID, $data, $timeOut=14400){

		$cacheArray = [

		];

		$cacheCount = Yii::app()->cache->get($key);
		if($cacheCount !== $data['purchase_count']) {
			$cacheArray['duration'] = 0;
				Yii::app()->cache->set($key, $data['purchase_count'], $expirytime);
		}

//		if($this->beginCache($anotherKey, $cacheArray)) {
//			$this->endCache();
//		}
	}

	/**
	 * Write a json file to the archive data folder during testing and
	 * production stabilization
	 * @param array|string $data
	 * @param bool $isInput = true
	 * @param string $batchTime  uses output from Bdate::mtime_table_name() to
	 * group input and output at the OS file leve
	 * @return bool $result
	 * @internal Development Status = Golden!
	 * @internal Development Status = latest rev ready for testing (rft)
	 */
	public static function archiveData($data, $isInput=true, $batchTime=null){
		//$fileName = 'AthleteTeams_storeModels_input' . Bdate::mtime_table_name();
		$localDebug =false;
		if (is_array($data)){
			$data = json_encode($data);
		} elseif (is_string($data)){
			if ( ! Bjson::isValidJsonString($data)){
				throw new CException("data is not valid JSON", 707);
			}
		}
		if (is_null($batchTime)){
			$batchTimeStr = Bdate::mtime_table_name();
		} else {
			$batchTimeStr = $batchTime;
		}
		//$fileName = 'AthleteTeams_storeModels_input_' . Bdate::mtime_table_name() .'.json';
		if ($isInput){
			// typically an mtfva array structure
			$fileName = 'AthleteTeams_storeModels_' . $batchTimeStr .'_input_.json';
		} else {
			// typically an scmda array structure
			$fileName = 'AthleteTeams_storeModels_' . $batchTimeStr .'_output_.json';
		}
		$archivePath = self::get__import_path('archive');
		$archiveUri  = Bfile::addSlash($archivePath) . $fileName;

		$result   = Bfile::write_to_file($archiveUri, $data);

		if ($localDebug === true){
			if ($result === true){
				$msg = "data update file: $fileName archived in $archivePath";
				YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			} else {
				$msg = "data update file: $fileName was NOT archived in $archivePath";
				YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			}
		}

		return $result;
	}

	/**
	 * Write a json file to the archive data folder during testing and production stabilization
	 * @param array $data
	 * @return bool $result
	 * @internal Development Status = ready for testing (rft)
	 *
	 */
	public static function archivedData_FetchFileList(){
		//$fileName = 'AthleteTeams_storeModels_input' . Bdate::mtime_table_name() . '.json';

		$archivePath = self::get__import_path('archive');
		// params
		$ext='json';
		$epocSecs=null;
		$maxFileCnt=null;
		$ignoreSubFolders=true;
		$files = Bfile::globFiles($archivePath, $ext, $epocSecs,
				$maxFileCnt, $ignoreSubFolders);

		return $files;
	}


	/**
	 * Get an actionable real file path to data file resources on the server
	 *
	 * @param string $path_type 'base_folder','to_process','archive','purged'
	 * @return string real file path to a data file folder
	 * @internal This function has been copied to the to the Bfile component: protected/components/functions/Bfile.php
	 * @example $this->get__import_path($path_type='to_process');
	 */
	public static function get__import_path($path_type=NULL)
	{
		if (empty($path_type)){
			$path_type = 'base_folder';
		}

		$base_path = Yii::app()->params['data_input__by_file__path']['real_path_base_folder'];
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($base_path, 10, true);
		// replace relative paths with real paths
		$paths = Yii::app()->params['data_input__by_file__path'];
		foreach ($paths as $idx=>$path){
			//$paths[$idx] = $path_from_alias . $path;
			if ($idx == 'real_path_base_folder'){
				continue;
			}
			$paths[$idx] = $base_path . $path;
		}

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($paths, 10, true);
		return $paths[$path_type];
	}

	/**
	 *
	 * @param type $models
	 * @param type $runValidation
	 * @param type $attributes
	 * @depreciated since 0.54.1
	 */
	public function saveModels_v2($models='all', $runValidation=true,$attributes=null){
		// The v1 processing loop
//		foreach ($mtfva as $model => $fva) {
//			if(in_array($model, $model_filter)){
//				// process model
//
//				// next line assumes each model has a store() method and they do NOT ALL have it.
//				// is the model's pkey OR fkey present?
//				//$model::store($fva);
//				if ($hasFatalError == true){
//					continue;
//				}
//				if (count($fva) == 0){
//					if ($localDebug){
//						trace('Skipping empty model array '. $model , 'AthleteTeams->saveModels()');
//					}
//					continue; //  skip the processing of empty models
//				}
//				$referenceModel = new $model();
//				$pk = $referenceModel->primaryKey(); // returns the field name of the primary key
//				if(isset($mtfva[$model][$pk])){
//					$pkVal = $mtfva[$model][$pk];
//				} else {
//					$pkVal = 0;
//				}
//				if ( (int)$pkVal > 0){
//					$crudScenario= 'insert';
//					$modelToSave = $model::model()->findByPk($pkVal);
//					if ($model == 'OrgType' && isset($fva['org_type_name']) && !empty($fva['org_type_name'])){
//						$org_type_name = $fva['org_type_name'];
//						// check for dupes
//						$dupeCheck = $model::model()->findByAttributes(['org_type_name'=>$org_type_name]);
//						if (!is_null($dupeCheck)){
//							if ($localDebug){
//								trace('Skipping duplicate '. $model , 'AthleteTeams->saveModels()');
//							}
//							continue; //  skip the processing of duplicate models
//						}
//
//					//continue; //  skip the processing of empty models
//
//					}
//					if ($model == 'TeamOrgType' && isset($fva['team_org_org_type_name']) && !empty($fva['team_org_org_type_name'])){
//						$org_type_name = $fva['org_type_name'];
//						// check for dupes
//						$dupeCheck = OrgType::model()->findByAttributes(['org_type_name'=>$org_type_name]);
//						if (!is_null($dupeCheck)){
//							if ($localDebug){
//								trace('Skipping duplicate '. $model , 'AthleteTeams->saveModels()');
//							}
//							continue; //  skip the processing of duplicate models
//						}
//
//
//					//continue; //  skip the processing of empty models
//
//					}
//					if ($model == 'TeamOrg' && isset($fva['team_org_org_name']) && !empty($fva['team_org_org_name'])){
//						$org_name = $fva['team_org_org_name'];
//						// check for dupes
//						if (isset($mtfva['Team']['team_id']) && (int)$mtfva['Team']['team_id'] > 0){
//							// Team row exists
//						}
//
//						$dupeCheck = Org::model()->findByAttributes(['org_name'=>$org_name]);
//						if (!is_null($dupeCheck)){
//							if ($localDebug){
//								trace('Skipping duplicate '. $model , 'AthleteTeams->saveModels()');
//							}
//							continue; //  skip the processing of duplicate models
//						}
//
//					//continue; //  skip the processing of empty models
//
//					}
//				}
//				elseif ( (int)$pkVal == 0)
//				{
//					// Assumes values are populated.
//					// @todo assert values are valid
//					// create the model
//					$crudScenario= 'update';
//					$modelToSave = new $model();
//
//				}
//				$runValidation = true;
//				$modelToSave->setAttributes($mtfva[$model]);
//				if ($modelToSave->save($runValidation) ){
//					$model_filter[$model] = ['crudScenario'=>$crudScenario, 'saved'=>true];
//				} else {
//					$model_filter[$model] = ['crudScenario'=>$crudScenario, 'saved'=>false];
//				}
//
//			}
//		}

//		return $modelFilter;
//		if(!$runValidation || $this->validate($attributes))
//			return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
//		else
//			return false;
	}

	/**
	 * Saves a specific model the view is composed of.
	 *
	 * The record is inserted as a row into the database table if its {@link isNewRecord}
	 * property is true (usually the case when the record is created using the 'new'
	 * operator). Otherwise, it will be used to update the corresponding row in the table
	 * (usually the case if the record is obtained using one of those 'find' methods.)
	 *
	 * Validation will be performed before saving the record. If the validation fails,
	 * the record will not be saved. You can call {@link getErrors()} to retrieve the
	 * validation errors.
	 *
	 * If the record is saved via insertion, its {@link isNewRecord} property will be
	 * set false, and its {@link scenario} property will be set to be 'update'.
	 * And if its primary key is auto-incremental and is not set before insertion,
	 * the primary key will be populated with the automatically generated key value.
	 *
	 * @param string | array $models
	 * @param boolean $runValidation whether to perform validation before saving the record.
	 * If the validation fails, the record will not be saved to database.
	 * @param array $attributes list of attributes that need to be saved. Defaults to null,
	 * meaning all attributes that are loaded from DB will be saved.
	 * @return boolean whether the saving succeeds
	 */

	public function saveModels_v1($models='all', $runValidation=true,$attributes=null)
	{
		$filter = null;
		$hasFatalError = $this->hasFatalError;
		$localDebug = true;
		// <editor-fold defaultstate="collapsed" desc="parse attributes param">
		if (! is_null($attributes)){
			// filter specific attributes
			if (is_array($attributes) && count($attributes) > 0){
				// loop
				foreach ($attributes as $attribute) {
					$filter[] = $attribute;
				}
			} elseif (is_string($attributes) && strlen($attributes) > 0){
				// filter items passed in
				if (stripos($attributes, ',') !== false){
					// parse string
					$fields = explode(',', $attributes);
					foreach ($fields as $field){
						$filter[] = $field;
					}
				} else {
					// its a non-delimited string
					$filter[] = $attributes;
				}
			}
		} else { }
		// </editor-fold>

		// @todo Process all the models in sequence
		// @todo Carry keys forward like store chain
		// @todo Determine ins|upd like StoreGeneric()?
		// if all the attributes are empty then return false
		$valCounts = array_count_values($attributes);
		$valKeys   = array_keys($valCounts);
		if (count($valCounts) == 1){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($valCounts, 10, true);
			return false;
		} elseif (count($valKeys) == 2){
			if (isset($valKeys[''])  && $valKeys[''] > 0){
				$valEmptyCount = $valKeys['']; // @todo cast as int
			} else {
				$valEmptyCount = 0;
			}
			// if $valKeys array contains an key = '' then a count for empty values exists
			$valEmptyCount = ( ( array_key_exists('',$valKeys) ) ?  (int)$valKeys[''] : 0  );
		}

		$vwAttr	= $this->attributes;  // Get this model's attribute values
		$mtfva	= self::generateModelValuesArray($vwAttr); // data envelope array
		$readOnlyModels = ['User','OrgType'];
		if ($models == 'all'){
			foreach ($mtfva as $model => $fva) { // mtfva = multi-table fva
				if(array_search($model, $readOnlyModels) !== false){
					continue; // skip this modele
				}
				$model_filter[] = $model;
			}
		} elseif(is_array($models)){
			$model_filter = $models;
		}

		foreach ($mtfva as $model => $fva) {
			if(in_array($model, $model_filter)){
				// process model

				// next line assumes each model has a store() method and they do NOT ALL have it.
				// is the model's pkey OR fkey present?
				//$model::store($fva);
				if ($hasFatalError == true){
					continue;
				}
				if (count($fva) == 0){
					if ($localDebug){
						trace('Skipping empty model array '. $model , 'AthleteTeams->saveModels()');
					}
					continue; //  skip the processing of empty models
				}
				$referenceModel = new $model();
				$pk = $referenceModel->primaryKey(); // returns the field name of the primary key
				if(isset($mtfva[$model][$pk])){
					$pkVal = $mtfva[$model][$pk];
				} else {
					$pkVal = 0;
				}
				if ( (int)$pkVal > 0){
					$crudScenario= 'insert';
					$modelToSave = $model::model()->findByPk($pkVal);
					if ($model == 'OrgType' && isset($fva['org_type_name']) && !empty($fva['org_type_name'])){
						$org_type_name = $fva['org_type_name'];
						// check for dupes
						$dupeCheck = $model::model()->findByAttributes(['org_type_name'=>$org_type_name]);
						if (!is_null($dupeCheck)){
							if ($localDebug){
								trace('Skipping duplicate '. $model , 'AthleteTeams->saveModels()');
							}
							continue; //  skip the processing of duplicate models
						}

					//continue; //  skip the processing of empty models

					}
					if ($model == 'TeamOrgType' && isset($fva['team_org_org_type_name']) && !empty($fva['team_org_org_type_name'])){
						$org_type_name = $fva['org_type_name'];
						// check for dupes
						$dupeCheck = OrgType::model()->findByAttributes(['org_type_name'=>$org_type_name]);
						if (!is_null($dupeCheck)){
							if ($localDebug){
								trace('Skipping duplicate '. $model , 'AthleteTeams->saveModels()');
							}
							continue; //  skip the processing of duplicate models
						}


					//continue; //  skip the processing of empty models

					}
					if ($model == 'TeamOrg' && isset($fva['team_org_org_name']) && !empty($fva['team_org_org_name'])){
						$org_name = $fva['team_org_org_name'];
						// check for dupes
						if (isset($mtfva['Team']['team_id']) && (int)$mtfva['Team']['team_id'] > 0){
							// Team row exists
						}

						$dupeCheck = Org::model()->findByAttributes(['org_name'=>$org_name]);
						if (!is_null($dupeCheck)){
							if ($localDebug){
								trace('Skipping duplicate '. $model , 'AthleteTeams->saveModels()');
							}
							continue; //  skip the processing of duplicate models
						}

					//continue; //  skip the processing of empty models

					}
				}
				elseif ( (int)$pkVal == 0)
				{
					// Assumes values are populated.
					// @todo assert values are valid
					// create the model
					$crudScenario= 'update';
					$modelToSave = new $model();

				}
				$runValidation = true;
				$modelToSave->setAttributes($mtfva[$model]);
				if ($modelToSave->save($runValidation) ){
					$model_filter[$model] = ['crudScenario'=>$crudScenario, 'saved'=>true];
				} else {
					$model_filter[$model] = ['crudScenario'=>$crudScenario, 'saved'=>false];
				}

			}
		}
		return $model_filter;
//		if(!$runValidation || $this->validate($attributes))
//			return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
//		else
//			return false;
	}


	/**
	 *
	 * @param type $parentID
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchIncludingTeam($parentID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        //$criteria->with=array('team');
        //$criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.team_id',$parentID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort;
        $sort->attributes = array(
            'person_name_full' => array(
            'asc' => 'person_name_full',
            'desc' => 'person_name_full DESC',
            ), '*', /* Treat all other columns normally */
        );
        /* End: Sort on related Model's columns */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <danabyrd@byrdbrain.com>
	 */
	public static function searchBySelect2($search = null, $id = null, $raw = null, $limit=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $pkName as id, $textColumn as `text` "
				 . "from $tableName "
				 . "where $textColumn LIKE :$textColumn "
				 . "order by $textColumn";
			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%'];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchTypeBySelect2($search = null, $id = null, $raw = null, $limit=null, $typeName=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$typeTable  = $tableName . '_type';
		$fkeyName   = $tableName . '_type_id';

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $tableName.$pkName as id, $tableName.$textColumn as `text` "
				 . "from $tableName inner join $typeTable on $tableName.$fkeyName = $typeTable.$fkeyName "
				 . "where $typeTable." .$typeTable . "_name = :typeName and $tableName.$textColumn LIKE :$textColumn "
				 . "order by $tableName.$textColumn";

			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%', ':typeName'=>$typeName];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'person_name_full' => Yii::t('app', 'Coach Name Full'),
                'person_age' => Yii::t('app', 'Person Age'),
                'team_name' => Yii::t('app', 'Team Name'),
                'age_group_name' => Yii::t('app', 'Age Group Name'),
                'person_city' => Yii::t('app', 'Person City'),
                'person_state_or_region' => Yii::t('app', 'Person State Or Region'),
                'gsm_staff' => Yii::t('app', 'Gsm Staff'),
                'coach_id' => Yii::t('app', 'Coach'),
                'coach_person_id' => Yii::t('app', 'Coach Person'),
                'coach_age_group_id' => Yii::t('app', 'Coach Age Group'),
                'coach_team_coach_id' => Yii::t('app', 'Coach Team Coach'),
                'coach_specialty' => Yii::t('app', 'Coach Specialty'),
                'coach_certifications' => Yii::t('app', 'Coach Certifications'),
                'coach_qrcode_uri' => Yii::t('app', 'Coach Qrcode Uri'),
                'coach_info_source_scrape_url' => Yii::t('app', 'Coach Info Source Scrape Url'),
                'person_id' => Yii::t('app', 'Person'),
                'person_org_id' => Yii::t('app', 'Person Org'),
                'person_app_user_id' => Yii::t('app', 'Person App User'),
                'person_person_type_id' => Yii::t('app', 'Person Person Type'),
                'person_user_id' => Yii::t('app', 'Person User'),
                'person_name_prefix' => Yii::t('app', 'Person Name Prefix'),
                'person_name_first' => Yii::t('app', 'Person Name First'),
                'person_name_middle' => Yii::t('app', 'Person Name Middle'),
                'person_name_last' => Yii::t('app', 'Person Name Last'),
                'person_name_suffix' => Yii::t('app', 'Person Name Suffix'),
                'person_phone_personal' => Yii::t('app', 'Person Phone Personal'),
                'person_email_personal' => Yii::t('app', 'Person Email Personal'),
                'person_phone_work' => Yii::t('app', 'Person Phone Work'),
                'person_email_work' => Yii::t('app', 'Person Email Work'),
                'person_position_work' => Yii::t('app', 'Person Position Work'),
                'person_gender_id' => Yii::t('app', 'Person Gender'),
                'person_image_headshot_url' => Yii::t('app', 'Person Image Headshot Url'),
                'person_name_nickname' => Yii::t('app', 'Person Name Nickname'),
                'person_date_of_birth' => Yii::t('app', 'Person Date Of Birth'),
                'person_dob_eng' => Yii::t('app', 'Person Dob Eng'),
                'person_bday_long' => Yii::t('app', 'Person Bday Long'),
                'person_height' => Yii::t('app', 'Person Height'),
                'person_height_imperial' => Yii::t('app', 'Person Height Imperial'),
                'person_weight' => Yii::t('app', 'Person Weight'),
                'person_bmi' => Yii::t('app', 'Person Bmi'),
                'person_tshirt_size' => Yii::t('app', 'Person Tshirt Size'),
                'person_addr_1' => Yii::t('app', 'Person Addr 1'),
                'person_addr_2' => Yii::t('app', 'Person Addr 2'),
                'person_addr_3' => Yii::t('app', 'Person Addr 3'),
                'person_postal_code' => Yii::t('app', 'Person Postal Code'),
                'person_country' => Yii::t('app', 'Person Country'),
                'person_country_code' => Yii::t('app', 'Person Country Code'),
                'person_profile_url' => Yii::t('app', 'Person Profile Url'),
                'person_profile_uri' => Yii::t('app', 'Person Profile Uri'),
                'person_high_school__graduation_year' => Yii::t('app', 'Person High School Graduation Year'),
                'person_college_graduation_year' => Yii::t('app', 'Person College Graduation Year'),
                'person_college_commitment_status' => Yii::t('app', 'Person College Commitment Status'),
                'person_type_id' => Yii::t('app', 'Person Type'),
                'person_type_name' => Yii::t('app', 'Person Type Name'),
                'person_type_desc_short' => Yii::t('app', 'Person Type Desc Short'),
                'person_type_desc_long' => Yii::t('app', 'Person Type Desc Long'),
                'gender_id' => Yii::t('app', 'Gender'),
                'gender_desc' => Yii::t('app', 'Gender Desc'),
                'gender_code' => Yii::t('app', 'Gender Code'),
                'org_id' => Yii::t('app', 'Org'),
                'org_org_type_id' => Yii::t('app', 'Org Org Type'),
                'org_org_level_id' => Yii::t('app', 'Org Org Level'),
                'org_name' => Yii::t('app', 'Org Name'),
                'org_type_name' => Yii::t('app', 'Org Type Name'),
                'org_website_url' => Yii::t('app', 'Org Website Url'),
                'org_twitter_url' => Yii::t('app', 'Org Twitter Url'),
                'org_facebook_url' => Yii::t('app', 'Org Facebook Url'),
                'org_phone_main' => Yii::t('app', 'Org Phone Main'),
                'org_email_main' => Yii::t('app', 'Org Email Main'),
                'org_addr1' => Yii::t('app', 'Org Addr1'),
                'org_addr2' => Yii::t('app', 'Org Addr2'),
                'org_addr3' => Yii::t('app', 'Org Addr3'),
                'org_city' => Yii::t('app', 'Org City'),
                'org_state_or_region' => Yii::t('app', 'Org State Or Region'),
                'org_postal_code' => Yii::t('app', 'Org Postal Code'),
                'org_country_code_iso3' => Yii::t('app', 'Org Country Code Iso3'),
                'team_org_org_type_id' => Yii::t('app', 'Team Org Org Type'),
                'team_org_org_type_name' => Yii::t('app', 'Team Org Org Type Name'),
                'team_org_basetable_org_type_id' => Yii::t('app', 'Team Org Basetable Org Type'),
                'team_org_basetable_org_type_name' => Yii::t('app', 'Team Org Basetable Org Type Name'),
                'team_org_org_id' => Yii::t('app', 'Team Org Org'),
                'team_org_org_level_id' => Yii::t('app', 'Team Org Org Level'),
                'team_org_org_level_name' => Yii::t('app', 'Team Org Org Level Name'),
                'team_org_basetable_org_level_id' => Yii::t('app', 'Team Org Basetable Org Level'),
                'team_org_basetable_org_level_name' => Yii::t('app', 'Team Org Basetable Org Level Name'),
                'team_org_org_name' => Yii::t('app', 'Team Org Org Name'),
                'team_org_org_website_url' => Yii::t('app', 'Team Org Org Website Url'),
                'team_org_org_twitter_url' => Yii::t('app', 'Team Org Org Twitter Url'),
                'team_org_org_facebook_url' => Yii::t('app', 'Team Org Org Facebook Url'),
                'team_org_org_phone_main' => Yii::t('app', 'Team Org Org Phone Main'),
                'team_org_org_email_main' => Yii::t('app', 'Team Org Org Email Main'),
                'team_org_org_addr1' => Yii::t('app', 'Team Org Org Addr1'),
                'team_org_org_addr2' => Yii::t('app', 'Team Org Org Addr2'),
                'team_org_org_addr3' => Yii::t('app', 'Team Org Org Addr3'),
                'team_org_org_city' => Yii::t('app', 'Team Org Org City'),
                'team_org_org_state_or_region' => Yii::t('app', 'Team Org Org State Or Region'),
                'team_org_org_postal_code' => Yii::t('app', 'Team Org Org Postal Code'),
                'team_org_org_country_code_iso3' => Yii::t('app', 'Team Org Org Country Code Iso3'),
                'team_org_org_governing_body' => Yii::t('app', 'Team Org Org Governing Body'),
                'team_org_org_ncaa_clearing_house_id' => Yii::t('app', 'Team Org Org Ncaa Clearing House'),
                'team_org_school_conference_id_main' => Yii::t('app', 'Team Org School Conference Id Main'),
                'team_org_school_conference_id_female' => Yii::t('app', 'Team Org School Conference Id Female'),
                'team_org_school_conference_id_male' => Yii::t('app', 'Team Org School Conference Id Male'),
                'team_org_school_school_ipeds_id' => Yii::t('app', 'Team Org School School Ipeds'),
                'team_org_school_conference_name' => Yii::t('app', 'Team Org School Conference Name'),
                'team_org_school_ncaa_division' => Yii::t('app', 'Team Org School Ncaa Division'),
                'team_org_school_conf_basetable_conference_id' => Yii::t('app', 'Team Org School Conf Basetable Conference'),
                'team_org_school_conf_basetable_conference_name' => Yii::t('app', 'Team Org School Conf Basetable Conference Name'),
                'team_org_school_conf_basetable_ncaa_division' => Yii::t('app', 'Team Org School Conf Basetable Ncaa Division'),
                'team_coach_id' => Yii::t('app', 'Team Coach'),
                'team_coach_team_id' => Yii::t('app', 'Team Coach Team'),
                'team_coach_coach_type_id' => Yii::t('app', 'Team Coach Coach Type'),
                'team_coach_primary_position_name' => Yii::t('app', 'Team Coach Primary Position Name'),
                'team_coach_begin_dt' => Yii::t('app', 'Team Coach Begin Dt'),
                'team_coach_end_dt' => Yii::t('app', 'Team Coach End Dt'),
                'team_coach_coach_id' => Yii::t('app', 'Team Coach Coach'),
                'team_coach_created_at' => Yii::t('app', 'Team Coach Created Dt'),
                'team_coach_updated_at' => Yii::t('app', 'Team Coach Updated Dt'),
                'team_org_id' => Yii::t('app', 'Team Org'),
                'team_school_id' => Yii::t('app', 'Team School'),
                'team_sport_id' => Yii::t('app', 'Team Sport'),
                'team_sport_name' => Yii::t('app', 'Team Sport Name'),
                'team_camp_id' => Yii::t('app', 'Team Camp'),
                'team_gender_id' => Yii::t('app', 'Team Gender'),
                'team_gender' => Yii::t('app', 'Team Gender'),
                'team_age_group_id' => Yii::t('app', 'Team Age Group'),
                'team_age_group' => Yii::t('app', 'Team Age Group'),
                'team_competition_season_id' => Yii::t('app', 'Team Competition Season'),
                'team_competition_season' => Yii::t('app', 'Team Competition Season'),
                'team_league_id' => Yii::t('app', 'Team League'),
                'team_league' => Yii::t('app', 'Team League'),
                'team_league_name' => Yii::t('app', 'Team League Name'),
                'organizational_level' => Yii::t('app', 'Organizational Level'),
                'team_governing_body' => Yii::t('app', 'Team Governing Body'),
                'team_website_url' => Yii::t('app', 'Team Website Url'),
                'team_schedule_url' => Yii::t('app', 'Team Schedule Url'),
                'team_schedule_uri' => Yii::t('app', 'Team Schedule Uri'),
                'team_statistical_highlights' => Yii::t('app', 'Team Statistical Highlights'),
                'team_team_division_id' => Yii::t('app', 'Team Team Division'),
                'team_division' => Yii::t('app', 'Team Division'),
                'team_division_name' => Yii::t('app', 'Team Division Name'),
                'team_city' => Yii::t('app', 'Team City'),
                'team_state_id' => Yii::t('app', 'Team State'),
                'team_country_id' => Yii::t('app', 'Team Country'),
                'team_wins' => Yii::t('app', 'Team Wins'),
                'team_losses' => Yii::t('app', 'Team Losses'),
                'team_draws' => Yii::t('app', 'Team Draws'),
                'team_wins_losses_draws' => Yii::t('app', 'Team Wins Losses Draws'),
                'team_basetable_team_league_id' => Yii::t('app', 'Team Basetable Team League'),
                'team_basetable_team_league_name' => Yii::t('app', 'Team Basetable Team League Name'),
                'team_basetable_team_division_id' => Yii::t('app', 'Team Basetable Team Division'),
                'team_basetable_team_division_name' => Yii::t('app', 'Team Basetable Team Division Name'),
        );
    }

	/**
	 * Override the base class method
	 * @return string
	 */
    public static function representingColumn() {
        return 'person_name_full';
    }

	/**
	 * Used to build-view an xlation between related models and thier relation name
	 * Allows a developer to 'see' (eyeball) the relations between models
	 * @version 0.1
	 * @since app v0.54.2
	 *
	 */
	public static function configUtility_RelationsByModelName() {
		$relationsTree          = self::relationsTree();
		$componentModels        = array_keys($relationsTree);

		$mRelations             = BaseModel::fetchModelRelationsMultiple($componentModels);
		$mRelationsByModelNames = BaseModel::relationsArrayToModelsArray($mRelations);
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($mRelationsByModelNames, 10, true);
		foreach($mRelationsByModelNames as $key=>$relations){
			echo $key = $relations . "<br>";

		}
		echo "complete <br>";
	}


	/**
	 * The order of models to save
	 * @param string $treeName 'Org-Person'|'Org-Team'
	 * @return array A hiearchy of relations
	 * @internal pulled source from self:relationsTree()
	 * @todo loop through $modelSequence and Populate
	 */
	protected static function saveModels_relationsTree($treeName='OrgPerson') {
		// <editor-fold defaultstate="collapsed" desc="v0 modelSequence">
//		$modelSequence=[
//			'User'         =>[],
//			'OrgType'      =>[],
//			'OrgLevel'     =>[],
//			'Org'          =>[],
//			//'PersonType', // is read-only
//			'Person'       =>[],
//			'Player'       =>[],
//			'SportPosition'=>[],
//			'TeamLeague'   =>[],
//			'TeamDivision'  =>[],
//			'Team'         =>[],
//			'TeamPlayer'   =>[],
//		];
		// </editor-fold>

		// Validate params
		$validParams=['OrgPerson','OrgTeam'];
		$useStrictSearch = true;
		if (! (array_search((string)$treeName,$validParams, $useStrictSearch) !== false) ){
			throw new CException('Parameter value is disallowed. Invalid request.',707, null);
		}

		// <editor-fold defaultstate="collapsed" desc="v01 baseline">


		$modelSequence_v01_baseline=[
			'OrgPerson'=>[
				'User'         =>[],
				'OrgType'      =>[],
				'OrgLevel'     =>[],
				'Org'          =>[],
				'Conference'   =>[],
				'School'	   =>[],
				//'PersonType', // is read-only
				'Person'       =>[],
				'Player'       =>[],
				'SportPosition'=>[],
				'TeamLeague'   =>[],
				'TeamDivision' =>[],
				'Team'         =>[],
				'TeamPlayer'   =>[],
			],
			'OrgTeam'=>[
				'OrgType'      =>[],
				'OrgLevel'     =>[],
				'Org'          =>[],
				'TeamLeague'   =>[],
				'TeamDivision' =>[],
				'Team'         =>[],
				'TeamPlayer'   =>[],
			],
		];
		// </editor-fold>

		// Use the view joins in reverse order for the two primary trees
		// Org-Person, Org-Team
		$modelSequence=[
			'OrgPerson'=>[
				'User'         =>[],
				'OrgType'      =>[],
				'OrgLevel'     =>[],
				'Org'          =>[],
				'Conference'   =>[],
				'School'	   =>[],
				//'PersonType', // is read-only
				'Person'       =>[],
				'Player'       =>[],
				'SportPosition'=>[],
				'TeamLeague'   =>[],
				'TeamDivision' =>[],
				'Team'         =>[],
				'TeamPlayer'   =>[],
			],
			'OrgTeam'=>[
				'OrgType'      =>[],
				'OrgLevel'     =>[],
				'Org'          =>[],
				'TeamLeague'   =>[],
				'TeamDivision'  =>[],
				'Team'         =>[],
				'TeamPlayer'   =>[],
			],
		];

		//return $modelSequence;
		return $modelSequence[$treeName];
	}

	/**
	 * The order of models to save
	 * @param string $treeName 'OrgPerson'|'OrgTeam'|'All'
	 * @return array A hiearchy of relations
	 * @version 1.1
	 */
	protected static function relationsTree($treeName='OrgPerson') {
		// <editor-fold defaultstate="collapsed" desc="v0 modelSequence">
//		$modelSequence=[
//			'User'         =>[],
//			'OrgType'      =>[],
//			'OrgLevel'     =>[],
//			'Org'          =>[],
//			//'PersonType', // is read-only
//			'Person'       =>[],
//			'Player'       =>[],
//			'SportPosition'=>[],
//			'TeamLeague'   =>[],
//			'TeamDivision'  =>[],
//			'Team'         =>[],
//			'TeamPlayer'   =>[],
//		];
		// </editor-fold>

		// Validate params
		$validParams=['OrgPerson','OrgTeam','All'];
		$useStrictSearch = true;
		if (! (array_search((string)$treeName,$validParams, $useStrictSearch) !== false) ){
			throw new CException('Parameter value is disallowed. Invalid request.',707, null);
		}

		// Use the view joins in reverse order for the two primary trees
		// Org-Person, Org-Team
		$modelSequence=[
			'OrgPerson'=>[
				'User'         =>['isReadOnly'=>true,],
				'OrgType'      =>[],
				'OrgLevel'     =>[],
				'Org'          =>[],
				'Conference'   =>[],
				'School'	   =>[],
				'PersonType'   =>['isReadOnly'=>true,],
				'Person'       =>[],
				'Player'       =>[],
				'SportPosition'=>[],
				'TeamLeague'   =>[],
				'TeamDivision' =>[],
				'Team'         =>[],
				'TeamPlayer'   =>[],
			],
			'OrgTeam'=>[
				'OrgType'      =>[],
				'OrgLevel'     =>[],
				// TeamOrg is a copy of the Org model
				'TeamOrg'      =>[],
				'TeamLeague'   =>[],
				'TeamDivision' =>[],
				'Team'         =>[],
				'TeamPlayer'   =>[],
			],
		];

		//return $modelSequence;
		//return $modelSequence[$treeName];

		// implement the ALL tree names sequence return
		if($treeName === 'All'){
			return $modelSequence;
		} else {
			return $modelSequence[$treeName];
		}


	}



	/**
	 * models=[]
	 *
	 */
	protected function storeChainConfig() {

		$chainConfig = [
			'bound__model_name'		=>'Player', // used to map the value of $_POST['pk']
			'submit_widget_class'	=>'TbEditableField',
			'submit_widget_type'	=>'select2',
			'submit_widget_handler'	=>'storeChain',
			'submit_widget_usecase'	=>'numeric_key', // text_value | numeric_key

			'field_values_array'=>[
				'models'=>[
					'OrgType'=>[],
					'OrgLevel'=>[],
					'Team'=>[
						'team_name'			=>'{{insert_text}}',
						'team_id'			=>'{{value}}',
						'age_group_id'		=>['{{fetch_value}}'=>
							['select'			=>'age_group_id',
								'from'			=>'player',
								'where'		=>'player_id = '.$data['player']['player_id']
							],
						],
						'org_id'			=>['{{fetch_value}}'=>
							['select'			=>'org_id',
								'from'			=>'person',
								'where'		=>'person_id = '.$data['player']['person_id']
							],
						],
						'sport_id'			=> 20,	// soccer
					],
					'TeamPlayer'=>[
						'team_player_id'=>['{{fetch_value}}'=>
							['select'		=>'team_player_id',
								'from'		=>'team_player',
								'where'		=>['and',
									'player_id = '.$data['player']['player_id'],
									//'team_id = {{value}}'],
									'team_id = {{shared_attribute}}'],
							],
						],
						'team_id'		=>'{{shared_attribute}}',
						'player_id'		=>$data['player']['player_id'],

					],
					'Player'=>[
						'player_id'				=>$data['player']['player_id'],
						'player_team_player_id'	=>['{{shared_attribute}}'=>[
							'model_name'		=>'TeamPlayer',
							'model_attribute'	=>'team_player_id',
							],
						],
					],
				],
			],
			'list_source'=>[
				'model_name'=>'Team',
				'text'		=>'team_name',
				'value'		=>'team_id',
				//'condition'	=>'org_type_id=2',
				'order'		=>'team_name'
			],
			'data write plan'=>[
				'Team',
				'TeamPlayer',
			],
		];


	}
	/**
	 * models=[]
	 *
	 */
	protected function storeChainConfig_v0() {


		$chainConfig = [
			'bound__model_name'		=>'Player', // used to map the value of $_POST['pk']
			'submit_widget_class'	=>'TbEditableField',
			'submit_widget_type'	=>'select2',
			'submit_widget_handler'	=>'storeChain',
			'submit_widget_usecase'	=>'numeric_key', // text_value | numeric_key

			'field_values_array'=>[
				'models'=>[
					'Team'=>[
						'team_name'			=>'{{insert_text}}',
						'team_id'			=>'{{value}}',
						'age_group_id'		=>['{{fetch_value}}'=>
							['select'			=>'age_group_id',
								'from'			=>'player',
								'where'		=>'player_id = '.$data['player']['player_id']
							],
						],
						'org_id'			=>['{{fetch_value}}'=>
							['select'			=>'org_id',
								'from'			=>'person',
								'where'		=>'person_id = '.$data['player']['person_id']
							],
						],
						'sport_id'			=> 20,	// soccer
					],
					'TeamPlayer'=>[
						'team_player_id'=>['{{fetch_value}}'=>
							['select'		=>'team_player_id',
								'from'		=>'team_player',
								'where'		=>['and',
									'player_id = '.$data['player']['player_id'],
									//'team_id = {{value}}'],
									'team_id = {{shared_attribute}}'],
							],
						],
						'team_id'		=>'{{shared_attribute}}',
						'player_id'		=>$data['player']['player_id'],

					],
					'Player'=>[
						'player_id'				=>$data['player']['player_id'],
						'player_team_player_id'	=>['{{shared_attribute}}'=>[
							'model_name'		=>'TeamPlayer',
							'model_attribute'	=>'team_player_id',
							],
						],
					],
				],
			],
			'list_source'=>[
				'model_name'=>'Team',
				'text'		=>'team_name',
				'value'		=>'team_id',
				//'condition'	=>'org_type_id=2',
				'order'		=>'team_name'
			],
			'data write plan'=>[
				'Team',
				'TeamPlayer',
			],
		];


	}

	public function get_scenarioCrud() {
		return $this->_scenarioCrud;

	}

	/**
	 * Sets a scenario for asynch ajax transactions and post backs
	 * @param type $scenario
	 * @return string
	 * @internal Development Status = ready for testing (rft)
	 * @todo wire up the caller saveModels
	 */
	public function setScenarioCrud($scenario, $requiredValues=[]) {
		$validScenarioParameters=['create','read','update','delete'];
		$useStrictSearch=true;
		if (! (array_search((string)$scenario,$validScenarioParameters, $useStrictSearch) !== false) ){
			throw new CHttpException(400, 'Scenario disallowed. Invalid request. Please do not repeat this request again.');
		}
		$scenarioCrudPrevious = $this->_scenarioCrud;
		//$scenarioSessionKey = $this->scenarioSessionKeyCrud;
		$scenarioSessionKey = self::$scenarioSessionKeyCrud;
		$udate = Bdate::udate();
		$field_value_array=[
			$scenarioSessionKey=>[
				'scenario'=>$scenario,
				'utime'   =>$udate,
				'scenarioPrevious'=>$scenarioCrudPrevious,
				'requiredValues'  => $requiredValues,
			]
		];
		$this->_scenarioCrud = $scenario;
		BaseModel::sessionStore($field_value_array);
	}

	/**
	 * Sets a scenario for asynch ajax transactions and post backs
	 * @param type $scenario
	 * @return string
	 * @internal Development Status = ready for testing (rft)
	 * @todo wire up the caller saveModels
	 */
	public function setScenarioRender($scenario, $requiredValues=[]) {
		$validScenarioParameters=['create','read','update','delete'];
		$useStrictSearch=true;
		if ( !(array_search((string)$scenario,$validScenarioParameters, $useStrictSearch) !== false) ){
			throw new CHttpException(400, 'Scenario disallowed. Invalid request. Please do not repeat this request again.');
		}
		$scenarioCrudPrevious = $this->_scenarioRender;
		//$scenarioSessionKey = $this->scenarioSessionKeyRender;
		$scenarioSessionKey = self::$scenarioSessionKeyRender;
		$udate = Bdate::udate();
		$field_value_array=[
			$scenarioSessionKey=>[
				'scenario'=>$scenario,
				'utime'   =>$udate,
				'scenarioPrevious'=>$scenarioCrudPrevious,
				'requiredValues'  => $requiredValues,
			]
		];
		$this->_scenarioRender = $scenario;
		BaseModel::sessionStore($field_value_array);
	}

	/**
	 *
	 * @return array
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function getScenarioCrud($returnArray=false) {
		// @todo if (key) in session memory, then return the session value
		$session = Yii::app()->session;
		//$scenarioSessionKey = $this->scenarioSessionKeyCrud;
		$scenarioSessionKey = self::$scenarioSessionKeyCrud;
		if (isset($session[$scenarioSessionKey])){
			$sessionVals = $session[$scenarioSessionKey];
		} else {
			$sessionVals = [];
		}

		if ($returnArray==true){
			return $sessionVals;
		} else {
			if (isset($sessionVals['scenario'])){
				return $sessionVals['scenario'];
			}else{
				return 'read'; // default scenario
			}
			//$flattenedScmda = self::storeMtvaCompositeScmda($modelTreeName, $out);
			// PHP best practice after any foreach
			unset($modelName, $modelSettings);
		}
	}


	/**
	 *
	 * @return array
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function getScenarioRender($returnArray=false) {
		// if (key) in session memory, then return the session value
		$session = Yii::app()->session;
		//$scenarioSessionKey = $this->scenarioSessionKeyRender;
		$scenarioSessionKey = self::$scenarioSessionKeyRender;
		if (isset($session[$scenarioSessionKey])){
			$sessionVals = $session[$scenarioSessionKey];
		} else {
			$sessionVals = [];
		}

		if ($returnArray==true){
			return $sessionVals;
		} else {
			if (isset($sessionVals['scenario'])){
				return $sessionVals['scenario'];
			}else{
				return 'read'; // default scenario
			}
		}
	}


	/**
	 *
	 * @param type $playerID
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchIncludingPlayer($playerID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        //$criteria->with=array('team');
        //$criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.player_id',$playerID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort();
//        $sort->attributes = array(
//            'team_name' => array(
//            'asc' => 'team_name',
//            'desc' => 'team_name DESC',
//            ), '*', /* Treat all other columns normally */
//        );

//        $sort->attributes = array(
//            'team_org_org_type_name' => array(
//				'asc' => 'team_org_org_type_name',
//				'desc' => 'team_org_org_type_name DESC',
//
//            ),
//            'team_org_org_name' => array(
//				'asc' => 'team_org_org_name',
//				'desc' => 'team_org_org_name DESC',
//
//            ),
//            'team_name' => array(
//				'asc' => 'team_name',
//				'desc' => 'team_name DESC',
//			),
//			'*', /* Treat all other columns normally */
//        );
		// multiSort
		// @see http://www.yiiframework.com/doc/api/1.1/CSort#attributes-detail
		$sort->multiSort = true;
        $sort->attributes = [
			'team'=>[
				'asc' =>'team_org_org_type_name, team_org_org_name, team_name',
				'desc'=>'team_org_org_type_name DESC, team_org_org_name DESC, team_name DESC',
				'label'=>'Team',
				'default'=>'asc',
			]
		];
//		$badExample = [
//            'team_org_org_type_name',
//            'team_org_org_name',
//            'team_name'
//        ];
        /* End: Sort on related Model's columns */


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }

	/**
	 * Use an CArrayDataProvider to provide a default sort
	 * @see http://www.yiiframework.com/doc/api/1.1/CArrayDataProvider
	 * @internal This totally didn't work... :(
	 */
	public function sortTest(){
		$rawData=Yii::app()->db->createCommand('SELECT * FROM vwPlayer')->queryAll($fetchAssociative=true);
		// or using: $rawData=User::model()->findAll();
		$dataProvider=new CArrayDataProvider($rawData, array(
			'id'=>'team_player_id',
			'sort'=>array(
				'attributes'=>array(
		            'team_org_org_type_name',
		            'team_org_org_name',
		            'team_name'
				),
			),
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
		// $dataProvider->getData() will return a list of arrays.
		return $dataProvider;
	}

	/**
	 * Override the BaseAthleteTeams rules so the generic rules can be regenerated
	 * at any time without overwriting the GSM Custom Business Rules
	 * @return array Business rules for Athlete Teams
	 *
	 */
    public function rules() {
        return array(
            array(	'person_type_name, team_org_org_name, team_name',
					'required',
					'message' => Yii::t('app', 'Field is required')
			),
            array(	'player_id, person_id, player_age_group_id, player_team_player_id, player_sport_position_id, player_sport_position2_id, org_id, app_user_id, person_weight, person_high_school__graduation_year, person_college_graduation_year, person_type_id, gender_id, org_type_id, org_level_id, team_org_org_type_id, team_org_basetable_org_type_id, team_org_org_level_id, team_org_basetable_org_level_id, team_org_school_conference_id_main, team_org_school_conference_id_female, team_org_school_conference_id_male, team_org_school_school_ipeds_id, team_org_school_conf_basetable_conference_id, team_player_id, team_id, team_play_sport_position_id, team_play_sport_position2_id, team_play_coach_id, team_play_current_team, team_org_id, team_school_id, team_sport_id, team_camp_id, team_gender_id, team_age_group_id, team_competition_season_id, team_league_id, team_division_id, team_state_id, team_country_id, team_wins, team_losses, team_draws, team_basetable_team_league_id, team_basetable_team_division_id',
					'numerical',
					'integerOnly'=>true
			),
            array(	'person_age, person_bmi',
					'numerical'
			),
            array(	'person_name_full, person_profile_url, team_website_url',
					'length',
					'max'=>90,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'team_name, person_city, org_email_main, org_city, team_org_org_level_name, team_org_basetable_org_level_name, team_org_org_email_main, team_org_org_city, team_org_school_conference_name, team_org_school_conf_basetable_conference_name, team_league, team_schedule_url, team_division_name, team_city, team_basetable_team_division_name',
					'length',
					'max'=>75,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'age_group_name, person_state_or_region, player_sport_position_name, player_access_code, player_sport_position_preference, person_name_first, person_name_middle, person_name_last, person_name_suffix, person_phone_personal, person_phone_work, person_position_work, person_name_nickname, person_height_imperial, person_country, person_type_name, person_type_desc_short, person_type_desc_long, org_type_name, org_state_or_region, team_org_org_type_name, team_org_basetable_org_type_name, team_org_org_state_or_region, team_org_org_governing_body, team_play_sport_position_name, team_play_sport_position2_name, team_play_primary_position_flattext, team_play_primary_position, team_play_coach_name, team_sport_name, team_age_group, team_league_name, organizational_level, team_governing_body, team_basetable_team_league_name',
					'length',
					'max'=>45,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'gsm_staff',
					'length',
					'max'=>65,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'player_parent_email, person_email_personal, person_email_work, person_profile_uri, team_competition_season, team_schedule_uri',
					'length',
					'max'=>60,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'player_sport_preference, team_wins_losses_draws',
					'length',
					'max'=>35,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'player_shot_side_preference, person_name_prefix, person_postal_code, person_college_commitment_status, org_phone_main, org_postal_code, team_org_org_phone_main, team_org_org_postal_code, team_org_org_ncaa_clearing_house_id',
					'length',
					'max'=>25,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'player_dominant_side, player_dominant_foot, person_height, gender_code, team_org_school_ncaa_division, team_org_school_conf_basetable_ncaa_division',
					'length',
					'max'=>5,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'player_statistical_highlights, team_play_statistical_highlights, team_statistical_highlights',
					'length',
					'max'=>300,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'person_image_headshot_url, person_addr_1, person_addr_2, person_addr_3, org_name, org_addr1, org_addr2, org_addr3, team_org_org_name, team_org_org_addr1, team_org_org_addr2, team_org_org_addr3',
					'length',
					'max'=>100,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'person_dob_eng, person_tshirt_size, team_division',
					'length',
					'max'=>10,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'person_bday_long',
					'length',
					'max'=>69,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'person_country_code, org_country_code_iso3, team_org_org_country_code_iso3',
					'length',
					'max'=>3,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'gender_desc',
					'length',
					'max'=>30,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'org_website_url, org_twitter_url, org_facebook_url, team_org_org_website_url, team_org_org_twitter_url, team_org_org_facebook_url',
					'length',
					'max'=>150,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'team_gender',
					'length',
					'max'=>1,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'player_waiver_minor_dt, player_waiver_adult_dt, person_date_of_birth, team_play_begin_dt, team_play_end_dt, team_play_created_at, team_play_updated_at',
					'safe'
			),
            array('person_name_full, person_age, team_name, age_group_name, person_city, person_state_or_region, gsm_staff, player_id, person_id, player_age_group_id, player_team_player_id, player_sport_position_id, player_sport_position2_id, player_sport_position_name, player_access_code, player_waiver_minor_dt, player_waiver_adult_dt, player_parent_email, player_sport_preference, player_sport_position_preference, player_shot_side_preference, player_dominant_side, player_dominant_foot, player_statistical_highlights, org_id, app_user_id, person_name_prefix, person_name_first, person_name_middle, person_name_last, person_name_suffix, person_phone_personal, person_email_personal, person_phone_work, person_email_work, person_position_work, person_image_headshot_url, person_name_nickname, person_date_of_birth, person_dob_eng, person_bday_long, person_height, person_height_imperial, person_weight, person_bmi, person_tshirt_size, person_addr_1, person_addr_2, person_addr_3, person_postal_code, person_country, person_country_code, person_profile_url, person_profile_uri, person_high_school__graduation_year, person_college_graduation_year, person_college_commitment_status, person_type_id, person_type_desc_short, person_type_desc_long, gender_id, gender_desc, gender_code, org_type_id, org_level_id, org_name, org_type_name, org_website_url, org_twitter_url, org_facebook_url, org_phone_main, org_email_main, org_addr1, org_addr2, org_addr3, org_city, org_state_or_region, org_postal_code, org_country_code_iso3, team_org_org_type_id, team_org_org_type_name, team_org_basetable_org_type_id, team_org_basetable_org_type_name, team_org_org_level_id, team_org_org_level_name, team_org_basetable_org_level_id, team_org_basetable_org_level_name, team_org_org_name, team_org_org_website_url, team_org_org_twitter_url, team_org_org_facebook_url, team_org_org_phone_main, team_org_org_email_main, team_org_org_addr1, team_org_org_addr2, team_org_org_addr3, team_org_org_city, team_org_org_state_or_region, team_org_org_postal_code, team_org_org_country_code_iso3, team_org_org_governing_body, team_org_org_ncaa_clearing_house_id, team_org_school_conference_id_main, team_org_school_conference_id_female, team_org_school_conference_id_male, team_org_school_school_ipeds_id, team_org_school_conference_name, team_org_school_ncaa_division, team_org_school_conf_basetable_conference_id, team_org_school_conf_basetable_conference_name, team_org_school_conf_basetable_ncaa_division, team_player_id, team_id, team_play_sport_position_id, team_play_sport_position2_id, team_play_sport_position_name, team_play_sport_position2_name, team_play_primary_position_flattext, team_play_primary_position, team_play_begin_dt, team_play_end_dt, team_play_coach_id, team_play_coach_name, team_play_current_team, team_play_statistical_highlights, team_play_created_at, team_play_updated_at, team_org_id, team_school_id, team_sport_id, team_sport_name, team_camp_id, team_gender_id, team_gender, team_age_group_id, team_age_group, team_competition_season_id, team_competition_season, team_league_id, team_league, team_league_name, organizational_level, team_governing_body, team_website_url, team_schedule_url, team_schedule_uri, team_statistical_highlights, team_division_id, team_division, team_division_name, team_city, team_state_id, team_country_id, team_wins, team_losses, team_draws, team_wins_losses_draws, team_basetable_team_league_id, team_basetable_team_league_name, team_basetable_team_division_id, team_basetable_team_division_name',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('person_name_full, person_age, team_name, age_group_name, person_city, person_state_or_region, gsm_staff, player_id, person_id, player_age_group_id, player_team_player_id, player_sport_position_id, player_sport_position2_id, player_sport_position_name, player_access_code, player_waiver_minor_dt, player_waiver_adult_dt, player_parent_email, player_sport_preference, player_sport_position_preference, player_shot_side_preference, player_dominant_side, player_dominant_foot, player_statistical_highlights, org_id, app_user_id, person_name_prefix, person_name_first, person_name_middle, person_name_last, person_name_suffix, person_phone_personal, person_email_personal, person_phone_work, person_email_work, person_position_work, person_image_headshot_url, person_name_nickname, person_date_of_birth, person_dob_eng, person_bday_long, person_height, person_height_imperial, person_weight, person_bmi, person_tshirt_size, person_addr_1, person_addr_2, person_addr_3, person_postal_code, person_country, person_country_code, person_profile_url, person_profile_uri, person_high_school__graduation_year, person_college_graduation_year, person_college_commitment_status, person_type_id, person_type_name, person_type_desc_short, person_type_desc_long, gender_id, gender_desc, gender_code, org_type_id, org_level_id, org_name, org_type_name, org_website_url, org_twitter_url, org_facebook_url, org_phone_main, org_email_main, org_addr1, org_addr2, org_addr3, org_city, org_state_or_region, org_postal_code, org_country_code_iso3, team_org_org_type_id, team_org_org_type_name, team_org_basetable_org_type_id, team_org_basetable_org_type_name, team_org_org_level_id, team_org_org_level_name, team_org_basetable_org_level_id, team_org_basetable_org_level_name, team_org_org_name, team_org_org_website_url, team_org_org_twitter_url, team_org_org_facebook_url, team_org_org_phone_main, team_org_org_email_main, team_org_org_addr1, team_org_org_addr2, team_org_org_addr3, team_org_org_city, team_org_org_state_or_region, team_org_org_postal_code, team_org_org_country_code_iso3, team_org_org_governing_body, team_org_org_ncaa_clearing_house_id, team_org_school_conference_id_main, team_org_school_conference_id_female, team_org_school_conference_id_male, team_org_school_school_ipeds_id, team_org_school_conference_name, team_org_school_ncaa_division, team_org_school_conf_basetable_conference_id, team_org_school_conf_basetable_conference_name, team_org_school_conf_basetable_ncaa_division, team_player_id, team_id, team_play_sport_position_id, team_play_sport_position2_id, team_play_sport_position_name, team_play_sport_position2_name, team_play_primary_position_flattext, team_play_primary_position, team_play_begin_dt, team_play_end_dt, team_play_coach_id, team_play_coach_name, team_play_current_team, team_play_statistical_highlights, team_play_created_at, team_play_updated_at, team_org_id, team_school_id, team_sport_id, team_sport_name, team_camp_id, team_gender_id, team_gender, team_age_group_id, team_age_group, team_competition_season_id, team_competition_season, team_league_id, team_league, team_league_name, organizational_level, team_governing_body, team_website_url, team_schedule_url, team_schedule_uri, team_statistical_highlights, team_division_id, team_division, team_division_name, team_city, team_state_id, team_country_id, team_wins, team_losses, team_draws, team_wins_losses_draws, team_basetable_team_league_id, team_basetable_team_league_name, team_basetable_team_division_id, team_basetable_team_division_name',
					'safe',
					'on'=>'search'
			),
			/* @see http://stackoverflow.com/questions/22859422/how-to-disable-a-field-on-update-in-yii */
			// After the scenario is set you can easily enforce the read-only logic with a couple of rules:
			// array('username', 'safe', 'except'=>'update'),
			// array('username', 'unsafe', 'on'=>'update'),

//			This is incorrect: <input id='username' readonly='true'>
//			This is correct: <input id='username' readonly='readonly'>
//			Therefore, change your code to
//
//			echo $form->textFieldRow($model,'username',array(
//					 'class'=>'span5',
//					 'maxlength'=>45,
//					 'readOnly'=>($model->scenario == 'update')? "readonly" : ""
//				 ));

//			array(
//
//			),

        );
    }

	/**
	 * Create a list of relations for every model in the AthleteTeams sql view
	 * @param bool $eSaveOnly =true Excludes any relation that is not eSaveRelatedBehavior compatible
	 * @internal Development Status = Golden!
	 * @example AthleteTeams::getRelationsAsSparseList();
	 * @uses self::getComponentModels() The source list of models to fetch relations info from
	 */
	public static function getRelationsAsSparseList($eSaveOnly=false){
		$componentModelsList = AthleteTeams::getComponentModels();
		// Assert the assumption that the models are related
		$mRelationsOfModelsToProcess = BaseModel::fetchModelRelationsMultiple($componentModelsList,$eSaveOnly);
		$mRelationsByModelNames      = BaseModel::relationsArrayToModelsArray($mRelationsOfModelsToProcess);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($mRelationsByModelNames, 10, true);
		//return;
		$mRelationsByModelNamesOut=$mRelationsByModelNames;
		$mRelationsByModelNamesOut2=[]; // build up the relations
		$relationsRemoved=[];
		$relationsRemovedDeep=[];

		$localDebug = false;
		$breakCount = 0;
		//$breakType=[];
		foreach($mRelationsByModelNames as $modelName=>$mRelationInfo){
			$relationsByModelName    = $mRelationInfo['byModelName'];
			//$relationsByRelationName = $mRelationInfo['byRelationName'];
			//$relationsByRelationType = $mRelationInfo['byRelationType'];

			if ($modelName == 'TeamOrg'){
				$x = "break here";
			}
			foreach($relationsByModelName as $relatedModelName=>$relationInfo){
				if(! in_array($relatedModelName,$componentModelsList)){
					$relationName = $relationInfo['relationName'];
					$relationType = $relationInfo['relationType'];

					if (isset($mRelationsByModelNamesOut[$modelName]['byModelName'][$relatedModelName])){
						unset($mRelationsByModelNamesOut[$modelName]['byModelName'][$relatedModelName]);
						$relationsRemovedDeep[$modelName]['byModelName'][]=$relatedModelName;
					} else {
						$x = "break here";
						$breakCount++;
					}
					if (isset($mRelationsByModelNamesOut[$modelName]['byRelationName'][$relationName])){
						unset($mRelationsByModelNamesOut[$modelName]['byRelationName'][$relationName]);
						$relationsRemovedDeep[$modelName]['byRelationName'][]=$relationName;
					} else {
						$x = "break here";
						$breakCount++;
					}
					if (isset($mRelationsByModelNamesOut[$modelName]['byRelationType'][$relationType][$relatedModelName])){
						unset($mRelationsByModelNamesOut[$modelName]['byRelationType'][$relationType][$relatedModelName]);
						$relationsRemovedDeep[$modelName]['byRelationType'][$relationType][]=$relatedModelName;
					} else {
						$x = "break here";
						$breakCount++;
					}
					$relationsRemoved[$modelName][]=$relatedModelName;
				} else {
					$mRelationsByModelNamesOut2[$modelName]['byModelName'][$relatedModelName]=$relationInfo;
				}
			}

		}
		$includeRemoved = false;
		// if errors were found include removals
		if ($breakCount > 0){
			$includeRemovedDeep = true;
		} else {
			$includeRemovedDeep = false;
		}
		if ($localDebug){
			$msg = "Component model relations";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($mRelationsByModelNamesOut, 10, true);
		}
		if ($includeRemoved){
			$msg2 = "Relations removed";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg2);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($relationsRemoved, 10, true);
		}
		if ($includeRemovedDeep){
			$msg3 = "Deep Relations removed";
			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg3);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($relationsRemovedDeep, 10, true);
		}

		return $mRelationsByModelNamesOut;
	}


    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                //'class' => 'EActiveRecordRelationBehavior',

				'class' => 'ESaveRelatedBehaviorAthleteTeams',
            ),
        ), parent::behaviors());
    }



	// StoreModel(s) before and after model. Also before and after all models
	// add hooks for handling

	/**
	 * @internal Called by storeModelByMtva() and storeModelByMtvaWithRelations()
	 */
	protected static function beforeStoreModelByMtva($modelName, array $mtva, $scmda=null, $runValidation = true){
		Yii::app()->cache();
	}

	/**
	 * @internal Called by storeModelByMtva() and storeModelByMtvaWithRelations()
	 */
	protected static function afterStoreModelByMtva(){

	}

	/**
	 * @internal Called by storeModelByMtva() and storeModelByMtvaWithRelations()
	 */
	protected static function beforeStoreMtva(){

	}

	/**
	 * @internal Called by storeModelByMtva() and storeModelByMtvaWithRelations()
	 */
	protected static function afterStoreMtva(){

	}

}
