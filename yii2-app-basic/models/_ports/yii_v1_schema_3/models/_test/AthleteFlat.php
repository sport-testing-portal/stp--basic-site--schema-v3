<?php

Yii::import('application.models._base.BaseAthleteFlat');

class AthleteFlat extends BaseAthleteFlat
{
    /**
     * @return AthleteFlat
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'AthleteFlat|AthleteFlats', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'person_name_full' => Yii::t('app', 'Person Name Full'),
                'person_age' => Yii::t('app', 'Person Age'),
                'person_city' => Yii::t('app', 'Person City'),
                'person_state_or_region' => Yii::t('app', 'Person State Or Region'),
                'gsm_staff' => Yii::t('app', 'Gsm Staff'),
                'player_id' => Yii::t('app', 'Player'),
                'player_person_id' => Yii::t('app', 'Player Person'),
                'player_age_group_id' => Yii::t('app', 'Player Age Group'),
                'player_team_player_id' => Yii::t('app', 'Player Team Player'),
                'player_sport_position_id' => Yii::t('app', 'Player Sport Position'),
                'player_sport_position2_id' => Yii::t('app', 'Player Sport Position2'),
                'player_sport_position_name' => Yii::t('app', 'Player Sport Position Name'),
                'player_access_code' => Yii::t('app', 'Player Access Code'),
                'player_waiver_minor_dt' => Yii::t('app', 'Player Waiver Minor Dt'),
                'player_waiver_adult_dt' => Yii::t('app', 'Player Waiver Adult Dt'),
                'player_parent_email' => Yii::t('app', 'Player Parent Email'),
                'player_sport_preference' => Yii::t('app', 'Player Sport Preference'),
                'player_sport_position_preference' => Yii::t('app', 'Player Sport Position Preference'),
                'player_shot_side_preference' => Yii::t('app', 'Player Shot Side Preference'),
                'player_dominant_side' => Yii::t('app', 'Player Dominant Side'),
                'player_dominant_foot' => Yii::t('app', 'Player Dominant Foot'),
                'player_statistical_highlights' => Yii::t('app', 'Player Statistical Highlights'),
                'person_id' => Yii::t('app', 'Person'),
                'person_org_id' => Yii::t('app', 'Person Org'),
                'person_app_user_id' => Yii::t('app', 'Person App User'),
                'person_person_type_id' => Yii::t('app', 'Person Person Type'),
                'person_user_id' => Yii::t('app', 'Person User'),
                'person_name_prefix' => Yii::t('app', 'Person Name Prefix'),
                'person_name_first' => Yii::t('app', 'Person Name First'),
                'person_name_middle' => Yii::t('app', 'Person Name Middle'),
                'person_name_last' => Yii::t('app', 'Person Name Last'),
                'person_name_suffix' => Yii::t('app', 'Person Name Suffix'),
                'person_phone_personal' => Yii::t('app', 'Person Phone Personal'),
                'person_email_personal' => Yii::t('app', 'Person Email Personal'),
                'person_phone_work' => Yii::t('app', 'Person Phone Work'),
                'person_email_work' => Yii::t('app', 'Person Email Work'),
                'person_position_work' => Yii::t('app', 'Person Position Work'),
                'person_gender_id' => Yii::t('app', 'Person Gender'),
                'person_image_headshot_url' => Yii::t('app', 'Person Image Headshot Url'),
                'person_name_nickname' => Yii::t('app', 'Person Name Nickname'),
                'person_date_of_birth' => Yii::t('app', 'Person Date Of Birth'),
                'person_dob_eng' => Yii::t('app', 'Person Dob Eng'),
                'person_bday_long' => Yii::t('app', 'Person Bday Long'),
                'person_height' => Yii::t('app', 'Person Height'),
                'person_height_imperial' => Yii::t('app', 'Person Height Imperial'),
                'person_weight' => Yii::t('app', 'Person Weight'),
                'person_bmi' => Yii::t('app', 'Person Bmi'),
                'person_tshirt_size' => Yii::t('app', 'Person Tshirt Size'),
                'person_addr_1' => Yii::t('app', 'Person Addr 1'),
                'person_addr_2' => Yii::t('app', 'Person Addr 2'),
                'person_addr_3' => Yii::t('app', 'Person Addr 3'),
                'person_postal_code' => Yii::t('app', 'Person Postal Code'),
                'person_country' => Yii::t('app', 'Person Country'),
                'person_country_code' => Yii::t('app', 'Person Country Code'),
                'person_profile_url' => Yii::t('app', 'Person Profile Url'),
                'person_profile_uri' => Yii::t('app', 'Person Profile Uri'),
                'person_high_school__graduation_year' => Yii::t('app', 'Person High School Graduation Year'),
                'person_college_graduation_year' => Yii::t('app', 'Person College Graduation Year'),
                'person_college_commitment_status' => Yii::t('app', 'Person College Commitment Status'),
                'created_at' => Yii::t('app', 'Created Dt'),
                'updated_at' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'person_type_id' => Yii::t('app', 'Person Type'),
                'person_type_name' => Yii::t('app', 'Person Type Name'),
                'person_type_desc_short' => Yii::t('app', 'Person Type Desc Short'),
                'person_type_desc_long' => Yii::t('app', 'Person Type Desc Long'),
                'gender_id' => Yii::t('app', 'Gender'),
                'gender_desc' => Yii::t('app', 'Gender Desc'),
                'gender_code' => Yii::t('app', 'Gender Code'),
                'org_id' => Yii::t('app', 'Org'),
                'org_org_type_id' => Yii::t('app', 'Org Org Type'),
                'org_org_level_id' => Yii::t('app', 'Org Org Level'),
                'org_name' => Yii::t('app', 'Org Name'),
                'org_type_name' => Yii::t('app', 'Org Type Name'),
                'org_website_url' => Yii::t('app', 'Org Website Url'),
                'org_twitter_url' => Yii::t('app', 'Org Twitter Url'),
                'org_facebook_url' => Yii::t('app', 'Org Facebook Url'),
                'org_phone_main' => Yii::t('app', 'Org Phone Main'),
                'org_email_main' => Yii::t('app', 'Org Email Main'),
                'org_addr1' => Yii::t('app', 'Org Addr1'),
                'org_addr2' => Yii::t('app', 'Org Addr2'),
                'org_addr3' => Yii::t('app', 'Org Addr3'),
                'org_city' => Yii::t('app', 'Org City'),
                'org_state_or_region' => Yii::t('app', 'Org State Or Region'),
                'org_postal_code' => Yii::t('app', 'Org Postal Code'),
                'org_country_code_iso3' => Yii::t('app', 'Org Country Code Iso3'),
        );
    }
}
