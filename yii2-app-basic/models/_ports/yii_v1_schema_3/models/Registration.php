<?php

Yii::import('application.models._base.BaseRegistration');

class Registration extends BaseRegistration
{
    /**
     * @return Registration
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Registration|Registrations', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    // Write registration values to multiple tables
    protected function afterSave()
    {
		$fva = $this->attributes;
		// @todo begin tran
		saveModels($models='all', $runValidation=true,$fva);
		// @todo end tran
        return parent::afterSave();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'registration_id'				=> Yii::t('app', 'ID'),
                'person_id'						=> Yii::t('app', 'Person'),
                'org_id'						=> Yii::t('app', 'Org'),
                'app_user_id'					=> Yii::t('app', 'App User'),
                'person_type_id'				=> Yii::t('app', 'Person Type'),
                'user_id'						=> Yii::t('app', 'User'),
                'person_name_prefix'			=> Yii::t('app', 'Prefix'),
                'person_name_first'				=> Yii::t('app', 'First'),
                'person_name_middle'			=> Yii::t('app', 'Middle'),
                'person_name_last'				=> Yii::t('app', 'Last'),
                'person_name_suffix'			=> Yii::t('app', 'Suffix'),
                'person_name_full'				=> Yii::t('app', 'Name'),
                'person_phone_personal'			=> Yii::t('app', 'Phone Personal'),
                'person_email_personal'			=> Yii::t('app', 'Email Personal'),
                'person_phone_work'				=> Yii::t('app', 'Phone Work'),
                'person_email_work'				=> Yii::t('app', 'Email Work'),
                'person_position_work'			=> Yii::t('app', 'Position @Work'),
                'gender_id'						=> Yii::t('app', 'Gender'),
                'person_image_headshot_url'		=> Yii::t('app', 'Headshot URL'),
                'person_name_nickname'			=> Yii::t('app', 'Nickname'),
                'person_date_of_birth'			=> Yii::t('app', 'Date Of Birth'),
                'person_height'					=> Yii::t('app', 'Height'),
                'person_weight'					=> Yii::t('app', 'Weight'),
                'person_tshirt_size'			=> Yii::t('app', 'Tshirt Size'),
                'person_addr_1'					=> Yii::t('app', 'Addr 1'),
                'person_addr_2'					=> Yii::t('app', 'Addr 2'),
                'person_addr_3'					=> Yii::t('app', 'Addr 3'),
                'person_city'					=> Yii::t('app', 'City'),
                'person_postal_code'			=> Yii::t('app', 'Postal Code'),
                'person_country'				=> Yii::t('app', 'Country'),
                'person_country_code'			=> Yii::t('app', 'Country Code'),
                'person_state_or_region'		=> Yii::t('app', 'State Or Region'),
                'player_id'						=> Yii::t('app', 'Player'),
                'player_access_code'			=> Yii::t('app', 'Special Access Code'),
                'player_waiver_minor_dt'		=> Yii::t('app', 'Waiver Minor Dt'),
                'player_waiver_adult_dt'		=> Yii::t('app', 'Waiver Adult Dt'),
                'player_parent_email'			=> Yii::t('app', 'Parent Email'),
                'player_sport_preference'		=> Yii::t('app', 'Sport Preference'),
                'player_sport_position_preference' => Yii::t('app', 'Sport Position Preference'),
                'player_shot_side_preference'	=> Yii::t('app', 'Shot Side Preference'),
                'coach_id'						=> Yii::t('app', 'Coach'),
                'coach_type_id'					=> Yii::t('app', 'Coach Type'),
                'coach_specialty'				=> Yii::t('app', 'Coach Specialty'),
                'coach_certifications'			=> Yii::t('app', 'Coach Certifications'),
                'coach_comments'				=> Yii::t('app', 'Coach Comments'),
                'coach_qrcode_uri'				=> Yii::t('app', 'Coach Qrcode URL'),
                'coach_info_source_scrape_url' => Yii::t('app', 'Coach Info Source Scrape Url'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
        );
    }


	/**
	 * Made to bulk assign view attribute values to base model attribute values
	 * @param array[] $field_value_array
	 * @version 4.5.0 synced with vwPlayer v4.5.0
	 */
	public static function fetchTargetModelFieldNameXlation($field_value_array) {
		//$fva = $field_value_array;
		$readonly_attr	= self::fetchReadOnlyAttributes(); // attributes like 'person_age' that are calculated values and can't be written back to the database
		$fva			= array_diff_key( $field_value_array, array_flip( $readonly_attr) ); // remove readonly fields
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($field_value_array, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fva, 10, true);
		//$field_list = array_keys($fva);
		$xlat=[
		'person_name_full'					=> ['model'=>'Person',	'attribute'=>'person_name_full'],
		// 'person_age`
		'team_name'							=> ['model'=>'Team',	'attribute'=>'team_name'],
		//'age_group_name' 					=> ['model'=>'AgeGroup','attribute'=>''],
		'person_city'						=> ['model'=>'Person',	'attribute'=>'person_city'],
		'person_state_or_region'			=> ['model'=>'Person',	'attribute'=>'person_state_or_region'],
		'gsm_staff'							=> ['model'=>'User',	'attribute'=>'first_name'],
		'player_id'							=> ['model'=>'Player',	'attribute'=>'player_id'],
		'person_id'							=> ['model'=>'Person',	'attribute'=>'person_id'],
		// confirmed
		'player_team_player_id'				=> ['model'=>'Player','attribute'=>'player_team_player_id'],
		'player_access_code'				=> ['model'=>'Player','attribute'=>'player_access_code'],
		'player_waiver_minor_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_minor_dt'],
		'player_waiver_adult_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_adult_dt'],
		'player_parent_email'				=> ['model'=>'Player','attribute'=>'player_parent_email'],
		'player_sport_preference'			=> ['model'=>'Player','attribute'=>'player_sport_preference'],
		'player_sport_position_preference'	=> ['model'=>'Player','attribute'=>'player_sport_position_preference'],
		'player_shot_side_preference' 		=> ['model'=>'Player','attribute'=>'player_shot_side_preference'],
		// confirmed
		'org_id'							=> ['model'=>'Person','attribute'=>'org_id'],
		'app_user_id'						=> ['model'=>'Person','attribute'=>'app_user_id'],
		'user_id'							=> ['model'=>'Person','attribute'=>'user_id'],
		'gender_id'							=> ['model'=>'Person','attribute'=>'gender_id'],
		'person_name_prefix'				=> ['model'=>'Person','attribute'=>'person_name_prefix'],
		'person_name_first'					=> ['model'=>'Person','attribute'=>'person_name_first'],
		'person_name_middle'				=> ['model'=>'Person','attribute'=>'person_name_middle'],
		'person_name_last'					=> ['model'=>'Person','attribute'=>'person_name_last'],
		'person_name_suffix'				=> ['model'=>'Person','attribute'=>'person_name_suffix'],
		'person_phone_personal'				=> ['model'=>'Person','attribute'=>'person_phone_personal'],
		'person_email_personal'				=> ['model'=>'Person','attribute'=>'person_email_personal'],
		'person_phone_work'					=> ['model'=>'Person','attribute'=>'person_phone_work'],
		'person_email_work'					=> ['model'=>'Person','attribute'=>'person_email_work'],
		'person_position_work'				=> ['model'=>'Person','attribute'=>'person_position_work'],
		'person_image_headshot_url' 		=> ['model'=>'Person','attribute'=>'person_image_headshot_url'],
		'person_name_nickname'				=> ['model'=>'Person','attribute'=>'person_name_nickname'],
		'person_date_of_birth'				=> ['model'=>'Person','attribute'=>'person_date_of_birth'],
		'person_height'						=> ['model'=>'Person','attribute'=>'person_height'],
		'person_weight'						=> ['model'=>'Person','attribute'=>'person_weight'],
		'person_tshirt_size'				=> ['model'=>'Person','attribute'=>'person_tshirt_size'],
		'person_addr_1'						=> ['model'=>'Person','attribute'=>'person_addr_1'],
		'person_addr_2'						=> ['model'=>'Person','attribute'=>'person_addr_2'],
		'person_addr_3'						=> ['model'=>'Person','attribute'=>'person_addr_3'],
		'person_postal_code'				=> ['model'=>'Person','attribute'=>'person_postal_code'],
		'person_country'					=> ['model'=>'Person','attribute'=>'person_country'],
		'person_country_code'				=> ['model'=>'Person','attribute'=>'person_country_code'],
		'person_type_id'					=> ['model'=>'Person','attribute'=>'person_type_id'],
		//'person_type_name'				=> ['model'=>'PersonType','attribute'=>'person_type_name'],  // readonly
		//'person_type_desc_short'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_short'],  // readonly
		//'person_type_desc_long'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_long'],  // readonly
		//'gender_desc'						=> ['model'=>'Gender',	'attribute'=>'gender_desc'], // readonly
		//'gender_code'						=> ['model'=>'Gender',	'attribute'=>'gender_code'], // readonly
		'org_type_id'						=> ['model'=>'Org','attribute'=>'org_type_id'],
		'org_name'							=> ['model'=>'Org','attribute'=>'org_name'],
		'org_type_name'						=> ['model'=>'OrgType','attribute'=>'org_type_name'],
		'org_website_url'					=> ['model'=>'Org','attribute'=>'org_website_url'],
		'org_twitter_url'					=> ['model'=>'Org','attribute'=>'org_twitter_url'],
		'org_facebook_url'					=> ['model'=>'Org','attribute'=>'org_facebook_url'],
		'org_phone_main'					=> ['model'=>'Org','attribute'=>'org_phone_main'],
		'org_email_main'					=> ['model'=>'Org','attribute'=>'org_email_main'],
		'org_addr1'							=> ['model'=>'Org','attribute'=>'org_addr1'],
		'org_addr2'							=> ['model'=>'Org','attribute'=>'org_addr2'],
		'org_addr3'							=> ['model'=>'Org','attribute'=>'org_addr3'],
		'org_city'							=> ['model'=>'Org','attribute'=>'org_city'],
		'org_state_or_region'				=> ['model'=>'Org','attribute'=>'org_state_or_region'],
		'org_postal_code'					=> ['model'=>'Org','attribute'=>'org_postal_code'],
		'org_country_code_iso3'				=> ['model'=>'Org','attribute'=>'org_country_code_iso3'],
		// confirmed
		'team_player_id'					=> ['model'=>'TeamPlayer','attribute'=>'team_player_id'],
		'team_id'							=> ['model'=>'TeamPlayer','attribute'=>'team_id'],
		'team_play_primary_position'		=> ['model'=>'TeamPlayer','attribute'=>'primary_position'],
		'team_play_begin_dt'				=> ['model'=>'TeamPlayer','attribute'=>'team_play_begin_dt'],
		'team_play_end_dt'					=> ['model'=>'TeamPlayer','attribute'=>'team_play_end_dt'],
		'team_play_coach_id'				=> ['model'=>'TeamPlayer','attribute'=>'coach_id'],
		'team_play_coach_name'				=> ['model'=>'TeamPlayer','attribute'=>'coach_name'],
		'team_play_created_dt'				=> ['model'=>'TeamPlayer','attribute'=>'created_dt'],
		'team_play_updated_dt'				=> ['model'=>'TeamPlayer','attribute'=>'updated_dt'],
		// confirmed
		'team_org_id'						=> ['model'=>'Team','attribute'=>'org_id'],
		'team_school_id'					=> ['model'=>'Team','attribute'=>'school_id'],
		'team_sport_id'						=> ['model'=>'Team','attribute'=>'sport_id'],
		'team_camp_id'						=> ['model'=>'Team','attribute'=>'camp_id'],
		'team_gender_id'					=> ['model'=>'Team','attribute'=>'gender_id'],
		'age_group_id'						=> ['model'=>'Team','attribute'=>'age_group_id'],
		'team_gender'						=> ['model'=>'Team','attribute'=>'team_gender'],
		'team_division'						=> ['model'=>'Team','attribute'=>'team_division'],
		//'team_age_group_depreciated' 		=> ['model'=>'Team','attribute'=>''],
		'coach_id'						=> ['model'=>'Coach','attribute'=>'coach_id'],
		'coach_type_id'					=> ['model'=>'Coach','attribute'=>'coach_type_id'],
		'coach_specialty'				=> ['model'=>'Coach','attribute'=>'coach_specialty'],
		'coach_certifications'			=> ['model'=>'Coach','attribute'=>'coach_certifications'],
		'coach_comments'				=> ['model'=>'Coach','attribute'=>'coach_comments'],
		'coach_qrcode_uri'				=> ['model'=>'Coach','attribute'=>'coach_qrcode_uri'],
		'coach_info_source_scrape_url'  => ['model'=>'Coach','attribute'=>'coach_info_source_scrape_url'],
		];

		return $xlat;
	}

	/**
	 *
	 * @param type $view_field_value_array
	 * @return $mtfva Multi-table field value array
	 * @internal Called by: $this->saveModels()
	 */
	public static function generateModelValuesArray($view_field_value_array) {
		$xlat	= self::fetchTargetModelFieldNameXlation($view_field_value_array);
		$rtn	= [];
		//$view_fields_passed_in = array_keys($$view_field_values_array);
		foreach ($view_field_value_array as $view_field_name => $view_field_value) {
			if(array_key_exists($view_field_name, $xlat)){
				//$models[] = $xlat[$view_field_name]['model'];
				$rtn[$xlat[$view_field_name]['model']][$xlat[$view_field_name]['attribute']]
						= $view_field_value;
			}
		}
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rtn, 10, true);
		return $rtn;
	}


	/**
	 * Returns an array of attributes like 'person_age' that are calculated values and can't be written back to the database
	 * @param type $field_value_array
	 * @internal called by: $this->fetchTargetModelFieldNameXlation()
	 */
	protected static function fetchReadOnlyAttributes() {
		//		$readonly_attr[]; //
		// return ['person_age', 'person_dob_eng', 'person_bday_long', 'person_height_imperial','person_bmi',];
		// $readonly_attr[] = 'person_age';
		$readonly_attr = [];
		return $readonly_attr;
	}


	/**
	 * Saves a specific model the view is composed of.
	 *
	 * The record is inserted as a row into the database table if its {@link isNewRecord}
	 * property is true (usually the case when the record is created using the 'new'
	 * operator). Otherwise, it will be used to update the corresponding row in the table
	 * (usually the case if the record is obtained using one of those 'find' methods.)
	 *
	 * Validation will be performed before saving the record. If the validation fails,
	 * the record will not be saved. You can call {@link getErrors()} to retrieve the
	 * validation errors.
	 *
	 * If the record is saved via insertion, its {@link isNewRecord} property will be
	 * set false, and its {@link scenario} property will be set to be 'update'.
	 * And if its primary key is auto-incremental and is not set before insertion,
	 * the primary key will be populated with the automatically generated key value.
	 *
	 * @param string | array $models
	 * @param boolean $runValidation whether to perform validation before saving the record.
	 * If the validation fails, the record will not be saved to database.
	 * @param array $attributes list of attributes that need to be saved. Defaults to null,
	 * meaning all attributes that are loaded from DB will be saved.
	 * @return boolean whether the saving succeeds
	 */

	public function saveModels($models='all', $runValidation=true,$attributes=null)
	{
		$filter = null;
		// <editor-fold defaultstate="collapsed" desc="parse attributes param">
		if (! is_null($attributes)){
			// filter specific attributes
			if (is_array($attributes) && count($attributes) > 0){
				// loop
				foreach ($attributes as $attribute) {
					$filter[] = $attribute;
				}
			} elseif (is_string($attributes) && strlen($attributes) > 0){
				// filter items passed in
				if (stripos($attributes, ',') !== false){
					// parse string
					$fields = explode(',', $attributes);
					foreach ($fields as $field){
						$filter[] = $field;
					}
				} else {
					// its a non-delimited string
					$filter[] = $attributes;
				}
			}
		}
		// </editor-fold>

		$vwAttr	= $this->attributes;  // Get this model's attribute values
		$mtfva	= self::generateModelValuesArray($vwAttr); // data envelope array

		if ($models == 'all'){
			foreach ($mtfva as $model => $fva) { // mtfva = multi-table fva
				$model_filter[] = $model;
			}
		} elseif(is_array($models)){
			$model_filter = $models;
		}
		foreach ($mfva as $model => $fva) {
			if(in_array($model, $model_filter)){
				// process model
				$model::store($fva);
			}
		}
//		if(!$runValidation || $this->validate($attributes))
//			return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
//		else
//			return false;
	}


}
