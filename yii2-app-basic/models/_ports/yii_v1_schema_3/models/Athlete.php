<?php

Yii::import('application.models._base.BaseAthlete');

class Athlete extends BaseAthlete
{
    /**
     * @return Player
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Player|Players', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Stores this table and its parent table - Person
	 * @param type $field_value_array
	 * @see Person::StorePersonWithOrg($field_value_array)
	 */
	public function StorePlayerWithPerson($field_value_array){
		$this->Store($field_value_array);
	}


	/**
	 * Store Player with parent table Person
	 * @param array[] $field_value_array A name value pair array ['field_name'=>'field value',...]
     * @return int $pk player_id
	 * @internal Insert or update a row, add a row in a parent table if needed
	 * @version 2.0.0
	 * @internal CEC 2.0.0
	 * @var data[person_name_first],data[last,person_name_last] are required!
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 * @todo fix person_id bug, a passed person_id is not seen.
	 * Psuedo Code
	 * look for player by pkey
	 * look for player by fkey (person_id)
	 * if player not found then insert player
	 *    if person exists then update
	 *    if person not exist then insert person
	 *    insert player
     */
    public function Store($field_value_array)
    {
		$pk_name		= $this->primaryKey();
		$fk_name		= 'person_id';	// linkage to parent table
		$class			= __CLASS__;	// Player
		$parentModel	= 'Person';
		$debug_local	= true;

		// use this for value updates inside this method to preserve the incoming params values
		$fva = $field_value_array;

		$requiredFields = [];
		$rules =[];


		// Search for a Player by using the primary key $player_id
		$player_row_found_yn = 'N';
		if (array_key_exists($pk_name, $fva)) { // player_id was found in the fva
			$pk_val = $fva[$pk_name]; // player_id
			//$row = Player::model()->findByPk($pkey_val); // returns object
			$player_row = $class::model()->findByPk($pk_val); // returns object
			if (! empty($player_row)){
				$player_row_found_yn = 'Y';
			}
		}

		// run a search for Player by foreign key $person_id
		if ($player_row_found_yn == 'N' && array_key_exists($fk_name, $fva)) {
			$fkey_val = (int)$fva[$fk_name];
			if ($fkey_val > 0){
			//$row = Player::model()->findByAttributes(array($foreign_key_field_name=>$fkey_val)); // returns object
				$player_row = $class::model()->findByAttributes(array($fk_name=>$fkey_val)); // returns AR object
			} else {
				$player_row=[];
			}
			if (! empty($player_row)){
				$player_row_found_yn = 'Y';
			}
		}

		$personInsertRequirementsMetYN = $this->personInsertRequirementsMetYN($field_value_array);

		// see if the person exists already
		if ($player_row_found_yn == 'Y' && isset($player_row['person_id']) && $player_row['person_id']>0 ){
			$person_id = (int)$player_row['person_id'];
		}
		if (! isset($person_id)){
			// see if person id is the attributes passed in
			if (isset($field_value_array['person_id'])){
				$person_id = $field_value_array['person_id'];
			}
		}
		//if ($player_row_found_yn == 'Y' && $person_id > 0){ // Why do we care if player was found?
		if ($person_id > 0){
			$Person = Person::model()->findByPk($person_id);
			if ($debug_local){
				//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($Person->attributes, 10, true);
			}
		} else {
			$Person = null;
		}

/*		if ( is_null($Person) && isset($fva['person_name_first']) && isset($fva['person_name_last']) ){
			$criteria = new CDbCriteria();
			$criteria->select    = array('person_id', 'person_name_first', 'person_name_last');
			$criteria->condition = 'person_name_first=:person_name_first';
			$criteria->addCondition('person_name_last=:person_name_last', 'AND');
			$criteria->order     = 'person_id';
			$criteria->params    = array(
				':person_name_first'=>$fva['person_name_first']
				,':person_name_last'=>$fva['person_name_last']
				);
			$Persons = Person::model()->findAll($criteria);
		} else {
			$Persons = [];
		}*/

		if (is_null($Person)){
			// insert?
			$person_id = Person::StorePersonWithOrg($fva);
		//} elseif (count($Persons, COUNT_RECURSIVE) > 0){
			// update
			// if a person is changing their name then the lookup would not have found them with the search.
			// In short, no update is needed in this method
			$person_id = $Person->attributes['person_id'];
			return (int)$person_id;
		}


		if ($player_row_found_yn == 'Y'){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $player_row->attributes;
			$player_row->attributes = $fva;
			$diff = array_diff($bfr_upd,$player_row->attributes);
			if (count($diff, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$player_row->attributes[$pk_name];
			}
			// The values must be different. Save the model.
			if ($player_row->save()){
				return (int)$player_row->attributes[$pk_name];
			} else {
				return 0;
			}

		} elseif ($player_row_found_yn == 'N'){
			// Insert
			//$model = new Player();
			$player = new $class();
			$player->unsetAttributes(); // clears default values
			$player->attributes = $fva;
			if ($player->save()){
				$id = (int)$player->attributes[$pk_name];
			} else {
				$id = 0;
			}

			return (int)$id;
		}

		return false;
    }

    /**
	 * @param int
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public function RowCountByPersonId($person_id)
    {
		$class = __CLASS__;
		$rows  = $class::model()->findAllByAttributes(array("person_id"=>$person_id));

		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rows, 10, true);
		if (is_array($rows) && count($rows, COUNT_RECURSIVE) > 0){
			$cnt = count($rows[0]);
		} else {
			$cnt = 0;
		}
		//$cnt = count($rows[0]);
		return $cnt;

    }



	/**
	 * Tests the fva to see if an insert can be performed
	 * @param array $field_value_array
	 * @return string Y=Ok to insert, N=insert forbidden
	 * @internal asserts org, and org_type_id are present OR an org_id > 0 is present
	 * @internal dev sts = rft
	 * @version 1.0.0
	 */
	protected function personInsertRequirementsMetYN(array $field_value_array) {
		// @todo: see if requirements for a person insert have been met
		// The requirements below are for inserting an org that is to be linked to a person. At the SQL level only org is required for an org row.
		$fva = $field_value_array;
		$parent_requirements_met_yn = 'N';

		// assert that an person_id was not passed in
		if (array_key_exists('person_id', $fva) && (int)($fva['person_id'] > 0)) {
			return $parent_requirements_met_yn='N'; // no insert allowed
		}

		// get person values to test from the fva
		if (array_key_exists('person_name_first', $fva) && strlen($fva['person_name_first'] > 0)) {
			$person_fname = $fva['person_name_first'];
		}
		if (array_key_exists('person_name_last', $fva) && strlen($fva['person_name_last'] > 0)) {
			$person_lname = $fva['person_name_last'];
		}

		// Test for valid values in the fva
		if (strlen($person_fname) > 0 && strlen($person_lname) > 0){
			$parent_requirements_met_yn = 'Y'; // insert can proceed
		}
		return $parent_requirements_met_yn;
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'player_id' => Yii::t('app', 'Player'),
                'person_id' => Yii::t('app', 'Person'),
                'age_group_id' => Yii::t('app', 'Age Group'),
                'player_team_player_id' => Yii::t('app', 'Team Player'),
                'player_sport_position_id' => Yii::t('app', 'Sport Position'),
                'player_sport_position2_id' => Yii::t('app', 'Sport Position2'),
                'player_access_code' => Yii::t('app', 'Access Code'),
                'player_waiver_minor_dt' => Yii::t('app', 'Waiver Minor Dt'),
                'player_waiver_adult_dt' => Yii::t('app', 'Waiver Adult Dt'),
                'player_parent_email' => Yii::t('app', 'Parent Email'),
                'player_sport_preference' => Yii::t('app', 'Sport Preference'),
                'player_sport_position_preference' => Yii::t('app', 'Sport Position Preference'),
                'player_shot_side_preference' => Yii::t('app', 'Shot Side Preference'),
                'player_dominant_foot' => Yii::t('app', 'Dominant Foot'),
                'created_at' => Yii::t('app', 'Created Dt'),
                'updated_at' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'campSessionPlayerEvals' => null,
                'ageGroup' => null,
                'person' => null,
                'playerSportPosition' => null,
                'playerTeamPlayer' => null,
                'playerAcademics' => null,
                'playerCampLogs' => null,
                'playerCollegeRecruits' => null,
                'playerContacts' => null,
                'playerSchools' => null,
                'playerSports' => null,
                'playerTeams' => null,
                'teamPlayers' => null,
        );
    }

	/**
	 *
	 * @return CActiveRecord (Person + PersonType + Org + OrgType + Player)
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 * @version 0.0.1
	 * @internal v0.0.1 is based on an early Person::athlete scope
	 * @deprecated since version 0.54.3
	 * @internal Development Status = code construction
	 */
	public function athlete_v0() {
		$personTypeID = 1;
		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
				'condition'=>'t.person_type_id = :person_type_id',
					'params'=>[':person_type_id'=>$personTypeID],
				'with'=>[
					'personType'=>[
						'condition'=>'t.person_type_id = personType.person_type_id',
					],
					'players'=>[
						'condition'=>'t.person_id = players.person_id',
					],
					'org'=>[
						'condition'=>'t.org_id = org.org_id',
						'with'=>'orgType',
					],
				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}


    public function relations() {
        return array(
            'campSessionPlayerEvals' => array(self::HAS_MANY, 'CampSessionPlayerEval', 'player_id'),
            'ageGroup' => array(self::BELONGS_TO, 'AgeGroup', 'age_group_id'),
            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
            'playerSportPosition' => array(self::BELONGS_TO, 'SportPosition', 'player_sport_position_id'),
            'playerTeamPlayer' => array(self::BELONGS_TO, 'TeamPlayer', 'player_team_player_id'),
            'playerAcademics' => array(self::HAS_MANY, 'PlayerAcademic', 'player_id'),
            'playerCampLogs' => array(self::HAS_MANY, 'PlayerCampLog', 'player_id'),
            'playerCollegeRecruits' => array(self::HAS_MANY, 'PlayerCollegeRecruit', 'player_id'),
            'playerContacts' => array(self::HAS_MANY, 'PlayerContact', 'player_id'),
            'playerSchools' => array(self::HAS_MANY, 'PlayerSchool', 'player_id'),
            'playerSports' => array(self::HAS_MANY, 'PlayerSport', 'player_id'),
            'playerTeams' => array(self::HAS_MANY, 'PlayerTeam', 'player_id'),
            'teamPlayers' => array(self::HAS_MANY, 'TeamPlayer', 'player_id'),
        );
    }

	/**
	 *
	 * @param string|int $personID
	 * @return CActiveRecord (Person + PersonType + Org + OrgType + Player)
	 * @example
	 *   Find an athlete by their person id
	 *     $athlete = Person::model()->athleteByPersonId($personID=157)->find();
	 *     $attrDeep = BaseModel::getAttributesDeepAsArray($athlete);
	 *     YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($attrDeep, 10, true);
	 *
	 */
	public function athlete2ByAthleteId($athleteId) {
		$personTypeID = 1;
		$personID     = $athleteId;
		$localDebug = true;

//		$tableAlias = $this->getTableAlias(false, false);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($tableAlias, 10, true);
		$this->getDbCriteria()->mergeWith(
			[
				//'condition'=>'t.player_id = :player_id' ,
				//'condition'=>$this->getTableAlias(false, false) . '.player_id_ = 78',
//				'condition'=>'t.person_id = :person_id'
//					.' and person.person_type_id = :person_type_id',
//				'on'=>"person.person_type_id = 1",
//					'params'=>[':person_id'=>$personID, ':person_type_id'=>$personTypeID],
				'with'=>[
					'person'=>[
						'with'=>[
							'org'=>[
//								//'condition'=>'person.org_id = org.org_id',
//								'with'=>[
//									'orgType',
								],
							],
//							'personType'=>[
//								'condition'=>'person.person_type_id = personType.person_type_id',
//							],
						],
					//],
//					'playerAcademics'=>[
//						//'condition'=>'t.player_id = teamPlayers.player_id',
//					],
//					'teamPlayers'=>[
//						'condition'=>'t.player_id = teamPlayers.player_id',
//					],
//					'teamPlayers'=>[
//						'condition'=>'t.player_id = teamPlayers.player_id',
//					],
				],
//				[
//					'condition'=>$this->getTableAlias(false,false) . '.person_type_id = :person_type_id',
//					'params'=>[':person_type_id__forceErr'=>$personTypeID],
//				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}


	/**
	 * Used to test athleteLazyLoad()
	 * Pulls t
	 */
	public static function athleteLazyLoad_testHarness() {
		$pk = 1177;
		//$model = Org::model()->findByPk($pk);
		$model = Person::model()->athlete()->findByPk(157);

		$modelsToLoad = ['TeamPlayers','Note','Media'];
		$byModelNames = self::orgLazyLoad($model, $modelsToLoad, []);

		$relationsToLoad = ['teams','notes','medias'];
		$byRelationNames = self::orgLazyLoad($model, [], $relationsToLoad);

		$attrDeepByModel    = BaseModel::getAttributesDeepAsArray($byModelNames);
		$attrDeepByRelation = BaseModel::getAttributesDeepAsArray($byRelationNames);

		echo "<h1>Lazy loads by model name follow</h1>";
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($attrDeepByModel, 10, true);
		echo "<h1>Lazy loads by relation name follow</h1>";
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($attrDeepByRelation, 10, true);
	}

	/**
	 * This is NOT a model scope. This method receives a Person model and appends
	 * Player|Athlete relations to it. It can also return a Player|Athlete sans
	 * Person object.
	 * @param CActiveRecord::Person $model | CActiveRecord::Athlete|Player $model
	 * @param array $modelsToLoad A list of model names to load
	 * @param array $relationsToLoad A list of athlete|player relation Names to load
	 * @return CActiveRecord::AweActiveRecord::Athlete | CActiveRecord::AweActiveRecord::Person
	 * @example Athlete::model()->athleteLazyLoadByAthleteID($athleteID)->findByPk($athleteID);
	 */
	public static function athleteLazyLoad($model, array $modelsToLoad=[], array $relationsToLoad=[]) {

		// Validate parameters
		$isPerson  = false;
		$isAthlete = false;
		$personTypeID = 0;
		$localDebug = false;

		if (is_a($model, 'Person')){
			$isPerson = true;
			$personID = $model->person_id;
			$playerID = 0;
			$personTypeID = $model->person_type_id;

		} elseif (is_a($model, 'Player') || is_a($model, 'Athlete')){
			$isAthlete = true;
			$personID = $model->person_id;
			$playerID = $model->player_id;
			if (isset($model->person)){
				$personTypeID = $model->person[person_type_id];
			}
		}

		if ((int)$personTypeID !== 1){
			throw new CException("A Person or Athlete object is required!", 707);
		}

		// @todo is this really neccessary? Why not both?
		if (count($modelsToLoad) > 0 && count($relationsToLoad) > 0){
			throw new CException("specify model names OR relation names. You can not pass both", 707);
		}

		if ($isPerson && ! isset($model->players)){
			// find the player|athlete object to add
			$athleteModel = Athlete::model()->athlete()->findByAttributes(['person_id'=>$personID]);

			if ($localDebug === true){
				$attrDeep = BaseModel::getAttributesDeepAsArray($athleteModel);
				$example = "\$model = Athlete::model()->athlete()->findByAttributes(['person_id'=>$personID]);";
				echo "<h1>Athlete->athlete() test harness</h1><pre>$example</pre>";
				YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($attrDeep, 10, true);
				return;
				//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($athleteModel, 10, true);
			}

		}

		// determin if the athlete model is missing any relations that are player.
		$relationsMetaBase = BaseModel::fetchModelRelations('Player');
		$relationsMeta     = BaseModel::fetchModelRelations('Athlete');
		$diff              = array_diff_key($relationsMetaBase, $relationsMeta);
		if (count($diff) > 0 ){
			$missingRelationNames = array_keys();
			$dumpText = CVarDumper::dumpAsString($missingRelationNames, 10, true);
			$msg = 'New relations have been added to the Player model that are '
					. 'not included in the Athlete model' . $dumpText;
			throw new CException($msg, 708);
		}
		// Key all operations off of model names even when relation names are passed in
		if (count($modelsToLoad) > 0){

			foreach ($modelsToLoad as $modelName){

				$models = $modelName::model()->findAllByAttributes(['person_id'=>$personID]);
				foreach ($models as $lazyModel){
					$model[$relationsMeta['byModelName'][$modelName]['relationName']] =  $lazyModel;
				}
				// PHP best practice for foreach loops
				unset($lazyModel);
			}
			// PHP best practice for foreach loops
			unset($modelName);
		}

		// Key all operations off of model names even when relation names are passed in
		if (count($relationsToLoad) > 0){
			//foreach ($relationsMeta['byRelationName'] as $relationName => $relationsInfo){
			foreach ($relationsToLoad as $relationName){
				$modelName = $relationsMeta['byRelationName'][$relationName]['relatedModel'];
				$models = $modelName::model()->findAllByAttributes(['org_id'=>$orgID]);
				foreach ($models as $lazyModel){
					$model[$relationName] =  $lazyModel;
				}
				// PHP best practice for foreach loops
				unset($lazyModel);
			}
			// PHP best practice for foreach loops
			unset($relationName);
		}

		// in-line person relations reference
		//		'addresses',
		//		'camps',
		//		'medias',
		//		'notes',
		//		'orgLevel', // OrgLevel HasMany Orgs eg a parent of org
		//		'orgType',  // OrgType HasMany Orgs eg a parent of org
		//		'paymentLogs',
		//		'people',
		//		'schools',
		//		'subscriptions',
		//		'teams',

		// in-line athlete relations reference
		// * @property CampSessionPlayerEval[] $campSessionPlayerEvals
		// * @property AgeGroup $ageGroup
		// * @property Person $person
		// * @property SportPosition $playerSportPosition
		// * @property TeamPlayer $playerTeamPlayer
		// * @property PlayerAcademic[] $playerAcademics
		// * @property PlayerCampLog[] $playerCampLogs
		// * @property PlayerCollegeRecruit[] $playerCollegeRecruits
		// * @property PlayerContact[] $playerContacts
		// * @property PlayerSchool[] $playerSchools
		// * @property PlayerSport[] $playerSports
		// * @property PlayerTeam[] $playerTeams
		// * @property TeamPlayer[] $teamPlayers
		return $model;
	}

	public function athleteScope_testHarness() {
		$personID = 157;
		$athleteModel = Athlete::model()->athlete()->
				findByAttributes(['person_id'=>$personID]);
		$localDebug = true;
		if ($localDebug === true){
			$attrDeep = BaseModel::getAttributesDeepAsArray($athleteModel);
			$example = "\$model = Athlete::model()->athlete()->findByAttributes(['person_id'=>$personID]);";
			echo "<h1>Athlete->athlete() test harness</h1><pre>$example</pre>";
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($attrDeep, 10, true);
			return;
			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($athleteModel, 10, true);
		}
			return;
	}

	/**
	 *
	 * @return CActiveRecord (Person + PersonType + Org + OrgType + Player, etc.)
	 * @example $model = Person::model()->athlete()->findByPk(157); // person_id
	 * @version 0.1.0
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public function athlete() {
		$personTypeID = 1;
		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
//				'condition'=>'t.person_type_id = :person_type_id',
//					'params'=>[':person_type_id'=>$personTypeID],
				'with'=>[
					'teamPlayers',
					'playerTeams',
					'playerSports',
					'playerSchools',
					'playerContacts',
					'playerCollegeRecruits',
					'playerCampLogs',
					'playerAcademics',
					'playerTeamPlayer',
					'playerSportPosition',
					'campSessionPlayerEvals',
					'ageGroup',
					'playerContacts',
					//'searchCriteriaTemplates',
				],
			]
		);

		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
		// in-line athlete relations reference
		//	'campSessionPlayerEvals' => array(self::HAS_MANY, 'CampSessionPlayerEval', 'player_id'),
		//	'ageGroup' => array(self::BELONGS_TO, 'AgeGroup', 'age_group_id'),
		//	'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
		//	'playerSportPosition' => array(self::BELONGS_TO, 'SportPosition', 'player_sport_position_id'),
		//	'playerTeamPlayer' => array(self::BELONGS_TO, 'TeamPlayer', 'player_team_player_id'),
		//	'playerAcademics' => array(self::HAS_MANY, 'PlayerAcademic', 'player_id'),
		//	'playerCampLogs' => array(self::HAS_MANY, 'PlayerCampLog', 'player_id'),
		//	'playerCollegeRecruits' => array(self::HAS_MANY, 'PlayerCollegeRecruit', 'player_id'),
		//	'playerContacts' => array(self::HAS_MANY, 'PlayerContact', 'player_id'),
		//	'playerSchools' => array(self::HAS_MANY, 'PlayerSchool', 'player_id'),
		//	'playerSports' => array(self::HAS_MANY, 'PlayerSport', 'player_id'),
		//	'playerTeams' => array(self::HAS_MANY, 'PlayerTeam', 'player_id'),
		//	'teamPlayers' => array(self::HAS_MANY, 'TeamPlayer', 'player_id'),

	}

}
