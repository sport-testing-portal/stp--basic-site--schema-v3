<?php

Yii::import('application.models._base.BaseVwTeamPlayer');

class VwTeamPlayer extends BaseVwTeamPlayer
{
    /**
     * @return VwTeamPlayer
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'VwTeamPlayer|VwTeamPlayers', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    public function primaryKey()
    {
        //return $this->tableName() . '_id';
		return 'id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }


	/**
	 *
	 * @param type $playerID
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchIncludingPlayer($playerID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        //$criteria->with=array('team');
        //$criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.player_id',$playerID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort;
        $sort->attributes = array(
            'team_name' => array(
            'asc' => 'team_name',
            'desc' => 'team_name DESC',
            ), '*', /* Treat all other columns normally */
        );
        /* End: Sort on related Model's columns */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }

	/**
	 * Made to bulk assign view attribute values to base model attribute values
	 * @param array[] $field_value_array
	 * @version 4.5.0 synced with vwPlayer v4.5.0
	 */
	public static function fetchTargetModelFieldNameXlation($field_value_array) {
		//$fva = $field_value_array;
		$readonly_attr	= self::fetchReadOnlyAttributes(); // attributes like 'person_age' that are calculated values and can't be written back to the database
		$fva			= array_diff_key( $field_value_array, array_flip( $readonly_attr) ); // remove readonly fields
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($field_value_array, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fva, 10, true);
		//$field_list = array_keys($fva);
		$xlat=[
		'person_name_full'					=> ['model'=>'Person',	'attribute'=>'person_name_full'],
		// 'person_age`
		'team_name'							=> ['model'=>'Team',	'attribute'=>'team_name'],
		//'age_group_name' 					=> ['model'=>'AgeGroup','attribute'=>''],
		'person_city'						=> ['model'=>'Person',	'attribute'=>'person_city'],
		'person_state_or_region'			=> ['model'=>'Person',	'attribute'=>'person_state_or_region'],
		'gsm_staff'							=> ['model'=>'User',	'attribute'=>'first_name'],
		'player_id'							=> ['model'=>'Player',	'attribute'=>'player_id'],
		'person_id'							=> ['model'=>'Person',	'attribute'=>'person_id'],
		// confirmed
		'player_team_player_id'				=> ['model'=>'Player','attribute'=>'player_team_player_id'],
		'player_access_code'				=> ['model'=>'Player','attribute'=>'player_access_code'],
		'player_waiver_minor_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_minor_dt'],
		'player_waiver_adult_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_adult_dt'],
		'player_parent_email'				=> ['model'=>'Player','attribute'=>'player_parent_email'],
		'player_sport_preference'			=> ['model'=>'Player','attribute'=>'player_sport_preference'],
		'player_sport_position_preference'	=> ['model'=>'Player','attribute'=>'player_sport_position_preference'],
		'player_shot_side_preference' 		=> ['model'=>'Player','attribute'=>'player_shot_side_preference'],
		'player_dominant_side'		 		=> ['model'=>'Player','attribute'=>'player_dominant_side'],
		'player_dominant_foot'		 		=> ['model'=>'Player','attribute'=>'player_dominant_foot'],
		'player_statistical_highlights'		=> ['model'=>'Player','attribute'=>'player_statistical_highlights'],
		// confirmed
		'org_id'							=> ['model'=>'Person','attribute'=>'org_id'],
		'app_user_id'						=> ['model'=>'Person','attribute'=>'app_user_id'],
		'person_name_prefix'				=> ['model'=>'Person','attribute'=>'person_name_prefix'],
		'person_name_first'					=> ['model'=>'Person','attribute'=>'person_name_first'],
		'person_name_middle'				=> ['model'=>'Person','attribute'=>'person_name_middle'],
		'person_name_last'					=> ['model'=>'Person','attribute'=>'person_name_last'],
		'person_name_suffix'				=> ['model'=>'Person','attribute'=>'person_name_suffix'],
		'person_phone_personal'				=> ['model'=>'Person','attribute'=>'person_phone_personal'],
		'person_email_personal'				=> ['model'=>'Person','attribute'=>'person_email_personal'],
		'person_phone_work'					=> ['model'=>'Person','attribute'=>'person_phone_work'],
		'person_email_work'					=> ['model'=>'Person','attribute'=>'person_email_work'],
		'person_position_work'				=> ['model'=>'Person','attribute'=>'person_position_work'],
		'person_image_headshot_url' 		=> ['model'=>'Person','attribute'=>'person_image_headshot_url'],
		'person_name_nickname'				=> ['model'=>'Person','attribute'=>'person_name_nickname'],
		'person_date_of_birth'				=> ['model'=>'Person','attribute'=>'person_date_of_birth'],
		'person_height'						=> ['model'=>'Person','attribute'=>'person_height'],
		'person_weight'						=> ['model'=>'Person','attribute'=>'person_weight'],
		'person_tshirt_size'				=> ['model'=>'Person','attribute'=>'person_tshirt_size'],
		'person_addr_1'						=> ['model'=>'Person','attribute'=>'person_addr_1'],
		'person_addr_2'						=> ['model'=>'Person','attribute'=>'person_addr_2'],
		'person_addr_3'						=> ['model'=>'Person','attribute'=>'person_addr_3'],
		'person_postal_code'				=> ['model'=>'Person','attribute'=>'person_postal_code'],
		'person_country'					=> ['model'=>'Person','attribute'=>'person_country'],
		'person_country_code'				=> ['model'=>'Person','attribute'=>'person_country_code'],
		'person_profile_url'				=> ['model'=>'Person','attribute'=>'person_profile_url'],
		'person_profile_uri'				=> ['model'=>'Person','attribute'=>'person_profile_uri'],
		'person_type_id'					=> ['model'=>'Person','attribute'=>'person_type_id'],
		//'person_type_name'				=> ['model'=>'PersonType','attribute'=>'person_type_name'],  // readonly
		//'person_type_desc_short'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_short'],  // readonly
		//'person_type_desc_long'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_long'],  // readonly
		//'gender_desc'						=> ['model'=>'Gender',	'attribute'=>'gender_desc'], // readonly
		//'gender_code'						=> ['model'=>'Gender',	'attribute'=>'gender_code'], // readonly
		'org_type_id'						=> ['model'=>'Org','attribute'=>'org_type_id'],
		'org_name'							=> ['model'=>'Org','attribute'=>'org_name'],
		'org_type_name'						=> ['model'=>'OrgType','attribute'=>'org_type_name'],
		'org_website_url'					=> ['model'=>'Org','attribute'=>'org_website_url'],
		'org_twitter_url'					=> ['model'=>'Org','attribute'=>'org_twitter_url'],
		'org_facebook_url'					=> ['model'=>'Org','attribute'=>'org_facebook_url'],
		'org_phone_main'					=> ['model'=>'Org','attribute'=>'org_phone_main'],
		'org_email_main'					=> ['model'=>'Org','attribute'=>'org_email_main'],
		'org_addr1'							=> ['model'=>'Org','attribute'=>'org_addr1'],
		'org_addr2'							=> ['model'=>'Org','attribute'=>'org_addr2'],
		'org_addr3'							=> ['model'=>'Org','attribute'=>'org_addr3'],
		'org_city'							=> ['model'=>'Org','attribute'=>'org_city'],
		'org_state_or_region'				=> ['model'=>'Org','attribute'=>'org_state_or_region'],
		'org_postal_code'					=> ['model'=>'Org','attribute'=>'org_postal_code'],
		'org_country_code_iso3'				=> ['model'=>'Org','attribute'=>'org_country_code_iso3'],
		// confirmed
		'team_player_id'					=> ['model'=>'TeamPlayer','attribute'=>'team_player_id'],
		'team_id'							=> ['model'=>'TeamPlayer','attribute'=>'team_id'],
		'team_play_primary_position'		=> ['model'=>'TeamPlayer','attribute'=>'primary_position'],
		'team_play_sport_position_id'		=> ['model'=>'TeamPlayer','attribute'=>'team_play_sport_position_id'],
		'team_play_sport_position2_id'		=> ['model'=>'TeamPlayer','attribute'=>'team_play_sport_position2_id'],
		'team_play_begin_dt'				=> ['model'=>'TeamPlayer','attribute'=>'team_play_begin_dt'],
		'team_play_end_dt'					=> ['model'=>'TeamPlayer','attribute'=>'team_play_end_dt'],
		'team_play_coach_id'				=> ['model'=>'TeamPlayer','attribute'=>'coach_id'],
		'team_play_coach_name'				=> ['model'=>'TeamPlayer','attribute'=>'coach_name'],
		'team_play_created_dt'				=> ['model'=>'TeamPlayer','attribute'=>'created_dt'],
		'team_play_updated_dt'				=> ['model'=>'TeamPlayer','attribute'=>'updated_dt'],
		'team_play_statistical_highlights'	=> ['model'=>'TeamPlayer','attribute'=>'team_play_statistical_highlights'],
		'team_play_current_team'	        => ['model'=>'TeamPlayer','attribute'=>'team_play_current_team'],

		//confirmed
		'team_play_sport_position_id'		=> ['model'=>'SportPosition_1','attribute'=>'sport_position_id'],
		'team_play_sport_position2_id'		=> ['model'=>'SportPosition_1','attribute'=>'sport_position_id'],
		'team_play_sport_position_name'		=> ['model'=>'SportPosition_2','attribute'=>'sport_position_name'],
		'team_play_sport_position2_name'	=> ['model'=>'SportPosition_2','attribute'=>'sport_position_name'],
		// confirmed
		'team_org_id'						=> ['model'=>'Team','attribute'=>'org_id'],
		'team_school_id'					=> ['model'=>'Team','attribute'=>'school_id'],
		'team_sport_id'						=> ['model'=>'Team','attribute'=>'sport_id'],
		'team_camp_id'						=> ['model'=>'Team','attribute'=>'camp_id'],
		'team_gender_id'					=> ['model'=>'Team','attribute'=>'gender_id'],
		'age_group_id'						=> ['model'=>'Team','attribute'=>'age_group_id'],
		'team_age_group'					=> ['model'=>'Team','attribute'=>'team_age_group'],
		'team_website_url'					=> ['model'=>'Team','attribute'=>'team_website_url'],
		'team_schedule_url'					=> ['model'=>'Team','attribute'=>'team_schedule_url'],
		'team_schedule_uri'					=> ['model'=>'Team','attribute'=>'team_schedule_uri'],
		'team_competition_season_id'		=> ['model'=>'Team','attribute'=>'team_competition_season_id'],
		'team_competition_season'			=> ['model'=>'Team','attribute'=>'team_competition_season'],
		'team_statistical_highlights'		=> ['model'=>'Team','attribute'=>'team_statistical_highlights'],
		'team_gender'						=> ['model'=>'Team','attribute'=>'team_gender'],
		'team_city'							=> ['model'=>'Team','attribute'=>'team_city'],
		'team_division'						=> ['model'=>'Team','attribute'=>'team_division'],
		'team_division_id'					=> ['model'=>'Team','attribute'=>'team_division_id'],
		'team_league_id'					=> ['model'=>'Team','attribute'=>'team_league_id'],
		'team_state_id'						=> ['model'=>'Team','attribute'=>'team_state_id'],
		'team_country_id'					=> ['model'=>'Team','attribute'=>'team_country_id'],
		// confirmed
		'team_org_org_type_id'				=> ['model'=>'Org','attribute'=>'org_type_id'],
		'team_org_org_type_name'			=> ['model'=>'OrgType','attribute'=>'org_type_name'],
		'team_org_org_name'					=> ['model'=>'Org','attribute'=>'org_name'],
		'team_org_org_ncaa_clearing_house_id'=> ['model'=>'Org','attribute'=>'org_ncaa_clearing_house_id'],
		'team_org_org_website_url'			=> ['model'=>'Org','attribute'=>'org_website_url'],
		'team_org_org_twitter_url'			=> ['model'=>'Org','attribute'=>'org_twitter_url'],
		'team_org_org_facebook_url'			=> ['model'=>'Org','attribute'=>'org_facebook_url'],
		'team_org_org_phone_main'			=> ['model'=>'Org','attribute'=>'org_phone_main'],
		'team_org_org_email_main'			=> ['model'=>'Org','attribute'=>'org_email_main'],
		'team_org_org_addr1'				=> ['model'=>'Org','attribute'=>'org_addr1'],
		'team_org_org_addr2'				=> ['model'=>'Org','attribute'=>'org_addr2'],
		'team_org_org_addr3'				=> ['model'=>'Org','attribute'=>'org_addr3'],
		'team_org_org_city'					=> ['model'=>'Org','attribute'=>'org_city'],
		'team_org_state_or_region'			=> ['model'=>'Org','attribute'=>'org_state_or_region'],
		'team_org_org_postal_code'			=> ['model'=>'Org','attribute'=>'org_postal_code'],
		'team_org_org_country_code_iso3'	=> ['model'=>'Org','attribute'=>'org_country_code_iso3'],
		'team_org_org_governing_body'	    => ['model'=>'Org','attribute'=>'org_governing_body'],
		// confirmed
		'team_division_id'					=> ['model'=>'TeamDivision','attribute'=>'team_division_id'],
		'team_division_name'				=> ['model'=>'TeamDivision','attribute'=>'team_division_name'],
		// confirmed
		'team_league_id'					=> ['model'=>'TeamLeague','attribute'=>'team_league_id'],
		'team_league_name'					=> ['model'=>'TeamLeague','attribute'=>'team_league_name'],

		//'team_age_group_depreciated' 		=> ['model'=>'Team','attribute'=>''],
		];

		return $xlat;
	}

	public static function generateModelValuesArray($view_field_value_array) {
		$xlat	= self::fetchTargetModelFieldNameXlation($view_field_value_array);
		$rtn	= [];
		//$view_fields_passed_in = array_keys($$view_field_values_array);
		foreach ($view_field_value_array as $view_field_name => $view_field_value) {
			if(array_key_exists($view_field_name, $xlat)){
				//$models[] = $xlat[$view_field_name]['model'];
				$rtn[$xlat[$view_field_name]['model']][$xlat[$view_field_name]['attribute']]
						= $view_field_value;
			}
		}
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rtn, 10, true);
		return $rtn;
	}


	/**
	 * Returns an array of attributes like 'person_age' that are calculated values and can't be written back to the database
	 * @param type $field_value_array
	 * @internal Development Status = code construction
	 */
	protected static function fetchReadOnlyAttributes() {
		//		$readonly_attr[]; //
		// return ['person_age', 'person_dob_eng', 'person_bday_long', 'person_height_imperial','person_bmi',];
		$readonly_attr[] = 'person_age';
		$readonly_attr[] = 'person_dob_eng';
		$readonly_attr[] = 'person_bday_long';
		$readonly_attr[] = 'person_height_imperial';
		$readonly_attr[] = 'person_bmi';
		$readonly_attr[] = 'age_group_name';
		$readonly_attr[] = 'person_type_name';		// ['model'=>'PersonType','attribute'=>'person_type_name'],  // readonly
		$readonly_attr[] = 'person_type_desc_short'; // ['model'=>'PersonType','attribute'=>'person_type_desc_short'],  // readonly
		$readonly_attr[] = 'person_type_desc_long';	//	=> ['model'=>'PersonType','attribute'=>'person_type_desc_long'],  // readonly
		$readonly_attr[] = 'gender_desc';			//	=> ['model'=>'Gender','attribute'=>'gender_desc'], // readonly
		$readonly_attr[] = 'gender_code';			//	=> ['model'=>'Gender','attribute'=>'gender_code'], // readonly
		$readonly_attr[] = 'gsm_staff';				//	=> ['model'=>'User','attribute'=>'first_name'], // readonly
		return $readonly_attr;
	}


	/**
	 * Saves a specific model the view is composed of.
	 *
	 * The record is inserted as a row into the database table if its {@link isNewRecord}
	 * property is true (usually the case when the record is created using the 'new'
	 * operator). Otherwise, it will be used to update the corresponding row in the table
	 * (usually the case if the record is obtained using one of those 'find' methods.)
	 *
	 * Validation will be performed before saving the record. If the validation fails,
	 * the record will not be saved. You can call {@link getErrors()} to retrieve the
	 * validation errors.
	 *
	 * If the record is saved via insertion, its {@link isNewRecord} property will be
	 * set false, and its {@link scenario} property will be set to be 'update'.
	 * And if its primary key is auto-incremental and is not set before insertion,
	 * the primary key will be populated with the automatically generated key value.
	 *
	 * @param string | array $models
	 * @param boolean $runValidation whether to perform validation before saving the record.
	 * If the validation fails, the record will not be saved to database.
	 * @param array $attributes list of attributes that need to be saved. Defaults to null,
	 * meaning all attributes that are loaded from DB will be saved.
	 * @return boolean whether the saving succeeds
	 */

	public function saveModels($models='all', $runValidation=true,$attributes=null)
	{
		$filter = null;
		// <editor-fold defaultstate="collapsed" desc="parse attributes param">
		if (! is_null($attributes)){
			// filter specific attributes
			if (is_array($attributes) && count($attributes) > 0){
				// loop
				foreach ($attributes as $attribute) {
					$filter[] = $attribute;
				}
			} elseif (is_string($attributes) && strlen($attributes) > 0){
				// filter items passed in
				if (stripos($attributes, ',') !== false){
					// parse string
					$fields = explode(',', $attributes);
					foreach ($fields as $field){
						$filter[] = $field;
					}
				} else {
					// its a non-delimited string
					$filter[] = $attributes;
				}
			}
		}
		// </editor-fold>

		$vwAttr	= $this->attributes;  // Get this model's attribute values
		$mtfva	= self::generateModelValuesArray($vwAttr); // data envelope array

		if ($models == 'all'){
			foreach ($mtfva as $model => $fva) { // mtfva = multi-table fva
				$model_filter[] = $model;
			}
		} elseif(is_array($models)){
			$model_filter = $models;
		}
		foreach ($mfva as $model => $fva) {
			if(in_array($model, $model_filter)){
				// process model
				$model::store($fva);
			}
		}
//		if(!$runValidation || $this->validate($attributes))
//			return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
//		else
//			return false;
	}


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'id' => Yii::t('app', 'ID'),
                'org_name' => Yii::t('app', 'Org Name'),
                'team_name' => Yii::t('app', 'Team Name'),
                'age_group_name' => Yii::t('app', 'Age Group'),
                'team_division' => Yii::t('app', 'Team Division'),
                'team_league' => Yii::t('app', 'Team League'),
                'team_league_id' => Yii::t('app', 'Team League ID'),
                'team_league_name' => Yii::t('app', 'Team League Name'),
                'team_division_id' => Yii::t('app', 'Team Division ID'),
                'team_division_name' => Yii::t('app', 'Team Division Name'),
                'organizational_level' => Yii::t('app', 'Organizational Level'),
                'team_city' => Yii::t('app', 'Team City'),
                'team_state_name' => Yii::t('app', 'Team State Name'),
                'team_country_name' => Yii::t('app', 'Team Country Name'),
                'gender_code' => Yii::t('app', 'Gender Code'),
                'gender_desc' => Yii::t('app', 'Gender Desc'),
                'coach_name' => Yii::t('app', 'Coach Name'),
                'player_name' => Yii::t('app', 'Player Name'),
                'team_player_id' => Yii::t('app', 'Team Player'),
                'coach_name' => Yii::t('app', 'Coach Name'),
                'coach_id' => Yii::t('app', 'Coach ID'),
                'team_play_begin_dt' => Yii::t('app', 'Begin Dt'),
                'team_play_end_dt' => Yii::t('app', 'End Dt'),
                'team_play_sport_position_id' => Yii::t('app', 'Team Play Sport Position'),
                'team_play_sport_position2_id' => Yii::t('app', 'Team Play Sport Position2'),
                'primary_sport_position' => Yii::t('app', 'Primary Position'),
                'secondary_sport_position' => Yii::t('app', 'Secondary Position'),
                'org_id' => Yii::t('app', 'Org'),
                'team_id' => Yii::t('app', 'Team'),
                //'coach_id' => Yii::t('app', 'Coach'),
                'player_id' => Yii::t('app', 'Player'),
                //'coach__person_id' => Yii::t('app', 'Coach Person'),
                'player__person_id' => Yii::t('app', 'Player Person'),
                'team_created_by_username' => Yii::t('app', 'Team Created By Username'),
                'team_updated_by_username' => Yii::t('app', 'Team Updated By Username'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
        );
    }
}
