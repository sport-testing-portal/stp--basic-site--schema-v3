<?php

/**
 * This is the model class for table "org".
 *
 * The followings are the available columns in table 'org':
 * @property integer $org_id
 * @property integer $org_type_id
 * @property integer $org_level_id
 * @property string $org
 * @property string $org_governing_body
 * @property string $org_ncaa_clearing_house_id
 * @property string $org_website_url
 * @property string $org_twitter_url
 * @property string $org_facebook_url
 * @property string $org_phone_main
 * @property string $org_email_main
 * @property string $org_addr1
 * @property string $org_addr2
 * @property string $org_addr3
 * @property string $org_city
 * @property string $org_state_or_region
 * @property string $org_postal_code
 * @property string $org_country_code_iso3
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * The followings are the available model relations:
 * @property Address[] $addresses
 * @property Camp[] $camps
 * @property Media[] $medias
 * @property Note[] $notes
 * @property OrgLevel $orgLevel
 * @property OrgType $orgType
 * @property PaymentLog[] $paymentLogs
 * @property Person[] $people
 * @property School[] $schools
 * @property Subscription[] $subscriptions
 * @property Team[] $teams
 */
class Org extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'org';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('org', 'required'),
			array('org_type_id, org_level_id, created_by, updated_by, lock', 'numerical', 'integerOnly'=>true),
			array('org, org_addr1, org_addr2, org_addr3', 'length', 'max'=>100),
			array('org_governing_body, org_state_or_region', 'length', 'max'=>45),
			array('org_ncaa_clearing_house_id, org_phone_main, org_postal_code', 'length', 'max'=>25),
			array('org_website_url, org_twitter_url, org_facebook_url', 'length', 'max'=>150),
			array('org_email_main, org_city', 'length', 'max'=>75),
			array('org_country_code_iso3', 'length', 'max'=>3),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('org_id, org_type_id, org_level_id, org, org_governing_body, org_ncaa_clearing_house_id, org_website_url, org_twitter_url, org_facebook_url, org_phone_main, org_email_main, org_addr1, org_addr2, org_addr3, org_city, org_state_or_region, org_postal_code, org_country_code_iso3, created_at, updated_at, created_by, updated_by, lock', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::HAS_MANY, 'Address', 'org_id'),
			'camps' => array(self::HAS_MANY, 'Camp', 'org_id'),
			'medias' => array(self::HAS_MANY, 'Media', 'org_id'),
			'notes' => array(self::HAS_MANY, 'Note', 'org_id'),
			'orgLevel' => array(self::BELONGS_TO, 'OrgLevel', 'org_level_id'),
			'orgType' => array(self::BELONGS_TO, 'OrgType', 'org_type_id'),
			'paymentLogs' => array(self::HAS_MANY, 'PaymentLog', 'org_id'),
			'people' => array(self::HAS_MANY, 'Person', 'org_id'),
			'schools' => array(self::HAS_MANY, 'School', 'org_id'),
			'subscriptions' => array(self::HAS_MANY, 'Subscription', 'org_id'),
			'teams' => array(self::HAS_MANY, 'Team', 'org_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'org_id' => 'Org',
			'org_type_id' => 'Org Type',
			'org_level_id' => 'Org Level',
			'org' => 'Org',
			'org_governing_body' => 'Org Governing Body',
			'org_ncaa_clearing_house_id' => 'Org Ncaa Clearing House',
			'org_website_url' => 'Org Website Url',
			'org_twitter_url' => 'Org Twitter Url',
			'org_facebook_url' => 'Org Facebook Url',
			'org_phone_main' => 'Org Phone Main',
			'org_email_main' => 'Org Email Main',
			'org_addr1' => 'Org Addr1',
			'org_addr2' => 'Org Addr2',
			'org_addr3' => 'Org Addr3',
			'org_city' => 'Org City',
			'org_state_or_region' => 'Org State Or Region',
			'org_postal_code' => 'Org Postal Code',
			'org_country_code_iso3' => 'Org Country Code Iso3',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'lock' => 'Lock',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('org_id',$this->org_id);
		$criteria->compare('org_type_id',$this->org_type_id);
		$criteria->compare('org_level_id',$this->org_level_id);
		$criteria->compare('org',$this->org,true);
		$criteria->compare('org_governing_body',$this->org_governing_body,true);
		$criteria->compare('org_ncaa_clearing_house_id',$this->org_ncaa_clearing_house_id,true);
		$criteria->compare('org_website_url',$this->org_website_url,true);
		$criteria->compare('org_twitter_url',$this->org_twitter_url,true);
		$criteria->compare('org_facebook_url',$this->org_facebook_url,true);
		$criteria->compare('org_phone_main',$this->org_phone_main,true);
		$criteria->compare('org_email_main',$this->org_email_main,true);
		$criteria->compare('org_addr1',$this->org_addr1,true);
		$criteria->compare('org_addr2',$this->org_addr2,true);
		$criteria->compare('org_addr3',$this->org_addr3,true);
		$criteria->compare('org_city',$this->org_city,true);
		$criteria->compare('org_state_or_region',$this->org_state_or_region,true);
		$criteria->compare('org_postal_code',$this->org_postal_code,true);
		$criteria->compare('org_country_code_iso3',$this->org_country_code_iso3,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('lock',$this->lock);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Org the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
