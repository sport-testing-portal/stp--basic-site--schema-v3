<?php

Yii::import('application.models._base.BaseOrg');

class Org extends BaseOrg
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    
    // Added by dbyrd via /protected/extensions/giix-core/giixModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via /protected/extensions/giix-core/giixModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->created_at = date('Y-m-d H:i:s');
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->created_by = (int)Yii::app()->user->getId();
            $this->updated_by = (int)Yii::app()->user->getId();
        } else {
            $this->updated_dt = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();            
        }
        return parent::beforeSave();
    }    
}

 