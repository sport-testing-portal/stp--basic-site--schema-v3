<?php

Yii::import('application.models._base.BaseVwTeam');

class VwTeam extends BaseVwTeam
{
	const MILE_FACTOR=69.1;
	const KILOMETER_FACTOR=111.045;

    /**
     * @return VwTeam
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'VwTeam|VwTeams', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Select data by distance
	 * @param decimal $origLat
	 * @param decimal $origLong
	 * @param decimal $maxDistance
	 * @param string $rtnKilometers
	 * @param string $rtnAssocArrYN
	 * @return CActiveRecord | array[]
	 * @internal An index optimized select calcs in 0.034 secs and fetches dataset in 0.005 secs
	 * @see: BaseModel::convertPostalCodeToLatLong()
	 */
	protected function fetchByDistance($latlongArr, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {

		if ($rtnKilometers == 'Y'){
			$distance_return_type_factor = KILOMETER_FACTOR;
		} else {
			$distance_return_type_factor = MILES_FACTOR;
		}

		$distance_calc = "$distance_return_type_factor * DEGREES(ACOS(COS(RADIANS(latpoint)) "
			."* COS(RADIANS(latitude)) "
			."* COS(RADIANS(longpoint) - RADIANS(longitude)) "
			." + SIN(RADIANS(latpoint)) "
			." * SIN(RADIANS(latitude)))) AS distance ";

        $query = ""
        . "select c.*  "
        . "	from ".$this->tableName()." t "
        . "	inner join  "
        . "	( "
        . "	select postal_code, place_name, state_name, latitude, longitude, "
        . "	 $distance_calc "
        . "	 from zip_code  "
        . "	 join ( "
        . "		 select  $origLat  AS latpoint,  $origLong AS longpoint "
        . "	   ) AS p ON 1=1 "
        . "		where longitude  "
        . "		BETWEEN longpoint - ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "			AND longpoint + ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "		having distance_in_miles < $maxDistance "
        . "		order by distance_in_miles desc "
        . "	) z on left(t.person_postal_code,5) = z.postal_code "
        . "; "
        ;

		$cmd = Yii::app()->db->createCommand($query);
		if ($rtnAssocArrYN === 'Y'){
			return $cmd->queryAll($fetchAssociative=true);
		} else {
			return $cmd->queryAll();
		}
	}

	/**
	 * Select data by distance
	 * @param decimal $origLat
	 * @param decimal $origLong
	 * @param decimal $maxDistance
	 * @param string $rtnKilometers
	 * @param string $rtnAssocArrYN
	 * @return CActiveRecord | array[]
	 * @internal An index optimized select calcs in 0.034 secs and fetches dataset in 0.005 secs
	 * @see: BaseModel::convertPostalCodeToLatLong()
	 */
	protected function fetchAllByDistance($LatLongArr, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {

		if ($rtnKilometers == 'Y'){
			$distance_return_type_factor = KILOMETER_FACTOR;
		} else {
			$distance_return_type_factor = MILES_FACTOR;
		}

		$distance_calc = "$distance_return_type_factor * DEGREES(ACOS(COS(RADIANS(latpoint)) "
			."* COS(RADIANS(latitude)) "
			."* COS(RADIANS(longpoint) - RADIANS(longitude)) "
			." + SIN(RADIANS(latpoint)) "
			." * SIN(RADIANS(latitude)))) AS distance ";

        $query = ""
        . "select c.*  "
        . "	from ".$this->tableName()." t "
        . "	inner join  "
        . "	( "
        . "	select postal_code, place_name, state_name, latitude, longitude, "
        . "	 $distance_calc "
        . "	 from zip_code  "
        . "	 join ( "
        . "		 select  $origLat  AS latpoint,  $origLong AS longpoint "
        . "	   ) AS p ON 1=1 "
        . "		where longitude  "
        . "		BETWEEN longpoint - ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "			AND longpoint + ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "		having distance_in_miles < $maxDistance "
        . "		order by distance_in_miles desc "
        . "	) z on left(t.person_postal_code,5) = z.postal_code "
        . "; "
        ;

		$cmd = Yii::app()->db->createCommand($query);
		if ($rtnAssocArrYN === 'Y'){
			return $cmd->queryAll($fetchAssociative=true);
		} else {
			return $cmd->queryAll();
		}
	}

	/**
	 *
	 * @param type $coachID
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchIncludingCoach($coachID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        //$criteria->with=array('team');
        //$criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.coach_id',$coachID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort;
        $sort->attributes = array(
            'person_name_full' => array(
            'asc' => 'person_name_full',
            'desc' => 'person_name_full DESC',
            ), '*', /* Treat all other columns normally */
        );
        /* End: Sort on related Model's columns */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }

	/**
	 *
	 * @param type $playerID
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchIncludingPlayer($playerID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        //$criteria->with=array('team');
        //$criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.player_id',$playerID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort;
        $sort->attributes = array(
            'person_name_full' => array(
            'asc' => 'person_name_full',
            'desc' => 'person_name_full DESC',
            ), '*', /* Treat all other columns normally */
        );
        /* End: Sort on related Model's columns */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }

}
