<?php

/**
 * This is the model base class for the table "app_user_profile_picture".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AppUserProfilePicture".
 *
 * Columns in table "app_user_profile_picture" available as properties of the model,
 * followed by relations of table "app_user_profile_picture" available as properties of the model.
 *
 * @property integer $app_user_profile_picture_id
 * @property integer $app_user_id
 * @property integer $app_user_profile_original_picture_id
 * @property string $app_user_profile_picture_filename
 * @property integer $app_user_profile_picture_width
 * @property integer $app_user_profile_picture_height
 * @property string $app_user_profile_picture_mimetype
 * @property string $app_user_profile_picture_contents
 * @property string $created_on
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AppUser $appUser
 */
abstract class BaseAppUserProfilePicture extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'app_user_profile_picture';
    }

    public static function representingColumn() {
        return 'app_user_profile_picture_filename';
    }

    public function rules() {
        return array(
            array(	'app_user_id, app_user_profile_picture_filename, app_user_profile_picture_width, app_user_profile_picture_height, app_user_profile_picture_mimetype, app_user_profile_picture_contents',
					'required',
					'message' => Yii::t('app', 'Field is required')
			),
            array(	'app_user_id, app_user_profile_original_picture_id, app_user_profile_picture_width, app_user_profile_picture_height, created_by, updated_by, lock',
					'numerical',
					'integerOnly'=>true
			),
            array(	'app_user_profile_picture_filename, app_user_profile_picture_mimetype',
					'length',
					'max'=>255,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_at, updated_at',
					'safe'
			),
            array('app_user_profile_original_picture_id, created_by, updated_by, lock, created_at, updated_at',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('app_user_profile_picture_id, app_user_id, app_user_profile_original_picture_id, app_user_profile_picture_filename, app_user_profile_picture_width, app_user_profile_picture_height, app_user_profile_picture_mimetype, app_user_profile_picture_contents, created_on, created_by, updated_by, lock, created_at, updated_at', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'appUser' => array(self::BELONGS_TO, 'AppUser', 'app_user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'app_user_profile_picture_id' => Yii::t('app', 'App User Profile Picture'),
            'app_user_id' => Yii::t('app', 'App User'),
            'app_user_profile_original_picture_id' => Yii::t('app', 'App User Profile Original Picture'),
            'app_user_profile_picture_filename' => Yii::t('app', 'App User Profile Picture Filename'),
            'app_user_profile_picture_width' => Yii::t('app', 'App User Profile Picture Width'),
            'app_user_profile_picture_height' => Yii::t('app', 'App User Profile Picture Height'),
            'app_user_profile_picture_mimetype' => Yii::t('app', 'App User Profile Picture Mimetype'),
            'app_user_profile_picture_contents' => Yii::t('app', 'App User Profile Picture Contents'),
            'created_on' => Yii::t('app', 'Created On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'lock' => Yii::t('app', 'Lock'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'appUser' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('app_user_profile_picture_id', $this->app_user_profile_picture_id);
        $criteria->compare('app_user_id', $this->app_user_id);
        $criteria->compare('app_user_profile_original_picture_id', $this->app_user_profile_original_picture_id);
        $criteria->compare('app_user_profile_picture_filename', $this->app_user_profile_picture_filename, true);
        $criteria->compare('app_user_profile_picture_width', $this->app_user_profile_picture_width);
        $criteria->compare('app_user_profile_picture_height', $this->app_user_profile_picture_height);
        $criteria->compare('app_user_profile_picture_mimetype', $this->app_user_profile_picture_mimetype, true);
        $criteria->compare('app_user_profile_picture_contents', $this->app_user_profile_picture_contents, true);
        $criteria->compare('created_on', $this->created_on, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('lock', $this->lock);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'timestampExpression' => new CDbExpression('NOW()'),
                'setUpdateOnCreate'   => true
            )
        ), parent::behaviors());
    }
}