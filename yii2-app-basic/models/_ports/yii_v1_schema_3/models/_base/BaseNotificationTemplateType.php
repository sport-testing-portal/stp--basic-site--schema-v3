<?php

/**
 * This is the model base class for the table "notification_template_type".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "NotificationTemplateType".
 *
 * Columns in table "notification_template_type" available as properties of the model,
 * followed by relations of table "notification_template_type" available as properties of the model.
 *
 * @property integer $notification_template_type_id
 * @property string $notification_template_type
 * @property string $notification_template_type_desc_short
 * @property string $notification_template_type_desc_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property NotificationTemplate[] $notificationTemplates
 */
abstract class BaseNotificationTemplateType extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'notification_template_type';
    }

    public static function representingColumn() {
        return 'notification_template_type';
    }

    public function rules() {
        return array(
            array(	'created_by, updated_by, lock',
					'numerical',
					'integerOnly'=>true
			),
            array(	'notification_template_type',
					'length',
					'max'=>45,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'notification_template_type_desc_short',
					'length',
					'max'=>75,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'notification_template_type_desc_long',
					'length',
					'max'=>175,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_at, updated_at',
					'safe'
			),
            array('notification_template_type, notification_template_type_desc_short, notification_template_type_desc_long, created_at, updated_at, created_by, updated_by, lock',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('notification_template_type_id, notification_template_type, notification_template_type_desc_short, notification_template_type_desc_long, created_at, updated_at, created_by, updated_by, lock', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'notificationTemplates' => array(self::HAS_MANY, 'NotificationTemplate', 'notification_template_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'notification_template_type_id' => Yii::t('app', 'Notification Template Type'),
            'notification_template_type' => Yii::t('app', 'Notification Template Type'),
            'notification_template_type_desc_short' => Yii::t('app', 'Notification Template Type Desc Short'),
            'notification_template_type_desc_long' => Yii::t('app', 'Notification Template Type Desc Long'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'lock' => Yii::t('app', 'Lock'),
            'notificationTemplates' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('notification_template_type_id', $this->notification_template_type_id);
        $criteria->compare('notification_template_type', $this->notification_template_type, true);
        $criteria->compare('notification_template_type_desc_short', $this->notification_template_type_desc_short, true);
        $criteria->compare('notification_template_type_desc_long', $this->notification_template_type_desc_long, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('lock', $this->lock);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'timestampExpression' => new CDbExpression('NOW()'),
                'setUpdateOnCreate'   => true
            )
        ), parent::behaviors());
    }
}