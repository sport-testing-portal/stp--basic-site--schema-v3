<?php

/**
 * This is the model base class for the table "tbl_migration".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "TblMigration".
 *
 * Columns in table "tbl_migration" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $version
 * @property integer $apply_time
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 */
abstract class BaseTblMigration extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'tbl_migration';
    }

    public static function representingColumn() {
        return 'version';
    }

    public function rules() {
        return array(
            array(	'version',
					'required',
					'message' => Yii::t('app', 'Field is required')
			),
            array(	'apply_time, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'version',
					'length',
					'max'=>255,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_at, updated_at',
					'safe'
			),
            array('apply_time, created_at, updated_at, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('version, apply_time, created_at, updated_at, created_by, updated_by', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'version' => Yii::t('app', 'Version'),
            'apply_time' => Yii::t('app', 'Apply Time'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('version', $this->version, true);
        $criteria->compare('apply_time', $this->apply_time);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'timestampExpression' => new CDbExpression('NOW()'),
                'setUpdateOnCreate'   => true
            )
        ), parent::behaviors());
    }
}