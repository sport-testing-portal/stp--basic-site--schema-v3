<?php

/**
 * This is the model base class for the table "address_type".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AddressType".
 *
 * Columns in table "address_type" available as properties of the model,
 * followed by relations of table "address_type" available as properties of the model.
 *
 * @property integer $address_type_id
 * @property string $address_type
 * @property string $address_type_description_short
 * @property string $address_type_description_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property Address[] $addresses
 */
abstract class BaseAddressType extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'address_type';
    }

    public static function representingColumn() {
        return 'address_type';
    }

    public function rules() {
        return array(
            array(	'address_type',
					'required',
					'message' => Yii::t('app', 'Field is required')
			),
            array(	'created_by, updated_by, lock',
					'numerical',
					'integerOnly'=>true
			),
            array(	'address_type, address_type_description_short',
					'length',
					'max'=>45,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'address_type_description_long',
					'length',
					'max'=>150,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_at, updated_at',
					'safe'
			),
            array('address_type_description_short, address_type_description_long, created_at, updated_at, created_by, updated_by, lock',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('address_type_id, address_type, address_type_description_short, address_type_description_long, created_at, updated_at, created_by, updated_by, lock', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'addresses' => array(self::HAS_MANY, 'Address', 'address_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'address_type_id' => Yii::t('app', 'Address Type'),
            'address_type' => Yii::t('app', 'Address Type'),
            'address_type_description_short' => Yii::t('app', 'Address Type Description Short'),
            'address_type_description_long' => Yii::t('app', 'Address Type Description Long'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'lock' => Yii::t('app', 'Lock'),
            'addresses' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('address_type_id', $this->address_type_id);
        $criteria->compare('address_type', $this->address_type, true);
        $criteria->compare('address_type_description_short', $this->address_type_description_short, true);
        $criteria->compare('address_type_description_long', $this->address_type_description_long, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('lock', $this->lock);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'timestampExpression' => new CDbExpression('NOW()'),
                'setUpdateOnCreate'   => true
            )
        ), parent::behaviors());
    }
}