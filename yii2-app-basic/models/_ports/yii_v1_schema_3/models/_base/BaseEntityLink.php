<?php

/**
 * This is the model base class for the table "entity_link".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "EntityLink".
 *
 * Columns in table "entity_link" available as properties of the model,
 * followed by relations of table "entity_link" available as properties of the model.
 *
 * @property integer $entity_link_id
 * @property integer $entity_link_type_id
 * @property integer $entity1_id
 * @property integer $entity2_id
 * @property string $entity_link_comment
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property EntityLinkType $entityLinkType
 */
abstract class BaseEntityLink extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'entity_link';
    }

    public static function representingColumn() {
        return 'entity_link_comment';
    }

    public function rules() {
        return array(
            array(	'entity_link_type_id, entity1_id, entity2_id, created_by, updated_by, lock',
					'numerical',
					'integerOnly'=>true
			),
            array(	'entity_link_comment',
					'length',
					'max'=>255,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_at, updated_at',
					'safe'
			),
            array('entity_link_type_id, entity1_id, entity2_id, entity_link_comment, created_at, updated_at, created_by, updated_by, lock',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('entity_link_id, entity_link_type_id, entity1_id, entity2_id, entity_link_comment, created_at, updated_at, created_by, updated_by, lock', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'entityLinkType' => array(self::BELONGS_TO, 'EntityLinkType', 'entity_link_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'entity_link_id' => Yii::t('app', 'Entity Link'),
            'entity_link_type_id' => Yii::t('app', 'Entity Link Type'),
            'entity1_id' => Yii::t('app', 'Entity1'),
            'entity2_id' => Yii::t('app', 'Entity2'),
            'entity_link_comment' => Yii::t('app', 'Entity Link Comment'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'lock' => Yii::t('app', 'Lock'),
            'entityLinkType' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('entity_link_id', $this->entity_link_id);
        $criteria->compare('entity_link_type_id', $this->entity_link_type_id);
        $criteria->compare('entity1_id', $this->entity1_id);
        $criteria->compare('entity2_id', $this->entity2_id);
        $criteria->compare('entity_link_comment', $this->entity_link_comment, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('lock', $this->lock);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'timestampExpression' => new CDbExpression('NOW()'),
                'setUpdateOnCreate'   => true
            )
        ), parent::behaviors());
    }
}