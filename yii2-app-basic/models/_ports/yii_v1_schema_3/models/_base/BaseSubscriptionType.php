<?php

/**
 * This is the model base class for the table "subscription_type".
 * DO NOT MODIFY THIS FILE! It is automatically generated by AweCrud.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SubscriptionType".
 *
 * Columns in table "subscription_type" available as properties of the model,
 * followed by relations of table "subscription_type" available as properties of the model.
 *
 * @property integer $subscription_type_id
 * @property string $subscription_type
 * @property string $subscription_desc_short
 * @property string $subscription_desc_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property Subscription[] $subscriptions
 * @property SubscriptionLog[] $subscriptionLogs
 */
abstract class BaseSubscriptionType extends AweActiveRecord {

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'subscription_type';
    }

    public static function representingColumn() {
        return 'subscription_type';
    }

    public function rules() {
        return array(
            array(	'created_by, updated_by, lock',
					'numerical',
					'integerOnly'=>true
			),
            array(	'subscription_type',
					'length',
					'max'=>75,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'subscription_desc_short',
					'length',
					'max'=>45,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'subscription_desc_long',
					'length',
					'max'=>150,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_at, updated_at',
					'safe'
			),
            array('subscription_type, subscription_desc_short, subscription_desc_long, created_at, updated_at, created_by, updated_by, lock',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            array('subscription_type_id, subscription_type, subscription_desc_short, subscription_desc_long, created_at, updated_at, created_by, updated_by, lock', 'safe', 'on'=>'search'),
        );
    }

    public function relations() {
        return array(
            'subscriptions' => array(self::HAS_MANY, 'Subscription', 'subscription_type_id'),
            'subscriptionLogs' => array(self::HAS_MANY, 'SubscriptionLog', 'subscription_type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'subscription_type_id' => Yii::t('app', 'Subscription Type'),
            'subscription_type' => Yii::t('app', 'Subscription Type'),
            'subscription_desc_short' => Yii::t('app', 'Subscription Desc Short'),
            'subscription_desc_long' => Yii::t('app', 'Subscription Desc Long'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'lock' => Yii::t('app', 'Lock'),
            'subscriptions' => null,
            'subscriptionLogs' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('subscription_type_id', $this->subscription_type_id);
        $criteria->compare('subscription_type', $this->subscription_type, true);
        $criteria->compare('subscription_desc_short', $this->subscription_desc_short, true);
        $criteria->compare('subscription_desc_long', $this->subscription_desc_long, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('created_by', $this->created_by);
        $criteria->compare('updated_by', $this->updated_by);
        $criteria->compare('lock', $this->lock);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function behaviors() {
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'timestampExpression' => new CDbExpression('NOW()'),
                'setUpdateOnCreate'   => true
            )
        ), parent::behaviors());
    }
}