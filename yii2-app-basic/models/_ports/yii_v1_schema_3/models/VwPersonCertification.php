<?php

Yii::import('application.models._base.BaseVwPersonCertification');

class VwPersonCertification extends BaseVwPersonCertification
{
    /**
     * @return VwPersonCertification
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Person Certification|Person Certifications', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    public function primaryKey()
    {
        //return 'person_certification_id';
		//return 'id' ; // 'person_certification_type_id' + '-', + 'person_certification_id';
		return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
		return false;
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }
	

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
				'vwPersonCertification_id'						=> Yii::t('app', 'CID'),
                'person'										=> Yii::t('app', 'Person'),
                'person_type'									=> Yii::t('app', 'Person Type'),
                'person_certification_id'						=> Yii::t('app', 'CertID'),
                'person_id'										=> Yii::t('app', 'PersonID'),
                'person_certification_type_id'					=> Yii::t('app', 'CertTypeID'),
                'person_certification_year'						=> Yii::t('app', 'Cert Year'),
                'person_type_id'								=> Yii::t('app', 'PersonTypeID'),
                'person_certification_type_name'				=> Yii::t('app', 'Type'),
                'person_certification_subtype_name'				=> Yii::t('app', 'SubType'),
                'person_certification_type_display_order'		=> Yii::t('app', 'Display Order'),
                'person_certification_type_description_short'	=> Yii::t('app', 'Desc Short'),
                'person_certification_type_description_long'	=> Yii::t('app', 'Desc Long'),
                'created_dt'									=> Yii::t('app', 'Created Dt'),
                'updated_dt'									=> Yii::t('app', 'Updated Dt'),
                'created_by'									=> Yii::t('app', 'Created By'),
                'updated_by'									=> Yii::t('app', 'Updated By'),
        );
    }

	/**
	 *
	 * @param int|string|null $person_id
	 * @param string|null $certTypeName 'USSF'|'NSCAA'|'UEFA'
	 * @return CActiveRecord PersonCertification
	 * @internal Scenario 1 - Return a single active record for data binding controls.
	 * @internal Scenario 2 - Return an array of active records for data binding controls.
	 * @example VwPersonCertification::fetchAllCertifications(159, 'USSF');
	 */
	public static function fetchAllCertifications($person_id=null, $certTypeName=null) {

		if ((int)$person_id > 0 && ! empty($certTypeName)){
			$scenario = 'bindOneRow';
		} elseif ((int)$person_id > 0 && empty($certTypeName)){
			$scenario = 'bindAllRows';
		}

		$placeHolders = ['USSF'=>0,'NSCAA'=>0,'UEFA'=>0,'Special Topics'=>32,'Other'=>0];

		if ($scenario == 'bindOneRow'){

			$condition	= 'person_certification_type_name="' . $certTypeName . '"'
					. ' AND person_id = :person_id' ;
			$params		= [':person_id'=>$person_id];
			$certification = VwPersonCertification::model()
				->find($condition, $params);
			if (is_null($certification) ){
				return PersonCertification::insertPlaceHolderRow($person_id, $certTypeName);
//				$condition = 'person_certification_type_name="' . $certTypeName . '"';
//				$certifications = VwPersonCertification::model()
//					->findAll($condition);
			} else {
				return $certification;
			}

		} elseif ($scenario == 'bindAllRows'){

			$criteria = new CDbCriteria;
			$criteria->order = 'person_certification_type_name, person_certification_type_display_order ASC';
			$attributes		= ['person_id'=>$person_id];
			$certifications = VwPersonCertification::model()
				->findAllByAttributes($attributes,$criteria);
			return $certifications;
		}
		//person_certification_type_description_long != "placeholder"



//		if (count($certifications) == 0 && !empty($certTypeName) ){
//				$condition = 'person_certification_type_name="' . $certTypeName . '"';
//				$certifications = VwPersonCertification::model()
//					->findAll($condition);
//		}
//		if(count($certifications) == 0){
//			// no rows found. return an empty model
//			$certification = new VwPersonCertification();
//			return $certification;
//		}elseif (!is_null($certifications)){
//			if (count($certifications) == 1){
//				// return a single CActiveRecord
//				return $certifications[0];
//			} else {
//				// return the array of CActiveRecords
//				return $certifications;
//			}
//		}
	}

	/**
	 *
	 * @param int|string|null $person_id
	 * @param string|null $certTypeName 'USSF'|'NSCAA'|'UEFA'
	 * @return CActiveRecord PersonCertification
	 * @internal Scenario 1 - Return a single active record for data binding controls.
	 * @internal Scenario 2 - Return an array of active records for data binding controls.
	 * @example VwPersonCertification::fetchAllCertifications(159, 'USSF');
	 */
	public static function fetchAllCertificationsv01($person_id=null, $certTypeName=null) {

		if ((int)$person_id > 0 && ! empty($certTypeName)){

			$condition	= 'person_certification_type_name="' . $certTypeName . '"'
					. ' AND person_id = :person_id' ;
			$params		= [':person_id'=>$person_id];
			$certifications = VwPersonCertification::model()
				->findAll($condition, $params);

		} elseif ((int)$person_id > 0 && empty($certTypeName)){

			$criteria = new CDbCriteria;
			$criteria->order = 'person_certification_type_display_order ASC';
			$attributes		= ['person_id'=>$person_id];
			$certifications = VwPersonCertification::model()
				->findAllByAttributes($attributes,$criteria);
		}

		if (count($certifications) == 0 && !empty($certTypeName) ){
				$condition = 'person_certification_type_name="' . $certTypeName . '"';
				$certifications = VwPersonCertification::model()
					->findAll($condition);
		}
		if(count($certifications) == 0){
			// no rows found. return an empty model
			$certification = new VwPersonCertification();
			return $certification;
		}elseif (!is_null($certifications)){
			if (count($certifications) == 1){
				// return a single CActiveRecord
				return $certifications[0];
			} else {
				// return the array of CActiveRecords
				return $certifications;
			}
		}
	}
	
	
	/**
	 * $person_id + $certTypeName Returns single assoc array of certification + certificationType field=>value
	 * @param int|string|null $person_id
	 * @param string|null $certTypeName 'USSF'|'NSCAA'|'UEFA'
	 * @return array[] PersonCertification + PersonCertificationType
	 * @internal $person_id + $certTypeName Returns single assoc array of certification + certificationType field=>value
	 * @internal Scenario 2 - Return an array of active records for data binding controls.
	 * @example $array = VwPersonCertification::fetchCertificationsArray(159, 'USSF');
	 * @example YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($array, 10, true);
	 */
	public static function fetchCertificationsArray($person_id=null, $certTypeName=null) {
		$out = [];

		$certifications = self::fetchAllCertifications($person_id, $certTypeName);

		if (!is_null($certifications)){
			if (count($certifications) > 1){
				foreach ($certifications as $certification){
					$out[] = $certification->attributes;
				}

			} else {
				// A count of one is assumed
				$out = $certifications->attributes;
			}
		}
		return $out;
	}
}
