<?php

Yii::import('application.models._base.BasePerson');

class PersonAthlete extends BasePerson
{

	public static $p_tableName  = 'person';
	public static $p_primaryKey = 'person_id';
	public static $p_textColumn = 'person_name_full';
	public static $p_modelName  = 'Person';

    /**
     * @return Person
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Person|People', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

    public static function representingColumn() {
        return 'person_name_full';
    }

	/**
	 * Stores this table and its parent table - Org
	 * @param array[] $fieldValueArray
	 * @return int $person_id
	 * @version 1.0.0
	 * @internal CEC v2.0.0
	 * @internal dev sts = ready for testing
	 */
	public static function StorePersonWithOrg($fieldValueArray){
		if (!is_array($fieldValueArray)){
			throw new CException('field_value array is a required parameter', 707, null);
		}
		$fva = $fieldValueArray;

		$fkey_name = 'org_id';

		if (array_key_exists($fkey_name, $fva) && (int)$fva[$fkey_name] > 0) {
			$org_id = (int)$fva[$fkey_name];
			// update org
			$org = Org::model()->findByPk($org_id);
			if (isset($org)){
				$org_exists = 'Y';
				$org->setAttributes($fva);
				$org->validate();
				$org->save();
				$person_id = self::Store($fva); // make updates and bail
				return $person_id;
			} else {
				$org_exists = 'N';
			}
		}

		// see if requirements for an org insert have been met
		// $parent_requirements_met_yn = $this->orgInsertRequirementsMetYN($fva);
		$parent_requirements_met_yn = self::orgInsertRequirementsMetYN($fva);
		if ( $parent_requirements_met_yn == 'Y'){
			$org_exists = 'N';
			$org = new Org();
			$org->setAttributes($fva);
			$org->validate();
			if ($org->save()){
				$org_id = (int)$org->attributes['org_id'];
				if ($org_id > 0){
					$fva['org_id'] = $org_id;
					$person_id = $this->Store($fva);
				}
			}
		} elseif( $parent_requirements_met_yn == 'N') {
			$person_id = self::Store($fieldValueArray);
		} else {
			throw new CException('Invalid use case: parent_requirmements_met');
		}
		return $person_id;
	}

	/**
	 * Tests the fva to see if an insert can be performed
	 * @param array $field_value_array
	 * @return string Y=Ok to insert, N=insert forbidden
	 * @internal asserts org, and org_type_id are present OR an org_id > 0 is present
	 * @internal dev sts = rft
	 * @version 1.0.0
	 * @internal Development Status = Golden!
	 */
	protected static function orgInsertRequirementsMetYN($field_value_array) {
		// @todo: see if requirements for an org insert have been met
		// The requirements below are for inserting an org that is to be linked to a person. At the SQL level only org is required for an org row.
		if (!is_array($field_value_array)){
			throw new CException("fieldValueArray is a required parameter", 707);
		}
		$fva = $field_value_array;
		$parent_requirements_met_yn = 'N';

		// assert that an org_id was not passed in
		if (array_key_exists('org_id', $fva) && (int)($fva['org_id'] > 0)) {
			return $parent_requirements_met_yn='N'; // no insert allowed
		}

		// get org values to test from the fva
		if (array_key_exists('org', $fva) && strlen($fva['org'] > 0)) {
			$org = $fva['org'];
		} else {
			$org = '';
		}
		if (array_key_exists('org_type_id', $fva) && (int)$fva['org_type_id'] > 0) {
			$org_type_id = (int)$fva['org_type_id'];
		} else {
			$org_type_id = 0;
		}

		// Test for valid values in the fva
		if (strlen($org) > 1 && (int)$org_type_id > 0){
			$parent_requirements_met_yn = 'Y'; // insert can proceed
		}
		return $parent_requirements_met_yn;
	}

	/**
	 * Handle person_name_full parsing when person_name_* parts fields have not been supplied
	 * @param array[] $field_value_array
	 * @throws CException
	 * @verson 1.0.0
	 * @internal Called by Person->Store()
	 * @internal Development Status = Golden!
	 * @author Dana Byrd <dana@globalsoccermetrix.com>
	 */
	protected static function personNameHandler($field_value_array) {
		if (!is_array($field_value_array)){
			throw new CException("fieldValueArray is a required parameter", 707);
		}
		$fva = $field_value_array;
		$pkName = self::model()->primaryKey();
		if(isset( $fva[$pkName]) && $fva[$pkName] > 0){
			$pkValue = $fva[$pkName];
		} else {
			$pkValue = '';
		}
		// Handle person name parts
		if (array_key_exists('person_name_full', $fva)
			&& (
				! array_key_exists('person_name_first', $fva)
				||
				! array_key_exists('person_name_last', $fva)
				)
		){
			$name_parts = Bcommon::splitNameIntoParts($fva['person_name_full']);

			if( ! array_key_exists('person_name_first', $fva) ){
				$fva['person_name_first'] = $name_parts["first_name"];
			}
			if( ! array_key_exists('person_name_last', $fva) ){
				$fva['person_name_last'] = $name_parts["last_name"];
			}
			if (array_key_exists("middle_name", $name_parts) && ! array_key_exists("person_name_middle", $fva)){
				$fva['person_name_middle'] = $name_parts["middle_name"];
			}
			if (array_key_exists("name_suffix", $name_parts) && ! array_key_exists("person_name_suffix", $fva)){
				$fva['person_name_suffix'] = $name_parts["name_suffix"];
			}

		} else {
			// All is OK already. No name parts processing needed.
			return $fva;
		}


		if ((int)$pkValue >0){
			// field names are not required on partial row updates
			return $fva;
		}
		// Assert that the name parts fields have been added for an insert eg pkValue=0
		if (array_key_exists('person_name_first', $fva) == false
		  || array_key_exists('person_name_last', $fva) == false)
		{
			throw new CException("Person's first name and last name are required!");
		} elseif (array_key_exists('person_type_id', $fva) == false) {
			$fva['person_type_id'] = 1; // 1=player
		}
		return $fva;
	}

	/**
	 * Store Person using an fva
     * @return int $person_id
	 * @internal Insert or update a row, add a row in a parent table if needed
	 * @internal dev sts = rft
	 * @version
	 * @internal Original source = Player.php
	 * @todo add array cast to the param, get scmda, update the scmda and return it
     */
    public static function Store($fieldValueArray)
    {
		if (!is_array($fieldValueArray)){
			throw new CException("fieldValueArray is a required parameter", 707);
		}
		//$pkName = self::model()->primaryKey();
		$errors=[];

		$validate = true;
		// handle fields coming from the registration page that have different column names than the 'Person' model attributes eg person name & email
		if (array_key_exists('firstName', $fieldValueArray) && array_key_exists('person_name_first', $fieldValueArray) == false){
			$fieldValueArray['person_name_first'] = $fieldValueArray['firstName'];
		}
		if (array_key_exists('lastName', $fieldValueArray) && array_key_exists('person_name_last', $fieldValueArray) == false){
			$fieldValueArray['person_name_last'] = $fieldValueArray['lastName'];
		}
		if (array_key_exists('email', $fieldValueArray) && array_key_exists('person_email_personal', $fieldValueArray) == false){
			$fieldValueArray['person_email_personal'] = $fieldValueArray['email'];
		}
		$fva	= self::personNameHandler($fieldValueArray); // preserve incoming parameter values for debugging
		//$pk_name = $this->primaryKey(); // can't use $this-> in a static method
		$pk_name = 'person_id';
		$class = __CLASS__;
		// run a search by primary key
		$row_found_yn = "N";
		if (array_key_exists($pk_name, $fva)) {
			$pkey_val = $fva[$pk_name];
			//$row = Player::model()->findByPk($pkey_val); // returns object
			$row = $class::model()->findByPk($pkey_val); // returns object
			if (! empty($row)){
				$row_found_yn = "Y";
			}
		}

		if ($row_found_yn === 'Y'){
			// Update
			// Warning! Assumption!! Next line assumes that all values passed in are mass assignable!
			// see if anything needs to be updated
			$bfr_upd = $row->attributes;
			//$row->attributes = $fva;
			$row->setAttributes($fva);
			$aft_upd = $row->attributes;
			//$diff = array_diff($bfr_upd,$row->attributes); @todo make notes re below
			$diffShowingOldValuesReplacedByTheUpdate = array_diff($bfr_upd,$aft_upd);
			$diffInboundData = array_diff($aft_upd, $bfr_upd);
			// @todo streamline the diffs with the CEC 3.0
			if (count($diffInboundData, COUNT_RECURSIVE) == 0){
				// before and after vals are the same. there is no need to save the update
				return (int)$row->attributes[$pk_name];
			}
			// The values must be different. Save the model.
			if ($row->save($validate)){
				return (int)$row->attributes[$pk_name];
			} else {
				$errors['Person'] = $row->getErrors();
				return 0;
			}

		} elseif ($row_found_yn === 'N'){
			// Insert
			$model = new $class();
			$model->unsetAttributes(); // clears default values
			$model->attributes = $fva;
			if ($model->save($validate)){
				$id = (int)$model->attributes[$pk_name];
			} else {
				$id = 0;
			}
			return (int)$id;
		}

		return false;
    }

    /**
	 * @param int
     * @return int
	 * Added locally as by dbyrd as a test for via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
     */
    public function RowCountByPersonId($person_id)
    {
		$class = __CLASS__;
		$rows  = $class::model()->findAllByAttributes(array("person_id"=>$person_id));

		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rows, 10, true);
		if (is_array($rows) && count($rows, COUNT_RECURSIVE) > 0){
			$cnt = count($rows[0]);
		} else {
			$cnt = 0;
		}
		//$cnt = count($rows[0]);
		return $cnt;

    }

	/**
	 *
	 * @param array[]|CActiveRecord $rows Expects an array of CActiveRecord
	 */
	protected static function deleteRows($rows) {
		$deletes=[];
		if (empty($rows)){
			return [];
		}
		if (is_object($rows)){
			// its a CActiveArray object
			$tableName  = $rows->tableName();
			$pk			= $rows->getPrimaryKey();
			$deleted	= $rows->delete();
			$deletes[$pk] = $deleted;

		} elseif (is_array($rows)){
			$tableName  = $rows[0]->tableName();
			foreach ($rows as $row) {
				$pk				= $row->getPrimaryKey();
				$deleted		= $row->delete();
				$deletes[$pk]	= $deleted;
			}
		}

		return [$tableName=>$deletes];
	}


	/**
	 *
	 * @param array[]|CActiveRecord $rows Expects an array of CActiveRecord
	 */
	protected static function deleteNestedRows($rows,$relations=[]) {
		//if ($rows)
		if (empty($rows)){
			return [];
		}
		if (is_string($relations)){
			$relations = explode(',', $relations);
		}
		//$typeof = (($rows instanceof CActiveRecord) ? 'CActiveRecord':'unknown');

		$deletes=[];
		$deleteSummary=[];
		$tableName  = $rows[0]->tableName();

		foreach ($rows as $row) {

			foreach ($relations as $relation) { // [relationName, relationName]

				if ($row->hasRelated($relation) == false){
					// @todo: loaded the related values
					$related	 = $row->getRelated($relation, true);
					if (! empty($related)){
						$childPk = $related[0]->getPrimaryKey();
						foreach ($related as $childRow){
							$childPk = $childRow->getPrimaryKey();
							$deleted = $childRow->delete();
							$deletes[$childPk] = $deleted;
						}
						$deleteSummary[$relation] = $deletes;
					}
				}
			}
//			// now delete the parent row
//				$pk			= $row->getPrimaryKey(); // returns string version of pkey
//				//$pk			= $row->attributes[$pk_name];
//				$deleted	= $row->delete();
//				$deletes[$pk] = $deleted;
		}

		// @todo: After all relations are deleted then delete the parent rows
		$delRows = self::deleteRows($rows);

		return [$delRows,$deleteSummary];
	}

	/**
	 *
	 * @param type $id
	 * @version 0.3 Uses generic 'parts' for the deletion work
	 */
	public static function cascadeDeletePerson($id){
//		$debug_local = true;

		$Person = Person::model()->findByPk($id);
		$Summaries = TestEvalSummaryLog::model()->findAllByAttributes(['person_id'=>$id]);
		$summaries = self::deleteNestedRows($Summaries, 'testEvalDetailLogs'); //testEvalDetailLogs
//		if ($debug_local){
//			$msg = "dumping summary deletes";
//			YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
//			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($summaries, 10, true);
//		}
		$Players = Player::model()->findAllByAttributes(['person_id'=>$id]);
		$players = self::deleteNestedRows($Players, 'teamPlayers');
		//$players = self::deleteRows($Players);
//		$msg = "dumping player deletes";
//		YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($players, 10, true);

		$Coaches = Coach::model()->findAllByAttributes(['person_id'=>$id]);
		$coaches = self::deleteNestedRows($Coaches, 'teamCoaches');
//		$msg = "dumping coach deletes";
//		YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
//		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($coaches, 10, true);

		$person = self::deleteRows($Person);

		return [$person, $summaries, $players, $coaches];
	}

	/**
	 *
	 * Deletes all linked models descending from a single person. Deletes person, player, team_player, summary, detail.
	 * @param int $id person_id
	 * @return string $result
	 * @internal Development Status = ready for testing (rft)
	 * @example http://gsm-public:8097/index-test.php/Person/CascadeDeletePerson/25
	 * @example http://gsm-public:8097/index-test.php/Person/CascadeDeletePerson/25?XDEBUG_SESSION_START=netbeans-xdebug&fileName=SpecialReport-dms-july2014-camps.1412526854.csv&forceCascade=false
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function cascadeDeletePerson_v2($id)
	{
		$delete_status=[];
		$Person = Person::model()->findByPk($id);
		$deleted_summary=[];

		$deletes = [];
		$noDelete=[];
		$Summaries = TestEvalSummaryLog::model()->findAllByAttributes(['person_id'=>$id]);
		foreach ($Summaries as $Summary) {
			$tesl_id = $Summary->test_eval_summary_log_id;
			$params = ['test_eval_summary_log_id'=>$tesl_id];
			$TestEvalDetailLog = TestEvalDetailLog::model()->findAllByAttributes($params);
			$tedl_deletes = [];
			foreach ($TestEvalDetailLog as $tedl){
				$del_id = $tedl->test_eval_detail_log_id;
				$deleted = $tedl->delete();
				if ($deleted){
					$tedl_deletes[] =  $del_id;
				} else {
					//$noDelete['test results'][] = $del_id;
				}
			}
			$delete_status['test results'] = $tedl_deletes;


			$del_id = $Summary->test_eval_summary_log_id;
			$deleted = $Summary->delete();
			if ($deleted){
				$deletes[] = $deleted_summary;
			} else {
				//$noDelete['test summaries'][] = $del_id;
			}
		}
//		$delete_status['test summaries'] = $deletes;
//		$deletes=[];
//
//		$Players = Player::model()->findAllByAttributes(['person_id'=>$id]);
//		foreach ($Players as $Player){
//			$player_id = $Player->player_id;
//			$TeamPlayers = TeamPlayer::model()->findAllByAttributes(['player_id'=>$player_id]);
//			$tplayer_deletes = [];
//			foreach($TeamPlayers as $TeamPlayer){
//				$tplayer_id = $TeamPlayer->attributes[$TeamPlayer->getPrimaryKey()];
//				$deleted = $TeamPlayer->delete();
//				if ($deleted){
//					$tplayer_deletes[] = $tplayer_id;
//				} else {
//					//$noDelete['team player'][] = $tplayer_id;
//				}
//			}
//			$delete_status['team player'] = $tplayer_deletes;
//			$del_id = $Player->attributes[$Player->getPrimaryKey()];
//			$deleted = $Player->delete();
//			if ($deleted){
//				$deletes[] = $del_id;
//			} else {
//				//$noDelete['team player'][] = $del_id;
//			}
//		}
//
//		$Coaches = Coach::model()->findAllByAttributes(['person_id'=>$id]);
//		foreach ($Coaches as $Coach){
//			$coach_id = $Coach->coach_id;
//			$TeamCoaches = TeamCoach::model()->findAllByAttributes(['coach_id'=>$coach_id]);
//			foreach($TeamCoaches as $TeamCoach){
//				$TeamCoach->delete();
//			}
//			$Coach->delete();
//		}

		$personName = $Person->person_name_full;
		$deleted = $Person->delete();
		if ( $deleted ){
			$result = 'Person ' . $personName . " ($id) was deleted from the database.";
		} else {
			$result = 'An error ocurred while deleting person: ' . $personName . ' ($id)';
		}
		return $result;
	}


	/**
	 * Deletes all linked models descending from a single person. Deletes person, player, team_player, summary, detail.
	 * @param int $id person_id
	 * @internal Development Status = ready for testing (rft)
	 * @example http://gsm-public:8097/index-test.php/Person/CascadeDeletePerson/25
	 * @example http://gsm-public:8097/index-test.php/Person/CascadeDeletePerson/25?XDEBUG_SESSION_START=netbeans-xdebug&fileName=SpecialReport-dms-july2014-camps.1412526854.csv&forceCascade=false
	 * @internal Development Status = ready for testing (rft)
	 * @internal passed all testing then seemed to break, eg stop working. Added error trapping in v0.2
	 */
	public static function cascadeDeletePerson_v1($id)
	{

		$Person = Person::model()->findByPk($id);

		$Summaries = TestEvalSummaryLog::model()->findAllByAttributes(['person_id'=>$id]);
		foreach ($Summaries as $Summary) {
			$tesl_id = $Summary->attributes[$Summary->primaryKey()];
			$params = ['test_eval_summary_log_id'=>$tesl_id];
			$TestEvalDetailLog = TestEvalDetailLog::model()->findAllByAttributes($params);
			foreach ($TestEvalDetailLog as $tedl){
				$tedl->delete();
			}
			$Summary->delete();
		}

		$Players = Player::model()->findAllByAttributes(['person_id'=>$id]);
		foreach ($Players as $Player){
			$player_id = $Player->player_id;
			$TeamPlayers = TeamPlayer::model()->findAllByAttributes(['player_id'=>$player_id]);
			foreach($TeamPlayers as $TeamPlayer){
				$TeamPlayer->delete();
			}
			$Player->delete();
		}

		$Coaches = Coach::model()->findAllByAttributes(['person_id'=>$id]);
		foreach ($Coaches as $Coach){
			$coach_id = $Coach->coach_id;
			$TeamCoaches = TeamCoach::model()->findAllByAttributes(['coach_id'=>$coach_id]);
			foreach($TeamCoaches as $TeamCoach){
				$TeamCoach->delete();
			}
			$Coach->delete();
		}


		$Person->delete();
	}

	/**
	 * @return array[] ['table_name'=>'alias']
	 */
	protected static function tableAliases() {
		return [
			'test_eval_detail_log'	=>'test results',
			'test_eval_summary_log'	=>'test results summary',
			'team_player'			=>'team player'
		];
	}

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'person_id' => Yii::t('app', 'Person ID'),
                'org_id' => Yii::t('app', 'Org'),
                'app_user_id' => Yii::t('app', 'App User'),
                'person_type_id' => Yii::t('app', 'Type'),
                'user_id' => Yii::t('app', 'User'),
                'person_name_prefix' => Yii::t('app', 'Name Prefix'),
                'person_name_first' => Yii::t('app', 'Name First'),
                'person_name_middle' => Yii::t('app', 'Name Middle'),
                'person_name_last' => Yii::t('app', 'Name Last'),
                'person_name_suffix' => Yii::t('app', 'Name Suffix'),
                'person_name_full' => Yii::t('app', 'Name Full'),
                'person_phone_personal' => Yii::t('app', 'Phone Personal'),
                'person_email_personal' => Yii::t('app', 'Email Personal'),
                'person_phone_work' => Yii::t('app', 'Phone Work'),
                'person_email_work' => Yii::t('app', 'Email Work'),
                'person_position_work' => Yii::t('app', 'Position Work'),
                'gender_id' => Yii::t('app', 'Gender'),
                'person_image_headshot_url' => Yii::t('app', 'Image Headshot Url'),
                'person_name_nickname' => Yii::t('app', 'Name Nickname'),
                'person_date_of_birth' => Yii::t('app', 'Date Of Birth'),
                'person_height' => Yii::t('app', 'Height'),
                'person_weight' => Yii::t('app', 'Weight'),
                'person_tshirt_size' => Yii::t('app', 'Tshirt Size'),
                'person_high_school__graduation_year' => Yii::t('app', 'High School Graduation Year'),
			    'person_college_graduation_year' => Yii::t('app', 'College Graduation Year'),
                'person_college_commitment_status' => Yii::t('app', 'College Commitment Status'),
                'person_addr_1' => Yii::t('app', 'Addr 1'),
                'person_addr_2' => Yii::t('app', 'Addr 2'),
                'person_addr_3' => Yii::t('app', 'Addr 3'),
                'person_city' => Yii::t('app', 'City'),
                'person_postal_code' => Yii::t('app', 'Postal Code'),
                'person_country' => Yii::t('app', 'Country'),
                'person_country_code' => Yii::t('app', 'Country Code'),
                'person_state_or_region' => Yii::t('app', 'State Or Region'),
                'org_affiliation_begin_dt' => Yii::t('app', 'Org Affiliation Begin Dt'),
                'org_affiliation_end_dt' => Yii::t('app', 'Org Affiliation End Dt'),
                'person_website' => Yii::t('app', 'Website'),
			    'person_profile_url' => Yii::t('app', 'Profile URL'),
                'person_profile_uri' => Yii::t('app', 'Profile URI'),
                'created_at' => Yii::t('app', 'Created Dt'),
                'updated_at' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'addresses' => null,
                'coaches' => null,
                'eventAttendeeLogs' => null,
                'medias' => null,
                'notes' => null,
                'paymentLogs' => null,
                'appUser' => null,
                'personCountryCode' => null,
                'gender' => null,
                'org' => null,
                'personType' => null,
                'user' => null,
                'personCertifications' => null,
                'players' => null,
                'playerContacts' => null,
                'playerResultsSearchLogs' => null,
                'searchCriteriaTemplates' => null,
                'searchHistories' => null,
                'subscriptions' => null,
                'testEvalSummaryLogs' => null,
        );
    }


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchBySelect2($search = null, $id = null, $raw = null, $limit=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $pkName as id, $textColumn as `text` "
				 . "from $tableName "
				 . "where $textColumn LIKE :$textColumn "
				 . "order by $textColumn";
			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%'];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchTypeBySelect2($search = null, $id = null, $raw = null, $limit=null, $typeName=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$typeTable  = $tableName . '_type';
		$fkeyName   = $tableName . '_type_id';

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $tableName.$pkName as id, $tableName.$textColumn as `text` "
				 . "from $tableName inner join $typeTable on $tableName.$fkeyName = $typeTable.$fkeyName "
				 . "where $typeTable." .$typeTable . "_name = :typeName and $tableName.$textColumn LIKE :$textColumn "
				 . "order by $tableName.$textColumn";

			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%', ':typeName'=>$typeName];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}

	/**
	 * A manually revised list of Yii relation types
	 * We can think that a PARENT table will be the one that doesn't have a foreign key,
	 *   and a CHILD table as the one who "depends" on the parent table, that is, it has a foreign key.
	 *   Given that, a CHILD BELONGS_TO a PARENT and a PARENT HAS_ONE CHILD
	 * @return array
	 * @see http://www.yiiframework.com/wiki/181/relations-belongs_to-versus-has_one/
	 */
    public function relations() {
        return array(
            'addresses' => array(self::HAS_MANY, 'Address', 'person_id'),
			//'coaches' => array(self::HAS_MANY, 'Coach', 'person_id'),             // CHasManyRelation
            'coaches' => array(self::HAS_ONE, 'Coach', 'person_id'),                // CHasOneRelation
            'eventAttendeeLogs' => array(self::HAS_MANY, 'EventAttendeeLog', 'person_id'),
            'medias' => array(self::HAS_MANY, 'Media', 'person_id'),
            'notes' => array(self::HAS_MANY, 'Note', 'person_id'),
            'paymentLogs' => array(self::HAS_MANY, 'PaymentLog', 'person_id'),
            'appUser' => array(self::BELONGS_TO, 'AppUser', 'app_user_id'),
            'personCountryCode' => array(self::BELONGS_TO, 'Country', 'person_country_code'),
            'gender' => array(self::BELONGS_TO, 'Gender', 'gender_id'),
            'org' => array(self::BELONGS_TO, 'Org', 'org_id'),
            'personType' => array(self::BELONGS_TO, 'PersonType', 'person_type_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'personCertifications' => array(self::HAS_MANY, 'PersonCertification', 'person_id'),
            //'players' => array(self::HAS_MANY, 'Player', 'person_id'),
			'players' => array(self::HAS_ONE, 'Player', 'person_id'),
            'playerContacts' => array(self::HAS_MANY, 'PlayerContact', 'person_id'),
            'playerResultsSearchLogs' => array(self::HAS_MANY, 'PlayerResultsSearchLog', 'person_id'),
            'searchCriteriaTemplates' => array(self::HAS_MANY, 'SearchCriteriaTemplate', 'person_id'),
            'searchHistories' => array(self::HAS_MANY, 'SearchHistory', 'person_id'),
            'subscriptions' => array(self::HAS_MANY, 'Subscription', 'person_id'),
            'testEvalSummaryLogs' => array(self::HAS_MANY, 'TestEvalSummaryLog', 'person_id'),
        );
    }

	/**
	 *
	 * @return CActiveRecord (Person + PersonType + Org + OrgType + Player, etc.)
	 * @example $model = Person::model()->athlete()->findByPk(157); // person_id
	 * @version 0.1.0
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public function athlete() {
		$personTypeID = 1;
		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
				'condition'=>'t.person_type_id = :person_type_id',
					'params'=>[':person_type_id'=>$personTypeID],
				'with'=>[
					'personType'=>[
						'condition'=>'t.person_type_id = personType.person_type_id',
					],
					'players'=>[
						'condition'=>'t.person_id = players.person_id',
						'with' =>[
							'together'=>true,
							'teamPlayers'=>[
								'with'=>[
									'together'=>true,
									'team',
						            'teamPlaySportPosition'
								],
							],
						],
					],
					'org'=>[
						'condition'=>'t.org_id = org.org_id',
						'with'=>'orgType',
					],
		            'addresses',
//					'user'=>[
//						//'condition'=>'t.user_id = user.id',
//						],
					'medias',
					'notes',
					'personCertifications',
					'eventAttendeeLogs',
					'testEvalSummaryLogs'=>[
						//'with'=>'testEvalDetails',
					],
					'playerContacts',
					'searchCriteriaTemplates',
				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}

	/**
	 *
	 * @param string|int $personID
	 * @return CActiveRecord (Person + PersonType + Org + OrgType + Player)
	 * @example
	 *   Find an athlete by their person id
	 *     $athlete = Person::model()->athleteByPersonId($personID=157)->find();
	 *     $attrDeep = BaseModel::getAttributesDeepAsArray($athlete);
	 *     YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($attrDeep, 10, true);
	 *
	 */
	public function athleteByPersonId($personID) {
		$personTypeID = 1;
		$localDebug = false;


		$this->getDbCriteria()->mergeWith(
			[
				'condition'=>$this->getTableAlias(false,false) . '.person_id = :person_id'
					.' and t.person_type_id = :person_type_id',
					'params'=>[':person_id'=>$personID, ':person_type_id'=>$personTypeID],
				'with'=>[
					'personType'=>[
						'condition'=>'t.person_type_id = personType.person_type_id',
					],
					'players'=>[
						'condition'=>'t.person_id = players.person_id',
					],
					'org'=>[
						'condition'=>'t.org_id = org.org_id',
						'with'=>'orgType',
					],
				],
//				[
//					'condition'=>$this->getTableAlias(false,false) . '.person_type_id = :person_type_id',
//					'params'=>[':person_type_id__forceErr'=>$personTypeID],
//				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}


	/**
	 * Returns Person, Org, Coach, CoachType models
	 * @param string|int $coachID
	 * @return CActiveRecord (Person + PersonType + Coach + CoachType + Org + orgType)
	 */
	public function coachByCoachId($coachID) {
		$coach = Coach::model()->findByPk($coachID);
		if (is_null($coach)){
			throw new CException('invalid coach ID parameter', 707);
		}
		$personID = $coach->person_id;
		$personTypeID = 2;
		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
				'condition'=>$this->getTableAlias(false,false) . '.person_id = :person_id'
					.' and t.person_type_id = :person_type_id',
					'params'=>[':person_id'=>$personID, ':person_type_id'=>$personTypeID],
				'with'=>[
					'personType'=>[
						'together' => true,
						'joinType' => 'INNER JOIN',
						'condition'=>'t.person_type_id = personType.person_type_id',
					],
					'coaches'=>[
						'together' => true,
						'joinType' => 'INNER JOIN',
						'condition'=>'t.person_id = coaches.person_id',
						'with'=>[
							'coachType'
						]
					],
					'org'=>[
						'condition'=>'t.org_id = org.org_id',
						'with'=>[
							'orgType',
						],
					],
					'personCertifications'=>[
//						'together' => true,
//						'condition'=>'t.person_id = personCertifications.person_id',
					],
				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}

	/**
	 * This is NOT a model scope
	 * @param CActiveRecord::Org $model
	 * @param array $modelsToLoad A list of model names to load
	 * @param array $relationsToLoad A list of org relation Names to load
	 * @return CActiveRecord::AweActiveRecord::Org
	 * @example Org::model()->orgLazyLoadByOrgID($orgID)->findByPk($orgID);
	 */
	public static function athleteLazyLoad($model, array $modelsToLoad=[], array $relationsToLoad=[]) {

		$orgID = $model->org_id;

		// @todo is this really neccessary? Why not both?
		if (count($modelsToLoad) > 0 && count($relationsToLoad) > 0){
			throw new CException("specify model names OR relation names. You can not pass both", 707);
		}

		$relationsMeta = BaseModel::fetchModelRelations('Org');

		// Key all operations off of model names even when relation names are passed in
		if (count($modelsToLoad) > 0){

			foreach ($modelsToLoad as $modelName){

				$models = $modelName::model()->findAllByAttributes(['org_id'=>$orgID]);
				foreach ($models as $lazyModel){
					$model[$relationsMeta['byModelName'][$modelName]['relationName']] =  $lazyModel;
				}
				// PHP best practice for foreach loops
				unset($lazyModel);
			}
			// PHP best practice for foreach loops
			unset($modelName);
		}

		// Key all operations off of model names even when relation names are passed in
		if (count($relationsToLoad) > 0){
			//foreach ($relationsMeta['byRelationName'] as $relationName => $relationsInfo){
			foreach ($relationsToLoad as $relationName){
				$modelName = $relationsMeta['byRelationName'][$relationName]['relatedModel'];
				$models = $modelName::model()->findAllByAttributes(['org_id'=>$orgID]);
				foreach ($models as $lazyModel){
					$model[$relationName] =  $lazyModel;
				}
				// PHP best practice for foreach loops
				unset($lazyModel);
			}
			// PHP best practice for foreach loops
			unset($relationName);
		}

		// in-line relations reference
		//		'addresses',
		//		'camps',
		//		'medias',
		//		'notes',
		//		'orgLevel', // OrgLevel HasMany Orgs eg a parent of org
		//		'orgType',  // OrgType HasMany Orgs eg a parent of org
		//		'paymentLogs',
		//		'people',
		//		'schools',
		//		'subscriptions',
		//		'teams',

		return $model;
	}


	/**
	 * Returns Person, Org, Coach, CoachType models
	 * @param type $personID
	 * @return \Person
	 */
	public function coachByPersonId($personID) {
		$personTypeID = 2;
		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
				'condition'=>$this->getTableAlias(false,false) . '.person_id = :person_id'
					.' and t.person_type_id = :person_type_id',
					'params'=>[':person_id'=>$personID, ':person_type_id'=>$personTypeID],
				'with'=>[
					'personType'=>[
						'condition'=>'t.person_type_id = personType.person_type_id',
					],
					'coaches'=>[
						'condition'=>'t.person_id = coaches.person_id',
						'with'=>[
							'coachType'
						]
					],
					'org'=>[
						'condition'=>'t.org_id = org.org_id',
					],
				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}

	/**
	 *
	 * @return CActiveRecord (Person + PersonType + Org + OrgType + Player, etc.)
	 * @example $model = Person::model()->coach()->findByPk(159); // person_id
	 * @version 0.1.0
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 *
	 */
	public function coach() {
		$personTypeID = 2;
		$localDebug = false;

		$this->getDbCriteria()->mergeWith(
			[
				'condition'=>'t.person_type_id = :person_type_id',
					'params'=>[':person_type_id'=>$personTypeID],
				'with'=>[
					'personType'=>[
						'condition'=>'t.person_type_id = personType.person_type_id',
					],
					'coaches'=>[
						'condition'=>'t.person_id = coaches.person_id',
						'with'=>[
							'coachType',
							'teamCoaches'=>[
								'with'=>'team',
							]
						]
					],
					'org'=>[
						'condition'=>'t.org_id = org.org_id',
						'with'=>[
							'orgType'
						]
					],
					'addresses',
					'medias',
					'notes',
					'personCertifications',
					'eventAttendeeLogs',
					'searchCriteriaTemplates',
				],
			]
		);

		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}

	/**
	 *
	 * @param type $personID
	 * @return array [Person=>[PersonType,Player]
	 */
	public function athlete_v2($personID=157) {
		$personTypeID = 1;
		$localDebug = false;

		$criteria = [

		];
		$this->getDbCriteria()->mergeWith(
			[
				'condition'=>$this->getTableAlias(false,false) . '.person_id = :person_id',
					'params'=>[':person_id'=>$personID],
				'with'=>[
					'personType'=>[
						'condition'=>'t.person_type_id = personType.person_type_id',
					],
					'players'=>[
						'condition'=>'t.person_id = players.person_id',
					],
				],
//				[
//					'condition'=>$this->getTableAlias(false,false) . '.person_type_id = :person_type_id',
//					'params'=>[':person_type_id__forceErr'=>$personTypeID],
//				],
			]
		);
		if ($localDebug === true){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($this->getDbCriteria(), 10, true);
		}
        return $this;
	}

	public function athlete_v1() {
		$personTypeID = 1;
		$this->getDbCriteria()->mergeWith(
			[
			'condition'=>'t.person_type_id = :person_type_id',
			'params'=>array(':person_type_id'=>$personTypeID)
			]
		);
        return $this;
	}

	/**
	 * Only returns a coach's person model and nothing else
	 * @return \Person
	 */
	public function coach_v1() {
		$personTypeID = 2;
		$this->getDbCriteria()->mergeWith(
			[
			'condition'=>'person_type_id = :person_type_id',
			'params'=>array(':person_type_id'=>$personTypeID)
			]
		);
		return $this;
	}

	/**
	 * @internal Confirms the use of a 'condition' clause and an 'on' clause to
	 * replace the functionality of mysql where clause that will work with left
	 * outer joined models.
	 * @return \Person
	 */
	public function athleteWithType() {
		//$personTypeID = 1;
		$this->getDbCriteria()->mergeWith(
//			[
//			'condition'=>'person_type_id = :person_type_id',
//			'params'=>array(':person_type_id'=>$personTypeID)
//			]
			[
				'with'=>[
					'personType'=>[
						'condition'=> "personType.person_type_id = 1",
						'on'=>"personType.person_type_id = 1",
					]
				]
	//					'with'=>[
	//						'teamCoaches'=>[
	//							'condition'=> "coaches.coach_id > 0",
	//							'on'=>"teamCoaches.coach_id > 0",
			]

		);
        return $this;
	}
	public function personTypeById($personTypeID) {
		$this->getDbCriteria()->mergeWith(
			[
			'condition'=>'person_type_id = :person_type_id',
			'params'=>array(':person_type_id'=>$personTypeID)
			]
		);
		return $this;
	}

	public function personByTypeName($personTypeName) {
		$this->getDbCriteria()->mergeWith(
			[
			'condition'=>'person_type_id = :person_type_id',
			'params'=>array(':person_type_id'=>$personTypeName)
			]
		);
		return $this;
	}

	public function coaches() {
		// or since 1.1.7
		$coaches=Person::model()->findAll(
		[
			'with'=>[
				'coaches'=>[

				],
				'scopes'=>[
					'coach',
					//'approved'
				]
			],
		]
		);

//		$personTypeID = 2;
//		$this->getDbCriteria()->mergeWith(
//			[
//			'condition'=>'person_type_id = :person_type_id',
//			'params'=>array(':person_type_id'=>$personTypeID)
//			]
//		);
		return $coaches;
	}

    /**
     *
     *
     *
     */
	public function coachesAsJson() {
		// or since 1.1.7
		$coaches=Person::model()->findAll(
		[
			'with'=>[
				'coaches'=>[
					'scopes'=>['recently','approved']
				],
			],
		]
		);

		$personTypeID = 2;
		$this->getDbCriteria()->mergeWith(
			[
			'condition'=>'person_type_id = :person_type_id',
			'params'=>array(':person_type_id'=>$personTypeID)
			]
		);
		return BaseModel::renderJsonDeep($coaches, $preventAppEnd=true);
	}

	public function coachesOnTeams() {
		// or since 1.1.7
		$this->getDbCriteria()->mergeWith(
			[
				'with'=>[
					'coaches'=>[
						'with'=>[
							'coachType',
							'coachTeamCoach',
	//							//'scopes'=>['recently','approved'],
							],
						//'scopes'=>['recently','approved'],
						],

					],
				//],
			]
		);
		return $this;
	}

	public function coachesOnTeams_v1() {
		// or since 1.1.7
		$coaches=Person::model()->findAll(
		[
			'with'=>[
				'coaches'=>[
					'with'=>[
						'coachTypes',
						'teamCoaches',
//							//'scopes'=>['recently','approved'],
						],
					//'scopes'=>['recently','approved'],
					],

				],
			//],
		]
		);

		return $coaches;
	}

	/**
	 * @param string|int $orgID
	 * returns DbCriteria
	 */
	public function schoolSelectForEdit($orgID) {
		$this->getDbCriteria()->mergeWith(
			[
				'with'=>[
					'org'=>[
						'with'=>[
							'orgType' =>[],
							'orgLevel'=>[],
							'schools', // =>[
								//'with'=>[
										// a relation with conference table is needed
										// 'schoolStats',
								//	]

							//], // conference
							// 'subscriptions' => null,
							'teams'   =>[
								'with'=>[
									'teamDivision'=>[],
									'teamLeague' =>[],
								],
							],
						],
						'with'=>[

						]
					]
				],

			]
		);
		return $this;
	}

	/**
	 * @param string|int $orgID
	 * returns DbCriteria
	 * @
	 */
	public function athleteTeamsForEdit($personID, $teamPlayerID=null) {
		$this->getDbCriteria()->mergeWith(
			[
				'condition'=>'t.person_id = :person_id',
				'params'   =>[':person_id'=>$personID],
				'with'=>[
					'org'=>[
						'with'=>[
							'orgType' ,
							'orgLevel',
							],
					],
					'players'=>[
						'with'=>[
							'teamPlayers'=>[
								'with'=>[
									'team',
									'teamPlaySportPosition',
								]
							],
						],
					],

//							'schools'=>[
//								'with'=>[
//										// a relation with conference table is needed
//										'conference'=>[],
//										// 'schoolStats',
//								],
//							],
							// 'subscriptions' => [],,
//							'teams',
//							'teams'   =>[
//								'with'=>[
//									'teamDivision',
//									'teamLeague',
//									'ageGroup'  ,
//									'teamCoaches',
//									'teamPlayers',
//									'testEvalSummaryLogs',
//								],
//							],
//						],
				//	],
				],

			]
		);
		return $this;
	}

    public function scopes()
    {
        return array(
            'athleteTest'=>array(
                'condition'=>'person_type_id = :person_type_id',
				'params'=>array(':person_type_id'=>'1'),
                'order'=>'created_at DESC',
                'limit'=>5,
            ),
            'coachTest'=>array(
                'condition'=>'person_type_id = :person_type_id',
				'params'=>array(':person_type_id'=>'1'),
            ),
            'scout'=>array(
                'condition'=>'created_at <= NOW()',
                'order'=>'created_at DESC',
                'limit'=>5,

            ),
        );
    }

	public function scopeArray(){
		//  __METHOD__ = 'Person::scopeArray'
		$scopes=[

		];
		// List relation names that you want to be fetched after the find or findAll
		$afterFindGetRelated = [
			'coach'=>[

			]
		];


		$thisMethod = __METHOD__;
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($thisMethod, 10, true);
	}
}
