<?php

Yii::import('application.models._base.BaseOrg');

class TeamOrg extends BaseOrg
{

	public static $p_tableName  = 'org';
	public static $p_primaryKey = 'org_id';
	public static $p_textColumn = 'org_name';
	public static $p_modelName  = 'Org';

	/**
     * @return Org
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Org|Orgs', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Selects only org_names that match the orgTypeId p
	 * @param int|string|array $orgTypeId Org->org_type_id
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 * @internal Development Status = Golden!
	 */
	public static function fetchAllAsDropDownListByOrgTypeId($orgTypeId=null) {
		// The select can be processed as a string or as an array of params
		//$criteria = new CDbCriteria();

		if (is_string($orgTypeId) && stripos($orgTypeId, ',') > 0){
			// a comma delimited string can be processed as is
			//$orgTypeIds = explode(',',$orgTypeId);
			$orgTypeIds = $orgTypeId;
		} elseif (is_array($orgTypeId) && count($orgTypeId) > 0){
			// an array needs to be converted to a comma delimited string to be processed
			//$orgTypeIds = implode(',', $orgTypeId);
			$orgTypeIds = $orgTypeId;
		} elseif (is_string($orgTypeId) || is_numeric($orgTypeId)){
			$orgTypeIds = $orgTypeId;
		} else {
			throw new CException('orgTypeId param needs be a comma delimited string or an indexed array of org_type_id values', 777);
		}

//		if (isset($orgTypeIds) and count($orgTypeIds) > 0){
//			// select multiple org types
//			$params = [':org_type_id'=>$orgTypeIds];
//		} else {
//			$params = [':org_type_id'=>$orgTypeIds];
//		}

		$params = [':org_type_id'=>$orgTypeIds];
//		if ( is_numeric($orgTypeId) && (int)$orgTypeId > 0){
//			$orgTypeIds = $orgTypeId;
//		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` "
				. "from $tableName "
				. "where org_type_id = :org_type_id "
				. "order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true, $params);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		return $list;
	}

	/**
	 * Get a grouped list from a list of org type namess
	 * @param string|array $orgTypeNameList A comma delimted string or an array, can also be a simple string with one value.
	 * @param bool $returnGroupedList true returns a grouped list, false returns a standard list without groups
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 * @internal Development Status = ready for testing (rft)
	 */
	public static function fetchAllAsDropDownListByOrgTypeName($orgTypeNameList=null,$returnGroupedList=true) {

		if (is_string($orgTypeNameList) && stripos($orgTypeNameList, ',') > 0){
			$orgTypes = explode(',',$orgTypes);
		} elseif (is_array($orgTypeNameList)){
			$orgTypes = $orgTypeNameList;
		} elseif (is_string($orgTypeNameList) && !empty($orgTypeNameList)){
			$orgTypes = $orgTypeNameList;
		} else {
			throw new CException('orgTypeNameList param needs be a comma delimited string or an indexed array of orgType names', 707);
		}

		$params = [':org_type_name'=>$orgTypes];

		if ($returnGroupedList == true){
			//$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
			$sql  = "select o.org_id as id, o.org_name as `text` ot.org_type_name as `group` "
				. "from org o inner join org_type ot on o.org_type_id = ot.org_type_id "
				. "where ot.org_type_name = :org_type_name order by ot.org_type_name, o.org_name ";
			$cmd  = Yii::app()->db->createCommand($sql);
			$data = $cmd->queryAll($fetchAssociative=true, $params);
			$list = CHtml::listData( $data, 'id', 'text', 'group'); //[]
			return $list;
		} else {
			$sql  = "select o.org_id as id, o.org_name as `text` "
				. "from org o inner join org_type ot on o.org_type_id = ot.org_type_id "
				. "where ot.org_type_name = :org_type_name order by o.org_name ";
		}
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true, $params);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		return $list;
	}

	/**
	 *
	 * @return array[] Example [0=>['id'=>1,'text'=>'rowValueText'], 1=>['id'=>'2', 'text'=>'anotherRowValueText'] ]
	 */
	public static function fetchAllAsSelect2List() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		return $data;
	}

	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchBySelect2($search = null, $id = null, $raw = null, $limit=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $pkName as id, $textColumn as `text` "
				 . "from $tableName "
				 . "where $textColumn LIKE :$textColumn "
				 . "order by $textColumn";
			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%'];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchTypeBySelect2($search = null, $id = null, $raw = null, $limit=null, $typeName=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$typeTable  = $tableName . '_type';
		$fkeyName   = $tableName . '_type_id';

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $tableName.$pkName as id, $tableName.$textColumn as `text` "
				 . "from $tableName inner join $typeTable on $tableName.$fkeyName = $typeTable.$fkeyName "
				 . "where $typeTable." .$typeTable . "_name = :typeName and $tableName.$textColumn LIKE :$textColumn "
				 . "order by $tableName.$textColumn";

			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%', ':typeName'=>$typeName];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}

    /**
     * @return array customized attribute labels (name=>label)
	 * @internal Add override of _base attributeLabels that will survive a _base model regeneration
     */
    public function attributeLabels() {
        return array(
                'org_id' => Yii::t('app', 'Org'),
                'org_type_id' => Yii::t('app', 'Type'),
                'org_level_id' => Yii::t('app', 'Level'),
                'org_name' => Yii::t('app', 'Name'),
                'org_governing_body' => Yii::t('app', 'Governing Body'),
                'org_ncaa_clearing_house_id' => Yii::t('app', 'Ncaa Clearing House'),
                'org_website_url' => Yii::t('app', 'Website Url'),
                'org_twitter_url' => Yii::t('app', 'Twitter Url'),
                'org_facebook_url' => Yii::t('app', 'Facebook Url'),
                'org_phone_main' => Yii::t('app', 'Phone Main'),
                'org_email_main' => Yii::t('app', 'Email Main'),
                'org_addr1' => Yii::t('app', 'Addr1'),
                'org_addr2' => Yii::t('app', 'Addr2'),
                'org_addr3' => Yii::t('app', 'Addr3'),
                'org_city' => Yii::t('app', 'City'),
                'org_state_or_region' => Yii::t('app', 'State Or Region'),
                'org_postal_code' => Yii::t('app', 'Postal Code'),
                'org_country_code_iso3' => Yii::t('app', 'Country'),
                'created_at' => Yii::t('app', 'Created Dt'),
                'updated_at' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'updated_by' => Yii::t('app', 'Updated By'),
				// List related tables
                'addresses'     => null,
                'camps'         => null,
                'medias'        => null,
                'notes'         => null,
                'orgLevel'      => null,
                'orgType'       => null,
                'paymentLogs'   => null,
                'people'        => null,
                'schools'       => null,
                'subscriptions' => null,
                'teams'         => null,
        );
    }

    public function relations() {
        return array(
            'addresses' => array(self::HAS_MANY, 'Address', 'org_id'),
            'camps' => array(self::HAS_MANY, 'Camp', 'org_id'),
            'medias' => array(self::HAS_MANY, 'Media', 'org_id'),
            'notes' => array(self::HAS_MANY, 'Note', 'org_id'),
            'orgLevel' => array(self::BELONGS_TO, 'OrgLevel', 'org_level_id'),
            'orgType' => array(self::BELONGS_TO, 'OrgType', 'org_type_id'),
            'paymentLogs' => array(self::HAS_MANY, 'PaymentLog', 'org_id'),
            'people' => array(self::HAS_MANY, 'Person', 'org_id'),
            'schools' => array(self::HAS_MANY, 'School', 'org_id'),
            'subscriptions' => array(self::HAS_MANY, 'Subscription', 'org_id'),
            'teams' => array(self::HAS_MANY, 'Team', 'org_id'),
        );
    }

	public function rules() {
        return array(
            array(	'org_name',
					'required',
					'message' => Yii::t('app', 'Field is required')
			),
            array(	'org_type_id, org_level_id, created_by, updated_by',
					'numerical',
					'integerOnly'=>true
			),
            array(	'org_name, org_addr1, org_addr2, org_addr3',
					'length',
					'max'=>100,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'org_governing_body, org_state_or_region',
					'length',
					'max'=>45,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'org_ncaa_clearing_house_id, org_phone_main, org_postal_code',
					'length',
					'max'=>25,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'org_website_url, org_twitter_url, org_facebook_url',
					'length',
					'max'=>150,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'org_email_main, org_city',
					'length',
					'max'=>75,
					'tooLong' => Yii::t('app', 'Field is required')
			),

			// Validate that the email is truly valid
			array('org_email_main',
				'email',
				'checkMX'=>true,
				'message' => Yii::t('app', 'The email address supplied is not valid')
				),

            array(	'org_country_code_iso3',
					'length',
					'max'=>3,
					'tooLong' => Yii::t('app', 'Field is required')
			),
            array(	'created_at, updated_at',
					'safe'
			),
            array('org_type_id, org_level_id, org_governing_body, org_ncaa_clearing_house_id, org_website_url, org_twitter_url, org_facebook_url, org_phone_main, org_email_main, org_addr1, org_addr2, org_addr3, org_city, org_state_or_region, org_postal_code, org_country_code_iso3, created_at, updated_at, created_by, updated_by',
					'default',
					'setOnEmpty' => true,
					'value' => null
			),
            //array('org_id, org_type_id, org_level_id, org_name, org_governing_body, org_ncaa_clearing_house_id, org_website_url, org_twitter_url, org_facebook_url, org_phone_main, org_email_main, org_addr1, org_addr2, org_addr3, org_city, org_state_or_region, org_postal_code, org_country_code_iso3, created_at, updated_at, created_by, updated_by', 'safe', 'on'=>'search'),
			array('org_id, org_type_id, org_level_id, org_name, org_governing_body, org_ncaa_clearing_house_id, org_website_url, org_twitter_url, org_facebook_url, org_phone_main, org_email_main, org_addr1, org_addr2, org_addr3, org_city, org_state_or_region, org_postal_code, org_country_code_iso3, created_at, updated_at, created_by, updated_by',
				'safe',
				'on'=>'search'
			),
			// Add custom rules
			// Model insert rules?

			// Model update rules
			// NEVER allow org_id to be updated
			// after the model is inserted
			array('org_id',
				'safe',
				'except'=>'update'
			),
			array('org_id',
				'unsafe',
				'on'=>'update'
			),
        );
    }

	}
