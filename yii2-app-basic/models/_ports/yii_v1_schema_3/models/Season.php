<?php

Yii::import('application.models._base.BaseSeason');

class Season extends BaseSeason
{

	public static $p_tableName  = 'season';
	public static $p_primaryKey = 'season_id';
	public static $p_textColumn = 'season';
	public static $p_modelName  = 'Season';

    /**
     * @return Season
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Season|Seasons', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }


	/**
	 *
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownList($returnTags=false) {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		if ($returnTags){
			return array_values($list);
		} else {
			return $list;
		}

	}

	/**
	 *
	 * @return array[] Example [0=>['id'=>1,'text'=>'rowValueText'], 1=>['id'=>'2', 'text'=>'anotherRowValueText'] ]
	 */
	public static function fetchAllAsSelect2List() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		return $data;
	}

}
