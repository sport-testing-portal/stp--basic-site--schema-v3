<?php

Yii::import('application.models._base.BaseAppParm');

/**
 * Prototype model for self-testing
 * \\Application\Models\CEC 
 *  ? 1.0 All models only reference applicable field names
 *  ? 1.0 All models with crud services offer a crud test interface
 */
class AppParm extends BaseAppParm
{
    /**
     * @return AppParm
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'AppParm|AppParms', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    public function primaryKey()
    {
        return $this->tableName() . '_id';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }
    
    /**
     * Add some methods for testing
     * Other test methods?
     * ? Add function to return other sites metadata at the controller level?
     */
    
    /**
     * 
     * @return array $
     */
    public function relationsMetaInfo($className=__CLASS__) {
        
//        return array(
//            'addressType' => array(self::BELONGS_TO, 'AddressType', 'address_type_id'),
//            'org' => array(self::BELONGS_TO, 'Org', 'org_id'),
//            'person' => array(self::BELONGS_TO, 'Person', 'person_id'),
//            'campLocations' => array(self::HAS_MANY, 'CampLocations', 'address_id'),
//            'campSessions' => array(self::HAS_MANY, 'CampSession', 'address_id'),
//            'campSessionLocations' => array(self::HAS_MANY, 'CampSessionLocation', 'address_id'),
//        );
        
        $relations = parent::model($className)->relations();
        $results = [];
        if (is_array($relations)){
            if (count($relations) === 0){
                echo "no relations returned <br>";
                $results[] = ['error'=>'no relations returned'];
            } else {
                $relationsMeta = ['relations'=>$relations];
                $results[] = $relationsMeta;
            }
            return $results;
        }
        
      
    }    

    
    /**
     * 
     * @param string | array $scenario
     * @internal dev status = Under Construction
     */
    public function testSecenario($scenario='crud') {
        
        // create
        // read
        // update
        // delete
        // crud = all 
        
    }   
    
    /**
     * 
     * @return array $result = [relations]
     */
    public function behaviorsMetaInfo() {
        /*
        return array_merge(array(
            'ActiveRecordRelation' => array(
                'class' => 'EActiveRecordRelationBehavior',
            ),
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at',
                'timestampExpression' => new CDbExpression('NOW()'),
                'setUpdateOnCreate'   => true
            )
        ), parent::behaviors());
        */
        $behaviors = parent::model($className=__CLASS__)->behaviors();
        if (is_array($behaviors)){
            return $behaviors;
        }
    }    
    

    /**
     * @return array customized attribute labels (name=>label)
     * @internal how is this different than AppParms::model()->attributes ???
     */
    public static function attributeArrayEmpty() {
        
        // iterate through attribute array from the model?
        // Yes, Iteration of the base model's attributes is a superior strategy 
        // instead of hard coding. 
        // Are any table attributes missing in the model's attributes list?
        // What is the ideal way to return the relations arrays? 
        // What is the ideal way to populate the relations arrays?
        $attr = self::model()->attributes;
        $attributes = [
            'address_id'      => '',
            'org_id'          => '',
            'person_id'       => '',
            'address_type_id' => '',
            'addr1'           => '',
            'addr2'           => '',
            'addr3'           => '',
            'city'            => '',
            'state_or_region' => '',
            'postal_code'     => '',
            'country'         => '',
            'country_code'    => '',
            // blameable fields
            'created_at'      => '',
            'updated_at'      => '',
            'created_by'      => 0,
            'updated_by'      => 0,
            'lock'            => 0,
            // relations 
            'addressType'          => null,
            'org'                  => null,
            'person'               => null,
            'campLocations'        => null,
            'campSessions'         => null,
            'campSessionLocations' => null,
        ];
        
        echo "attr test result follows <br>";
        barray::echo_array($attr);        
        
        $testResult=[];
        $test = array_diff_assoc($attributes, $attr, $testResult);
        echo "test result follows <br>";
        barray::echo_array($testResult);
        
        echo "test array follows <br>";
        barray::echo_array($test);
        
        return $attributes;
    }
    


    /**
     * 
     * @param string | array $testScenario
     * @return \CActiveDataProvider
     */
    public function searchTestData($testScenario) {
        $criteria = new CDbCriteria;

        // test ids
        // Use created_by as a tag for finding test data?
        $criteria->compare('address_id', $this->address_id);
        $criteria->compare('org_id', $this->org_id);
        $criteria->compare('person_id', $this->person_id);
        $criteria->compare('address_type_id', $this->address_type_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }   
    
    

    public function rulesMetaInfo() {
        return array(
            array(	'org_id, person_id, address_type_id, created_by, updated_by, lock',
					'numerical',
					'integerOnly'=>true
			),
        );
    }    
    
}
