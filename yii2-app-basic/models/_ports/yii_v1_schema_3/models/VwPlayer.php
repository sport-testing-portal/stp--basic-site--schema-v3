<?php
/**
 * Based on vwPlayer v4.9.0 eg ~/Dropbox/prj_gsm_backend/source_code/SQL/gsm/vwPlayer - create v4.9.0 2015-10-08.sql
 */
Yii::import('application.models._base.BaseVwPlayer');

class VwPlayer extends BaseVwPlayer
{

	const MILE_FACTOR=69.1;
	const KILOMETER_FACTOR=111.045;

	public static $p_tableName  = 'vwPlayer';
	public static $p_primaryKey = 'team_player_id'; //'player_id' is NOT the primary key;
	public static $p_textColumn = 'person_name_full';
	public static $p_modelName  = 'VwPlayer';

	protected $hasFatalError = false;

	/**
     * @return VwPlayer
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Player|Players', $n);
    }

	/**
	 * Returns a model property that uniquely identifies each model row
	 * @return	string A column name
	 * @see Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
	 */
    public function primaryKey()
    {
        //return $this->tableName() . '_id';
		return 'team_player_id'; // this field is unique per data row
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    /**
	 *
	 * @return boolean
	 */
    protected function beforeSave()
    {
		// internal attributes need to be written to the individual tables
		// @todo consider using the fieldxlation routine here so a call to this->model->save
		//   will actually parse the model's attributes into an mfva and store all the values.
		if (true===true){
			return false; // data updates disallowed
		}

        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Select data by distance
	 * @param decimal $origLat
	 * @param decimal $origLong
	 * @param decimal $maxDistance
	 * @param string $rtnKilometers
	 * @param string $rtnAssocArrYN
	 * @return CActiveRecord | array[]
	 * @internal An index optimized select calcs in 0.034 secs and fetches dataset in 0.005 secs
	 * @see: BaseModel::convertPostalCodeToLatLong()
	 */
	protected function fetchByDistance($origLat, $origLong, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {

		if ($rtnKilometers == 'Y'){
			$distance_return_type_factor = KILOMETER_FACTOR;
		} else {
			$distance_return_type_factor = MILES_FACTOR;
		}

		$distance_calc = "$distance_return_type_factor * DEGREES(ACOS(COS(RADIANS(latpoint)) "
			."* COS(RADIANS(latitude)) "
			."* COS(RADIANS(longpoint) - RADIANS(longitude)) "
			." + SIN(RADIANS(latpoint)) "
			." * SIN(RADIANS(latitude)))) AS distance ";

        $query = ""
        . "select c.*  "
        . "	from ".$this->tableName()." t "
        . "	inner join  "
        . "	( "
        . "	select postal_code, place_name, state_name, latitude, longitude, "
        . "	 $distance_calc "
        . "	 from zip_code  "
        . "	 join ( "
        . "		 select  $origLat  AS latpoint,  $origLong AS longpoint "
        . "	   ) AS p ON 1=1 "
        . "		where longitude  "
        . "		BETWEEN longpoint - ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "			AND longpoint + ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "		having distance_in_miles < $maxDistance "
        . "		order by distance_in_miles desc "
        . "	) z on left(t.person_postal_code,5) = z.postal_code "
        . "; "
        ;

		$cmd = Yii::app()->db->createCommand($query);
		if ($rtnAssocArrYN === 'Y'){
			return $cmd->queryAll($fetchAssociative=true);
		} else {
			return $cmd->queryAll();
		}
	}

	/**
	 * Made to bulk assign view attribute values to base model attribute values
	 * @param array[] $field_value_array
	 * @version 4.5.0 synced with vwPlayer v4.5.0
	 * @todo Add OrgLevel fields
	 */
	public static function fetchTargetModelFieldNameXlation($field_value_array) {
		//$fva = $field_value_array;
		// attributes like 'person_age' that are calculated values and can't be written back to the database
		$readonly_attr	= self::fetchReadOnlyAttributes();
		$fva			= array_diff_key( $field_value_array, array_flip( $readonly_attr) ); // remove readonly fields
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($field_value_array, 10, true);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($fva, 10, true);
		//$field_list = array_keys($fva);
		$xlat=[
		'person_name_full'					=> ['model'=>'Person',	'attribute'=>'person_name_full'],
		// 'person_age`
		'team_name'							=> ['model'=>'Team',	'attribute'=>'team_name'],
		//'age_group_name' 					=> ['model'=>'AgeGroup','attribute'=>''],
		'person_city'						=> ['model'=>'Person',	'attribute'=>'person_city'],
		'person_state_or_region'			=> ['model'=>'Person',	'attribute'=>'person_state_or_region'],
		'gsm_staff'							=> ['model'=>'User',	'attribute'=>'first_name'],
		'player_id'							=> ['model'=>'Player',	'attribute'=>'player_id'],
		'person_id'							=> ['model'=>'Person',	'attribute'=>'person_id'],
		// confirmed
		'player_team_player_id'				=> ['model'=>'Player','attribute'=>'player_team_player_id'],
		'player_access_code'				=> ['model'=>'Player','attribute'=>'player_access_code'],
		'player_waiver_minor_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_minor_dt'],
		'player_waiver_adult_dt'			=> ['model'=>'Player','attribute'=>'player_waiver_adult_dt'],
		'player_parent_email'				=> ['model'=>'Player','attribute'=>'player_parent_email'],
		'player_sport_preference'			=> ['model'=>'Player','attribute'=>'player_sport_preference'],
		'player_sport_position_preference'	=> ['model'=>'Player','attribute'=>'player_sport_position_preference'],
		'player_shot_side_preference' 		=> ['model'=>'Player','attribute'=>'player_shot_side_preference'],
		'player_dominant_side'		 		=> ['model'=>'Player','attribute'=>'player_dominant_side'],
		'player_dominant_foot'		 		=> ['model'=>'Player','attribute'=>'player_dominant_foot'],
		'player_statistical_highlights'		=> ['model'=>'Player','attribute'=>'player_statistical_highlights'],
		// confirmed
		'org_id'							=> ['model'=>'Person','attribute'=>'org_id'],
		'app_user_id'						=> ['model'=>'Person','attribute'=>'app_user_id'],
		'person_name_prefix'				=> ['model'=>'Person','attribute'=>'person_name_prefix'],
		'person_name_first'					=> ['model'=>'Person','attribute'=>'person_name_first'],
		'person_name_middle'				=> ['model'=>'Person','attribute'=>'person_name_middle'],
		'person_name_last'					=> ['model'=>'Person','attribute'=>'person_name_last'],
		'person_name_suffix'				=> ['model'=>'Person','attribute'=>'person_name_suffix'],
		'person_phone_personal'				=> ['model'=>'Person','attribute'=>'person_phone_personal'],
		'person_email_personal'				=> ['model'=>'Person','attribute'=>'person_email_personal'],
		'person_phone_work'					=> ['model'=>'Person','attribute'=>'person_phone_work'],
		'person_email_work'					=> ['model'=>'Person','attribute'=>'person_email_work'],
		'person_position_work'				=> ['model'=>'Person','attribute'=>'person_position_work'],
		'person_image_headshot_url' 		=> ['model'=>'Person','attribute'=>'person_image_headshot_url'],
		'person_name_nickname'				=> ['model'=>'Person','attribute'=>'person_name_nickname'],
		'person_date_of_birth'				=> ['model'=>'Person','attribute'=>'person_date_of_birth'],
		'person_height'						=> ['model'=>'Person','attribute'=>'person_height'],
		'person_weight'						=> ['model'=>'Person','attribute'=>'person_weight'],
		'person_tshirt_size'				=> ['model'=>'Person','attribute'=>'person_tshirt_size'],
		'person_addr_1'						=> ['model'=>'Person','attribute'=>'person_addr_1'],
		'person_addr_2'						=> ['model'=>'Person','attribute'=>'person_addr_2'],
		'person_addr_3'						=> ['model'=>'Person','attribute'=>'person_addr_3'],
		'person_postal_code'				=> ['model'=>'Person','attribute'=>'person_postal_code'],
		'person_country'					=> ['model'=>'Person','attribute'=>'person_country'],
		'person_country_code'				=> ['model'=>'Person','attribute'=>'person_country_code'],
		'person_profile_url'				=> ['model'=>'Person','attribute'=>'person_profile_url'],
		'person_profile_uri'				=> ['model'=>'Person','attribute'=>'person_profile_uri'],
		'person_college_commitment_status'	=> ['model'=>'Person','attribute'=>'person_college_commitment_status'],
		'person_type_id'					=> ['model'=>'Person','attribute'=>'person_type_id'],
		//'person_type_name'				=> ['model'=>'PersonType','attribute'=>'person_type_name'],  // readonly
		//'person_type_desc_short'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_short'],  // readonly
		//'person_type_desc_long'			=> ['model'=>'PersonType','attribute'=>'person_type_desc_long'],  // readonly
		//'gender_desc'						=> ['model'=>'Gender',	'attribute'=>'gender_desc'], // readonly
		//'gender_code'						=> ['model'=>'Gender',	'attribute'=>'gender_code'], // readonly
		'org_type_id'						=> ['model'=>'Org','attribute'=>'org_type_id'],
		'org_name'							=> ['model'=>'Org','attribute'=>'org_name'],
		'org_type_name'						=> ['model'=>'OrgType','attribute'=>'org_type_name'],
		'org_website_url'					=> ['model'=>'Org','attribute'=>'org_website_url'],
		'org_twitter_url'					=> ['model'=>'Org','attribute'=>'org_twitter_url'],
		'org_facebook_url'					=> ['model'=>'Org','attribute'=>'org_facebook_url'],
		'org_phone_main'					=> ['model'=>'Org','attribute'=>'org_phone_main'],
		'org_email_main'					=> ['model'=>'Org','attribute'=>'org_email_main'],
		'org_addr1'							=> ['model'=>'Org','attribute'=>'org_addr1'],
		'org_addr2'							=> ['model'=>'Org','attribute'=>'org_addr2'],
		'org_addr3'							=> ['model'=>'Org','attribute'=>'org_addr3'],
		'org_city'							=> ['model'=>'Org','attribute'=>'org_city'],
		'org_state_or_region'				=> ['model'=>'Org','attribute'=>'org_state_or_region'],
		'org_postal_code'					=> ['model'=>'Org','attribute'=>'org_postal_code'],
		'org_country_code_iso3'				=> ['model'=>'Org','attribute'=>'org_country_code_iso3'],
		// confirmed
		'team_player_id'					=> ['model'=>'TeamPlayer','attribute'=>'team_player_id'],
		'team_id'							=> ['model'=>'TeamPlayer','attribute'=>'team_id'],
		'team_play_primary_position'		=> ['model'=>'TeamPlayer','attribute'=>'primary_position'],
		'team_play_sport_position_id'		=> ['model'=>'TeamPlayer','attribute'=>'team_play_sport_position_id'],
		'team_play_sport_position2_id'		=> ['model'=>'TeamPlayer','attribute'=>'team_play_sport_position2_id'],
		'team_play_begin_dt'				=> ['model'=>'TeamPlayer','attribute'=>'team_play_begin_dt'],
		'team_play_end_dt'					=> ['model'=>'TeamPlayer','attribute'=>'team_play_end_dt'],
		'team_play_coach_id'				=> ['model'=>'TeamPlayer','attribute'=>'coach_id'],
		'team_play_coach_name'				=> ['model'=>'TeamPlayer','attribute'=>'coach_name'],
		'team_play_created_dt'				=> ['model'=>'TeamPlayer','attribute'=>'created_dt'],
		'team_play_updated_dt'				=> ['model'=>'TeamPlayer','attribute'=>'updated_dt'],
		'team_play_statistical_highlights'	=> ['model'=>'TeamPlayer','attribute'=>'team_play_statistical_highlights'],
		'team_play_current_team'	        => ['model'=>'TeamPlayer','attribute'=>'team_play_current_team'],

		//confirmed
		'team_play_sport_position_id'		=> ['model'=>'SportPosition_1','attribute'=>'sport_position_id'],
		'team_play_sport_position2_id'		=> ['model'=>'SportPosition_1','attribute'=>'sport_position_id'],
		'team_play_sport_position_name'		=> ['model'=>'SportPosition_2','attribute'=>'sport_position_name'],
		'team_play_sport_position2_name'	=> ['model'=>'SportPosition_2','attribute'=>'sport_position_name'],
		// confirmed
		'team_org_id'						=> ['model'=>'Team','attribute'=>'org_id'],
		'team_school_id'					=> ['model'=>'Team','attribute'=>'school_id'],
		'team_sport_id'						=> ['model'=>'Team','attribute'=>'sport_id'],
		'team_camp_id'						=> ['model'=>'Team','attribute'=>'camp_id'],
		'team_gender_id'					=> ['model'=>'Team','attribute'=>'gender_id'],
		'age_group_id'						=> ['model'=>'Team','attribute'=>'age_group_id'],
		'team_age_group'					=> ['model'=>'Team','attribute'=>'team_age_group'],
		'team_website_url'					=> ['model'=>'Team','attribute'=>'team_website_url'],
		'team_schedule_url'					=> ['model'=>'Team','attribute'=>'team_schedule_url'],
		'team_schedule_uri'					=> ['model'=>'Team','attribute'=>'team_schedule_uri'],
		'team_competition_season_id'		=> ['model'=>'Team','attribute'=>'team_competition_season_id'],
		'team_competition_season'			=> ['model'=>'Team','attribute'=>'team_competition_season'],
		'team_statistical_highlights'		=> ['model'=>'Team','attribute'=>'team_statistical_highlights'],
		'team_gender'						=> ['model'=>'Team','attribute'=>'team_gender'],
		'team_city'							=> ['model'=>'Team','attribute'=>'team_city'],
		'team_division'						=> ['model'=>'Team','attribute'=>'team_division'],
		'team_division_id'					=> ['model'=>'Team','attribute'=>'team_division_id'],
		'team_league_id'					=> ['model'=>'Team','attribute'=>'team_league_id'],
		'team_state_id'						=> ['model'=>'Team','attribute'=>'team_state_id'],
		'team_country_id'					=> ['model'=>'Team','attribute'=>'team_country_id'],
		// confirmed
		'team_org_org_type_id'				  => ['model'=>'Org','attribute'=>'org_type_id'],
		'team_org_org_type_name'			  => ['model'=>'OrgType','attribute'=>'org_type_name'],
		'team_org_org_name'					  => ['model'=>'Org','attribute'=>'org_name'],
		'team_org_org_ncaa_clearing_house_id' => ['model'=>'Org','attribute'=>'org_ncaa_clearing_house_id'],
		'team_org_org_website_url'			  => ['model'=>'Org','attribute'=>'org_website_url'],
		'team_org_org_twitter_url'			  => ['model'=>'Org','attribute'=>'org_twitter_url'],
		'team_org_org_facebook_url'			  => ['model'=>'Org','attribute'=>'org_facebook_url'],
		'team_org_org_phone_main'			  => ['model'=>'Org','attribute'=>'org_phone_main'],
		'team_org_org_email_main'			 => ['model'=>'Org','attribute'=>'org_email_main'],
		'team_org_org_addr1'				=> ['model'=>'Org','attribute'=>'org_addr1'],
		'team_org_org_addr2'				=> ['model'=>'Org','attribute'=>'org_addr2'],
		'team_org_org_addr3'				=> ['model'=>'Org','attribute'=>'org_addr3'],
		'team_org_org_city'					=> ['model'=>'Org','attribute'=>'org_city'],
		'team_org_state_or_region'			=> ['model'=>'Org','attribute'=>'org_state_or_region'],
		'team_org_org_postal_code'			=> ['model'=>'Org','attribute'=>'org_postal_code'],
		'team_org_org_country_code_iso3'	=> ['model'=>'Org','attribute'=>'org_country_code_iso3'],
		'team_org_org_governing_body'	    => ['model'=>'Org','attribute'=>'org_governing_body'],
		// confirmed
		'team_division_id'					=> ['model'=>'TeamDivision','attribute'=>'team_division_id'],
		'team_division_name'				=> ['model'=>'TeamDivision','attribute'=>'team_division_name'],
		// confirmed
		'team_league_id'					=> ['model'=>'TeamLeague','attribute'=>'team_league_id'],
		'team_league_name'					=> ['model'=>'TeamLeague','attribute'=>'team_league_name'],

		//'team_age_group_depreciated' 		=> ['model'=>'Team','attribute'=>''],
		];

		return $xlat;
	}

	/*
	 * @param array $view_field_value_array A simple field-name=>field-value array
	 * @returns array $rtn A multi-table field value array
	 * @uses self::relationsTree() To order the models by their relations hieararchy
	 */
	public static function generateModelValuesArray($view_field_value_array) {
		$debug  = 1;
		$xlat	= self::fetchTargetModelFieldNameXlation($view_field_value_array);
		$tree   = self::relationsTree();
		$rtn	= [];
		//$view_fields_passed_in = array_keys($$view_field_values_array);
		foreach ($view_field_value_array as $view_field_name => $view_field_value) {
			if(array_key_exists($view_field_name, $xlat)){
				//$models[] = $xlat[$view_field_name]['model'];
				$rtn[$xlat[$view_field_name]['model']][$xlat[$view_field_name]['attribute']]
						= $view_field_value;
				$tree[$xlat[$view_field_name]['model']][$xlat[$view_field_name]['attribute']]
						= $view_field_value;
			}
		}
		if ($debug===1){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($rtn, 10, true);
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($tree, 10, true);
		}
		//return $rtn;
		return $tree;
	}


	/**
	 * Returns an array of attributes like 'person_age' that are calculated values and can't be written back to the database
	 * @param type $field_value_array
	 * @internal Development Status = code construction
	 */
	protected static function fetchReadOnlyAttributes() {
		//		$readonly_attr[]; //
		// return ['person_age', 'person_dob_eng', 'person_bday_long', 'person_height_imperial','person_bmi',];
		$readonly_attr[] = 'person_age';
		$readonly_attr[] = 'person_dob_eng';
		$readonly_attr[] = 'person_bday_long';
		$readonly_attr[] = 'person_height_imperial';
		$readonly_attr[] = 'team_org_org_type_name'; // This field maps OrgType.org_type_name
		$readonly_attr[] = 'person_bmi';
		$readonly_attr[] = 'age_group_name';
		$readonly_attr[] = 'person_type_name';		// ['model'=>'PersonType','attribute'=>'person_type_name'],  // readonly
		$readonly_attr[] = 'person_type_desc_short'; // ['model'=>'PersonType','attribute'=>'person_type_desc_short'],  // readonly
		$readonly_attr[] = 'person_type_desc_long';	//	=> ['model'=>'PersonType','attribute'=>'person_type_desc_long'],  // readonly
		$readonly_attr[] = 'gender_desc';			//	=> ['model'=>'Gender','attribute'=>'gender_desc'], // readonly
		$readonly_attr[] = 'gender_code';			//	=> ['model'=>'Gender','attribute'=>'gender_code'], // readonly
		$readonly_attr[] = 'gsm_staff';				//	=> ['model'=>'User','attribute'=>'first_name'], // readonly
		return $readonly_attr;
	}


	/**
	 * Saves a specific model the view is composed of.
	 *
	 * The record is inserted as a row into the database table if its {@link isNewRecord}
	 * property is true (usually the case when the record is created using the 'new'
	 * operator). Otherwise, it will be used to update the corresponding row in the table
	 * (usually the case if the record is obtained using one of those 'find' methods.)
	 *
	 * Validation will be performed before saving the record. If the validation fails,
	 * the record will not be saved. You can call {@link getErrors()} to retrieve the
	 * validation errors.
	 *
	 * If the record is saved via insertion, its {@link isNewRecord} property will be
	 * set false, and its {@link scenario} property will be set to be 'update'.
	 * And if its primary key is auto-incremental and is not set before insertion,
	 * the primary key will be populated with the automatically generated key value.
	 *
	 * @param string | array $models
	 * @param boolean $runValidation whether to perform validation before saving the record.
	 * If the validation fails, the record will not be saved to database.
	 * @param array $attributes list of attributes that need to be saved. Defaults to null,
	 * meaning all attributes that are loaded from DB will be saved.
	 * @return boolean whether the saving succeeds
	 */

	public function saveModels($models='all', $runValidation=true,$attributes=null)
	{
		$filter = null;
		$hasFatalError = $this->hasFatalError;
		// <editor-fold defaultstate="collapsed" desc="parse attributes param">
		if (! is_null($attributes)){
			// filter specific attributes
			if (is_array($attributes) && count($attributes) > 0){
				// loop
				foreach ($attributes as $attribute) {
					$filter[] = $attribute;
				}
			} elseif (is_string($attributes) && strlen($attributes) > 0){
				// filter items passed in
				if (stripos($attributes, ',') !== false){
					// parse string
					$fields = explode(',', $attributes);
					foreach ($fields as $field){
						$filter[] = $field;
					}
				} else {
					// its a non-delimited string
					$filter[] = $attributes;
				}
			}
		} else { }
		// </editor-fold>

		// @todo Process all the models in sequence
		// @todo Carry keys forward like store chain
		// @todo Determine ins|upd like StoreGeneric()?
		// if all the attributes are empty then return false
		$valCounts = array_count_values($attributes);
		$valKeys   = array_keys($valCounts);
		if (count($valCounts) == 1){
			YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($valCounts, 10, true);
			return false;
		} elseif (count($valKeys) == 2){
			if (isset($valKeys[''])  && $valKeys[''] > 0){
				$valEmptyCount = $valKeys['']; // @todo cast as int
			} else {
				$valEmptyCount = 0;
			}
			// if $valKeys array contains an key = '' then a count for empty values exists
			$valEmptyCount = ( ( array_key_exists('',$valKeys) ) ?  (int)$valKeys[''] : 0  );
		}

		$vwAttr	= $this->attributes;  // Get this model's attribute values
		$mtfva	= self::generateModelValuesArray($vwAttr); // data envelope array
		$readOnlyModels = ['User','OrgType'];
		if ($models == 'all'){
			foreach ($mtfva as $model => $fva) { // mtfva = multi-table fva
				if(array_search($model, $readOnlyModels) !== false){
					continue; // skip this modele
				}
				$model_filter[] = $model;
			}
		} elseif(is_array($models)){
			$model_filter = $models;
		}

		foreach ($mtfva as $model => $fva) {
			if(in_array($model, $model_filter)){
				// process model

				// next line assumes each model has a store() method and they do NOT ALL have it.
				// is the model's pkey OR fkey present?
				//$model::store($fva);
				if ($hasFatalError == true){
					continue;
				}
				if (count($fva) == 0){
					continue; //  skip the processing of empty models
				}
				$referenceModel = new $model();
				$pk = $referenceModel->primaryKey(); // returns the field name of the primary key
				if(isset($mtfva[$model][$pk])){
					$pkVal = $mtfva[$model][$pk];
				} else {
					$pkVal = 0;
				}
				if ( (int)$pkVal > 0){
					$crudScenario= 'insert';
					$modelToSave = $model::model()->findByPk($pkVal);
				}
				else
				{
					// create the model
					$crudScenario= 'update';
					$modelToSave = new $model();

				}
				$runValidation = true;
				$modelToSave->setAttributes($mtfva[$model]);
				if ($modelToSave->save($runValidation) ){
					$model_filter[$model] = ['crudScenario'=>$crudScenario, 'saved'=>true];
				} else {
					$model_filter[$model] = ['crudScenario'=>$crudScenario, 'saved'=>false];
				}

			}
		}
		return $model_filter;
//		if(!$runValidation || $this->validate($attributes))
//			return $this->getIsNewRecord() ? $this->insert($attributes) : $this->update($attributes);
//		else
//			return false;
	}


	/**
	 *
	 * @param type $parentID
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchIncludingTeam($parentID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        //$criteria->with=array('team');
        //$criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.team_id',$parentID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort;
        $sort->attributes = array(
            'person_name_full' => array(
            'asc' => 'person_name_full',
            'desc' => 'person_name_full DESC',
            ), '*', /* Treat all other columns normally */
        );
        /* End: Sort on related Model's columns */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <danabyrd@byrdbrain.com>
	 */
	public static function searchBySelect2($search = null, $id = null, $raw = null, $limit=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $pkName as id, $textColumn as `text` "
				 . "from $tableName "
				 . "where $textColumn LIKE :$textColumn "
				 . "order by $textColumn";
			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%'];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchTypeBySelect2($search = null, $id = null, $raw = null, $limit=null, $typeName=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$typeTable  = $tableName . '_type';
		$fkeyName   = $tableName . '_type_id';

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $tableName.$pkName as id, $tableName.$textColumn as `text` "
				 . "from $tableName inner join $typeTable on $tableName.$fkeyName = $typeTable.$fkeyName "
				 . "where $typeTable." .$typeTable . "_name = :typeName and $tableName.$textColumn LIKE :$textColumn "
				 . "order by $tableName.$textColumn";

			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%', ':typeName'=>$typeName];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'person_name_full' => Yii::t('app', 'Athlete Name'),
                'person_age' => Yii::t('app', 'Person Age'),
                'team_name' => Yii::t('app', 'Team Name'),
                'age_group_name' => Yii::t('app', 'Age Group Name'),
                'person_city' => Yii::t('app', 'Person City'),
                'person_state_or_region' => Yii::t('app', 'Person State Or Region'),
                'gsm_staff' => Yii::t('app', 'Gsm Staff'),
                'player_id' => Yii::t('app', 'Player ID'),
                'person_id' => Yii::t('app', 'Person ID'),
                'player_age_group_id' => Yii::t('app', 'Player Age Group'),
                'player_team_player_id' => Yii::t('app', 'Player Team Player'),
                'player_sport_position_id' => Yii::t('app', 'Player Sport Position'),
                'player_sport_position2_id' => Yii::t('app', 'Player Sport Position2'),
                'player_sport_position_name' => Yii::t('app', 'Sport Position'),
                'player_access_code' => Yii::t('app', 'Player Access Code'),
                'player_waiver_minor_dt' => Yii::t('app', 'Player Waiver Minor Dt'),
                'player_waiver_adult_dt' => Yii::t('app', 'Player Waiver Adult Dt'),
                'player_parent_email' => Yii::t('app', 'Player Parent Email'),
                'player_sport_preference' => Yii::t('app', 'Player Sport Preference'),
                'player_sport_position_preference' => Yii::t('app', 'Player Sport Position Preference'),
                'player_shot_side_preference' => Yii::t('app', 'Player Shot Side Preference'),
                'player_dominant_side' => Yii::t('app', 'Dominant Side'),
                'player_dominant_foot' => Yii::t('app', 'Dominant Foot'),
                'player_statistical_highlights' => Yii::t('app', 'Player Statistical Highlights'),
                'org_id' => Yii::t('app', 'Org ID'),
                'app_user_id' => Yii::t('app', 'App User'),
                'person_name_prefix' => Yii::t('app', 'Person Name Prefix'),
                'person_name_first' => Yii::t('app', 'Person Name First'),
                'person_name_middle' => Yii::t('app', 'Person Name Middle'),
                'person_name_last' => Yii::t('app', 'Person Name Last'),
                'person_name_suffix' => Yii::t('app', 'Person Name Suffix'),
                'person_phone_personal' => Yii::t('app', 'Person Phone Personal'),
                'person_email_personal' => Yii::t('app', 'Person Email Personal'),
                'person_phone_work' => Yii::t('app', 'Person Phone Work'),
                'person_email_work' => Yii::t('app', 'Person Email Work'),
                'person_position_work' => Yii::t('app', 'Person Position Work'),
                'person_image_headshot_url' => Yii::t('app', 'Person Image Headshot Url'),
                'person_name_nickname' => Yii::t('app', 'Person Name Nickname'),
                'person_date_of_birth' => Yii::t('app', 'Person Date Of Birth'),
                'person_dob_eng' => Yii::t('app', 'Person Dob Eng'),
                'person_bday_long' => Yii::t('app', 'Person Bday Long'),
                'person_height' => Yii::t('app', 'Person Height'),
                'person_height_imperial' => Yii::t('app', 'Person Height Imperial'),
                'person_weight' => Yii::t('app', 'Person Weight'),
                'person_bmi' => Yii::t('app', 'Person Bmi'),
                'person_tshirt_size' => Yii::t('app', 'Person Tshirt Size'),
                'person_addr_1' => Yii::t('app', 'Person Addr 1'),
                'person_addr_2' => Yii::t('app', 'Person Addr 2'),
                'person_addr_3' => Yii::t('app', 'Person Addr 3'),
                'person_postal_code' => Yii::t('app', 'Person Postal Code'),
                'person_country' => Yii::t('app', 'Person Country'),
                'person_country_code' => Yii::t('app', 'Person Country Code'),
                'person_profile_url' => Yii::t('app', 'Athlete Profile Url'),
                'person_profile_uri' => Yii::t('app', 'Athlete Profile Uri'),
                'person_college_commitment_status' => Yii::t('app', 'College Commitment Status'),
                'person_type_id' => Yii::t('app', 'Person Type'),
                'person_type_name' => Yii::t('app', 'Person Type Name'),
                'person_type_desc_short' => Yii::t('app', 'Person Type Desc Short'),
                'person_type_desc_long' => Yii::t('app', 'Person Type Desc Long'),
                'gender_id' => Yii::t('app', 'Gender'),
                'gender_desc' => Yii::t('app', 'Gender Desc'),
                'gender_code' => Yii::t('app', 'Gender Code'),
                'org_type_id' => Yii::t('app', 'Org Type'),
                'org_name' => Yii::t('app', 'Org Name'),
                'org_type_name' => Yii::t('app', 'Org Type Name'),
                'org_website_url' => Yii::t('app', 'Org Website Url'),
                'org_twitter_url' => Yii::t('app', 'Org Twitter Url'),
                'org_facebook_url' => Yii::t('app', 'Org Facebook Url'),
                'org_phone_main' => Yii::t('app', 'Org Phone Main'),
                'org_email_main' => Yii::t('app', 'Org Email Main'),
                'org_addr1' => Yii::t('app', 'Org Addr1'),
                'org_addr2' => Yii::t('app', 'Org Addr2'),
                'org_addr3' => Yii::t('app', 'Org Addr3'),
                'org_city' => Yii::t('app', 'Org City'),
                'org_state_or_region' => Yii::t('app', 'Org State Or Region'),
                'org_postal_code' => Yii::t('app', 'Org Postal Code'),
                'org_country_code_iso3' => Yii::t('app', 'Org Country Code Iso3'),
                'team_org_org_type_id' => Yii::t('app', 'Team Org Org Type'),
                'team_org_org_type_name' => Yii::t('app', 'Team Org Org Type Name'),
                'team_org_org_name' => Yii::t('app', 'Team Org Org Name'),
                'team_org_org_website_url' => Yii::t('app', 'Team Org Org Website Url'),
                'team_org_org_twitter_url' => Yii::t('app', 'Team Org Org Twitter Url'),
                'team_org_org_facebook_url' => Yii::t('app', 'Team Org Org Facebook Url'),
                'team_org_org_phone_main' => Yii::t('app', 'Team Org Org Phone Main'),
                'team_org_org_email_main' => Yii::t('app', 'Team Org Org Email Main'),
                'team_org_org_addr1' => Yii::t('app', 'Team Org Org Addr1'),
                'team_org_org_addr2' => Yii::t('app', 'Team Org Org Addr2'),
                'team_org_org_addr3' => Yii::t('app', 'Team Org Org Addr3'),
                'team_org_org_city' => Yii::t('app', 'Team Org Org City'),
                'team_org_org_state_or_region' => Yii::t('app', 'Team Org Org State Or Region'),
                'team_org_org_postal_code' => Yii::t('app', 'Team Org Org Postal Code'),
                'team_org_org_country_code_iso3' => Yii::t('app', 'Team Org Org Country Code Iso3'),
                'team_org_org_governing_body' => Yii::t('app', 'Team Org Org Governing Body'),
                'team_org_org_ncaa_clearing_house_id' => Yii::t('app', 'Team Org Org Ncaa Clearing House'),
                'team_player_id' => Yii::t('app', 'Team Player'),
                'team_id' => Yii::t('app', 'Team'),
                'team_play_sport_position_id' => Yii::t('app', 'Team Play Sport Position'),
                'team_play_sport_position2_id' => Yii::t('app', 'Team Play Sport Position2'),
                'team_play_sport_position_name' => Yii::t('app', 'Team Play Sport Position Name'),
                'team_play_sport_position2_name' => Yii::t('app', 'Team Play Sport Position2 Name'),
                'team_play_primary_position_flattext' => Yii::t('app', 'Team Play Primary Position Flattext'),
                'team_play_primary_position' => Yii::t('app', 'Team Play Primary Position'),
                'team_play_begin_dt' => Yii::t('app', 'Team Play Begin Dt'),
                'team_play_end_dt' => Yii::t('app', 'Team Play End Dt'),
                'team_play_coach_id' => Yii::t('app', 'Team Play Coach'),
                'team_play_coach_name' => Yii::t('app', 'Team Play Coach Name'),
                'team_play_current_team' => Yii::t('app', 'Team Play Current Team'),
                'team_play_statistical_highlights' => Yii::t('app', 'Team Play Statistical Highlights'),
                'team_play_created_dt' => Yii::t('app', 'Team Play Created Dt'),
                'team_play_updated_dt' => Yii::t('app', 'Team Play Updated Dt'),
                'team_org_id' => Yii::t('app', 'Team Org'),
                'team_school_id' => Yii::t('app', 'Team School'),
                'team_sport_id' => Yii::t('app', 'Team Sport'),
                'team_camp_id' => Yii::t('app', 'Team Camp'),
                'team_gender_id' => Yii::t('app', 'Team Gender'),
                'team_gender' => Yii::t('app', 'Team Gender'),
                'team_age_group_id' => Yii::t('app', 'Team Age Group'),
                'team_age_group' => Yii::t('app', 'Team Age Group'),
                'team_competition_season_id' => Yii::t('app', 'Team Competition Season'),
                'team_competition_season' => Yii::t('app', 'Team Competition Season'),
                'team_league_id' => Yii::t('app', 'Team League'),
                'team_league' => Yii::t('app', 'Team League'),
                'organizational_level' => Yii::t('app', 'Organizational Level'),
                'team_governing_body' => Yii::t('app', 'Team Governing Body'),
                'team_website_url' => Yii::t('app', 'Team Website Url'),
                'team_schedule_url' => Yii::t('app', 'Team Schedule Url'),
                'team_schedule_uri' => Yii::t('app', 'Team Schedule Uri'),
                'team_statistical_highlights' => Yii::t('app', 'Team Statistical Highlights'),
                'team_division_id' => Yii::t('app', 'Team Division'),
                'team_division' => Yii::t('app', 'Team Division'),
                'team_city' => Yii::t('app', 'Team City'),
                'team_state_id' => Yii::t('app', 'Team State'),
                'team_country_id' => Yii::t('app', 'Team Country'),
        );
    }

	/**
	 * Override the base class method
	 * @return string
	 */
    public static function representingColumn() {
        return 'person_name_full';
    }

	/**
	 * The order of models to save
	 * @return array A hiearchy of relations
	 */
	protected static function relationsTree() {
		$modelSequence=[
			'User'         =>[],
			'OrgType'      =>[],
			'OrgLevel'     =>[],
			'Org'          =>[],
			//'PersonType', // is read-only
			'Person'       =>[],
			'Player'       =>[],
			'SportPosition'=>[],
			'TeamLeague'   =>[],
			'TeamDivision'  =>[],
			'Team'         =>[],
			'TeamPlayer'   =>[],
		];
		return $modelSequence;
	}

	/**
	 * models=[]
	 *
	 */
	protected function storeChainConfig() {

		$chainConfig = [
			'bound__model_name'		=>'Player', // used to map the value of $_POST['pk']
			'submit_widget_class'	=>'TbEditableField',
			'submit_widget_type'	=>'select2',
			'submit_widget_handler'	=>'storeChain',
			'submit_widget_usecase'	=>'numeric_key', // text_value | numeric_key

			'field_values_array'=>[
				'models'=>[
					'OrgType'=>[],
					'OrgLevel'=>[],
					'Team'=>[
						'team_name'			=>'{{insert_text}}',
						'team_id'			=>'{{value}}',
						'age_group_id'		=>['{{fetch_value}}'=>
							['select'			=>'age_group_id',
								'from'			=>'player',
								'where'		=>'player_id = '.$data['player']['player_id']
							],
						],
						'org_id'			=>['{{fetch_value}}'=>
							['select'			=>'org_id',
								'from'			=>'person',
								'where'		=>'person_id = '.$data['player']['person_id']
							],
						],
						'sport_id'			=> 20,	// soccer
					],
					'TeamPlayer'=>[
						'team_player_id'=>['{{fetch_value}}'=>
							['select'		=>'team_player_id',
								'from'		=>'team_player',
								'where'		=>['and',
									'player_id = '.$data['player']['player_id'],
									//'team_id = {{value}}'],
									'team_id = {{shared_attribute}}'],
							],
						],
						'team_id'		=>'{{shared_attribute}}',
						'player_id'		=>$data['player']['player_id'],

					],
					'Player'=>[
						'player_id'				=>$data['player']['player_id'],
						'player_team_player_id'	=>['{{shared_attribute}}'=>[
							'model_name'		=>'TeamPlayer',
							'model_attribute'	=>'team_player_id',
							],
						],
					],
				],
			],
			'list_source'=>[
				'model_name'=>'Team',
				'text'		=>'team_name',
				'value'		=>'team_id',
				//'condition'	=>'org_type_id=2',
				'order'		=>'team_name'
			],
			'data write plan'=>[
				'Team',
				'TeamPlayer',
			],
		];


	}
	/**
	 * models=[]
	 *
	 */
	protected function storeChainConfig_v0() {


		$chainConfig = [
			'bound__model_name'		=>'Player', // used to map the value of $_POST['pk']
			'submit_widget_class'	=>'TbEditableField',
			'submit_widget_type'	=>'select2',
			'submit_widget_handler'	=>'storeChain',
			'submit_widget_usecase'	=>'numeric_key', // text_value | numeric_key

			'field_values_array'=>[
				'models'=>[
					'Team'=>[
						'team_name'			=>'{{insert_text}}',
						'team_id'			=>'{{value}}',
						'age_group_id'		=>['{{fetch_value}}'=>
							['select'			=>'age_group_id',
								'from'			=>'player',
								'where'		=>'player_id = '.$data['player']['player_id']
							],
						],
						'org_id'			=>['{{fetch_value}}'=>
							['select'			=>'org_id',
								'from'			=>'person',
								'where'		=>'person_id = '.$data['player']['person_id']
							],
						],
						'sport_id'			=> 20,	// soccer
					],
					'TeamPlayer'=>[
						'team_player_id'=>['{{fetch_value}}'=>
							['select'		=>'team_player_id',
								'from'		=>'team_player',
								'where'		=>['and',
									'player_id = '.$data['player']['player_id'],
									//'team_id = {{value}}'],
									'team_id = {{shared_attribute}}'],
							],
						],
						'team_id'		=>'{{shared_attribute}}',
						'player_id'		=>$data['player']['player_id'],

					],
					'Player'=>[
						'player_id'				=>$data['player']['player_id'],
						'player_team_player_id'	=>['{{shared_attribute}}'=>[
							'model_name'		=>'TeamPlayer',
							'model_attribute'	=>'team_player_id',
							],
						],
					],
				],
			],
			'list_source'=>[
				'model_name'=>'Team',
				'text'		=>'team_name',
				'value'		=>'team_id',
				//'condition'	=>'org_type_id=2',
				'order'		=>'team_name'
			],
			'data write plan'=>[
				'Team',
				'TeamPlayer',
			],
		];


	}
}
