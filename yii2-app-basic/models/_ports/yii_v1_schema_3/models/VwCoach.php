<?php

Yii::import('application.models._base.BaseVwCoach');

class VwCoach extends BaseVwCoach
{
	const MILE_FACTOR=69.1;
	const KILOMETER_FACTOR=111.045;

	public static $p_tableName  = 'vwCoach';
	public static $p_primaryKey = 'coach_id';
	public static $p_textColumn = 'person_name_full';
	public static $p_modelName  = 'VwCoach';


    /**
     * @return VwCoach
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Coach|Coaches', $n);
    }


    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
	/**
	 *
	 * @return string
	 * @see CoachController->loadProfileInfo($coach_id, $coach_team_coach_id=null)
	 */
    public function primaryKey()
    {
        //return $this->tableName() . '_id';
		//return 'coach_id'; // Coach id is not unique once multiple teams are linked to a coach.
		return 'team_coach_id'; // Team coach id is null until a team is added, but it is unique once multiple teams are linked to a coach.
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/gsm-schema-v3/model.php
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
			if (empty($this->created_at)){
				$this->created_at = date('Y-m-d H:i:s');
			}
            $this->updated_at = date('Y-m-d H:i:s');
			// Enable GSM processes to force a user id eg impersonate a user for data imports
			if (empty($this->created_by)){
				$this->created_by = (int)Yii::app()->user->getId();
			}
			if (empty($this->updated_by)){
				$this->updated_by = (int)Yii::app()->user->getId();
			}
        } else {
            $this->updated_at = date('Y-m-d H:i:s');
            $this->updated_by = (int)Yii::app()->user->getId();
        }
        return parent::beforeSave();
    }

	/**
	 * Select data by distance
	 * @param decimal $origLat
	 * @param decimal $origLong
	 * @param decimal $maxDistance
	 * @param string $rtnKilometers
	 * @param string $rtnAssocArrYN
	 * @return CActiveRecord | array[]
	 * @internal An index optimized select calcs in 0.034 secs and fetches dataset in 0.005 secs
	 * @see: BaseModel::convertPostalCodeToLatLong()
	 */
	protected function fetchByDistance($origLat, $origLong, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {

		if ($rtnKilometers == 'Y'){
			$distance_return_type_factor = KILOMETER_FACTOR;
		} else {
			$distance_return_type_factor = MILES_FACTOR;
		}

		$distance_calc = "$distance_return_type_factor * DEGREES(ACOS(COS(RADIANS(latpoint)) "
			."* COS(RADIANS(latitude)) "
			."* COS(RADIANS(longpoint) - RADIANS(longitude)) "
			." + SIN(RADIANS(latpoint)) "
			." * SIN(RADIANS(latitude)))) AS distance ";


        $query = ""
        . "select c.*  "
        . "	from ".$this->tableName()." t "
        . "	inner join  "
        . "	( "
        . "	select postal_code, place_name, state_name, latitude, longitude, "
        . "	 $distance_calc "
        . "	 from zip_code  "
        . "	 join ( "
        . "		 select  $origLat  AS latpoint,  $origLong AS longpoint "
        . "	   ) AS p ON 1=1 "
        . "		where longitude  "
        . "		BETWEEN longpoint - ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "			AND longpoint + ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "		having distance_in_miles < $maxDistance "
        . "		order by distance_in_miles desc "
        . "	) z on left(t.person_postal_code,5) = z.postal_code "
        . "; "
        ;

		$cmd = Yii::app()->db->createCommand($query);
		if ($rtnAssocArrYN === 'Y'){
			return $cmd->queryAll($fetchAssociative=true);
		} else {
			return $cmd->queryAll();
		}
	}

	/**
	 * Use the Haversine Formula to display the closest matches
	 * @param type $param
	 * @return CActiveRecord | array[]
	 * @internal dev sts = RFT
	 * @see: http://stackoverflow.com/questions/2234204/latitude-longitude-find-nearest-latitude-longitude-complex-sql-or-complex-calc
	 * @internal test harness: fetchAllByDistance($origLat=40.0018, $origLon=-75.1179); // philadelphia
	 * @todo: Add join with coach table after initial testing
	 */
	public function fetchAllByDistanceFromLatLong($origLat, $origLon, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {

		if ($rtnKilometers == 'Y'){
			$distance_return_type_factor = KILOMETER_FACTOR;
		} else {
			$distance_return_type_factor = MILES_FACTOR;
		}

		// @todo: Replace hard coded 69.1 with $distance_return_type_factor
		$query = ""
			. "select t.*  "
			. "	from ".$this->tableName()." t "
			. "	inner join  "
			. "	( "
				."select postal_code, place_name, state_name, latitude, longitude, "
				."3956 * 2 * ASIN(SQRT(POWER(SIN(	($origLat - abs(latitude) ) "
				."*pi()/180/2	),2) "
				." +COS( $origLat*pi()/180 )"
				."*COS( abs(latitude) *pi()/180) "
				."*POWER(	SIN(($origLon - longitude)*pi()/180/2	),2 "
				." )))  as distance "
				."from zip_code "
				."where "
				."longitude between ($origLon - $maxDistance/abs(cos(radians($origLat))*69.1)) "
					."and ($origLon+$maxDistance/abs(cos(radians($origLat))*69.1)) "
					."and latitude between ($origLat-($maxDistance/69.1)) "
					."and ($origLat+($maxDistance/69.1)) "
				."having distance < $maxDistance "
			//."order by distance desc "
			//."limit 3000"
			. "	) z on left(t.person_postal_code,5) = z.postal_code "
			;


        $query = ""
        . "select c.*  "
        . "	from ".$this->tableName()." t "
        . "	inner join  "
        . "	( "
        . "	select postal_code, place_name, state_name, latitude, longitude, "
        . "	 $distance_calc "
        . "	 from zip_code  "
        . "	 join ( "
        . "		 select  $origLat  AS latpoint,  $origLong AS longpoint "
        . "	   ) AS p ON 1=1 "
        . "		where longitude  "
        . "		BETWEEN longpoint - ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "			AND longpoint + ($maxDistance / ($distance_return_type_factor * COS(RADIANS(latpoint)))) "
        . "		having distance_in_miles < $maxDistance "
        . "		order by distance_in_miles desc "
        . "	) z on left(t.person_postal_code,5) = z.postal_code "
        . "; "
        ;


		$cmd = Yii::app()->db->createCommand($query);
		$sql = $cmd->query_text;
		YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($sql, 10, true);

		if ($rtnAssocArrYN === 'Y'){
			return $cmd->queryAll($fetchAssociative=true);
		} else {
			return $cmd->queryAll();
		}
	}

	/**
	 *
	 * @param type $zipcode
	 * @param type $maxDistance
	 * @param type $rtnKilometers
	 * @param type $rtnAssocArrYN
	 * @return CActiveRecord | array[]
	 */
	public function fetchAllByDistanceFromZipCode($zipcode, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {
		$latlong = BaseModel::convertPostalCodeToLatLong($postalCode);
		$result  = $this->fetchAllByDistanceFromLatLong($latlong['latitude'], $latlong['longitude'], $maxDistance, $rtnKilometers, $rtnAssocArrYN);
		return $result;
	}

	/**
	 *
	 * @param type $param
	 * @see: http://stackoverflow.com/questions/2234204/latitude-longitude-find-nearest-latitude-longitude-complex-sql-or-complex-calc
	 * @internal test harness: fetchAllByDistance($origLat=40.0018, $origLon=-75.1179); // philadelphia
	 */
	protected function fetchAllByDistance_v000_proto($origLat, $origLong, $maxDistance=150, $rtnKilometers='N',  $rtnAssocArrYN='N') {
		$query = ""
			."SELECT postal_code, latitude, longitude, "
			."3956 * 2 * ASIN(SQRT(POWER(SIN(	($origLat - abs(latitude) ) "
			."*pi()/180/2	),2) "
			." +COS( $origLat*pi()/180 )"
			."*COS( abs(latitude) *pi()/180) "
			."*POWER(	SIN(($origLon - longitude)*pi()/180/2	),2 "
			." )))  as distance "
			."from zip_code "
			."where "
			."longitude between ($origLon - $maxDistance/abs(cos(radians($origLat))*69.1)) "
				."and ($origLon+$maxDistance/abs(cos(radians($origLat))*69.1)) "
				."and latitude between ($origLat-($maxDistance/69.1)) "
				."and ($origLat+($maxDistance/69.1)) "
		."having distance < $maxDistance "
		."order by distance desc "
		."limit 3000";

		$cmd = Yii::app()->db->createCommand($query);
		if ($rtnAssocArrYN === 'Y'){
			return $cmd->queryAll($fetchAssociative=true);
		} else {
			return $cmd->queryAll();
		}
	}



	/**
	 *
	 * @param type $parentID
	 * @return \CActiveDataProvider
	 * @internal Development Status = code construction
	 * @see http://www.yiiframework.com/wiki/323/dynamic-parent-and-child-cgridciew-on-single-view-using-ajax-to-update-child-gridview-via-controller-with-many_many-relation-after-row-in-parent-gridview-was-clicked/
	 * @internal Basecode pulled from public function searchIncludingPermissions($parentID){} example at the URL above
	 */
    public function searchIncludingTeam($parentID)
    {
        /* This function creates a dataprovider with RolePermission
        models, based on the parameters received in the filtering-model.
        It also includes related Permission models, obtained via the
        relPermission relation. */
        $criteria=new CDbCriteria;
        //$criteria->with=array('team');
        //$criteria->together = true;


        /* filter on role-grid PK ($parentID) received from the
        controller*/
        $criteria->compare('t.team_id',$parentID,false);

        /* Filter on default Model's column if user entered parameter*/
//        $criteria->compare('t.permission_id',$this->permission_id,true);

        /* Filter on related Model's column if user entered parameter*/
//        $criteria->compare('relPermission.permission_desc',
//            $this->permission_desc_param,true);

        /* Sort on related Model's columns */
        $sort = new CSort;
        $sort->attributes = array(
            'person_name_full' => array(
            'asc' => 'person_name_full',
            'desc' => 'person_name_full DESC',
            ), '*', /* Treat all other columns normally */
        );
        /* End: Sort on related Model's columns */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort, /* Needed for sort */
        ));
    }


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchBySelect2($search = null, $id = null, $raw = null, $limit=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $pkName as id, $textColumn as `text` "
				 . "from $tableName "
				 . "where $textColumn LIKE :$textColumn "
				 . "order by $textColumn";
			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%'];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


	/**
	 * Call with $raw = 1 for a Select2 intial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be return and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <dana@globalsoccermetrix.com>, <dana@byrdbrain.com>
	 */
	public static function searchTypeBySelect2($search = null, $id = null, $raw = null, $limit=null, $typeName=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$typeTable  = $tableName . '_type';
		$fkeyName   = $tableName . '_type_id';

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $tableName.$pkName as id, $tableName.$textColumn as `text` "
				 . "from $tableName inner join $typeTable on $tableName.$fkeyName = $typeTable.$fkeyName "
				 . "where $typeTable." .$typeTable . "_name = :typeName and $tableName.$textColumn LIKE :$textColumn "
				 . "order by $tableName.$textColumn";

			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%', ':typeName'=>$typeName];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
				return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'user_id' => Yii::t('app', 'User'),
                'person_id' => Yii::t('app', 'Person'),
                'org_id' => Yii::t('app', 'Org'),
                'coach_id' => Yii::t('app', 'Coach'),
                'team_coach_id' => Yii::t('app', 'Team Coach'),
                'app_user_id' => Yii::t('app', 'App User'),
                'person_type_id' => Yii::t('app', 'Person Type ID'),
                'person_type_name' => Yii::t('app', 'Person Type'),
                'coach_type_id' => Yii::t('app', 'Coach Type ID'),
                'coach_type_name' => Yii::t('app', 'Coach Type'),
                'coach_team_coach_id' => Yii::t('app', 'Coach Team Coach'),
                'org_name' => Yii::t('app', 'Org Name'),
                'org_type_id' => Yii::t('app', 'Org Type'),
                'org_type_name' => Yii::t('app', 'Org Type Name'),
                'team_name' => Yii::t('app', 'Team Name'),
                'team_id' => Yii::t('app', 'Team'),
                'person_name_prefix' => Yii::t('app', 'Name Prefix'),
                'person_name_first' => Yii::t('app', 'Name First'),
                'person_name_middle' => Yii::t('app', 'Name Middle'),
                'person_name_last' => Yii::t('app', 'Name Last'),
                'person_name_suffix' => Yii::t('app', 'Name Suffix'),
                'person_name_full' => Yii::t('app', 'Coach Name'),
                'gender_id' => Yii::t('app', 'Gender'),
                'gender_code' => Yii::t('app', 'Gender Code'),
                'gender_desc' => Yii::t('app', 'Gender Desc'),
                'person_phone_personal' => Yii::t('app', 'Phone Personal'),
                'person_email_personal' => Yii::t('app', 'Email Personal'),
                'person_phone_work' => Yii::t('app', 'Phone Work'),
                'person_email_work' => Yii::t('app', 'Email Work'),
                'person_position_work' => Yii::t('app', 'Position At Work'),
                'person_image_headshot_url' => Yii::t('app', 'Image Headshot Url'),
                'person_name_nickname' => Yii::t('app', 'Person Nickname'),
                'person_date_of_birth' => Yii::t('app', 'Date Of Birth'),
                'person_height' => Yii::t('app', 'Coach Height'),
                'person_weight' => Yii::t('app', 'Coach Weight'),
                'person_tshirt_size' => Yii::t('app', 'Tshirt Size'),
                'person_addr_1' => Yii::t('app', 'Person Addr 1'),
                'person_addr_2' => Yii::t('app', 'Person Addr 2'),
                'person_addr_3' => Yii::t('app', 'Person Addr 3'),
                'person_city' => Yii::t('app', 'Person City'),
                'person_postal_code' => Yii::t('app', 'Person Postal Code'),
                'person_country' => Yii::t('app', 'Country'),
                'person_country_code' => Yii::t('app', 'Country Code'),
                'person_state_or_region' => Yii::t('app', 'State Or Region'),
                'org_affiliation_begin_dt' => Yii::t('app', 'Org Affiliation Begin Dt'),
                'org_affiliation_end_dt' => Yii::t('app', 'Org Affiliation End Dt'),
                'created_dt' => Yii::t('app', 'Created Dt'),
                'updated_dt' => Yii::t('app', 'Updated Dt'),
                'created_by' => Yii::t('app', 'Created By'),
                'person_created_by_username' => Yii::t('app', 'P Ins By User'),
                'updated_by' => Yii::t('app', 'Updated By'),
                'person_updated_by_username' => Yii::t('app', 'P Upd By User'),
                'coach_specialty' => Yii::t('app', 'Coach Specialty'),
                'coach_certifications' => Yii::t('app', 'Coach Certifications'),
                'coach_comments' => Yii::t('app', 'Coach Comments'),
                'coach_qrcode_uri' => Yii::t('app', 'Coach Qrcode Uri'),
                'coach_info_source_scrape_url' => Yii::t('app', 'Coach Info Source Scrape Url'),
                'coach_created_by_username' => Yii::t('app', 'C Ins By User'),
                'coach_created_by' => Yii::t('app', 'Coach Created By'),
                'coach_updated_by_username' => Yii::t('app', 'C Upd By User'),
                'coach_updated_by' => Yii::t('app', 'Coach Updated By'),
                'org_website_url' => Yii::t('app', 'Org Website Url'),
                'org_twitter_url' => Yii::t('app', 'Org Twitter Url'),
                'org_facebook_url' => Yii::t('app', 'Org Facebook Url'),
                'org_phone_main' => Yii::t('app', 'Org Phone Main'),
                'org_email_main' => Yii::t('app', 'Org Email Main'),
                'org_addr1' => Yii::t('app', 'Org Addr1'),
                'org_addr2' => Yii::t('app', 'Org Addr2'),
                'org_addr3' => Yii::t('app', 'Org Addr3'),
                'org_city' => Yii::t('app', 'Org City'),
                'org_state_or_region' => Yii::t('app', 'Org State Or Region'),
                'org_postal_code' => Yii::t('app', 'Org Postal Code'),
                'org_country_code_iso3' => Yii::t('app', 'Org Country Code Iso3'),
                'team_coach_primary_position' => Yii::t('app', 'Team Coach Primary Position'),
                'team_coach_coach_type_id' => Yii::t('app', 'Team Coach Coach Type'),
                'team_coach_begin_dt' => Yii::t('app', 'Team Coach Begin Dt'),
                'team_coach_end_dt' => Yii::t('app', 'Team Coach End Dt'),
                'team_coach_created_dt' => Yii::t('app', 'Team Coach Created Dt'),
                'team_coach_updated_dt' => Yii::t('app', 'Team Coach Updated Dt'),
                'team_coach_created_by' => Yii::t('app', 'Team Coach Created By'),
                'team_coach_updated_by' => Yii::t('app', 'Team Coach Updated By'),
                'team_org_id' => Yii::t('app', 'Team Org'),
                'team_school_id' => Yii::t('app', 'Team School'),
                'team_sport_id' => Yii::t('app', 'Team Sport'),
                'team_camp_id' => Yii::t('app', 'Team Camp'),
                'team_gender_id' => Yii::t('app', 'Team Gender'),
                'team_age_group_id' => Yii::t('app', 'Team Age Group'),
                'team_gender' => Yii::t('app', 'Team Gender'),
                'team_division' => Yii::t('app', 'Team Division'),
                'team_age_group_depreciated' => Yii::t('app', 'Team Age Group Depreciated'),
                'team_wins' => Yii::t('app', 'Team Wins'),
                'team_losses' => Yii::t('app', 'Team Losses'),
                'team_draws' => Yii::t('app', 'Team Draws'),
                'team_winlossdraw' => Yii::t('app', 'Team Winlossdraw'),
        );
    }

	/**
	 * Add override of the _base method that wil survive regeneration of the model
	 * @return string
	 */
    public static function representingColumn() {
        return 'person_name_full';
    }

}
