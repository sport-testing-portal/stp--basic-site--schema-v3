<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property string $activation_key
 * @property datetime $created_on
 * @property datetime $updated_on
 * @property datetime $last_visit_on
 * @property datetime $password_set_on
 * @property boolean $email_verified
 * @property boolean $is_active
 * @property boolean $is_disabled
 * @property string $one_time_password_secret
 * @property string $one_time_password_code
 * @property integer $one_time_password_counter
 * @property datetime $srbac_set_on Works as a semaphor flag for setting permissions.
 *
 * dbyrd: add fields for person attributes capture
 * @property string person_addr_1
 * @property string person_addr_2
 * @property string person_city
 * @property string person_state_or_region
 * @property string person_postal_code
 * @property string person_date_of_birth
 * @property string gender_id
 *
 * The followings are the available model relations:
 * @property UserLoginAttempt[] $userLoginAttempts
 * @property UserProfilePicture[] $userProfilePictures
 * @property UserRemoteIdentity[] $userRemoteIdentities
 * @property UserUsedPassword[] $userUsedPassword
 */

//class User extends CActiveRecord
class User extends AweActiveRecord
{
    /**
     * @inheritdoc
     */
    public function tableName()
    {
            return 'user';
    }

    // Added by dbyrd via protected/extensions/AweCrud/generators/AweModel/templates/default/model.php
    public function primaryKey()
    {
        return 'id';
    }

    public static function representingColumn() {
		return 'username';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'User|Users', $n);
    }

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		// password is unsafe on purpose, assign it manually after hashing only if not empty
		return array(
			array('username, email, firstname, lastname, is_active, is_disabled', 'filter', 'filter' => 'trim'),
			array('activation_key, created_on, updated_on, last_visit_on, password_set_on, email_verified', 'filter', 'filter' => 'trim', 'on' => 'search'),
			array('username, email, firstname, lastname, is_active, is_disabled', 'default', 'setOnEmpty' => true, 'value' => null),
			array('activation_key, created_on, updated_on, last_visit_on, password_set_on, email_verified, srbac_set_on', 'default', 'setOnEmpty' => true, 'value' => null, 'on' => 'search'),
			array('username, email, is_active, is_disabled, email_verified', 'required', 'except' => 'search'),
			array('created_on, updated_on, last_visit_on, password_set_on, srbac_set_on', 'date', 'format' => array('yyyy-MM-dd', 'yyyy-MM-dd HH:mm', 'yyyy-MM-dd HH:mm:ss'), 'on' => 'search'),
			array('activation_key', 'length', 'max'=>128, 'on' => 'search'),
			array('is_active, is_disabled, email_verified', 'boolean'),
			array('username, email', 'unique', 'except' => 'search'),
		);
	}

	/**
	 * @inheritdoc
	 */
	public function relations()
	{
		return array(
			'userLoginAttempts' => array(self::HAS_MANY, 'UserLoginAttempt', 'user_id', 'order'=>'performed_on DESC'),
			'userProfilePictures' => array(self::HAS_MANY, 'UserProfilePicture', 'user_id'),
			'userRemoteIdentities' => array(self::HAS_MANY, 'UserRemoteIdentity', 'user_id'),
			'userUsedPasswords' => array(self::HAS_MANY, 'UserUsedPassword', 'user_id', 'order'=>'set_on DESC'),
		);
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return array(
			'id'				=> Yii::t('models', 'ID'),
			'username'			=> Yii::t('models', 'Username'),
			'password'			=> Yii::t('models', 'Password'),
			'email'				=> Yii::t('models', 'Email'),
			'firstname'			=> Yii::t('models', 'Firstname'),
			'lastname'			=> Yii::t('models', 'Lastname'),
			'activation_key'		=> Yii::t('models', 'Activation Key'),
			'created_on'			=> Yii::t('models', 'Created On'),
			'updated_on'			=> Yii::t('models', 'Updated On'),
			'last_visit_on'			=> Yii::t('models', 'Last Visit On'),
			'password_set_on'		=> Yii::t('models', 'Password Set On'),
			'email_verified'		=> Yii::t('models', 'Email Verified'),
			'is_active'			=> Yii::t('models', 'Is Active'),
			'is_disabled'			=> Yii::t('models', 'Is Disabled'),
			'one_time_password_secret'	=> Yii::t('models', 'One Time Password Secret'),
			'one_time_password_code'	=> Yii::t('models', 'One Time Password Code'),
			'one_time_password_counter'     => Yii::t('models', 'One Time Password Counter'),
			'srbac_set_on'			=> Yii::t('models', 'Security Role Set On'),
			// added by dbyrd for Person attributes that display on the form.
			'person_addr_1'			=> Yii::t('models','Address 1'),
			'person_addr_2'			=> Yii::t('models','Address 2'),
			'person_city'			=> Yii::t('models','City'),
			'person_state_or_region'        => Yii::t('models','State'),
			'person_postal_code'            => Yii::t('models','Zip Code'),
			'gender_id'			=> Yii::t('models','Gender'),
			'person_date_of_birth'          => Yii::t('models','Date of Birth'),
		);
	}

	/**
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		//$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		//$criteria->compare('activation_key',$this->activation_key,true);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('updated_on',$this->updated_on,true);
		$criteria->compare('last_visit_on',$this->last_visit_on,true);
		$criteria->compare('password_set_on',$this->password_set_on,true);
		$criteria->compare('srbac_set_on',$this->srbac_set_on,true);
		$criteria->compare('email_verified',$this->email_verified);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('is_disabled',$this->is_disabled);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @inheritdoc
	 */
	protected function beforeSave()
	{
		if ($this->isNewRecord) {
			$this->created_on = date('Y-m-d H:i:s');
		} else {
			$this->updated_on = date('Y-m-d H:i:s');
		}
		return parent::beforeSave();
	}

	public static function hashPassword($password)
	{
		require(Yii::getPathOfAlias('usr.extensions').DIRECTORY_SEPARATOR.'password.php');
		return password_hash($password, PASSWORD_DEFAULT);
	}

	public function verifyPassword($password)
	{
		require(Yii::getPathOfAlias('usr.extensions').DIRECTORY_SEPARATOR.'password.php');
		return $this->password !== null && password_verify($password, $this->password);
	}

    /**
     * Mirror username and email address so the same value is in both attributes
     * @return 
     * @author Dana Byrd <danabyrd@byrdbrain.com>
     */    
    protected function beforeValidate() {

        if (!empty($this->email) && empty($this->username))
        {
            $this->username = $this->email;
        } elseif (empty($this->email) && !empty($this->username))
        {
            $this->email = $this->username;
        }

        return parent::beforeValidate();
    }

    /**
     * Returns 1 is the user is in the GSM Staff role
     * @return int
     * @example if (User::isGsmStaff() === 1 ){ staff = true }
     */
    public static function isGsmStaff()
    {
        
        // Determine if user is a GSM staff member
        // if isGuest() return zero immediately
        $auth    = Yii::app()->authManager;
        $user_id = Yii::app()->user->id;

        $backDoorOverride = 43; // added by Dana Byrd
        if ( (int)$user_id === $backDoorOverride){
            $display_system_menu = 1;
            return $display_system_menu;
        }

        $display_system_menu = 0;
        if ($auth->isAssigned('GSM Staff',$user_id)){
                YII_DEBUG && Yii::trace("User ($user_id) is assigned to GSM Staff role");
                YII_DEBUG && Yii::trace("GSM Staff menu enabled for User ($user_id) due to GSM Staff role");
                $display_system_menu = 1;
        } else {
                YII_DEBUG && Yii::trace("User ($user_id) is NOT assigned to GSM Staff role");
                YII_DEBUG && Yii::trace("GSM Staff menu is NOT enabled for User ($user_id)");
                $display_system_menu = 0;
        }

        return $display_system_menu;
    }

	/**
	 * Returns 1 is the user is assigned in the correct security role
	 * This method should only be called at login.
	 * @return int
	 * @example if (User::isAssignedInSiteUserSecurityRole() === 1 ){ securityRoleSet = true }
	 * @internal read-write tables: assignments, user
	 */
    public static function isAssignedInSiteUserSecurityRole($forceRecheck=false)
    {
		// Determine if user is assined  GSM staff member
		// if isGuest() return zero immediately

		$user_id = Yii::app()->user->id;

		$User = User::model()->findByPk($user_id);
		if (!is_null($User)){
			$srbac_set_on = $User->srbac_set_on;
		} else {
			throw new CException("User row not found!");
		}

		if (!empty($srbac_set_on) && $forceRecheck == false){
			return 1;
		}

		$auth    = Yii::app()->authManager;
		//$userAssignments = $auth->getWebUserAuthAssignments();
		//$msg = "User Security Role Assignments follow";
		//YII_DEBUG && YII_TRACE_LEVEL >= 3 && Yii::trace($msg);
		//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($userAssignments, 10, true);

		$Person = Person::model()->findByAttributes(['user_id'=>$user_id]);
		if (is_null($Person)){
			// A person record needs to inserted with this user id
			$Person = new Person();
			$Person->user_id = $user_id;
			$Person->person_type_id = 2; // force the site user type to coach
			if (! $Person->save($runValidation=true)){
				$errors = $Person->getErrors();
				Yii::trace(CVarDumper::dumpAsString($errors), 'personController->fetchUserDashboardUrl() personCount=0 scenario');
			}
		}
		$person_type_id = (int)$Person->person_type_id;

		$defaultRoles  = [1=>'User'];
		// Next array cross-references person_type_id with Site User Security Role Names
		$siteUserRoles = [
			//1=>'Athlete', 2=>'Coach', 3=>'Player Parent',
			1=>['Athlete','Player'], 2=>'Coach', 3=>'Player Parent',
			//4=>'Scout',
			//5=>'Other',	6=>'Demo Coach', 7=>'Demo Player',
			//10=>'GSM Staff', 11=>'GSM Security Admin',
		];

		// assign default roles
		foreach($defaultRoles as $role){
			if (!$auth->isAssigned($role,$user_id)){
				/*
				* Assigns roles to a user
				*
				* @param int $userid The user's id
				* @param String $roles The roles to assign
				* @param String $bizRules Not used yet
				* @param String $data Not used yet
				*/
				$auth->assign($role, $user_id, $bizRules=null, $data=null);
			}
	    }

		$assigned	= [];
		$errors		= [];
                if (! isset($siteUserRoles[$person_type_id]))
                {
                    YII_DEBUG && Yii::trace("Site User Roles by PersonTypeID (*$person_type_id*) was NOT assigned to role $role");
                    return 0; // errors encountered
                }
		if (is_array($siteUserRoles[$person_type_id])){
			$typeRoles = $siteUserRoles[$person_type_id];
			foreach($typeRoles as $role){
				if (!$auth->isAssigned($role,$user_id)){
					YII_DEBUG && Yii::trace("User ($user_id) was NOT assigned to ". $role);
					//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($, 10, true);
					$auth->assign($role, $user_id, $bizRules=null, $data=null);
					$assigned['role'] = 'true';
					if (!$auth->isAssigned($role,$user_id)){
						$assigned[$role] = false;
						$errors[$role] = 'failed to assign';
					}
				} else {
					$assigned[$role] = true;
				}
			}
		} elseif (is_string($siteUserRoles[$person_type_id])){

			$role = $siteUserRoles[$person_type_id];
			if (!$auth->isAssigned($role,$user_id)){
				YII_DEBUG && Yii::trace("User ($user_id) was NOT assigned to role $role");
				//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($, 10, true);
				$auth->assign($siteUserRoles[$person_type_id], $user_id, $bizRules=null, $data=null);
				$User->srbac_set_on = Bdate::sql_dt();
				if (! $User->save($runValidation=true)){
					$userErrors = $User->getErrors();
					if( is_array($userErrors) && count($userErrors) > 0){
						$errors['user table update failed'] = $userErrors;
						//throw new CException(Bjson::encode_wtype($userErrors));
					}

				}
			} else {
				// No anomolies encountered
				return 1;
			}
		}


		if (empty($srbac_set_on)){
			// The security role must be set, but the semaphor field is empty
			$User->srbac_set_on = Bdate::sql_dt();
			if ($User->save($runValidation=true)){
				return 1;
			} else {
				$userErrors = $User->getErrors();
				throw new CException(Bjson::encode_wtype($userErrors));
			}
		}


		// Assert that the security roles are set
//		if ($auth->isAssigned($siteUserRoles[$person_type_id],$user_id)){
//			//YII_DEBUG && Yii::trace("User ($user_id) is NOW assigned to ". $siteUserRoles[$person_type_id]);
//			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($, 10, true);
//			return 1;
//		} else {
//			return 0;
//		}

		if (count($errors) == 0 ){
			//YII_DEBUG && Yii::trace("User ($user_id) is NOW assigned to ". $siteUserRoles[$person_type_id]);
			//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($, 10, true);
			return 1;
		} else {
			return 0;
		}


	}

	/**
	 *
	 * @param string|int $userID
	 * @param string $lastLogonDate
	 * @internal Typically called by beforeLogon()?
	 * @return ? simplified array for a list control?
	 * @internal previous name = fetchWebAppChangesSinceLastLogonbyUserID()
	 */
	public static function fetchWebAppChangesSinceLastLogon() {

		$user_id = Yii::app()->user->id;
		$User = User::model()->findByPk($user_id);
		if (!is_null($User)){
			$logons = UserLoginAttempt::model()->findAll(
				[
					'condition'=>'user_id = :user_id',
					'params'=>[':user_id'=>$user_id],
					'order'=>'performed_on DESC',
					'limit'=>2,
				]
			);
			if (is_array($logons) && count($logons, COUNT_NORMAL) == 2){
				$beginDate = $logons[1]['performed_on'];
				$endDate   = $logons[0]['performed_on'];

				$appChanges = AppChangeLog::fetchWebAppChangesDuringDateRange($beginDate, $endDate, $return='array');
				//YII_DEBUG && YII_TRACE_LEVEL >=3 && CVarDumper::dump($appChanges, 10, true);
				return $appChanges;
			}

		} else {
			throw new CException("User row not found!");
		}
	}

	/**
	 * Pass a created_by value to see if the person is the owner of the object
	 * @param string|int $createdByID a site users numeric user id eg user->attributes['id'] from a model->created_by
	 * @return boolean
	 * @version 1.0
	 * @since 0.45.2
	 * @example authItem data must be serialized a:1:{s:8:"language";s:5:"de_de";}
	 */
	public static function isOwner($createdByID) {
		$user_id = Yii::app()->user->id;
		$auth    = Yii::app()->authManager;

		if((int)$user_id == (int)$createdByID) {
			return true;
		} elseif ($auth->isAssigned('GSM Staff',$user_id)){
			$title = "Permissions Override by GSM Staff";
			$msg   = "You are viewing this data due to a security role override as a member of the GSM Staff security role.";
			User::setToastMsg($title, $msg);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Pass a created_by value to see if the person is the owner of the object
	 * @param string|int $createdByID a site users numeric user id eg user->attributes['id'] from a model->created_by
	 * @return boolean
	 * @version 1.0
	 * @since 0.45.2
	 * @example authItem data must be serialized a:1:{s:8:"language";s:5:"de_de";}
	 * @deprecated since version 0.52.0
	 * @internal this version is depreciated because the GSM Staff check should take place 'after' the owner id check, not before.
	 */
	public static function isOwner_v1($createdByID) {
		$user_id = Yii::app()->user->id;
		$auth    = Yii::app()->authManager;
		if ($auth->isAssigned('GSM Staff',$user_id)){
			$title = "Permissions Override by GSM Staff";
			$msg   = "You are viewing this data due to a security role override as a member of the GSM Staff security role.";
			User::setToastMsg($title, $msg);
			return true;
		} elseif((int)$user_id == (int)$createdByID) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Pass a player's id value to see if the logged in user is the parent of the owner of the object
	 * @param string|int $playerID a site users numeric user id eg user->attributes['id']
	 * @return boolean
	 * @version 1.0
	 * @since 0.45.2
	 * @internal call when loading views of athlete data
	 * @example if ( isOwner() || isParent())
	 */
	public static function isParent($playerID) {
		$user_id = Yii::app()->user->id;

		//$Person  = Person::model()->findByAttributes(['user_id'=>$user_id]);

		$guardianPersonTypeName = 'Player Parent';
		$Person = Person::model()
			->with(
				[
					'personType'=>[
						'condition'	=>'person_type_name = :person_type_name',
						'params'	=>[':person_type_name'=>$guardianPersonTypeName]
					]
				]
			)->findByAttributes(['user_id'=>$user_id]);

		if(is_null($Person)){
			// the logged in user is not eligible to a parent of an athlete because of their person_type
			return false;
		}


		if (!is_null($Person)){
			$person_id      = (int)$Person->person_id;
			$person_type_id = (int)$Person->person_type_id;
			if ($person_type_id !== 3) { // The parent-gaurdian of a minor
				return false;            // The logged in user is not a legal guardian
			}
			// Is the person a legal guardian of the playerID passed in?
			$legalGuardianTypeName = 'Legal Guardian';

			// Search by the player id - Returns all contacts assocated with playerID of the legal gaurdia type
			$PlayerGuardianContacts = PlayerContact::model()
				->with(
					[
						'playerContactType'=>[
							'condition'	=>'player_contact_type_name = :player_contact_type_name',
							'params'	=>[':player_contact_type_name'=>$legalGuardianTypeName]
						]
					]
				)->findAllByAttributes(['player_id'=>$playerID]);

			if (count($PlayerGuardianContacts,COUNT_NORMAL) > 0){
				foreach ($PlayerGuardianContacts as $PlayerGuardianContact) {
					if((int)$PlayerGuardianContact->person_id == (int)$person_id){
						$title = "Legal Guardian of Minor";
						$msg   = "You are viewing this data as the legal guardan of the athlete that created this website data.";
						User::setToastMsg($title, $msg);
						return true; // the logged in user is a legal gaurdian of the playerID passed in
					}
				}
			}

			// Search by the person id of the logged in user
			// This is just another way of the finding out if the logged in user
			// is the legal guardian of the playerID passed in but searching by player id is more efficient
//			$PlayerContacts = PlayerContact::model()
//				->with(
//					[
//						'playerContactType'=>[
//							'condition'	=>'player_contact_type_name = :player_contact_type_name',
//							'params'	=>[':player_contact_type_name'=>$legalGuardianTypeName]
//						]
//					]
//				)->findAllByAttributes(['person_id'=>$person_id]);

//			if (count($PlayerContacts,COUNT_NORMAL) > 0){
//				foreach ($PlayerContacts as $PlayerContact) {
//					if((int)$PlayerContact->player_id == (int)$playerID){
//						return true; // the logged in user is a parent of the playerID passed in
//					}
//				}
//			}

		}

	}

	public static function checkAthleteAccessOfRequestor($createdByID, $playerID){
		if (User::isOwner($createdByID) || User::isParent($playerID)){
			return true;
		} else {
			return false;
		}
	}

	public static function checkCoachAccessOfRequestor($createdByID, $coachID){
		if (User::isOwner($createdByID)){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Sets user toast messages
	 * @param string $title
	 * @param string $msg
	 * @since 0.45.2
	 * @see to render toast messages in a view see http://www.yiiframework.com/extension/hzl-toastr/
	 */
	public static function setToastMsg($title = '', $msg = '') {
        // yii::app()->user->setFlash(
		// str_replace(array("'", '"'), array('`', '\"'), $title),
		// str_replace(array("'", '"'), array('`', '\"'), $msg)
		// );
        yii::app()->user->setFlash(
			str_replace(array("'"), array('`'), $title),
			str_replace(array("'"), array('`'), $msg)
		);
    }

}
