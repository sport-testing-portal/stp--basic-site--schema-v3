<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\School;

/**
 * app\models\SchoolSearch represents the model behind the search form about `app\models\School`.
 */
 class SchoolSearch extends School
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'org_id', 'religious_affiliation_id', 'conference_id__female', 'conference_id__male', 'conference_id__main', 'school_ipeds_id', 'school_unit_id', 'school_ope_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = School::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'school_id' => $this->school_id,
            'org_id' => $this->org_id,
            'religious_affiliation_id' => $this->religious_affiliation_id,
            'conference_id__female' => $this->conference_id__female,
            'conference_id__male' => $this->conference_id__male,
            'conference_id__main' => $this->conference_id__main,
            'school_ipeds_id' => $this->school_ipeds_id,
            'school_unit_id' => $this->school_unit_id,
            'school_ope_id' => $this->school_ope_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
