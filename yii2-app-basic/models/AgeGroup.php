<?php

namespace app\models;

use Yii;
use \app\models\base\AgeGroup as BaseAgeGroup;

/**
 * This is the model class for table "age_group".
 */
class AgeGroup extends BaseAgeGroup
{
    
    // Next four vars pulled from Yii1 MVP
    public static $p_tableName  = 'age_group';
    public static $p_primaryKey = 'age_group_id';
    public static $p_textColumn = 'age_group_name';
    public static $p_modelName  = 'AgeGroup';    
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['sport_id', 'age_group_min', 'age_group_max', 'display_order', 'created_by', 'updated_by'], 'integer'],
            [['age_group'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['age_group', 'age_group_desc'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'age_group_id' => 'Age Group ID',
            'sport_id' => 'Sport ID',
            'age_group' => 'Age Group',
            'age_group_desc' => 'Age Group Desc',
            'age_group_min' => 'Age Group Min',
            'age_group_max' => 'Age Group Max',
            'display_order' => 'Display Order',
            'lock' => 'Lock',
        ];
    }
    
    //**********************
    // code below added by dbyrd
    /*
     * @todo test fetchAllAsDropDownList()
     * @todo test fetchAllAsSelect2List()
     * @todo test searchBySelect2()
     * 
     */
    
	/**
	 * Return model values suitable for
	 *   TbActiveForm::dropDownList(), or
	 *   TbActiveForm->select2Row()
	 * @param bool $returnTags if true returns [0=>'rowValueText', 1='anotherRowValueText'] as zero based indexed array
	 * @return array[] Example ['1'=>'rowValueText', '2'='anotherRowValueText']
	 */
	public static function fetchAllAsDropDownList($returnTags=false) {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		// This table has to be sorted by display order since the items are not compatible with alpha sorting
		//$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by display_order";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		$list = CHtml::listData( $data, 'id', 'text'); //[]
		if ($returnTags){
			return array_values($list);
		} else {
			return $list;
		}
	}    

	/**
	 *
	 * @return array[] Example [0=>['id'=>1,'text'=>'rowValueText'], 1=>['id'=>'2', 'text'=>'anotherRowValueText'] ]
	 */
	public static function fetchAllAsSelect2List() {

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;

		$sql  = "select $pkName as id, $textColumn as `text` from $tableName order by $textColumn";
		$cmd  = Yii::app()->db->createCommand($sql);
		$data = $cmd->queryAll($fetchAssociative=true);
		return $data;
	}
        
	/**
	 * Call with $raw = 1 for a Select2 initial data load and return an array
	 * Call with $search='sometext' to perform a search and return a JSON object stream
	 * @param string $search typically a word fragment. A non null value causes the search result to be returned as a JSON object stream
	 * @param string|int $id Primary key of the table being accessed
	 * @param int $raw null or 0 has no effect, 1 means that a native array should be returned and NOT JSON
	 * @return array[] | json-object $out
	 * @version 1.1.1
	 * @since 0.46
	 * @internal called by athlete and coach views
	 * @author Dana Byrd <danabyrd@byrdbrain.com>
	 */
	public static function searchBySelect2($search = null, $id = null, $raw = null, $limit=null) {
		// Validate input parameters
		if ( !is_null($search) && $raw > 0){
			// a search and an initial data load ($raw > 0) are not compatible
			// initial data load and search must be performed indepently eg separately
			throw new CHttpException('500','Invalid data request');
		}

		$tableName  = self::$p_tableName;
		$pkName     = self::$p_primaryKey;
		$textColumn = self::$p_textColumn;
		$modelName  = self::$p_modelName;

		$out = ['more' => false];
		if (!is_null($search)) {
			$sql = "select $pkName as id, $textColumn as `text` "
				 . "from $tableName "
				 . "where $textColumn LIKE :$textColumn "
				 . "order by $textColumn";
			if ($limit > 0){
				$sql .= " limit $limit";
			}

			$cmd    = Yii::app()->db->createCommand($sql);
			$params = [':'.$textColumn=>'%'.$search.'%'];
			$data   = $cmd->queryAll($fetchAssociative=true,$params);
			$out['results'] = array_values($data);
		}
		elseif ($raw > 0 && $id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out  = [$id => $text];
				return $out; // return a native array NOT a JSON object
			} else {
				return ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		elseif ($raw > 0 && $id == 0) {
			// A null id was passed in on a select2 inital data load
			return ['id' => 0, 'text' => 'No matching records found'];
		}
		elseif ($id > 0) {
			$model = $modelName::model()->findByPk($id);
			if (!is_null($model)){
				$text = $model->$textColumn;
				$out['results'] = [$id => $text];
			} else {
				$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
			}
		}
		else {
			$out['results'] = ['id' => 0, 'text' => 'No matching records found'];
		}
		if(is_array($out)){
			if (! array_key_exists('more', $out) || !array_key_exists('results', $out)){
				throw new CHttpException('500','Invalid data request');
			}
			if (!is_null($search)){
				// The calling Select2 control is performing a search and will expect a JSON Object in a Stream Context
                                // // Next line is a yii1 call. The call is valid and definitely worked.
				//return Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
                                return app\modules\dbyrd\functions\Bjson::convertArrayToJsonObjectStreamAndEndYiiApp($out);
			}
		}
	}
        
}
