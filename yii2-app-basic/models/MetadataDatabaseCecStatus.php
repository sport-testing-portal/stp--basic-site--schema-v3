<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataDatabaseCecStatus as BaseMetadataDatabaseCecStatus;

/**
 * This is the model class for table "metadata__database_cec_status".
 */
class MetadataDatabaseCecStatus extends BaseMetadataDatabaseCecStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['database_id', 'database_cec_id', 'cec_status'], 'required'],
            [['database_id', 'database_cec_id', 'created_by', 'updated_by'], 'integer'],
            [['cec_status_at', 'created_at', 'updated_at'], 'safe'],
            [['cec_status', 'cec_status_tag', 'cec_status_bfr'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'cec_status_id' => 'Cec Status ID',
            'database_id' => 'Database ID',
            'database_cec_id' => 'Database Cec ID',
            'cec_status' => 'Cec Status',
            'cec_status_tag' => 'Cec Status Tag',
            'cec_status_bfr' => 'Cec Status Bfr',
            'cec_status_at' => 'Cec Status At',
            'lock' => 'Lock',
        ];
    }
}
