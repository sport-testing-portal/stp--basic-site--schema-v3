<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PlayerSchool]].
 *
 * @see PlayerSchool
 */
class PlayerSchoolQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PlayerSchool[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PlayerSchool|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
