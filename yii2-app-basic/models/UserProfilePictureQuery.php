<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserProfilePicture]].
 *
 * @see UserProfilePicture
 */
class UserProfilePictureQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return UserProfilePicture[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserProfilePicture|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
