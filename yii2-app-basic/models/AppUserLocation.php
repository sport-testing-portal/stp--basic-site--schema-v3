<?php

namespace app\models;

use Yii;
use \app\models\base\AppUserLocation as BaseAppUserLocation;

/**
 * This is the model class for table "app_user_location".
 */
class AppUserLocation extends BaseAppUserLocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'app_user_id', 'location_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['location_description'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_user_location_id' => 'App User Location ID',
            'user_id' => 'User ID',
            'app_user_id' => 'App User ID',
            'location_id' => 'Location ID',
            'location_description' => 'Location Description',
            'lock' => 'Lock',
        ];
    }
}
