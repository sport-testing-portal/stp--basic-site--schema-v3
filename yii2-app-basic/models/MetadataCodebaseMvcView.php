<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataCodebaseMvcView as BaseMetadataCodebaseMvcView;

/**
 * This is the model class for table "metadata__codebase_mvc_view".
 */
class MetadataCodebaseMvcView extends BaseMetadataCodebaseMvcView
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['codebase_id'], 'required'],
            [['codebase_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['view_function', 'view_file', 'view_params', 'view_url'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'codebase_mvc_view_id' => 'Codebase Mvc View ID',
            'codebase_id' => 'Codebase ID',
            'view_function' => 'View Function',
            'view_file' => 'View File',
            'view_params' => 'View Params',
            'view_url' => 'View Url',
            'lock' => 'Lock',
        ];
    }
}
