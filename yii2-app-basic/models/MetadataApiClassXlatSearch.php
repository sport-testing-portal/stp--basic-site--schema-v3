<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataApiClassXlat;

/**
 * app\models\MetadataApiClassXlatSearch represents the model behind the search form about `app\models\MetadataApiClassXlat`.
 */
 class MetadataApiClassXlatSearch extends MetadataApiClassXlat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['metadata__api_class_xlat_id', 'metadata__api_class_id_1', 'metadata__api_class_id_2', 'created_by', 'updated_by'], 'integer'],
            [['metadata__api_class_xlat', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataApiClassXlat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'metadata__api_class_xlat_id' => $this->metadata__api_class_xlat_id,
            'metadata__api_class_id_1' => $this->metadata__api_class_id_1,
            'metadata__api_class_id_2' => $this->metadata__api_class_id_2,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'metadata__api_class_xlat', $this->metadata__api_class_xlat])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
