<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SchoolStat]].
 *
 * @see SchoolStat
 */
class SchoolStatQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SchoolStat[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SchoolStat|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
