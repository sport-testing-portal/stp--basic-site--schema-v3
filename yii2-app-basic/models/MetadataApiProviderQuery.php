<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetadataApiProvider]].
 *
 * @see MetadataApiProvider
 */
class MetadataApiProviderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetadataApiProvider[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetadataApiProvider|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
