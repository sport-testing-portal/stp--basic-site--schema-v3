<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataCodebaseMvcControl as BaseMetadataCodebaseMvcControl;

/**
 * This is the model class for table "metadata__codebase_mvc_control".
 */
class MetadataCodebaseMvcControl extends BaseMetadataCodebaseMvcControl
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['codebase_id'], 'required'],
            [['codebase_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['controller_actions', 'action_function', 'action_params', 'action_url'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'codebase_mvc_control_id' => 'Codebase Mvc Control ID',
            'codebase_id' => 'Codebase ID',
            'controller_actions' => 'Controller Actions',
            'action_function' => 'Action Function',
            'action_params' => 'Action Params',
            'action_url' => 'Action Url',
            'lock' => 'Lock',
        ];
    }
}
