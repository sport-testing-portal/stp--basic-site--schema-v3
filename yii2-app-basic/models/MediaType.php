<?php

namespace app\models;

use Yii;
use \app\models\base\MediaType as BaseMediaType;

/**
 * This is the model class for table "media_type".
 */
class MediaType extends BaseMediaType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['media_type'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['media_type'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'media_type_id' => 'Media Type ID',
            'media_type' => 'Media Type',
            'lock' => 'Lock',
        ];
    }
}
