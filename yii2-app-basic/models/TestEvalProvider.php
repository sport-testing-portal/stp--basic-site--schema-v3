<?php

namespace app\models;

use Yii;
use \app\models\base\TestEvalProvider as BaseTestEvalProvider;

/**
 * This is the model class for table "test_eval_provider".
 */
class TestEvalProvider extends BaseTestEvalProvider
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['test_eval_provider'], 'required'],
            [['effective_begin_dt', 'effective_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['test_eval_provider', 'test_eval_provider_desc_short'], 'string', 'max' => 75],
            [['test_eval_provider_desc_long', 'test_eval_provider_website_url'], 'string', 'max' => 175],
            [['test_eval_provider_logo_url'], 'string', 'max' => 95],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'test_eval_provider_id' => 'Test Eval Provider ID',
            'test_eval_provider' => 'Test Eval Provider',
            'test_eval_provider_desc_short' => 'Test Eval Provider Desc Short',
            'test_eval_provider_desc_long' => 'Test Eval Provider Desc Long',
            'test_eval_provider_website_url' => 'Test Eval Provider Website Url',
            'test_eval_provider_logo_url' => 'Test Eval Provider Logo Url',
            'effective_begin_dt' => 'Effective Begin Dt',
            'effective_end_dt' => 'Effective End Dt',
            'lock' => 'Lock',
        ];
    }
}
