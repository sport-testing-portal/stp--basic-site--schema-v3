<?php

namespace app\models;

use Yii;
use \app\models\base\ZipCodeDistinct as BaseZipCodeDistinct;

/**
 * This is the model class for table "zip_code_distinct".
 */
class ZipCodeDistinct extends BaseZipCodeDistinct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['accuracy', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['country_code_iso2'], 'string', 'max' => 2],
            [['postal_code', 'state_code', 'county_code', 'community_code'], 'string', 'max' => 20],
            [['place_name'], 'string', 'max' => 180],
            [['state_name', 'community_name'], 'string', 'max' => 100],
            [['county_name', 'latitude', 'longitude'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'zip_code_id' => 'Zip Code ID',
            'country_code_iso2' => 'Country Code Iso2',
            'postal_code' => 'Postal Code',
            'place_name' => 'Place Name',
            'state_name' => 'State Name',
            'state_code' => 'State Code',
            'county_name' => 'County Name',
            'county_code' => 'County Code',
            'community_name' => 'Community Name',
            'community_code' => 'Community Code',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'accuracy' => 'Accuracy',
            'lock' => 'Lock',
        ];
    }
}
