<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PlayerMetrixByTheNumbersStructure]].
 *
 * @see PlayerMetrixByTheNumbersStructure
 */
class PlayerMetrixByTheNumbersStructureQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PlayerMetrixByTheNumbersStructure[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PlayerMetrixByTheNumbersStructure|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
