<?php

namespace app\models;

use Yii;
use \app\models\base\Sport as BaseSport;

/**
 * This is the model class for table "sport".
 */
class Sport extends BaseSport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['sport'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['sport', 'ncaa_sport_name', 'olympic_sport_name', 'xgame_sport_name'], 'string', 'max' => 45],
            [['sport_desc_short'], 'string', 'max' => 75],
            [['sport_desc_long'], 'string', 'max' => 175],
            [['high_school_yn', 'ncaa_yn', 'olympic_yn', 'xgame_sport_yn', 'lock'], 'string', 'max' => 1],
            [['ncaa_sport_type', 'olympic_sport_season', 'xgame_sport_season', 'olympic_sport_gender', 'xgame_sport_gender'], 'string', 'max' => 15],
            [['ncaa_sport_season_male', 'ncaa_sport_season_female', 'ncaa_sport_season_coed'], 'string', 'max' => 30],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'sport_id' => 'Sport ID',
            'sport' => 'Sport',
            'sport_desc_short' => 'Sport Desc Short',
            'sport_desc_long' => 'Sport Desc Long',
            'high_school_yn' => 'High School Yn',
            'ncaa_yn' => 'Ncaa Yn',
            'olympic_yn' => 'Olympic Yn',
            'xgame_sport_yn' => 'Xgame Sport Yn',
            'ncaa_sport_name' => 'Ncaa Sport Name',
            'olympic_sport_name' => 'Olympic Sport Name',
            'xgame_sport_name' => 'Xgame Sport Name',
            'ncaa_sport_type' => 'Ncaa Sport Type',
            'ncaa_sport_season_male' => 'Ncaa Sport Season Male',
            'ncaa_sport_season_female' => 'Ncaa Sport Season Female',
            'ncaa_sport_season_coed' => 'Ncaa Sport Season Coed',
            'olympic_sport_season' => 'Olympic Sport Season',
            'xgame_sport_season' => 'Xgame Sport Season',
            'olympic_sport_gender' => 'Olympic Sport Gender',
            'xgame_sport_gender' => 'Xgame Sport Gender',
            'lock' => 'Lock',
        ];
    }
}
