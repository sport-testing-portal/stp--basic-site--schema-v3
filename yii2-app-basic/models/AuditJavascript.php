<?php

namespace app\models;

use Yii;
use \app\models\base\AuditJavascript as BaseAuditJavascript;

/**
 * This is the model class for table "audit_javascript".
 */
class AuditJavascript extends BaseAuditJavascript
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['entry_id', 'created', 'type', 'message'], 'required'],
            [['entry_id'], 'integer'],
            [['created'], 'safe'],
            [['message', 'data'], 'string'],
            [['type'], 'string', 'max' => 20],
            [['origin'], 'string', 'max' => 512],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'entry_id' => 'Entry ID',
            'type' => 'Type',
            'message' => 'Message',
            'origin' => 'Origin',
            'data' => 'Data',
        ];
    }
}
