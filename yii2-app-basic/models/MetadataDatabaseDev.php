<?php

namespace app\models;

use Yii;
use \app\models\base\MetadataDatabaseDev as BaseMetadataDatabaseDev;

/**
 * This is the model class for table "metadata__database_dev".
 */
class MetadataDatabaseDev extends BaseMetadataDatabaseDev
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['database_dev'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['database_dev'], 'string', 'max' => 75],
            [['database_dev_desc_short', 'dev_status_tag'], 'string', 'max' => 45],
            [['database_dev_desc_long'], 'string', 'max' => 175],
            [['dev_scope'], 'string', 'max' => 253],
            [['rule_file_uri'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'database_dev_id' => 'Database Dev ID',
            'database_dev' => 'Database Dev',
            'database_dev_desc_short' => 'Database Dev Desc Short',
            'database_dev_desc_long' => 'Database Dev Desc Long',
            'dev_status_tag' => 'Dev Status Tag',
            'dev_scope' => 'Dev Scope',
            'rule_file_uri' => 'Rule File Uri',
            'lock' => 'Lock',
        ];
    }
}
