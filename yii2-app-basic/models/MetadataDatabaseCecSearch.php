<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataDatabaseCec;

/**
 * app\models\MetadataDatabaseCecSearch represents the model behind the search form about `app\models\MetadataDatabaseCec`.
 */
 class MetadataDatabaseCecSearch extends MetadataDatabaseCec
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['database_cec_id', 'created_by', 'updated_by'], 'integer'],
            [['database_cec', 'database_cec_desc_short', 'database_cec_desc_long', 'cec_status_tag', 'cec_scope', 'rule_file_uri', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataDatabaseCec::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'database_cec_id' => $this->database_cec_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'database_cec', $this->database_cec])
            ->andFilterWhere(['like', 'database_cec_desc_short', $this->database_cec_desc_short])
            ->andFilterWhere(['like', 'database_cec_desc_long', $this->database_cec_desc_long])
            ->andFilterWhere(['like', 'cec_status_tag', $this->cec_status_tag])
            ->andFilterWhere(['like', 'cec_scope', $this->cec_scope])
            ->andFilterWhere(['like', 'rule_file_uri', $this->rule_file_uri])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
