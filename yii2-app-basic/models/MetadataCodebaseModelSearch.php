<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MetadataCodebaseModel;

/**
 * app\models\MetadataCodebaseModelSearch represents the model behind the search form about `app\models\MetadataCodebaseModel`.
 */
 class MetadataCodebaseModelSearch extends MetadataCodebaseModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_model_id', 'codebase_id', 'created_by', 'updated_by'], 'integer'],
            [['json_data_structure', 'json_data_sample', 'json_data_example', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MetadataCodebaseModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'codebase_model_id' => $this->codebase_model_id,
            'codebase_id' => $this->codebase_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'json_data_structure', $this->json_data_structure])
            ->andFilterWhere(['like', 'json_data_sample', $this->json_data_sample])
            ->andFilterWhere(['like', 'json_data_example', $this->json_data_example])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
