<?php

namespace app\controllers;

use Yii;
use app\models\MetadataCodebase;
use app\models\MetadataCodebaseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MetadataCodebaseController implements the CRUD actions for MetadataCodebase model.
 * @since 0.7.0
 */
class MetadataCodebaseController extends Controller
{
    public $layout = 'column1-menu-metadata';
        
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MetadataCodebase models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MetadataCodebaseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MetadataCodebase model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMetadataCodebaseCecStatus = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataCodebaseCecStatuses,
        ]);
        $providerMetadataCodebaseDevStatus = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataCodebaseDevStatuses,
        ]);
        $providerMetadataCodebaseMvcControl = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataCodebaseMvcControls,
        ]);
        $providerMetadataCodebaseMvcModel = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataCodebaseMvcModels,
        ]);
        $providerMetadataCodebaseMvcView = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataCodebaseMvcViews,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMetadataCodebaseCecStatus' => $providerMetadataCodebaseCecStatus,
            'providerMetadataCodebaseDevStatus' => $providerMetadataCodebaseDevStatus,
            'providerMetadataCodebaseMvcControl' => $providerMetadataCodebaseMvcControl,
            'providerMetadataCodebaseMvcModel' => $providerMetadataCodebaseMvcModel,
            'providerMetadataCodebaseMvcView' => $providerMetadataCodebaseMvcView,
        ]);
    }

    /**
     * Creates a new MetadataCodebase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MetadataCodebase();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->codebase_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MetadataCodebase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new MetadataCodebase();
        }else{
            $model = $this->findModel($id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->codebase_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MetadataCodebase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
    * Creates a new MetadataCodebase model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($id) {
        $model = new MetadataCodebase();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($id);
        }
    
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->codebase_id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Finds the MetadataCodebase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MetadataCodebase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MetadataCodebase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataCodebaseCecStatus
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataCodebaseCecStatus()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataCodebaseCecStatus');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMetadataCodebaseCecStatus', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataCodebaseDevStatus
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataCodebaseDevStatus()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataCodebaseDevStatus');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMetadataCodebaseDevStatus', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataCodebaseMvcControl
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataCodebaseMvcControl()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataCodebaseMvcControl');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMetadataCodebaseMvcControl', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataCodebaseMvcModel
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataCodebaseMvcModel()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataCodebaseMvcModel');
            if( (Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) 
                || Yii::$app->request->post('_action') == 'add'){
                $row[] = [];
                }
            return $this->renderAjax('_formMetadataCodebaseMvcModel', ['row' => $row]);
                
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataCodebaseMvcView
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataCodebaseMvcView()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataCodebaseMvcView');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' 
                && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMetadataCodebaseMvcView', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    


    /**
     * Replaces all occurences in a CSS class or ID name with the provided replacement.
     * 
     * @param string $subject
     * @param array $search
     * @param array $replace
     * @return string 
     * @see https://stackoverflow.com/questions/7462815/regular-expression-for-css-class-names
     */
    function replaceCssClassNames($subject, $search, $replace)
    {
        // <editor-fold defaultstate="collapsed" desc="explanation of the regular expression">
        /*  To explain the regular expression:

        /([\#\.][a-z0-9\-_]*)(' . preg_quote($s) . ')/i
        
        It has to start with either # or .
        A letter, number or - _ can follow as many times as they want (*). 
         * (If you have funkier class names you might have to add characters.)
        The string in question.
        Also: I have used anonymous functions. If you are not using PHP 5.3.0+ you have 
        to actually create the two mapping functions, and 
         use: array_map(mySearchRegexFuncion, $search); instead.
        */
        // </editor-fold>
        
        // First generate all search regular expressions
        $searchRegexpressions = array_map(function($s)
        {
            return '/([\#\.][a-z0-9\-_]*)(' . preg_quote($s) . ')/i';
        }, $search);
        // Now all associated replacement expressions
        $replaceRegexpressions = array_map(function($r)
        {
            return '$1' . $r;
        }, $replace);

        do {
            // Now do the replacements as often as necessary to replace every occurence.
            $subject = preg_replace($searchRegexpressions, $replaceRegexpressions, $subject, -1, $count);
        } while ($count > 0);

        return $subject;
    }   
    
    //
    /**
     * 
     * @param type $param
     * @internal 
     * @see https://ryankyle.wordpress.com/2011/05/25/find-css-classes-in-html-using-regular-expressions/
     */
    protected function findCssClassNamesInPhpCode($param) {
        //$regex = 'class=[‘|"][a-zA-Z ^"’]*';
        $regex = 'class="(.+?)"|class=\'(.+?)\''; // by dbyrd - its dirt simple and it works
        // preg_quote($string, '/');
        /*
            $keywords = '$40 for a g3/400';
            $keywords = preg_quote($keywords, '/');

            echo $keywords;
            This will produce the following result −
                \$40 for a g3\/400        
        */
    }

    
    /**
     * 
     * @param type $param
     * @internal 
     * @see https://ryankyle.wordpress.com/2011/05/25/find-css-classes-in-html-using-regular-expressions/
     */
    protected function findCssClassNamesInCssFiles($param) {
        $regex = '\.className *[{|,]';
        // one regeg for both functions
        $regex = '(class=[‘|"][a-zA-Z ^"’]*<className>)|(\.className *[{|,])';
        $regex = '(.+?)\{'; //dbyrd
    }
}
