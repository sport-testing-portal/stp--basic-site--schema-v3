<?php

namespace app\controllers;

use Yii;
use app\models\MetadataApiClass;
use app\models\MetadataApiClassSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MetadataApiClassController implements the CRUD actions for MetadataApiClass model.
 * @since 0.8.0
 */
class MetadataApiClassController extends Controller
{
    public $layout = 'column1-menu-metadata';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MetadataApiClass models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MetadataApiClassSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MetadataApiClass model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMetadataApiClassFunc = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataApiClassFuncs,
        ]);
        $providerMetadataApiClassXlat = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataApiClassXlats,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMetadataApiClassFunc' => $providerMetadataApiClassFunc,
            'providerMetadataApiClassXlat' => $providerMetadataApiClassXlat,
        ]);
    }

    /**
     * Creates a new MetadataApiClass model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MetadataApiClass();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->metadata__api_class_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MetadataApiClass model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new MetadataApiClass();
        }else{
            $model = $this->findModel($id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->metadata__api_class_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MetadataApiClass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
    * Creates a new MetadataApiClass model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($id) {
        $model = new MetadataApiClass();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($id);
        }
    
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->metadata__api_class_id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Finds the MetadataApiClass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MetadataApiClass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MetadataApiClass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataApiClassFunc
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataApiClassFunc()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataApiClassFunc');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMetadataApiClassFunc', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataApiClassXlat
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataApiClassXlat()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataApiClassXlat');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMetadataApiClassXlat', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /* Additions by Dana Byrd post scaffolding */
    

    /**
    * Action to load Yii 2.0 classes into models
    * for MetadataApiClassXlat
    * @author Dana Byrd <danabyrd@byrdbrain.com>
    * @param string $id Yii API ID (1-1, 2-0)
    * @internal Loop through a json file importing non-duplicate rows
    * @internal dev status = alpha-level-functional, not productionalized, not golden!
    * @todo finish the return value for this method
    * @internal Runs a shell command in real-time to acquire a file. This function is specific to one api provider.
    * @internal Routes /metadata-api-class/import-yii-api-classes-from-json 
    * @example http://localhost:8212/metadata-api-class/import-yii-api-classes-from-json?id=1-1
    * @example http://localhost:8212/metadata-api-class/import-yii-api-classes-from-json?id=2-0
    * @return mixed
    */
    public function actionImportYiiApiClassesFromJson($id)
    {
        // Sample shell commands that are executed manually
        // curl -o /var/www/sites/orgs/gitlab/stp--basic-site--schema-v3/yii2-app-basic/yii2-api-v1-1x.json https://www.yiiframework.com/doc/api/1.1 -H 'Accept: application/json'
        // curl -o /var/www/sites/orgs/gitlab/stp--basic-site--schema-v3/yii2-app-basic/yii2-api-v2-0.json https://www.yiiframework.com/doc/api/2.0 -H 'Accept: application/json'
        $fileJson = '';
        
        $duplicatesFound = [];
        $rowsInserted    = [];
        
        $pathInfo  = Yii::$app->params['data_input__by_file__path'];
        $inboundBasePath  = $pathInfo['base_folder2'];
        $realInboundBasePath = Yii::$app->basePath . $inboundBasePath;
        //echo 'Inbound BasePath <pre><code>' . \yii\helpers\VarDumper::dumpAsString($inboundBasePath) . '</pre></code>';
        //echo 'Real Inbound BasePath <pre><code>' . \yii\helpers\VarDumper::dumpAsString($realInboundBasePath) . '</pre></code>';
        
        $inboundDataPath  = $realInboundBasePath . $pathInfo['to_process'];
        
        $sourceFile1 = 'yii2-api-v1-1x.json';
        $sourceFile2 = 'yii2-api-v2-0.json';
        if ($id == '1-1'){
            $sourceFile = $inboundDataPath . $sourceFile1;
            $apiProviderId = 1;
         
        } else {
            // default to current api
            $sourceFile = $inboundDataPath . $sourceFile2;
            $apiProviderId = 2;
        }
        //echo '<pre><code>' . \yii\helpers\VarDumper::dumpAsString($sourceFile) . '</pre></code>';
//                $fileJsonContents = file_get_contents($sourceFile);
//                $fileJson = \yii\helpers\Json::decode($fileJsonContents);


        
        $jsonFields  = ['name','description','url'];
        
        
        $modelCount = MetadataApiClass::find()
            ->where(['metadata__api_provider_id'=>$apiProviderId])
            ->indexBy('metadata__api_class_id')
            ->count();
        //echo 'Model Count <pre><code>' . \yii\helpers\VarDumper::dumpAsString($modelCount) . '</pre></code>';
//            echo 'Models <pre><code>' . \yii\helpers\VarDumper::dumpAsString($models) . '</pre></code>';

        if (file_exists($sourceFile) !== false){
            $fileJsonContents = file_get_contents($sourceFile);
            $fileJson = \yii\helpers\Json::decode($fileJsonContents);
            //echo 'Source File <pre><code>' . yii\helpers\VarDumper::dumpAsString($fileJson) . '</pre></code>';
        } else {
            echo 'Source File <b>not found</b><pre><code>' . yii\helpers\VarDumper::dumpAsString($sourceFile) . '</pre></code>';
        }         
//        $model = MetadataApiClass::find(1);
        
        //$modelFields = MetadataApiClass::className()->attributes;
        
//        echo '<pre><code>' . yii\helpers\VarDumper::dumpAsString($model) . '</pre></code>';
//        if ( ! file_exists($fileJson) !== true){
//            return false;
//        }
        foreach ($fileJson as $idx1=>$array1){
            if ($idx1 === 'classes'){
                foreach ($array1 as $idx2=>$array2){
                    $row = [];
                    //echo '<pre><code>' . yii\helpers\VarDumper::dumpAsString($array2) . '</pre></code>';
                    foreach ($jsonFields as $JsonFieldName){
                        $value = (isset($array2[$JsonFieldName])? $array2[$JsonFieldName] :'');
                        $row[$JsonFieldName] = $value;
                    }    
                    if (! empty($row)){
                        // test for the presence of the row values
//                        echo 'json vals <pre><code>' . yii\helpers\VarDumper::dumpAsString($row) . '</pre></code>';
                        $modelVals=[
                            'metadata__api_provider_id'   =>$apiProviderId,
                            'metadata__api_class'         =>$row['name'],
                            'metadata__api_class_desc'    =>$row['description'],
                            'metadata__api_class_docs_url'=>$row['url'],
                        ];
//                        echo 'model vals <pre><code>' . yii\helpers\VarDumper::dumpAsString($modelVals) . '</pre></code>';
                        
                        $existingRow = MetadataApiClass::find()
                            ->where([
                                'metadata__api_provider_id'=>$apiProviderId,
                                'metadata__api_class'=>$row['name']
                                ])
                            ->all();
                        if (!empty($existingRow)){
                            // duplicate found
                            //echo 'duplicate found <pre><code>' . yii\helpers\VarDumper::dumpAsString($existingRow) . '</pre></code>';
                            $duplicatesFound[] = $existingRow;
                            continue;
                        }
                        $model = new MetadataApiClass();
                        $model->attributes = $modelVals;
                        $result = $model->save();
                        if($result){
//                            echo 'save result <pre><code>' . yii\helpers\VarDumper::dumpAsString($result) . '</pre></code>';
                            $rowsInserted[] = $row;
                        }
                    }
//                    $type = (isset($array2['type'])? $array2['type'] :'');
//                    $type = (isset($array2['type'])? $array2['type'] :'');
//                    $type = (isset($array2['type'])? $array2['type'] :'');
                }
            }
        }
        echo 'Rows inserted <pre><code>' . yii\helpers\VarDumper::dumpAsString($rowsInserted) . '</pre></code>';
    }    
}
