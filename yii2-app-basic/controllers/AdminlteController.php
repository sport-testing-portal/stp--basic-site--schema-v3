<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use vendor\dektrium\user\models;

class AdminlteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // The views use model attributes and model methods so passing a simple array is right out
        $this->layout = 'adminlte-main';
        //return $this->render('testing/app/default/index');
        //$params=[Yii::$app->user->id];
        //echo '<pre><code>' . \yii\helpers\VarDumper::dumpAsString($params) . '</pre></code>';
        // next line returns an active query
        //$user = new \dektrium\user\models\User();
        
        // The views use model attributes and model methods so passing a simple array is right out
        //$user = \dektrium\user\models\User::find(['user_id', Yii::$app->user->id])->with('profile')->asArray()->one();
        $user = \dektrium\user\models\User::find(['user_id', Yii::$app->user->id])->with('profile')->one();
        //echo '\$user' . '<pre><code>' . \yii\helpers\VarDumper::dumpAsString($user) . '</pre></code>';
        
        //$profile = new \dektrium\user\models\Profile();
        //$model2 = $profile->find(['user_id', Yii::$app->user->id])->with('user')->asArray();
        $profile = $user['profile'];
        //var_dump($profile);
        //$profile = $user->getRelatedRecords('profile');
        //echo '$profile <pre><code>' . \yii\helpers\VarDumper::dumpAsString($profile['profile']) . '</pre></code>';
        $this->layout = 'adminlte-main';
        //return $this->render('testing/app/default/index', ['profile'=>$profile]);        
        return $this->render('testing/app/default/index', ['profile'=>$profile]);        
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionFront()
    {
        $this->layout = 'adminlte-main';
        return $this->render('testing/app/default/index');
    }
    


    
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }    
}
