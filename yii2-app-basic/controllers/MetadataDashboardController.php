<?php

/* 
 * @package integrate codebase-with-database v0.0.0.4 (To insert all mvc code files names and paths into the database so the code files can be managed more efficiently and effectively)
 * @package database driven bulk find-replace v0.0.0.3 (To Mass manipulate the code by rule-sets stored in the database)
 * @package cec tracking v0.0.0.0      (to know the code by its adherence to certain design principles)
 * @package package tracking v0.0.0.0  (to know the code by its call stacks eg it's actionable trees of code)
 * @package automated system tests v0.0.0.0  (to validate the code base and specifically codebase-to-database interactions)
 */

/**
 * CEC - Consistent Engineering Criteria
 * cec-y1c-0001-code-layout-00 - A controller is a collection of packages. 
 * Each package has an inherent code maturity and code attrition level. 
 * A package is a code implementation of a desired feature.
 * ? List features implemented by the controller file?
 * ? List the packages of the controller
 * ? List the packages by feature
 * ? I want to see the code base by features, packages, and action trees eg code-coverage and call stacks
 * My purpose : to bring divine clarity, structure, and understanding to the code base and application.
 */

namespace app\controllers;

use Yii;
use app\models\MetadataCodebase;
use app\models\MetadataCodebaseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\base\UserException;

//use \app\modules\dbyrd\functions;

/**
 * MetadataCodebaseController implements the CRUD actions for MetadataCodebase model.
 * @since 0.7.0
 */
class MetadataDashboardController extends Controller
{
    public $layout = 'column1-menu-metadata';
    //public $defaultAction = 'admin';
    protected $appPath = '';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    
    /**
     * Lists all MetadataCodebase models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MetadataCodebaseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$this->layout='column1-menu.php'; // this works
        //$this->getDatabaseSchema3Stats([]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MetadataCodebase model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMetadataCodebaseCecStatus = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataCodebaseCecStatuses,
        ]);
        $providerMetadataCodebaseControl = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataCodebaseControls,
        ]);
        $providerMetadataCodebaseView = new \yii\data\ArrayDataProvider([
            'allModels' => $model->metadataCodebaseViews,
        ]);
        $this->layout='//layouts/column1-menu.php';
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMetadataCodebaseCecStatus' => $providerMetadataCodebaseCecStatus,
            'providerMetadataCodebaseControl' => $providerMetadataCodebaseControl,
            'providerMetadataCodebaseView' => $providerMetadataCodebaseView,
        ]);
    }

    /**
     * Creates a new MetadataCodebase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MetadataCodebase();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->codebase_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MetadataCodebase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new MetadataCodebase();
        }else{
            $model = $this->findModel($id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->codebase_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MetadataCodebase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
    * Creates a new MetadataCodebase model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($id) {
        $model = new MetadataCodebase();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($id);
        }
    
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->codebase_id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Finds the MetadataCodebase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MetadataCodebase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MetadataCodebase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataCodebaseCecStatus
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataCodebaseCecStatus()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataCodebaseCecStatus');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMetadataCodebaseCecStatus', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataCodebaseControl
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataCodebaseControl()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataCodebaseControl');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMetadataCodebaseControl', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MetadataCodebaseView
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMetadataCodebaseView()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MetadataCodebaseView');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMetadataCodebaseView', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function getFilesFromDisk($params) {
        // by file function?
        $codeFunctions = \app\models\MetadataCodebaseFunction::find()->all();
        
        
    }
    
    
    
// mature code
    
    // <editor-fold defaultstate="collapsed" desc="package integrate codebase-with-database v0.0.0.4">
    // * package name: integrate codebase-with-database v0.0.0.4 
    // * package purpose: To insert all mvc code files names and paths into the 
    // database so the code files can be managed more efficiently and effectively.

    
    /**
     * Load code base table with entries
     * @package bulk find-replace v1
     */
    public function actionPopulate()
    {
        $basePath = Yii::getAlias('@app'); // .../yii2-app-basic
        $codeFolders = [
            'models'         => $basePath . '/models',
            'models/base'    => $basePath . '/models/base',
            'controllers'    => $basePath . '/controllers',
            'views'          => $basePath . '/views',
            ];
        if (!is_dir($codeFolders['models']) !== false){
            //$this->echo_array($codeFolders);
            throw new UserException('invalid file path!');
        }

        $codeFound=[];
        foreach ($codeFolders as $key=>$folderName) {

            $files = $this->globFiles($folderName); 
            if ($key !== 'views'){
                $codeFound[$key] = $files; 
            }
            if ($key === 'views'){
                // handle recursively
                foreach ($files as $viewFolder){
                    if (is_array($viewFolder)){
                        $uri = $viewFolder['file_uri'];
                        if (is_dir($uri)){
                            $viewFiles = $this->globFiles($uri);
                            $codeFound[] = $viewFiles;
                        }
                    }
                }
            }
        }
        $result = $this->insertGlobItems($codeFound);
        $insertCnt = 0;
        $foundCnt  = 0;
        foreach($result as $resultKey1=>$resultShell){
            foreach ($resultShell as $resultKey2=>$innerResult){
                if (array_key_exists('insert', $innerResult)){
                    if ((int)$innerResult['insert'] === 0){
                        $foundCnt+= 1;
                    }
                    $insertCnt += (int)$innerResult['insert'];
                }
            }
        }
        unset($resultKey1, $resultKey2);
        echo "\$foundCnt = $foundCnt, \$insertCnt = $insertCnt \n";
        //var_dump($result);

    }       
    
    // code in development    
    
    // <editor-fold defaultstate="collapsed" desc="package cec tracking v0.0.0.0">
    // package name: cec tracking v0.0.0.0      
    // package purpose: To know the code and its maturity status by its adherence 
    // to certain design principles.
    
    /** @package-sub-module *************(get uri from the database )**********/
    
    /**
     * Converts an array in glob format uri into relative uri format
     * @return array $modelsRelativeUri
     * @calledBy cecAddNewFilesToCodebaseTracking
     * 
     * @package CEC Tracking v0.0.0.1
     */
    protected function fetchMvcModelsAllRelativeUri() {
        $verbose = false;
        $method = "fetchMvcModels AllRelativeUri()";
        $models = $this->fetchMvcModelsAll();
        if (is_array($models) && empty($models)){
            echo "the call to fetchMvcModelsAll() returned an empty array <br>";
            return [];
        } elseif (is_array($models) && !empty ($models)) {
             //  this is the nominal code path
            echo "<b>$method</b> ";
            echo "the call to fetchMvcModelsAll() returned an array with <b>" . count($models) 
                .  "</b> items in the array <br>";                
        } else {
            echo "the call to fetchMvcModelsAll() returned a non-array <br>";
            return [];
        }
        
        $modelsRelativeUri = [];
        foreach ($models as $model){
            // path + file name arrary
            $relativeUri = $model->file_path_uri . '/' . $model->file_name;
            $modelsRelativeUri[] = $relativeUri;
        }
        if ($verbose){
            if (is_array($modelsRelativeUri)){
                echo "\$modelsRelativeUri to follow <br>";
                //echo Barray::echo_array($modelsRelativeUri);
                echo Yii::$app->barray->echo_array2($modelsRelativeUri);
            }  
        }
        return $modelsRelativeUri;
    }

    /**
     * Convert an array in database format into a relative uri format
     * @return array $viewRelativeUri
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @calledBy actionLoadAllViews
     * @calls fetchMvcViewsAll
     * 
     * @package CEC Tracking v0.0.0.1
     */
    protected function fetchMvcViewsAllRelativeUri() {
        $verbose = false;
        $method = "fetchMvcViews AllRelativeUri()";
        $models = $this->fetchMvcViewsAll();
        if (is_array($models) && empty($models)){
            echo "fetchMvcViewsAll() returned an empty array <br>";
            return [];
        } elseif (is_array($models) && !empty ($models)) {
             //  this is the nominal code path
            echo "<b>$method</b> ";
            echo "the call to fetchMvcViewsAll() returned an array with <b>" . count($models) 
                .  "</b> items in the array <br>";              
        } else {
            echo "fetchMvcViewsAll() returned a non-array <br>";
            return [];
        }
        $viewRelativeUri = [];
        foreach ($models as $model){
            // path + file name arrary
            $relativeUri = $model->file_path_uri . '/' . $model->file_name;
            $viewRelativeUri[] = $relativeUri;
        }
        if ($verbose){
            if (is_array($viewRelativeUri)){
                echo "\$viewRelativeUri to follow <br>";
                echo Barray::echo_array($viewRelativeUri);
            }  
        }
        return $viewRelativeUri;
    }

    /**
     * Convert an array in database format into a relative uri format
     * @return array $viewRelativeUri
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @calledBy actionLoadAllViews
     * @calls fetchMvcViewsAll
     * 
     * @package CEC Tracking v0.0.0.1
     */
    protected function fetchMvcViewsAllRelativeUri_ports() {
        $verbose = false;
        $method = "fetchMvcViews AllRelativeUri()";
        $models = $this->fetchMvcViewsAll();
        if (is_array($models) && empty($models)){
            echo "fetchMvcViewsAll() returned an empty array <br>";
            return [];
        } elseif (is_array($models) && !empty ($models)) {
             //  this is the nominal code path
            echo "<b>$method</b> ";
            echo "the call to fetchMvcViewsAll() returned an array with <b>" . count($models) 
                .  "</b> items in the array <br>";              
        } else {
            echo "fetchMvcViewsAll() returned a non-array <br>";
            return [];
        }
        $viewRelativeUri = [];
        foreach ($models as $model){
            // path + file name arrary
            $relativeUri = $model->file_path_uri . '/_ports/' . $model->file_name;
            $viewRelativeUri[] = $relativeUri;
        }
        if ($verbose){
            if (is_array($viewRelativeUri)){
                echo "\$viewRelativeUri to follow <br>";
                echo Barray::echo_array($viewRelativeUri);
            }  
        }
        return $viewRelativeUri;
    }
    
    /**
     * Get a list of relative Uri from the metadata__codebase table
     * @return array $modelsRelativeUri
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @todo silence the intra if statement echos
     * @internal dev status = Testing Passed. Appears to be functionally solid.
     * @internal dev status = still outputs debug text.
     * @package CEC Tracking v0.0.0.1
     */
    protected function fetchMvcControllersAllRelativeUri() {
        $verbose = false;
        $method = "fetchMvcControllers AllRelativeUri()";
        $models = $this->fetchMvcControllersAll();
        if (is_null($models)){
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned null <br>";
            return [];
        }
        if (is_array($models) && empty($models)){
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned an empty array <br>";
            return [];
        } elseif (is_object($models)){
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned a single object rather than an array <br>";
            return [];
        } elseif (is_array($models) && !empty ($models)) {
             //  this is the nominal code path
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned an array with <b>" . count($models) 
                .  "</b> items in the array <br>";  
        } else {
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned a non-array non-object<br>";
            return [];
        }
        $controllerRelativeUri = [];
        foreach ($models as $model){
            // path + file name arrary
            $relativeUri = $model->file_path_uri . '/' . $model->file_name;
            $controllerRelativeUri[] = $relativeUri;
        }
        if ($verbose){
            if (is_array($controllerRelativeUri)){
                echo "<b>$method</b> running in verbose mode<br>";
                echo "\$controllersRelativeUri to follow <br>";
                echo Barray::echo_array($controllerRelativeUri);
            }  
        }
        return $controllerRelativeUri;
    }
    
    /** @package-sub-module END *********(get uri from the database )**********/
    
    /**
     * 
     * @param type $glob
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @package CEC Tracking v0.0.0.1
     */
    protected function convertGlobToRelativeUri($glob) {
        //$glob = Bfile::globFiles($sourceFolder);
        $filesRelativeUri=[];
        $basePath = Yii::app()->basePath;
        foreach ($glob as $item){
            if (!empty($item['file_uri'])){
                $fileUri = $item['file_uri'];
                //$fileName = Bfile::base_name($fileUri);
                $fileName = Yii::$app->bfile->base_name($fileUri);
                $fileUri2 = str_ireplace($basePath, '', $fileUri);  // base path removed
                $fileUri3 = str_ireplace($fileName, '', $fileUri2); // file name removed
                //$fileUri4 = Bfile::remove_trailing_slash($fileUri3);                            
                $filesRelativeUri[] = $fileUri3 . $fileName; 
            }
        }
        $verbose=false;
        if ($verbose){
            if (is_array($filesRelativeUri)){
                echo "\$filesRelativeUri to follow <br>";
                //echo Barray::echo_array($filesRelativeUri);
                echo Yii::$app->barray->echo_array2($filesRelativeUri) . "<br>";
            }     
        }
        return $filesRelativeUri;
    }
    
    /**
     * Determines which relative URI are not in the database
     * @param array $target A list of file URI from the hard disk
     * @param array $source A list of file URI from the database
     * @return array $return A list of uri in target that are not in source 
     * 
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @example findOrphansInUriArrays($fileUri,$modelUri)
     * 
     * @internal $target is typically a list of files from disk
     * @internal $source is typically a list from the database
     * @internal $return is list of files on disk that are not in the database
     * @package cec-y1x-cec-tracking-v0.0.0.1
     * @internal development status = Golden!
     */
    protected function findOrphansInUriArrays($target, $source) {
        $work = $target;
        $verbose=false;
        $debug=true;
        if ($debug){
            echo 'findOrphans - InUriArrays '
                . '<b>' .count($target) . ' files </b> uri (target) passed in and ' 
                . '<b>'. count($source) . ' models </b> uri (source) passed in <br>';
        }
        foreach ($source as $sourceItem){
            if (array_search($sourceItem, $work, $strict=true) !== false){
                $key = array_search($sourceItem, $work, $strict=true);
                $value = $work[$key];
                if ($verbose){
                    echo "source item = $sourceItem <br>";
                    echo "key = $key <br>";
                    echo "value in work = $value <br>";
                }
                if ($sourceItem == $value){
                    if ($verbose){echo "items match <br>";}
                    unset($work[$key]);
                }
            }
        }
        if ($debug){
            $removedCnt = (count($target) - count($work));
            echo 'findOrphans - InUriArrays '
                . '<b>Removed ' .$removedCnt . ' file uri</b> from the '.count($target). ' target uri passed in <br>'; 
                //. '<b>'. count($source) . '</b> model uri (source) passed in <br>';
        }        
        
        // remove directories
        $work2 = $work;

        foreach ($work as $key2 => $item){
            if (stripos($item, '//') !== false){
                if ($verbose){echo "removing item $item <br>";}
                unset($work2[$key2]);
            }            
        }
        
        if ($debug){
            $removedDirCnt = (count($work) - count($work2));
            echo 'findOrphans - InUriArrays '
                . '<b>Removed '.$removedDirCnt.' directories </b>  from the targets passed in <br>'; 
                //. '<b>'. count($source) . '</b> model uri (source) passed in <br>';
            echo 'findOrphans - InUriArrays '
                . '<b>Found ' . count($work2) . ' orphan </b> file uri in the targets passed in <br>';             
            echo Barray::echo_array($work2);
        }        
        //echo Barray::echo_array($work2);
        $return = $work2;
        return $return;
    }
    
    /**
     * 
     * @return array $viewFiles A one dimensional array of file globs and No directories
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @calledBy actionLoadAllViewByDisk
     * @internal development status = Golden!
     * @package CEC Tracking v0.0.0.1
     */
    protected function globMvcViews($sourceFolderUri) {
        $viewFiles=[];
        //$viewRootDir = Bfile::globFiles($sourceFolderUri);           // yii1
        $viewRootDir = Yii::$app->bfile->globFiles2($sourceFolderUri); // yii2
        foreach ($viewRootDir as $globbedItem){
            $uri='';
            if (isset($globbedItem['file_uri']))
            {
                $uri = $globbedItem['file_uri'];
                if (strlen($uri)>1)
                {
                    $length=1;
                    $lastChar = substr($uri, strlen($uri)-1, $length);
                    //echo "last char = $lastChar <br>";

                    if ($lastChar === '/')
                    {

                        if (is_dir($uri)){
                            //$viewDir = Bfile::globFiles($uri);
                            $viewDir = Yii::$app->bfile->globFiles2($uri);
                            if ( is_array($viewDir) )
                            {
                                //echo Barray::echo_array($viewDir);
                                foreach ($viewDir as $fileGlob) {
                                    $viewFiles[]=$fileGlob;
                                }
                            //$viewFiles[]=$viewDir;
                            }
                        } 
                    } elseif(is_file($uri)) {
                        $viewFiles[]=$globbedItem;
                    }
                }

            }
        }

        echo "<b>Found " . count($viewFiles) . " files</b> on disk <br>";        
        return $viewFiles;
    }
    
    /**
     * Load code base table with entries
     * @todo prevent duplicate entries
     * @calls $this->parseFileUriFromAFileList($fileList)
     * @calls Bfile::globFiles()
     * @internal dev status = Golden!
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesOnDisk?XDEBUG_SESSION_START=netbeans-xdebug
     * @internal http://localhost:8212/metadata-dashboard/find-code-files-on-disk?XDEBUG_SESSION_START=netbeans-xdebug
     * @package bulk find-replace v1
     */
    public function actionFindCodeFilesOnDisk()
    {
        //$filepath = Yii::app()->basePath.'/files/foldername/filename.ext';
        //$basePath = Yii::app()->basePath;
        $basePath = Yii::getAlias('@app');
        $codeFolders = [
            'models'         => $basePath . '/models',
            'models/base'    => $basePath . '/models/base',
            'controllers'    => $basePath . '/controllers',
            'views'          => $basePath . '/views',
            ];

        $itemsProcessedCnt   = 0;
        $itemsProcessed      = [];
        $foldersProcessedCnt = 0;
        $foldersProcessed    = [];
        $filesProcessedCnt   = 0;
        $filesProcessed      = [];
        
        $maxFilesToProcessCnt = 0;   
        
        foreach ($codeFolders as $folderNameStem=>$folderPath) 
        {
            // add a throttle for testing a small batch of files
            if ($maxFilesToProcessCnt > 0 && $filesProcessedCnt >= $maxFilesToProcessCnt ){
                break;
            }

            $files = $this->globFiles($folderPath);
            $fileURI=[];
            if (is_array($files)){
                //echo "its an array test for file uri <b>$folderNameStem</b><br>";
                foreach ($files as $fileMetadata){
                    if (isset($fileMetadata['file_uri']) ){
                        $fileURI[]= $fileMetadata['file_uri'];
                    }
                }
                if (1===2){
                    echo "file uri follow <br>";
                    //Barray::echo_array($fileURI);
                }
            }

            foreach ($fileURI as $fileOrFolder){
                $itemsProcessedCnt++;
                if (is_file($fileOrFolder)){
                    //echo "is file: $fileOrFolder <br>";
                    $filesProcessedCnt++;
                    $filesProcessed[] = $fileOrFolder;
                }                
                if (is_dir($fileOrFolder) && $folderNameStem === 'views'){
                    $foldersProcessedCnt++;
                    $foldersProcessed[] = $fileOrFolder;
                    //$fileList = Bfile::globFiles($fileOrFolder);
                    $fileList = $this->globFiles($fileOrFolder);
                    if (is_array($fileList)){
                        //echo "found a directory <b>$fileOrFolder</b><br>";
                        $filesInFolder = $this->parseFileUriFromAFileList($fileList);
                        foreach ($filesInFolder as $file_uri_item) {
                            $itemsProcessedCnt++;
                            $itemsProcessed[] = $file_uri_item;
                            if (is_dir($file_uri_item)){
                                //echo "<b>is a sub folder</b> $file_uri_item <br>";
                            }
                            if (is_file($file_uri_item)){
                                //echo "is file in sub folder $file_uri_item <br>";
                                $filesProcessedCnt++;
                                $filesProcessed[] = $file_uri_item;
                            }
                        }
                    }
                }
            }
        }
        $verboseYN = 'N';
        if ($verboseYN === 'Y'){
            echo "<h1>items processed $itemsProcessedCnt </h1><br>";
            echo "<h2>folders processed $foldersProcessedCnt </h2><br>";
            echo "<h2>files processed $filesProcessedCnt </h2><br>";
        }
        $debugYN = 'N';
        if ($debugYN === 'Y'){
            echo "<h3>files</h3>";
            Barray::echo_array($filesProcessed);
            echo "<h3>folders</h3>";
            Barray::echo_array($foldersProcessed);            
        }
        
        $this->insertManyRowsCodebaseTable($filesProcessed);
    }
    
    
    /**
     * Insert item in a file array into the code base table
     * @param string $fileUri
     * @return int
     * @internal dev status = Golden!
     * http://localhost:8207/index.php/metadataCodebase/findCodeFilesOnDisk?XDEBUG_SESSION_START=netbeans-xdebug
     * http://localhost:8212/metadata-database/findCodeFilesOnDisk?XDEBUG_SESSION_START=netbeans-xdebug
     */
    public function insertOneRowCodebaseTable($fileUri)
    {
        //$basePath = Yii::app()->basePath;
        $basePath = Yii::getAlias('@app');
        //$fileName = Bfile::base_name($fileUri);
        $fileName = $this->base_name2($fileUri);
        $fileUri2 = str_ireplace($basePath, '', $fileUri);  // base path removed
        $fileUri3 = str_ireplace($fileName, '', $fileUri2); // file name removed
        //$fileUri4 = Bfile::remove_trailing_slash($fileUri3);
        $fileUri4 = $this->remove_trailing_slash($fileUri3);
        // @todo: xlat column names
        if (empty($fileName) || empty($fileUri4)){
            throw new CException("empty values");
        }
        //Barray::echo_array(['file'=>$fileName, 'path'=>$fileUri4]);
        //$this->echo_array(['file'=>$fileName, 'path'=>$fileUri4]);
        $condition='file_name=:c1 AND file_path_uri=:c2';
        $params    = [':c1'=>$fileName, ':c2'=>$fileUri4];
        //$model = MetadataCodebase::find()->where($condition, $params)->asArray(false)->all();
        $model = MetadataCodebase::find()->where($condition, $params)->asArray(false)->one();
        /*
        $criteria = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_name=:c1 AND file_path_uri=:c2';
        $criteria->params    = [':c1'=>$fileName, ':c2'=>$fileUri4];
        $model = MetadataCodebase::model()->find($criteria);   // will return null if not found
        */

        if (is_array($model)){
            if (! empty($model)){
                if (array_key_exists(0, $model)){
                    if (is_object($model[0])){
                        //var_dump($model);
                        return 0;
                    }
                }
            }
//            if (is_object($model[0])){
//                //var_dump($model);
//                return 0;
//            } elseif (is_null($model[0])){
//                var_dump($model);
//            }
        }

        if (empty($model)){
            $model2 = new MetadataCodebase();
            $model2->attributes = ['file_name'=>$fileName, 'file_path_uri'=>$fileUri4];
            //Barray::echo_array($model2->attributes);
            //return;
            $validate = true;

            if($model2->save($validate)) {
                //echo "model saved <br>";
                return 1;
            } else {
                return 0;
            }             
        } else {
            //echo "model was found! <br>";
            return 0;
        }
    }    
    
    /**
     * Insert items in a file array into the code base table
     * @param array $files  [0=>'file1',1=>'file2']
     * @return int
     * @calledBy actionFindCodeFilesOnDisk()
     * @internal dev status = Golden!
     * @todo handle relative uri paths
     */
    public function insertManyRowsCodebaseTable($files)
    {
        /* The format output by $this->globfiles()
         37 => 
          array (size=11)
          'path' => string '/var/www/sites/orgs/gitlab/stp--basic-site--schema-v3/yii2-app-basic/views' (length=74)
          'basename' => string 'team' (length=4)
          'filename' => string 'team' (length=4)
          'size' => int 4096
          'updated_dt' => string '2018-08-14 18:40:11' (length=19)
          'created_dt' => string '2018-08-30 2:07:47' (length=18)
          'accessed_dt' => string '2018-09-12 19:44:43' (length=19)
          'sha256' => string 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855' (length=64)
          'file_uri' => string '/var/www/sites/orgs/gitlab/stp--basic-site--schema-v3/yii2-app-basic/views/team/' (length=80)
          'file_uri_rel' => string '/views/team' (length=12)
          'short_path_rel' => string '/views' (length=8)
         */
        if (! is_array($files)){
            throw new UserException("Only arrays are allowable as a parameter");    
        }

        //$counter = 0;
        $insertResult=[];
        $arrayKeys = array_keys($files);
        if (array_search('models', $arrayKeys) !== false){
            foreach ($files as $key=>$fileGlobs){
                if($key==='models'||$key==='controllers'||$key==='models/base'){
                    foreach($fileGlobs as $idx=>$glob){
                        if (array_key_exists('file_uri', $glob)){
                            $fileUri = $glob['file_uri'];
                            $insertResult[] = ['file_uri'=>$this->insertOneRowCodebaseTable($fileUri)];
                        }
                    }
                } elseif($key === 'views'){
                    foreach($fileGlobs as $idx=>$glob){
                        if (array_key_exists('file_uri', $glob)){
                            $fileUri = $glob['file_uri'];
                            if (is_file($fileUri) !== false){
                                $insertResult[] = ['file_uri'=>
                                    $this->insertOneRowCodebaseTable($fileUri)];
                            } 
                        }
                    }
                } else {
                    // unknown use case
                    \var_dump($fileGlobs);
                }  
            }
        }
    }
    
    /**
     * Insert items in a file array into the code base table
     * @param array $files  [0=>'file1',1=>'file2']
     * @return array $result[key1[key2]['file_uri'=>'','insert'=>0|1]
     * @calledBy actionFindCodeFilesOnDisk()
     * @calledBy actionPopulate()
     * @internal dev status = Golden!
     * 
     */
    protected function insertGlobItems($files)
    {
        /* The format output by $this->globfiles()
         37 => 
          array (size=11)
          'path' => string '/var/www/sites/orgs/gitlab/stp--basic-site--schema-v3/yii2-app-basic/views' (length=74)
          'basename' => string 'team' (length=4)
          'filename' => string 'team' (length=4)
          'size' => int 4096
          'updated_dt' => string '2018-08-14 18:40:11' (length=19)
          'created_dt' => string '2018-08-30 2:07:47' (length=18)
          'accessed_dt' => string '2018-09-12 19:44:43' (length=19)
          'sha256' => string 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855' (length=64)
          'file_uri' => string '/var/www/sites/orgs/gitlab/stp--basic-site--schema-v3/yii2-app-basic/views/team/' (length=80)
          'file_uri_rel' => string '/views/team' (length=12)
          'short_path_rel' => string '/views' (length=8)
         */
        if (! is_array($files)){
            throw new UserException("Only arrays are allowable as a parameter");    
        }

        //$counter = 0;
        $insertResult=[];
        $fileUriKey = 'file_uri';
        $fileUri = '';

        foreach ($files as $key1=>$fileGlobs){
            foreach ($fileGlobs as $key2=>$glob){
                if (array_key_exists($fileUriKey, $glob) !== false){
                    $fileUri = $glob[$fileUriKey];
                    if (is_file($fileUri) !== false){
                        $insertResult[$key1][$key2] = [
                            'file_uri'=>$fileUri,
                            'insert'  =>$this->insertOneRowCodebaseTable($fileUri)
                            ];
                    }                    
                }
            }

        }
        return $insertResult;
    }    
        
    /**
     * Return string keyed array ['file_uri'=>'somefile']
     * @param type $mixed
     * @return type
     * @calledBy $this->actionPopulate(), $this->actionFindCodeFilesOnDisk()
     */
    protected function parseFileUriFromAFileList($mixed) {
        $uriOut=[];
        if (is_array($mixed)){
            foreach($mixed as $metadata){
                if (isset($metadata['file_uri'])){
                    $uriOut[] = $metadata['file_uri'];
                }
            }
        }
        return $uriOut;
    }
        
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="package database driven bulk find-replace v0.0.0.3">
    // package name: database driven bulk find-replace v0.0.0.3
    // package purpose: To Mass manipulate the code by rule-sets stored in the database

    /**
     * 
     * @param type $id
     */
    public function actionFindPackagesById($id){
        $packages = $this->fetchAllPackageNamesInCodeFileById($id);
        dd($packages);
    }
    
    /**
     * Find rows with an unknown status, or a known status
     * @param $cecID 1 = no issues, 2 = has errors
     * @internal dev status = Under Construction
     * @calls $this->manageCecStatus()
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesInDatabase?cecID=1
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesInDatabase?cecID=1&readOnlyYn=Y
     * @internal http://localhost:8212/metadata-dashboard/findCodeFilesInDatabase?cecID=1&readOnlyYn=Y
     * @package bulk find-replace v1
     */
    public function actionFindCodeFilesInDatabase($cecID=2,$readOnlyYn='Y')
    {
        //$readOnlyYn='Y';
        if (!empty($cecID)){
            $rows = $this->fetchRowsToProcessByCec($cecID);
        } else {
            $rows = $this->fetchRowsToProcess(); // returns an array of CActiveRecords with cec=2
        }
        if (is_array($rows)){
            foreach ($rows as $idx => $model) 
            {
                if ($idx >= 100) {
                    break;
                }
                echo "<h1>$model->file_path_uri -- $model->file_name</h1>";
                //Barray::echo_array($model->attributes);
                Yii::$app->barray->echo_array2($model->attributes);
                $result = $this->manageCecStatus($model,$readOnlyYn);
                //Barray::echo_array($result, 'Y');
                // remove the before and after content to minimize screen output
                echo "<h3>process output</h3>";
                unset($result['original']);
                unset($result['revised']);
                //Barray::echo_array($result, 'Y');
                Yii::$app->barray->echo_array2($result, 'Y');
                echo "<h3>****************process item complete********************</h3>";
            }

        }

        if (empty($rows)){
            echo "nothing found <br>";
        }
    }
    
    /**
     * Find rows with an unknown status, or a known status
     * @param $cecID 1 = no issues, 2 = has errors
     * @param array
     * @internal dev status = Ready for Testing
     * @calls cecPackageInstall::package()
     * @calls MetadataCodebase::model()->findAllBySql()
     * @calls $this->manageCecStatusUsingRules()
     * @calls fetchRowsToProcessByCec($cecID)
     * @calls fetchRowsToProcess()
     * @calls calls $this->manageCecStatusUsingRules(($model,$rules,$readOnlyYn))
     * @prefork calls $this->manageCecStatus()
     * 
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesUsingRules?cecID=1
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesUsingRules?cecID=6&readOnlyYn=Y
     * @package CEC - tracking v0.0.0.1
     * @prefork package bulk find-replace v1
     * @internal forked from actionFindCodeFilesInDatabase v.0.0.0.1
     */
    public function actionFindCodeFilesUsingRules($cecID=2, $readOnlyYn='Y')
    {
        //$readOnlyYn='Y';
        // @todo find a way to add selection criterial like sql in a cec definition
        $cec = MetadataCodebaseCec::model()->findByPk($cecID);
        if ( isset($cec->cec_scope) && isset($cec->rule_file_uri) ){
            $cecScope = $cec->cec_scope;
            $settingsFile = $cec->rule_file_uri;

            $basePath = Yii::app()->basePath;
            //$file = '/var/www/sites/gsm/dev/mvp-yii-1.1.19/mvp-yii-1.1.19/protected/views/_cec/cec-y1v-0001-v.0.0.0.1-install.php';
            //$settingsFile = '/views/_cec/cec-y1v-0001-v.0.0.0.1-install.php';
            $settingsFilePath = $basePath . $settingsFile;
            require_once($settingsFilePath);
            // <editor-fold defaultstate="collapsed" desc="array layout">
            /*
                [
                'codebase_cec_id'=>6, // used for status log entries
                'scope'=>[
                    'query'=>[
                        'sql'=>"select * from metadata__codebase "
                             . "where file_name = '_form.php';"]],             
                  'find'=>['string' => $find,'regex' => '',],
                  'replace'=>['string' => $replace,'regex' => '',
                ]
             * 
             */
            // </editor-fold>            
            $settings = cecPackageInstall::package();
            if (isset($settings['scope']['query']['sql'])){
                $sql = $settings['scope']['query']['sql'];
            }
            $rows = MetadataCodebase::model()->findAllBySql($sql, $params=[]);
            if (! empty($rows) && is_array($rows)){
               if (true){
                   echo "found " . count($rows) . " rows to process <br>";
               }
                
            }
            if (isset($settings['find']['regex']) && isset($settings['replace']['regex'])){
                $rules=[
                    [    
                    'find'    => $settings['find']['regex'],
                    'replace' => $settings['replace']['regex'],
                    ]
                ];  
                if (isset($settings['find']['regexmods'])){
                    $rules[0]['regexmods'] = $settings['find']['regexmods'];
                }
                if (isset($settings['find']['use regex'])){
                    $useRegex = $settings['find']['use regex'];
                    $rules[0]['use regex'] =  $useRegex;
                }
            } elseif (isset($settings['find']['string']) && isset($settings['replace']['string'])){
                $rules=[
                    
                    'find'    => $settings['find']['string'],
                    'replace' => $settings['replace']['string'],
                    
                ];                
                $useRegex = false;
            }
           
            
        } else {
            // version 0.0.0.1 data grab
            if (!empty($cecID)){
                $rows = $this->fetchRowsToProcessByCec($cecID);
            } else {
                $rows = $this->fetchRowsToProcess(); // returns an array of CActiveRecords with cec=2
            }
        }
        if (is_array($rows)){
            foreach ($rows as $idx => $model) 
            {
//                if ($idx >= 1) {
//                    break;
//                }
                echo "<h1>$model->file_path_uri -- $model->file_name</h1>";
                //Barray::echo_array($model->attributes,'Y');
                Yii::$app->barray->echo_array2($model->attributes,'Y');
                $result = $this->manageCecStatusUsingRules($model,$rules,$settings, $readOnlyYn);
                //Barray::echo_array($result, 'Y');
                // remove the before and after content to minimize screen output
                echo "<h3>process output</h3>";
                unset($result['original']);
                unset($result['revised']);
                //Barray::echo_array($result, 'Y');
                Yii::$app->barray->echo_array2($result, 'Y');
                echo "<h3>****************process item complete********************</h3>";
            }

        }

        if (empty($rows)){
            echo "nothing found <br>";
        }
    }    
    
    /**
     * Run and SQL fetch of the revised column list
     * @calledBy replaceSchema_fieldNames_v1_names_to_v3_names()
     * @calledby $this->translateSchemaColumnNames()
     * @category source code database schema xlation
     * @internal dev status = Ready for Testing
     * @package bulk find-replace v1
     */
    protected function fetchSchemaVersionColumnXlation() {
        // Return [0=>['find'=>"abc",'replace'=>"123"],1=>['find'=>'', 'replace'=>'']
        // start with a small hard coded batch
        $xlat = [
            0 => ['find'=>'created_at', 'replace'=>'created_at'],
            1 => ['find'=>'updated_at', 'replace'=>'updated_at']
        ];
        return $xlat;
    }
       
    /**
     * Gets the most recent status record
     * @param $cecID 1 = no issues, 2 = has errors
     * @return CActiveRecord
     * @package bulk find-replace v1
     */
    protected function fetchRowsToProcessByCec($cecID)
    {
        // -- select on the most recent record and having a particular status
        $sql = "SELECT mc.*
            FROM metadata__codebase mc
            left join 
            (
                        select m1.codebase_id, max(m1.cec_status_id) as max_cec_status_id 
                                from metadata__codebase_cec_status m1
                                group by m1.codebase_id 
                ) d1 on mc.codebase_id = d1.codebase_id
                left join 
                                metadata__codebase_cec_status mccs
                                on d1.max_cec_status_id = mccs.cec_status_id 
                where mccs.codebase_cec_id = :p1
            order by mc.codebase_id, mccs.created_at desc; ";
        $params = [':p1'=>$cecID];
        
//        $criteria->select    = 'codebase_id, file_name, file_path_uri';
//        $criteria->condition = 'file_name=:c1 AND file_path_uri=:c2';
//        $criteria->params    = [':p1'=>$cecID];           
        $rows = MetadataCodebase::model()->findAllBySql($sql, $params); 
        return $rows;
    }    
     
    /**
     * Gets the most recent status record
     * @return CActiveRecord
     * @package bulk find-replace v1
     */
    protected function fetchRowsToProcess()
    {
        // -- select on the most recent record and having a particular status
        $sql = "SELECT mc.*
            FROM metadata__codebase mc
            left join 
            (
                        select m1.codebase_id, max(m1.cec_status_id) as max_cec_status_id 
                                from metadata__codebase_cec_status m1
                                group by m1.codebase_id 
                ) d1 on mc.codebase_id = d1.codebase_id
                left join 
                                metadata__codebase_cec_status mccs
                                on d1.max_cec_status_id = mccs.cec_status_id 
                where mccs.codebase_cec_id = 2
            order by mc.codebase_id, mccs.created_at desc; ";
        $params = [];
        $rows = MetadataCodebase::model()->findAllBySql($sql, $params); 
        return $rows;
    }

    /**
     * Run a business rule on a single file and write status to database
     * @param MetadataCodebase::model() $codebaseModel
     * @param array $rules A find-replace array [['find'=>'','replace'=>'']]
     * @param readOnlyYn default = Y
     * @prefork calledBy $this->actionFindCodeFilesInDatabase()
     * @preFork calls $this->xlatOneFile()
     * @todo proposed callers list: []
     * 
     * @calledBy actionFindCodeFilesUsingRules($cecId, $readOnlyYn)
     * @calls xlatOneFileUsingRules($content, $rules)
     * 
     * @prefork package bulk find-replace v1
     * @internal forked from manageCecStatus v0.0.0.1
     * @todo change the middle manager function xlatOneFile to a non-hardcoded function or one that takes params for the ruleset
     * @package CEC Tracking v0.0.0.1
     * @see manageCecStatus() that was used bulk-edit-replace v0.0.0.1
     * @internal Scope - Manager of One File at a time, must be called by a multi-item manager method
     * @version 0.0.0.2 status id matches the specific rule being processed
     * @internal version history: 0.0.0.1 contained hard coded CEC ids
     */
    protected function manageCecStatusUsingRules($codebaseModel, $rules, $settings, $readOnlyYn='Y') {
        // build a physical path to access the file
        if (is_array($rules)){
            echo "Received a rules array in " . __METHOD__ . "<br>";
            dd($rules);
            // @todo the rules array is missing the rule metadata
        }
        if (is_array($settings)){
            echo "Received a settings array in " . __METHOD__ . "<br>";
            dd($settings);
            // @todo the rules array is missing the rule metadata
        }        
        $model = $codebaseModel;
        //$basePath = Yii::app()->basePath;
        $basePath = Yii::getAlias('@app');
        $filePath = $basePath . $model->file_path_uri . '/' . $model->file_name;
        if (is_file($filePath)){
            $result = $this->xlatOneFileByRules($filePath, $rules, $readOnlyYn);
            if (is_array($result)) {
                //Barray::echo_array($result);
                if (is_array($settings)){
                    if (isset($settings['codebase_cec_id'])){
                        $codebaseCecId =  $settings['codebase_cec_id'];
                    } else {
                        $codebaseCecId = 0;
                        throw new CException("The settings array must contain a CEC Id");
                    }
                }
                $status = ['codebase_id'=>$model->codebase_id];
                if ($result['sha256 changed'] === 'yes'){
                    // set status = 2 has errors
                    // @todo how do I know there has been an error? the sha has changed
                    // @todo how do I know the correct id and bias of pass fail to write to the db?
                    // @todo the test bias must come from the rules array!
                    if ($codebaseCecId > 0){
                        $status['codebase_cec_id'] = $codebaseCecId; // prefork = 2
                    } else {
                        $status['codebase_cec_id'] = 2; // prefork = 2
                    }
                    $status['cec_status'] = 'has errors';
                } else if ($result['sha256 changed'] === 'no'){
                    // set status = 1 all valid
                   if ($codebaseCecId > 0){
                        $status['codebase_cec_id'] = $codebaseCecId; // prefork = 1
                    } else {
                        $status['codebase_cec_id'] = 1; // prefork = 1
                    }                    
                    $status['cec_status'] = 'no issues found';
                }
                if ($result['file modified'] === 'yes'){
                    //set status = 1 all valid
                    $status['codebase_cec_id'] = 1;
                    $status['cec_status'] = 'file was modified';
                }
                $mStat = new MetadataCodebaseCecStatus;
                $mStat->attributes = $status;
                Barray::echo_array($status,'Y');                
                $validate = true;
                if ($mStat->save($validate)) {
                    echo "file evaluation <b>status written to database</b> <br>";
                } else {
                    Barray::echo_array($mStat->errors);
                    echo "file evaluation status <b>was not written</b> to database <br>";
                }
                return $result;
            }
        }
    }
    
    
   /**
     * Writes revised source code to the hard disk
     * @param type $fileUriList
     * @param array $rules Search and replace parameter array [['find'='','replace'=''],...]
     * @preFork calls $this->replaceSchema_fieldNames_v1_names_to_v3_names
     * @calls ???
     * @todo Determine who|what to call
     * @preForm calledBy $this->manageCecStatus($codebaseModel, $readOnlyYn='Y')
     * @calledBy $this->manageCecStatusUsingRules()
     * @internal dev status = Ready for Testing
     * @package bulk find-replace v1
     */
    protected function xlatOneFileByRules($fileUri, $rules, $readOnlyYn='Y') {
        //$readOnlyYn = "Y";
        $ruleSetResultLog = [];
        //@todo if view file then call view cec v0001?

        if (is_file($fileUri)){
            $fileContentsOriginal = file_get_contents($fileUri);
            $sha256_file_bfr = hash_file('sha256', $fileUri);
            $ruleSetResult = $this->replaceStringsUsingRules($fileContentsOriginal,$rules);
            $ruleSetResultLog[] = $ruleSetResult;
            if ($readOnlyYn === 'N'){
                // write to the source file with the replacesments
                echo "running in disk write-enabled mode <br>";
                if (isset($ruleSetResult['replacement cnt'])){
                    $replacementCnt = $ruleSetResult['replacement cnt'];
                    if (isset($ruleSetResult['revised'])){
                        $revisedText = $ruleSetResult['revised'];
                    } else {
                        echo "failed write logic test #1 <br>";
                    }
                    if ($replacementCnt > 0 && strlen($revisedText) > 0){
                        file_put_contents($fileUri, $revisedText);
                        $replacementCnt = 0;
                        $revisedText = '';
                    } else {
                        echo "failed write logic test #2 <br>";
                        echo "\$replacementCnt=*$replacementCnt* <br>";
                        echo "strlen(\$revisedText=*". strlen($revisedText) . "* <br>";
                    }
                } else {
                    echo "failed replacement cnt logic test #1 <br>";
                }
            } else {
                echo "running in read-only mode <br>";
            }
            $sha256_bfr = hash('sha256', $ruleSetResult['original']);
            $sha256_aft = hash('sha256', $ruleSetResult['revised']);
            
            $sha256_file_aft = hash_file('sha256', $fileUri);
            
            if ($sha256_bfr !== $sha256_aft){
                $ruleSetResult['sha256 original'] = $sha256_bfr;
                $ruleSetResult['sha256 revised']  = $sha256_aft;
                $ruleSetResult['sha256 changed']  = 'yes';
            } else {
                $ruleSetResult['sha256 changed']  = 'no';
            }
            if ($sha256_file_bfr !== $sha256_file_aft){
                $ruleSetResult['file modified']   = 'yes';            
            } else {
                $ruleSetResult['file modified'] = 'no';            
            }
            return $ruleSetResult;
        } else {
            throw new CException ("A file URI must be passed in");
        }
    }
    
    /**
     * Run a business rule on a single file and write status to database
     * @param MetadataCodebase::model() $codebaseModel
     * @param readOnlyYn default = Y
     * @calledBy $this->actionFindCodeFilesInDatabase()
     * @calls $this->xlatOneFile()
     * @package bulk find-replace v1
     * @version 0.0.0.1
     * @internal dev status = Golden!
     */
    protected function manageCecStatus($codebaseModel, $readOnlyYn='Y') {
        // build a physical path to access the file
        $model = $codebaseModel;
        $basePath = Yii::app()->basePath;
        $filePath = $basePath . $model->file_path_uri . '/' . $model->file_name;
        if (is_file($filePath)){
            $result = $this->xlatOneFile($filePath, $readOnlyYn);
            if (is_array($result)) {
                //Barray::echo_array($result);
                $status = ['codebase_id'=>$model->codebase_id];
                if ($result['sha256 changed'] === 'yes'){
                    // set status = 2 has errors
                    $status['codebase_cec_id'] = 2;
                    $status['cec_status'] = 'has errors';
                } else if ($result['sha256 changed'] === 'no'){
                    // set status = 1 all valid
                    $status['codebase_cec_id'] = 1;
                    $status['cec_status'] = 'no issues found';
                }
                if ($result['file modified'] === 'yes'){
                    //set status = 1 all valid
                    $status['codebase_cec_id'] = 1;
                    $status['cec_status'] = 'file was modified';
                }
                $mStat = new MetadataCodebaseCecStatus;
                $mStat->attributes = $status;
                Barray::echo_array($status);                
                $validate = true;
                if ($mStat->save($validate)) {
                    echo "file evaluation <b>status written to database</b> <br>";
                } else {
                    Barray::echo_array($mStat->errors);
                    echo "file evaluation status <b>was not written</b> to database <br>";
                }
                return $result;
            }
        }
    }
    
   /**
     * Writes revised source code to the hard disk
     * @param type $fileUriList
     * @calls $this->replaceSchema_fieldNames_v1_names_to_v3_names
     * @calledBy $this->manageCecStatus($codebaseModel, $readOnlyYn='Y')
     * @internal dev status = Ready for Testing
     * @package bulk find-replace v1
     * @version 0.0.0.1
     */
    protected function xlatOneFile($fileUri, $readOnlyYn='Y') {
        //$readOnlyYn = "Y";
        $ruleSetResultLog = [];

        if (is_file($fileUri)){
            $fileContentsOriginal = file_get_contents($fileUri);
            $sha256_file_bfr = hash_file('sha256', $fileUri);
            $ruleSetResult = $this->replaceStrings($fileContentsOriginal);
            $ruleSetResultLog[] = $ruleSetResult;
            if ($readOnlyYn === 'N'){
                // write to the source file with the replacesments
                echo "running in disk write-enabled mode <br>";
                if (isset($ruleSetResult['replacement cnt'])){
                    $replacementCnt = $ruleSetResult['replacement cnt'];
                    if (isset($ruleSetResult['revised'])){
                        $revisedText = $ruleSetResult['revised'];
                    } else {
                        echo "failed write logic test #1 <br>";
                    }
                    if ($replacementCnt > 0 && strlen($revisedText) > 0){
                        file_put_contents($fileUri, $revisedText);
                        $replacementCnt = 0;
                        $revisedText = '';
                    } else {
                        echo "failed write logic test #2 <br>";
                        echo "\$replacementCnt=*$replacementCnt* <br>";
                        echo "strlen(\$revisedText=*". strlen($revisedText) . "* <br>";
                    }
                } else {
                    echo "failed replacement cnt logic test #1 <br>";
                }
            } else {
                echo "running in read-only mode <br>";
            }
            $sha256_bfr = hash('sha256', $ruleSetResult['original']);
            $sha256_aft = hash('sha256', $ruleSetResult['revised']);
            
            $sha256_file_aft = hash_file('sha256', $fileUri);
            
            if ($sha256_bfr !== $sha256_aft){
                $ruleSetResult['sha256 original'] = $sha256_bfr;
                $ruleSetResult['sha256 revised']  = $sha256_aft;
                $ruleSetResult['sha256 changed']  = 'yes';
            } else {
                $ruleSetResult['sha256 changed']  = 'no';
            }
            if ($sha256_file_bfr !== $sha256_file_aft){
                $ruleSetResult['file modified']   = 'yes';            
            } else {
                $ruleSetResult['file modified'] = 'no';            
            }
            return $ruleSetResult;
        } else {
            throw new CException ("A file URI must be passed in");
        }
    }

    /**
     * read-only search-replace test using a ruleset array passed in
     * @param string $fileContent
     * @param array $rules A find replace array [[find='',replace='']]
     * @PreFork calledBy $this->xlatOneFile($fileUri, $readOnlyYn='Y')
     * 
     * @calls $this->fetchSchemaVersionColumnXlation()
     * @category source code database-schema xlation
     * @return array [original, revised, rules processed cnt, search results]
     * @internal dev status = Ready for Testing
     * @internal forked from replaceStrings() which is used by "bulk find-replace v1"
     * 
     * @internal this is a second version of the replaceStrings concept, but the 
     * @internal first version using a ruleset passed in
     * @package CEC-tracking v0.0.0.1
     * @version 0.0.0.1
     */
    protected function replaceStringsUsingRules($fileContent,$rules) {
        $debug = true;
        if (!is_array($rules)){
            throw new CException("the \$rules param must be passed as an array");
        }
        if ($debug){
            echo "<h3> rules follow</h3>";
            dd([$rules]);            
        }
        // Next function returns: [0=>['find'=>"abc", 'replace'=>"123"], 1=>[]],
        $workCopy   = $fileContent;
        $ruleProcessedCnt = 0;
        $replacementTotalCnt = 0;
        $results = [];
        foreach($rules as $idx => $row){
            if ($debug){
                echo "<h3> rule row follows - from inside the loop of replaceStringUsingRules</h3>";
                dd([$row]);            
            }            
            if (isset($row['find'])  && isset($row['replace']) ){
                $find    = $row['find']; 
                $replace = $row['replace'];
            } else {
                $find    = '';
                $replace = '';
            }
            if (strlen($find) > 0 && strlen($replace) > 0){
                $replaceCnt = null;
                if (isset($row['use regex']) && $row['use regex'] === true){
                    echo "<h3> taking the regex route</h3>";
                    if (isset($row['regexmods'])){
                        $regexMods = $row['regexmods'];
                    } else {
                        $regexMods = '';
                    }

                    $stringCleaned = preg_replace($find.$regexMods, $replace, $workCopy, 1, $replaceCnt);
                } else {
                    echo "<h3> taking the string route</h3>";
                    $stringCleaned = str_replace($find, $replace, $workCopy, $replaceCnt);
                }
                $workCopyLog[] = $stringCleaned;
                $replacementTotalCnt += (int)$replaceCnt;
                $workCopy = $stringCleaned;
                $ruleProcessedCnt++;
                
                $results[] = ['find'=>$find, 'replace'=>$replace, 'count'=>$replaceCnt];
            }
        }

        $output = [
            'original'           => $fileContent,
            'revised'            => $workCopy,
            'rules processed cnt'=> $ruleProcessedCnt,
            'replacement cnt'    => $replacementTotalCnt,
            'search results'     => $results
        ];
        
        if ($debug === true){
            echo "<pre>" . __METHOD__ . "</pre><br>";
            echo "<h1>items processed $ruleProcessedCnt </h1><br>";
            Barray::echo_array($workCopyLog,'Y');
            // @todo test how echo_array compares to dd, easy no html escaping. But is that assertion true?
            //dd($workCopyLog);
            echo "<h2>output array </h2><br>";
            Barray::echo_array($output,'Y');
        }        
        return $output;
    }
          
    /**
     * read-only find schema v1 field names
     * @param string $fileContent
     * @calledBy $this->xlatOneFile($fileUri, $readOnlyYn='Y')
     * @calls $this->fetchSchemaVersionColumnXlation()
     * @category source code database-schema xlation
     * @return array [original, revised, rules processed cnt, search results]
     * @internal dev status = Ready for Testing
     * @package bulk find-replace v1
     * @version 0.0.0.1
     */
    protected function replaceStrings($fileContent) {
        
        // Next function returns: [0=>['find'=>"abc", 'replace'=>"123"], 1=>[]],
        $phase = 'one';
        if ($phase === 'one'){
            $schemaXlat = $this->fetchSchemaVersionColumnXlation(); // handles [find=created_dt, replace=created_at]
        }
        if ($phase === 'two'){
            $schemaXlat = $this->fetchSchemav2tov3Mods($direction); // handles [find=org_name, replace=org]
        }
        $workCopy   = $fileContent;
        $ruleProcessedCnt = 0;
        $replacementTotalCnt = 0;
        $results = [];
        foreach($schemaXlat as $idx => $row){
            
            if (isset($row['find'])  && isset($row['replace']) ){
                $find    = $row['find']; 
                $replace = $row['replace'];
            } else {
                $find    = '';
                $replace = '';
            }
            if (strlen($find) > 0 && strlen($replace) > 0){
                $replaceCnt = null;
                $stringCleaned = str_replace($find, $replace, $workCopy, $replaceCnt);
                $workCopyLog[] = $stringCleaned;
                $replacementTotalCnt += (int)$replaceCnt;
                $workCopy = $stringCleaned;
                $ruleProcessedCnt++;
                $results[] = ['find'=>$find, 'replace'=>$replace, 'count'=>$replaceCnt];
            }
        }

        $output = [
            'original'           => $fileContent,
            'revised'            => $workCopy,
            'rules processed cnt'=> $ruleProcessedCnt,
            'replacement cnt'    => $replacementTotalCnt,
            'search results'     => $results
        ];
        $debug = false;
        if ($debug === true){
            echo "<pre>" . __METHOD__ . "</pre><br>";
            echo "<h1>items processed $ruleProcessedCnt </h1><br>";
            Barray::echo_array($workCopyLog);
            echo "<h2>output array </h2><br>";
            Barray::echo_array($output);
        }        
        return $output;
    }
      
    /**
     * @param $direction v3-to-v2, v2-to-v3
     * @package database driven bulk find-replace v0.0.0.3
     */
    protected function fetchSchemav2tov3Mods($direction='v2-to-v3') {
        $sql = "select distinct TABLE_NAME, concat(TABLE_NAME, '_name') as gsm_v2_type_name
	, concat(TABLE_NAME, '') as gsm_v3_type_name
	from information_schema.COLUMNS
	where TABLE_SCHEMA = 'gsm_dev_v2' and COLUMN_NAME = concat(TABLE_NAME, '_name');";
        
        $columns = Yii::app()->db->createCommand($sql)->queryAll($fetchAssociative=true);
        
        if (!empty($direction) && $direction === 'v2-to-v3'){
            $find    = 'gsm_v2_type_name';
            $replace = 'gsm_v3_type_name';
        } elseif (!empty($direction) && $direction === 'v3-to-v2') {
            $find    = 'gsm_v3_type_name';
            $replace = 'gsm_v2_type_name';
        } else {
            throw new CException("unknown parameter value");
        }
        
        $editFind = [];
        if (!empty($columns) && is_array($columns)){
            foreach ($columns as $row){
                $editFind[] = ['find'=>$row[$find], 'replace'=>$row[$replace]];
            }
            // inject manually added tables & columns
            if ($direction === 'v2-to-v3'){
                // Next two tables are missed by the select above
                $editFind[] = ['find'=>'short_name', 'replace'=>'country'];
                $editFind[] = ['find'=>'gsm_sport_name', 'replace'=>'sport'];
            }
        }
        return $editFind;
    }
    

    // </editor-fold>    
    
    /**
     * Add any untracked models, views, and controllers into the code base metadata table
     * @param array $scope
     * @calledBy cecValidateAllRules
     * @internal this is a necessary step to insure that all-code is accounted for. 
     * @internal all-inclusive is crucial aspect of the cec tracking project
     * @internal dev status = Under Construction
     * @version 0.0.1.0 - completed in sprint #1 for this package
     * @internal Call Stack
     * @package CEC Tracking v0.0.0.1
     */
    protected function cecAddNewFilesToCodebaseTracking($scope=['models','views','controllers']) {
        // compare glob_files to file list from database
        // then array_diff( the two arrays )
        // insert the deltas result into the codebase table
        //$scope=['models'];
        $debug   = true;
        $verbose = true;
        
        if (is_array($scope)){
            
            //$basePath = Yii::app()->basePath;
            $basePath = Yii::getAlias('@app');
            $orphanUri=[];
            foreach ($scope as $codebaseCategory)
            {
                $sourceFolder = $basePath .'/'. $codebaseCategory ;
                
                if ($codebaseCategory === 'models'){
                    //$files = Bfile::globFiles($sourceFolder);
                    $files = Yii::$app->bfile->globFiles2($sourceFolder);
                    //$filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    $filesRelativeUri  = $this->convertGlobToRelativeUri($files);
                    $modelsRelativeUri = $this->fetchMvcModelsAllRelativeUri();
                    $orphansM          = $this->findOrphansInUriArrays($filesRelativeUri, $modelsRelativeUri);
                    $orphanUri[$codebaseCategory] = $orphansM;
                    $this->insertManyRowsCodebaseTable($orphansM);

                } elseif ($codebaseCategory === 'views'){
                    $viewFiles         = $this->globMvcViews($sourceFolder);
                    $filesRelativeUri  = $this->convertGlobToRelativeUri($viewFiles);
                    $viewsRelativeUri  = $this->fetchMvcViewsAllRelativeUri();
                    $orphansV          = $this->findOrphansInUriArrays($filesRelativeUri, $viewsRelativeUri);
                    $orphanUri[$codebaseCategory] = $orphansV;
                    $this->insertManyRowsCodebaseTable($orphansV);

                } elseif ($codebaseCategory === 'controllers'){
                    
                    $filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    $controllersRelUri = $this->fetchMvcControllersAllRelativeUri();
                    $orphansC          = $this->findOrphansInUriArrays($filesRelativeUri, $controllersRelUri);
                    $orphanUri[$codebaseCategory] = $orphansC;
                    $this->insertManyRowsCodebaseTable($orphansC);

                }
            }
            return $orphanUri;
            
        } else {
            throw new CException("the parameter passed in must be an array");
        }        
    }
        
    /**
     * Returns an array of CActiveRecords by specifying file types to retrieve: models, views, controllers
     * @param array $scope default = ['models','views','controllers']
     * @throws CException
     * @calledBy cecValidateAllRules()
     * @internal dev status = Under Construction
     * @package CEC Tracking v0.0.0.1
     * @version 0.0.1
     */
    protected function cecFindCodebaseItemsByScopeCategory($scope=['models','views','controllers']) {
        if (is_array($scope)){

            $itemsToProcess = [];
            foreach ($scope as $codebaseCategory){
                
                if ($codebaseCategory === 'models'){
                    $itemsToProcess[$codebaseCategory] = $this->fetchMvcModel();
                } elseif ($codebaseCategory === 'views'){
                    $itemsToProcess[$codebaseCategory] = $this->fetchMvcViews();
                } elseif ($codebaseCategory === 'controllers'){
                    $itemsToProcess[$codebaseCategory] = $this->fetchMvcControllers();
                }
            }
            return $itemsToProcess;
        } else {
            throw new CException("the parameter passed in must be an array");
        }
    }
    
    /**
     * Returns an array of CActiveRecords by specifying file types to retrieve: models, views, controllers
     * Used to Read PORTED code into the database. Needs work. 
     * @param array $scope default = ['models','views','controllers']
     * @throws CException
     * @calledBy cecValidateAllRules()
     * @internal dev status = Under Construction
     * @package CEC Tracking v0.0.0.1
     * @version 0.0.2
     * 
     */
    protected function cecFindCodebaseItemsPortsByScopeCategory($scope=['models','views','controllers']) {
        return 0;
        if (is_array($scope)){

            $itemsToProcess = [];
            foreach ($scope as $codebaseCategory){
                
                if ($codebaseCategory === 'models'){
                    $itemsToProcess[$codebaseCategory] = $this->fetchMvcModel();
                } elseif ($codebaseCategory === 'views'){
                    $itemsToProcess[$codebaseCategory] = $this->fetchMvcViews();
                } elseif ($codebaseCategory === 'controllers'){
                    $itemsToProcess[$codebaseCategory] = $this->fetchMvcControllers();
                }
            }
            
        } else {
            throw new CException("the parameter passed in must be an array");
        }
    }
    
    /**
     *
     * 
     * @param array $rule ['rule name'=>'', 'cec tag'='', mixed $needles, 'regex_valid'=>'', 'regex_invalid'=>'']
     * @calledBy $this->cecValidateAllRules($rules)
     * @internal dev status = Ready for Testing
     * @package cec testing v1.0.0
     */ 
    protected function cecValidateOneRule($rule) {
        // Use string "found" approach?
        // Use string "not found" approach?
        // Use regex "found" approach?
        // Use regex "not found" approach?
        
        // select test approach or read test_approach from the rule definition
        if (!is_array($rule)){
            throw new CException("must pass an array");
        }
        // process string based tests
        if (isset($rule['cec_eval_stack']) ){
            $evalCallStack = $rule['cec_eval_stack'];
        }
        // process string based tests
        if (isset($rule['needle_posit']) ) {
            $needlePosit = $rule['needle_posit'];
            if (!empty($needlePosit)){
                $resultNeedlePosit = $this->cecFind($needlePosit); 
            } else {
                $resultNeedlePosit = 'skipped test';
            }
        }
        if (isset($rule['needle_negate']) ){
            $needleNegate = $rule['needle_negate'];
            if (! empty($needleNegate)){
                $resultNeedleNegate = $this->cecFind($needleNegate);
            } else {
                $resultNeedleNegate = 'skipped test';
            }            
        }
        // process regex based tests
        if (isset($rule['regex_posit']) ) {
            $regexPosit = $rule['regex_posit'];
            if (! empty($regexPosit)){
                $resultRegexPosit = $this->cecMatch($regexPosit);
            } else {
                $resultRegexPosit = 'skipped test';
            }            
            
        }
        if (isset($rule['regex_negate']) ){
            $regexNegate = $rule['regex_negate'];
            if (! empty($regexNegate)){
                $resultRegexNegate = $this->cecMatch($regexNegate);
            } else {
                $resultRegexNegate = 'skipped test';
            }            
            
        }        
        
        $results =
            [
                'needle posit test'  => $resultNeedlePosit,
                'needle negate test' => $resultNeedleNegate,
                
                'regex posit test'   => $resultRegexPosit,
                'regex negate test'  => $resultRegexNegate,                
            ];
        return $results;
    }

    
    /**
     *
     * 
     * @param array $rules [0=>['rule name'=>'', 'cec tag'='', mixed $needles, 'regex_valid'=>'', 'regex_invalid'=>''], ]
     * @calledBy TBD
     * @calls $this-cecValidateOneRule()
     * @internal dev status = Ready for Testing
     * @package CEC Testing v1.0.0
     * @internal Call Stack
     *    cecFindCodebaseItemsByScopeCategory
     *    cecCodeFileHasChangedSinceLastCecCheck
     *    cecValidateOneRule()
     *      cecFind()
     *      cecMatch()
     * @todo get this working with models only
     * @internal pass in rule-categories to process eg model, view, controller?
     * @package CEC Tracking v0.0.0.1
     */ 
    public function cecValidateAllRules($rules, $scope=['models','views','controllers']) {
        $scopeTest = ['models'];
        $itemsToProcess = $this->cecFindCodebaseItemsByScopeCategory($scopeTest);
        //$itemsToProcess = $this->cecFindCodebaseItemsByScopeCategory($scope);
        
        // Process all items included by their scope category models, views, controllers
        foreach ($itemsToProcess as $testTarget){
            
           
            if (is_array($testTarget)){
                if (isset($testTarget['file_uri'])){
                    $targetFileUri = $testTarget['file_uri'];
                } else {
                    continue;
                }
            }
            // apply a codefile hasnt changed filter === 'yes'
            $apply_codefile_hasnt_changed_filter = 'yes';
            if ($apply_codefile_hasnt_changed_filter === 'yes'){
                // @todo items to process file uri
                $codefileHasChanged = $this->cecCodeFileHasChangedSinceLastCecCheck($targetFileUri);
                continue;
            }
            if (is_array($rules)){
                $results = [];
                // Run each rule for each code file processed
                foreach ($rules as $rule){
                    
                    $result = $this->cecValidateOneRule($rule);
                    $results[] = $result;
                }
                return $results;
            } else {
                throw new CException("the parameter passed in must be an array");
            }            
            
        }
        /*
        if (is_array($rules)){
            $results = [];
            foreach ($rules as $rule){
                $result = $this->cecValidateOneRule($rule);
                $results[] = $result;
            }
            return $results;
        } else {
            throw new CException("the parameter passed in must be an array");
        }
          
         * i'm saving this temporarilly to be used in case the loop above doesn't work as expected
         */      
    }
    
        
    /**
     * Write CEC status results to log table with sha256
     * @param @force default false - ignores file hasn't changed flag
     * @package CEC tracking v0.0.0.1
     * @since CEC tracking v0.0.0.1
     * @version 0.0.0.1
     * @internal Runs in a chatty manner updating the database as each code file is processed
     * @todo pseudo code this
     */
    protected function cecUpdateLog($targetFileUri, $force=false){
        
        $hash_enabled_yn = 'Y';
        $sha256 = '';
        // file has been modified
        $fileModifiedSinceLastUpdate = false;
        
        
        
        Bfile::globFiles($sha256);
        if ($hash_enabled_yn === 'Y'){
           $hash256 = hash_file('sha256', $targetFileUri);
        } else {
            $hash256 = 'null';
        }
        
        // fetch rules
        // loop through rules
    }
 
    /**
     * Returns an empty|blank cec rule template
     * @internal start with string needles then evolve towards regex and response type validations
     * @package cec testing v1
     */
    protected function cecRuleTemplate(){
        $cecRuleDefault = [
            'rule name'     =>'', 'cec tag'=>'',
            'needle_posit'  =>'', 'needle_negate'=>'', // string based rule
            //'regex_posit'   =>'', 'regex_negate' =>'', // regex based rule
            //'response_posit'=>'', 'response_negate' => '',
            'cec_eval_stack'=>''];
        return $cecRuleDefault;
    }
    
    
    /**
     * Execute a regular expression search based on a regex passed in
     * @param $regex      regular expression as an immutable string within single quotes
     * @param $regexMods  regex modifiers eg global parameters
     * @internal dev status = Under Construction
     * @calledBy cecValidateOneRule
     * @example $this->cecMatch('CEC y1c 0.0.0.1');
     * @package cec testing v1.00
     */
    protected function cecMatch($regex, $regexMods){
        // find matches
        $matches = [];
        $matchLog = [$matches, $match_cnt];
        return $matchLog;
    }
    
    /**
     * Execute a search based on a string parameter passed in
     * @param string $needle
     * @param string $replace
     * @param string $haystack
     * @internal dev status = Under Construction
     * @calledBy cecValidateOneRule
     * @example $this->cecFind('CEC y1c 0.0.0.1');
     * @package cec testing v1.0.0
     * 
     */
    protected function cecFind($needle, $replace='', $haystack) {
        // find matches
        $count=null;
        str_replace($needle, $replace, $haystack, $count);
        return $count;
        
    }  

    /**
     * Return count of string found in string passed in
     * @param string $needle
     * @param string $haystack
     * @param string $replace =''
     * @internal dev status = ready for testing
     * @calledBy 
     * @example $this->str_findCount('CEC y1c 0.0.0.1');
     * @package cec testing v1.0.0
     * 
     */
    protected function str_findCount($needle, $haystack, $replace='') {
        // find matches
        $count=null;
        str_replace($needle, $replace, $haystack, $count);
        return $count;
    }    
    
    /**
     * Execute a search based on a string parameter passed in
     * @param string $needle
     * @param string $replace
     * @param string $haystack
     * @return array ['count'=>1, 'before'=>'abcdef', 'after'=>'abc'];
     * @internal dev status = Under Construction
     * @calledBy cecValidateOneRule
     * @example $this->cecReplaceByString('package CEC y1c 0.0.0.1', 'package CEC y1c 0.0.2.1', $);
     * @package CEC Tracking v0.0.0.1
     */
    protected function cecReplaceByString($needle, $replace='', $haystack) {
        // find matches
        $count=null;
        $work = $haystack;
        str_replace($needle, $replace, $work, $count);
        return ['count'=>$count, 'before'=>$haystack, 'after'=>$work];
    }    
    
    
    // ************************************************************************
    // start code port from mvp3

    /**
     * Execute a search based on a string parameter passed in. Return find count.
     * @param string $needle
     * @param string $replace
     * @param string $haystack
     * @return null | int Null if nothing found. If found returns count
     * @internal dev status = Under Construction
     * @calledBy cecValidateOneRule
     * @example $this->cecReplaceByRegex('CEC y1c 0.0.0.1');
     * @package CEC Tracking v0.0.0.1
     */
    protected function cecReplaceByRegex($needle, $replace='', $haystack) {
        // find matches
        $findCount=null;
        str_replace($needle, $replace, $haystack, $findCount);
        return $findCount;
        
    }     

    /**
     * Get a list of a call controller code files from the database.
     * @internal dev status = Ready for Testing
     */
    protected function fetchMvcControllersAll() {
        $fileUriStub         = '/controllers';
        $verbose = false;
        /*
        $criteria            = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_path_uri=:c1';
        $criteria->params    = [':c1'=>$fileUriStub];
        $models = MetadataCodebase::model()->findAll($criteria);   // will return null if not found
         * 
         */
        //$condition->select    = 'codebase_id, file_name, file_path_uri';
        $condition = 'file_path_uri=:c1';
        $params    = [':c1'=>$fileUriStub];
        
        $models = MetadataCodebase::find()->where($condition, $params)->all();
        if (is_array($models)){
            if (isset($models[0]) && is_object($models[0])){
                $recordsFound = true;
                $count = count($models);
                if ($verbose){
                    echo "fetchMvcControllersAll() found <b>$count</b> controllers <br>";
                }
                return $models;
            } else {
                $recordsFound = true;
            }
        }
        if (is_object($models)){
            
        } else {
            throw new CException ("not an object!");
        }
        return [];
    }        
    
    /**
     * Get a list of a call controller code files from the database.
     * @internal dev status = Ready for Testing
     * @deprecated since version 0.6.0
     */    
    protected function fetchMvcControllersAll_yii1x() {
        $fileUriStub         = '/controllers';
        $verbose = false;
        $criteria            = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_path_uri=:c1';
        $criteria->params    = [':c1'=>$fileUriStub];
        $models = MetadataCodebase::model()->findAll($criteria);   // will return null if not found
        if (is_array($models)){
            if (isset($models[0]) && is_object($models[0])){
                $recordsFound = true;
                $count = count($models);
                if ($verbose){
                    echo "fetchMvcControllersAll() found <b>$count</b> controllers <br>";
                }
                return $models;
            } else {
                $recordsFound = true;
            }
        }
        if (is_object($models)){
            
        } else {
            throw new CException ("not an object!");
        }
        return [];
    }    
    // </editor-fold>
    /** @package-functionality-block-end **************************************/
    
    
    /**
     * @todo receive an array from fetch controllers all
     * @calls fetchControllerActions
     */
    public function actionListControllers() {
        $count = (int) $this->fetchControllersAll();
        echo "<b>$count</b> controllers found <br>";
        echo "<b>$count</b> controller actions found <br>";
        
        foreach ($count as $controller){
            $actions = $this->fetchControllerActions($controller);
            echo "<b>$controller</b> actions found <br>";
            
            if (is_array($actions)){
                Barray::echo_array($actions);
            } else {
                echo "<b>$controller</b> No actions found! <br>";
            }
        }
    }
    
    /**
     * @calledBy $this->actionLoadAllModels()
     * @internal dev status = Ready for Testing
     */
    protected function testModel($model) {
        // if the test method exists then execute it.
        $method_name = 'testModel';
        if (method_exists($model, $method_name)){
            $result = $model->testModel();
            if (is_array($result)){
                echo "testing model <br>";
                Barray::echo_array($result);
            }
        }
        
    }

    /**
     * @calledBy $this->actionLoadAllModels()
     * @internal 
     * @
     */
    protected function testController($controller) {
        // if the test method exists then execute it.
        $method_name = 'testController';
        if (method_exists($controller, $method_name)){
            $result = $controller->testModel();
            if (is_array($result)){
                echo "testing controller <br>";
                Barray::echo_array($result);
            }
        }
        
    }    
    
    /**
     * @calledBy $this->actionLoadAllModels()
     * @internal dev status = Ready for Testing
     */
    protected function testView($model, $view) {
        // if the test method exists then execute it.
        $method_name = 'testView';
        
        if (method_exists($model, $method_name)){
            $result = $model->testModel();
            if (is_array($result)){
                echo "testing view <br>";
                Barray::echo_array($result);
            }
        }
        
    }  
    
    /**
     * 
     * @param CActiveRecord $codebaseModel
     * @calledBy fetchAllPackageNamesInCodeFileByModel
     */
    protected function cecDbToUri($codebaseModel) {
        $model = $codebaseModel;
        if (! is_object($model)){
            throw new CException("A MetadataCodebase model must be passed in");
        }
        if (!isset($model->file_name) || empty($model->file_name)){
            throw new CException("The codebase file_name is not present");
        }
        if (!isset($model->file_path_uri) || empty($model->file_path_uri)){
            throw new CException("The codebase file_path_uri is not present");
        }       
        $basePath = Yii::app()->basePath;
        $fileUri = $basePath . '/'. $model->file_path_uri .'/'.$model->file_name;
        
        if (is_file($fileUri)){
            return $fileUri;
        } else {
            throw new CException("file was not found!");
        }
        
    }
    
    /**
     * 
     * @param CActiveRecord $codebaseModel
     * @calls cecDbToUri($codebaseModel)
     * @calls fetchMethodsFromCodeFileByFileUri($fileUri)
     * @calls 
     */
    protected function fetchAllPackageNamesInCodeFileByModel($codebaseModel) {
        $fileUri = $this->cecDbToUri($codebaseModel);
        if (isset($codebaseModel->file_name)){
            $fileName = $codebaseModel->file_name;
        } else {
            $fileName = 'unknown file name';
        }
        $content = file_get_contents($fileUri);
        if (empty($content)){
            throw new CException("content is empty! eg the code file is empty");
        }
        echo "<br><b>" . strlen($content) . "</b> bytes of code file content found in <b>$fileName</b><br>";
        $pattern = '/@package (.+?)\n/';
        $matches=null;
        preg_match_all($pattern, $content, $matches);
        $output = [];
        //echo "<br>All matches follow <br>";
        //dd($matches);
        foreach ($matches as $outerIdx => $match){
            echo "Outer loop \$OuterIdx = <b>$outerIdx</b> <br>";
            $fullStringMatches=[];
            if ($outerIdx === 0){
                // A full string match, the outer match that we don't want
                $fullStringMatches[] = $match;
            }elseif ($outerIdx === 1){
                echo "<b>". count($match, COUNT_RECURSIVE) ."</b> elements in cec item match <br>"; 
                $dupesRemoved = [];
                foreach ($match as $matchItem){
                    //echo "\$matchItem = $matchItem <br>";
                    //dd($matchItem);
                    
                    
                    //echo "<b>". count($match, COUNT_RECURSIVE) ."</b> elements in match <br>"; ;
                    //dd($match);
                    if (array_search($matchItem, $output, $strict=false) !== false){
                        //echo "found the item (match $matchItem) in position $pos $output[$pos] <br>";
                        $dupesRemoved[] = $matchItem;
                        continue;
                    } else {

                        $output[] = $matchItem;
                    }
                }
            }
            
            //dd($match);
        }
        $temp = $output;
        arsort($temp);
        $output2 = $temp;
        $output3 = [];
        foreach($temp as $t1){
            $output3[] = $t1;
        }
        dd($output2);
        dd($output);
        echo "<br>";
        //dd($output);
        echo "<br><br>Methods in <b>$fileName</b> follow <br>";
        $methods = $this->fetchMethodsFromCodeFileByFileUri($fileUri);
        dd($methods);
        return $output3;
    }
    
    /**
     * http://localhost:8207/index.php/metadataCodebase/findPackagesById?id=413
     * http://localhost:8212/metadata-dashboard/findPackagesById?id=413
     * @param int|string $codebaseModelPk
     * @calls $this->fetchAllPackageNamesInCodeFileByModel($model)
     */
    protected function fetchAllPackageNamesInCodeFileById($codebaseModelPk) {
        $model = MetadataCodebase::model()->findByPk($codebaseModelPk);
        return $this->fetchAllPackageNamesInCodeFileByModel($model);

    }
    

    // </editor-fold>

    
    /**
     * Lists all MetadataCodebase models.
     * @return mixed
     * @example http://localhost:8212/metadata-dashboard/index-of-tables?XDEBUG_SESSION_START=netbeans-xdebug
     */
    public function actionIndexOfTables()
    {
        $this->getDatabaseSchema3Stats();
        return;
        //$searchModel = new MetadataCodebaseSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$this->layout='column1-menu.php'; // this works
        //$dataProvider = $this->getDatabaseSchema3Stats([]);
        /*
        return $this->render('indexOfTables', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
         * 
         */
    }
    
    /**
     * Populate metadata__database with rows. Sets flag fields.
     * @return mixed
     * @author Dana Byrd
     * @internal dev status = Golden!
     */    
    protected function getDatabaseSchema3Stats() {
        //-- select table that DOES NOT meet the rule of having a primary key
        
        /*
        $sqlview_name_pattern = 'vw%';
        $sqldatabase_name = 'sport__dev_design_003';
        $params=['has_named_primary_key','has_all_v3_fields',];
        $params=[':table_schema'=>'sport__dev_design_003'];
         * 
         */
        // The select below pulls three datasets and merges them together
        $sql = "
            select distinct
            c.table_name
            ,case a.table_name when ifnull(a.table_name, false) then 'Y' else 'N' end as has_named_primary_key
            ,case b.table_name when ifnull(b.table_name, false) then 'Y' else 'N' end as has_v3_fields
            ,case d.table_name when ifnull(d.table_name, false) then 'Y' else 'N' end as has_representing_field
            from information_schema.columns c
            left outer join(
                SELECT 
                table_name
                FROM information_schema.columns
                where table_schema = 'sport__dev_design_003'
                and table_name not like 'vw%'                
                and column_name = concat(table_name,'_id') 
                    and data_type = 'int' 
                    and column_type = 'int(11)' 
                    and column_key = 'PRI' 
                    and extra = 'auto_increment'
                    and ordinal_position = 1
            ) as a
                on c.table_name = a.table_name
            left outer join(
                SELECT table_name
                FROM information_schema.columns
                where 
                table_schema = 'sport__dev_design_003'
                and column_name in ('lock','created_by','created_at','updated_by','updated_at')
                and table_name not like 'vw%'
                group by table_name
                having count(*) = 5
            ) b on c.table_name = b.table_name
            left outer join(

                SELECT table_name
                FROM information_schema.columns
                where 
                table_schema = 'sport__dev_design_003'
                and column_name = table_name
                and table_name not like 'vw%'
                group by table_name

            ) d on c.table_name = d.table_name            
            where 
                c.table_schema = 'sport__dev_design_003'
                and c.table_name not like 'vw%'
            ;";
        
        $connection = Yii::$app->getDb();
        $cmd = $connection->createCommand($sql);
        $rows = $cmd->queryAll();
        
        // populate metadata__database
        $schemaName = 'sport__dev_design_003';
        foreach ($rows as $row){
            $tableName = isset($row['table_name'])             ? $row['table_name'] : '';
            $namedKey  = isset($row['has_named_primary_key'])  ? $row['has_named_primary_key'] : '';
            $v3_fields = isset($row['has_v3_fields'])          ? $row['has_v3_fields'] : '';
            $repField  = isset($row['has_representing_field']) ? $row['has_representing_field'] : '';
            if (empty($tableName) || empty($schemaName)){
                continue;
            }
            $model = \app\models\MetadataDatabase::find()
                ->where(['table_name'=>$tableName, 'schema_name'=>$schemaName])
                //->indexBy('database_id')
                ->one();
            if (is_object($model)){
                //echo 'Row<pre><code>' . \yii\helpers\VarDumper::dumpAsString($row) . '</pre></code>';
                //echo 'Model<pre><code>' . \yii\helpers\VarDumper::dumpAsString($model) . '</pre></code>';
                $model->has_named_primary_key  = $namedKey;
                $model->has_representing_field = $repField;
                $model->has_v3_fields          = $v3_fields;
                $saved = $model->save($runValidation=true);
                if (! $saved){
                    //echo 'Model Not Saved!<pre><code>' . \yii\helpers\VarDumper::dumpAsString($model) . '</pre></code>';
                    echo 'Model Not Saved!<pre><code>' . \yii\helpers\VarDumper::dumpAsString($model->errors) . '</pre></code>';
                }
            } else {
                $model = new \app\models\MetadataDatabase();
                $model->table_name = $tableName;
                $model->schema_name= $schemaName;
                $model->has_named_primary_key  = $namedKey;
                $model->has_representing_field = $repField;
                $model->has_v3_fields          = $v3_fields;                
                if ($model->save($runValidation=true)){
                    
                }
            }
        }
         
        
        //echo '<pre><code>' . \yii\helpers\VarDumper::dumpAsString($result) . '</pre></code>';
        //return $models;

    }        
    

    // <editor-fold defaultstate="collapsed" desc="depreciated code">
    
    /**
     * Add any untracked models, views, and controllers into the code base metadata table
     * @param array $scope
     * @calledBy cecValidateAllRules
     * @internal this is a necessary step to insure that all-code is accounted for. 
     * @internal all-inclusive is crucial aspect of the cec tracking project
     * @internal dev status = depreciated 
     * @version 0.0.0.2
     * @deprecated since version 0.0.0.2
     */
    protected function cecAddNewFilesToCodebaseTracking_v0002($scope=['models','views','controllers']) {
        // compare glob_files to file list from database
        // then array_diff( the two arrays )
        // insert the deltas result into the codebase table
        //$scope=['models'];
        $debug   = true;
        $verbose = true;
        
        if (is_array($scope)){
            
            $basePath = Yii::app()->basePath;
            $orphanUri=[];
            foreach ($scope as $codebaseCategory)
            {
                $sourceFolder = $basePath .'/'. $codebaseCategory ;
                
                if ($codebaseCategory === 'models'){

                    $filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    $modelsRelativeUri = $this->fetchMvcModelsAllRelativeUri();
                    $orphansM          = $this->findOrphansInUriArrays($filesRelativeUri, $modelsRelativeUri);
                    $orphanUri[$codebaseCategory] = $orphansM;
                    //$this->insertManyRowsCodebaseTable($orphansM);

                } elseif ($codebaseCategory === 'views'){
                    $viewFiles=[];
                    $viewRootDir = Bfile::globFiles($sourceFolder);
                    foreach ($viewRootDir as $idx => $globbedItem){
                        $uri='';
                        if (isset($globbedItem['file_uri']))
                        {
                            $uri = $globbedItem['file_uri'];
                            if (strlen($uri)>1)
                            {
                                $lastChar = substr($uri, strlen($uri)-1, $length=1);
                                //echo "last char = $lastChar <br>";
                            
                                if ($lastChar === '/')
                                {
                                  
                                    if (is_dir($uri)){
                                        $viewDir = Bfile::globFiles($uri);
                                        if ( is_array($viewDir) )
                                        {
                                            //echo Barray::echo_array($viewDir);
                                            foreach ($viewDir as $fileGlob) {
                                                $viewFiles[]=$fileGlob;
                                            }
                                        //$viewFiles[]=$viewDir;
                                        }
                                    } 
                                } elseif(is_file($uri)) {
                                    $viewFiles[]=$globbedItem;
                                }
                            }
                            
                        }
                        //echo "Item " . Barray::echo_array($globbedItem) . "<br>";
//                        if (is_dir($item)){
//                            
//                        }
                        //if ( (int)$idx > 5 ){break;}
                    }
                    //$filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    // create a single dimensional array
                    
                    echo "<b>Found " . count($viewFiles) . " files</b> on disk <br>";
                    //echo Barray::echo_array($viewFiles);
                    $filesRelativeUri  = $this->convertGlobToRelativeUri($viewFiles);
                    $viewsRelativeUri  = $this->fetchMvcViewsAllRelativeUri();
                    $orphansV          = $this->findOrphansInUriArrays($filesRelativeUri, $viewsRelativeUri);
                    $orphanUri[$codebaseCategory] = $orphansV;
                    //$this->insertManyRowsCodebaseTable($orphansV);

                } elseif ($codebaseCategory === 'controllers'){
                    
                    $filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    $controllersRelUri = $this->fetchMvcControllersAllRelativeUri();
                    $orphansC          = $this->findOrphansInUriArrays($filesRelativeUri, $controllersRelUri);
                    $orphanUri[$codebaseCategory] = $orphansC;
                    //$this->insertManyRowsCodebaseTable($orphansC);

                }
            }
            return $orphanUri;
            
        } else {
            throw new CException("the parameter passed in must be an array");
        }        
    }
      
    /**
     * Get a list of a call controller code files from the database.
     * @internal this doesn't work for views. A like statement is needed.
     * @deprecated since version 0.0.0.2
     */
    protected function fetchMvcViewsAllv1() {
        $fileUriStub = '/views';
        $criteria = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_path_uri=:c1';
        $criteria->params    = [':c1'=>$fileUriStub];
        $models = MetadataCodebase::model()->findAll($criteria);   // will return null if not found
        if (is_array($models)){
            if (isset($models[0]) && is_object($models[0])){
                $recordsFound = true;
                $count = count($models);
                echo "found <b>$count</b> models <br>";
                return $models;
            } else {
                $recordsFound = true;
            }
        } else {
            echo "no records found! <br>";
        }
        
        if (empty($models )){
            echo "no records found! <br>";
            die;
            return;
            
        }
    }

// </editor-fold>
        
    
    // <editor-fold defaultstate="collapsed" desc="dead wood code">
    
    /**
     * 
     * @param type $mixedParam
     * @return int
     */
    protected function parseFileDirectoryFromArray($mixedParam) {
        echo "parsing file directory <br>";
        if (! isset($mixedParam)){
            echo "empty variable passed in <br>";
            return 0;
        }
        if (is_array($mixedParam)){
            echo "its an array <br>";
        } elseif (is_string($mixedParam)) {
            return $mixedParam;
        }
        
    }
    
    
    /**
     * Writes revised source code to the hard disk
     * @param type $fileUriList
     * @calls $this->replaceSchema_fieldNames_v1_names_to_v3_names
     * @internal dev status = Ready for Testing
     */
    protected function translateSchemaColumnNames($fileUriList, $readOnlyYn='Y') {
        //$readOnlyYn = "Y";
        $ruleSetResultLog = [];
        foreach($fileUriList as $fileUri){
            if (is_file($fileUri)){
                $fileContentsOriginal = file_get_contents($fileUri);
                $ruleSetResult = $this->replaceStrings($fileContentsOriginal);
                $ruleSetResultLog[] = $ruleSetResult;
                if ($readOnlyYn === 'N'){
                    // write to the source file with the replacesments
                    if (isset($ruleSetResult['replacement cnt'])){
                        $replacementCnt = $ruleSetResult['replacement cnt'];
                        if (isset($ruleSetResult['revised'])){
                            $revisedText = $ruleSetResult['revised'];
                        }
                        if ($replacementCnt > 0 && strlen($revisedText > 0)){
                            file_put_contents($fileUri, $revisedText);
                            $replacementCnt=0;
                            $revisedText='';
                        }
                    }
                }
            }
        }
    }
    
    /**
     * @internal WARNING: Does NOT filter ROWS from a MultiRow table - will return false positives
     * @deprecated since version 0.53
     */
    protected function fetchRowsToProcess_hasCecStatusOf2() {

        $sql = "SELECT mc.* 
                FROM metadata__codebase mc
                        left outer join metadata__codebase_cec_status mccs
                                on mc.codebase_id = mccs.codebase_id 
                where mccs.codebase_cec_id = 2;";        
        $params = [];
        $rows = MetadataCodebase::model()->findAllBySql($sql, $params);          
        return $rows;
    }
      
    /**
     * 
     * @return CActiveRecord
     */
    protected function fetchRowsToProcess_hasNoStatusRows() {
        // find rows that don't have a status
        $sql = "SELECT mc.* 
                FROM metadata__codebase mc
                        left outer join metadata__codebase_cec_status mccs
                                on mc.codebase_id = mccs.codebase_id 
                where mccs.codebase_id is null;";
        $params = [];
        $rows = MetadataCodebase::model()->findAllBySql($sql, $params); 
        return $rows;        
    }
    
    /**
     * Return string keyed array ['file_uri'=>'somefile']
     * @param type $fileListArray
     * @return type
     * @calledBy $this->actionPopulate(), $this->actionPopulateStrict()
     */
    public function parseFilenameAndUriFromAFileList($fileListArray) {
        $uriOut=[];
        if (is_array($fileListArray)){
            foreach($fileListArray as $metadata){
                if (isset($metadata['file'])  && isset($metadata['file_uri'])){
                    $fileName = $metadata['file'];
                    $uriOut[]=['file_uri'=>$metadata['file_uri'] ];
                }                
//                if (isset($metadata['file_uri'])){
//                    $fileUri = $metadata['file_uri'];
//                }
                
            }
        }
        return $uriOut;
    }    
    
    /**
     * Get a list of all model code files from the database.
     * @internal dev status = Golden!
     */
    protected function fetchMvcModelsAll() {
        $fileUriStub = '/models';
        $verbose = false;
        $criteria = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_path_uri=:c1';
        $criteria->params    = [':c1'=>$fileUriStub];
        $models = MetadataCodebase::model()->findAll($criteria);   // will return null if not found
        if (is_array($models)){
            if (isset($models[0]) && is_object($models[0])){
                $recordsFound = true;
                $count = count($models);
                if ($verbose){
                    echo "fetchMvcModelsAll() found <b>$count</b> models <br>";
                }
                return $models;
            } else {
                $recordsFound = true;
            }
        } else {
            echo "no records found! <br>";
        }
        
        if (empty($models )){
            echo "no records found! <br>";
            die;
            return;
            
        }
    }
        
    /**
     * Get an array of files that includes meta-data about the files including the sha256 for deduping.
     * @param string $sourceFolder     The location of your files
     * @param string $ext              File extension you want to limit to (i.e.: *.txt)
     * @param int    $epocSecs         If you only want files that are at least so old.
     * @param int    $maxFileCnt       Number of files you want to return
     * @param bool   $ignoreSubFolders Ignore sub folders
     * @return array[]
     * @internal return structure ['basename','ext','filename','size','updated_dt','created_dt','accessed_dt','sha256','file_uri']
     * @author   alan@synergymx.com, debugged and rewritten by Dana Byrd <danabyrd@byrdbrain.com>
     * @internal source = http://php.net/manual/en/control-structures.break.php
     * @internal Development Status = Golden!
     */
    protected function globFiles($sourceFolder, $ext=null, $epocSecs=null, $maxFileCnt=null, $ignoreSubFolders=false){
        // dana: php glob() appears to prefer, ie only works with a physical file path on some websites.
        //   so I've added a reference to SERVER doc root and also added the automatic
        //   appendage of a trailing slash where needed as glob also seems to require the trailing
        //   slash, but this is unconfirmed as yet.
        // Sample output
        //  [path] => c:\temp\my_videos\
        //  [name] => fluffy_bunnies.flv
        //  [size] => 21160480
        //  [date] => 2007-10-30 16:48:05

        if( !is_dir( $sourceFolder ) ) {
            $msg =  "Invalid directory! (*$sourceFolder*)\n\n";
            return $msg;
        }
        
        // add a trailing slash
        if (! stripos($sourceFolder, '*') !== false){
                $globPath    = $this->add_trailing_slash($sourceFolder)."*"; // append '/*' to the path
        } else {
                // a wild card file skeleton is already included in the path. Use the path as is.
                $globPath    = $sourceFolder;
        }
        $basePath     = Yii::getAlias('@app');
        $filesList    = glob($globPath, GLOB_MARK); // glob_mark adds trailing slash to the folders returned
        $set_limit    = 0;
	$out	      = [];

        $hash_enabled_yn = 'Y';
        $targetFileUri = "";
        foreach($filesList as $key => $fileUri) {

            if( !is_null($maxFileCnt) && $set_limit == $maxFileCnt ){
                // maximum file count has been reached
                break;
            }
            if( $ignoreSubFolders && substr($fileUri, -1) == '/'){
                // a trailing slash indicates a sub folder
                continue;
            }

            if(is_null($epocSecs) || filemtime( $fileUri ) > $epocSecs ){
                $targetFileUri = $fileUri;
                if ($hash_enabled_yn == "Y"){
                    $hash256 = hash_file('sha256', $targetFileUri);
                }
                $path_parts = pathinfo( $fileUri );
                if ( !empty($ext) && $path_parts['extension'] !== $ext){
                    continue;
                }

                $out[$key]['path']          = $path_parts['dirname'];
                $out[$key]['basename']      = $path_parts['basename'];  // includes extension
                if (isset($path_parts['extension'])){
                    $out[$key]['ext']           = $path_parts['extension'];
                }
                $out[$key]['filename']      = $path_parts['filename'];  // sans extension
                $out[$key]['size']          = filesize( $fileUri );
                $out[$key]['updated_dt']    = date('Y-m-d G:i:s', filemtime( $fileUri ) );
                $out[$key]['created_dt']    = date('Y-m-d G:i:s', filectime( $fileUri ) );
                $out[$key]['accessed_dt']   = date('Y-m-d G:i:s', fileatime( $fileUri ) );
                $out[$key]['sha256']        = $hash256;
                $out[$key]['file_uri']      = $fileUri;
                $out[$key]['file_uri_rel']  = $this->remove_trailing_slash(
                    str_ireplace($basePath, '', $fileUri));
                
                //$relativePath = str_ireplace($basePath, '', $fileUri);
                $fileBasename = $path_parts['basename'];
                $out[$key]['short_path_rel']  = $this->remove_trailing_slash(
                    str_ireplace($fileBasename, '', $out[$key]['file_uri_rel']));
                
                if (is_dir($fileUri) !== false){
                    $out[$key]['is_dir_yn'] = 'Y';
                } else {
                    $out[$key]['is_dir_yn'] = 'N';
                }
              
                $set_limit++;
            }
        }
        if(!empty($out)){
            return $out;
        } else {
            $msg =  "No files found! (*$sourceFolder*)\n\n";
        }
    }
    
    /**
     * Add a trailing slash to a path if a slash isn't already present
     * @param string $str
     * @return string
     */
    protected function add_trailing_slash($str){
        $str .= (substr($str, -1) == '/' ? '' : '/');
        return $str;
    }

    /**
     * remove a trailing backslash from a file path
     * @param string $str
     * @return string
     */
    protected function remove_trailing_slash($str){
        if (substr($str, -1) == '/'){
            $str = substr($str, 0, -1);
        }
        return $str;
    }    

    /**
     *
     * @param array $array
     * @param string $encode_html_entities_yn
     * @see $this->dumpAsString()
     * @internal Development Status = Golden!
     */
    protected function echo_array(array $array, $encode_html_entities_yn=NULL){
        // Display easy-to-read valid PHP array code
        if ( empty($encode_html_entities_yn) ){
            $encode_html_entities_yn = "N";
        } else {
            // The field has a value so validate the parameter value passed in.
            if ( $encode_html_entities_yn !== "Y" && $encode_html_entities_yn !== "N"){
                $msg = "The correct values for \$encode_html_entities_yn is Y or N. Please try again.";
                error_Handler(__METHOD__, __LINE__, "<b>$msg</b><br />");
            }
        }

        if ( $encode_html_entities_yn == "Y") {
            echo '<pre>' . htmlentities(var_export( $array, true )) . '</pre>';
        } else {
            echo '<pre>' . var_export( $array, true ) . '</pre>';
        }
    }

    /**
     * returns file name and extension from a URI
     * @param string $file_uri
     * @return string
     */
    protected function base_name2($file_uri){
        // return myfile.php from /somefolder/myfile.php
        return pathinfo($file_uri, PATHINFO_BASENAME);
    }     
    // </editor-fold>
}
