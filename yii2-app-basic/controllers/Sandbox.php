<?php

namespace app\controllers;

use Yii;
//use app\models\Camp;
//use app\models\CampSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CampController implements the CRUD actions for Camp model.
 */
class CampController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Runs a sandbox action.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGiiTest()
    {
        $model = new Camp();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->camp_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for CampSport
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddCampSport()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('CampSport');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formCampSport', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*
     * 
        CREATE TABLE `sport__dev_design_003`.`org_address` (
          `org_id` INT NOT NULL,
          `address_id` INT NOT NULL,
          PRIMARY KEY (`org_id`, `address_id`));
     */
    
    
    
    /**
     * Codeblock from: vendor/mootensai/yii2-enhanced-gii/BaseGenerator.php
     * Checks if the given table is a junction table.
     * For simplicity, this method only deals with the case where the pivot contains two PK columns,
     * each referencing a column in a different table.
     * @param TableSchema the table being checked
     * @param TableSchema $table
     * @return array|boolean the relevant foreign key constraint information if the table is a junction table,
     * or false if the table is not a junction table.
     */
    protected function checkPivotTable($table)
    {
        $pk = $table->primaryKey;
        if (count($pk) !== 2) {
            return false;
        }
        $fks = [];
        foreach ($table->foreignKeys as $refs) {
            if (count($refs) === 2) {
                if (isset($refs[$pk[0]])) {
                    $fks[$pk[0]] = [$refs[0], $refs[$pk[0]]];
                } elseif (isset($refs[$pk[1]])) {
                    $fks[$pk[1]] = [$refs[0], $refs[$pk[1]]];
                }
            }
        }
        if (count($fks) === 2 && $fks[$pk[0]][0] !== $fks[$pk[1]][0]) {
            return $fks;
        } else {
            return false;
        }
    }

    /**
     * Codeblock from: vendor/mootensai/yii2-enhanced-gii/BaseGenerator.php
     * Checks if the given table is a junction table.
     * For simplicity, this method only deals with the case where the pivot contains two PK columns,
     * each referencing a column in a different table.
     * @param TableSchema the table being checked
     * @param TableSchema $table
     * @return array|boolean the relevant foreign key constraint information if the table is a junction table,
     * or false if the table is not a junction table.
     * @internal dbyrd converted the settings array to work with a forked version of extendedGii
     */
    protected function extendedGiiSettings($setting = 'preview'){

        // <editor-fold defaultstate="collapsed" desc="preview settings">
        $previewSettings = [

            '_csrf' => 'xzdyVFUrmPPL-F9COAMC4a2SXL7h10cl7_PWF6lhSB_xbwYyA1Phu4aLbCBMcE2bw9EX15KcdX2LnJxQ5VcPcg==',
            'Generator' => [
                'db' => 'db',
                'tableName' => 'note',
                'nsModel' => 'app\\models',
                'modelClass' => '',
                'skippedRelations' => '',
                'nameAttribute' => 'name, title, username',
                'hiddenColumns' => 'id, lock',
                'skippedColumns' => 'created_at, updated_at, created_by, updated_by, deleted_at, deleted_by, created, modified, deleted',
                'enableI18N' => '0',
                'messageCategory' => 'app',
                'nsController' => 'app\\controllers',
                'controllerClass' => '',
                'baseControllerClass' => 'yii\\web\\Controller',
                'pluralize' => '1',
                'loggedUserOnly' => '0',
                'expandable' => '1',
                'pdf' => '0',
                'cancelable' => '1',
                'saveAsNew' => '1',
                'useTablePrefix' => '0',
                'generateSearchModel' => '1',
                'nsSearchModel' => 'app\\models',
                'searchModelClass' => '',
                'indexWidgetType' => 'grid',
                'viewPath' => '@app/views',
                'template' => 'default'
            ],
            'preview' => '',
        ]; 
        // </editor-fold> 
        
        // <editor-fold defaultstate="collapsed" desc="generate settings">
        $generateSettings = 
        [
            '_csrf' => 'eoAHRxO1Sbyf1kU2MPVpi-SJikm9Y3lKfj_DxVMnTXhM2HMhRc0w9NKldlREhibxisrBIM4oSxIaUImCHxEKFQ==',
            'Generator' => [
                'db' => 'db',
                'tableName' => 'note',
                'nsModel' => 'app\\models',
                'modelClass' => 'Note',
                'skippedRelations' => '',
                'nameAttribute' => 'name, title, username',
                'hiddenColumns' => 'id, lock',
                'skippedColumns' => 'created_at, updated_at, created_by, updated_by, deleted_at, deleted_by, created, modified, deleted',
                'enableI18N' => '0',
                'messageCategory' => 'app',
                'nsController' => 'app\\controllers',
                'controllerClass' => 'NoteController',
                'baseControllerClass' => 'yii\\web\\Controller',
                'pluralize' => '1',
                'loggedUserOnly' => '0',
                'expandable' => '1',
                'pdf' => '0',
                'cancelable' => '1',
                'saveAsNew' => '1',
                'useTablePrefix' => '0',
                'generateSearchModel' => '1',
                'nsSearchModel' => 'app\\models',
                'searchModelClass' => 'NoteSearch',
                'indexWidgetType' => 'grid',
                'viewPath' => '@app/views',
                'template' => 'default',
            ],
            'answers' => [
                'f17a4976412428e9238b25d57ea49636' => '1',
                '5bfe67194b27820d480659d2b2c75d78' => '1',
                '89b61ed5499c242bfc66feeade1818b0' => '1',
                '07db6019fce7777e7793be4490842728' => '1',
                '165a57d803898dd24db81add814cc6f9' => '1',
                '58d40a399815ecd327c1dc991bf7bf6f' => '1',
                '6dcce94d31c4fde42a159dce6cf943ab' => '1',
                'cdcdeec675834db20b040d29eca15ee1' => '1',
                '3752e2d615986a9c00ce013a026d91ed' => '1',
                '36441a10fb0b6ad65e29a4605ec8a5ce' => '1',
                '7597687f58cdec7f8724672e85d59290' => '1',
                '64736e726ff8b28c30ccfd0a85c5f679' => '1',
            ],
            'generate' => ''
        ];            
        // </editor-fold>        
        
        
        if ( isset($setting) && $setting === 'preview' ){
            return $previewSettings;
        } 
        elseif ( isset($setting) && $setting === 'generate' ){
            return $generateSettings;
        }
        elseif ( isset($setting) && $setting === 'bundle-all' ){
            return ['preview'=>$previewSettings, 'generate'=>$generateSettings];
            
        } else {
            //throw new \yii\base\UserException("Invalid parameter passed");
            $msg = 'Allowed parameters were unspecified(preview, generate, bundle-all)';
            throw new \yii\base\InvalidArgumentException($msg);
        }
         
    }        
}