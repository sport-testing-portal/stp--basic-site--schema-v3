<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class WikiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
       $this->layout = "/column1-wiki";

       return parent::beforeAction($action);
    }    
    
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('wiki');
    }

    
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionWiki()
    {
        return $this->render('wiki');
    }    
    
    public function actionExtensions()
    {
        return $this->render('yii2-extensions');
    }  
    
    public function actionDatabaseRelations()
    {
        return $this->render('database-relations');
    }      
    
    public function actionDatabaseRequirements()
    {
        return $this->render('database-requirements');
    }     
    
    public function actionMigrationCreator()
    {
        return $this->render('extension-yii2-migration-creator');
    }     
    
    public function actionSaveRelations()
    {
        return $this->render('extension-yii2-save-relations-behavior');
    }     

    public function actionFaq()
    {
        
        return $this->render('faq');
    }     
}
