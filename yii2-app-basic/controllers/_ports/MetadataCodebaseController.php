<?php
// Even with the AweController as the base class of this controller the SRBAC permissions
// are being enforced on all actions for this controller even on my local developement workstation.
// The permissions on this web app are completely table driven as the lowest levels within
// the very architecture of the application. This web app is 100% locked down from the 
// inside out as all variations of a "Controller" automaticall inherent a security aware
// component of their code-dna that says all actions must be approved at the database level.
//    if no database autorization exists then authorization doesn't exist. 
///       the two concepts of database-access and application-routing ==> controller has 
//        blocked by a lower level a lower level function of "class inheritence" within
//        the definition of a "Controller" class of object within the Yii environment
//        database authrization and any-and-all access points (urls) within the app 
//        are bound to the same access law and authorization laws

// @notes Add function to return other sites metadata at the controller level? 
// The feature above would allow bulk-find-replace across sites. 
// This would be very handy in a read-only capacity for testing.
// ? add function to test views->models, models->views from the controller level crud?
// ? add function to test models from the controller level crud?
// ? add function to iterate through the controllers public actions?
// ? add function to fetch all controller urls
// run the all models as a CEC 001 test and output to html preferably a view

/* 
 * @package integrate codebase-with-database v0.0.0.4 (To insert all mvc code files names and paths into the database so the code files can be managed more efficiently and effectively)
 * @package database driven bulk find-replace v0.0.0.3 (To Mass manipulate the code by rule-sets stored in the database)
 * @package cec tracking v0.0.0.0      (to know the code by its adherence to certain design principles)
 * @package package tracking v0.0.0.0  (to know the code by its call stacks eg it's actionable trees of code)
 * @package automated system tests v0.0.0.0  (to validate the code base and specifically codebase-to-database interactions)
 */

/**
 * CEC - Consistent Engineering Criteria
 * cec-y1c-0001-code-layout-00 - A controller is a collection of packages. 
 * Each package has an inherent code maturity and code attrition level. 
 * A package is a code implementation of a desired feature.
 * ? List features implemented by the controller file?
 * ? List the packages of the controller
 * ? List the packages by feature
 * ? I want to see the code base by features, packages, and action trees eg code-coverage and call stacks
 * My purpose : to bring divine clarity, structure, and understanding to the code base and application.
 */

class MetadataCodebaseController extends AweController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public $defaultAction = 'admin';

    // <editor-fold defaultstate="collapsed" desc="yii generic generated code">
    

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
                'model' => $this->loadModel($id),
        ));
    }
    
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new MetadataCodebase;

        $this->performAjaxValidation($model, 'metadata-codebase-form');

        if(isset($_POST['MetadataCodebase']))
        {
            $model->attributes = $_POST['MetadataCodebase'];
            if($model->save()) {
                $this->redirect(array('view', 'id' => $model->metadata__codebase_id));
            }
        }

        $this->render('create',array(
                'model' => $model,
            ));
    }

    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model, 'metadata-codebase-form');

        if(isset($_POST['MetadataCodebase']))
        {
                $model->attributes = $_POST['MetadataCodebase'];
                if($model->save()) {
                        $this->redirect(array('view','id' => $model->metadata__codebase_id));
                }
        }

        $this->render('update',array(
                'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax'])){
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        }
        else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }


    /**
     * Lists all models.
     */
    public function actionIndex()
    {
//            $dataProvider=new CActiveDataProvider('MetadataCodebase');
//            $this->render('index', array(
//                    'dataProvider' => $dataProvider,
//            ));
            /*
            $test1 = $this->fetchSchemav2tov3Mods($direction='v2-to-v3');
            if (is_array($test1)){
                Barray::echo_array($test1);
            }
             * move on to next test?
             */
            $username = 'danabyrd@byrdbrain.com';
            $record = User::model()->findByAttributes(array('username'=>$username));
            Barray::echo_array($record->attributes);
            
            $user = new UserIdentity($username, 'cdu7s5ab');
            //$user2 = UserIdentity::createFromUser($user,'cdu7s5ab');
            CVarDumper::dump($user);
            echo "version number = " . Yii::app()->params["adminEmail"] . "<br>";
            echo "version number = " . Yii::app()->params["versionNumberShort"] . "<br>";
            $basePath = Yii::app()->basePath;
            require_once ($basePath . '/vendor/marcovtwout/yii-environment/VersionNumber_1.php');
            $v = new VersionNumber();
            //$v->setVersionNumber();
            echo $v->version('s'). "<br>";
            echo VersionNumber::version('l'). "<br>";
            echo VersionNumber::version('v'). "<br>";
            echo VersionNumber::vSpan(0). "<br>";
            echo Bver::vSpan(1). "<br>";
            //env = new \marcovtwout\YiiEnvironment\Environment();
            
            //$this->cecAddNewFilesToCodebaseTracking($scope=['models','views','controllers']);
    }
    /**
     * Lists all models.
     */
    public function actionMailer()
    {
        //xdebug_break();
        echo "Running " . __METHOD__ .'<br>';
        $basePath = Yii::app()->basePath;
        $path = $basePath . '/modules/usr/extensions/mailer/';
        echo is_dir($path) . "<br>";
        //require_once($path . 'phpmailer/class.phpmailer.php');
        echo "is file? " . is_file($path . 'PHPMailer.php') . "<br>";
        require_once($path . 'EMailer.php');
        Yii::import('application.modules.usr.mailer.EMailer');
        //Yii::import('application.models._base.BaseAddress');
        xdebug_break();
        $m = new EMailer();
        echo "is object" . is_object($m) . "<br>";
         
        /**
        * Include the the PHPMailer class.
           */
        /*
        //require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.'class.phpmailer.php');
        //require_once($path . 'phpmailer/class.phpmailer.php');
        require_once($path . 'phpmailer/class.phpmailer.php');
        //$dir = Yii::import('application.modules.usr.extensions.mailer');
        //Yii::import('application.modules.usr.extensions.mailer.phpmailer.PHPMailer.PHPMailer');
        $dir = Yii::import('application.modules.usr.extensions.mailer.phpmailer.class.phpmailer');
        echo "required worked \$dir = $dir<br>";
        //$mail = new PHPMailer\PHPMailer($exceptions=true);
//        $mail = new PHPMailer($exceptions=true);
//        echo (is_object($mail));
        
        
//        if (is_array($test1)){
//            Barray::echo_array($test1);
//        }
*/

            //env = new \marcovtwout\YiiEnvironment\Environment();
            
            //$this->cecAddNewFilesToCodebaseTracking($scope=['models','views','controllers']);
        
    }
    
    public function actionImportTrelloChecklists()
    {
        $importFile = 'trello.codebeautify.json';
        $basePath = Yii::app()->basePath;
        $filePath = $basePath . '/data/';
        $fileUri = $filePath . $importFile;
        if (is_file($fileUri)){
            $json_string = file_get_contents($fileUri);
            $json = Bjson::decode_wtype($json_string);
        }
        if (is_array($json)){
            foreach ($json as $idx => $jsonValue){
                echo "json \$idx = $idx <br>";
                //dd($jsonValue);
                if ($idx === 'checklists'){
                    $checklists = $jsonValue;
                }
            }
            $opml = '';
            $opml2 = [];
            echo "<br> parsing checklists <br>";
            $opmlHeader='<?xml version="1.0" encoding="UTF-8"?>' . "\n"
                . '<opml version="2.0">' . "\n"
                . '<head>' . "\n"
                . '    <title>Checklist test 123456</title>' . "\n"
                . '    <dateModified>Tue, 13 Mar 2018 00:43:47 +0000</dateModified>' . "\n"
                . '    <ownerName>jepi@gmail.com</ownerName>' . "\n" 
                . '</head>' . "\n"
                . '<body>' . "\n";
            echo "<pre><code>";
            $opml   .= $opmlHeader;
            $opml2[] = $opmlHeader;
            //echo $opmlHeader;
            foreach ($checklists as $idx2=>$checklist){
                echo "json \$idx2 = $idx2 <br>";
                if (isset($checklist['checkItems'])){
                    //echo "<h3>found check items</h3> <br>";
                    $checkItems = $checklist['checkItems'];
                    $checkItemsCnt = count($checkItems);

                    if (isset($checklist['name'])){
                        $checklistName = $checklist['name'];
                        echo "<p>checklist <b>$checklistName</b> (<b>$checkItemsCnt</b>)</p> <br>";
                        //echo '<outline text="' . $checklistName . '"</outline>'." <br>";
                        $opml .= '<outline text="' . $checklistName . '" status="open" />' . "\n";
                        //$opml2[] = '<outline text="' . $checklistName . '" status="open" _status="indeterminate" />' ;
                        $opml2[] = "<outline text=\"$checklistName\" status=\"open\" _status=\"indeterminate\" />" ;
                        
                        //echo "<pre><code>";
                    }
                    
                    foreach ($checkItems as $idx3 => $checkItem){
                        if (isset($checkItem['state'])){
                            $checkItemState = $checkItem['state'];
                            if ($checkItemState === 'incomplete'){
                                $status = 'open';
                            } else {
                                $status = 'closed';
                            }
                        }
                        if (isset($checkItem['name'])){
                            $checkItemName = $checkItem['name'];
                            //echo $checkItemState .' -- '. $checkItem['name'] ."<br>";
                            echo $checkItemState .' -- '. $checkItem['name'] ."<br>";
                            //echo '    <outline text="' . $checkItemName . '"</outline>' . "\n";
                            $checkItemName2 = str_ireplace("'", "", $checkItemName);
                            $opml .= '    <outline text="' . $checkItemName . '" status="'.$status.'"  _status="indeterminate" />' . "\n";
                            $opml2[] = "<outline text=\"$checkItemName2\" status=\"$status\"  _status=\"indeterminate\" />";
                        }
                    }
                    //echo "</code></pre>";
                }
//                $OpmlFooter='</body>' . "\n"
//                        . '</opml>' . "\n";
//                echo $OpmlFooter;
//                echo "</code></pre>";
                //dd($checklist);
            }
            $opmlFooter='</body>' . "\n"
                    . '</opml>' . "\n";
            $opml2[]= $opmlFooter;
            //echo $opmlFooter;
            $opml .= $opmlFooter;
            echo "</code></pre>";            
            $fileNameOut = 'trello.txt';
            $filePathOut = $basePath . '/runtime/';
            //$filePathOut = $basePath . '/views/ageGroup/';
            //$filePathOut = $basePath . '/controllers/_cec/';
            //$filePathOut = $basePath . '../assets/';
            if (! is_dir($filePathOut)){
                echo "directy doesn't exist <br>";
            }
            //$written = Bfile::write_to_file($filePathOut . $fileNameOut, $opml, $write_mode='w+');
            //$written = Bfile::write_to_file($filePathOut . $fileNameOut, $opml, 'w');
            //$written2 = Bfile::write_to_file($filePathOut . $fileNameOut, 'test');
            $written3 = Bfile::write_to_opml_file($filePathOut . $fileNameOut, $opml2);
            //echo $written . "<br>";
            //echo $written2 . "<br>";
            echo $written3 . "<br>";
            //dd($opml);
            //var_dump($opml);
            //echo Barray::dumpAsString($opml2);
            
        }
        
    }    
    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new MetadataCodebase('search');
        $model->unsetAttributes(); // clear any default values
        if(isset($_GET['MetadataCodebase'])){
            $model->attributes = $_GET['MetadataCodebase'];
        }
        $this->render('admin', array(
            'model' => $model,
            ));
    }
    
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */

    public function loadModel($id, $modelClass=__CLASS__)
    {
            $model = MetadataCodebase::model()->findByPk($id);
            if($model === null){
                    throw new CHttpException(404,'The requested page does not exist.');
            }
            return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model, $form=null)
    {
            if(isset($_POST['ajax']) && $_POST['ajax'] === 'metadata-codebase-form')
            {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
            }
    }    
    
    // End of yii generic generated code
    // </editor-fold> 
    

// mature code
    
    // <editor-fold defaultstate="collapsed" desc="package integrate codebase-with-database v0.0.0.4">
    // * package name: integrate codebase-with-database v0.0.0.4 
    // * package purpose: To insert all mvc code files names and paths into the 
    // database so the code files can be managed more efficiently and effectively.

    
    /**
     * Load code base table with entries
     * @package bulk find-replace v1
     */
    public function actionPopulate()
    {
        //$filepath = Yii::app()->basePath.'/files/foldername/filename.ext';
        $basePath = Yii::app()->basePath;
        $codeFolders = [
            'models'         => $basePath . '/models',
            'models/_base'   => $basePath . '/models/_base',
            'controllers'    => $basePath . '/controllers',
            'views'          => $basePath . '/views',
            ];
        // glob folders
        //$fileExtension = "php";
        $itemsProcessedCnt = 0;
        $filesProcessed = [];
        $foldersProcessed = [];
        $filesProcessedCnt = 0;
        $foldersProcessedCnt = 0;
        //$folderInProcessKey = "";
        //$parentFolderInProcessKey = 0;
        $parentFolderInProcess = "";
        //$foldersProcessedKeys = [];

        //foreach ($codeFolders as $keyLevel1 => $folderName) {
        foreach ($codeFolders as $folderName) {
            
        
            //$files = Bfile::globFiles($folderName, $fileExtension);
            $files = Bfile::globFiles($folderName);
            $fileURI=[];
            if (is_array($files)){
                echo "its an array test for file uri <br>";
                foreach ($files as $fileMetadata){
                    if (isset($fileMetadata['file_uri']) ){
                        $fileURI[]= $fileMetadata['file_uri'];
                    }
                }
                if (1===2){
                    echo "file uri follow <br>";
                    Barray::echo_array($fileURI);
                }
                //Barray::echo_array($files);
            }
            //$folderInProcessKey       = $keyLevel1;
            //$parentFolderInProcessKey = $keyLevel1;
            $parentFolderInProcess    = $folderName;
//            if (is_array($files)){
//                echo "its an array <br>";
//            }

            foreach ($fileURI as $fileOrFolder){
                // does the use of a file extension block the addition of folders?
                $fileList = [];
//                $directoryTest = $this->parseFileDirectoryFromArray($fileOrFolder);
//                if (is_array($directoryTest)){
//                    throw new CException("is an array");
//                } else {
//                    $fileOrFolder = $directoryTest;
//                }
                if (is_dir($fileOrFolder)){
                    // recurse
                    $fileList = Bfile::globFiles($fileOrFolder);
                    if (is_array($fileList)){
                        //echo "found a directory <b>$fileOrFolder</b><br>";
                        $filesInFolder = $this->parseFileUriFromAFileList($fileList);
                        foreach ($filesInFolder as $file_uri_item) {
                            $itemsProcessedCnt++;
                            $filesProcessed[] = $file_uri_item;
                            echo "$file_uri_item <br>";
                        }
                        //Barray::echo_array($filesInFolder);
                    }
//                    if ($this->populateTable($fileList) === 1){
//                        $foldersProcessedCnt++;
//                        $foldersProcessed[] = $fileOrFolder;
//                        $filesProcessedCnt += count($fileList); 
//                        $filesProcessed[] = $fileList;
//                    }                        
                }
                if (is_file($fileOrFolder)){
                    // process
                    echo "$fileOrFolder <br>";
                    $itemsProcessedCnt++;
                    $filesProcessed[] = $fileOrFolder;
                    //$fileList[] = $fileOrFolder;
                    
                }
            }


//            if ($this->populateTable($fileList) === 1)
//            {
//                $foldersProcessedCnt++;
//                $filesProcessedCnt += count($fileList); 
//                $filesProcessed[] = $fileList;
//                $foldersProcessed[$parentFolderInProcess][] = $parentFolderInProcess;
//            }
        }
        
//        foreach ($codeFolders as $keyLevel1 => $folderName) {
//            Barray::echo_array($foldersProcessed, $encode_html_entities_yn='Y');
//        }
        echo "<h1>items processed $itemsProcessedCnt </h1><br>";
        
        Barray::echo_array($filesProcessed);
    }
    
    /**
     * Load code base table with entries
     * @todo prevent duplicate entries
     * @calls $this->parseFileUriFromAFileList($fileList)
     * @calls Bfile::globFiles()
     * @internal dev status = Golden!
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesOnDisk?XDEBUG_SESSION_START=netbeans-xdebug
     * @package bulk find-replace v1
     */
    public function actionFindCodeFilesOnDisk()
    {
        //$filepath = Yii::app()->basePath.'/files/foldername/filename.ext';
        $basePath = Yii::app()->basePath;
        $codeFolders = [
            'models'         => $basePath . '/models',
            'models/_base'   => $basePath . '/models/_base',
            'controllers'    => $basePath . '/controllers',
            'views'          => $basePath . '/views',
            ];

        $itemsProcessedCnt   = 0;
        $itemsProcessed      = [];
        $foldersProcessedCnt = 0;
        $foldersProcessed    = [];
        $filesProcessedCnt   = 0;
        $filesProcessed      = [];
        
        $maxFilesToProcessCnt = 0;   
        
        foreach ($codeFolders as $folderNameStem=>$folderPath) 
        {
            // add a throttle for testing a small batch of files
            if ($maxFilesToProcessCnt > 0 && $filesProcessedCnt >= $maxFilesToProcessCnt ){
                break;
            }
            $files = Bfile::globFiles($folderPath);
            $fileURI=[];
            if (is_array($files)){
                //echo "its an array test for file uri <b>$folderNameStem</b><br>";
                foreach ($files as $fileMetadata){
                    if (isset($fileMetadata['file_uri']) ){
                        $fileURI[]= $fileMetadata['file_uri'];
                    }
                }
                if (1===2){
                    echo "file uri follow <br>";
                    Barray::echo_array($fileURI);
                }
            }

            foreach ($fileURI as $fileOrFolder){
                $itemsProcessedCnt++;
                if (is_file($fileOrFolder)){
                    //echo "is file: $fileOrFolder <br>";
                    $filesProcessedCnt++;
                    $filesProcessed[] = $fileOrFolder;
                }                
                if (is_dir($fileOrFolder) && $folderNameStem === 'views'){
                    $foldersProcessedCnt++;
                    $foldersProcessed[] = $fileOrFolder;
                    $fileList = Bfile::globFiles($fileOrFolder);
                    if (is_array($fileList)){
                        //echo "found a directory <b>$fileOrFolder</b><br>";
                        $filesInFolder = $this->parseFileUriFromAFileList($fileList);
                        foreach ($filesInFolder as $file_uri_item) {
                            $itemsProcessedCnt++;
                            $itemsProcessed[] = $file_uri_item;
                            if (is_dir($file_uri_item)){
                                //echo "<b>is a sub folder</b> $file_uri_item <br>";
                            }
                            if (is_file($file_uri_item)){
                                //echo "is file in sub folder $file_uri_item <br>";
                                $filesProcessedCnt++;
                                $filesProcessed[] = $file_uri_item;
                            }
                        }
                    }
                }
            }
        }
        $verboseYN = 'N';
        if ($verboseYN === 'Y'){
            echo "<h1>items processed $itemsProcessedCnt </h1><br>";
            echo "<h2>folders processed $foldersProcessedCnt </h2><br>";
            echo "<h2>files processed $filesProcessedCnt </h2><br>";
        }
        $debugYN = 'N';
        if ($debugYN === 'Y'){
            echo "<h3>files</h3>";
            Barray::echo_array($filesProcessed);
            echo "<h3>folders</h3>";
            Barray::echo_array($foldersProcessed);            
        }
        
        $this->insertManyRowsCodebaseTable($filesProcessed);
    }
    
    
    /**
     * Insert item in a file array into the code base table
     * @param string $fileUri
     * @return int
     * @internal dev status = Golden!
     * http://localhost:8207/index.php/metadataCodebase/findCodeFilesOnDisk?XDEBUG_SESSION_START=netbeans-xdebug
     */
    public function insertOneRowCodebaseTable($fileUri)
    {
        $basePath = Yii::app()->basePath;
        $fileName = Bfile::base_name($fileUri);
        $fileUri2 = str_ireplace($basePath, '', $fileUri);  // base path removed
        $fileUri3 = str_ireplace($fileName, '', $fileUri2); // file name removed
        $fileUri4 = Bfile::remove_trailing_slash($fileUri3);
        // @todo: xlat column names
        if (empty($fileName) || empty($fileUri4)){
            throw new CException("empty values");
        }
        Barray::echo_array(['file'=>$fileName, 'path'=>$fileUri4]);
        
        $criteria = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_name=:c1 AND file_path_uri=:c2';
        $criteria->params    = [':c1'=>$fileName, ':c2'=>$fileUri4];
        $model = MetadataCodebase::model()->find($criteria);   // will return null if not found
        
        if (empty($model)){
            $model2 = new MetadataCodebase();
            $model2->attributes = ['file_name'=>$fileName, 'file_path_uri'=>$fileUri4];
            //Barray::echo_array($model2->attributes);
            //return;
            $validate = true;

            if($model2->save($validate)) {
                echo "model saved <br>";
                return 1;
            } else {
                return 0;
            }             
        } else {
            echo "model was found! <br>";
        }
    }    
    
    /**
     * Insert items in a file array into the code base table
     * @param array $files  [0=>'file1',1=>'file2']
     * @return int
     * @calledBy actionFindCodeFilesOnDisk()
     * @internal dev status = Golden!
     * @todo handle relative uri paths
     */
    public function insertManyRowsCodebaseTable($files)
    {
        if (is_array($files))
        {
            $counter = 0;
            foreach ($files as $fileUri){
                // what if it is a partial uri eg a relative uri?
                $basePath = Yii::app()->basePath;
                if (stripos($fileUri, $basePath) !== false){
                    // this file uri is a full path
                    $this->insertOneRowCodebaseTable($fileUri);
                } else {
                    // must be a relative uri?
                    echo "Processing $fileUri as (" . $basePath . $fileUri . ") <br>";
                    $insertResult = $this->insertOneRowCodebaseTable($basePath . $fileUri);
                    
                }
                //$this->insertOneRowCodebaseTable($fileUri);
                $counter++;
                if ($counter >= 1){
                    //break;
                }
            }
        }
        else 
        {
            throw new CException("Only arrays are allowable as a parameter");
        }
    }
        
    /**
     * Return string keyed array ['file_uri'=>'somefile']
     * @param type $mixed
     * @return type
     * @calledBy $this->actionPopulate(), $this->actionFindCodeFilesOnDisk()
     */
    protected function parseFileUriFromAFileList($mixed) {
        $uriOut=[];
        if (is_array($mixed)){
            foreach($mixed as $metadata){
                if (isset($metadata['file_uri'])){
                    $uriOut[] = $metadata['file_uri'];
                }
            }
        }
        return $uriOut;
    }
        
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="package database driven bulk find-replace v0.0.0.3">
    // package name: database driven bulk find-replace v0.0.0.3
    // package purpose: To Mass manipulate the code by rule-sets stored in the database

    /**
     * 
     * @param type $id
     */
    public function actionFindPackagesById($id){
        $packages = $this->fetchAllPackageNamesInCodeFileById($id);
        dd($packages);
    }
    
    /**
     * Find rows with an unknown status, or a known status
     * @param $cecID 1 = no issues, 2 = has errors
     * @internal dev status = Under Construction
     * @calls $this->manageCecStatus()
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesInDatabase?cecID=1
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesInDatabase?cecID=1&readOnlyYn=Y
     * @package bulk find-replace v1
     */
    public function actionFindCodeFilesInDatabase($cecID=2,$readOnlyYn='Y')
    {
        //$readOnlyYn='Y';
        if (!empty($cecID)){
            $rows = $this->fetchRowsToProcessByCec($cecID);
        } else {
            $rows = $this->fetchRowsToProcess(); // returns an array of CActiveRecords with cec=2
        }
        if (is_array($rows)){
            foreach ($rows as $idx => $model) 
            {
                if ($idx >= 100) {
                    break;
                }
                echo "<h1>$model->file_path_uri -- $model->file_name</h1>";
                Barray::echo_array($model->attributes);
                $result = $this->manageCecStatus($model,$readOnlyYn);
                //Barray::echo_array($result, 'Y');
                // remove the before and after content to minimize screen output
                echo "<h3>process output</h3>";
                unset($result['original']);
                unset($result['revised']);
                Barray::echo_array($result, 'Y');
                echo "<h3>****************process item complete********************</h3>";
            }

        }

        if (empty($rows)){
            echo "nothing found <br>";
        }
    }
    
    /**
     * Find rows with an unknown status, or a known status
     * @param $cecID 1 = no issues, 2 = has errors
     * @param array
     * @internal dev status = Ready for Testing
     * @calls cecPackageInstall::package()
     * @calls MetadataCodebase::model()->findAllBySql()
     * @calls $this->manageCecStatusUsingRules()
     * @calls fetchRowsToProcessByCec($cecID)
     * @calls fetchRowsToProcess()
     * @calls calls $this->manageCecStatusUsingRules(($model,$rules,$readOnlyYn))
     * @prefork calls $this->manageCecStatus()
     * 
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesUsingRules?cecID=1
     * @internal http://localhost:8207/index.php/metadataCodebase/findCodeFilesUsingRules?cecID=6&readOnlyYn=Y
     * @package CEC - tracking v0.0.0.1
     * @prefork package bulk find-replace v1
     * @internal forked from actionFindCodeFilesInDatabase v.0.0.0.1
     */
    public function actionFindCodeFilesUsingRules($cecID=2, $readOnlyYn='Y')
    {
        //$readOnlyYn='Y';
        // @todo find a way to add selection criterial like sql in a cec definition
        $cec = MetadataCodebaseCec::model()->findByPk($cecID);
        if ( isset($cec->cec_scope) && isset($cec->rule_file_uri) ){
            $cecScope = $cec->cec_scope;
            $settingsFile = $cec->rule_file_uri;

            $basePath = Yii::app()->basePath;
            //$file = '/var/www/sites/gsm/dev/mvp-yii-1.1.19/mvp-yii-1.1.19/protected/views/_cec/cec-y1v-0001-v.0.0.0.1-install.php';
            //$settingsFile = '/views/_cec/cec-y1v-0001-v.0.0.0.1-install.php';
            $settingsFilePath = $basePath . $settingsFile;
            require_once($settingsFilePath);
            // <editor-fold defaultstate="collapsed" desc="array layout">
            /*
                [
                'codebase_cec_id'=>6, // used for status log entries
                'scope'=>[
                    'query'=>[
                        'sql'=>"select * from metadata__codebase "
                             . "where file_name = '_form.php';"]],             
                  'find'=>['string' => $find,'regex' => '',],
                  'replace'=>['string' => $replace,'regex' => '',
                ]
             * 
             */
            // </editor-fold>            
            $settings = cecPackageInstall::package();
            if (isset($settings['scope']['query']['sql'])){
                $sql = $settings['scope']['query']['sql'];
            }
            $rows = MetadataCodebase::model()->findAllBySql($sql, $params=[]);
            if (! empty($rows) && is_array($rows)){
               if (true){
                   echo "found " . count($rows) . " rows to process <br>";
               }
                
            }
            if (isset($settings['find']['regex']) && isset($settings['replace']['regex'])){
                $rules=[
                    [    
                    'find'    => $settings['find']['regex'],
                    'replace' => $settings['replace']['regex'],
                    ]
                ];  
                if (isset($settings['find']['regexmods'])){
                    $rules[0]['regexmods'] = $settings['find']['regexmods'];
                }
                if (isset($settings['find']['use regex'])){
                    $useRegex = $settings['find']['use regex'];
                    $rules[0]['use regex'] =  $useRegex;
                }
            } elseif (isset($settings['find']['string']) && isset($settings['replace']['string'])){
                $rules=[
                    
                    'find'    => $settings['find']['string'],
                    'replace' => $settings['replace']['string'],
                    
                ];                
                $useRegex = false;
            }
           
            
        } else {
            // version 0.0.0.1 data grab
            if (!empty($cecID)){
                $rows = $this->fetchRowsToProcessByCec($cecID);
            } else {
                $rows = $this->fetchRowsToProcess(); // returns an array of CActiveRecords with cec=2
            }
        }
        if (is_array($rows)){
            foreach ($rows as $idx => $model) 
            {
//                if ($idx >= 1) {
//                    break;
//                }
                echo "<h1>$model->file_path_uri -- $model->file_name</h1>";
                Barray::echo_array($model->attributes,'Y');
                $result = $this->manageCecStatusUsingRules($model,$rules,$settings, $readOnlyYn);
                //Barray::echo_array($result, 'Y');
                // remove the before and after content to minimize screen output
                echo "<h3>process output</h3>";
                unset($result['original']);
                unset($result['revised']);
                Barray::echo_array($result, 'Y');
                echo "<h3>****************process item complete********************</h3>";
            }

        }

        if (empty($rows)){
            echo "nothing found <br>";
        }
    }    
    
    /**
     * Run and SQL fetch of the revised column list
     * @calledBy replaceSchema_fieldNames_v1_names_to_v3_names()
     * @calledby $this->translateSchemaColumnNames()
     * @category source code database schema xlation
     * @internal dev status = Ready for Testing
     * @package bulk find-replace v1
     */
    protected function fetchSchemaVersionColumnXlation() {
        // Return [0=>['find'=>"abc",'replace'=>"123"],1=>['find'=>'', 'replace'=>'']
        // start with a small hard coded batch
        $xlat = [
            0 => ['find'=>'created_at', 'replace'=>'created_at'],
            1 => ['find'=>'updated_at', 'replace'=>'updated_at']
        ];
        return $xlat;
    }
       
    /**
     * Gets the most recent status record
     * @param $cecID 1 = no issues, 2 = has errors
     * @return CActiveRecord
     * @package bulk find-replace v1
     */
    protected function fetchRowsToProcessByCec($cecID)
    {
        // -- select on the most recent record and having a particular status
        $sql = "SELECT mc.*
            FROM metadata__codebase mc
            left join 
            (
                        select m1.codebase_id, max(m1.cec_status_id) as max_cec_status_id 
                                from metadata__codebase_cec_status m1
                                group by m1.codebase_id 
                ) d1 on mc.codebase_id = d1.codebase_id
                left join 
                                metadata__codebase_cec_status mccs
                                on d1.max_cec_status_id = mccs.cec_status_id 
                where mccs.codebase_cec_id = :p1
            order by mc.codebase_id, mccs.created_at desc; ";
        $params = [':p1'=>$cecID];
        
//        $criteria->select    = 'codebase_id, file_name, file_path_uri';
//        $criteria->condition = 'file_name=:c1 AND file_path_uri=:c2';
//        $criteria->params    = [':p1'=>$cecID];           
        $rows = MetadataCodebase::model()->findAllBySql($sql, $params); 
        return $rows;
    }    
     
    /**
     * Gets the most recent status record
     * @return CActiveRecord
     * @package bulk find-replace v1
     */
    protected function fetchRowsToProcess()
    {
        // -- select on the most recent record and having a particular status
        $sql = "SELECT mc.*
            FROM metadata__codebase mc
            left join 
            (
                        select m1.codebase_id, max(m1.cec_status_id) as max_cec_status_id 
                                from metadata__codebase_cec_status m1
                                group by m1.codebase_id 
                ) d1 on mc.codebase_id = d1.codebase_id
                left join 
                                metadata__codebase_cec_status mccs
                                on d1.max_cec_status_id = mccs.cec_status_id 
                where mccs.codebase_cec_id = 2
            order by mc.codebase_id, mccs.created_at desc; ";
        $params = [];
        $rows = MetadataCodebase::model()->findAllBySql($sql, $params); 
        return $rows;
    }

    /**
     * Run a business rule on a single file and write status to database
     * @param MetadataCodebase::model() $codebaseModel
     * @param array $rules A find-replace array [['find'=>'','replace'=>'']]
     * @param readOnlyYn default = Y
     * @prefork calledBy $this->actionFindCodeFilesInDatabase()
     * @preFork calls $this->xlatOneFile()
     * @todo proposed callers list: []
     * 
     * @calledBy actionFindCodeFilesUsingRules($cecId, $readOnlyYn)
     * @calls xlatOneFileUsingRules($content, $rules)
     * 
     * @prefork package bulk find-replace v1
     * @internal forked from manageCecStatus v0.0.0.1
     * @todo change the middle manager function xlatOneFile to a non-hardcoded function or one that takes params for the ruleset
     * @package CEC Tracking v0.0.0.1
     * @see manageCecStatus() that was used bulk-edit-replace v0.0.0.1
     * @internal Scope - Manager of One File at a time, must be called by a multi-item manager method
     * @version 0.0.0.2 status id matches the specific rule being processed
     * @internal version history: 0.0.0.1 contained hard coded CEC ids
     */
    protected function manageCecStatusUsingRules($codebaseModel, $rules, $settings, $readOnlyYn='Y') {
        // build a physical path to access the file
        if (is_array($rules)){
            echo "Received a rules array in " . __METHOD__ . "<br>";
            dd($rules);
            // @todo the rules array is missing the rule metadata
        }
        if (is_array($settings)){
            echo "Received a settings array in " . __METHOD__ . "<br>";
            dd($settings);
            // @todo the rules array is missing the rule metadata
        }        
        $model = $codebaseModel;
        $basePath = Yii::app()->basePath;
        $filePath = $basePath . $model->file_path_uri . '/' . $model->file_name;
        if (is_file($filePath)){
            $result = $this->xlatOneFileByRules($filePath, $rules, $readOnlyYn);
            if (is_array($result)) {
                //Barray::echo_array($result);
                if (is_array($settings)){
                    if (isset($settings['codebase_cec_id'])){
                        $codebaseCecId =  $settings['codebase_cec_id'];
                    } else {
                        $codebaseCecId = 0;
                        throw new CException("The settings array must contain a CEC Id");
                    }
                }
                $status = ['codebase_id'=>$model->codebase_id];
                if ($result['sha256 changed'] === 'yes'){
                    // set status = 2 has errors
                    // @todo how do I know there has been an error? the sha has changed
                    // @todo how do I know the correct id and bias of pass fail to write to the db?
                    // @todo the test bias must come from the rules array!
                    if ($codebaseCecId > 0){
                        $status['codebase_cec_id'] = $codebaseCecId; // prefork = 2
                    } else {
                        $status['codebase_cec_id'] = 2; // prefork = 2
                    }
                    $status['cec_status'] = 'has errors';
                } else if ($result['sha256 changed'] === 'no'){
                    // set status = 1 all valid
                   if ($codebaseCecId > 0){
                        $status['codebase_cec_id'] = $codebaseCecId; // prefork = 1
                    } else {
                        $status['codebase_cec_id'] = 1; // prefork = 1
                    }                    
                    $status['cec_status'] = 'no issues found';
                }
                if ($result['file modified'] === 'yes'){
                    //set status = 1 all valid
                    $status['codebase_cec_id'] = 1;
                    $status['cec_status'] = 'file was modified';
                }
                $mStat = new MetadataCodebaseCecStatus;
                $mStat->attributes = $status;
                Barray::echo_array($status,'Y');                
                $validate = true;
                if ($mStat->save($validate)) {
                    echo "file evaluation <b>status written to database</b> <br>";
                } else {
                    Barray::echo_array($mStat->errors);
                    echo "file evaluation status <b>was not written</b> to database <br>";
                }
                return $result;
            }
        }
    }
    
    
   /**
     * Writes revised source code to the hard disk
     * @param type $fileUriList
     * @param array $rules Search and replace parameter array [['find'='','replace'=''],...]
     * @preFork calls $this->replaceSchema_fieldNames_v1_names_to_v3_names
     * @calls ???
     * @todo Determine who|what to call
     * @preForm calledBy $this->manageCecStatus($codebaseModel, $readOnlyYn='Y')
     * @calledBy $this->manageCecStatusUsingRules()
     * @internal dev status = Ready for Testing
     * @package bulk find-replace v1
     */
    protected function xlatOneFileByRules($fileUri, $rules, $readOnlyYn='Y') {
        //$readOnlyYn = "Y";
        $ruleSetResultLog = [];
        //@todo if view file then call view cec v0001?

        if (is_file($fileUri)){
            $fileContentsOriginal = file_get_contents($fileUri);
            $sha256_file_bfr = hash_file('sha256', $fileUri);
            $ruleSetResult = $this->replaceStringsUsingRules($fileContentsOriginal,$rules);
            $ruleSetResultLog[] = $ruleSetResult;
            if ($readOnlyYn === 'N'){
                // write to the source file with the replacesments
                echo "running in disk write-enabled mode <br>";
                if (isset($ruleSetResult['replacement cnt'])){
                    $replacementCnt = $ruleSetResult['replacement cnt'];
                    if (isset($ruleSetResult['revised'])){
                        $revisedText = $ruleSetResult['revised'];
                    } else {
                        echo "failed write logic test #1 <br>";
                    }
                    if ($replacementCnt > 0 && strlen($revisedText) > 0){
                        file_put_contents($fileUri, $revisedText);
                        $replacementCnt = 0;
                        $revisedText = '';
                    } else {
                        echo "failed write logic test #2 <br>";
                        echo "\$replacementCnt=*$replacementCnt* <br>";
                        echo "strlen(\$revisedText=*". strlen($revisedText) . "* <br>";
                    }
                } else {
                    echo "failed replacement cnt logic test #1 <br>";
                }
            } else {
                echo "running in read-only mode <br>";
            }
            $sha256_bfr = hash('sha256', $ruleSetResult['original']);
            $sha256_aft = hash('sha256', $ruleSetResult['revised']);
            
            $sha256_file_aft = hash_file('sha256', $fileUri);
            
            if ($sha256_bfr !== $sha256_aft){
                $ruleSetResult['sha256 original'] = $sha256_bfr;
                $ruleSetResult['sha256 revised']  = $sha256_aft;
                $ruleSetResult['sha256 changed']  = 'yes';
            } else {
                $ruleSetResult['sha256 changed']  = 'no';
            }
            if ($sha256_file_bfr !== $sha256_file_aft){
                $ruleSetResult['file modified']   = 'yes';            
            } else {
                $ruleSetResult['file modified'] = 'no';            
            }
            return $ruleSetResult;
        } else {
            throw new CException ("A file URI must be passed in");
        }
    }
    
    /**
     * Run a business rule on a single file and write status to database
     * @param MetadataCodebase::model() $codebaseModel
     * @param readOnlyYn default = Y
     * @calledBy $this->actionFindCodeFilesInDatabase()
     * @calls $this->xlatOneFile()
     * @package bulk find-replace v1
     * @version 0.0.0.1
     * @internal dev status = Golden!
     */
    protected function manageCecStatus($codebaseModel, $readOnlyYn='Y') {
        // build a physical path to access the file
        $model = $codebaseModel;
        $basePath = Yii::app()->basePath;
        $filePath = $basePath . $model->file_path_uri . '/' . $model->file_name;
        if (is_file($filePath)){
            $result = $this->xlatOneFile($filePath, $readOnlyYn);
            if (is_array($result)) {
                //Barray::echo_array($result);
                $status = ['codebase_id'=>$model->codebase_id];
                if ($result['sha256 changed'] === 'yes'){
                    // set status = 2 has errors
                    $status['codebase_cec_id'] = 2;
                    $status['cec_status'] = 'has errors';
                } else if ($result['sha256 changed'] === 'no'){
                    // set status = 1 all valid
                    $status['codebase_cec_id'] = 1;
                    $status['cec_status'] = 'no issues found';
                }
                if ($result['file modified'] === 'yes'){
                    //set status = 1 all valid
                    $status['codebase_cec_id'] = 1;
                    $status['cec_status'] = 'file was modified';
                }
                $mStat = new MetadataCodebaseCecStatus;
                $mStat->attributes = $status;
                Barray::echo_array($status);                
                $validate = true;
                if ($mStat->save($validate)) {
                    echo "file evaluation <b>status written to database</b> <br>";
                } else {
                    Barray::echo_array($mStat->errors);
                    echo "file evaluation status <b>was not written</b> to database <br>";
                }
                return $result;
            }
        }
    }
    
   /**
     * Writes revised source code to the hard disk
     * @param type $fileUriList
     * @calls $this->replaceSchema_fieldNames_v1_names_to_v3_names
     * @calledBy $this->manageCecStatus($codebaseModel, $readOnlyYn='Y')
     * @internal dev status = Ready for Testing
     * @package bulk find-replace v1
     * @version 0.0.0.1
     */
    protected function xlatOneFile($fileUri, $readOnlyYn='Y') {
        //$readOnlyYn = "Y";
        $ruleSetResultLog = [];

        if (is_file($fileUri)){
            $fileContentsOriginal = file_get_contents($fileUri);
            $sha256_file_bfr = hash_file('sha256', $fileUri);
            $ruleSetResult = $this->replaceStrings($fileContentsOriginal);
            $ruleSetResultLog[] = $ruleSetResult;
            if ($readOnlyYn === 'N'){
                // write to the source file with the replacesments
                echo "running in disk write-enabled mode <br>";
                if (isset($ruleSetResult['replacement cnt'])){
                    $replacementCnt = $ruleSetResult['replacement cnt'];
                    if (isset($ruleSetResult['revised'])){
                        $revisedText = $ruleSetResult['revised'];
                    } else {
                        echo "failed write logic test #1 <br>";
                    }
                    if ($replacementCnt > 0 && strlen($revisedText) > 0){
                        file_put_contents($fileUri, $revisedText);
                        $replacementCnt = 0;
                        $revisedText = '';
                    } else {
                        echo "failed write logic test #2 <br>";
                        echo "\$replacementCnt=*$replacementCnt* <br>";
                        echo "strlen(\$revisedText=*". strlen($revisedText) . "* <br>";
                    }
                } else {
                    echo "failed replacement cnt logic test #1 <br>";
                }
            } else {
                echo "running in read-only mode <br>";
            }
            $sha256_bfr = hash('sha256', $ruleSetResult['original']);
            $sha256_aft = hash('sha256', $ruleSetResult['revised']);
            
            $sha256_file_aft = hash_file('sha256', $fileUri);
            
            if ($sha256_bfr !== $sha256_aft){
                $ruleSetResult['sha256 original'] = $sha256_bfr;
                $ruleSetResult['sha256 revised']  = $sha256_aft;
                $ruleSetResult['sha256 changed']  = 'yes';
            } else {
                $ruleSetResult['sha256 changed']  = 'no';
            }
            if ($sha256_file_bfr !== $sha256_file_aft){
                $ruleSetResult['file modified']   = 'yes';            
            } else {
                $ruleSetResult['file modified'] = 'no';            
            }
            return $ruleSetResult;
        } else {
            throw new CException ("A file URI must be passed in");
        }
    }

    /**
     * read-only search-replace test using a ruleset array passed in
     * @param string $fileContent
     * @param array $rules A find replace array [[find='',replace='']]
     * @PreFork calledBy $this->xlatOneFile($fileUri, $readOnlyYn='Y')
     * 
     * @calls $this->fetchSchemaVersionColumnXlation()
     * @category source code database-schema xlation
     * @return array [original, revised, rules processed cnt, search results]
     * @internal dev status = Ready for Testing
     * @internal forked from replaceStrings() which is used by "bulk find-replace v1"
     * 
     * @internal this is a second version of the replaceStrings concept, but the 
     * @internal first version using a ruleset passed in
     * @package CEC-tracking v0.0.0.1
     * @version 0.0.0.1
     */
    protected function replaceStringsUsingRules($fileContent,$rules) {
        $debug = true;
        if (!is_array($rules)){
            throw new CException("the \$rules param must be passed as an array");
        }
        if ($debug){
            echo "<h3> rules follow</h3>";
            dd([$rules]);            
        }
        // Next function returns: [0=>['find'=>"abc", 'replace'=>"123"], 1=>[]],
        $workCopy   = $fileContent;
        $ruleProcessedCnt = 0;
        $replacementTotalCnt = 0;
        $results = [];
        foreach($rules as $idx => $row){
            if ($debug){
                echo "<h3> rule row follows - from inside the loop of replaceStringUsingRules</h3>";
                dd([$row]);            
            }            
            if (isset($row['find'])  && isset($row['replace']) ){
                $find    = $row['find']; 
                $replace = $row['replace'];
            } else {
                $find    = '';
                $replace = '';
            }
            if (strlen($find) > 0 && strlen($replace) > 0){
                $replaceCnt = null;
                if (isset($row['use regex']) && $row['use regex'] === true){
                    echo "<h3> taking the regex route</h3>";
                    if (isset($row['regexmods'])){
                        $regexMods = $row['regexmods'];
                    } else {
                        $regexMods = '';
                    }

                    $stringCleaned = preg_replace($find.$regexMods, $replace, $workCopy, 1, $replaceCnt);
                } else {
                    echo "<h3> taking the string route</h3>";
                    $stringCleaned = str_replace($find, $replace, $workCopy, $replaceCnt);
                }
                $workCopyLog[] = $stringCleaned;
                $replacementTotalCnt += (int)$replaceCnt;
                $workCopy = $stringCleaned;
                $ruleProcessedCnt++;
                
                $results[] = ['find'=>$find, 'replace'=>$replace, 'count'=>$replaceCnt];
            }
        }

        $output = [
            'original'           => $fileContent,
            'revised'            => $workCopy,
            'rules processed cnt'=> $ruleProcessedCnt,
            'replacement cnt'    => $replacementTotalCnt,
            'search results'     => $results
        ];
        
        if ($debug === true){
            echo "<pre>" . __METHOD__ . "</pre><br>";
            echo "<h1>items processed $ruleProcessedCnt </h1><br>";
            Barray::echo_array($workCopyLog,'Y');
            // @todo test how echo_array compares to dd, easy no html escaping. But is that assertion true?
            //dd($workCopyLog);
            echo "<h2>output array </h2><br>";
            Barray::echo_array($output,'Y');
        }        
        return $output;
    }
          
    /**
     * read-only find schema v1 field names
     * @param string $fileContent
     * @calledBy $this->xlatOneFile($fileUri, $readOnlyYn='Y')
     * @calls $this->fetchSchemaVersionColumnXlation()
     * @category source code database-schema xlation
     * @return array [original, revised, rules processed cnt, search results]
     * @internal dev status = Ready for Testing
     * @package bulk find-replace v1
     * @version 0.0.0.1
     */
    protected function replaceStrings($fileContent) {
        
        // Next function returns: [0=>['find'=>"abc", 'replace'=>"123"], 1=>[]],
        $phase = 'one';
        if ($phase === 'one'){
            $schemaXlat = $this->fetchSchemaVersionColumnXlation(); // handles [find=created_dt, replace=created_at]
        }
        if ($phase === 'two'){
            $schemaXlat = $this->fetchSchemav2tov3Mods($direction); // handles [find=org_name, replace=org]
        }
        $workCopy   = $fileContent;
        $ruleProcessedCnt = 0;
        $replacementTotalCnt = 0;
        $results = [];
        foreach($schemaXlat as $idx => $row){
            
            if (isset($row['find'])  && isset($row['replace']) ){
                $find    = $row['find']; 
                $replace = $row['replace'];
            } else {
                $find    = '';
                $replace = '';
            }
            if (strlen($find) > 0 && strlen($replace) > 0){
                $replaceCnt = null;
                $stringCleaned = str_replace($find, $replace, $workCopy, $replaceCnt);
                $workCopyLog[] = $stringCleaned;
                $replacementTotalCnt += (int)$replaceCnt;
                $workCopy = $stringCleaned;
                $ruleProcessedCnt++;
                $results[] = ['find'=>$find, 'replace'=>$replace, 'count'=>$replaceCnt];
            }
        }

        $output = [
            'original'           => $fileContent,
            'revised'            => $workCopy,
            'rules processed cnt'=> $ruleProcessedCnt,
            'replacement cnt'    => $replacementTotalCnt,
            'search results'     => $results
        ];
        $debug = false;
        if ($debug === true){
            echo "<pre>" . __METHOD__ . "</pre><br>";
            echo "<h1>items processed $ruleProcessedCnt </h1><br>";
            Barray::echo_array($workCopyLog);
            echo "<h2>output array </h2><br>";
            Barray::echo_array($output);
        }        
        return $output;
    }
      
    /**
     * @param $direction v3-to-v2, v2-to-v3
     * @package database driven bulk find-replace v0.0.0.3
     */
    protected function fetchSchemav2tov3Mods($direction='v2-to-v3') {
        $sql = "select distinct TABLE_NAME, concat(TABLE_NAME, '_name') as gsm_v2_type_name
	, concat(TABLE_NAME, '') as gsm_v3_type_name
	from information_schema.COLUMNS
	where TABLE_SCHEMA = 'gsm_dev_v2' and COLUMN_NAME = concat(TABLE_NAME, '_name');";
        
        $columns = Yii::app()->db->createCommand($sql)->queryAll($fetchAssociative=true);
        
        if (!empty($direction) && $direction === 'v2-to-v3'){
            $find    = 'gsm_v2_type_name';
            $replace = 'gsm_v3_type_name';
        } elseif (!empty($direction) && $direction === 'v3-to-v2') {
            $find    = 'gsm_v3_type_name';
            $replace = 'gsm_v2_type_name';
        } else {
            throw new CException("unknown parameter value");
        }
        
        $editFind = [];
        if (!empty($columns) && is_array($columns)){
            foreach ($columns as $row){
                $editFind[] = ['find'=>$row[$find], 'replace'=>$row[$replace]];
            }
            // inject manually added tables & columns
            if ($direction === 'v2-to-v3'){
                // Next two tables are missed by the select above
                $editFind[] = ['find'=>'short_name', 'replace'=>'country'];
                $editFind[] = ['find'=>'gsm_sport_name', 'replace'=>'sport'];
            }
        }
        return $editFind;
    }
    

    // </editor-fold>

// code in development    
    
    // <editor-fold defaultstate="collapsed" desc="package cec tracking v0.0.0.0">
    // package name: cec tracking v0.0.0.0      
    // package purpose: To know the code and its maturity status by its adherence 
    // to certain design principles.
    
    /** @package-sub-module *************(get uri from the database )**********/
    
    /**
     * Converts an array in glob format uri into relative uri format
     * @return array $modelsRelativeUri
     * @calledBy cecAddNewFilesToCodebaseTracking
     * 
     * @package CEC Tracking v0.0.0.1
     */
    protected function fetchMvcModelsAllRelativeUri() {
        $verbose = false;
        $method = "fetchMvcModels AllRelativeUri()";
        $models = $this->fetchMvcModelsAll();
        if (is_array($models) && empty($models)){
            echo "the call to fetchMvcModelsAll() returned an empty array <br>";
            return [];
        } elseif (is_array($models) && !empty ($models)) {
             //  this is the nominal code path
            echo "<b>$method</b> ";
            echo "the call to fetchMvcModelsAll() returned an array with <b>" . count($models) 
                .  "</b> items in the array <br>";                
        } else {
            echo "the call to fetchMvcModelsAll() returned a non-array <br>";
            return [];
        }
        
        $modelsRelativeUri = [];
        foreach ($models as $model){
            // path + file name arrary
            $relativeUri = $model->file_path_uri . '/' . $model->file_name;
            $modelsRelativeUri[] = $relativeUri;
        }
        if ($verbose){
            if (is_array($modelsRelativeUri)){
                echo "\$modelsRelativeUri to follow <br>";
                echo Barray::echo_array($modelsRelativeUri);
            }  
        }
        return $modelsRelativeUri;
    }

    /**
     * Convert an array in database format into a relative uri format
     * @return array $viewRelativeUri
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @calledBy actionLoadAllViews
     * @calls fetchMvcViewsAll
     * 
     * @package CEC Tracking v0.0.0.1
     */
    protected function fetchMvcViewsAllRelativeUri() {
        $verbose = false;
        $method = "fetchMvcViews AllRelativeUri()";
        $models = $this->fetchMvcViewsAll();
        if (is_array($models) && empty($models)){
            echo "fetchMvcViewsAll() returned an empty array <br>";
            return [];
        } elseif (is_array($models) && !empty ($models)) {
             //  this is the nominal code path
            echo "<b>$method</b> ";
            echo "the call to fetchMvcViewsAll() returned an array with <b>" . count($models) 
                .  "</b> items in the array <br>";              
        } else {
            echo "fetchMvcViewsAll() returned a non-array <br>";
            return [];
        }
        $viewRelativeUri = [];
        foreach ($models as $model){
            // path + file name arrary
            $relativeUri = $model->file_path_uri . '/' . $model->file_name;
            $viewRelativeUri[] = $relativeUri;
        }
        if ($verbose){
            if (is_array($viewRelativeUri)){
                echo "\$viewRelativeUri to follow <br>";
                echo Barray::echo_array($viewRelativeUri);
            }  
        }
        return $viewRelativeUri;
    }

    /**
     * Get a list of relative Uri from the metadata__codebase table
     * @return array $modelsRelativeUri
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @todo silence the intra if statement echos
     * @internal dev status = Testing Passed. Appears to be functionally solid.
     * @internal dev status = still outputs debug text.
     * @package CEC Tracking v0.0.0.1
     */
    protected function fetchMvcControllersAllRelativeUri() {
        $verbose = false;
        $method = "fetchMvcControllers AllRelativeUri()";
        $models = $this->fetchMvcControllersAll();
        if (is_null($models)){
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned null <br>";
            return [];
        }
        if (is_array($models) && empty($models)){
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned an empty array <br>";
            return [];
        } elseif (is_object($models)){
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned a single object rather than an array <br>";
            return [];
        } elseif (is_array($models) && !empty ($models)) {
             //  this is the nominal code path
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned an array with <b>" . count($models) 
                .  "</b> items in the array <br>";  
        } else {
            echo "<b>$method</b> ";
            echo "the call to fetchMvcControllersAll() returned a non-array non-object<br>";
            return [];
        }
        $controllerRelativeUri = [];
        foreach ($models as $model){
            // path + file name arrary
            $relativeUri = $model->file_path_uri . '/' . $model->file_name;
            $controllerRelativeUri[] = $relativeUri;
        }
        if ($verbose){
            if (is_array($controllerRelativeUri)){
                echo "<b>$method</b> running in verbose mode<br>";
                echo "\$controllersRelativeUri to follow <br>";
                echo Barray::echo_array($controllerRelativeUri);
            }  
        }
        return $controllerRelativeUri;
    }
    
    /** @package-sub-module END *********(get uri from the database )**********/
    
    /**
     * 
     * @param type $glob
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @package CEC Tracking v0.0.0.1
     */
    protected function convertGlobToRelativeUri($glob) {
        //$glob = Bfile::globFiles($sourceFolder);
        $filesRelativeUri=[];
        $basePath = Yii::app()->basePath;
        foreach ($glob as $item){
            if (!empty($item['file_uri'])){
                $fileUri = $item['file_uri'];
                $fileName = Bfile::base_name($fileUri);
                $fileUri2 = str_ireplace($basePath, '', $fileUri);  // base path removed
                $fileUri3 = str_ireplace($fileName, '', $fileUri2); // file name removed
                //$fileUri4 = Bfile::remove_trailing_slash($fileUri3);                            
                $filesRelativeUri[] = $fileUri3 . $fileName; 
            }
        }
        $verbose=false;
        if ($verbose){
            if (is_array($filesRelativeUri)){
                echo "\$filesRelativeUri to follow <br>";
                echo Barray::echo_array($filesRelativeUri);
            }     
        }
        return $filesRelativeUri;
    }
    
    /**
     * Determines which relative URI are not in the database
     * @param array $target A list of file URI from the hard disk
     * @param array $source A list of file URI from the database
     * @return array $return A list of uri in target that are not in source 
     * 
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @example findOrphansInUriArrays($fileUri,$modelUri)
     * 
     * @internal $target is typically a list of files from disk
     * @internal $source is typically a list from the database
     * @internal $return is list of files on disk that are not in the database
     * @package cec-y1x-cec-tracking-v0.0.0.1
     * @internal development status = Golden!
     */
    protected function findOrphansInUriArrays($target, $source) {
        $work = $target;
        $verbose=false;
        $debug=true;
        if ($debug){
            echo 'findOrphans - InUriArrays '
                . '<b>' .count($target) . ' files </b> uri (target) passed in and ' 
                . '<b>'. count($source) . ' models </b> uri (source) passed in <br>';
        }
        foreach ($source as $sourceItem){
            if (array_search($sourceItem, $work, $strict=true) !== false){
                $key = array_search($sourceItem, $work, $strict=true);
                $value = $work[$key];
                if ($verbose){
                    echo "source item = $sourceItem <br>";
                    echo "key = $key <br>";
                    echo "value in work = $value <br>";
                }
                if ($sourceItem == $value){
                    if ($verbose){echo "items match <br>";}
                    unset($work[$key]);
                }
            }
        }
        if ($debug){
            $removedCnt = (count($target) - count($work));
            echo 'findOrphans - InUriArrays '
                . '<b>Removed ' .$removedCnt . ' file uri</b> from the '.count($target). ' target uri passed in <br>'; 
                //. '<b>'. count($source) . '</b> model uri (source) passed in <br>';
        }        
        
        // remove directories
        $work2 = $work;

        foreach ($work as $key2 => $item){
            if (stripos($item, '//') !== false){
                if ($verbose){echo "removing item $item <br>";}
                unset($work2[$key2]);
            }            
        }
        
        if ($debug){
            $removedDirCnt = (count($work) - count($work2));
            echo 'findOrphans - InUriArrays '
                . '<b>Removed '.$removedDirCnt.' directories </b>  from the targets passed in <br>'; 
                //. '<b>'. count($source) . '</b> model uri (source) passed in <br>';
            echo 'findOrphans - InUriArrays '
                . '<b>Found ' . count($work2) . ' orphan </b> file uri in the targets passed in <br>';             
            echo Barray::echo_array($work2);
        }        
        //echo Barray::echo_array($work2);
        $return = $work2;
        return $return;
    }
    
    /**
     * 
     * @return array $viewFiles A one dimensional array of file globs and No directories
     * @calledBy cecAddNewFilesToCodebaseTracking
     * @calledBy actionLoadAllViewByDisk
     * @internal development status = Golden!
     * @package CEC Tracking v0.0.0.1
     */
    protected function globMvcViews($sourceFolderUri) {
        $viewFiles=[];
        $viewRootDir = Bfile::globFiles($sourceFolderUri);
        foreach ($viewRootDir as $globbedItem){
            $uri='';
            if (isset($globbedItem['file_uri']))
            {
                $uri = $globbedItem['file_uri'];
                if (strlen($uri)>1)
                {
                    $length=1;
                    $lastChar = substr($uri, strlen($uri)-1, $length);
                    //echo "last char = $lastChar <br>";

                    if ($lastChar === '/')
                    {

                        if (is_dir($uri)){
                            $viewDir = Bfile::globFiles($uri);
                            if ( is_array($viewDir) )
                            {
                                //echo Barray::echo_array($viewDir);
                                foreach ($viewDir as $fileGlob) {
                                    $viewFiles[]=$fileGlob;
                                }
                            //$viewFiles[]=$viewDir;
                            }
                        } 
                    } elseif(is_file($uri)) {
                        $viewFiles[]=$globbedItem;
                    }
                }

            }
        }

        echo "<b>Found " . count($viewFiles) . " files</b> on disk <br>";        
        return $viewFiles;
    }

    /**
     * Add any untracked models, views, and controllers into the code base metadata table
     * @param array $scope
     * @calledBy cecValidateAllRules
     * @internal this is a necessary step to insure that all-code is accounted for. 
     * @internal all-inclusive is crucial aspect of the cec tracking project
     * @internal dev status = Under Construction
     * @version 0.0.1.0 - completed in sprint #1 for this package
     * @internal Call Stack
     * @package CEC Tracking v0.0.0.1
     */
    protected function cecAddNewFilesToCodebaseTracking($scope=['models','views','controllers']) {
        // compare glob_files to file list from database
        // then array_diff( the two arrays )
        // insert the deltas result into the codebase table
        //$scope=['models'];
        $debug   = true;
        $verbose = true;
        
        if (is_array($scope)){
            
            $basePath = Yii::app()->basePath;
            $orphanUri=[];
            foreach ($scope as $codebaseCategory)
            {
                $sourceFolder = $basePath .'/'. $codebaseCategory ;
                
                if ($codebaseCategory === 'models'){

                    $filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    $modelsRelativeUri = $this->fetchMvcModelsAllRelativeUri();
                    $orphansM          = $this->findOrphansInUriArrays($filesRelativeUri, $modelsRelativeUri);
                    $orphanUri[$codebaseCategory] = $orphansM;
                    $this->insertManyRowsCodebaseTable($orphansM);

                } elseif ($codebaseCategory === 'views'){
                    $viewFiles         = $this->globMvcViews($sourceFolder);
                    $filesRelativeUri  = $this->convertGlobToRelativeUri($viewFiles);
                    $viewsRelativeUri  = $this->fetchMvcViewsAllRelativeUri();
                    $orphansV          = $this->findOrphansInUriArrays($filesRelativeUri, $viewsRelativeUri);
                    $orphanUri[$codebaseCategory] = $orphansV;
                    $this->insertManyRowsCodebaseTable($orphansV);

                } elseif ($codebaseCategory === 'controllers'){
                    
                    $filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    $controllersRelUri = $this->fetchMvcControllersAllRelativeUri();
                    $orphansC          = $this->findOrphansInUriArrays($filesRelativeUri, $controllersRelUri);
                    $orphanUri[$codebaseCategory] = $orphansC;
                    $this->insertManyRowsCodebaseTable($orphansC);

                }
            }
            return $orphanUri;
            
        } else {
            throw new CException("the parameter passed in must be an array");
        }        
    }
        
    /**
     * Returns an array of CActiveRecords by specifying file types to retrieve: models, views, controllers
     * @param array $scope default = ['models','views','controllers']
     * @throws CException
     * @calledBy cecValidateAllRules()
     * @internal dev status = Under Construction
     * @package CEC Tracking v0.0.0.1
     */
    protected function cecFindCodebaseItemsByScopeCategory($scope=['models','views','controllers']) {
        if (is_array($scope)){

            $itemsToProcess = [];
            foreach ($scope as $codebaseCategory){
                
                if ($codebaseCategory === 'models'){
                    $itemsToProcess[$codebaseCategory] = $this->fetchMvcModel();
                } elseif ($codebaseCategory === 'views'){
                    $itemsToProcess[$codebaseCategory] = $this->fetchMvcViews();
                } elseif ($codebaseCategory === 'controllers'){
                    $itemsToProcess[$codebaseCategory] = $this->fetchMvcControllers();
                }
            }
            
        } else {
            throw new CException("the parameter passed in must be an array");
        }
    }
    
    /**
     *
     * 
     * @param array $rules [0=>['rule name'=>'', 'cec tag'='', mixed $needles, 'regex_valid'=>'', 'regex_invalid'=>''], ]
     * @calledBy TBD
     * @calls $this-cecValidateOneRule()
     * @internal dev status = Ready for Testing
     * @package CEC Testing v1.0.0
     * @internal Call Stack
     *    cecFindCodebaseItemsByScopeCategory
     *    cecCodeFileHasChangedSinceLastCecCheck
     *    cecValidateOneRule()
     *      cecFind()
     *      cecMatch()
     * @todo get this working with models only
     * @internal pass in rule-categories to process eg model, view, controller?
     * @package CEC Tracking v0.0.0.1
     */ 
    public function cecValidateAllRules($rules, $scope=['models','views','controllers']) {
        $scopeTest = ['models'];
        $itemsToProcess = $this->cecFindCodebaseItemsByScopeCategory($scopeTest);
        //$itemsToProcess = $this->cecFindCodebaseItemsByScopeCategory($scope);
        
        // Process all items included by their scope category models, views, controllers
        foreach ($itemsToProcess as $testTarget){
            
           
            if (is_array($testTarget)){
                if (isset($testTarget['file_uri'])){
                    $targetFileUri = $testTarget['file_uri'];
                } else {
                    continue;
                }
            }
            // apply a codefile hasnt changed filter === 'yes'
            $apply_codefile_hasnt_changed_filter = 'yes';
            if ($apply_codefile_hasnt_changed_filter === 'yes'){
                // @todo items to process file uri
                $codefileHasChanged = $this->cecCodeFileHasChangedSinceLastCecCheck($targetFileUri);
                continue;
            }
            if (is_array($rules)){
                $results = [];
                // Run each rule for each code file processed
                foreach ($rules as $rule){
                    
                    $result = $this->cecValidateOneRule($rule);
                    $results[] = $result;
                }
                return $results;
            } else {
                throw new CException("the parameter passed in must be an array");
            }            
            
        }
        /*
        if (is_array($rules)){
            $results = [];
            foreach ($rules as $rule){
                $result = $this->cecValidateOneRule($rule);
                $results[] = $result;
            }
            return $results;
        } else {
            throw new CException("the parameter passed in must be an array");
        }
          
         * i'm saving this temporarilly to be used in case the loop above doesn't work as expected
         */      
    }
    
    /**
     *
     * 
     * @param array $rule ['rule name'=>'', 'cec tag'='', mixed $needles, 'regex_valid'=>'', 'regex_invalid'=>'']
     * @calledBy $this->cecValidateAllRules($rules)
     * @internal dev status = Ready for Testing
     * @package cec testing v1.0.0
     */ 
    protected function cecValidateOneRule($rule) {
        // Use string "found" approach?
        // Use string "not found" approach?
        // Use regex "found" approach?
        // Use regex "not found" approach?
        
        // select test approach or read test_approach from the rule definition
        if (!is_array($rule)){
            throw new CException("must pass an array");
        }
        // process string based tests
        if (isset($rule['cec_eval_stack']) ){
            $evalCallStack = $rule['cec_eval_stack'];
        }
        // process string based tests
        if (isset($rule['needle_posit']) ) {
            $needlePosit = $rule['needle_posit'];
            if (!empty($needlePosit)){
                $resultNeedlePosit = $this->cecFind($needlePosit); 
            } else {
                $resultNeedlePosit = 'skipped test';
            }
        }
        if (isset($rule['needle_negate']) ){
            $needleNegate = $rule['needle_negate'];
            if (! empty($needleNegate)){
                $resultNeedleNegate = $this->cecFind($needleNegate);
            } else {
                $resultNeedleNegate = 'skipped test';
            }            
        }
        // process regex based tests
        if (isset($rule['regex_posit']) ) {
            $regexPosit = $rule['regex_posit'];
            if (! empty($regexPosit)){
                $resultRegexPosit = $this->cecMatch($regexPosit);
            } else {
                $resultRegexPosit = 'skipped test';
            }            
            
        }
        if (isset($rule['regex_negate']) ){
            $regexNegate = $rule['regex_negate'];
            if (! empty($regexNegate)){
                $resultRegexNegate = $this->cecMatch($regexNegate);
            } else {
                $resultRegexNegate = 'skipped test';
            }            
            
        }        
        
        $results =
            [
                'needle posit test'  => $resultNeedlePosit,
                'needle negate test' => $resultNeedleNegate,
                
                'regex posit test'   => $resultRegexPosit,
                'regex negate test'  => $resultRegexNegate,                
            ];
        return $results;
    }
      
    
    /**
     * Write CEC status results to log table with sha256
     * @param @force default false - ignores file hasn't changed flag
     * @package CEC tracking v0.0.0.1
     * @since CEC tracking v0.0.0.1
     * @version 0.0.0.1
     * @internal Runs in a chatty manner updating the database as each code file is processed
     * @todo pseudo code this
     */
    protected function cecUpdateLog($targetFileUri, $force=false){
        
        $hash_enabled_yn = 'Y';
        $sha256 = '';
        // file has been modified
        $fileModifiedSinceLastUpdate = false;
        
        
        
        Bfile::globFiles($sha256);
        if ($hash_enabled_yn === 'Y'){
           $hash256 = hash_file('sha256', $targetFileUri);
        } else {
            $hash256 = 'null';
        }
        
        // fetch rules
        // loop through rules
    }
 
    /**
     * Returns an empty|blank cec rule template
     * @internal start with string needles then evolve towards regex and response type validations
     * @package cec testing v1
     */
    protected function cecRuleTemplate(){
        $cecRuleDefault = [
            'rule name'     =>'', 'cec tag'=>'',
            'needle_posit'  =>'', 'needle_negate'=>'', // string based rule
            //'regex_posit'   =>'', 'regex_negate' =>'', // regex based rule
            //'response_posit'=>'', 'response_negate' => '',
            'cec_eval_stack'=>''];
        return $cecRuleDefault;
    }

    
    /**
     * Execute a regular expression search based on a regex passed in
     * @param $regex      regular expression as an immutable string within single quotes
     * @param $regexMods  regex modifiers eg global parameters
     * @internal dev status = Under Construction
     * @calledBy cecValidateOneRule
     * @example $this->cecMatch('CEC y1c 0.0.0.1');
     * @package cec testing v1.00
     */
    protected function cecMatch($regex, $regexMods){
        // find matches
        $matches = [];
        $matchLog = [$matches, $match_cnt];
        return $matchLog;
    }
    
    /**
     * Execute a search based on a string parameter passed in
     * @param string $needle
     * @param string $replace
     * @param string $haystack
     * @internal dev status = Under Construction
     * @calledBy cecValidateOneRule
     * @example $this->cecFind('CEC y1c 0.0.0.1');
     * @package cec testing v1.0.0
     * 
     */
    protected function cecFind($needle, $replace='', $haystack) {
        // find matches
        $count=null;
        str_replace($needle, $replace, $haystack, $count);
        return $count;
        
    }
    
    /**
     * Execute a search based on a string parameter passed in
     * @param string $needle
     * @param string $replace
     * @param string $haystack
     * @return array ['count'=>1, 'before'=>'abcdef', 'after'=>'abc'];
     * @internal dev status = Under Construction
     * @calledBy cecValidateOneRule
     * @example $this->cecReplaceByString('package CEC y1c 0.0.0.1', 'package CEC y1c 0.0.2.1', $);
     * @package CEC Tracking v0.0.0.1
     */
    protected function cecReplaceByString($needle, $replace='', $haystack) {
        // find matches
        $count=null;
        $work = $haystack;
        str_replace($needle, $replace, $work, $count);
        return ['count'=>$count, 'before'=>$haystack, 'after'=>$work];
    }    
    
    
    /**
     * Execute a search based on a string parameter passed in
     * @param string $needle
     * @param string $replace
     * @param string $haystack
     * @internal dev status = Under Construction
     * @calledBy cecValidateOneRule
     * @example $this->cecReplaceByRegex('CEC y1c 0.0.0.1');
     * @package CEC Tracking v0.0.0.1
     */
    protected function cecReplaceByRegex($needle, $replace='', $haystack) {
        // find matches
        $findCount=null;
        str_replace($needle, $replace, $haystack, $count);
        return $count;
        
    }    
    
    // <editor-fold defaultstate="collapsed" desc="intra-package deadwood">
    /**
     * 
     * The validate function is always read-only and always returns valid, not-valid
     * @param string $targetFileUri
     * @return $answer yes | no | error
     * @package cec testing v1.0.0
     * @internal Wouldn't this typically be called in a loop? 
     * @internal   And thus Why hash a file during a loop?
     * @calls findCodebaseModelByPathStubAndFileName
     */
    protected function cecCodeFileHasChangedSinceLastCecCheck($targetFileUri){
        
        // 1. get the file hash
        // 2. lookup the file in the cec status log
        // 3. if log->hash == file->hash return 'no' else 'yes'
        
        $debug = true;
        $hash_enabled_yn = 'Y';
        
        // Run the database search first. If not found return 'yes'
        // i need file path and file name to run the query @see insert into codebase
        $model = $this->findCodebaseModelByPathStubAndFileName($targetFileUri);
        if (is_object($model)){
            // fetch related records?
        }
        // @todo make the item below
        $logHash = $model->log->sha256;
        
        $fileinfo = Bfile::globFiles($targetFileUri);
        if ($debug === true){ echo Barray::echo_array($fileinfo);}
        
        if ($hash_enabled_yn === 'Y'){
           $sha256 = hash_file('sha256', $targetFileUri);
        } else {
            $sha256 = 'null';
        }
        
        $cec_rules=[];
        $cec_rules[] = $this->cecRuleTemplate();
        
        if ($logHash === $sha256){
            $hasChanged = 'no';
        } else {
            $hasChanged = 'yes';
        }
        
        return $hasChanged;
    }    

    /**
     * 
     * @param string $fileUri
     * @return CActiveRecord | null
     * @throws CException
     * 
     * @calledBy cecCodeFileHasChangedSinceLastCecCheck
     */
    protected function findCodebaseModelByPathStubAndFileName($fileUri) {
        $basePath = Yii::app()->basePath;
        $fileName = Bfile::base_name($fileUri);
        $fileUri2 = str_ireplace($basePath, '', $fileUri);  // base path removed
        $fileUri3 = str_ireplace($fileName, '', $fileUri2); // file name removed
        $fileUri4 = Bfile::remove_trailing_slash($fileUri3);
        // @todo: xlat column names
        if (empty($fileName) || empty($fileUri4)){
            throw new CException("empty values");
        }
        Barray::echo_array(['file'=>$fileName, 'path'=>$fileUri4]);
        
        $criteria = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_name=:c1 AND file_path_uri=:c2';
        $criteria->params    = [':c1'=>$fileName, ':c2'=>$fileUri4];
        $model = MetadataCodebase::model()->find($criteria);   // will return null if not found
        
        if (! empty($model)){
            return $model;
        } else {
            return null;
        }       
    }
    
    // <editor-fold defaultstate="collapsed" desc="intra-package deadwood">
    /**
     * Write cec status results to log table with sha256
     * 
     * The validate function is always read-only and always returns valid, not-valid
     * @param string $targetFileUri
     * @return $valid
     * @package cec testing v1.0.0
     * @internal Wouldn't this typically be called in a loop? 
     * @internal   And thus Why hash a file during a loop?
     */
    protected function cecValidate($targetFileUri){
        $hash_enabled_yn = 'Y';
        $successful = false;
        $sha256 = '';
        Bfile::globFiles($sha256);
        if ($hash_enabled_yn === 'Y'){
           $hash256 = hash_file('sha256', $targetFileUri);
        } else {
            $hash256 = 'null';
        }
        
        $cec_rules=[];
        $statusTemplate = ['is valid', 'not-valid'];
        
        if ($successful === true){
            $staus = $statusTemplate[0]; // valid
        } else {
            $staus = $statusTemplate[1]; // not-valid
        }
        $valalidation_status = [];
        
        return $validatation_status;
    }

    
    /**
     *
     * Proves that the rule is true
     * @param array $rule ['rule name'=>'', 'cec tag'='', mixed $needles, 'regex_valid'=>'', 'regex_invalid'=>'']
     * @calledBy $this->cecValidateAllRules($rules)
     * @package cec tracking v001
     * 
     */ 
    protected function cecAssertOneRule($rule) {
        // Use string "found" approach?
        // Use string "not found" approach?
        // Use regex "found" approach?
        // Use regex "not found" approach?
        return $rule;
    }    
// </editor-fold>
    
    
    // </editor-fold>

    
    
    // <editor-fold defaultstate="collapsed" desc="package automated system tests v0.0.0.0">
    // package name: automated system tests v0.0.0.0
    // package purpose: To validate the code base and specifically codebase-to-database interactions.
    /**
     * @todo ? add _test sub folder to models/? A json data file in _test/ 
     *       could contain test-patterns for the model with the same file name?
     * @todo models->test->load, models->test->crud-base, models->test->crud-related
     * @todo use three separate files for the data testing configs? (load, crud-base, crud-related)
     * @todo add in a generic format like a yii extension to benefit any yii site with a database connection.
     * @todo nah - go with a tightly bound class eg add it quick - add to the code base 
     *       controller. add note in code to migrate later
     * @todo ?add self-test() to models? Add to AweActiveRecord?
     * @todo Recommend app parms table as the first self-verifying model
     * @todo Push a single row of data into the model - create, read, update, delete.
     * @todo Is there a way to automate regression testing?
     * @package cec  v1
     * http://localhost:8207/index.php/metadataCodebase/findCodeFilesInDatabase?cecID=2&readOnlyYn=N
     * @package CEC Tracking v0.0.0.1
     */
    public function actionLoadAllModels()
    {

        $basePath = Yii::app()->basePath;
        $folderName = $basePath . '/models';
        $fileList = Bfile::globFiles($folderName);
        echo "Found <b>" . count($fileList) . "</b> files on disk<br>";
        
        foreach ($fileList as $idx => $fileinfo){
            if (isset($fileinfo['file_uri'])){
                $modelName = Bfile::base_name($fileinfo['file_uri']);
                $modelsToSkip=['Children.php','Fathers.php','ContactForm.php',
                    'ContactFormGsm.php','FileUpload.php','Itemchildren.php','LoginForm.php',
                    ];
                if (substr($modelName,0,7) === 'AppUser'){
                    continue;
                } elseif (substr($modelName,0,7) === 'Assignm'){
                    continue;
                } elseif (substr($modelName,0,4) === 'Auth'){
                    continue;
                } elseif (array_search($modelName, $modelsToSkip, $strict=true) !== false){
                    continue;
                }
                $className = basename($modelName,'.php');
                $contents  = file_get_contents($fileinfo['file_uri']);

                echo "processing \$modelName $modelName <br>";
                echo "looking for **".'class '.$className.' **'."<br>";
                if (stripos($contents, 'class '.$className.' ') !== false){
                    echo "processing \$className $className <br>";
                    $model = new $className;
                    $this->modelCreateTestDataShell($model);                    
                } else {
                    echo "<b>skipping</b> over $idx - $modelName because it doesn't have a correct class name <br>";
                }
                
            }
            if ($idx >= 500){
                break;
            }
        }
    }

    
    /**
     * With the model open when this is called it writes data using the name
     * of the model into the models/_test folder
     * @package CEC Tracking v0.0.0.1
     */
    protected function modelCreateTestDataShell($model) {
        $rows = $model->findAll(['limit'=>10]);
        if (is_null($rows)){
            $count = 0;
        } elseif (is_array($rows)){
            $count = count($rows);
        }
        // Save to JSON
        //dd($rows);
        $rowsOut = [];
        $idx = 0;
        foreach ($rows as $idx=>$row){
            $rowsOut[]= $row->attributes;
            
        } 
        if (is_null($idx)){
            $idx=0;
        }

        
        //dd($rowsOut);
        $baseUrl = Yii::app()->basePath;
        $modelName = get_class($model) . '.php';
        $json = Bjson::encode_wtype($rowsOut);
        $fileName = str_ireplace('.php', '.json', $modelName);
        $filePath = $baseUrl .'/models/_test/';
        $fileUri  = $filePath . $fileName;
        echo "writting <b>". strlen($json) . "</b> bytes to $fileUri <br>";
        Bfile::write_to_file($fileUri, $json, $writeMode='w+');
        //Bfile::write_to_file($fileUri, $json);
        if (is_file($fileUri)){
            //trace("<b>$idx</b> rows of data written to $fileName", __METHOD__);
            echo "<span style='color:green;font-weight:bold;'><b>$idx</b> rows of data written to $fileName </span><br>";
            //echo "<b>$idx</b> rows of data written to $fileName <br>";
        } else {
            echo "<span style='color:red;font-weight:bold;'><b>zero</b> rows of data were written to $fileName </span><br>";
        }
    }    
    
    /**
     * @todo ? add _test sub folder to models/? A json data file in _test/ 
     *       could contain test-patterns for the model with the same file name?
     * @todo models->test->load, models->test->crud-base, models->test->crud-related
     * @todo use three separate files for the data testing configs? (load, crud-base, crud-related)
     * @todo add in a generic format like a yii extension to benefit any yii site with a database connection.
     * @todo nah - go with a tightly bound class eg add it quick - add to the code base 
     *       controller. add note in code to migrate later
     * @todo ?add self-test() to models? Add to AweActiveRecord?
     * @todo Recommend app parms table as the first self-verifying model
     * @todo Push a single row of data into the model - create, read, update, delete.
     * @package CEC Tracking v0.0.0.1
     */
    public function actionLoadAllContollers()
    {
        $basePath = Yii::app()->basePath;
        $baseUrl  = Yii::app()->baseUrl;
        $folderName = $basePath . '/controllers';
        $fileList = Bfile::globFiles($folderName);
        // get a list of controller routes
        $controllerUrls = []; // index by controller name? class name, route name?
        foreach ($fileList as $fileinfo){
            if (isset($fileinfo['file_uri'])){
                $fileUri = $fileinfo['file_uri'];
                if (! is_file($fileUri)){
                    throw new CException("Not a file");
                }
                $fileName = Bfile::base_name($fileUri);
                $contents = file_get_contents($fileUri);
                $controllerRouteName = "class name with lower init cap? <br>";
                // hard coded base list
                $viewUrlHard = ['/index','/view','/update','/create','/admin'];
                // a list of actions should be parsed via regex? Or iterated from the object?
                $actions = [];
                foreach ($viewUrlHard as $actionName){
                    $actions[$actionName] = $baseUrl . $controllerRouteName . $actionName;
                }
                $controllerUrls[$fileName] =  $actions;
                $className = "";
                $controller = new $className;
            }
        }
    }    

    /**
     * @todo ? add _test sub folder to models/? A json data file in _test/ 
     *       could contain test-patterns for the model with the same file name?
     * @todo models->test->load, models->test->crud-base, models->test->crud-related
     * @todo use three separate files for the data testing configs? (load, crud-base, crud-related)
     * @todo add in a generic format like a yii extension to benefit any yii site with a database connection.
     * @todo nah - go with a tightly bound class eg add it quick - add to the code base 
     *       controller. add note in code to migrate later
     * @todo ?add self-test() to models? Add to AweActiveRecord?
     * @todo Recommend app parms table as the first self-verifying model
     * @todo Push a single row of data into the model - create, read, update, delete.
     * @todo Load models by database or by disk? Loading by database is far more efficient.
     * fetchMvcViewsAllRelativeUri
     * @calls globMvcViews($sourceFolderUri) 
     * @internal dev status = Ready for Testing
     * @package CEC Tracking v0.0.0.1
     */
    public function actionLoadAllViewsByDisk()
    {
        $basePath = Yii::app()->basePath;
        $sourceFolderUri = $basePath . '/views';
        $fileList = $this->globMvcViews($sourceFolderUri);
        
        foreach ($fileList as $fileinfo){
            // process on the _form files for installatio
            if (isset($fileinfo['file_uri'])){
                $viewName = Bfile::base_name($fileinfo['file_uri']);
                $className = "";
                //$model = new $className;
            }
        }
    }
    
    /**
     * @calledBy A manually crafted URL
     * @example http://localhost:8207/index.php/metadataCodebase/findCodeFilesUsingRules?cecID=6&readOnlyYn=Y
     * @see $this->findCodeFilesUsingRules(cecID=6,readOnlyYn=Y)
     * @Calls 
     * @calls fetchMvcViewsAllRelativeUri()
     * @calls cecPackageInstallIntoViewFile()
     * 
     * @todo needs a corollary to xlatOneFile
     * @todo needs a corollary with manageCecStatus and xlat. Both are tight and tested.
     * @package CEC Tracking v0.0.0.1
     */
    public function actionLoadAllViewsBySql()
    {
        $basePath = Yii::app()->basePath;
        //$folderName = $basePath . '/views';
        $fileList = $this->fetchMvcViewsAllRelativeUri();
        $installTests=[];
        $filesSkipped=[];
        foreach ($fileList as $relativeUri){
            // process on the _form files for installatio
            $fileUri = $basePath .'/'. $relativeUri;
            if (is_file($fileUri)){
                if (strpos($relativeUri, '/_form.php') !== false){
                    // test cec installation
                    // next function works sorta like xlatOneFile but seems less 
                    // evolved code wise especially around error testing 
                    // the method below is fast prototype made for time efficiency 
                    // to see if views can be the first recipients of a bulk-applied 
                    // CEC code injection using a string identifier rather than regex
                    $test = $this->cecPackageInstallIntoViewFile($fileUri); 
                    $installTests[$relativeUri]=$test;
                } else {
                    $filesSkipped[] = $relativeUri;
                }

            }
        }
        if (count($installTests) > 0){
            //Barray::echo_array($installTests);
            dd($installTests);
        }
        $debug = true;
        if ($debug){
            dd($filesSkipped);
        }
    }    

    /**
     * 
     * @calledBy actionLoadAllViews as a prototype for CEC code injection into views
     * @version 0.0.0.1
     * @internal dev status = Ready for Testing
     * @internal this method has a similar function to xlatOneFile. xlat is more 
     * @internal polished. consider pulling code from there.
     * @internal dev status = Ready for Testing
     * @package CEC Tracking v0.0.0.1
     */
    protected function cecPackageInstallIntoViewFile($viewFileUri) {
        if (! is_file($viewFileUri)){
            throw new CException("the file path passed in is invalid");
        }
        if (true){
            $basePath = Yii::app()->basePath;
            //$file = '/var/www/sites/gsm/dev/mvp-yii-1.1.19/mvp-yii-1.1.19/protected/views/_cec/cec-y1v-0001-v.0.0.0.1-install.php';
            $settingsFile = '/views/_cec/cec-y1v-0001-v.0.0.0.1-install.php';
            $settingsFilePath = $basePath . $settingsFile;
            require_once($settingsFilePath);
            /*
                [
                  'find'=>['string' => $find,'regex' => '',],
                  'replace'=>['string' => $replace,'regex' => '',
                ]
             * 
             */
            $install = cecPackageInstall::package();
            // prep array for passing
            $rule=[
                0=>[
                'find'    => $install['find']['string'],
                'replace' => $install['replace']['string'],
                ]
            ];
            
            //$content = file_get_contents($viewFileUri);
            //$test = $this->replaceStringsUsingRules($content, $rule);
            $test = $this->xlatOneFileByRules($viewFileUri, $rule, $readOnlyYn='Y');
            return $test;
        }
    }
    
    
    /**
     * 
     * @param type $controllerUri
     * @throws CException
     * @category cec: 001 self-testing code base
     * @author Dana Byrd <danabyrd@byrdbrain.com>
     * @internal dev status = Ready for Testing
     * @todo return html with embedded urls
     * @package CEC Tracking v0.0.0.1
     */
    protected function fetchControllerActions($controllerUri) {
        
        if (! is_file($controllerUri)){
            throw new CException("not a controller file");
        }
        $contents = file_get_contents($controllerUri);
        // @todo escape the regex string below for php?
        $pattern = '/public function action(.*?)\(/';
        $matchInfo = [];
        $flags = '';
        $matches = preg_match_all($pattern, $contents, $matchInfo, $flags);
        
        if (! empty($matches)){
            echo Barray::echo_array($matches);
            
            // produce html output
            foreach ($matchInfo as $match){
                echo "matchInfo Item <br>";
                echo Barray::echo_array($match);
            }
        }
    }
    
    /**
     * Use regex to find all the methods in a code file
     * @param type $fileUri
     * @throws CException
     * @category cec: 001 self-testing code base
     * @author Dana Byrd <danabyrd@byrdbrain.com>
     * @internal dev status = Ready for Testing
     * @todo return html with embedded urls
     * @package CEC Tracking v0.0.0.1
     */
    protected function fetchMethodsFromCodeFileByFileUri($fileUri) {
        
        if (! is_file($fileUri)){
            throw new CException("not a controller file");
        }
        $contents = file_get_contents($fileUri);
        // kiki 'public function (.*\(.*\))|protected function (.*\(.*\))'
        // @todo escape the regex string below for php?
        $pattern = '/public function (.*\(.*\))|protected function (.*\(.*\))/';
        $matchInfo = [];
        //$flags = ''; // none dot=all needs to be turned off for the regex above to work
        $matches = preg_match_all($pattern, $contents, $matchInfo);
        
        if (! empty($matches)){
            dd($matches);
            //echo Barray::echo_array($matches);
            
            // produce html output
            $output=[];
            foreach ($matchInfo as $idx => $match){
                
                if ($idx === 0){
                    $matchName = 'all methods'; 
                } elseif ($idx === 1){
                    $matchName = 'public methods'; 
                } elseif ($idx === 2){
                    $matchName = 'protected methods'; 
                }
                $output[$matchName] = $match ;
                //echo "match item in array <b>$idx</b><br>";
                echo "<br><br><b>$matchName</b> match items in array <b>#$idx</b> <br>";
                //echo Barray::echo_array($match);
                dd($match);
            }
        }
    }
    
    /**
     * 
     * @param string $contents The contents of a code file
     * @throws CException
     * @category cec: 001 self-testing code base
     * @author Dana Byrd <danabyrd@byrdbrain.com>
     * @internal dev status = Ready for Testing
     * @todo return html with embedded urls
     * @see 
     * @internal dev status = Under Construction
     * @package CEC Tracking v0.0.0.1
     * 
     */
    protected function cecMatchPackagesCodeBlock($contents) {
        
        if (! is_file($controllerUri)){
            throw new CException("not a controller file");
        }
        //$contents = file_get_contents($uri);
        // kiki original regex '/\*\*.\s\*\sCEC - Internal Packages.+?\/\*\* End of CEC - Internal Packages \*/'
        // @todo escape the regex string below for php?
        $pattern = '//\*\*.\s\*\sCEC - Internal Packages.+?\/\*\* End of CEC - Internal Packages \*//';
        $matchInfo = [];
        $flags = 'S'; // dot matches everything inlcuding line endings
        $matches = preg_match_all($pattern, $contents, $matchInfo, $flags);
        
        if (! empty($matches)){
            //echo Barray::echo_array($matches);
            
            // produce html output
            $output = [];
            foreach ($matchInfo as $idx => $match){
                if ($idx === 0){
                    $matchName = 'full string match';
                }elseif ($idx === 1){
                    $matchName = 'package names';
                }
                $output[$matchName] = $match;
                echo "matchInfo Item <br>";
                echo "<br><br><b>$matchName</b> match items in array <b>#$idx</b> <br>";
                //echo Barray::echo_array($match);
                dd($match);
            }
            return $output;
        }
    }
    
    /**
     * Returns the comment block above a CEC code injection block
     * @param string $contents The contents of a code file
     * @throws CException
     * @category cec: 001 self-testing code base
     * @author Dana Byrd <danabyrd@byrdbrain.com>
     * @internal dev status = Ready for Testing
     * @todo return html with embedded urls
     * 
     */
    protected function cecMatchCecCommentBlock($contents) {
        
//        if (! is_file($contents)){
//            throw new CException("not a controller file");
//        }
        //$contents = file_get_contents($uri);
        // @todo escape the regex string below for php?
        $pattern = '/\/\*\*.\s\*\sCEC - Consistent Engineering Criteria.+?\*\//';
        $matchInfo = [];
        $flags = 'S'; // dot matches everything inlcuding line endings
        $matches = preg_match_all($pattern, $contents, $matchInfo, $flags);
        
        if (! empty($matches)){
            echo "The all matches array follows<br>";
            //echo Barray::echo_array($matches);
            dd($matches);
            
            // produce html output
            foreach ($matchInfo as $idx => $match){
                echo "matchInfo Item \$idx=<b>$idx</b><br>";
                //echo Barray::echo_array($match);
                dd($match);
            }
        }
    }    


    
    /**
     * 
     * @param string $fileUri
     * @throws CException
     * @category cec: 001 self-testing code base
     * @author Dana Byrd <danabyrd@byrdbrain.com>
     * @internal dev status = Ready for Testing
     * @todo return html with embedded urls
     * @calledBy ???
     */
    protected function cecMatchCecCommentBlockByUri($fileUri) {
        
        if (! is_file($fileUri)){
            throw new CException("not a controller file");
        }
        $contents = file_get_contents($fileUri);
        // @todo escape the regex string below for php?
        $pattern = '/\/\*\*.\s\*\sCEC - Consistent Engineering Criteria.+?\*\//';
        $matchInfo = [];
        $flags = 'S'; // dot matches everything inlcuding line endings
        $matches = preg_match_all($pattern, $contents, $matchInfo, $flags);
        
        if (! empty($matches)){
            echo "<b>" . count($matches) . "</b> matches found <br>";
            echo Barray::echo_array($matches);
            
            // produce html output
            foreach ($matchInfo as $match){
                echo "matchInfo Item <br>";
                echo Barray::echo_array($match);
            }
        }
    }
    
    /** @package-functionality-block-begin********(fetch models from db)*******/
    // <editor-fold defaultstate="collapsed" desc="fetchMvc* from database">
    
    /**
     * Get a list of a call controller code files from the database.
     * @internal dev status = Golden!
     */
    protected function fetchMvcModelsAll() {
        $fileUriStub = '/models';
        $verbose = false;
        $criteria = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_path_uri=:c1';
        $criteria->params    = [':c1'=>$fileUriStub];
        $models = MetadataCodebase::model()->findAll($criteria);   // will return null if not found
        if (is_array($models)){
            if (isset($models[0]) && is_object($models[0])){
                $recordsFound = true;
                $count = count($models);
                if ($verbose){
                    echo "fetchMvcModelsAll() found <b>$count</b> models <br>";
                }
                return $models;
            } else {
                $recordsFound = true;
            }
        } else {
            echo "no records found! <br>";
        }
        
        if (empty($models )){
            echo "no records found! <br>";
            die;
            return;
            
        }
    }
    
    /**
     * Get a list of a call controller code files from the database.
     * @internal dev status = Ready for Testing
     */
    protected function fetchMvcViewsAll() {
        $fileUriStub = '/views%';
        $verbose = false;
        $criteria = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_path_uri like :c1';
        $criteria->params    = [':c1'=>$fileUriStub];
        $models = MetadataCodebase::model()->findAll($criteria);   // will return null if not found
        if (is_array($models)){
            if (isset($models[0]) && is_object($models[0])){
                $recordsFound = true;
                $count = count($models);
                if ($verbose){
                    echo "fetchMvcViewsAll() found <b>$count</b> views <br>";
                }
                return $models;
            } else {
                $recordsFound = true;
            }
        }
        return [];
    }        

    /**
     * Get a list of a call controller code files from the database.
     * @internal dev status = Ready for Testing
     */
    protected function fetchMvcControllersAll() {
        $fileUriStub         = '/controllers';
        $verbose = false;
        $criteria            = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_path_uri=:c1';
        $criteria->params    = [':c1'=>$fileUriStub];
        $models = MetadataCodebase::model()->findAll($criteria);   // will return null if not found
        if (is_array($models)){
            if (isset($models[0]) && is_object($models[0])){
                $recordsFound = true;
                $count = count($models);
                if ($verbose){
                    echo "fetchMvcControllersAll() found <b>$count</b> controllers <br>";
                }
                return $models;
            } else {
                $recordsFound = true;
            }
        }
        if (is_object($models)){
            
        } else {
            throw new CException ("not an object!");
        }
        return [];
    }        
    // </editor-fold>
    /** @package-functionality-block-end **************************************/
    
    
    /**
     * @todo receive an array from fetch controllers all
     * @calls fetchControllerActions
     */
    public function actionListControllers() {
        $count = (int) $this->fetchControllersAll();
        echo "<b>$count</b> controllers found <br>";
        echo "<b>$count</b> controller actions found <br>";
        
        foreach ($count as $controller){
            $actions = $this->fetchControllerActions($controller);
            echo "<b>$controller</b> actions found <br>";
            
            if (is_array($actions)){
                Barray::echo_array($actions);
            } else {
                echo "<b>$controller</b> No actions found! <br>";
            }
        }
    }
    
    /**
     * @calledBy $this->actionLoadAllModels()
     * @internal dev status = Ready for Testing
     */
    protected function testModel($model) {
        // if the test method exists then execute it.
        $method_name = 'testModel';
        if (method_exists($model, $method_name)){
            $result = $model->testModel();
            if (is_array($result)){
                echo "testing model <br>";
                Barray::echo_array($result);
            }
        }
        
    }

    /**
     * @calledBy $this->actionLoadAllModels()
     * @internal 
     * @
     */
    protected function testController($controller) {
        // if the test method exists then execute it.
        $method_name = 'testController';
        if (method_exists($controller, $method_name)){
            $result = $controller->testModel();
            if (is_array($result)){
                echo "testing controller <br>";
                Barray::echo_array($result);
            }
        }
        
    }    
    
    /**
     * @calledBy $this->actionLoadAllModels()
     * @internal dev status = Ready for Testing
     */
    protected function testView($model, $view) {
        // if the test method exists then execute it.
        $method_name = 'testView';
        
        if (method_exists($model, $method_name)){
            $result = $model->testModel();
            if (is_array($result)){
                echo "testing view <br>";
                Barray::echo_array($result);
            }
        }
        
    }  
    
    /**
     * 
     * @param CActiveRecord $codebaseModel
     * @calledBy fetchAllPackageNamesInCodeFileByModel
     */
    protected function cecDbToUri($codebaseModel) {
        $model = $codebaseModel;
        if (! is_object($model)){
            throw new CException("A MetadataCodebase model must be passed in");
        }
        if (!isset($model->file_name) || empty($model->file_name)){
            throw new CException("The codebase file_name is not present");
        }
        if (!isset($model->file_path_uri) || empty($model->file_path_uri)){
            throw new CException("The codebase file_path_uri is not present");
        }       
        $basePath = Yii::app()->basePath;
        $fileUri = $basePath . '/'. $model->file_path_uri .'/'.$model->file_name;
        
        if (is_file($fileUri)){
            return $fileUri;
        } else {
            throw new CException("file was not found!");
        }
        
    }
    
    /**
     * 
     * @param CActiveRecord $codebaseModel
     * @calls cecDbToUri($codebaseModel)
     * @calls fetchMethodsFromCodeFileByFileUri($fileUri)
     * @calls 
     */
    protected function fetchAllPackageNamesInCodeFileByModel($codebaseModel) {
        $fileUri = $this->cecDbToUri($codebaseModel);
        if (isset($codebaseModel->file_name)){
            $fileName = $codebaseModel->file_name;
        } else {
            $fileName = 'unknown file name';
        }
        $content = file_get_contents($fileUri);
        if (empty($content)){
            throw new CException("content is empty! eg the code file is empty");
        }
        echo "<br><b>" . strlen($content) . "</b> bytes of code file content found in <b>$fileName</b><br>";
        $pattern = '/@package (.+?)\n/';
        $matches=null;
        preg_match_all($pattern, $content, $matches);
        $output = [];
        //echo "<br>All matches follow <br>";
        //dd($matches);
        foreach ($matches as $outerIdx => $match){
            echo "Outer loop \$OuterIdx = <b>$outerIdx</b> <br>";
            $fullStringMatches=[];
            if ($outerIdx === 0){
                // A full string match, the outer match that we don't want
                $fullStringMatches[] = $match;
            }elseif ($outerIdx === 1){
                echo "<b>". count($match, COUNT_RECURSIVE) ."</b> elements in cec item match <br>"; 
                $dupesRemoved = [];
                foreach ($match as $matchItem){
                    //echo "\$matchItem = $matchItem <br>";
                    //dd($matchItem);
                    
                    
                    //echo "<b>". count($match, COUNT_RECURSIVE) ."</b> elements in match <br>"; ;
                    //dd($match);
                    if (array_search($matchItem, $output, $strict=false) !== false){
                        //echo "found the item (match $matchItem) in position $pos $output[$pos] <br>";
                        $dupesRemoved[] = $matchItem;
                        continue;
                    } else {

                        $output[] = $matchItem;
                    }
                }
            }
            
            //dd($match);
        }
        $temp = $output;
        arsort($temp);
        $output2 = $temp;
        $output3 = [];
        foreach($temp as $t1){
            $output3[] = $t1;
        }
        dd($output2);
        dd($output);
        echo "<br>";
        //dd($output);
        echo "<br><br>Methods in <b>$fileName</b> follow <br>";
        $methods = $this->fetchMethodsFromCodeFileByFileUri($fileUri);
        dd($methods);
        return $output3;
    }
    
    /**
     * http://localhost:8207/index.php/metadataCodebase/findPackagesById?id=413
     * @param int|string $codebaseModelPk
     * @calls $this->fetchAllPackageNamesInCodeFileByModel($model)
     */
    protected function fetchAllPackageNamesInCodeFileById($codebaseModelPk) {
        $model = MetadataCodebase::model()->findByPk($codebaseModelPk);
        return $this->fetchAllPackageNamesInCodeFileByModel($model);

    }
    

    // </editor-fold>

    

    // <editor-fold defaultstate="collapsed" desc="depreciated code">
    
    /**
     * Add any untracked models, views, and controllers into the code base metadata table
     * @param array $scope
     * @calledBy cecValidateAllRules
     * @internal this is a necessary step to insure that all-code is accounted for. 
     * @internal all-inclusive is crucial aspect of the cec tracking project
     * @internal dev status = depreciated 
     * @version 0.0.0.2
     * @deprecated since version 0.0.0.2
     */
    protected function cecAddNewFilesToCodebaseTracking_v0002($scope=['models','views','controllers']) {
        // compare glob_files to file list from database
        // then array_diff( the two arrays )
        // insert the deltas result into the codebase table
        //$scope=['models'];
        $debug   = true;
        $verbose = true;
        
        if (is_array($scope)){
            
            $basePath = Yii::app()->basePath;
            $orphanUri=[];
            foreach ($scope as $codebaseCategory)
            {
                $sourceFolder = $basePath .'/'. $codebaseCategory ;
                
                if ($codebaseCategory === 'models'){

                    $filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    $modelsRelativeUri = $this->fetchMvcModelsAllRelativeUri();
                    $orphansM          = $this->findOrphansInUriArrays($filesRelativeUri, $modelsRelativeUri);
                    $orphanUri[$codebaseCategory] = $orphansM;
                    //$this->insertManyRowsCodebaseTable($orphansM);

                } elseif ($codebaseCategory === 'views'){
                    $viewFiles=[];
                    $viewRootDir = Bfile::globFiles($sourceFolder);
                    foreach ($viewRootDir as $idx => $globbedItem){
                        $uri='';
                        if (isset($globbedItem['file_uri']))
                        {
                            $uri = $globbedItem['file_uri'];
                            if (strlen($uri)>1)
                            {
                                $lastChar = substr($uri, strlen($uri)-1, $length=1);
                                //echo "last char = $lastChar <br>";
                            
                                if ($lastChar === '/')
                                {
                                  
                                    if (is_dir($uri)){
                                        $viewDir = Bfile::globFiles($uri);
                                        if ( is_array($viewDir) )
                                        {
                                            //echo Barray::echo_array($viewDir);
                                            foreach ($viewDir as $fileGlob) {
                                                $viewFiles[]=$fileGlob;
                                            }
                                        //$viewFiles[]=$viewDir;
                                        }
                                    } 
                                } elseif(is_file($uri)) {
                                    $viewFiles[]=$globbedItem;
                                }
                            }
                            
                        }
                        //echo "Item " . Barray::echo_array($globbedItem) . "<br>";
//                        if (is_dir($item)){
//                            
//                        }
                        //if ( (int)$idx > 5 ){break;}
                    }
                    //$filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    // create a single dimensional array
                    
                    echo "<b>Found " . count($viewFiles) . " files</b> on disk <br>";
                    //echo Barray::echo_array($viewFiles);
                    $filesRelativeUri  = $this->convertGlobToRelativeUri($viewFiles);
                    $viewsRelativeUri  = $this->fetchMvcViewsAllRelativeUri();
                    $orphansV          = $this->findOrphansInUriArrays($filesRelativeUri, $viewsRelativeUri);
                    $orphanUri[$codebaseCategory] = $orphansV;
                    //$this->insertManyRowsCodebaseTable($orphansV);

                } elseif ($codebaseCategory === 'controllers'){
                    
                    $filesRelativeUri  = $this->convertGlobToRelativeUri(Bfile::globFiles($sourceFolder));
                    $controllersRelUri = $this->fetchMvcControllersAllRelativeUri();
                    $orphansC          = $this->findOrphansInUriArrays($filesRelativeUri, $controllersRelUri);
                    $orphanUri[$codebaseCategory] = $orphansC;
                    //$this->insertManyRowsCodebaseTable($orphansC);

                }
            }
            return $orphanUri;
            
        } else {
            throw new CException("the parameter passed in must be an array");
        }        
    }
      
    /**
     * Get a list of a call controller code files from the database.
     * @internal this doesn't work for views. A like statement is needed.
     * @deprecated since version 0.0.0.2
     */
    protected function fetchMvcViewsAllv1() {
        $fileUriStub = '/views';
        $criteria = new CDbCriteria();
        $criteria->select    = 'codebase_id, file_name, file_path_uri';
        $criteria->condition = 'file_path_uri=:c1';
        $criteria->params    = [':c1'=>$fileUriStub];
        $models = MetadataCodebase::model()->findAll($criteria);   // will return null if not found
        if (is_array($models)){
            if (isset($models[0]) && is_object($models[0])){
                $recordsFound = true;
                $count = count($models);
                echo "found <b>$count</b> models <br>";
                return $models;
            } else {
                $recordsFound = true;
            }
        } else {
            echo "no records found! <br>";
        }
        
        if (empty($models )){
            echo "no records found! <br>";
            die;
            return;
            
        }
    }

// </editor-fold>
        
    
    // <editor-fold defaultstate="collapsed" desc="dead wood code">
    
    /**
     * 
     * @param type $mixedParam
     * @return int
     */
    protected function parseFileDirectoryFromArray($mixedParam) {
        echo "parsing file directory <br>";
        if (! isset($mixedParam)){
            echo "empty variable passed in <br>";
            return 0;
        }
        if (is_array($mixedParam)){
            echo "its an array <br>";
        } elseif (is_string($mixedParam)) {
            return $mixedParam;
        }
        
    }
    
    
    /**
     * Writes revised source code to the hard disk
     * @param type $fileUriList
     * @calls $this->replaceSchema_fieldNames_v1_names_to_v3_names
     * @internal dev status = Ready for Testing
     */
    protected function translateSchemaColumnNames($fileUriList, $readOnlyYn='Y') {
        //$readOnlyYn = "Y";
        $ruleSetResultLog = [];
        foreach($fileUriList as $fileUri){
            if (is_file($fileUri)){
                $fileContentsOriginal = file_get_contents($fileUri);
                $ruleSetResult = $this->replaceStrings($fileContentsOriginal);
                $ruleSetResultLog[] = $ruleSetResult;
                if ($readOnlyYn === 'N'){
                    // write to the source file with the replacesments
                    if (isset($ruleSetResult['replacement cnt'])){
                        $replacementCnt = $ruleSetResult['replacement cnt'];
                        if (isset($ruleSetResult['revised'])){
                            $revisedText = $ruleSetResult['revised'];
                        }
                        if ($replacementCnt > 0 && strlen($revisedText > 0)){
                            file_put_contents($fileUri, $revisedText);
                            $replacementCnt=0;
                            $revisedText='';
                        }
                    }
                }
            }
        }
    }
    
    /**
     * @internal WARNING: Does NOT filter ROWS from a MultiRow table - will return false positives
     * @deprecated since version 0.53
     */
    protected function fetchRowsToProcess_hasCecStatusOf2() {

        $sql = "SELECT mc.* 
                FROM metadata__codebase mc
                        left outer join metadata__codebase_cec_status mccs
                                on mc.codebase_id = mccs.codebase_id 
                where mccs.codebase_cec_id = 2;";        
        $params = [];
        $rows = MetadataCodebase::model()->findAllBySql($sql, $params);          
        return $rows;
    }
      
    /**
     * 
     * @return CActiveRecord
     */
    protected function fetchRowsToProcess_hasNoStatusRows() {
        // find rows that don't have a status
        $sql = "SELECT mc.* 
                FROM metadata__codebase mc
                        left outer join metadata__codebase_cec_status mccs
                                on mc.codebase_id = mccs.codebase_id 
                where mccs.codebase_id is null;";
        $params = [];
        $rows = MetadataCodebase::model()->findAllBySql($sql, $params); 
        return $rows;        
    }
    
    /**
     * Return string keyed array ['file_uri'=>'somefile']
     * @param type $fileListArray
     * @return type
     * @calledBy $this->actionPopulate(), $this->actionPopulateStrict()
     */
    protected function parseFilenameAndUriFromAFileList($fileListArray) {
        $uriOut=[];
        if (is_array($fileListArray)){
            foreach($fileListArray as $metadata){
                if (isset($metadata['file'])  && isset($metadata['file_uri'])){
                    $fileName = $metadata['file'];
                }                
                if (isset($metadata['file_uri'])){
                    $fileUri = $metadata['file_uri'];
                }
                
            }
        }
        return $uriOut;
    }    
    
    // </editor-fold>


}

