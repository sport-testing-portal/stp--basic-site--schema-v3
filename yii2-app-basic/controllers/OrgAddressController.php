<?php

namespace app\controllers;

use Yii;
use app\models\OrgAddress;
use app\models\OrgAddressSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrgAddressController implements the CRUD actions for OrgAddress model.
 */
class OrgAddressController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrgAddress models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrgAddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrgAddress model.
     * @param integer $org_id
     * @param integer $address_id
     * @return mixed
     */
    public function actionView($org_id, $address_id)
    {
        $model = $this->findModel($org_id, $address_id);
        return $this->render('view', [
            'model' => $this->findModel($org_id, $address_id),
        ]);
    }

    /**
     * Creates a new OrgAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrgAddress();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'org_id' => $model->org_id, 'address_id' => $model->address_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OrgAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $org_id
     * @param integer $address_id
     * @return mixed
     */
    public function actionUpdate($org_id, $address_id)
    {
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new OrgAddress();
        }else{
            $model = $this->findModel($org_id, $address_id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'org_id' => $model->org_id, 'address_id' => $model->address_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OrgAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $org_id
     * @param integer $address_id
     * @return mixed
     */
    public function actionDelete($org_id, $address_id)
    {
        $this->findModel($org_id, $address_id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    /**
    * Creates a new OrgAddress model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($org_id, $address_id) {
        $model = new OrgAddress();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($org_id, $address_id);
        }
    
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'org_id' => $model->org_id, 'address_id' => $model->address_id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Finds the OrgAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $org_id
     * @param integer $address_id
     * @return OrgAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($org_id, $address_id)
    {
        if (($model = OrgAddress::findOne(['org_id' => $org_id, 'address_id' => $address_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
